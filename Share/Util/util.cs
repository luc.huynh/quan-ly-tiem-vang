﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.Util
{
    public static class Util
    {
        public static string TaoMaTheoNamThangNgayGioPhutGiay()
        {
            DateTime now = DateTime.Now;

            return now.Year.ToString() +
                now.Month.ToString() +
                now.Day.ToString() +
                now.Hour.ToString() +
                now.Minute.ToString() +
                now.Second.ToString();
        }

        public static bool CheckMaTemValid(string maTem)
        {
            try
            {
                if (maTem.Length < 5) return false;

                maTem = maTem.ToUpper();

                if (maTem[0] <= 'A' && 'Z' >= maTem[0]) return false;

                int.Parse(maTem.Substring(1));

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public static double LamTronKhoiLuongCan(double khoiLuongCan)
        {
            return Math.Round(khoiLuongCan, 1);
        }
    }
}
