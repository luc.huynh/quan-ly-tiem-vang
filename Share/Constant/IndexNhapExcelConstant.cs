﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.Constant
{
    public static class IndexNhapExcelConstant
    {
        public static int MA_SP = 0;
        public static int TEN_SP = 1;
        public static int MA_TEM = 2;
        public static int GIA_BAN = 3;
        public static int TIEN_CONG = 4;
        public static int NGAY_NHAP = 5;
        public static int HAM_LUONG_VANG = 6;
        public static int TI_LE_HOT = 7;
        public static int KHOI_LUONG_TINH = 8;
        public static int KHOI_LUONG_DA = 9;
        public static int KHOI_LUONG_VANG = 10;
        public static int KI_HIEU = 11;
        public static int NHA_CUNG_CAP = 12;
        public static int MA_CHUYEN_KHO = 13;
    }
}
