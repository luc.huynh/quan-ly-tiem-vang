﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.Constant
{
    public static class EmployeePermission
    {
        public static string NHAN_VIEN = "NHANVIEN";
        public static string QUAN_LY = "QUANLY";
        public static string NHAP_KHO_YES = "YES";
        public static string NHAP_KHO_NO = "NO";
    }
}
