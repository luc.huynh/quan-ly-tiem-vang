﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.Constant
{
    public static class TieuChiThongKeConstant
    {
        public static int VALUE_LOAI_GIAO_DICH_MUA = 1;
        public static int VALUE_LOAI_GIAO_DICH_BAN = 2;
        public static int VALUE_LOAI_GIAO_DICH_DOI_VANG = 3;

        public static string DISPLAY_LOAI_GIAO_DICH_MUA = "Mua vàng";
        public static string DISPLAY_LOAI_GIAO_DICH_BAN = "Bán vàng";
        public static string DISPLAY_LOAI_GIAO_DICH_DOI_VANG = "Vàng đổi vàng";

        public static int VALUE_THOI_GIAN_HOM_NAY = 1;
        public static int VALUE_THOI_GIAN_KHOAN_THOI_GIAN = 2;

        public static string DISPLAY_THOI_GIAN_HOM_NAY = "Hôm nay";
        public static string DISPLAY_THOI_GIAN_KHOAN_THOI_GIAN = "Chọn khoản thời gian";
    }
}
