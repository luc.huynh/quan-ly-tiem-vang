﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.Constant
{
    public static class Constant
    {
        public static int NHAP_KHO_MOI = 1;
        public static int NHAP_KHO_CU = 2;

        public static string MA_SAN_PHAM_DEFAULT = "A0000";
        public static string MA_KHACH_HANG_DEFAULT = "1";

        public static int BAD_VALUE = -1;

        public static int VANG_CU_MUA_VAO = 1;
        public static int VANG_CU_DOI_NGANG = 2;
        public static int VANG_CU_CAT_BOT = 3;
        public static int VANG_THANH_LY_CAM_DO = 4;
        public static int VANG_BAN_RA = 5;
        public static int VANG_DOI_NGANG_RA = 6;

        public static string VANG_CU_MUA_VAO_DISPLAY = "Vàng mua vào";
        public static string VANG_CU_DOI_NGANG_DISPLAY = "Vàng đổi ngang";
        public static string VANG_CU_CAT_BOT_DISPLAY = "Vàng cắt";
        public static string VANG_THANH_LY_CAM_DO_DISPLAY = "Vàng thanh lý cầm đồ";
        public static string VANG_BAN_RA_DISPLAY = "Vàng bán ra";
        public static string VANG_DOI_NGANG_RA_DISPLAY = "Vàng đổi ngang ra";

        public static string MALE = "NAM";
        public static string FEMALE = "NỮ";

        public static string YES = "YES";
        public static string NO = "NO";

        public static int DEFAULT_TYPE = 0;

        public static string TIEU_DE_THU_TIEN_KHACH_HANG = "Thu tiền KH (7) = (3)+(4)-(5)+(6)";
        public static string TIEU_DE_TRA_TIEN_KHACH_HANG = "Trả tiền KH (7) = (3)+(4)-(5)+(6)";
    }
}
