﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang.LichSu
{
    public partial class frmLichSuVangDoiVang : DevExpress.XtraEditors.XtraForm
    {
        public frmLichSuVangDoiVang()
        {
            InitializeComponent();
            grvLichSuVangDoiVang.IndicatorWidth = 70;
        }

        private void frmLichSuVangDoiVang_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            grcLichSuVangDoiVang.DataSource = LichSuDoiVangBUS.Instance.GetListLichSuDoiVang();
            grcSanPhamMuaVao.DataSource = null;
            grcSanPhamBanRa.DataSource = null;
        }

        private void btnXoaLichSuVangDoiVang_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_HOA_DON) == DialogResult.Yes)
            {
                LichSuDoiVang lichSuDoiVang = (LichSuDoiVang)grvLichSuVangDoiVang.GetFocusedRow();

                if (LichSuDoiVangBUS.Instance.XoaHoaDon(lichSuDoiVang))
                {
                    LoadData();
                    PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.DELETE_ERROR);
                }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void grcLichSuVangDoiVang_Click(object sender, EventArgs e)
        {
            LichSuDoiVang lichSuDoiVang = (LichSuDoiVang)grvLichSuVangDoiVang.GetFocusedRow();

            if (lichSuDoiVang != null)
            {
                grcSanPhamBanRa.DataSource = SanPhamBUS.Instance.GetListSanPhamByLichSuBan(lichSuDoiVang.LichSuBan);
                grcSanPhamMuaVao.DataSource = SanPhamKhoCuBUS.Instance.GetListSanPhamByLichSuMua(lichSuDoiVang.LichSuMua);
            }
        }

        private void grvLichSuVangDoiVang_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }
    }
}