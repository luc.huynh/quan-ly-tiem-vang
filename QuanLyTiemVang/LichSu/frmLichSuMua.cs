﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang
{
    public partial class frmLichSuMua : DevExpress.XtraEditors.XtraForm
    {
        public frmLichSuMua()
        {
            InitializeComponent();
            grvLichSuMua.IndicatorWidth = 70;
        }

        private void frmLichSuMua_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            LichSuMuaBUS.Instance.GetListGridControl(grcLichSuMua);
            grcSanPham.DataSource = null;
        }

        private void grvSanPham_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnXoaSanPham_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.Yes)
            {
                SanPhamKhoCu sanPham = (SanPhamKhoCu)grvSanPham.GetFocusedRow();
                List<SanPhamKhoCu> listSanPhamXoaHoaDon = new List<SanPhamKhoCu>();
                LichSuMua lichSuMua = (LichSuMua)grvLichSuMua.GetFocusedRow();

                listSanPhamXoaHoaDon.Add(sanPham);

                if (SanPhamKhoCuBUS.Instance.XoaSanPhamTheoLichSuMua(listSanPhamXoaHoaDon))
                {
                    grcSanPham.DataSource = SanPhamKhoCuBUS.Instance.GetListSanPhamByLichSuMua(lichSuMua);
                    PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.DELETE_ERROR);
                }
            }
        }

        private void btnXoaHoaDon_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_HOA_DON) == DialogResult.Yes)
            {
                LichSuMua lichSuMua = (LichSuMua)grvLichSuMua.GetFocusedRow();

                if (LichSuMuaBUS.Instance.XoaHoaDon(lichSuMua))
                {
                    LoadData();
                    PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.DELETE_ERROR);
                }
            }
        }

        private void grcLichSuMua_Click(object sender, EventArgs e)
        {
            LichSuMua lichSuMua = (LichSuMua)grvLichSuMua.GetFocusedRow();

            if (lichSuMua != null)
            {
                grcSanPham.DataSource = SanPhamKhoCuBUS.Instance.GetListSanPhamByLichSuMua(lichSuMua);
            }
        }
    }
}