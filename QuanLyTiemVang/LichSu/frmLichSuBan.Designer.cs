﻿namespace QuanLyTiemVang
{
    partial class frmLichSuBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gctLichSuBan = new DevExpress.XtraGrid.GridControl();
            this.grvLichSuBan = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclMaLichSuBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTongTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNgayGiaoDich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTienBot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclThanhTienHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaHoaDon = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnXoaLichSu = new DevExpress.XtraBars.BarButtonItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcSanPhamHoaDon = new DevExpress.XtraGrid.GridControl();
            this.grvSanPhamHoaDon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclMaTem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTiLeHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTienCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKiHieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNhaCungCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclMaChuyenKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaSanPhamHoaDon = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gctLichSuBan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuBan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaSanPhamHoaDon)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gctLichSuBan);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1217, 327);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Danh sách hóa đơn bán hàng";
            // 
            // gctLichSuBan
            // 
            this.gctLichSuBan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gctLichSuBan.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gctLichSuBan.Location = new System.Drawing.Point(2, 25);
            this.gctLichSuBan.MainView = this.grvLichSuBan;
            this.gctLichSuBan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gctLichSuBan.Name = "gctLichSuBan";
            this.gctLichSuBan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaHoaDon});
            this.gctLichSuBan.Size = new System.Drawing.Size(1213, 300);
            this.gctLichSuBan.TabIndex = 25;
            this.gctLichSuBan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichSuBan});
            this.gctLichSuBan.Click += new System.EventHandler(this.gctLichSuBan_Click);
            // 
            // grvLichSuBan
            // 
            this.grvLichSuBan.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclMaLichSuBan,
            this.grclTongTien,
            this.grclTenNhanVien,
            this.grclTenKhachHang,
            this.grclNgayGiaoDich,
            this.grclTienBot,
            this.grclThanhTienHoaDon,
            this.gridColumn2});
            this.grvLichSuBan.CustomizationFormBounds = new System.Drawing.Rectangle(628, 255, 210, 172);
            this.grvLichSuBan.GridControl = this.gctLichSuBan;
            this.grvLichSuBan.Name = "grvLichSuBan";
            this.grvLichSuBan.OptionsBehavior.ReadOnly = true;
            this.grvLichSuBan.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvLichSuBan.OptionsFind.AlwaysVisible = true;
            this.grvLichSuBan.OptionsFind.FindDelay = 100;
            this.grvLichSuBan.OptionsFind.FindNullPrompt = "";
            this.grvLichSuBan.OptionsFind.ShowFindButton = false;
            this.grvLichSuBan.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.grvLichSuBan.OptionsView.ShowGroupPanel = false;
            this.grvLichSuBan.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvLichSuBan_CustomDrawRowIndicator);
            // 
            // grclMaLichSuBan
            // 
            this.grclMaLichSuBan.Caption = "Mã lịch sử bán";
            this.grclMaLichSuBan.FieldName = "MaLichSuBan";
            this.grclMaLichSuBan.Name = "grclMaLichSuBan";
            this.grclMaLichSuBan.Visible = true;
            this.grclMaLichSuBan.VisibleIndex = 0;
            // 
            // grclTongTien
            // 
            this.grclTongTien.Caption = "Tổng tiền vàng";
            this.grclTongTien.DisplayFormat.FormatString = "n0";
            this.grclTongTien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclTongTien.FieldName = "TongTienVang";
            this.grclTongTien.Name = "grclTongTien";
            this.grclTongTien.Visible = true;
            this.grclTongTien.VisibleIndex = 2;
            // 
            // grclTenNhanVien
            // 
            this.grclTenNhanVien.Caption = "Tên nhân viên";
            this.grclTenNhanVien.FieldName = "NhanVien.Ten";
            this.grclTenNhanVien.Name = "grclTenNhanVien";
            this.grclTenNhanVien.Visible = true;
            this.grclTenNhanVien.VisibleIndex = 5;
            // 
            // grclTenKhachHang
            // 
            this.grclTenKhachHang.Caption = "Tên khách hàng";
            this.grclTenKhachHang.FieldName = "KhachHang.Ten";
            this.grclTenKhachHang.Name = "grclTenKhachHang";
            this.grclTenKhachHang.Visible = true;
            this.grclTenKhachHang.VisibleIndex = 6;
            // 
            // grclNgayGiaoDich
            // 
            this.grclNgayGiaoDich.Caption = "Ngày giao dịch";
            this.grclNgayGiaoDich.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.grclNgayGiaoDich.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.grclNgayGiaoDich.FieldName = "NgayGiaoDich";
            this.grclNgayGiaoDich.Name = "grclNgayGiaoDich";
            this.grclNgayGiaoDich.Visible = true;
            this.grclNgayGiaoDich.VisibleIndex = 1;
            // 
            // grclTienBot
            // 
            this.grclTienBot.Caption = "Tiền bớt";
            this.grclTienBot.DisplayFormat.FormatString = "n0";
            this.grclTienBot.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclTienBot.FieldName = "TienBot";
            this.grclTienBot.Name = "grclTienBot";
            this.grclTienBot.Visible = true;
            this.grclTienBot.VisibleIndex = 3;
            // 
            // grclThanhTienHoaDon
            // 
            this.grclThanhTienHoaDon.Caption = "Thành tiền";
            this.grclThanhTienHoaDon.DisplayFormat.FormatString = "n0";
            this.grclThanhTienHoaDon.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclThanhTienHoaDon.FieldName = "ThanhTienHoaDon";
            this.grclThanhTienHoaDon.Name = "grclThanhTienHoaDon";
            this.grclThanhTienHoaDon.Visible = true;
            this.grclThanhTienHoaDon.VisibleIndex = 4;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Xóa hóa đơn";
            this.gridColumn2.ColumnEdit = this.btnXoaHoaDon;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 7;
            // 
            // btnXoaHoaDon
            // 
            this.btnXoaHoaDon.AutoHeight = false;
            this.btnXoaHoaDon.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnXoaHoaDon.Name = "btnXoaHoaDon";
            this.btnXoaHoaDon.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaHoaDon.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnXoaHoaDon_ButtonClick);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.btnXoaLichSu});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(398, 185);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem1, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Làm mới dữ liệu";
            this.barButtonItem1.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1217, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 658);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1217, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 608);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1217, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 608);
            // 
            // btnXoaLichSu
            // 
            this.btnXoaLichSu.Id = 2;
            this.btnXoaLichSu.Name = "btnXoaLichSu";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 377);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1217, 6);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcSanPhamHoaDon);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 383);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1217, 275);
            this.groupControl2.TabIndex = 7;
            this.groupControl2.Text = "Danh sách sản phẩm thuộc hóa đơn bán";
            // 
            // grcSanPhamHoaDon
            // 
            this.grcSanPhamHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcSanPhamHoaDon.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamHoaDon.Location = new System.Drawing.Point(2, 25);
            this.grcSanPhamHoaDon.MainView = this.grvSanPhamHoaDon;
            this.grcSanPhamHoaDon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamHoaDon.MenuManager = this.barManager1;
            this.grcSanPhamHoaDon.Name = "grcSanPhamHoaDon";
            this.grcSanPhamHoaDon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaSanPhamHoaDon});
            this.grcSanPhamHoaDon.Size = new System.Drawing.Size(1213, 248);
            this.grcSanPhamHoaDon.TabIndex = 0;
            this.grcSanPhamHoaDon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamHoaDon});
            // 
            // grvSanPhamHoaDon
            // 
            this.grvSanPhamHoaDon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclMaTem,
            this.grclTenSanPham,
            this.grclHamLuongVang,
            this.grclKhoiLuongTinh,
            this.grclKhoiLuongDa,
            this.grclKhoiLuongVang,
            this.grclTiLeHot,
            this.grclTienCong,
            this.grclGiaBan,
            this.grclKiHieu,
            this.grclNhaCungCap,
            this.grclMaChuyenKho,
            this.gridColumn1});
            this.grvSanPhamHoaDon.GridControl = this.grcSanPhamHoaDon;
            this.grvSanPhamHoaDon.Name = "grvSanPhamHoaDon";
            this.grvSanPhamHoaDon.OptionsBehavior.ReadOnly = true;
            this.grvSanPhamHoaDon.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamHoaDon.OptionsView.ShowGroupPanel = false;
            // 
            // grclMaTem
            // 
            this.grclMaTem.Caption = "Mã tem";
            this.grclMaTem.FieldName = "MaTem";
            this.grclMaTem.Name = "grclMaTem";
            this.grclMaTem.Visible = true;
            this.grclMaTem.VisibleIndex = 0;
            // 
            // grclTenSanPham
            // 
            this.grclTenSanPham.Caption = "Tên sản phẩm";
            this.grclTenSanPham.FieldName = "TenSanPham";
            this.grclTenSanPham.Name = "grclTenSanPham";
            this.grclTenSanPham.Visible = true;
            this.grclTenSanPham.VisibleIndex = 1;
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "HLV";
            this.grclHamLuongVang.FieldName = "HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 2;
            // 
            // grclKhoiLuongTinh
            // 
            this.grclKhoiLuongTinh.Caption = "KL Tổng";
            this.grclKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.grclKhoiLuongTinh.Name = "grclKhoiLuongTinh";
            this.grclKhoiLuongTinh.Visible = true;
            this.grclKhoiLuongTinh.VisibleIndex = 3;
            // 
            // grclKhoiLuongDa
            // 
            this.grclKhoiLuongDa.Caption = "KL Đá";
            this.grclKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.grclKhoiLuongDa.Name = "grclKhoiLuongDa";
            this.grclKhoiLuongDa.Visible = true;
            this.grclKhoiLuongDa.VisibleIndex = 4;
            // 
            // grclKhoiLuongVang
            // 
            this.grclKhoiLuongVang.Caption = "KL Vàng";
            this.grclKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.grclKhoiLuongVang.Name = "grclKhoiLuongVang";
            this.grclKhoiLuongVang.Visible = true;
            this.grclKhoiLuongVang.VisibleIndex = 5;
            // 
            // grclTiLeHot
            // 
            this.grclTiLeHot.Caption = "Tỉ lệ hột";
            this.grclTiLeHot.FieldName = "TiLeHot";
            this.grclTiLeHot.Name = "grclTiLeHot";
            this.grclTiLeHot.Visible = true;
            this.grclTiLeHot.VisibleIndex = 6;
            // 
            // grclTienCong
            // 
            this.grclTienCong.Caption = "Tiền công";
            this.grclTienCong.DisplayFormat.FormatString = "n0";
            this.grclTienCong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclTienCong.FieldName = "TienCong";
            this.grclTienCong.Name = "grclTienCong";
            this.grclTienCong.Visible = true;
            this.grclTienCong.VisibleIndex = 7;
            // 
            // grclGiaBan
            // 
            this.grclGiaBan.Caption = "Giá bán";
            this.grclGiaBan.DisplayFormat.FormatString = "n0";
            this.grclGiaBan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaBan.FieldName = "GiaBan";
            this.grclGiaBan.Name = "grclGiaBan";
            this.grclGiaBan.Visible = true;
            this.grclGiaBan.VisibleIndex = 8;
            // 
            // grclKiHieu
            // 
            this.grclKiHieu.Caption = "Kí hiệu";
            this.grclKiHieu.FieldName = "KiHieu";
            this.grclKiHieu.Name = "grclKiHieu";
            this.grclKiHieu.Visible = true;
            this.grclKiHieu.VisibleIndex = 9;
            // 
            // grclNhaCungCap
            // 
            this.grclNhaCungCap.Caption = "Nhà cung cấp";
            this.grclNhaCungCap.FieldName = "NhaCungCap.TenNhaCungCap";
            this.grclNhaCungCap.Name = "grclNhaCungCap";
            this.grclNhaCungCap.Visible = true;
            this.grclNhaCungCap.VisibleIndex = 10;
            // 
            // grclMaChuyenKho
            // 
            this.grclMaChuyenKho.Caption = "Mã chuyển kho";
            this.grclMaChuyenKho.FieldName = "MaChuyenKho";
            this.grclMaChuyenKho.Name = "grclMaChuyenKho";
            this.grclMaChuyenKho.Visible = true;
            this.grclMaChuyenKho.VisibleIndex = 11;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Xóa";
            this.gridColumn1.ColumnEdit = this.btnXoaSanPhamHoaDon;
            this.gridColumn1.Name = "gridColumn1";
            // 
            // btnXoaSanPhamHoaDon
            // 
            this.btnXoaSanPhamHoaDon.AutoHeight = false;
            this.btnXoaSanPhamHoaDon.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnXoaSanPhamHoaDon.Name = "btnXoaSanPhamHoaDon";
            this.btnXoaSanPhamHoaDon.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaSanPhamHoaDon.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnXoaSanPhamHoaDon_ButtonClick);
            // 
            // frmLichSuBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1217, 658);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmLichSuBan";
            this.Text = "Hóa đơn bán hàng";
            this.Load += new System.EventHandler(this.frmLichSuBan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gctLichSuBan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuBan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaSanPhamHoaDon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gctLichSuBan;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichSuBan;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaLichSuBan;
        private DevExpress.XtraGrid.Columns.GridColumn grclTongTien;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn grclNgayGiaoDich;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn grclTienBot;
        private DevExpress.XtraBars.BarButtonItem btnXoaLichSu;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcSanPhamHoaDon;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamHoaDon;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraGrid.Columns.GridColumn grclThanhTienHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaTem;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclTiLeHot;
        private DevExpress.XtraGrid.Columns.GridColumn grclTienCong;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaBan;
        private DevExpress.XtraGrid.Columns.GridColumn grclKiHieu;
        private DevExpress.XtraGrid.Columns.GridColumn grclNhaCungCap;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaChuyenKho;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaSanPhamHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaHoaDon;
    }
}