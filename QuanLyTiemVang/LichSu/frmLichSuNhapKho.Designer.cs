﻿namespace QuanLyTiemVang
{
    partial class frmLichSuNhapKho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gctNhapKho = new DevExpress.XtraGrid.GridControl();
            this.grvNhapKho = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclMaNhapKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNhapVaoKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNgayNhap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rBtnChuyenKho = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gctNhapKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvNhapKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rBtnChuyenKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem1, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Làm mới dữ liệu";
            this.barButtonItem1.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(859, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 587);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(859, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 537);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(859, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 537);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 50);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(859, 537);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gctNhapKho);
            this.groupControl1.Location = new System.Drawing.Point(16, 16);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(827, 505);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Danh sách nhập kho";
            // 
            // gctNhapKho
            // 
            this.gctNhapKho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gctNhapKho.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gctNhapKho.Location = new System.Drawing.Point(2, 25);
            this.gctNhapKho.MainView = this.grvNhapKho;
            this.gctNhapKho.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gctNhapKho.Name = "gctNhapKho";
            this.gctNhapKho.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rBtnChuyenKho});
            this.gctNhapKho.Size = new System.Drawing.Size(823, 478);
            this.gctNhapKho.TabIndex = 25;
            this.gctNhapKho.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvNhapKho});
            // 
            // grvNhapKho
            // 
            this.grvNhapKho.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclMaNhapKho,
            this.grclTenNhanVien,
            this.grclNhapVaoKho,
            this.grclNgayNhap});
            this.grvNhapKho.CustomizationFormBounds = new System.Drawing.Rectangle(628, 255, 210, 172);
            this.grvNhapKho.GridControl = this.gctNhapKho;
            this.grvNhapKho.Name = "grvNhapKho";
            this.grvNhapKho.OptionsBehavior.Editable = false;
            this.grvNhapKho.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvNhapKho.OptionsFind.AlwaysVisible = true;
            this.grvNhapKho.OptionsFind.FindDelay = 100;
            this.grvNhapKho.OptionsFind.FindNullPrompt = "";
            this.grvNhapKho.OptionsFind.ShowFindButton = false;
            this.grvNhapKho.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.grvNhapKho.OptionsView.ShowGroupPanel = false;
            // 
            // grclMaNhapKho
            // 
            this.grclMaNhapKho.Caption = "Mã nhập kho";
            this.grclMaNhapKho.FieldName = "MaNhapKho";
            this.grclMaNhapKho.Name = "grclMaNhapKho";
            this.grclMaNhapKho.Visible = true;
            this.grclMaNhapKho.VisibleIndex = 0;
            // 
            // grclTenNhanVien
            // 
            this.grclTenNhanVien.Caption = "Tên nhân viên";
            this.grclTenNhanVien.FieldName = "NhanVien.Ten";
            this.grclTenNhanVien.Name = "grclTenNhanVien";
            this.grclTenNhanVien.Visible = true;
            this.grclTenNhanVien.VisibleIndex = 1;
            // 
            // grclNhapVaoKho
            // 
            this.grclNhapVaoKho.Caption = "Nhập vào kho";
            this.grclNhapVaoKho.FieldName = "NhapKhoMoiHoacKhoCu";
            this.grclNhapVaoKho.Name = "grclNhapVaoKho";
            this.grclNhapVaoKho.Visible = true;
            this.grclNhapVaoKho.VisibleIndex = 2;
            // 
            // grclNgayNhap
            // 
            this.grclNgayNhap.Caption = "Ngày nhập";
            this.grclNgayNhap.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.grclNgayNhap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.grclNgayNhap.FieldName = "NgayNhap";
            this.grclNgayNhap.Name = "grclNgayNhap";
            this.grclNgayNhap.Visible = true;
            this.grclNgayNhap.VisibleIndex = 3;
            // 
            // rBtnChuyenKho
            // 
            this.rBtnChuyenKho.AutoHeight = false;
            this.rBtnChuyenKho.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.Extract_todays_changes_icon, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.rBtnChuyenKho.Name = "rBtnChuyenKho";
            this.rBtnChuyenKho.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(859, 537);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(833, 511);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // frmLichSuNhapKho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 587);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmLichSuNhapKho";
            this.Text = "Lịch sử nhập kho";
            this.Load += new System.EventHandler(this.frmLichSuNhapKho_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gctNhapKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvNhapKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rBtnChuyenKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gctNhapKho;
        private DevExpress.XtraGrid.Views.Grid.GridView grvNhapKho;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaNhapKho;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn grclNhapVaoKho;
        private DevExpress.XtraGrid.Columns.GridColumn grclNgayNhap;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit rBtnChuyenKho;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}