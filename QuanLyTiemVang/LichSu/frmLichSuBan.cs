﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang
{
    public partial class frmLichSuBan : DevExpress.XtraEditors.XtraForm
    {
        public frmLichSuBan()
        {
            InitializeComponent();
            grvLichSuBan.IndicatorWidth = 70;
        }

        private void frmLichSuBan_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            LichSuBanBUS.Instance.GetListGridControl(gctLichSuBan);
            grcSanPhamHoaDon.DataSource = null;
        }

        private void grvLichSuBan_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnXoaSanPhamHoaDon_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.Yes)
            {
                SanPham sanPham = (SanPham)grvSanPhamHoaDon.GetFocusedRow();
                List<SanPham> listSanPhamXoaHoaDon = new List<SanPham>();
                LichSuBan lichSuBan = (LichSuBan)grvLichSuBan.GetFocusedRow();

                listSanPhamXoaHoaDon.Add(sanPham);

                if (SanPhamBUS.Instance.XoaMaLichSuBan(listSanPhamXoaHoaDon))
                {
                    grcSanPhamHoaDon.DataSource = SanPhamBUS.Instance.GetListSanPhamByLichSuBan(lichSuBan);
                    PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.DELETE_ERROR);
                }
            }
        }

        private void btnXoaHoaDon_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_HOA_DON) == DialogResult.Yes)
            {
                LichSuBan lichSuBan = (LichSuBan)grvLichSuBan.GetFocusedRow();

                if (LichSuBanBUS.Instance.XoaHoaDon(lichSuBan))
                {
                    LoadData();
                    PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.DELETE_ERROR);
                }
            }
        }

        private void gctLichSuBan_Click(object sender, EventArgs e)
        {
            LichSuBan lichSuBan = (LichSuBan)grvLichSuBan.GetFocusedRow();

            if (lichSuBan != null)
            {
                grcSanPhamHoaDon.DataSource = SanPhamBUS.Instance.GetListSanPhamByLichSuBan(lichSuBan);
            }
        }
    }
}