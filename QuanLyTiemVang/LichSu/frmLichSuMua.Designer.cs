﻿namespace QuanLyTiemVang
{
    partial class frmLichSuMua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.grcLichSuMua = new DevExpress.XtraGrid.GridControl();
            this.lichSuMuaBindingSource = new System.Windows.Forms.BindingSource();
            this.grvLichSuMua = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaLichSuMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayGiaoDich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienCongThem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThanhTienHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXoaHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaHoaDon = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcSanPham = new DevExpress.XtraGrid.GridControl();
            this.sanPhamKhoCuBindingSource = new System.Windows.Forms.BindingSource();
            this.grvSanPham = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTiLeHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDonGiaMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXoaSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaSanPham = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuMuaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamKhoCuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaSanPham)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.grcLichSuMua);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1274, 334);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Danh sách hóa đơn mua hàng";
            // 
            // grcLichSuMua
            // 
            this.grcLichSuMua.DataSource = this.lichSuMuaBindingSource;
            this.grcLichSuMua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcLichSuMua.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcLichSuMua.Location = new System.Drawing.Point(2, 25);
            this.grcLichSuMua.MainView = this.grvLichSuMua;
            this.grcLichSuMua.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcLichSuMua.Name = "grcLichSuMua";
            this.grcLichSuMua.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaHoaDon});
            this.grcLichSuMua.Size = new System.Drawing.Size(1270, 307);
            this.grcLichSuMua.TabIndex = 25;
            this.grcLichSuMua.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichSuMua});
            this.grcLichSuMua.Click += new System.EventHandler(this.grcLichSuMua_Click);
            // 
            // lichSuMuaBindingSource
            // 
            this.lichSuMuaBindingSource.DataSource = typeof(DTO.Models.LichSuMua);
            // 
            // grvLichSuMua
            // 
            this.grvLichSuMua.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaLichSuMua,
            this.colNgayGiaoDich,
            this.colTongTienVang,
            this.colTienCongThem,
            this.colThanhTienHoaDon,
            this.colKhachHang,
            this.colNhanVien,
            this.colXoaHoaDon});
            this.grvLichSuMua.CustomizationFormBounds = new System.Drawing.Rectangle(628, 255, 210, 172);
            this.grvLichSuMua.GridControl = this.grcLichSuMua;
            this.grvLichSuMua.Name = "grvLichSuMua";
            this.grvLichSuMua.OptionsBehavior.ReadOnly = true;
            this.grvLichSuMua.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvLichSuMua.OptionsFind.AlwaysVisible = true;
            this.grvLichSuMua.OptionsFind.FindDelay = 100;
            this.grvLichSuMua.OptionsFind.FindNullPrompt = "";
            this.grvLichSuMua.OptionsFind.ShowFindButton = false;
            this.grvLichSuMua.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.grvLichSuMua.OptionsView.ShowGroupPanel = false;
            this.grvLichSuMua.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvSanPham_CustomDrawRowIndicator);
            // 
            // colMaLichSuMua
            // 
            this.colMaLichSuMua.Caption = "Mã hóa đơn";
            this.colMaLichSuMua.FieldName = "MaLichSuMua";
            this.colMaLichSuMua.Name = "colMaLichSuMua";
            this.colMaLichSuMua.Visible = true;
            this.colMaLichSuMua.VisibleIndex = 0;
            // 
            // colNgayGiaoDich
            // 
            this.colNgayGiaoDich.Caption = "Ngày giao dịch";
            this.colNgayGiaoDich.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayGiaoDich.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayGiaoDich.FieldName = "NgayGiaoDich";
            this.colNgayGiaoDich.Name = "colNgayGiaoDich";
            this.colNgayGiaoDich.Visible = true;
            this.colNgayGiaoDich.VisibleIndex = 1;
            // 
            // colTongTienVang
            // 
            this.colTongTienVang.Caption = "Tổng tiền vàng";
            this.colTongTienVang.DisplayFormat.FormatString = "n0";
            this.colTongTienVang.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienVang.FieldName = "TongTienVang";
            this.colTongTienVang.Name = "colTongTienVang";
            this.colTongTienVang.Visible = true;
            this.colTongTienVang.VisibleIndex = 2;
            // 
            // colTienCongThem
            // 
            this.colTienCongThem.Caption = "Tiền cộng thêm";
            this.colTienCongThem.DisplayFormat.FormatString = "n0";
            this.colTienCongThem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTienCongThem.FieldName = "TienCongThem";
            this.colTienCongThem.Name = "colTienCongThem";
            this.colTienCongThem.Visible = true;
            this.colTienCongThem.VisibleIndex = 3;
            // 
            // colThanhTienHoaDon
            // 
            this.colThanhTienHoaDon.Caption = "Thành tiền";
            this.colThanhTienHoaDon.DisplayFormat.FormatString = "n0";
            this.colThanhTienHoaDon.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colThanhTienHoaDon.FieldName = "ThanhTienHoaDon";
            this.colThanhTienHoaDon.Name = "colThanhTienHoaDon";
            this.colThanhTienHoaDon.Visible = true;
            this.colThanhTienHoaDon.VisibleIndex = 4;
            // 
            // colKhachHang
            // 
            this.colKhachHang.Caption = "Tên khách hàng";
            this.colKhachHang.FieldName = "KhachHang.Ten";
            this.colKhachHang.Name = "colKhachHang";
            this.colKhachHang.Visible = true;
            this.colKhachHang.VisibleIndex = 5;
            // 
            // colNhanVien
            // 
            this.colNhanVien.Caption = "Tên nhân viên";
            this.colNhanVien.FieldName = "NhanVien.Ten";
            this.colNhanVien.Name = "colNhanVien";
            this.colNhanVien.Visible = true;
            this.colNhanVien.VisibleIndex = 6;
            // 
            // colXoaHoaDon
            // 
            this.colXoaHoaDon.Caption = "Xóa";
            this.colXoaHoaDon.ColumnEdit = this.btnXoaHoaDon;
            this.colXoaHoaDon.Name = "colXoaHoaDon";
            this.colXoaHoaDon.Visible = true;
            this.colXoaHoaDon.VisibleIndex = 7;
            // 
            // btnXoaHoaDon
            // 
            this.btnXoaHoaDon.AutoHeight = false;
            this.btnXoaHoaDon.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnXoaHoaDon.Name = "btnXoaHoaDon";
            this.btnXoaHoaDon.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaHoaDon.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnXoaHoaDon_ButtonClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Làm mới dữ liệu";
            this.barButtonItem1.Glyph = global::QuanLyTiemVang.Properties.Resources.Button_Refresh_icon;
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1274, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 604);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Làm mới dữ liệu";
            this.barButtonItem2.Glyph = global::QuanLyTiemVang.Properties.Resources.Button_Refresh_icon;
            this.barButtonItem2.Id = 0;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(1274, 50);
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl1.Size = new System.Drawing.Size(0, 604);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLamMoiDuLieu,
            this.barButtonItem3});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 0;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1274, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 654);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1274, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 604);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1274, 50);
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl2.Size = new System.Drawing.Size(0, 604);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Xóa lịch sử";
            this.barButtonItem3.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.barButtonItem3.Id = 1;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 384);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1274, 6);
            this.splitterControl1.TabIndex = 7;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcSanPham);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 390);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1274, 264);
            this.groupControl2.TabIndex = 8;
            this.groupControl2.Text = "Danh sách sản phẩm hóa đơn";
            // 
            // grcSanPham
            // 
            this.grcSanPham.DataSource = this.sanPhamKhoCuBindingSource;
            this.grcSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcSanPham.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPham.Location = new System.Drawing.Point(2, 25);
            this.grcSanPham.MainView = this.grvSanPham;
            this.grcSanPham.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPham.MenuManager = this.barManager1;
            this.grcSanPham.Name = "grcSanPham";
            this.grcSanPham.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaSanPham});
            this.grcSanPham.Size = new System.Drawing.Size(1270, 237);
            this.grcSanPham.TabIndex = 0;
            this.grcSanPham.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPham});
            // 
            // sanPhamKhoCuBindingSource
            // 
            this.sanPhamKhoCuBindingSource.DataSource = typeof(DTO.Models.SanPhamKhoCu);
            // 
            // grvSanPham
            // 
            this.grvSanPham.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenSanPham,
            this.colKiHieuHamLuongVang,
            this.colKhoiLuongTinh,
            this.colKhoiLuongDa,
            this.colKhoiLuongVang,
            this.colTiLeHot,
            this.colDonGiaMuaVao,
            this.colGiaMuaVao,
            this.colXoaSanPham});
            this.grvSanPham.GridControl = this.grcSanPham;
            this.grvSanPham.Name = "grvSanPham";
            this.grvSanPham.OptionsBehavior.ReadOnly = true;
            this.grvSanPham.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPham.OptionsView.ShowGroupPanel = false;
            // 
            // colTenSanPham
            // 
            this.colTenSanPham.Caption = "Tên sản phẩm";
            this.colTenSanPham.FieldName = "TenSanPham";
            this.colTenSanPham.Name = "colTenSanPham";
            this.colTenSanPham.Visible = true;
            this.colTenSanPham.VisibleIndex = 0;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "HLV";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 3;
            // 
            // colKhoiLuongTinh
            // 
            this.colKhoiLuongTinh.Caption = "KL Tổng";
            this.colKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.colKhoiLuongTinh.Name = "colKhoiLuongTinh";
            this.colKhoiLuongTinh.Visible = true;
            this.colKhoiLuongTinh.VisibleIndex = 4;
            // 
            // colKhoiLuongDa
            // 
            this.colKhoiLuongDa.Caption = "KL Đá";
            this.colKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.colKhoiLuongDa.Name = "colKhoiLuongDa";
            this.colKhoiLuongDa.Visible = true;
            this.colKhoiLuongDa.VisibleIndex = 5;
            // 
            // colKhoiLuongVang
            // 
            this.colKhoiLuongVang.Caption = "KL Vàng";
            this.colKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.colKhoiLuongVang.Name = "colKhoiLuongVang";
            this.colKhoiLuongVang.Visible = true;
            this.colKhoiLuongVang.VisibleIndex = 6;
            // 
            // colTiLeHot
            // 
            this.colTiLeHot.Caption = "Tỉ lệ hột";
            this.colTiLeHot.FieldName = "TiLeHot";
            this.colTiLeHot.Name = "colTiLeHot";
            this.colTiLeHot.Visible = true;
            this.colTiLeHot.VisibleIndex = 7;
            // 
            // colDonGiaMuaVao
            // 
            this.colDonGiaMuaVao.Caption = "Đơn giá mua";
            this.colDonGiaMuaVao.DisplayFormat.FormatString = "n0";
            this.colDonGiaMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDonGiaMuaVao.FieldName = "DonGiaMuaVao";
            this.colDonGiaMuaVao.Name = "colDonGiaMuaVao";
            this.colDonGiaMuaVao.Visible = true;
            this.colDonGiaMuaVao.VisibleIndex = 1;
            // 
            // colGiaMuaVao
            // 
            this.colGiaMuaVao.Caption = "Thành tiền";
            this.colGiaMuaVao.DisplayFormat.FormatString = "n0";
            this.colGiaMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGiaMuaVao.FieldName = "GiaMuaVao";
            this.colGiaMuaVao.Name = "colGiaMuaVao";
            this.colGiaMuaVao.Visible = true;
            this.colGiaMuaVao.VisibleIndex = 2;
            // 
            // colXoaSanPham
            // 
            this.colXoaSanPham.Caption = "Xóa";
            this.colXoaSanPham.ColumnEdit = this.btnXoaSanPham;
            this.colXoaSanPham.Name = "colXoaSanPham";
            // 
            // btnXoaSanPham
            // 
            this.btnXoaSanPham.AutoHeight = false;
            this.btnXoaSanPham.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnXoaSanPham.Name = "btnXoaSanPham";
            this.btnXoaSanPham.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaSanPham.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnXoaSanPham_ButtonClick);
            // 
            // frmLichSuMua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1274, 654);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmLichSuMua";
            this.Text = "Hóa đơn mua vàng";
            this.Load += new System.EventHandler(this.frmLichSuMua_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuMuaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamKhoCuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaSanPham)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl grcLichSuMua;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichSuMua;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcSanPham;
        private System.Windows.Forms.BindingSource sanPhamKhoCuBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn colTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colTiLeHot;
        private DevExpress.XtraGrid.Columns.GridColumn colDonGiaMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colXoaSanPham;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaSanPham;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private System.Windows.Forms.BindingSource lichSuMuaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMaLichSuMua;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayGiaoDich;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienVang;
        private DevExpress.XtraGrid.Columns.GridColumn colTienCongThem;
        private DevExpress.XtraGrid.Columns.GridColumn colThanhTienHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn colKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn colXoaHoaDon;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaHoaDon;
    }
}