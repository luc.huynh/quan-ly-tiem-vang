﻿namespace QuanLyTiemVang.LichSu
{
    partial class frmLichSuVangDoiVang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.grcLichSuVangDoiVang = new DevExpress.XtraGrid.GridControl();
            this.lichSuDoiVangBindingSource = new System.Windows.Forms.BindingSource();
            this.grvLichSuVangDoiVang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaLichSuBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangMoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangCu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKLVMoiTruKLVCu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThanhTienVangMoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienVangCu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienConLai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienCongThem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienBot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThuTienKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayGiaoDich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXoaHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaLichSuVangDoiVang = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcSanPhamBanRa = new DevExpress.XtraGrid.GridControl();
            this.sanPhamBindingSource = new System.Windows.Forms.BindingSource();
            this.grvSanPhamBanRa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaTem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenSanPham1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTinh1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongDa1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVang1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sanPhamKhoCuBindingSource = new System.Windows.Forms.BindingSource();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.grcSanPhamMuaVao = new DevExpress.XtraGrid.GridControl();
            this.sanPhamKhoCuBindingSource1 = new System.Windows.Forms.BindingSource();
            this.grvSanPhamMuaVao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLoaiVangMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDonGiaMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuVangDoiVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuDoiVangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuVangDoiVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaLichSuVangDoiVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamKhoCuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamMuaVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamKhoCuBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamMuaVao)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLamMoiDuLieu,
            this.barButtonItem3});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 0;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1245, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 747);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1245, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 697);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1245, 50);
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl2.Size = new System.Drawing.Size(0, 697);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Xóa lịch sử";
            this.barButtonItem3.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.barButtonItem3.Id = 1;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.grcLichSuVangDoiVang);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1245, 293);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Danh sách hóa đơn vàng đổi vàng";
            // 
            // grcLichSuVangDoiVang
            // 
            this.grcLichSuVangDoiVang.DataSource = this.lichSuDoiVangBindingSource;
            this.grcLichSuVangDoiVang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcLichSuVangDoiVang.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcLichSuVangDoiVang.Location = new System.Drawing.Point(2, 25);
            this.grcLichSuVangDoiVang.MainView = this.grvLichSuVangDoiVang;
            this.grcLichSuVangDoiVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcLichSuVangDoiVang.MenuManager = this.barManager1;
            this.grcLichSuVangDoiVang.Name = "grcLichSuVangDoiVang";
            this.grcLichSuVangDoiVang.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaLichSuVangDoiVang});
            this.grcLichSuVangDoiVang.Size = new System.Drawing.Size(1241, 266);
            this.grcLichSuVangDoiVang.TabIndex = 0;
            this.grcLichSuVangDoiVang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichSuVangDoiVang});
            this.grcLichSuVangDoiVang.Click += new System.EventHandler(this.grcLichSuVangDoiVang_Click);
            // 
            // lichSuDoiVangBindingSource
            // 
            this.lichSuDoiVangBindingSource.DataSource = typeof(DTO.Models.LichSuDoiVang);
            // 
            // grvLichSuVangDoiVang
            // 
            this.grvLichSuVangDoiVang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaLichSuBan,
            this.colKhoiLuongVangMoi,
            this.colKhoiLuongVangCu,
            this.colKLVMoiTruKLVCu,
            this.colThanhTienVangMoi,
            this.colTongTienVangCu,
            this.colTienConLai,
            this.colTienCongThem,
            this.colTienBot,
            this.colThuTienKhachHang,
            this.colNgayGiaoDich,
            this.colXoaHoaDon});
            this.grvLichSuVangDoiVang.GridControl = this.grcLichSuVangDoiVang;
            this.grvLichSuVangDoiVang.Name = "grvLichSuVangDoiVang";
            this.grvLichSuVangDoiVang.OptionsBehavior.ReadOnly = true;
            this.grvLichSuVangDoiVang.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvLichSuVangDoiVang.OptionsFind.AlwaysVisible = true;
            this.grvLichSuVangDoiVang.OptionsFind.FindDelay = 100;
            this.grvLichSuVangDoiVang.OptionsFind.FindNullPrompt = "";
            this.grvLichSuVangDoiVang.OptionsFind.ShowFindButton = false;
            this.grvLichSuVangDoiVang.OptionsView.ShowGroupPanel = false;
            this.grvLichSuVangDoiVang.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvLichSuVangDoiVang_CustomDrawRowIndicator);
            // 
            // colMaLichSuBan
            // 
            this.colMaLichSuBan.Caption = "Mã hóa đơn";
            this.colMaLichSuBan.FieldName = "MaLichSuBan";
            this.colMaLichSuBan.Name = "colMaLichSuBan";
            this.colMaLichSuBan.Visible = true;
            this.colMaLichSuBan.VisibleIndex = 0;
            this.colMaLichSuBan.Width = 101;
            // 
            // colKhoiLuongVangMoi
            // 
            this.colKhoiLuongVangMoi.Caption = "KLV mới";
            this.colKhoiLuongVangMoi.FieldName = "KhoiLuongVangMoi";
            this.colKhoiLuongVangMoi.Name = "colKhoiLuongVangMoi";
            this.colKhoiLuongVangMoi.Visible = true;
            this.colKhoiLuongVangMoi.VisibleIndex = 2;
            this.colKhoiLuongVangMoi.Width = 88;
            // 
            // colKhoiLuongVangCu
            // 
            this.colKhoiLuongVangCu.Caption = "KLV cũ";
            this.colKhoiLuongVangCu.FieldName = "KhoiLuongVangCu";
            this.colKhoiLuongVangCu.Name = "colKhoiLuongVangCu";
            this.colKhoiLuongVangCu.Visible = true;
            this.colKhoiLuongVangCu.VisibleIndex = 3;
            this.colKhoiLuongVangCu.Width = 83;
            // 
            // colKLVMoiTruKLVCu
            // 
            this.colKLVMoiTruKLVCu.Caption = "KLV mới - KLV cũ";
            this.colKLVMoiTruKLVCu.FieldName = "KLVMoiTruKLVCu";
            this.colKLVMoiTruKLVCu.Name = "colKLVMoiTruKLVCu";
            this.colKLVMoiTruKLVCu.Visible = true;
            this.colKLVMoiTruKLVCu.VisibleIndex = 4;
            this.colKLVMoiTruKLVCu.Width = 104;
            // 
            // colThanhTienVangMoi
            // 
            this.colThanhTienVangMoi.Caption = "Thành tiền (1)";
            this.colThanhTienVangMoi.DisplayFormat.FormatString = "n0";
            this.colThanhTienVangMoi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colThanhTienVangMoi.FieldName = "ThanhTienVangMoi";
            this.colThanhTienVangMoi.Name = "colThanhTienVangMoi";
            this.colThanhTienVangMoi.Visible = true;
            this.colThanhTienVangMoi.VisibleIndex = 5;
            this.colThanhTienVangMoi.Width = 104;
            // 
            // colTongTienVangCu
            // 
            this.colTongTienVangCu.Caption = "Tổng tiền dẻ lạ (2)";
            this.colTongTienVangCu.DisplayFormat.FormatString = "n0";
            this.colTongTienVangCu.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienVangCu.FieldName = "TongTienVangCu";
            this.colTongTienVangCu.Name = "colTongTienVangCu";
            this.colTongTienVangCu.Visible = true;
            this.colTongTienVangCu.VisibleIndex = 6;
            this.colTongTienVangCu.Width = 104;
            // 
            // colTienConLai
            // 
            this.colTienConLai.Caption = "Còn lại (3)";
            this.colTienConLai.DisplayFormat.FormatString = "n0";
            this.colTienConLai.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTienConLai.FieldName = "TienConLai";
            this.colTienConLai.Name = "colTienConLai";
            this.colTienConLai.Visible = true;
            this.colTienConLai.VisibleIndex = 7;
            this.colTienConLai.Width = 104;
            // 
            // colTienCongThem
            // 
            this.colTienCongThem.Caption = "Tiền công (4)";
            this.colTienCongThem.DisplayFormat.FormatString = "n0";
            this.colTienCongThem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTienCongThem.FieldName = "TienCongThem";
            this.colTienCongThem.Name = "colTienCongThem";
            this.colTienCongThem.Visible = true;
            this.colTienCongThem.VisibleIndex = 8;
            this.colTienCongThem.Width = 104;
            // 
            // colTienBot
            // 
            this.colTienBot.Caption = "Tiền bớt (5)";
            this.colTienBot.DisplayFormat.FormatString = "n0";
            this.colTienBot.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTienBot.FieldName = "TienBot";
            this.colTienBot.Name = "colTienBot";
            this.colTienBot.Visible = true;
            this.colTienBot.VisibleIndex = 9;
            this.colTienBot.Width = 104;
            // 
            // colThuTienKhachHang
            // 
            this.colThuTienKhachHang.Caption = "Thu / Trả tiền KH (6)";
            this.colThuTienKhachHang.DisplayFormat.FormatString = "n0";
            this.colThuTienKhachHang.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colThuTienKhachHang.FieldName = "ThuTienKhachHang";
            this.colThuTienKhachHang.Name = "colThuTienKhachHang";
            this.colThuTienKhachHang.Visible = true;
            this.colThuTienKhachHang.VisibleIndex = 10;
            this.colThuTienKhachHang.Width = 139;
            // 
            // colNgayGiaoDich
            // 
            this.colNgayGiaoDich.Caption = "Ngày";
            this.colNgayGiaoDich.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayGiaoDich.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayGiaoDich.FieldName = "NgayGiaoDich";
            this.colNgayGiaoDich.Name = "colNgayGiaoDich";
            this.colNgayGiaoDich.Visible = true;
            this.colNgayGiaoDich.VisibleIndex = 1;
            this.colNgayGiaoDich.Width = 101;
            // 
            // colXoaHoaDon
            // 
            this.colXoaHoaDon.Caption = "Xóa";
            this.colXoaHoaDon.ColumnEdit = this.btnXoaLichSuVangDoiVang;
            this.colXoaHoaDon.Name = "colXoaHoaDon";
            this.colXoaHoaDon.Visible = true;
            this.colXoaHoaDon.VisibleIndex = 11;
            this.colXoaHoaDon.Width = 85;
            // 
            // btnXoaLichSuVangDoiVang
            // 
            this.btnXoaLichSuVangDoiVang.AutoHeight = false;
            this.btnXoaLichSuVangDoiVang.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnXoaLichSuVangDoiVang.Name = "btnXoaLichSuVangDoiVang";
            this.btnXoaLichSuVangDoiVang.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaLichSuVangDoiVang.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnXoaLichSuVangDoiVang_ButtonClick);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcSanPhamBanRa);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 349);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1245, 180);
            this.groupControl2.TabIndex = 7;
            this.groupControl2.Text = "Danh sách sản phẩm bán ra";
            // 
            // grcSanPhamBanRa
            // 
            this.grcSanPhamBanRa.DataSource = this.sanPhamBindingSource;
            this.grcSanPhamBanRa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcSanPhamBanRa.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamBanRa.Location = new System.Drawing.Point(2, 25);
            this.grcSanPhamBanRa.MainView = this.grvSanPhamBanRa;
            this.grcSanPhamBanRa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamBanRa.MenuManager = this.barManager1;
            this.grcSanPhamBanRa.Name = "grcSanPhamBanRa";
            this.grcSanPhamBanRa.Size = new System.Drawing.Size(1241, 153);
            this.grcSanPhamBanRa.TabIndex = 0;
            this.grcSanPhamBanRa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamBanRa});
            // 
            // sanPhamBindingSource
            // 
            this.sanPhamBindingSource.DataSource = typeof(DTO.Models.SanPham);
            // 
            // grvSanPhamBanRa
            // 
            this.grvSanPhamBanRa.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaTem,
            this.colTenSanPham1,
            this.colHamLuongVang,
            this.colKhoiLuongTinh1,
            this.colKhoiLuongDa1,
            this.colKhoiLuongVang1,
            this.colTienCong,
            this.colGiaBan});
            this.grvSanPhamBanRa.GridControl = this.grcSanPhamBanRa;
            this.grvSanPhamBanRa.Name = "grvSanPhamBanRa";
            this.grvSanPhamBanRa.OptionsBehavior.ReadOnly = true;
            this.grvSanPhamBanRa.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamBanRa.OptionsView.ShowGroupPanel = false;
            // 
            // colMaTem
            // 
            this.colMaTem.Caption = "Mã tem";
            this.colMaTem.FieldName = "MaTem";
            this.colMaTem.Name = "colMaTem";
            this.colMaTem.Visible = true;
            this.colMaTem.VisibleIndex = 0;
            // 
            // colTenSanPham1
            // 
            this.colTenSanPham1.Caption = "Tên sản phẩm";
            this.colTenSanPham1.FieldName = "TenSanPham";
            this.colTenSanPham1.Name = "colTenSanPham1";
            this.colTenSanPham1.Visible = true;
            this.colTenSanPham1.VisibleIndex = 1;
            // 
            // colHamLuongVang
            // 
            this.colHamLuongVang.Caption = "HLV";
            this.colHamLuongVang.FieldName = "HamLuongVang";
            this.colHamLuongVang.Name = "colHamLuongVang";
            this.colHamLuongVang.Visible = true;
            this.colHamLuongVang.VisibleIndex = 2;
            // 
            // colKhoiLuongTinh1
            // 
            this.colKhoiLuongTinh1.Caption = "KL Tổng";
            this.colKhoiLuongTinh1.FieldName = "KhoiLuongTinh";
            this.colKhoiLuongTinh1.Name = "colKhoiLuongTinh1";
            this.colKhoiLuongTinh1.Visible = true;
            this.colKhoiLuongTinh1.VisibleIndex = 3;
            // 
            // colKhoiLuongDa1
            // 
            this.colKhoiLuongDa1.Caption = "KL Đá";
            this.colKhoiLuongDa1.FieldName = "KhoiLuongDa";
            this.colKhoiLuongDa1.Name = "colKhoiLuongDa1";
            this.colKhoiLuongDa1.Visible = true;
            this.colKhoiLuongDa1.VisibleIndex = 4;
            // 
            // colKhoiLuongVang1
            // 
            this.colKhoiLuongVang1.Caption = "KL Vàng";
            this.colKhoiLuongVang1.FieldName = "KhoiLuongVang";
            this.colKhoiLuongVang1.Name = "colKhoiLuongVang1";
            this.colKhoiLuongVang1.Visible = true;
            this.colKhoiLuongVang1.VisibleIndex = 5;
            // 
            // colTienCong
            // 
            this.colTienCong.Caption = "Tiền công";
            this.colTienCong.DisplayFormat.FormatString = "n0";
            this.colTienCong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTienCong.FieldName = "TienCong";
            this.colTienCong.Name = "colTienCong";
            this.colTienCong.Visible = true;
            this.colTienCong.VisibleIndex = 6;
            // 
            // colGiaBan
            // 
            this.colGiaBan.Caption = "Thành tiền";
            this.colGiaBan.DisplayFormat.FormatString = "n0";
            this.colGiaBan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGiaBan.FieldName = "GiaBan";
            this.colGiaBan.Name = "colGiaBan";
            this.colGiaBan.Visible = true;
            this.colGiaBan.VisibleIndex = 7;
            // 
            // sanPhamKhoCuBindingSource
            // 
            this.sanPhamKhoCuBindingSource.DataSource = typeof(DTO.Models.SanPhamKhoCu);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.grcSanPhamMuaVao);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 535);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1245, 212);
            this.groupControl3.TabIndex = 9;
            this.groupControl3.Text = "Danh sách sản phẩm mua vào";
            // 
            // grcSanPhamMuaVao
            // 
            this.grcSanPhamMuaVao.DataSource = this.sanPhamKhoCuBindingSource1;
            this.grcSanPhamMuaVao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcSanPhamMuaVao.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamMuaVao.Location = new System.Drawing.Point(2, 25);
            this.grcSanPhamMuaVao.MainView = this.grvSanPhamMuaVao;
            this.grcSanPhamMuaVao.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamMuaVao.MenuManager = this.barManager1;
            this.grcSanPhamMuaVao.Name = "grcSanPhamMuaVao";
            this.grcSanPhamMuaVao.Size = new System.Drawing.Size(1241, 185);
            this.grcSanPhamMuaVao.TabIndex = 0;
            this.grcSanPhamMuaVao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamMuaVao});
            // 
            // sanPhamKhoCuBindingSource1
            // 
            this.sanPhamKhoCuBindingSource1.DataSource = typeof(DTO.Models.SanPhamKhoCu);
            // 
            // grvSanPhamMuaVao
            // 
            this.grvSanPhamMuaVao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLoaiVangMuaVao,
            this.colTenSanPham,
            this.colKiHieuHamLuongVang,
            this.colKhoiLuongTinh,
            this.colKhoiLuongDa,
            this.colKhoiLuongVang,
            this.colDonGiaMuaVao,
            this.colGiaMuaVao});
            this.grvSanPhamMuaVao.GridControl = this.grcSanPhamMuaVao;
            this.grvSanPhamMuaVao.Name = "grvSanPhamMuaVao";
            this.grvSanPhamMuaVao.OptionsBehavior.ReadOnly = true;
            this.grvSanPhamMuaVao.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamMuaVao.OptionsView.ShowGroupPanel = false;
            // 
            // colLoaiVangMuaVao
            // 
            this.colLoaiVangMuaVao.Caption = "Loại vàng";
            this.colLoaiVangMuaVao.FieldName = "LoaiVangMuaVao";
            this.colLoaiVangMuaVao.Name = "colLoaiVangMuaVao";
            this.colLoaiVangMuaVao.Visible = true;
            this.colLoaiVangMuaVao.VisibleIndex = 0;
            // 
            // colTenSanPham
            // 
            this.colTenSanPham.Caption = "Tên";
            this.colTenSanPham.FieldName = "TenSanPham";
            this.colTenSanPham.Name = "colTenSanPham";
            this.colTenSanPham.Visible = true;
            this.colTenSanPham.VisibleIndex = 1;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "HLV";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 2;
            // 
            // colKhoiLuongTinh
            // 
            this.colKhoiLuongTinh.Caption = "KL Tổng";
            this.colKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.colKhoiLuongTinh.Name = "colKhoiLuongTinh";
            this.colKhoiLuongTinh.Visible = true;
            this.colKhoiLuongTinh.VisibleIndex = 3;
            // 
            // colKhoiLuongDa
            // 
            this.colKhoiLuongDa.Caption = "KL Đá";
            this.colKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.colKhoiLuongDa.Name = "colKhoiLuongDa";
            this.colKhoiLuongDa.Visible = true;
            this.colKhoiLuongDa.VisibleIndex = 4;
            // 
            // colKhoiLuongVang
            // 
            this.colKhoiLuongVang.Caption = "KL Vàng";
            this.colKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.colKhoiLuongVang.Name = "colKhoiLuongVang";
            this.colKhoiLuongVang.Visible = true;
            this.colKhoiLuongVang.VisibleIndex = 5;
            // 
            // colDonGiaMuaVao
            // 
            this.colDonGiaMuaVao.Caption = "Đơn giá mua";
            this.colDonGiaMuaVao.DisplayFormat.FormatString = "n0";
            this.colDonGiaMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDonGiaMuaVao.FieldName = "DonGiaMuaVao";
            this.colDonGiaMuaVao.Name = "colDonGiaMuaVao";
            this.colDonGiaMuaVao.Visible = true;
            this.colDonGiaMuaVao.VisibleIndex = 6;
            // 
            // colGiaMuaVao
            // 
            this.colGiaMuaVao.Caption = "Thành tiền";
            this.colGiaMuaVao.DisplayFormat.FormatString = "n0";
            this.colGiaMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGiaMuaVao.FieldName = "GiaMuaVao";
            this.colGiaMuaVao.Name = "colGiaMuaVao";
            this.colGiaMuaVao.Visible = true;
            this.colGiaMuaVao.VisibleIndex = 7;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 343);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1245, 6);
            this.splitterControl1.TabIndex = 10;
            this.splitterControl1.TabStop = false;
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl2.Location = new System.Drawing.Point(0, 529);
            this.splitterControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(1245, 6);
            this.splitterControl2.TabIndex = 11;
            this.splitterControl2.TabStop = false;
            // 
            // frmLichSuVangDoiVang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 747);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl2);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmLichSuVangDoiVang";
            this.Text = "Hóa đơn vàng đổi vàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmLichSuVangDoiVang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuVangDoiVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuDoiVangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuVangDoiVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaLichSuVangDoiVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamKhoCuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamMuaVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamKhoCuBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamMuaVao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcSanPhamBanRa;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamBanRa;
        private DevExpress.XtraGrid.GridControl grcLichSuVangDoiVang;
        private System.Windows.Forms.BindingSource lichSuDoiVangBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichSuVangDoiVang;
        private DevExpress.XtraGrid.Columns.GridColumn colMaLichSuBan;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangMoi;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCu;
        private DevExpress.XtraGrid.Columns.GridColumn colKLVMoiTruKLVCu;
        private DevExpress.XtraGrid.Columns.GridColumn colThanhTienVangMoi;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienVangCu;
        private DevExpress.XtraGrid.Columns.GridColumn colTienConLai;
        private DevExpress.XtraGrid.Columns.GridColumn colTienCongThem;
        private DevExpress.XtraGrid.Columns.GridColumn colTienBot;
        private DevExpress.XtraGrid.Columns.GridColumn colThuTienKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayGiaoDich;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl grcSanPhamMuaVao;
        private System.Windows.Forms.BindingSource sanPhamKhoCuBindingSource1;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamMuaVao;
        private System.Windows.Forms.BindingSource sanPhamBindingSource;
        private System.Windows.Forms.BindingSource sanPhamKhoCuBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colXoaHoaDon;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaLichSuVangDoiVang;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiVangMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colDonGiaMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colMaTem;
        private DevExpress.XtraGrid.Columns.GridColumn colTenSanPham1;
        private DevExpress.XtraGrid.Columns.GridColumn colHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTinh1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongDa1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVang1;
        private DevExpress.XtraGrid.Columns.GridColumn colTienCong;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaBan;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
    }
}