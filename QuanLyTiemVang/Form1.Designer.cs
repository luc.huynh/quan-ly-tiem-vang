﻿namespace QuanLyTiemVang
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnBanVang = new DevExpress.XtraBars.BarButtonItem();
            this.btnVangDoiVang = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnChuyenKho = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnLichSuMua = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapDauNgay = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuanLyKhoMoi = new DevExpress.XtraBars.BarButtonItem();
            this.btnLichSuBan = new DevExpress.XtraBars.BarButtonItem();
            this.btnHamLuongVang = new DevExpress.XtraBars.BarButtonItem();
            this.btnLichSuNhapKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnThuMuaVang = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuanLyLoaiTien = new DevExpress.XtraBars.BarButtonItem();
            this.btnTyGiaTien = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuyDoiTien = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhaCungCap = new DevExpress.XtraBars.BarButtonItem();
            this.btnTaoMoiCamDo = new DevExpress.XtraBars.BarButtonItem();
            this.btnHoaDonDaThanhToan = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeKhoMoi = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeGiaoDich = new DevExpress.XtraBars.BarButtonItem();
            this.btnHoaDonMua = new DevExpress.XtraBars.BarButtonItem();
            this.btnLichSuVangDoiVang = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeKhoCu = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapNgayDauKi = new DevExpress.XtraBars.BarButtonItem();
            this.btnHopDongDenHanThanhLy = new DevExpress.XtraBars.BarButtonItem();
            this.btnPhatSinhThuChi = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeThuChi = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeMuaBan = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeCamDo = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongKeNgoaiTeVaThuChi = new DevExpress.XtraBars.BarButtonItem();
            this.btnKiemKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuanLyGiaCong = new DevExpress.XtraBars.BarButtonItem();
            this.btnThoGiaCong = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup19 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup21 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage7 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup18 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage8 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup14 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup15 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup16 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup20 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage6 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup17 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btnBanVang,
            this.btnVangDoiVang,
            this.btnNhanVien,
            this.btnNhapKho,
            this.btnChuyenKho,
            this.barButtonItem1,
            this.btnLichSuMua,
            this.btnNhapDauNgay,
            this.btnQuanLyKhoMoi,
            this.btnLichSuBan,
            this.btnHamLuongVang,
            this.btnLichSuNhapKho,
            this.btnKhachHang,
            this.btnThuMuaVang,
            this.btnQuanLyLoaiTien,
            this.btnTyGiaTien,
            this.btnQuyDoiTien,
            this.btnNhaCungCap,
            this.btnTaoMoiCamDo,
            this.btnHoaDonDaThanhToan,
            this.btnThongKeKhoMoi,
            this.btnThongKeGiaoDich,
            this.btnHoaDonMua,
            this.btnLichSuVangDoiVang,
            this.btnThongKeKhoCu,
            this.btnNhapNgayDauKi,
            this.btnHopDongDenHanThanhLy,
            this.btnPhatSinhThuChi,
            this.btnThongKeThuChi,
            this.btnThongKeMuaBan,
            this.btnThongKeCamDo,
            this.btnThongKeNgoaiTeVaThuChi,
            this.btnKiemKho,
            this.btnQuanLyGiaCong,
            this.btnThoGiaCong});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbonControl1.MaxItemId = 32;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage7,
            this.ribbonPage8,
            this.ribbonPage3,
            this.ribbonPage4,
            this.ribbonPage5,
            this.ribbonPage6});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1183, 179);
            // 
            // btnBanVang
            // 
            this.btnBanVang.Caption = "Bán vàng";
            this.btnBanVang.Id = 1;
            this.btnBanVang.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.cash_register_icon;
            this.btnBanVang.LargeWidth = 100;
            this.btnBanVang.Name = "btnBanVang";
            this.btnBanVang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBanVang_ItemClick);
            // 
            // btnVangDoiVang
            // 
            this.btnVangDoiVang.Caption = "Vàng đổi Vàng";
            this.btnVangDoiVang.Id = 2;
            this.btnVangDoiVang.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.small_business_icon;
            this.btnVangDoiVang.LargeWidth = 100;
            this.btnVangDoiVang.Name = "btnVangDoiVang";
            this.btnVangDoiVang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnVangDoiVang_ItemClick);
            // 
            // btnNhanVien
            // 
            this.btnNhanVien.Caption = "Nhân viên";
            this.btnNhanVien.Id = 4;
            this.btnNhanVien.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.client_account_template_icon;
            this.btnNhanVien.LargeWidth = 100;
            this.btnNhanVien.Name = "btnNhanVien";
            this.btnNhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhanVien_ItemClick);
            // 
            // btnNhapKho
            // 
            this.btnNhapKho.Caption = "Nhập kho";
            this.btnNhapKho.Id = 3;
            this.btnNhapKho.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.box_down_icon;
            this.btnNhapKho.LargeWidth = 100;
            this.btnNhapKho.Name = "btnNhapKho";
            this.btnNhapKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapKho_ItemClick);
            // 
            // btnChuyenKho
            // 
            this.btnChuyenKho.Caption = "Quản lý kho cũ";
            this.btnChuyenKho.Id = 4;
            this.btnChuyenKho.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.door_in_icon;
            this.btnChuyenKho.LargeWidth = 100;
            this.btnChuyenKho.Name = "btnChuyenKho";
            this.btnChuyenKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnChuyenKho_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Lịch sử mua";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // btnLichSuMua
            // 
            this.btnLichSuMua.Caption = "Hóa đơn mua";
            this.btnLichSuMua.Id = 7;
            this.btnLichSuMua.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.clipboard_invoice_icon;
            this.btnLichSuMua.LargeWidth = 100;
            this.btnLichSuMua.Name = "btnLichSuMua";
            this.btnLichSuMua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLichSuMua_ItemClick);
            // 
            // btnNhapDauNgay
            // 
            this.btnNhapDauNgay.Caption = "Giá vàng đầu ngày";
            this.btnNhapDauNgay.Id = 8;
            this.btnNhapDauNgay.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.text_field_icon;
            this.btnNhapDauNgay.LargeWidth = 100;
            this.btnNhapDauNgay.Name = "btnNhapDauNgay";
            this.btnNhapDauNgay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapDauNgay_ItemClick);
            // 
            // btnQuanLyKhoMoi
            // 
            this.btnQuanLyKhoMoi.Caption = "Quản lý kho mới";
            this.btnQuanLyKhoMoi.Id = 9;
            this.btnQuanLyKhoMoi.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.door_out_icon;
            this.btnQuanLyKhoMoi.LargeWidth = 100;
            this.btnQuanLyKhoMoi.Name = "btnQuanLyKhoMoi";
            this.btnQuanLyKhoMoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuanLyKhoMoi_ItemClick);
            // 
            // btnLichSuBan
            // 
            this.btnLichSuBan.Caption = "Hóa đơn bán";
            this.btnLichSuBan.Id = 10;
            this.btnLichSuBan.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.payment_icon;
            this.btnLichSuBan.LargeWidth = 100;
            this.btnLichSuBan.Name = "btnLichSuBan";
            this.btnLichSuBan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLichSuBan_ItemClick);
            // 
            // btnHamLuongVang
            // 
            this.btnHamLuongVang.Caption = "Hàm lượng vàng";
            this.btnHamLuongVang.Id = 10;
            this.btnHamLuongVang.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.coins_icon;
            this.btnHamLuongVang.LargeWidth = 100;
            this.btnHamLuongVang.Name = "btnHamLuongVang";
            this.btnHamLuongVang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHamLuongVang_ItemClick);
            // 
            // btnLichSuNhapKho
            // 
            this.btnLichSuNhapKho.Caption = "Lịch sử nhập kho";
            this.btnLichSuNhapKho.Id = 11;
            this.btnLichSuNhapKho.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Folder_URL_History_icon;
            this.btnLichSuNhapKho.LargeWidth = 100;
            this.btnLichSuNhapKho.Name = "btnLichSuNhapKho";
            this.btnLichSuNhapKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLichSuNhapKho_ItemClick);
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Caption = "Danh sách khách hàng";
            this.btnKhachHang.Id = 11;
            this.btnKhachHang.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.user_icon;
            this.btnKhachHang.LargeWidth = 100;
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhachHang_ItemClick);
            // 
            // btnThuMuaVang
            // 
            this.btnThuMuaVang.Caption = "Thu mua vàng";
            this.btnThuMuaVang.Id = 12;
            this.btnThuMuaVang.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.coins_in_hand_icon;
            this.btnThuMuaVang.LargeWidth = 100;
            this.btnThuMuaVang.Name = "btnThuMuaVang";
            this.btnThuMuaVang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThuMuaVang_ItemClick);
            // 
            // btnQuanLyLoaiTien
            // 
            this.btnQuanLyLoaiTien.Caption = "Quản lý loại tiền";
            this.btnQuanLyLoaiTien.Id = 13;
            this.btnQuanLyLoaiTien.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.credit_icon;
            this.btnQuanLyLoaiTien.LargeWidth = 100;
            this.btnQuanLyLoaiTien.Name = "btnQuanLyLoaiTien";
            this.btnQuanLyLoaiTien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuanLyLoaiTien_ItemClick_1);
            // 
            // btnTyGiaTien
            // 
            this.btnTyGiaTien.Caption = "Tỷ giá tiền";
            this.btnTyGiaTien.Id = 14;
            this.btnTyGiaTien.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.piechart_icon;
            this.btnTyGiaTien.LargeWidth = 100;
            this.btnTyGiaTien.Name = "btnTyGiaTien";
            this.btnTyGiaTien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTyGiaTien_ItemClick_1);
            // 
            // btnQuyDoiTien
            // 
            this.btnQuyDoiTien.Caption = "Quy đổi tiền";
            this.btnQuyDoiTien.Id = 15;
            this.btnQuyDoiTien.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.conversion_of_currency_icon;
            this.btnQuyDoiTien.LargeWidth = 100;
            this.btnQuyDoiTien.Name = "btnQuyDoiTien";
            this.btnQuyDoiTien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuyDoiTien_ItemClick_1);
            // 
            // btnNhaCungCap
            // 
            this.btnNhaCungCap.Caption = "Nhà cung cấp";
            this.btnNhaCungCap.Id = 13;
            this.btnNhaCungCap.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.bank_icon;
            this.btnNhaCungCap.LargeWidth = 100;
            this.btnNhaCungCap.Name = "btnNhaCungCap";
            this.btnNhaCungCap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhaCungCap_ItemClick);
            // 
            // btnTaoMoiCamDo
            // 
            this.btnTaoMoiCamDo.Caption = "Hợp đồng cầm đồ";
            this.btnTaoMoiCamDo.Id = 14;
            this.btnTaoMoiCamDo.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.document_mark_as_final_icon;
            this.btnTaoMoiCamDo.LargeWidth = 100;
            this.btnTaoMoiCamDo.Name = "btnTaoMoiCamDo";
            this.btnTaoMoiCamDo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTaoMoiCamDo_ItemClick);
            // 
            // btnHoaDonDaThanhToan
            // 
            this.btnHoaDonDaThanhToan.Caption = "Hợp đồng đã thanh toán";
            this.btnHoaDonDaThanhToan.Id = 15;
            this.btnHoaDonDaThanhToan.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.table_icon;
            this.btnHoaDonDaThanhToan.LargeWidth = 100;
            this.btnHoaDonDaThanhToan.Name = "btnHoaDonDaThanhToan";
            this.btnHoaDonDaThanhToan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHoaDonDaThanhToan_ItemClick);
            // 
            // btnThongKeKhoMoi
            // 
            this.btnThongKeKhoMoi.Caption = "Thống kê kho mới";
            this.btnThongKeKhoMoi.Id = 16;
            this.btnThongKeKhoMoi.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.statistics_icon;
            this.btnThongKeKhoMoi.LargeWidth = 100;
            this.btnThongKeKhoMoi.Name = "btnThongKeKhoMoi";
            this.btnThongKeKhoMoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongKeKhoMoi_ItemClick);
            // 
            // btnThongKeGiaoDich
            // 
            this.btnThongKeGiaoDich.Caption = "Thống kê giao dịch";
            this.btnThongKeGiaoDich.Id = 17;
            this.btnThongKeGiaoDich.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.credit_icon;
            this.btnThongKeGiaoDich.LargeWidth = 100;
            this.btnThongKeGiaoDich.Name = "btnThongKeGiaoDich";
            this.btnThongKeGiaoDich.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongKeGiaoDich_ItemClick);
            // 
            // btnHoaDonMua
            // 
            this.btnHoaDonMua.Caption = "Hóa đơn mua vàng";
            this.btnHoaDonMua.Id = 19;
            this.btnHoaDonMua.Name = "btnHoaDonMua";
            // 
            // btnLichSuVangDoiVang
            // 
            this.btnLichSuVangDoiVang.Caption = "Hóa đơn vàng đổi vàng";
            this.btnLichSuVangDoiVang.Id = 20;
            this.btnLichSuVangDoiVang.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.small_business_icon;
            this.btnLichSuVangDoiVang.LargeWidth = 100;
            this.btnLichSuVangDoiVang.Name = "btnLichSuVangDoiVang";
            this.btnLichSuVangDoiVang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLichSuVangDoiVang_ItemClick);
            // 
            // btnThongKeKhoCu
            // 
            this.btnThongKeKhoCu.Caption = "Thống kê kho cũ";
            this.btnThongKeKhoCu.Id = 21;
            this.btnThongKeKhoCu.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.statistics_icon;
            this.btnThongKeKhoCu.LargeWidth = 100;
            this.btnThongKeKhoCu.Name = "btnThongKeKhoCu";
            // 
            // btnNhapNgayDauKi
            // 
            this.btnNhapNgayDauKi.Caption = "Nhập ngày đầu kì";
            this.btnNhapNgayDauKi.Id = 22;
            this.btnNhapNgayDauKi.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.text_field_icon;
            this.btnNhapNgayDauKi.LargeWidth = 100;
            this.btnNhapNgayDauKi.Name = "btnNhapNgayDauKi";
            this.btnNhapNgayDauKi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapNgayDauKi_ItemClick);
            // 
            // btnHopDongDenHanThanhLy
            // 
            this.btnHopDongDenHanThanhLy.Caption = "HĐ sắp đến hạn thanh lý";
            this.btnHopDongDenHanThanhLy.Id = 23;
            this.btnHopDongDenHanThanhLy.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.time_icon;
            this.btnHopDongDenHanThanhLy.LargeWidth = 100;
            this.btnHopDongDenHanThanhLy.Name = "btnHopDongDenHanThanhLy";
            this.btnHopDongDenHanThanhLy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHopDongDenHanThanhLy_ItemClick);
            // 
            // btnPhatSinhThuChi
            // 
            this.btnPhatSinhThuChi.Caption = "Phát sinh thu - chi ";
            this.btnPhatSinhThuChi.Id = 24;
            this.btnPhatSinhThuChi.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.company_generosity_icon;
            this.btnPhatSinhThuChi.LargeWidth = 100;
            this.btnPhatSinhThuChi.Name = "btnPhatSinhThuChi";
            this.btnPhatSinhThuChi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPhatSinhThuChi_ItemClick);
            // 
            // btnThongKeThuChi
            // 
            this.btnThongKeThuChi.Caption = "Thống kê tiền mặt";
            this.btnThongKeThuChi.Id = 25;
            this.btnThongKeThuChi.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.chart_bar_add_icon;
            this.btnThongKeThuChi.LargeWidth = 100;
            this.btnThongKeThuChi.Name = "btnThongKeThuChi";
            this.btnThongKeThuChi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongKeThuChi_ItemClick);
            // 
            // btnThongKeMuaBan
            // 
            this.btnThongKeMuaBan.Caption = "Mua - bán";
            this.btnThongKeMuaBan.Id = 26;
            this.btnThongKeMuaBan.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.table_chart_icon;
            this.btnThongKeMuaBan.LargeWidth = 100;
            this.btnThongKeMuaBan.Name = "btnThongKeMuaBan";
            this.btnThongKeMuaBan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongKeMuaBan_ItemClick);
            // 
            // btnThongKeCamDo
            // 
            this.btnThongKeCamDo.Caption = "Cầm đồ";
            this.btnThongKeCamDo.Id = 27;
            this.btnThongKeCamDo.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.bank_icon;
            this.btnThongKeCamDo.LargeWidth = 100;
            this.btnThongKeCamDo.Name = "btnThongKeCamDo";
            this.btnThongKeCamDo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongKeCamDo_ItemClick);
            // 
            // btnThongKeNgoaiTeVaThuChi
            // 
            this.btnThongKeNgoaiTeVaThuChi.Caption = "Ngoại tệ và thu - chi";
            this.btnThongKeNgoaiTeVaThuChi.Id = 28;
            this.btnThongKeNgoaiTeVaThuChi.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.conversion_of_currency_icon;
            this.btnThongKeNgoaiTeVaThuChi.LargeWidth = 100;
            this.btnThongKeNgoaiTeVaThuChi.Name = "btnThongKeNgoaiTeVaThuChi";
            this.btnThongKeNgoaiTeVaThuChi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongKeNgoaiTeVaThuChi_ItemClick);
            // 
            // btnKiemKho
            // 
            this.btnKiemKho.Caption = "Kiểm kho";
            this.btnKiemKho.Id = 29;
            this.btnKiemKho.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.calculator_icon;
            this.btnKiemKho.LargeWidth = 100;
            this.btnKiemKho.Name = "btnKiemKho";
            this.btnKiemKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKiemKho_ItemClick);
            // 
            // btnQuanLyGiaCong
            // 
            this.btnQuanLyGiaCong.Caption = "Quản lý gia công";
            this.btnQuanLyGiaCong.Id = 30;
            this.btnQuanLyGiaCong.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.small_business_icon;
            this.btnQuanLyGiaCong.LargeWidth = 100;
            this.btnQuanLyGiaCong.Name = "btnQuanLyGiaCong";
            this.btnQuanLyGiaCong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuanLyGiaCong_ItemClick);
            // 
            // btnThoGiaCong
            // 
            this.btnThoGiaCong.Caption = "Thợ gia công";
            this.btnThoGiaCong.Id = 31;
            this.btnThoGiaCong.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.user_icon;
            this.btnThoGiaCong.LargeWidth = 100;
            this.btnThoGiaCong.Name = "btnThoGiaCong";
            this.btnThoGiaCong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThoGiaCong_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup8,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Quản lý nhập xuất";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnBanVang);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnVangDoiVang);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnThuMuaVang);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.btnNhapDauNgay);
            this.ribbonPageGroup8.ItemLinks.Add(this.btnHamLuongVang);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnNhapKho);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnNhaCungCap);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6,
            this.ribbonPageGroup9,
            this.ribbonPageGroup19,
            this.ribbonPageGroup21});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Quản lý kho";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.btnChuyenKho);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.btnQuanLyKhoMoi);
            this.ribbonPageGroup9.ItemLinks.Add(this.btnThongKeKhoMoi);
            this.ribbonPageGroup9.ItemLinks.Add(this.btnKiemKho);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.ShowCaptionButton = false;
            // 
            // ribbonPageGroup19
            // 
            this.ribbonPageGroup19.ItemLinks.Add(this.btnPhatSinhThuChi);
            this.ribbonPageGroup19.Name = "ribbonPageGroup19";
            this.ribbonPageGroup19.ShowCaptionButton = false;
            // 
            // ribbonPageGroup21
            // 
            this.ribbonPageGroup21.ItemLinks.Add(this.btnQuanLyGiaCong);
            this.ribbonPageGroup21.ItemLinks.Add(this.btnThoGiaCong);
            this.ribbonPageGroup21.Name = "ribbonPageGroup21";
            this.ribbonPageGroup21.ShowCaptionButton = false;
            // 
            // ribbonPage7
            // 
            this.ribbonPage7.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup12,
            this.ribbonPageGroup7,
            this.ribbonPageGroup18});
            this.ribbonPage7.Name = "ribbonPage7";
            this.ribbonPage7.Text = "Quản lý cầm đồ ";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.ItemLinks.Add(this.btnTaoMoiCamDo);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            this.ribbonPageGroup12.ShowCaptionButton = false;
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.btnHoaDonDaThanhToan);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            // 
            // ribbonPageGroup18
            // 
            this.ribbonPageGroup18.ItemLinks.Add(this.btnHopDongDenHanThanhLy);
            this.ribbonPageGroup18.Name = "ribbonPageGroup18";
            this.ribbonPageGroup18.ShowCaptionButton = false;
            // 
            // ribbonPage8
            // 
            this.ribbonPage8.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup13,
            this.ribbonPageGroup14,
            this.ribbonPageGroup15});
            this.ribbonPage8.Name = "ribbonPage8";
            this.ribbonPage8.Text = "Quản lý quy đổi ngoại tệ";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.ItemLinks.Add(this.btnQuanLyLoaiTien);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.ShowCaptionButton = false;
            // 
            // ribbonPageGroup14
            // 
            this.ribbonPageGroup14.ItemLinks.Add(this.btnTyGiaTien);
            this.ribbonPageGroup14.Name = "ribbonPageGroup14";
            this.ribbonPageGroup14.ShowCaptionButton = false;
            // 
            // ribbonPageGroup15
            // 
            this.ribbonPageGroup15.ItemLinks.Add(this.btnQuyDoiTien);
            this.ribbonPageGroup15.Name = "ribbonPageGroup15";
            this.ribbonPageGroup15.ShowCaptionButton = false;
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Quản lý khách hàng";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnKhachHang);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Quản lý nhân viên";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.btnNhanVien);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup11,
            this.ribbonPageGroup10,
            this.ribbonPageGroup16,
            this.ribbonPageGroup20});
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "Thống kê";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.btnLichSuNhapKho);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.ShowCaptionButton = false;
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.btnLichSuBan);
            this.ribbonPageGroup10.ItemLinks.Add(this.btnLichSuMua);
            this.ribbonPageGroup10.ItemLinks.Add(this.btnLichSuVangDoiVang);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.ShowCaptionButton = false;
            // 
            // ribbonPageGroup16
            // 
            this.ribbonPageGroup16.ItemLinks.Add(this.btnThongKeGiaoDich);
            this.ribbonPageGroup16.ItemLinks.Add(this.btnThongKeThuChi);
            this.ribbonPageGroup16.Name = "ribbonPageGroup16";
            this.ribbonPageGroup16.ShowCaptionButton = false;
            // 
            // ribbonPageGroup20
            // 
            this.ribbonPageGroup20.ItemLinks.Add(this.btnThongKeMuaBan);
            this.ribbonPageGroup20.ItemLinks.Add(this.btnThongKeCamDo);
            this.ribbonPageGroup20.ItemLinks.Add(this.btnThongKeNgoaiTeVaThuChi);
            this.ribbonPageGroup20.Name = "ribbonPageGroup20";
            this.ribbonPageGroup20.ShowCaptionButton = false;
            this.ribbonPageGroup20.Text = "Thống kê tổng hợp";
            // 
            // ribbonPage6
            // 
            this.ribbonPage6.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup17});
            this.ribbonPage6.Name = "ribbonPage6";
            this.ribbonPage6.Text = "Cài đặt";
            // 
            // ribbonPageGroup17
            // 
            this.ribbonPageGroup17.ItemLinks.Add(this.btnNhapNgayDauKi);
            this.ribbonPageGroup17.Name = "ribbonPageGroup17";
            this.ribbonPageGroup17.ShowCaptionButton = false;
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // Form1
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1183, 783);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl1;
            this.Text = "QUẢN LÝ TIỆM VÀNG";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btnBanVang;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.BarButtonItem btnVangDoiVang;
        private DevExpress.XtraBars.BarButtonItem btnNhapKho;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage6;
        private DevExpress.XtraBars.BarButtonItem btnChuyenKho;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem btnNhanVien;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnLichSuMua;
        private DevExpress.XtraBars.BarButtonItem btnNhapDauNgay;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem btnQuanLyKhoMoi;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage7;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage8;
        private DevExpress.XtraBars.BarButtonItem btnLichSuBan;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.BarButtonItem btnHamLuongVang;
        private DevExpress.XtraBars.BarButtonItem btnLichSuNhapKho;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.BarButtonItem btnKhachHang;
        private DevExpress.XtraBars.BarButtonItem btnThuMuaVang;
        private DevExpress.XtraBars.BarButtonItem btnQuanLyLoaiTien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
        private DevExpress.XtraBars.BarButtonItem btnTyGiaTien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup14;
        private DevExpress.XtraBars.BarButtonItem btnQuyDoiTien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup15;
        private DevExpress.XtraBars.BarButtonItem btnNhaCungCap;
        private DevExpress.XtraBars.BarButtonItem btnTaoMoiCamDo;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
        private DevExpress.XtraBars.BarButtonItem btnHoaDonDaThanhToan;
        private DevExpress.XtraBars.BarButtonItem btnThongKeKhoMoi;
        private DevExpress.XtraBars.BarButtonItem btnThongKeGiaoDich;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup16;
        private DevExpress.XtraBars.BarButtonItem btnHoaDonMua;
        private DevExpress.XtraBars.BarButtonItem btnLichSuVangDoiVang;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem btnThongKeKhoCu;
        private DevExpress.XtraBars.BarButtonItem btnNhapNgayDauKi;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup17;
        private DevExpress.XtraBars.BarButtonItem btnHopDongDenHanThanhLy;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup18;
        private DevExpress.XtraBars.BarButtonItem btnPhatSinhThuChi;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup19;
        private DevExpress.XtraBars.BarButtonItem btnThongKeThuChi;
        private DevExpress.XtraBars.BarButtonItem btnThongKeMuaBan;
        private DevExpress.XtraBars.BarButtonItem btnThongKeCamDo;
        private DevExpress.XtraBars.BarButtonItem btnThongKeNgoaiTeVaThuChi;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup20;
        private DevExpress.XtraBars.BarButtonItem btnKiemKho;
        private DevExpress.XtraBars.BarButtonItem btnQuanLyGiaCong;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup21;
        private DevExpress.XtraBars.BarButtonItem btnThoGiaCong;
    }
}

