﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;

namespace QuanLyTiemVang.ThuChi
{
    public partial class frmPhatSinhThuChi : DevExpress.XtraEditors.XtraForm
    {
        public frmPhatSinhThuChi()
        {
            InitializeComponent();
        }

        List<string> listNameNotNeedClear = new List<string>
        {
            "txtTenNhanVien"
        };

        List<PhieuPhatSinhThuChi> listPhieuPhatSinhThuChiDb = new List<PhieuPhatSinhThuChi>();
 
        private void frmPhatSinhThuChi_Load(object sender, EventArgs e)
        {
            txtTenNhanVien.Text = Form1.nhanVien.Ten;
            detNgayXuatPhieu.DateTime = DateTime.Today;
            grcLichSuPhatSinhThuChi.DataSource = listPhieuPhatSinhThuChiDb;

            LoadData();
        }

        private void LoadData()
        {
            detNgayXuatPhieu.Properties.MaxValue = DateTime.Today;
            LoaiPhieuPhatSinhThuChiBUS.Instance.GetLookupEdit(lueLoaiPhieu);

            listPhieuPhatSinhThuChiDb.Clear();
            listPhieuPhatSinhThuChiDb.AddRange(PhieuPhatSinhThuChiBUS.Instance.GetList());
            grcLichSuPhatSinhThuChi.RefreshDataSource();
        }

        private void HienThiDuLieuLenForm(PhieuPhatSinhThuChi phieuPhatSinh)
        {
            lueLoaiPhieu.EditValue = phieuPhatSinh.MaLoaiPhieuPhatSinhThuChi;
            detNgayXuatPhieu.EditValue = phieuPhatSinh.NgayXuatPhieu;
            txtTenNhanVien.EditValue = phieuPhatSinh.NhanVien.Ten;
            txtSoTien.EditValue = phieuPhatSinh.SoTien;
            txtHoVaTen.EditValue = phieuPhatSinh.HoVaTen;
            txtLyDo.EditValue = phieuPhatSinh.LyDo;
            txtGhiChu.EditValue = phieuPhatSinh.GhiChu;
        }

        private bool IsValidForm()
        {
            dxerHoVaTen.Dispose();
            dxerLoaiPhieu.Dispose();
            dxerLyDo.Dispose();
            dxerNgayXuatPhieu.Dispose();
            dxerSoTien.Dispose();

            bool check = true;

            if (string.IsNullOrWhiteSpace(txtHoVaTen.Text))
            {
                dxerHoVaTen.SetError(txtHoVaTen, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(txtLyDo.Text))
            {
                dxerLyDo.SetError(txtLyDo, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (txtSoTien.EditValue == null || (int)txtSoTien.EditValue < 1)
            {
                dxerSoTien.SetError(txtSoTien, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (detNgayXuatPhieu.EditValue == null)
            {
                dxerNgayXuatPhieu.SetError(detNgayXuatPhieu, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (lueLoaiPhieu.EditValue == null)
            {
                dxerLoaiPhieu.SetError(lueLoaiPhieu, ValidationUltils.INPUT_NULL);
                check = false;
            }

            return check;
        }

        private PhieuPhatSinhThuChi GetDoiTuongTuForm()
        {
            if (IsValidForm())
            {
                return new PhieuPhatSinhThuChi
                {
                    GhiChu = txtGhiChu.Text,
                    HoVaTen = txtHoVaTen.Text,
                    LyDo = txtLyDo.Text,
                    MaLoaiPhieuPhatSinhThuChi = (int)lueLoaiPhieu.EditValue,
                    MaNhanVien = Form1.nhanVien.MaNhanVien,
                    NgayXuatPhieu = detNgayXuatPhieu.DateTime.Date,
                    SoTien = (int)txtSoTien.EditValue
                };
            }

            return null;
        }

        private void ClearForm()
        {
            ClearFormService.ClearAllForm(this, listNameNotNeedClear);
            txtTenNhanVien.EditValue = Form1.nhanVien.Ten;
            btnCapNhap.Tag = null;
            btnXoaPhieu.Tag = null;
        }

        private void grcLichSuPhatSinhThuChi_DoubleClick(object sender, EventArgs e)
        {
            PhieuPhatSinhThuChi phieuPhatSinh = (PhieuPhatSinhThuChi)grvLichSuPhatSinhThuChi.GetFocusedRow();

            if (phieuPhatSinh != null)
            {
                HienThiDuLieuLenForm(phieuPhatSinh);

                btnCapNhap.Tag = phieuPhatSinh;
                btnXoaPhieu.Tag = phieuPhatSinh;
            }
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PhieuPhatSinhThuChi phieuPhatSinh = GetDoiTuongTuForm();

            if (phieuPhatSinh == null)
            {
                return;
            }

            PhieuPhatSinhThuChi phieuThuChiInserted = PhieuPhatSinhThuChiBUS.Instance.Insert(phieuPhatSinh);

            if (phieuThuChiInserted != null)
            {
                InPhieuPhatSinhThuChi(PhieuPhatSinhThuChiBUS.Instance.GetById(phieuThuChiInserted.MaPhieuPhatSinhThuChi));
                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.INSERT_ERROR);
        }

        private void btnCapNhap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PhieuPhatSinhThuChi phieuPhatSinh = GetDoiTuongTuForm();

            if (phieuPhatSinh == null || btnCapNhap.Tag == null)
            {
                return;
            }

            PhieuPhatSinhThuChi phieuPhatSinhTag = (PhieuPhatSinhThuChi)btnCapNhap.Tag;

            phieuPhatSinh.MaPhieuPhatSinhThuChi = phieuPhatSinhTag.MaPhieuPhatSinhThuChi;

            if (PhieuPhatSinhThuChiBUS.Instance.Update(phieuPhatSinh) != null)
            {
                PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.UPDATE_ERROR);
        }

        private void btnXoaPhieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnXoaPhieu.Tag == null)
            {
                return;
            }

            PhieuPhatSinhThuChi phieuPhatSinh = (PhieuPhatSinhThuChi)btnXoaPhieu.Tag;

            var dialogResult = PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW);

            if (dialogResult == DialogResult.No)
            {
                return;
            }

            if (PhieuPhatSinhThuChiBUS.Instance.DeleteById(phieuPhatSinh.MaPhieuPhatSinhThuChi) != null)
            {
                PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.DELETE_ERROR);
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearFormService.ClearAllForm(this, listNameNotNeedClear);
            txtTenNhanVien.EditValue = Form1.nhanVien.Ten;
            btnCapNhap.Tag = null;
            btnXoaPhieu.Tag = null;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnInPhieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PhieuPhatSinhThuChi phieuPhatSinh = (PhieuPhatSinhThuChi)grvLichSuPhatSinhThuChi.GetFocusedRow();

            if (phieuPhatSinh != null)
            {
                InPhieuPhatSinhThuChi(phieuPhatSinh);
            }
            else
            {
                PopupService.Instance.Warning(MessageWarning.CHUA_CHON_PHIEU_IN);
            }
        }

        private void InPhieuPhatSinhThuChi(PhieuPhatSinhThuChi phieuThuChi)
        {
            if (phieuThuChi != null)
            {
                PhieuThuChiPrint phieuPhatSinhThuChiPrint = new PhieuThuChiPrint();
                phieuPhatSinhThuChiPrint.DataSource = new PhieuPhatSinhThuChi[] { phieuThuChi };
                phieuPhatSinhThuChiPrint.ShowPreviewDialog();
            }
            else
            {
                PopupService.Instance.Warning(MessageWarning.CHUA_CHON_PHIEU_IN);
            }
        }
    }
}