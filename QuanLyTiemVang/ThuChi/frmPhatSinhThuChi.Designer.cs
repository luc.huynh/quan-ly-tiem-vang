﻿namespace QuanLyTiemVang.ThuChi
{
    partial class frmPhatSinhThuChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnCapNhap = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoaPhieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtGhiChu = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDo = new DevExpress.XtraEditors.TextEdit();
            this.txtSoTien = new DevExpress.XtraEditors.TextEdit();
            this.txtHoVaTen = new DevExpress.XtraEditors.TextEdit();
            this.detNgayXuatPhieu = new DevExpress.XtraEditors.DateEdit();
            this.lueLoaiPhieu = new DevExpress.XtraEditors.LookUpEdit();
            this.txtTenNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcLichSuPhatSinhThuChi = new DevExpress.XtraGrid.GridControl();
            this.phieuPhatSinhThuChiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvLichSuPhatSinhThuChi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaPhieuPhatSinhThuChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayXuatPhieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoVaTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLyDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiPhieuPhatSinhThuChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dxerLoaiPhieu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerNgayXuatPhieu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerHoVaTen = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerSoTien = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyDo = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.btnInPhieu = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoVaTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayXuatPhieu.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayXuatPhieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiPhieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuPhatSinhThuChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phieuPhatSinhThuChiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuPhatSinhThuChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiPhieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayXuatPhieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHoVaTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerSoTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDo)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnThem,
            this.btnCapNhap,
            this.btnXoaPhieu,
            this.btnInPhieu});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 14;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThem, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnCapNhap, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoaPhieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInPhieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnThem
            // 
            this.btnThem.Caption = "Thêm phiếu và in";
            this.btnThem.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThem.Id = 10;
            this.btnThem.Name = "btnThem";
            this.btnThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThem_ItemClick);
            // 
            // btnCapNhap
            // 
            this.btnCapNhap.Caption = "Cập nhập";
            this.btnCapNhap.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnCapNhap.Id = 11;
            this.btnCapNhap.Name = "btnCapNhap";
            this.btnCapNhap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCapNhap_ItemClick);
            // 
            // btnXoaPhieu
            // 
            this.btnXoaPhieu.Caption = "Xóa phiếu";
            this.btnXoaPhieu.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoaPhieu.Id = 12;
            this.btnXoaPhieu.Name = "btnXoaPhieu";
            this.btnXoaPhieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoaPhieu_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1036, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 618);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1036, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 568);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1036, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 568);
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1036, 193);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Thông tin phiếu thu - chi";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtGhiChu);
            this.layoutControl1.Controls.Add(this.txtLyDo);
            this.layoutControl1.Controls.Add(this.txtSoTien);
            this.layoutControl1.Controls.Add(this.txtHoVaTen);
            this.layoutControl1.Controls.Add(this.detNgayXuatPhieu);
            this.layoutControl1.Controls.Add(this.lueLoaiPhieu);
            this.layoutControl1.Controls.Add(this.txtTenNhanVien);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1032, 166);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(150, 128);
            this.txtGhiChu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtGhiChu.MenuManager = this.barManager1;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(866, 22);
            this.txtGhiChu.StyleController = this.layoutControl1;
            this.txtGhiChu.TabIndex = 10;
            // 
            // txtLyDo
            // 
            this.txtLyDo.Location = new System.Drawing.Point(150, 100);
            this.txtLyDo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDo.MenuManager = this.barManager1;
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.Size = new System.Drawing.Size(866, 22);
            this.txtLyDo.StyleController = this.layoutControl1;
            this.txtLyDo.TabIndex = 9;
            // 
            // txtSoTien
            // 
            this.txtSoTien.Location = new System.Drawing.Point(150, 72);
            this.txtSoTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSoTien.MenuManager = this.barManager1;
            this.txtSoTien.Name = "txtSoTien";
            this.txtSoTien.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtSoTien.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoTien.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSoTien.Properties.NullText = "0 VNĐ";
            this.txtSoTien.Size = new System.Drawing.Size(866, 22);
            this.txtSoTien.StyleController = this.layoutControl1;
            this.txtSoTien.TabIndex = 8;
            // 
            // txtHoVaTen
            // 
            this.txtHoVaTen.Location = new System.Drawing.Point(654, 44);
            this.txtHoVaTen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHoVaTen.MenuManager = this.barManager1;
            this.txtHoVaTen.Name = "txtHoVaTen";
            this.txtHoVaTen.Size = new System.Drawing.Size(362, 22);
            this.txtHoVaTen.StyleController = this.layoutControl1;
            this.txtHoVaTen.TabIndex = 7;
            // 
            // detNgayXuatPhieu
            // 
            this.detNgayXuatPhieu.EditValue = null;
            this.detNgayXuatPhieu.Location = new System.Drawing.Point(150, 44);
            this.detNgayXuatPhieu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detNgayXuatPhieu.MenuManager = this.barManager1;
            this.detNgayXuatPhieu.Name = "detNgayXuatPhieu";
            this.detNgayXuatPhieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayXuatPhieu.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayXuatPhieu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayXuatPhieu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayXuatPhieu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detNgayXuatPhieu.Size = new System.Drawing.Size(364, 22);
            this.detNgayXuatPhieu.StyleController = this.layoutControl1;
            this.detNgayXuatPhieu.TabIndex = 6;
            // 
            // lueLoaiPhieu
            // 
            this.lueLoaiPhieu.Location = new System.Drawing.Point(150, 16);
            this.lueLoaiPhieu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueLoaiPhieu.MenuManager = this.barManager1;
            this.lueLoaiPhieu.Name = "lueLoaiPhieu";
            this.lueLoaiPhieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiPhieu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiPhieu", "Loại phiếu")});
            this.lueLoaiPhieu.Properties.NullText = "";
            this.lueLoaiPhieu.Size = new System.Drawing.Size(364, 22);
            this.lueLoaiPhieu.StyleController = this.layoutControl1;
            this.lueLoaiPhieu.TabIndex = 5;
            // 
            // txtTenNhanVien
            // 
            this.txtTenNhanVien.Location = new System.Drawing.Point(654, 16);
            this.txtTenNhanVien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTenNhanVien.MenuManager = this.barManager1;
            this.txtTenNhanVien.Name = "txtTenNhanVien";
            this.txtTenNhanVien.Properties.ReadOnly = true;
            this.txtTenNhanVien.Size = new System.Drawing.Size(362, 22);
            this.txtTenNhanVien.StyleController = this.layoutControl1;
            this.txtTenNhanVien.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1032, 166);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtTenNhanVien;
            this.layoutControlItem1.Location = new System.Drawing.Point(504, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(502, 28);
            this.layoutControlItem1.Text = "Nhân viên xuất phiếu";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(130, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueLoaiPhieu;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(504, 28);
            this.layoutControlItem2.Text = "Loại phiếu";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(130, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.detNgayXuatPhieu;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(504, 28);
            this.layoutControlItem3.Text = "Ngày";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(130, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtHoVaTen;
            this.layoutControlItem4.Location = new System.Drawing.Point(504, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(502, 28);
            this.layoutControlItem4.Text = "Họ và tên";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(130, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtSoTien;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1006, 28);
            this.layoutControlItem5.Text = "Số tiền";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(130, 17);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtGhiChu;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(1006, 28);
            this.layoutControlItem7.Text = "Ghi chú";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(130, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtLyDo;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1006, 28);
            this.layoutControlItem6.Text = "Lý do";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(130, 16);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcLichSuPhatSinhThuChi);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 243);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1036, 375);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Lịch sử phát sinh thu chi";
            // 
            // grcLichSuPhatSinhThuChi
            // 
            this.grcLichSuPhatSinhThuChi.DataSource = this.phieuPhatSinhThuChiBindingSource;
            this.grcLichSuPhatSinhThuChi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcLichSuPhatSinhThuChi.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcLichSuPhatSinhThuChi.Location = new System.Drawing.Point(2, 25);
            this.grcLichSuPhatSinhThuChi.MainView = this.grvLichSuPhatSinhThuChi;
            this.grcLichSuPhatSinhThuChi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcLichSuPhatSinhThuChi.MenuManager = this.barManager1;
            this.grcLichSuPhatSinhThuChi.Name = "grcLichSuPhatSinhThuChi";
            this.grcLichSuPhatSinhThuChi.Size = new System.Drawing.Size(1032, 348);
            this.grcLichSuPhatSinhThuChi.TabIndex = 0;
            this.grcLichSuPhatSinhThuChi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichSuPhatSinhThuChi});
            this.grcLichSuPhatSinhThuChi.DoubleClick += new System.EventHandler(this.grcLichSuPhatSinhThuChi_DoubleClick);
            // 
            // phieuPhatSinhThuChiBindingSource
            // 
            this.phieuPhatSinhThuChiBindingSource.DataSource = typeof(DTO.Models.PhieuPhatSinhThuChi);
            // 
            // grvLichSuPhatSinhThuChi
            // 
            this.grvLichSuPhatSinhThuChi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaPhieuPhatSinhThuChi,
            this.colNgayXuatPhieu,
            this.colSoTien,
            this.colHoVaTen,
            this.colLyDo,
            this.colGhiChu,
            this.colNhanVien,
            this.colLoaiPhieuPhatSinhThuChi});
            this.grvLichSuPhatSinhThuChi.GridControl = this.grcLichSuPhatSinhThuChi;
            this.grvLichSuPhatSinhThuChi.Name = "grvLichSuPhatSinhThuChi";
            this.grvLichSuPhatSinhThuChi.OptionsBehavior.Editable = false;
            this.grvLichSuPhatSinhThuChi.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvLichSuPhatSinhThuChi.OptionsFind.AlwaysVisible = true;
            this.grvLichSuPhatSinhThuChi.OptionsFind.FindDelay = 100;
            this.grvLichSuPhatSinhThuChi.OptionsFind.FindNullPrompt = "";
            this.grvLichSuPhatSinhThuChi.OptionsFind.ShowFindButton = false;
            this.grvLichSuPhatSinhThuChi.OptionsView.ShowGroupPanel = false;
            // 
            // colMaPhieuPhatSinhThuChi
            // 
            this.colMaPhieuPhatSinhThuChi.Caption = "Mã phiếu";
            this.colMaPhieuPhatSinhThuChi.FieldName = "MaPhieuPhatSinhThuChi";
            this.colMaPhieuPhatSinhThuChi.Name = "colMaPhieuPhatSinhThuChi";
            this.colMaPhieuPhatSinhThuChi.Visible = true;
            this.colMaPhieuPhatSinhThuChi.VisibleIndex = 0;
            // 
            // colNgayXuatPhieu
            // 
            this.colNgayXuatPhieu.Caption = "Ngày";
            this.colNgayXuatPhieu.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayXuatPhieu.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayXuatPhieu.FieldName = "NgayXuatPhieu";
            this.colNgayXuatPhieu.Name = "colNgayXuatPhieu";
            this.colNgayXuatPhieu.Visible = true;
            this.colNgayXuatPhieu.VisibleIndex = 2;
            // 
            // colSoTien
            // 
            this.colSoTien.Caption = "Số tiền";
            this.colSoTien.DisplayFormat.FormatString = "###,###,###,###,### VNĐ";
            this.colSoTien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSoTien.FieldName = "SoTien";
            this.colSoTien.Name = "colSoTien";
            this.colSoTien.Visible = true;
            this.colSoTien.VisibleIndex = 3;
            // 
            // colHoVaTen
            // 
            this.colHoVaTen.Caption = "Họ và tên";
            this.colHoVaTen.FieldName = "HoVaTen";
            this.colHoVaTen.Name = "colHoVaTen";
            this.colHoVaTen.Visible = true;
            this.colHoVaTen.VisibleIndex = 4;
            // 
            // colLyDo
            // 
            this.colLyDo.Caption = "Lý do";
            this.colLyDo.FieldName = "LyDo";
            this.colLyDo.Name = "colLyDo";
            this.colLyDo.Visible = true;
            this.colLyDo.VisibleIndex = 5;
            // 
            // colGhiChu
            // 
            this.colGhiChu.Caption = "Ghi chú";
            this.colGhiChu.FieldName = "GhiChu";
            this.colGhiChu.Name = "colGhiChu";
            this.colGhiChu.Visible = true;
            this.colGhiChu.VisibleIndex = 6;
            // 
            // colNhanVien
            // 
            this.colNhanVien.Caption = "Tên nhân viên";
            this.colNhanVien.FieldName = "NhanVien.Ten";
            this.colNhanVien.Name = "colNhanVien";
            this.colNhanVien.Visible = true;
            this.colNhanVien.VisibleIndex = 7;
            // 
            // colLoaiPhieuPhatSinhThuChi
            // 
            this.colLoaiPhieuPhatSinhThuChi.Caption = "Loại phiếu";
            this.colLoaiPhieuPhatSinhThuChi.FieldName = "LoaiPhieuPhatSinhThuChi.TenLoaiPhieu";
            this.colLoaiPhieuPhatSinhThuChi.Name = "colLoaiPhieuPhatSinhThuChi";
            this.colLoaiPhieuPhatSinhThuChi.Visible = true;
            this.colLoaiPhieuPhatSinhThuChi.VisibleIndex = 1;
            // 
            // dxerLoaiPhieu
            // 
            this.dxerLoaiPhieu.ContainerControl = this;
            // 
            // dxerNgayXuatPhieu
            // 
            this.dxerNgayXuatPhieu.ContainerControl = this;
            // 
            // dxerHoVaTen
            // 
            this.dxerHoVaTen.ContainerControl = this;
            // 
            // dxerSoTien
            // 
            this.dxerSoTien.ContainerControl = this;
            // 
            // dxerLyDo
            // 
            this.dxerLyDo.ContainerControl = this;
            // 
            // btnInPhieu
            // 
            this.btnInPhieu.Caption = "In phiếu";
            this.btnInPhieu.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInPhieu.Id = 13;
            this.btnInPhieu.Name = "btnInPhieu";
            this.btnInPhieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInPhieu_ItemClick);
            // 
            // frmPhatSinhThuChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 618);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmPhatSinhThuChi";
            this.Text = "Phát sinh thu chi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPhatSinhThuChi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoVaTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayXuatPhieu.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayXuatPhieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiPhieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuPhatSinhThuChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phieuPhatSinhThuChiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuPhatSinhThuChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiPhieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayXuatPhieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHoVaTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerSoTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTenNhanVien;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtGhiChu;
        private DevExpress.XtraEditors.TextEdit txtLyDo;
        private DevExpress.XtraEditors.TextEdit txtSoTien;
        private DevExpress.XtraEditors.TextEdit txtHoVaTen;
        private DevExpress.XtraEditors.DateEdit detNgayXuatPhieu;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiPhieu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnCapNhap;
        private DevExpress.XtraBars.BarButtonItem btnXoaPhieu;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcLichSuPhatSinhThuChi;
        private System.Windows.Forms.BindingSource phieuPhatSinhThuChiBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichSuPhatSinhThuChi;
        private DevExpress.XtraGrid.Columns.GridColumn colMaPhieuPhatSinhThuChi;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayXuatPhieu;
        private DevExpress.XtraGrid.Columns.GridColumn colSoTien;
        private DevExpress.XtraGrid.Columns.GridColumn colHoVaTen;
        private DevExpress.XtraGrid.Columns.GridColumn colLyDo;
        private DevExpress.XtraGrid.Columns.GridColumn colGhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn colNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiPhieuPhatSinhThuChi;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLoaiPhieu;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNgayXuatPhieu;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHoVaTen;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerSoTien;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyDo;
        private DevExpress.XtraBars.BarButtonItem btnInPhieu;
    }
}