﻿namespace QuanLyTiemVang.NHAPDAUNGAY
{
    partial class frmNhapDauNgay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.grpDanhSachHamLuongVang = new DevExpress.XtraEditors.GroupControl();
            this.grcDanhSachHamLuongVang = new DevExpress.XtraGrid.GridControl();
            this.grvDanhSachHamLuongVang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaVangMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayNhap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnNhapDauNgay = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lichSuGiaVangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachHamLuongVang)).BeginInit();
            this.grpDanhSachHamLuongVang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuGiaVangBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.grpDanhSachHamLuongVang);
            this.layoutControl1.Controls.Add(this.btnNhapDauNgay);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(546, 323);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // grpDanhSachHamLuongVang
            // 
            this.grpDanhSachHamLuongVang.Controls.Add(this.grcDanhSachHamLuongVang);
            this.grpDanhSachHamLuongVang.Location = new System.Drawing.Point(12, 12);
            this.grpDanhSachHamLuongVang.Name = "grpDanhSachHamLuongVang";
            this.grpDanhSachHamLuongVang.Size = new System.Drawing.Size(522, 273);
            this.grpDanhSachHamLuongVang.TabIndex = 6;
            this.grpDanhSachHamLuongVang.Text = "Thông tin giá vàng";
            // 
            // grcDanhSachHamLuongVang
            // 
            this.grcDanhSachHamLuongVang.DataSource = this.lichSuGiaVangBindingSource;
            this.grcDanhSachHamLuongVang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachHamLuongVang.Location = new System.Drawing.Point(2, 20);
            this.grcDanhSachHamLuongVang.MainView = this.grvDanhSachHamLuongVang;
            this.grcDanhSachHamLuongVang.Name = "grcDanhSachHamLuongVang";
            this.grcDanhSachHamLuongVang.Size = new System.Drawing.Size(518, 251);
            this.grcDanhSachHamLuongVang.TabIndex = 0;
            this.grcDanhSachHamLuongVang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDanhSachHamLuongVang});
            // 
            // grvDanhSachHamLuongVang
            // 
            this.grvDanhSachHamLuongVang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclHamLuongVang,
            this.grclGiaVang,
            this.grclGiaVangMuaVao,
            this.NgayNhap});
            this.grvDanhSachHamLuongVang.GridControl = this.grcDanhSachHamLuongVang;
            this.grvDanhSachHamLuongVang.Name = "grvDanhSachHamLuongVang";
            this.grvDanhSachHamLuongVang.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvDanhSachHamLuongVang.OptionsView.ShowGroupPanel = false;
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "Hàm Lượng vàng";
            this.grclHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.OptionsColumn.AllowEdit = false;
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 0;
            // 
            // grclGiaVang
            // 
            this.grclGiaVang.Caption = "Giá Vàng Bán Ra (1 chỉ)";
            this.grclGiaVang.DisplayFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaVang.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaVang.FieldName = "GiaVangBanRa";
            this.grclGiaVang.GroupFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaVang.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaVang.Name = "grclGiaVang";
            this.grclGiaVang.Visible = true;
            this.grclGiaVang.VisibleIndex = 1;
            // 
            // grclGiaVangMuaVao
            // 
            this.grclGiaVangMuaVao.Caption = "Giá Vàng Mua Vào (1 chỉ)";
            this.grclGiaVangMuaVao.DisplayFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaVangMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaVangMuaVao.FieldName = "GiaVangMuaVao";
            this.grclGiaVangMuaVao.GroupFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaVangMuaVao.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaVangMuaVao.Name = "grclGiaVangMuaVao";
            this.grclGiaVangMuaVao.Visible = true;
            this.grclGiaVangMuaVao.VisibleIndex = 2;
            // 
            // NgayNhap
            // 
            this.NgayNhap.Caption = "Ngày nhập";
            this.NgayNhap.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayNhap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayNhap.FieldName = "NgayBan";
            this.NgayNhap.Name = "NgayNhap";
            this.NgayNhap.Visible = true;
            this.NgayNhap.VisibleIndex = 3;
            // 
            // btnNhapDauNgay
            // 
            this.btnNhapDauNgay.Location = new System.Drawing.Point(12, 289);
            this.btnNhapDauNgay.Name = "btnNhapDauNgay";
            this.btnNhapDauNgay.Size = new System.Drawing.Size(522, 22);
            this.btnNhapDauNgay.StyleController = this.layoutControl1;
            this.btnNhapDauNgay.TabIndex = 5;
            this.btnNhapDauNgay.Text = "Nhập";
            this.btnNhapDauNgay.Click += new System.EventHandler(this.btnNhapDauNgay_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(546, 323);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnNhapDauNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 277);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(526, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.grpDanhSachHamLuongVang;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(526, 277);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lichSuGiaVangBindingSource
            // 
            this.lichSuGiaVangBindingSource.DataSource = typeof(DTO.Models.LichSuGiaVang);
            // 
            // frmNhapDauNgay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 323);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNhapDauNgay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập giá vàng";
            this.Load += new System.EventHandler(this.frmNhapDauNgay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachHamLuongVang)).EndInit();
            this.grpDanhSachHamLuongVang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuGiaVangBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnNhapDauNgay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.GroupControl grpDanhSachHamLuongVang;
        private DevExpress.XtraGrid.GridControl grcDanhSachHamLuongVang;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDanhSachHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn NgayNhap;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaVangMuaVao;
        private System.Windows.Forms.BindingSource lichSuGiaVangBindingSource;
    }
}