﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyTiemVang.Constants;
using BUS;
using DTO.TemporaryModel;
using DTO.Models;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang.NHAPDAUNGAY
{
    public partial class frmNhapDauNgay : Form
    {
        //Chứa giá vàng hiện tại được lấy từ cơ sở dữ liệu đưa lên
        private List<LichSuGiaVang> listGiaVangHienTai;

        public frmNhapDauNgay()
        {
            InitializeComponent();
        }

        private void btnNhapDauNgay_Click(object sender, EventArgs e)
        {
            int index = 0;
            foreach (LichSuGiaVang giavangHienTai in listGiaVangHienTai)
            {
                giavangHienTai.GiaVangBanRa = ((LichSuGiaVang)grvDanhSachHamLuongVang.GetRow(index)).GiaVangBanRa;

                giavangHienTai.GiaVangMuaVao = ((LichSuGiaVang)grvDanhSachHamLuongVang.GetRow(index)).GiaVangMuaVao;
                index++;
            }

            if (LichSuGiaVangBUS.Instance.NhapGiaVangDauNgay(listGiaVangHienTai, Form1.nhanVien.MaNhanVien, DateTime.Now))
            {
                PopupService.Instance.Success(ErrorUltils.SUCCESS);
                this.Hide();
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.ENOUGH_DATA);
            }
        }

        private void frmNhapDauNgay_Load(object sender, EventArgs e)
        {
            listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();
            grcDanhSachHamLuongVang.DataSource = listGiaVangHienTai;
        }
    }
}
