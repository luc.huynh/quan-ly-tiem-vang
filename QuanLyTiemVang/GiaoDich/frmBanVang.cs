﻿using System;
using System.Windows.Forms;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.BluePrints;
using QuanLyTiemVang.InTem;
using DevExpress.XtraEditors;
using QuanLyTiemVang.Constants.StringUltils;
using System.Collections.Generic;
using DTO.TemporaryModel;
using System.Linq;
using Share.Util;
using Share.Constant;

namespace QuanLyTiemVang
{
    public partial class frmBanVang : Form
    {
        public frmBanVang()
        {
            InitializeComponent();
            grvSanPham.IndicatorWidth = 70;
        }

        private List<SanPham> listSanPhamBanRa = new List<SanPham>(); // danh sách chứa các sản phẩm trong hóa đơn bán ra
        private List<SanPham> listSanPhamChuaBan = new List<SanPham>(); // danh sách các sản phẩm chưa bán
        private LichSuBan lichSuBan = null; // lịch sử bán hiện tại của form

        private void frmSell_Load(object sender, EventArgs e)
        {
            SetDataSourceForAllGridControl();
            GetNewSoHoaDon();
            LoadData();
        }

        private void LoadData()
        {
            LoadKhachHang();
            SetDefaultValueForForm();
            GetDataForGridSanPhamChuaBan();
            ReloadAllGridControl();
        }

        private void LoadKhachHang()
        {
            KhachHangBUS.Instance.GetList(lueKhachHang);
        }

        private void SetDefaultValueForForm()
        {
            detNgayBan.DateTime = DateTime.Now;
            txtNhanVien.Text = Form1.nhanVien.Ten;

            txtMaTem.Focus();
        }

        private void SetDataSourceForAllGridControl()
        {
            grcSanPham.DataSource = listSanPhamChuaBan;
            grcHoaDon.DataSource = listSanPhamBanRa;
        }

        private void GetDataForGridSanPhamChuaBan()
        {
            List<SanPham> listSanPhamTemp = SanPhamBUS.Instance.GetListSanPhamChuaBan();

            listSanPhamChuaBan.Clear();

            foreach (var sanPham in listSanPhamTemp)
            {
                listSanPhamChuaBan.Add(sanPham);
            }

            HienThiTongSoSanPham();
        }

        private void GetNewSoHoaDon()
        {
            txtSoHoaDon.Text = Util.TaoMaTheoNamThangNgayGioPhutGiay();
        }

        private void HienThiTongSoSanPham()
        {
            lbTongSoLuongSanPham.Text = "Tổng số sản phẩm: " + listSanPhamChuaBan.Count.ToString();
        }

        private void ReloadAllGridControl()
        {
            grcSanPham.RefreshDataSource();
            grcHoaDon.RefreshDataSource();
        }

        private void grcSanPham_DoubleClick(object sender, EventArgs e)
        {
            var sanPham = (SanPham)grvSanPham.GetFocusedRow();

            HienThiChiTietSanPham(sanPham);
        }

        private void HienThiChiTietSanPham(SanPham sanPham)
        {
            try
            {
                btnThemVaoHoaDon.Tag = sanPham;

                txtTienCong.EditValue = sanPham.TienCong;
                txtMaTem.Text = sanPham.MaTem;
                txtTenSanPham.Text = sanPham.TenSanPham;
                txtGiaBan.EditValue = sanPham.GiaBan;
                txtHamLuongVang.Text = sanPham.HamLuongVang.ToString();
                txtTiLeHot.Text = sanPham.TiLeHot.ToString();

                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiVang, txtPhanVang, txtLyVang);

                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiTinh, txtPhanTinh, txtLyTinh);

                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiDa, txtPhanDa, txtLyDa);
            }
            catch (Exception ex)
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                Console.WriteLine(ex.ToString());
            }
        }

        private void ThemSanPhamVaoHoaDon(SanPham sanPham)
        {
            listSanPhamBanRa.Add(sanPham);
            listSanPhamChuaBan.Remove(sanPham);
        }

        private void XoaSanPhamKhoiHoaDon(SanPham sanPham)
        {
            listSanPhamBanRa.Remove(sanPham);
            listSanPhamChuaBan.Add(sanPham);
        }

        private void TinhToanLichSuBan()
        {
            lichSuBan = BanVangService.Instance.TinhToanLichSuBan(listSanPhamBanRa, txtTienBot, lueKhachHang);
            HienThiThanhTienHoaDon();
        }

        private void HienThiThanhTienHoaDon()
        {
            if (lichSuBan != null)
            {
                txtThanhTien.EditValue = lichSuBan.ThanhTienHoaDon;
            }
        }

        private void btnThemVaoHoaDon_Click(object sender, EventArgs e)
        {
            if (btnThemVaoHoaDon.Tag != null)
            {
                SanPham sanPham = (SanPham)btnThemVaoHoaDon.Tag;

                if (txtGiaBan.EditValue == null)
                {
                    PopupService.Instance.Error(ErrorUltils.PRICE_NULL);
                    return;
                }

                ThemSanPhamVaoHoaDon(sanPham);
                RefreshAllgridView();
                TinhToanLichSuBan();
                ClearFormThongTinSanPham();
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.TEM_NULL);
            }

        }

        private void btnNhapLai_Click(object sender, EventArgs e)
        {
            ClearFormThongTinSanPham();
        }

        /// <summary>
        /// Xóa tất cả thông tin ở mục sản phẩm đang chọn
        /// </summary>
        private void ClearFormThongTinSanPham()
        {
            btnThemVaoHoaDon.Tag = null;
            txtMaTem.Text =
                txtTenSanPham.Text =
                txtTiLeHot.Text =
                txtHamLuongVang.Text =
                txtGiaBan.Text =
                txtChiVang.Text =
                txtPhanVang.Text =
                txtLyTinh.Text =
                txtChiTinh.Text =
                txtPhanTinh.Text =
                txtLyDa.Text =
                txtChiDa.Text =
                txtPhanDa.Text =
                txtLyVang.Text = null;
            txtTienCong.EditValue = null;
        }

        /// <summary>
        /// Mô tả hành động khi đóng form
        /// Các công việc sẽ thực hiện: Xóa hóa hơn tạm thời --> Xóa danh sách mặt hàng trong list hóa đơn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSell_FormClosed(object sender, FormClosedEventArgs e)
        {
            //HoaDonBUS.Instance.XoaTatCaSanPhamOHoaDon();
        }

        private void btnNhapLaiHoaDon_Click(object sender, EventArgs e)
        {
            GetDataForGridSanPhamChuaBan();
            listSanPhamBanRa.Clear();
            RefreshAllgridView();

            //Tổng tiền
            txtTienBot.EditValue = null;
            txtThanhTien.EditValue = null;
        }

        private void InHoaDonBanVang(DateTime ngayBan)
        {
            XtraReport1 hoaDonIn = new XtraReport1();
            List<HoaDonBanVang> hoaDonBanVangs = BanVangService.Instance.GetListHoaDonBanVang(
                listSanPhamBanRa,
                lueKhachHang,
                txtTienBot,
                txtSoHoaDon.Text,
                ngayBan);

            hoaDonIn.DataSource = hoaDonBanVangs;
            hoaDonIn.ShowPreviewDialog();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            var result = PopupService.Instance.Question(MessageWarning.THANH_TOAN_BAN_VANG);

            if (result == DialogResult.No)
            {
                return;
            }

            string maKhachHang = null;

            if (lueKhachHang.EditValue != null)
            {
                maKhachHang = lueKhachHang.EditValue.ToString();
            }
            if (listSanPhamBanRa.Count == 0)
            {
                PopupService.Instance.Warning(ErrorUltils.HOA_DON_INVALID);
                return;
            }

            lichSuBan.MaLichSuBan = txtSoHoaDon.Text;

            LichSuBan lichSuBanThanhToan = LichSuBanBUS.Instance.ThanhToanHoaDon(lichSuBan, listSanPhamBanRa);

            if (lichSuBanThanhToan == null)
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                return;
            }

            InHoaDonBanVang(lichSuBan.NgayGiaoDich);

            PopupService.Instance.Success(MessageSucces.GIAO_DICH_SUCCES);

            lichSuBan = null;
            listSanPhamBanRa.Clear();

            ClearFormThongTinSanPham();
            GetNewSoHoaDon();
            GetDataForGridSanPhamChuaBan();
            RefreshAllgridView();

            //Tổng tiền
            txtThanhTien.EditValue = null;
            txtTienBot.EditValue = null;
        }

        private void btnDeleteItem_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            SanPham sanPham = (SanPham)grvHoaDon.GetFocusedRow();

            XoaSanPhamKhoiHoaDon(sanPham);
            RefreshAllgridView();
            TinhToanLichSuBan();
        }

        private void RefreshAllgridView()
        {
            grcHoaDon.RefreshDataSource();
            grcSanPham.RefreshDataSource();
        }

        private void btnInTemMotSanPham_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string maTem;
            if (string.IsNullOrWhiteSpace(txtMaTem.Text))
            {
                PopupService.Instance.Error(ValidationUltils.NHAP_TEM_BAN_HANG);
            }
            else
            {
                maTem = txtMaTem.Text;
                SanPham sanPham = SanPhamBUS.Instance.GetSanPhamByMaTem(maTem);
                if (sanPham == null)
                {
                    PopupService.Instance.Error(ValidationUltils.KHONG_TIM_THAY_MA_SAN_PHAM);
                }
                else
                {
                    TemVang temVang = new TemVang();
                    temVang.DataSource = new SanPhamInTem[] { BluePrint.Instance.ConvertToSanPhamInTem(sanPham) };
                    temVang.ShowPreviewDialog();
                }
            }
        }

        private bool IsValidCatVang()
        {
            if (txtTinhCat.EditValue == null
                && txtVangCat.EditValue == null
                && txtDaCat.EditValue == null)
            {
                return false;
            }

            return true;
        }

        private void btnCatVang_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMaTem.Text))
            {
                PopupService.Instance.Error(ValidationUltils.NHAP_TEM_BAN_HANG);
                return;
            }

            if (!IsValidCatVang())
            {
                PopupService.Instance.Error(ValidationUltils.NHAP_KHOI_LUONG_CAT_VANG);
                return;
            }

            string maTem = txtMaTem.Text;
            SanPham sanPham = SanPhamBUS.Instance.GetSanPhamByMaTem(maTem);
            double khoiLuongVangCat = double.Parse(txtVangCat.Text);
            double khoiLuongDaCat = double.Parse(txtDaCat.Text);
            double khoiLuongTinhCat = double.Parse(txtTinhCat.Text);

            if (sanPham.KhoiLuongDa < khoiLuongDaCat
                || sanPham.KhoiLuongTinh < khoiLuongTinhCat
                || sanPham.KhoiLuongVang < khoiLuongVangCat)
            {
                PopupService.Instance.Error(ValidationUltils.KHOI_LUONG_CAT_KHONG_DU);
                return;
            }

            if (PopupService.Instance.Question(MessageWarning.CAT_VANG) == DialogResult.Yes)
            {
                List<KiHieuHamLuongVang> listHamLuongVang = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang();

                SanPhamKhoCu sanPhamCatVang = new SanPhamKhoCu
                {
                    KhoiLuongDa = khoiLuongDaCat,
                    KhoiLuongVang = khoiLuongVangCat,
                    KhoiLuongTinh = khoiLuongTinhCat,
                    NgayNhap = DateTime.Now,
                    TenSanPham = sanPham.TenSanPham,
                    TiLeHot = 0,
                    LoaiVangMuaVao = Constant.VANG_CU_CAT_BOT,
                    MaHamLuongVang = listHamLuongVang.FirstOrDefault(x => x.HamLuongVang == sanPham.HamLuongVang).MaHamLuongVang,
                    MaNhaCungCap = sanPham.MaNhaCungCap,
                    MaNhanVien = Form1.nhanVien.MaNhanVien
                };

                if (!SanPhamKhoCuBUS.Instance.Insert(sanPhamCatVang, 1))
                {
                    PopupService.Instance.Error(MessageError.CAT_VANG_INSERT);
                    return;
                }

                sanPham.KhoiLuongTinh -= khoiLuongTinhCat;
                sanPham.KhoiLuongDa -= khoiLuongDaCat;
                sanPham.KhoiLuongVang -= khoiLuongVangCat;

                List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();

                foreach (var giaVangTheoLoai in listGiaVangHienTai)
                {
                    if (sanPham.HamLuongVang.Equals(giaVangTheoLoai.KiHieuHamLuongVang.HamLuongVang))
                    {
                        int giaBan = (int)Math.Round(sanPham.KhoiLuongVang * (giaVangTheoLoai.GiaVangBanRa / 100.0) + sanPham.TienCong);

                        sanPham.GiaBan = TienUtil.LamTronTienViet(giaBan);
                    }
                }

                if (SanPhamBUS.Instance.UpdateSanPham(sanPham) == null)
                {
                    PopupService.Instance.Error(MessageError.CAT_VANG_UPDATE);
                    return;
                }

                PopupService.Instance.Success(MessageSucces.CAT_VANG);
            }
        }

        private void grvSanPham_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
            listSanPhamBanRa.Clear();
            ReloadAllGridControl();

            //Tổng tiền
            txtThanhTien.EditValue = null;
            txtTienBot.EditValue = null;
            txtDaCat.Text = "0";
            txtTinhCat.Text = "0";
            txtVangCat.Text = "0";
            txtMaTem.Text = "";
        }

        private void txtTienBot_EditValueChanged(object sender, EventArgs e)
        {
            TinhToanLichSuBan();
        }

        private void grvSanPham_RowCountChanged(object sender, EventArgs e)
        {
            HienThiTongSoSanPham();
        }

        private void txtTinhCat_EditValueChanged(object sender, EventArgs e)
        {
            if (txtTinhCat.EditValue != null && txtDaCat.EditValue != null)
            {
                txtVangCat.EditValue = Math.Round(double.Parse(txtTinhCat.Text) - double.Parse(txtDaCat.Text), 1);
            }
        }

        private void txtDaCat_EditValueChanged(object sender, EventArgs e)
        {
            if (txtTinhCat.EditValue != null && txtDaCat.EditValue != null)
            {
                txtVangCat.EditValue = Math.Round(double.Parse(txtTinhCat.Text) - double.Parse(txtDaCat.Text), 1);
            }
        }

        private void txtMaTem_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtMaTem.Text))
            {
                SanPham sanPham = listSanPhamChuaBan.FirstOrDefault(x => x.MaTem == txtMaTem.Text);

                if (sanPham != null)
                {
                    HienThiChiTietSanPham(sanPham);
                }
                else
                {
                    ClearFormThongTinSanPham();
                }
            }
        }
    }
}
