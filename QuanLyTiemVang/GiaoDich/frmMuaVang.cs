﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using QuanLyTiemVang.Constants.StringUltils;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.BluePrints;
using DTO.TemporaryModel;
using DevExpress.XtraReports.UI;
using Share.Util;
using Share.Constant;

namespace QuanLyTiemVang.GiaoDich
{
    public partial class frmMuaVang : DevExpress.XtraEditors.XtraForm
    {
        private List<SanPhamKhoCu> listVangMuaVao = new List<SanPhamKhoCu>(); // Sản phẩm trong hóa đơn vàng mua vào

        private LichSuMua lichSuMua = null; // Lịch sử mua vàng hiện tại

        public frmMuaVang()
        {
            InitializeComponent();
        }

        private void frmMuaVang_Load(object sender, EventArgs e)
        {
            SetValueDefault();
            GetNewSoHoaDon();
            SetDataSourceForAllGridControl();
            LoadData();
            ReloadAllGridControl();
        }

        private void GetNewSoHoaDon()
        {
            txtSoHoaDon.Text = Util.TaoMaTheoNamThangNgayGioPhutGiay();
        }

        private void LoadKhachHang()
        {
            KhachHangBUS.Instance.GetLookupEdit(lueKhachHang);
        }

        private void SetValueDefault()
        {
            detNgayNhap.DateTime = DateTime.Now;
            txtNhanVien.Text = Form1.nhanVien.Ten;
            txtTenSanPham.Focus();
        }

        private void SetDataSourceForAllGridControl()
        {
            grcHoaDonMua.DataSource = listVangMuaVao;
        }

        private void ReloadAllGridControl()
        {
            grcHoaDonMua.RefreshDataSource();
        }

        private void LoadData()
        {
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVang);
            LoadKhachHang();
            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
        }

        private void ClearFormSanPham()
        {
            txtTenSanPham.Text = null;
            lueHamLuongVang.EditValue = null;
            txtTiLeHot.EditValue = null;

            txtChiTinh.EditValue = null;
            txtPhanTinh.EditValue = null;
            txtLyTinh.EditValue = null;

            txtChiDa.EditValue = null;
            txtPhanDa.EditValue = null;
            txtLyDa.EditValue = null;

            txtChiVang.EditValue = null;
            txtPhanVang.EditValue = null;
            txtLyVang.EditValue = null;

            txtGiaMua.EditValue = null;
            txtDonGiaMua.EditValue = null;
        }

        private bool IsValidSanPham()
        {
            dxerHamLuongVang.Dispose();
            dxerTiLeHot.Dispose();
            dxerLyDa.Dispose();
            dxerLyTinh.Dispose();
            dxerLyVang.Dispose();
            dxerTenSanPham.Dispose();
            dxerGiaMua.Dispose();

            bool check = true;
            double outValue;

            if (lueHamLuongVang.EditValue == null)
            {
                dxerHamLuongVang.SetError(lueHamLuongVang, ValidationUltils.HAM_LUONG_VANG);
                check = false;
            }

            if (!double.TryParse(txtLyTinh.Text, out outValue))
            {
                dxerLyTinh.SetError(txtLyTinh, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyDa.Text, out outValue))
            {
                dxerLyDa.SetError(txtLyDa, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyVang.Text, out outValue))
            {
                dxerLyVang.SetError(txtLyVang, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtTiLeHot.Text, out outValue))
            {
                dxerTiLeHot.SetError(txtTiLeHot, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(txtTenSanPham.Text))
            {
                dxerTenSanPham.SetError(txtTenSanPham, ValidationUltils.TEN_SAN_PHAM);
                check = false;
            }

            if (txtGiaMua.EditValue == null || string.IsNullOrWhiteSpace(txtGiaMua.Text))
            {
                dxerGiaMua.SetError(txtGiaMua, ValidationUltils.GIA_MUA);
                check = false;
            }

            return check;
        }

        private void TinhToanLichSuMua()
        {
            lichSuMua = MuaVangService.Instance.TinhToanLichSuMua(listVangMuaVao, txtTienCongThem, lueKhachHang);
            HienThiThanhTienHoaDon();
        }

        private void HienThiThanhTienHoaDon()
        {
            if (lichSuMua != null)
            {
                txtThanhTien.EditValue = lichSuMua.ThanhTienHoaDon;
            }
        }

        private void ClearAll()
        {
            ClearFormSanPham();
            listVangMuaVao.Clear();
            ReloadAllGridControl();
        }

        private void btnNhapLaiSanPham_Click(object sender, EventArgs e)
        {
            ClearFormSanPham();
        }

        private void btnThemVaoHoaDon_Click(object sender, EventArgs e)
        {
            if (!IsValidSanPham())
            {
                PopupService.Instance.Error(ErrorUltils.ENOUGH_DATA);
                return;
            }
            else
            {
                List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();

                SanPhamKhoCu sanPhamKhoCu = new SanPhamKhoCu
                {
                    TenSanPham = txtTenSanPham.Text,
                    MaHamLuongVang = (int)lueHamLuongVang.EditValue,
                    LoaiVangMuaVao = Constant.VANG_CU_MUA_VAO,
                    KhoiLuongTinh = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiTinh, txtPhanTinh, txtLyTinh),
                    KhoiLuongDa = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiDa, txtPhanDa, txtLyDa),
                    KhoiLuongVang = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiVang, txtPhanVang, txtLyVang),
                    GiaMuaVao = string.IsNullOrWhiteSpace(txtGiaMua.EditValue.ToString()) ? 0 : int.Parse(txtGiaMua.EditValue.ToString()),
                    DonGiaMuaVao = listGiaVangHienTai.FirstOrDefault(x => x.MaHamLuongVang == (int)lueHamLuongVang.EditValue).GiaVangMuaVao,
                    TiLeHot = txtTiLeHot.EditValue == null ? 0 : double.Parse(txtTiLeHot.EditValue.ToString()),
                    KiHieuHamLuongVang = (KiHieuHamLuongVang)lueHamLuongVang.GetSelectedDataRow(),
                    MaNhanVien = Form1.nhanVien.MaNhanVien
                };

                listVangMuaVao.Add(sanPhamKhoCu);
                ReloadAllGridControl();
                TinhToanLichSuMua();
                ClearFormSanPham();
            }
        }

        #region Tính KVL khi nhập KLĐ và KLT
        double tongKhoiLuongTinh = 0;
        double tongKhoiLuongDa = 0;

        private void txtChiTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);
        }

        private void txtPhanTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);
        }

        private void txtLyTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);
        }

        private void txtChiDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);
        }

        private void txtPhanDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);
        }

        private void txtLyDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);
        }
        #endregion

        private void btnXoa_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            SanPhamKhoCu sanPhamDelete = (SanPhamKhoCu)grvHoaDonMua.GetFocusedRow();
            listVangMuaVao.Remove(sanPhamDelete);
            ReloadAllGridControl();
            TinhToanLichSuMua();
        }

        private void InHoaDonMuaVang(List<SanPhamKhoCu> listVangMuaVao)
        {
            HoaDonMuaVangPrint hoaDonIn = new HoaDonMuaVangPrint();
            List<HoaDonMuaVang> hoaDonMuaVangs = MuaVangService.Instance.GetListHoaDonMuaVang(
                listVangMuaVao,
                lueKhachHang,
                lichSuMua);

            hoaDonIn.DataSource = hoaDonMuaVangs;
            hoaDonIn.PageHeight = 670 + 45 * hoaDonMuaVangs.Count;
            hoaDonIn.ShowPreviewDialog();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            var result = PopupService.Instance.Question(MessageWarning.THANH_TOAN_MUA_VANG);

            if (result == DialogResult.No)
            {
                return;
            }

            try
            {
                if (listVangMuaVao.Count == 0)
                {
                    PopupService.Instance.Error(ValidationUltils.LOAI_VANG_MUA_VAO);
                    return;
                }

                string maKhachHang = null;

                if (lueKhachHang.EditValue != null)
                {
                    maKhachHang = lueKhachHang.EditValue.ToString();
                }

                lichSuMua.MaLichSuMua = txtSoHoaDon.Text;

                LichSuMua lichSuMuaThanhToan = LichSuMuaBUS.Instance.ThanhToanHoaDon(lichSuMua, listVangMuaVao);

                if (lichSuMuaThanhToan != null)
                {
                    InHoaDonMuaVang(listVangMuaVao);

                    ClearAll();
                    GetNewSoHoaDon();
                    txtTienCongThem.EditValue = null;
                    txtThanhTien.EditValue = null;
                    AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
                    PopupService.Instance.Success(MessageSucces.GIAO_DICH_SUCCES);
                }
            }
            catch (Exception er)
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR + er.Message);
            }
        }

        private void btnNhapLaiHoaDon_Click(object sender, EventArgs e)
        {
            listVangMuaVao.Clear();
            ReloadAllGridControl();
            txtTienCongThem.EditValue = null;
            txtThanhTien.EditValue = null;
        }

        private void lueHamLuongVang_EditValueChanged(object sender, EventArgs e)
        {
            if (lueHamLuongVang.EditValue != null)
            {
                Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVang,
                tongKhoiLuongTinh,
                tongKhoiLuongDa);

                List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();
                int? donGiaMua = listGiaVangHienTai.FirstOrDefault(x => x.MaHamLuongVang == (int)lueHamLuongVang.EditValue).GiaVangBanRa;

                txtDonGiaMua.EditValue = donGiaMua.HasValue ? donGiaMua.Value : 0;
            }
        }

        private void txtTienCongThem_EditValueChanged(object sender, EventArgs e)
        {
            TinhToanLichSuMua();
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }
    }
}