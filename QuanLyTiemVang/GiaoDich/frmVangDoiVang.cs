﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.Models;
using BUS;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;
using DevExpress.XtraReports.UI;
using DTO.TemporaryModel;
using QuanLyTiemVang.BluePrints;
using Share.Util;
using Share.Constant;

namespace QuanLyTiemVang.VANGDOIVANG
{
    public partial class frmVangDoiVang : DevExpress.XtraEditors.XtraForm
    {
        #region Khai báo
        private List<SanPham> listSanPhamChuaBan = new List<SanPham>();

        private List<SanPham> listVangTrongHoaDonBan = new List<SanPham>(); // Sản phẩm trong danh sách hóa đơn bán ra

        private List<SanPham> listVangBanRa = new List<SanPham>(); // Danh sách vàng bán ra

        private List<SanPham> listVangDoiNgangRa = new List<SanPham>(); // Danh sách vàng đổi ngang ra

        private List<SanPhamKhoCu> listVangMuaVao = new List<SanPhamKhoCu>(); // Sản phẩm trong mục vàng vào

        LichSuDoiVang lichSuDoiVang = new LichSuDoiVang(); // Lịch sử đổi vàng đang thực hiện
        #endregion

        public frmVangDoiVang()
        {
            InitializeComponent();
            grvDanhSachSanPhamTaiKhoMoi.IndicatorWidth = 70;
        }

        private void frmVangDoiVang_Load(object sender, EventArgs e)
        {
            SetDataSourceForAllGridControl();
            GetNewSoHoaDon();
            LoadData();
        }

        private void LoadData()
        {
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVangCu);
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVangMoi);
            SanPhamBUS.Instance.GetLookupEditOfLoaiVangMuaVao(lueLoaiVangMuaVao);
            SanPhamBUS.Instance.GetLookupEditOfLoaiVangBanRa(lueLoaiVangBanRa);
            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPhamCu);
            LoadKhachHang();
            SetDefaultValueForForm();
            GetDataForGridSanPhamChuaBan();
            ReloadAllGridControl();
        }

        private void LoadKhachHang()
        {
            KhachHangBUS.Instance.GetList(lueKhachHang);
        }

        private void GetNewSoHoaDon()
        {
            txtSoHoaDon.Text = Util.TaoMaTheoNamThangNgayGioPhutGiay();
        }

        private void SetDefaultValueForForm()
        {
            detNgayBan.DateTime = DateTime.Now;
            txtNhanVien.Text = Form1.nhanVien.Ten;

            txtMaTemMoi.Focus();
        }

        private void SetDataSourceForAllGridControl()
        {
            grcDanhSachSanPhamTaiKhoMoi.DataSource = listSanPhamChuaBan;
            grcVangRa.DataSource = listVangTrongHoaDonBan;
            grcVangVao.DataSource = listVangMuaVao;
        }

        private void GetDataForGridSanPhamChuaBan()
        {
            List<SanPham> listSanPhamTemp = SanPhamBUS.Instance.GetListSanPhamChuaBan();

            listSanPhamChuaBan.Clear();

            foreach (var sanPham in listSanPhamTemp)
            {
                listSanPhamChuaBan.Add(sanPham);
            }
        }

        private void ReloadAllGridControl()
        {
            grcDanhSachSanPhamTaiKhoMoi.RefreshDataSource();
            grcVangRa.RefreshDataSource();
            grcVangVao.RefreshDataSource();
        }

        private void grcDanhSachSanPhamTaiKhoMoi_DoubleClick(object sender, EventArgs e)
        {
            SanPham sanPham = (SanPham)grvDanhSachSanPhamTaiKhoMoi.GetFocusedRow();
            HienThiChiTietSanPhamMoi(sanPham);
        }

        private void HienThiChiTietSanPhamMoi(SanPham sanPham)
        {
            try
            {
                btnThemSanPhamMoiVaoHoaDon.Tag = sanPham;

                txtMaTemMoi.Text = sanPham.MaTem;
                txtTenSanPhamMoi.Text = sanPham.TenSanPham;
                lueHamLuongVangMoi.Text = sanPham.HamLuongVang;
                txtTiLeHotMoi.Text = sanPham.TiLeHot.ToString();
                txtGiaBan.EditValue = sanPham.GiaBan;
                txtTienCong.EditValue = sanPham.TienCong;
                txtTiLeHotMoi.EditValue = sanPham.TiLeHot;

                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiVangMoi, txtPhanVangMoi, txtLyVangMoi);

                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiTinhMoi, txtPhanTinhMoi, txtLyTinhMoi);

                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiDaMoi, txtPhanDaMoi, txtLyDaMoi);
            }
            catch
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
            }
        }

        private void btnNhapLai_Click(object sender, EventArgs e)
        {
            NhapLaiSanPhamMoi();
        }

        private void NhapLaiSanPhamMoi()
        {
            btnThemSanPhamMoiVaoHoaDon.Tag = null;
            lueHamLuongVangMoi.EditValue = null;
            txtMaTemMoi.Text = null;
            txtTenSanPhamMoi.Text = null;
            txtTiLeHotMoi.Text = null;
            txtGiaBan.Text = null;
            txtTienCong.Text = null;

            txtChiTinhMoi.Text = null;
            txtPhanTinhMoi.Text = null;
            txtLyTinhMoi.Text = null;

            txtChiDaMoi.Text = null;
            txtPhanDaMoi.Text = null;
            txtLyDaMoi.Text = null;

            txtChiVangMoi.Text = null;
            txtPhanVangMoi.Text = null;
            txtLyVangMoi.Text = null;
        }

        private bool IsValidSanPhamMoi()
        {
            dxerLoaiVangBanRa.Dispose();

            bool check = true;

            if (lueLoaiVangBanRa.EditValue == null)
            {
                dxerLoaiVangBanRa.SetError(lueLoaiVangBanRa, ValidationUltils.LOAI_VANG_BAN_RA);
                check = false;
            }

            return check;
        }

        private void btnThemSanPhamMoiVaoHoaDon_Click(object sender, EventArgs e)
        {
            if (!IsValidSanPhamMoi())
            {
                return;
            }

            if (btnThemSanPhamMoiVaoHoaDon.Tag != null)
            {
                SanPham sanPham = (SanPham)btnThemSanPhamMoiVaoHoaDon.Tag;

                if (listVangDoiNgangRa.Count > 0)
                {
                    if (sanPham.KiHieuHamLuongVang.HamLuongVang.Equals(listVangDoiNgangRa.First().KiHieuHamLuongVang.HamLuongVang))
                    {
                        if (Convert.ToInt32(lueLoaiVangBanRa.EditValue) == Constant.VANG_DOI_NGANG_RA)
                        {
                            listVangDoiNgangRa.Add(sanPham);
                        }
                        else
                        {
                            PopupService.Instance.Warning(ValidationUltils.VANG_BAN_RA_TRUNG_HAM_LUONG_VANG_DOI_RA);
                            return;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(lueLoaiVangBanRa.EditValue) == Constant.VANG_DOI_NGANG_RA)
                        {
                            PopupService.Instance.Warning(ValidationUltils.KHONG_TRUNG_HAM_LUONG_VANG);
                            return;
                        }
                        else
                        {
                            listVangBanRa.Add(sanPham);
                        }
                    }
                }
                else if (listVangDoiNgangRa.Count == 0 && listVangBanRa.Count > 0)
                {
                    bool isDuplicate = false;

                    foreach (SanPham vangBanRa in listVangBanRa)
                    {
                        if (sanPham.KiHieuHamLuongVang.HamLuongVang.Equals(vangBanRa.KiHieuHamLuongVang.HamLuongVang))
                        {
                            isDuplicate = true;
                            break;
                        }
                    }

                    if (isDuplicate)
                    {
                        if (Convert.ToInt32(lueLoaiVangBanRa.EditValue) == Constant.VANG_DOI_NGANG_RA)
                        {
                            PopupService.Instance.Warning(ValidationUltils.VANG_DOI_RA_TRUNG_HLV_BAN_RA);
                            return;
                        }
                        else
                        {
                            listVangBanRa.Add(sanPham);
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(lueLoaiVangBanRa.EditValue) == Constant.VANG_DOI_NGANG_RA)
                        {
                            listVangDoiNgangRa.Add(sanPham);
                        }
                        else
                        {
                            listVangBanRa.Add(sanPham);
                        }
                    }
                }
                else
                {
                    if (Convert.ToInt32(lueLoaiVangBanRa.EditValue) == Constant.VANG_DOI_NGANG_RA)
                    {
                        listVangDoiNgangRa.Add(sanPham);
                    }
                    else
                    {
                        listVangBanRa.Add(sanPham);
                    }
                }
                
                listVangTrongHoaDonBan.Add(sanPham);
                HienThiThongTinDoiVangLenForm();
                listSanPhamChuaBan.Remove(sanPham);
                NhapLaiSanPhamMoi();
                ReloadAllGridControl();
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }

        }

        private void ClearFormDoiVang()
        {
            txtKLVMoiTruKLVCu.EditValue = null;
            txtGiaBanMua.EditValue = null;
            txtThanhTienVangDoiVang.EditValue = null;
            txtTongTienVangCu.EditValue = null;
            txtTienConLai.EditValue = null;
            txtCuoiCung.EditValue = null;
        }

        private LichSuDoiVang LayThongTinTuVangDoiVangTuForm(bool tienCongEdited = false)
        {
            if (listVangTrongHoaDonBan.Count == 0 || listVangMuaVao.Count == 0)
            {
                return null;
            }

            return VangDoiVangService.Instance.TinhToanVangDoiVang(
                listVangBanRa,
                listVangDoiNgangRa,
                listVangMuaVao,
                txtTienCongThem,
                txtTienBot,
                tienCongEdited);
        }

        private void HienThiThongTinDoiVangLenForm(bool tienCongEdited = false)
        {
            lichSuDoiVang = LayThongTinTuVangDoiVangTuForm(tienCongEdited);

            if (lichSuDoiVang == null)
            {
                ClearFormDoiVang();
                return;
            }

            txtKLVMoiTruKLVCu.Text = lichSuDoiVang.KhoiLuongVangMoi +
                " - " + lichSuDoiVang.KhoiLuongVangCu +
                " = " + lichSuDoiVang.KLVMoiTruKLVCu;
            txtGiaBanMua.EditValue = lichSuDoiVang.GiaBan;
            txtThanhTienVangDoiVang.EditValue = lichSuDoiVang.ThanhTienVangMoi;
            txtTongTienVangCu.EditValue = lichSuDoiVang.TongTienVangCu;
            txtTienConLai.EditValue = lichSuDoiVang.TienConLai;
            txtCuoiCung.EditValue = lichSuDoiVang.ThuTienKhachHang;
            txtTienCongThem.EditValue = lichSuDoiVang.TienCongThem;
            txtTongTienVangBanRa.EditValue = lichSuDoiVang.TongTienVangBanRa;
        }

        private bool IsValidSanPhamCu()
        {
            dxerHamLuongVang.Dispose();
            dxerTiLeHot.Dispose();
            dxerLyDaCu.Dispose();
            dxerLyTinhCu.Dispose();
            dxerLyVangCu.Dispose();
            dxerTenSanPhamCu.Dispose();
            dxerLoaiVangMuaVao.Dispose();

            bool check = true;
            double outValue;

            if (lueHamLuongVangCu.EditValue == null)
            {
                dxerHamLuongVang.SetError(lueHamLuongVangCu, ValidationUltils.HAM_LUONG_VANG);
                check = false;
            }

            if (lueLoaiVangMuaVao.EditValue == null)
            {
                dxerLoaiVangMuaVao.SetError(lueLoaiVangMuaVao, ValidationUltils.LOAI_VANG_MUA_VAO);
                check = false;
            }

            if (!double.TryParse(txtLyTinhCu.Text, out outValue))
            {
                dxerLyTinhCu.SetError(txtLyTinhCu, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyDaCu.Text, out outValue))
            {
                dxerLyDaCu.SetError(txtLyDaCu, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyVangCu.Text, out outValue))
            {
                dxerLyVangCu.SetError(txtLyVangCu, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtTiLeHotCu.Text, out outValue))
            {
                dxerTiLeHot.SetError(txtTiLeHotCu, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(txtTenSanPhamCu.Text))
            {
                dxerTenSanPhamCu.SetError(txtTenSanPhamCu, ValidationUltils.TEN_SAN_PHAM);
                check = false;
            }

            return check;
        }

        private void btnThemSanPhamCuVaoHoaDon_Click(object sender, EventArgs e)
        {
            if (!IsValidSanPhamCu())
            {
                PopupService.Instance.Error(ErrorUltils.ENOUGH_DATA);
                return;
            }
            else
            {
                List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();

                SanPhamKhoCu sanPhamKhoCu = new SanPhamKhoCu
                {
                    TenSanPham = txtTenSanPhamCu.Text,
                    MaHamLuongVang = (int)lueHamLuongVangCu.EditValue,
                    LoaiVangMuaVao = (int)lueLoaiVangMuaVao.EditValue,
                    KhoiLuongTinh = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiTinhCu, txtPhanTinhCu, txtLyTinhCu),
                    KhoiLuongDa = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiDaCu, txtPhanDaCu, txtLyDaCu),
                    KhoiLuongVang = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiVangCu, txtPhanVangCu, txtLyVangCu),
                    GiaMuaVao = string.IsNullOrWhiteSpace(txtGiaMua.EditValue.ToString()) ? 0 : int.Parse(txtGiaMua.EditValue.ToString()),
                    DonGiaMuaVao = listGiaVangHienTai.FirstOrDefault(x => x.MaHamLuongVang == (int)lueHamLuongVangCu.EditValue).GiaVangMuaVao,
                    TiLeHot = txtTiLeHotCu.EditValue == null ? 0 : double.Parse(txtTiLeHotCu.EditValue.ToString()),
                    KiHieuHamLuongVang = (KiHieuHamLuongVang)lueHamLuongVangCu.GetSelectedDataRow(),
                    MaNhanVien = Form1.nhanVien.MaNhanVien
                };

                listVangMuaVao.Add(sanPhamKhoCu);
                HienThiThongTinDoiVangLenForm();
                ReloadAllGridControl();
                NhapLaiSanPhamCu();
            }
        }

        private void btnNhapLaiSanPhamCu_Click(object sender, EventArgs e)
        {
            NhapLaiSanPhamCu();
        }

        private void NhapLaiSanPhamCu()
        {
            txtTenSanPhamCu.Text = null;
            txtTiLeHotCu.Text = null;
            txtGiaMua.Text = null;

            txtChiVangCu.Text = null;
            txtPhanVangCu.Text = null;
            txtLyVangCu.Text = null;

            txtChiDaCu.Text = null;
            txtPhanDaCu.Text = null;
            txtLyDaCu.Text = null;

            txtChiTinhCu.Text = null;
            txtPhanTinhCu.Text = null;
            txtLyTinhCu.Text = null;
        }

        private void XoaAllSanPhamDoiNgangVao()
        {
            for (int i = 0; i < listVangMuaVao.Count; i++)
            {
                if (listVangMuaVao[i].LoaiVangMuaVao == Constant.VANG_CU_DOI_NGANG)
                {
                    listVangMuaVao.Remove(listVangMuaVao[i]);
                    i--;
                }
            }
        }

        private void InHoaDonDoiVang(
            List<SanPham> listVangBanRa,
            List<SanPhamKhoCu> listVangMuaVao,
            LichSuDoiVang lichSuDoiVang)
        {
            HoaDonVangDoiVangPrint hoaDonIn = new HoaDonVangDoiVangPrint();
            List<HoaDonVangDoiVang> hoaDonBanVangs = VangDoiVangService.Instance.ConvertToListHoaDonBanVang(
                listVangBanRa,
                listVangMuaVao,
                lichSuDoiVang,
                lueKhachHang);

            hoaDonIn.DataSource = hoaDonBanVangs;
            hoaDonIn.ShowPreviewDialog();
        }

        private bool IsValidHamLuongVangDoiNgang()
        {
            for (int i = 0; i < listVangDoiNgangRa.Count - 1; i++)
            {
                if (!listVangDoiNgangRa[i].HamLuongVang.Equals(listVangDoiNgangRa[i + 1].HamLuongVang))
                {
                    return false;
                }
            }

            List<KiHieuHamLuongVang> listHamLuongVang = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang().Where(x => x.HamLuongVang.Equals(listVangDoiNgangRa.First().HamLuongVang)).ToList();

            KiHieuHamLuongVang hamLuongVangDoiNgang = listHamLuongVang.FirstOrDefault();

            if (hamLuongVangDoiNgang == null)
            {
                return false;
            }

            foreach (SanPhamKhoCu sanPham in listVangMuaVao)
            {
                if (sanPham.LoaiVangMuaVao == Constant.VANG_CU_DOI_NGANG && sanPham.MaHamLuongVang != hamLuongVangDoiNgang.MaHamLuongVang)
                {
                    return false;
                }
            }

            return true;
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            var result = PopupService.Instance.Question(MessageWarning.THANH_TOAN_DOI_VANG);

            if (result == DialogResult.No)
            {
                return;
            }

            try
            {
                if (listVangTrongHoaDonBan.Count == 0 || listVangMuaVao.Count == 0)
                {
                    PopupService.Instance.Warning(ValidationUltils.DANH_SACH_VANG_DOI_VANG);
                    return;
                }

                if ((listVangMuaVao.Count(x => x.LoaiVangMuaVao == Constant.VANG_CU_DOI_NGANG) > 0 && listVangDoiNgangRa.Count > 0) ||
                    (listVangMuaVao.Count(x => x.LoaiVangMuaVao == Constant.VANG_CU_DOI_NGANG) == 0 && listVangDoiNgangRa.Count == 0))
                {
                    // tiếp tục chạy code phía dưới, còn sai thì chạy vào else và dừng lại
                }
                else
                {
                    PopupService.Instance.Warning(ValidationUltils.VALID_DANH_SACH_DOI_NGANG_RA_VAO);
                    return;
                }

                if (listVangDoiNgangRa.Count > 0 && !IsValidHamLuongVangDoiNgang())
                {
                    PopupService.Instance.Warning(ValidationUltils.KHONG_TRUNG_HAM_LUONG_VANG);
                    return;
                }

                lichSuDoiVang.MaLichSuDoiVang = lichSuDoiVang.MaLichSuBan = lichSuDoiVang.MaLichSuMua = txtSoHoaDon.Text;

                LichSuDoiVang lichSuDoiVangDaThanhToan = LichSuDoiVangBUS.Instance.ThanhToanHoaDon(
                    lichSuDoiVang,
                    listVangTrongHoaDonBan,
                    listVangMuaVao,
                    lueKhachHang,
                    Form1.nhanVien.MaNhanVien);

                if (lichSuDoiVangDaThanhToan != null)
                {
                    InHoaDonDoiVang(listVangTrongHoaDonBan, listVangMuaVao, lichSuDoiVang);
                    ClearAll();
                    LoadData();
                    txtTienCongThem.EditValue = null;
                    txtTienBot.EditValue = null;
                    GetNewSoHoaDon();
                    PopupService.Instance.Success(MessageSucces.GIAO_DICH_SUCCES);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.INSERT_ERROR);
                }
            }
            catch (Exception er)
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR + er.Message);
            }
        }

        /// <summary>
        /// Xóa một mặt hàng ra khỏi danh sách sản phẩm ra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            SanPham sanPham = (SanPham)grvVangRa.GetFocusedRow();

            if (listVangDoiNgangRa.Count == 1 && listVangDoiNgangRa.IndexOf(sanPham) != -1)
            {
                if (PopupService.Instance.Question(MessageQuestion.XOA_MOI_SAN_PHAM_VANG_DOI_RA) == DialogResult.No)
                {
                    return;
                }
            }

            listVangTrongHoaDonBan.Remove(sanPham);
            listVangDoiNgangRa.Remove(sanPham);
            listVangBanRa.Remove(sanPham);
            listSanPhamChuaBan.Add(sanPham);

            if (listVangDoiNgangRa.Count == 0)
            {
                XoaAllSanPhamDoiNgangVao();
                lueLoaiVangMuaVao.EditValue = null;
                lueHamLuongVangCu.EditValue = null;
                lueHamLuongVangCu.ReadOnly = false;
            }

            HienThiThongTinDoiVangLenForm();
            ReloadAllGridControl();
        }

        private void btnXoaKhoiMatHangCu_Click(object sender, EventArgs e)
        {
            SanPhamKhoCu sanPham = (SanPhamKhoCu)grvVangVao.GetFocusedRow();
            listVangMuaVao.Remove(sanPham);
            HienThiThongTinDoiVangLenForm();
            ReloadAllGridControl();
        }

        private void btnNhapLaiHoaDon_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void ClearAll()
        {
            NhapLaiSanPhamMoi();
            NhapLaiSanPhamCu();
            listVangMuaVao.Clear();
            listSanPhamChuaBan.AddRange(listVangTrongHoaDonBan);
            listVangTrongHoaDonBan.Clear();
            listVangBanRa.Clear();
            listVangDoiNgangRa.Clear();
            HienThiThongTinDoiVangLenForm();
            ReloadAllGridControl();
        }

        private void txtMaTem_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtMaTemMoi.Text))
            {
                SanPham sanPham = listSanPhamChuaBan.Find(x => x.MaTem == txtMaTemMoi.Text);

                if (sanPham != null)
                {
                    HienThiChiTietSanPhamMoi(sanPham);
                }
                else
                {
                    NhapLaiSanPhamMoi();
                }
            }
        }

        #region Tính KVL khi nhập KLĐ và KLT
        double tongKhoiLuongTinhCu = 0;
        double tongKhoiLuongDaCu = 0;

        private void txtChiTinhCu_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinhCu = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinhCu, txtPhanTinhCu, txtLyTinhCu);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                txtChiVangCu, txtPhanVangCu, txtLyVangCu);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVangCu,
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                lueLoaiVangMuaVao);
        }

        private void txtPhanTinhCu_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinhCu = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinhCu, txtPhanTinhCu, txtLyTinhCu);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                txtChiVangCu, txtPhanVangCu, txtLyVangCu);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVangCu,
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                lueLoaiVangMuaVao);
        }

        private void txtLyTinhCu_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinhCu = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinhCu, txtPhanTinhCu, txtLyTinhCu);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                txtChiVangCu, txtPhanVangCu, txtLyVangCu);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVangCu,
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                lueLoaiVangMuaVao);
        }

        private void txtChiDaCu_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDaCu = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDaCu, txtPhanDaCu, txtLyDaCu);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                txtChiVangCu, txtPhanVangCu, txtLyVangCu);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVangCu,
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                lueLoaiVangMuaVao);
        }

        private void txtPhanDaCu_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDaCu = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDaCu, txtPhanDaCu, txtLyDaCu);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                txtChiVangCu, txtPhanVangCu, txtLyVangCu);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVangCu,
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                lueLoaiVangMuaVao);
        }

        private void txtLyDaCu_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDaCu = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDaCu, txtPhanDaCu, txtLyDaCu);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu,
                txtChiVangCu, txtPhanVangCu, txtLyVangCu);
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                 txtGiaMua,
                 lueHamLuongVangCu,
                 tongKhoiLuongTinhCu,
                 tongKhoiLuongDaCu,
                 lueLoaiVangMuaVao);
        }
        #endregion

        private void lueHamLuongVangCu_EditValueChanged(object sender, EventArgs e)
        {
            Calculator.Instance.HienThiGiaMuaTheoKLTinhVaKLDa(
                txtGiaMua,
                lueHamLuongVangCu,
                tongKhoiLuongTinhCu,
                tongKhoiLuongDaCu);
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
            ReloadAllGridControl();
        }

        private void grvDanhSachSanPhamTaiKhoMoi_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void lueLoaiVangMuaVao_EditValueChanged(object sender, EventArgs e)
        {
            if (lueLoaiVangMuaVao.EditValue == null)
            {
                return;
            }

            if ((int)lueLoaiVangMuaVao.EditValue == Constant.VANG_CU_DOI_NGANG)
            {
                if (listVangDoiNgangRa.Count == 0)
                {
                    PopupService.Instance.Warning(ValidationUltils.NOT_EXIST_SAN_PHAM_DOI_RA);
                    lueLoaiVangMuaVao.EditValue = null;
                    return;
                }

                lueHamLuongVangCu.Text = listVangDoiNgangRa.First().HamLuongVang;
                lueHamLuongVangCu.ReadOnly = true;
            }
            else
            {
                lueHamLuongVangCu.EditValue = null;
                lueHamLuongVangCu.ReadOnly = false;
            }
        }

        private void lueHamLuongVangMoi_EditValueChanged(object sender, EventArgs e)
        {
            //if (lueLoaiVangMuaVao.EditValue != null && (int)lueLoaiVangMuaVao.EditValue == Constant.VANG_CU_DOI_NGANG)
            //{
            //    lueHamLuongVangCu.EditValue = lueHamLuongVangMoi.EditValue;
            //}
        }

        private void txtTienCongThem_EditValueChanged(object sender, EventArgs e)
        {
            if (ckeChinhSuaTienCong.Checked)
            {
                HienThiThongTinDoiVangLenForm(true);
            }
        }

        private void txtTienBot_EditValueChanged(object sender, EventArgs e)
        {
            HienThiThongTinDoiVangLenForm();
        }

        private void lueLoaiVangBanRa_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void ckeChinhSuaTienCong_CheckedChanged(object sender, EventArgs e)
        {
            if (ckeChinhSuaTienCong.Checked)
            {
                txtTienCongThem.ReadOnly = false;
            }
            else
            {
                txtTienCongThem.ReadOnly = true; 
            }
        }
    }
}