﻿namespace QuanLyTiemVang
{
    partial class frmBanVang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBanVang));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.grpHoaDon = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTienBot = new DevExpress.XtraEditors.TextEdit();
            this.lueKhachHang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.btnNhapLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.detNgayBan = new DevExpress.XtraEditors.DateEdit();
            this.grcHoaDon = new DevExpress.XtraGrid.GridControl();
            this.grvHoaDon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclTem_grcHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenSanPham_grcHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaBan_grcHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHanhDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDeleteItem = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.txtSoHoaDon = new DevExpress.XtraEditors.TextEdit();
            this.txtThanhTien = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.grpDanhSachSanPham = new DevExpress.XtraEditors.GroupControl();
            this.lbTongSoLuongSanPham = new System.Windows.Forms.Label();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.grcSanPham = new DevExpress.XtraGrid.GridControl();
            this.grvSanPham = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclTem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTiLeHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaChuyenKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grpThongTinSanPham = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTienCong = new DevExpress.XtraEditors.TextEdit();
            this.txtVangCat = new DevExpress.XtraEditors.TextEdit();
            this.txtDaCat = new DevExpress.XtraEditors.TextEdit();
            this.btnCatVang = new DevExpress.XtraEditors.SimpleButton();
            this.txtTinhCat = new DevExpress.XtraEditors.TextEdit();
            this.txtLyVang = new DevExpress.XtraEditors.TextEdit();
            this.txtChiDa = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDa = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDa = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinh = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLyTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtChiVang = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVang = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNhapLai = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemVaoHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.txtGiaBan = new DevExpress.XtraEditors.TextEdit();
            this.txtHamLuongVang = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHot = new DevExpress.XtraEditors.TextEdit();
            this.txtTenSanPham = new DevExpress.XtraEditors.TextEdit();
            this.txtMaTem = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnInTemMotSanPham = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.grpHoaDon)).BeginInit();
            this.grpHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHoaDon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachSanPham)).BeginInit();
            this.grpDanhSachSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinSanPham)).BeginInit();
            this.grpThongTinSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVangCat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaCat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhCat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            this.SuspendLayout();
            // 
            // grpHoaDon
            // 
            this.grpHoaDon.Controls.Add(this.layoutControl2);
            this.grpHoaDon.Dock = System.Windows.Forms.DockStyle.Right;
            this.grpHoaDon.Location = new System.Drawing.Point(909, 50);
            this.grpHoaDon.Margin = new System.Windows.Forms.Padding(4);
            this.grpHoaDon.Name = "grpHoaDon";
            this.grpHoaDon.Size = new System.Drawing.Size(435, 579);
            this.grpHoaDon.TabIndex = 200;
            this.grpHoaDon.Text = "Hóa đơn";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.txtTienBot);
            this.layoutControl2.Controls.Add(this.lueKhachHang);
            this.layoutControl2.Controls.Add(this.txtNhanVien);
            this.layoutControl2.Controls.Add(this.btnNhapLaiHoaDon);
            this.layoutControl2.Controls.Add(this.btnThanhToan);
            this.layoutControl2.Controls.Add(this.detNgayBan);
            this.layoutControl2.Controls.Add(this.grcHoaDon);
            this.layoutControl2.Controls.Add(this.txtSoHoaDon);
            this.layoutControl2.Controls.Add(this.txtThanhTien);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 25);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(431, 552);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txtTienBot
            // 
            this.txtTienBot.Location = new System.Drawing.Point(90, 453);
            this.txtTienBot.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienBot.Name = "txtTienBot";
            this.txtTienBot.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienBot.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienBot.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienBot.Properties.MaxLength = 9;
            this.txtTienBot.Size = new System.Drawing.Size(325, 22);
            this.txtTienBot.StyleController = this.layoutControl2;
            this.txtTienBot.TabIndex = 209;
            this.txtTienBot.EditValueChanged += new System.EventHandler(this.txtTienBot_EditValueChanged);
            // 
            // lueKhachHang
            // 
            this.lueKhachHang.Location = new System.Drawing.Point(90, 44);
            this.lueKhachHang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueKhachHang.Name = "lueKhachHang";
            this.lueKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKhachHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HoLot", "Họ lót"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Ten", "Tên")});
            this.lueKhachHang.Properties.NullText = "";
            this.lueKhachHang.Size = new System.Drawing.Size(122, 22);
            this.lueKhachHang.StyleController = this.layoutControl2;
            this.lueKhachHang.TabIndex = 208;
            // 
            // txtNhanVien
            // 
            this.txtNhanVien.Location = new System.Drawing.Point(292, 44);
            this.txtNhanVien.Margin = new System.Windows.Forms.Padding(4);
            this.txtNhanVien.Name = "txtNhanVien";
            this.txtNhanVien.Properties.ReadOnly = true;
            this.txtNhanVien.Size = new System.Drawing.Size(123, 22);
            this.txtNhanVien.StyleController = this.layoutControl2;
            this.txtNhanVien.TabIndex = 204;
            // 
            // btnNhapLaiHoaDon
            // 
            this.btnNhapLaiHoaDon.Location = new System.Drawing.Point(16, 509);
            this.btnNhapLaiHoaDon.Margin = new System.Windows.Forms.Padding(4);
            this.btnNhapLaiHoaDon.Name = "btnNhapLaiHoaDon";
            this.btnNhapLaiHoaDon.Size = new System.Drawing.Size(196, 27);
            this.btnNhapLaiHoaDon.StyleController = this.layoutControl2;
            this.btnNhapLaiHoaDon.TabIndex = 206;
            this.btnNhapLaiHoaDon.Text = "Nhập lại";
            this.btnNhapLaiHoaDon.Click += new System.EventHandler(this.btnNhapLaiHoaDon_Click);
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnThanhToan.Appearance.Options.UseForeColor = true;
            this.btnThanhToan.Location = new System.Drawing.Point(218, 509);
            this.btnThanhToan.Margin = new System.Windows.Forms.Padding(4);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(197, 27);
            this.btnThanhToan.StyleController = this.layoutControl2;
            this.btnThanhToan.TabIndex = 4;
            this.btnThanhToan.Text = "Thanh Toán";
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // detNgayBan
            // 
            this.detNgayBan.EditValue = null;
            this.detNgayBan.Location = new System.Drawing.Point(292, 16);
            this.detNgayBan.Margin = new System.Windows.Forms.Padding(4);
            this.detNgayBan.Name = "detNgayBan";
            this.detNgayBan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayBan.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayBan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.detNgayBan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayBan.Properties.ReadOnly = true;
            this.detNgayBan.Size = new System.Drawing.Size(123, 22);
            this.detNgayBan.StyleController = this.layoutControl2;
            this.detNgayBan.TabIndex = 203;
            // 
            // grcHoaDon
            // 
            this.grcHoaDon.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grcHoaDon.Location = new System.Drawing.Point(16, 72);
            this.grcHoaDon.MainView = this.grvHoaDon;
            this.grcHoaDon.Margin = new System.Windows.Forms.Padding(4);
            this.grcHoaDon.Name = "grcHoaDon";
            this.grcHoaDon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDeleteItem});
            this.grcHoaDon.Size = new System.Drawing.Size(399, 375);
            this.grcHoaDon.TabIndex = 205;
            this.grcHoaDon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvHoaDon});
            // 
            // grvHoaDon
            // 
            this.grvHoaDon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclTem_grcHoaDon,
            this.grclTenSanPham_grcHoaDon,
            this.grclGiaBan_grcHoaDon,
            this.grclHanhDong});
            this.grvHoaDon.GridControl = this.grcHoaDon;
            this.grvHoaDon.Name = "grvHoaDon";
            this.grvHoaDon.OptionsBehavior.ReadOnly = true;
            this.grvHoaDon.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvHoaDon.OptionsView.ShowGroupPanel = false;
            // 
            // grclTem_grcHoaDon
            // 
            this.grclTem_grcHoaDon.Caption = "Mã tem";
            this.grclTem_grcHoaDon.FieldName = "MaTem";
            this.grclTem_grcHoaDon.Name = "grclTem_grcHoaDon";
            this.grclTem_grcHoaDon.Visible = true;
            this.grclTem_grcHoaDon.VisibleIndex = 1;
            // 
            // grclTenSanPham_grcHoaDon
            // 
            this.grclTenSanPham_grcHoaDon.Caption = "Tên sản phẩm";
            this.grclTenSanPham_grcHoaDon.FieldName = "TenSanPham";
            this.grclTenSanPham_grcHoaDon.Name = "grclTenSanPham_grcHoaDon";
            this.grclTenSanPham_grcHoaDon.Visible = true;
            this.grclTenSanPham_grcHoaDon.VisibleIndex = 0;
            this.grclTenSanPham_grcHoaDon.Width = 73;
            // 
            // grclGiaBan_grcHoaDon
            // 
            this.grclGiaBan_grcHoaDon.Caption = "Giá Bán";
            this.grclGiaBan_grcHoaDon.DisplayFormat.FormatString = "n0";
            this.grclGiaBan_grcHoaDon.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaBan_grcHoaDon.FieldName = "GiaBan";
            this.grclGiaBan_grcHoaDon.Name = "grclGiaBan_grcHoaDon";
            this.grclGiaBan_grcHoaDon.Visible = true;
            this.grclGiaBan_grcHoaDon.VisibleIndex = 2;
            this.grclGiaBan_grcHoaDon.Width = 72;
            // 
            // grclHanhDong
            // 
            this.grclHanhDong.Caption = "Xóa";
            this.grclHanhDong.ColumnEdit = this.btnDeleteItem;
            this.grclHanhDong.Name = "grclHanhDong";
            this.grclHanhDong.Visible = true;
            this.grclHanhDong.VisibleIndex = 3;
            this.grclHanhDong.Width = 58;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.AutoHeight = false;
            this.btnDeleteItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xóa", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnDeleteItem.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnDeleteItem.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnDeleteItem_ButtonClick);
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(90, 16);
            this.txtSoHoaDon.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Properties.ReadOnly = true;
            this.txtSoHoaDon.Size = new System.Drawing.Size(122, 22);
            this.txtSoHoaDon.StyleController = this.layoutControl2;
            this.txtSoHoaDon.TabIndex = 201;
            // 
            // txtThanhTien
            // 
            this.txtThanhTien.Location = new System.Drawing.Point(90, 481);
            this.txtThanhTien.Margin = new System.Windows.Forms.Padding(4);
            this.txtThanhTien.Name = "txtThanhTien";
            this.txtThanhTien.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtThanhTien.Properties.Appearance.Options.UseForeColor = true;
            this.txtThanhTien.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtThanhTien.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThanhTien.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtThanhTien.Properties.ReadOnly = true;
            this.txtThanhTien.Size = new System.Drawing.Size(325, 22);
            this.txtThanhTien.StyleController = this.layoutControl2;
            this.txtThanhTien.TabIndex = 207;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.layoutControlItem8,
            this.layoutControlItem14,
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem11,
            this.layoutControlItem36});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(431, 552);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtSoHoaDon;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(202, 28);
            this.layoutControlItem7.Text = "Số hóa đơn";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(71, 17);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.grcHoaDon;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(405, 381);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.detNgayBan;
            this.layoutControlItem13.Location = new System.Drawing.Point(202, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(203, 28);
            this.layoutControlItem13.Text = "Ngày";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(71, 16);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnThanhToan;
            this.layoutControlItem8.Location = new System.Drawing.Point(202, 493);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(203, 33);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.btnNhapLaiHoaDon;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 493);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(202, 33);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtNhanVien;
            this.layoutControlItem9.Location = new System.Drawing.Point(202, 28);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(203, 28);
            this.layoutControlItem9.Text = "Nhân viên";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(71, 16);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtThanhTien;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 465);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(405, 28);
            this.layoutControlItem1.Text = "Thành tiền";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(71, 17);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.lueKhachHang;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(202, 28);
            this.layoutControlItem11.Text = "Khách hàng";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(71, 16);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.txtTienBot;
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 437);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(405, 28);
            this.layoutControlItem36.Text = "Tiền bớt";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(71, 17);
            // 
            // grpDanhSachSanPham
            // 
            this.grpDanhSachSanPham.Controls.Add(this.lbTongSoLuongSanPham);
            this.grpDanhSachSanPham.Controls.Add(this.layoutControl3);
            this.grpDanhSachSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDanhSachSanPham.Location = new System.Drawing.Point(2, 329);
            this.grpDanhSachSanPham.Margin = new System.Windows.Forms.Padding(4);
            this.grpDanhSachSanPham.Name = "grpDanhSachSanPham";
            this.grpDanhSachSanPham.Size = new System.Drawing.Size(899, 244);
            this.grpDanhSachSanPham.TabIndex = 100;
            this.grpDanhSachSanPham.Text = "Danh Sách sản phẩm";
            // 
            // lbTongSoLuongSanPham
            // 
            this.lbTongSoLuongSanPham.AutoSize = true;
            this.lbTongSoLuongSanPham.Location = new System.Drawing.Point(272, 5);
            this.lbTongSoLuongSanPham.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTongSoLuongSanPham.Name = "lbTongSoLuongSanPham";
            this.lbTongSoLuongSanPham.Size = new System.Drawing.Size(42, 17);
            this.lbTongSoLuongSanPham.TabIndex = 1;
            this.lbTongSoLuongSanPham.Text = "label4";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.grcSanPham);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 25);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(895, 217);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // grcSanPham
            // 
            this.grcSanPham.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grcSanPham.Location = new System.Drawing.Point(16, 16);
            this.grcSanPham.MainView = this.grvSanPham;
            this.grcSanPham.Margin = new System.Windows.Forms.Padding(4);
            this.grcSanPham.Name = "grcSanPham";
            this.grcSanPham.Size = new System.Drawing.Size(863, 185);
            this.grcSanPham.TabIndex = 102;
            this.grcSanPham.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPham});
            this.grcSanPham.DoubleClick += new System.EventHandler(this.grcSanPham_DoubleClick);
            // 
            // grvSanPham
            // 
            this.grvSanPham.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclTem,
            this.grclTenSanPham,
            this.grclHamLuongVang,
            this.grclTiLeHot,
            this.grclGiaBan,
            this.grclKhoiLuongTinh,
            this.grclKhoiLuongDa,
            this.grclKhoiLuongVang,
            this.MaChuyenKho});
            this.grvSanPham.GridControl = this.grcSanPham;
            this.grvSanPham.Name = "grvSanPham";
            this.grvSanPham.OptionsBehavior.Editable = false;
            this.grvSanPham.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPham.OptionsFind.AlwaysVisible = true;
            this.grvSanPham.OptionsFind.FindDelay = 100;
            this.grvSanPham.OptionsFind.FindNullPrompt = "";
            this.grvSanPham.OptionsFind.ShowFindButton = false;
            this.grvSanPham.OptionsView.ShowGroupPanel = false;
            this.grvSanPham.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvSanPham_CustomDrawRowIndicator);
            this.grvSanPham.RowCountChanged += new System.EventHandler(this.grvSanPham_RowCountChanged);
            // 
            // grclTem
            // 
            this.grclTem.Caption = "Mã tem";
            this.grclTem.FieldName = "MaTem";
            this.grclTem.Name = "grclTem";
            this.grclTem.Visible = true;
            this.grclTem.VisibleIndex = 1;
            this.grclTem.Width = 64;
            // 
            // grclTenSanPham
            // 
            this.grclTenSanPham.Caption = "Tên sản phẩm";
            this.grclTenSanPham.FieldName = "TenSanPham";
            this.grclTenSanPham.Name = "grclTenSanPham";
            this.grclTenSanPham.Visible = true;
            this.grclTenSanPham.VisibleIndex = 0;
            this.grclTenSanPham.Width = 83;
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "HLV";
            this.grclHamLuongVang.FieldName = "HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 2;
            this.grclHamLuongVang.Width = 42;
            // 
            // grclTiLeHot
            // 
            this.grclTiLeHot.Caption = "TLH";
            this.grclTiLeHot.FieldName = "TiLeHot";
            this.grclTiLeHot.Name = "grclTiLeHot";
            this.grclTiLeHot.Visible = true;
            this.grclTiLeHot.VisibleIndex = 3;
            this.grclTiLeHot.Width = 45;
            // 
            // grclGiaBan
            // 
            this.grclGiaBan.Caption = "Giá Bán";
            this.grclGiaBan.DisplayFormat.FormatString = "n0";
            this.grclGiaBan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaBan.FieldName = "GiaBan";
            this.grclGiaBan.Name = "grclGiaBan";
            this.grclGiaBan.Visible = true;
            this.grclGiaBan.VisibleIndex = 4;
            this.grclGiaBan.Width = 63;
            // 
            // grclKhoiLuongTinh
            // 
            this.grclKhoiLuongTinh.Caption = "KLT";
            this.grclKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.grclKhoiLuongTinh.Name = "grclKhoiLuongTinh";
            this.grclKhoiLuongTinh.Visible = true;
            this.grclKhoiLuongTinh.VisibleIndex = 5;
            this.grclKhoiLuongTinh.Width = 64;
            // 
            // grclKhoiLuongDa
            // 
            this.grclKhoiLuongDa.Caption = "KLĐ";
            this.grclKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.grclKhoiLuongDa.Name = "grclKhoiLuongDa";
            this.grclKhoiLuongDa.Visible = true;
            this.grclKhoiLuongDa.VisibleIndex = 6;
            this.grclKhoiLuongDa.Width = 64;
            // 
            // grclKhoiLuongVang
            // 
            this.grclKhoiLuongVang.Caption = "KLV";
            this.grclKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.grclKhoiLuongVang.Name = "grclKhoiLuongVang";
            this.grclKhoiLuongVang.Visible = true;
            this.grclKhoiLuongVang.VisibleIndex = 7;
            this.grclKhoiLuongVang.Width = 64;
            // 
            // MaChuyenKho
            // 
            this.MaChuyenKho.Caption = "Mã chuyển kho";
            this.MaChuyenKho.FieldName = "MaChuyenKho";
            this.MaChuyenKho.Name = "MaChuyenKho";
            this.MaChuyenKho.Visible = true;
            this.MaChuyenKho.VisibleIndex = 8;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(895, 217);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.grcSanPham;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(869, 191);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 625);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(909, 4);
            this.splitter1.TabIndex = 13;
            this.splitter1.TabStop = false;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl1.Location = new System.Drawing.Point(903, 50);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 575);
            this.splitterControl1.TabIndex = 23;
            this.splitterControl1.TabStop = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.grpDanhSachSanPham);
            this.panelControl1.Controls.Add(this.grpThongTinSanPham);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 50);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(903, 575);
            this.panelControl1.TabIndex = 24;
            // 
            // grpThongTinSanPham
            // 
            this.grpThongTinSanPham.Controls.Add(this.layoutControl1);
            this.grpThongTinSanPham.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpThongTinSanPham.Location = new System.Drawing.Point(2, 2);
            this.grpThongTinSanPham.Margin = new System.Windows.Forms.Padding(4);
            this.grpThongTinSanPham.Name = "grpThongTinSanPham";
            this.grpThongTinSanPham.Size = new System.Drawing.Size(899, 327);
            this.grpThongTinSanPham.TabIndex = 1;
            this.grpThongTinSanPham.Text = "Thông tin sản phẩm";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtTienCong);
            this.layoutControl1.Controls.Add(this.txtVangCat);
            this.layoutControl1.Controls.Add(this.txtDaCat);
            this.layoutControl1.Controls.Add(this.btnCatVang);
            this.layoutControl1.Controls.Add(this.txtTinhCat);
            this.layoutControl1.Controls.Add(this.txtLyVang);
            this.layoutControl1.Controls.Add(this.txtChiDa);
            this.layoutControl1.Controls.Add(this.txtPhanDa);
            this.layoutControl1.Controls.Add(this.txtLyDa);
            this.layoutControl1.Controls.Add(this.txtChiTinh);
            this.layoutControl1.Controls.Add(this.txtPhanTinh);
            this.layoutControl1.Controls.Add(this.label3);
            this.layoutControl1.Controls.Add(this.label2);
            this.layoutControl1.Controls.Add(this.txtLyTinh);
            this.layoutControl1.Controls.Add(this.txtChiVang);
            this.layoutControl1.Controls.Add(this.txtPhanVang);
            this.layoutControl1.Controls.Add(this.label1);
            this.layoutControl1.Controls.Add(this.btnNhapLai);
            this.layoutControl1.Controls.Add(this.btnThemVaoHoaDon);
            this.layoutControl1.Controls.Add(this.txtGiaBan);
            this.layoutControl1.Controls.Add(this.txtHamLuongVang);
            this.layoutControl1.Controls.Add(this.txtTiLeHot);
            this.layoutControl1.Controls.Add(this.txtTenSanPham);
            this.layoutControl1.Controls.Add(this.txtMaTem);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(429, 148, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(895, 300);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtTienCong
            // 
            this.txtTienCong.Location = new System.Drawing.Point(555, 226);
            this.txtTienCong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienCong.Name = "txtTienCong";
            this.txtTienCong.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienCong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienCong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienCong.Properties.ReadOnly = true;
            this.txtTienCong.Size = new System.Drawing.Size(324, 22);
            this.txtTienCong.StyleController = this.layoutControl1;
            this.txtTienCong.TabIndex = 33;
            // 
            // txtVangCat
            // 
            this.txtVangCat.Location = new System.Drawing.Point(555, 44);
            this.txtVangCat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVangCat.Name = "txtVangCat";
            this.txtVangCat.Properties.Mask.EditMask = "f1";
            this.txtVangCat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtVangCat.Properties.NullText = "0";
            this.txtVangCat.Properties.ReadOnly = true;
            this.txtVangCat.Size = new System.Drawing.Size(107, 22);
            this.txtVangCat.StyleController = this.layoutControl1;
            this.txtVangCat.TabIndex = 32;
            // 
            // txtDaCat
            // 
            this.txtDaCat.Location = new System.Drawing.Point(338, 44);
            this.txtDaCat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDaCat.Name = "txtDaCat";
            this.txtDaCat.Properties.Mask.EditMask = "f1";
            this.txtDaCat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDaCat.Properties.MaxLength = 8;
            this.txtDaCat.Properties.NullText = "0";
            this.txtDaCat.Size = new System.Drawing.Size(106, 22);
            this.txtDaCat.StyleController = this.layoutControl1;
            this.txtDaCat.TabIndex = 31;
            this.txtDaCat.EditValueChanged += new System.EventHandler(this.txtDaCat_EditValueChanged);
            // 
            // btnCatVang
            // 
            this.btnCatVang.Location = new System.Drawing.Point(668, 44);
            this.btnCatVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCatVang.Name = "btnCatVang";
            this.btnCatVang.Size = new System.Drawing.Size(211, 27);
            this.btnCatVang.StyleController = this.layoutControl1;
            this.btnCatVang.TabIndex = 30;
            this.btnCatVang.Text = "Cắt vàng";
            this.btnCatVang.Click += new System.EventHandler(this.btnCatVang_Click);
            // 
            // txtTinhCat
            // 
            this.txtTinhCat.Location = new System.Drawing.Point(121, 44);
            this.txtTinhCat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTinhCat.Name = "txtTinhCat";
            this.txtTinhCat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTinhCat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTinhCat.Properties.Mask.EditMask = "f1";
            this.txtTinhCat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTinhCat.Properties.MaxLength = 8;
            this.txtTinhCat.Properties.NullText = "0";
            this.txtTinhCat.Size = new System.Drawing.Size(106, 22);
            this.txtTinhCat.StyleController = this.layoutControl1;
            this.txtTinhCat.TabIndex = 29;
            this.txtTinhCat.EditValueChanged += new System.EventHandler(this.txtTinhCat_EditValueChanged);
            // 
            // txtLyVang
            // 
            this.txtLyVang.Location = new System.Drawing.Point(773, 195);
            this.txtLyVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyVang.Name = "txtLyVang";
            this.txtLyVang.Properties.ReadOnly = true;
            this.txtLyVang.Size = new System.Drawing.Size(106, 22);
            this.txtLyVang.StyleController = this.layoutControl1;
            this.txtLyVang.TabIndex = 28;
            // 
            // txtChiDa
            // 
            this.txtChiDa.Location = new System.Drawing.Point(338, 164);
            this.txtChiDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiDa.Name = "txtChiDa";
            this.txtChiDa.Properties.ReadOnly = true;
            this.txtChiDa.Size = new System.Drawing.Size(106, 22);
            this.txtChiDa.StyleController = this.layoutControl1;
            this.txtChiDa.TabIndex = 27;
            // 
            // txtPhanDa
            // 
            this.txtPhanDa.Location = new System.Drawing.Point(555, 164);
            this.txtPhanDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanDa.Name = "txtPhanDa";
            this.txtPhanDa.Properties.ReadOnly = true;
            this.txtPhanDa.Size = new System.Drawing.Size(107, 22);
            this.txtPhanDa.StyleController = this.layoutControl1;
            this.txtPhanDa.TabIndex = 26;
            // 
            // txtLyDa
            // 
            this.txtLyDa.Location = new System.Drawing.Point(773, 164);
            this.txtLyDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDa.Name = "txtLyDa";
            this.txtLyDa.Properties.ReadOnly = true;
            this.txtLyDa.Size = new System.Drawing.Size(106, 22);
            this.txtLyDa.StyleController = this.layoutControl1;
            this.txtLyDa.TabIndex = 25;
            // 
            // txtChiTinh
            // 
            this.txtChiTinh.Location = new System.Drawing.Point(338, 133);
            this.txtChiTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiTinh.Name = "txtChiTinh";
            this.txtChiTinh.Properties.ReadOnly = true;
            this.txtChiTinh.Size = new System.Drawing.Size(106, 22);
            this.txtChiTinh.StyleController = this.layoutControl1;
            this.txtChiTinh.TabIndex = 24;
            // 
            // txtPhanTinh
            // 
            this.txtPhanTinh.Location = new System.Drawing.Point(555, 133);
            this.txtPhanTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanTinh.Name = "txtPhanTinh";
            this.txtPhanTinh.Properties.ReadOnly = true;
            this.txtPhanTinh.Size = new System.Drawing.Size(107, 22);
            this.txtPhanTinh.StyleController = this.layoutControl1;
            this.txtPhanTinh.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(211, 25);
            this.label3.TabIndex = 22;
            this.label3.Text = "Khối lượng vàng";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 25);
            this.label2.TabIndex = 21;
            this.label2.Text = "Khối lượng đá";
            // 
            // txtLyTinh
            // 
            this.txtLyTinh.Location = new System.Drawing.Point(773, 133);
            this.txtLyTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyTinh.Name = "txtLyTinh";
            this.txtLyTinh.Properties.ReadOnly = true;
            this.txtLyTinh.Size = new System.Drawing.Size(106, 22);
            this.txtLyTinh.StyleController = this.layoutControl1;
            this.txtLyTinh.TabIndex = 20;
            // 
            // txtChiVang
            // 
            this.txtChiVang.EditValue = " ";
            this.txtChiVang.Location = new System.Drawing.Point(338, 195);
            this.txtChiVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiVang.Name = "txtChiVang";
            this.txtChiVang.Properties.ReadOnly = true;
            this.txtChiVang.Size = new System.Drawing.Size(106, 22);
            this.txtChiVang.StyleController = this.layoutControl1;
            this.txtChiVang.TabIndex = 19;
            // 
            // txtPhanVang
            // 
            this.txtPhanVang.Location = new System.Drawing.Point(555, 195);
            this.txtPhanVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanVang.Name = "txtPhanVang";
            this.txtPhanVang.Properties.ReadOnly = true;
            this.txtPhanVang.Size = new System.Drawing.Size(107, 22);
            this.txtPhanVang.StyleController = this.layoutControl1;
            this.txtPhanVang.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "Khối lượng tổng";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Location = new System.Drawing.Point(16, 254);
            this.btnNhapLai.Margin = new System.Windows.Forms.Padding(4);
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.Size = new System.Drawing.Size(406, 27);
            this.btnNhapLai.StyleController = this.layoutControl1;
            this.btnNhapLai.TabIndex = 4;
            this.btnNhapLai.Text = "Nhập lại";
            this.btnNhapLai.Click += new System.EventHandler(this.btnNhapLai_Click);
            // 
            // btnThemVaoHoaDon
            // 
            this.btnThemVaoHoaDon.Location = new System.Drawing.Point(428, 254);
            this.btnThemVaoHoaDon.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemVaoHoaDon.Name = "btnThemVaoHoaDon";
            this.btnThemVaoHoaDon.Size = new System.Drawing.Size(451, 27);
            this.btnThemVaoHoaDon.StyleController = this.layoutControl1;
            this.btnThemVaoHoaDon.TabIndex = 5;
            this.btnThemVaoHoaDon.Text = "Thêm Vào Hóa Đơn";
            this.btnThemVaoHoaDon.Click += new System.EventHandler(this.btnThemVaoHoaDon_Click);
            // 
            // txtGiaBan
            // 
            this.txtGiaBan.Location = new System.Drawing.Point(121, 226);
            this.txtGiaBan.Margin = new System.Windows.Forms.Padding(4);
            this.txtGiaBan.Name = "txtGiaBan";
            this.txtGiaBan.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtGiaBan.Properties.Appearance.Options.UseForeColor = true;
            this.txtGiaBan.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtGiaBan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaBan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGiaBan.Properties.ReadOnly = true;
            this.txtGiaBan.Size = new System.Drawing.Size(323, 22);
            this.txtGiaBan.StyleController = this.layoutControl1;
            this.txtGiaBan.TabIndex = 3;
            // 
            // txtHamLuongVang
            // 
            this.txtHamLuongVang.EditValue = "";
            this.txtHamLuongVang.Location = new System.Drawing.Point(121, 105);
            this.txtHamLuongVang.Margin = new System.Windows.Forms.Padding(4);
            this.txtHamLuongVang.Name = "txtHamLuongVang";
            this.txtHamLuongVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtHamLuongVang.Properties.ReadOnly = true;
            this.txtHamLuongVang.Size = new System.Drawing.Size(323, 22);
            this.txtHamLuongVang.StyleController = this.layoutControl1;
            this.txtHamLuongVang.TabIndex = 8;
            // 
            // txtTiLeHot
            // 
            this.txtTiLeHot.Location = new System.Drawing.Point(555, 105);
            this.txtTiLeHot.Margin = new System.Windows.Forms.Padding(4);
            this.txtTiLeHot.Name = "txtTiLeHot";
            this.txtTiLeHot.Properties.Mask.EditMask = "f3";
            this.txtTiLeHot.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTiLeHot.Properties.ReadOnly = true;
            this.txtTiLeHot.Size = new System.Drawing.Size(324, 22);
            this.txtTiLeHot.StyleController = this.layoutControl1;
            this.txtTiLeHot.TabIndex = 7;
            // 
            // txtTenSanPham
            // 
            this.txtTenSanPham.Location = new System.Drawing.Point(121, 77);
            this.txtTenSanPham.Margin = new System.Windows.Forms.Padding(4);
            this.txtTenSanPham.Name = "txtTenSanPham";
            this.txtTenSanPham.Properties.ReadOnly = true;
            this.txtTenSanPham.Size = new System.Drawing.Size(758, 22);
            this.txtTenSanPham.StyleController = this.layoutControl1;
            this.txtTenSanPham.TabIndex = 6;
            // 
            // txtMaTem
            // 
            this.txtMaTem.Location = new System.Drawing.Point(121, 16);
            this.txtMaTem.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaTem.Name = "txtMaTem";
            this.txtMaTem.Properties.Mask.EditMask = "(\\p{Lu}|\\d)+";
            this.txtMaTem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMaTem.Size = new System.Drawing.Size(758, 22);
            this.txtMaTem.StyleController = this.layoutControl1;
            this.txtMaTem.TabIndex = 2;
            this.txtMaTem.EditValueChanged += new System.EventHandler(this.txtMaTem_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem18,
            this.layoutControlItem16,
            this.layoutControlItem19,
            this.layoutControlItem12,
            this.layoutControlItem25,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem22,
            this.layoutControlItem26,
            this.layoutControlItem29,
            this.layoutControlItem32,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem24,
            this.layoutControlItem28,
            this.layoutControlItem31,
            this.layoutControlItem23,
            this.layoutControlItem27,
            this.layoutControlItem30,
            this.layoutControlItem35});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(895, 300);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtTenSanPham;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 61);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(869, 28);
            this.layoutControlItem3.Text = "Tên sản phẩm";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtMaTem;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(869, 28);
            this.layoutControlItem2.Text = "Mã tem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtGiaBan;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(434, 28);
            this.layoutControlItem18.Text = "Giá Bán";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.btnThemVaoHoaDon;
            this.layoutControlItem16.Location = new System.Drawing.Point(412, 238);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(457, 36);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnNhapLai;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 238);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(412, 36);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.label1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 117);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.txtLyTinh;
            this.layoutControlItem25.Location = new System.Drawing.Point(652, 117);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem25.Text = "Ly";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtHamLuongVang;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(434, 28);
            this.layoutControlItem6.Text = "Hàm lượng vàng";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTiLeHot;
            this.layoutControlItem4.Location = new System.Drawing.Point(434, 89);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(435, 28);
            this.layoutControlItem4.Text = "Tỉ lệ hột";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.label2;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 148);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.label3;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 179);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txtLyDa;
            this.layoutControlItem29.Location = new System.Drawing.Point(652, 148);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem29.Text = "Ly";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txtLyVang;
            this.layoutControlItem32.Location = new System.Drawing.Point(652, 179);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem32.Text = "Ly";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtTinhCat;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(217, 33);
            this.layoutControlItem20.Text = "KLT cắt";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.btnCatVang;
            this.layoutControlItem21.Location = new System.Drawing.Point(652, 28);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(217, 33);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.txtDaCat;
            this.layoutControlItem33.Location = new System.Drawing.Point(217, 28);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(217, 33);
            this.layoutControlItem33.Text = "KLĐ cắt";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.txtVangCat;
            this.layoutControlItem34.Location = new System.Drawing.Point(434, 28);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(218, 33);
            this.layoutControlItem34.Text = "KLV cắt";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtChiVang;
            this.layoutControlItem24.Location = new System.Drawing.Point(217, 179);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem24.Text = "Chỉ";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txtChiTinh;
            this.layoutControlItem28.Location = new System.Drawing.Point(217, 117);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem28.Text = "Chỉ";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.txtChiDa;
            this.layoutControlItem31.Location = new System.Drawing.Point(217, 148);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(217, 31);
            this.layoutControlItem31.Text = "Chỉ";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtPhanVang;
            this.layoutControlItem23.Location = new System.Drawing.Point(434, 179);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(218, 31);
            this.layoutControlItem23.Text = "Phân";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txtPhanTinh;
            this.layoutControlItem27.Location = new System.Drawing.Point(434, 117);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(218, 31);
            this.layoutControlItem27.Text = "Phân";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.txtPhanDa;
            this.layoutControlItem30.Location = new System.Drawing.Point(434, 148);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(218, 31);
            this.layoutControlItem30.Text = "Phân";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.txtTienCong;
            this.layoutControlItem35.Location = new System.Drawing.Point(434, 210);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(435, 28);
            this.layoutControlItem35.Text = "Tiền công";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(102, 17);
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLamMoiDuLieu,
            this.btnInTemMotSanPham});
            this.barManager2.MainMenu = this.bar2;
            this.barManager2.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInTemMotSanPham, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 0;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInTemMotSanPham
            // 
            this.btnInTemMotSanPham.Caption = "In tem sản phẩm";
            this.btnInTemMotSanPham.Glyph = global::QuanLyTiemVang.Properties.Resources.barcode_icon;
            this.btnInTemMotSanPham.Id = 1;
            this.btnInTemMotSanPham.Name = "btnInTemMotSanPham";
            this.btnInTemMotSanPham.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInTemMotSanPham_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControl1.Size = new System.Drawing.Size(1344, 50);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 629);
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControl2.Size = new System.Drawing.Size(1344, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 50);
            this.barDockControl3.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControl3.Size = new System.Drawing.Size(0, 579);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1344, 50);
            this.barDockControl4.Margin = new System.Windows.Forms.Padding(5);
            this.barDockControl4.Size = new System.Drawing.Size(0, 579);
            // 
            // frmBanVang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 629);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.grpHoaDon);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmBanVang";
            this.Text = "Bán Vàng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSell_FormClosed);
            this.Load += new System.EventHandler(this.frmSell_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpHoaDon)).EndInit();
            this.grpHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHoaDon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachSanPham)).EndInit();
            this.grpDanhSachSanPham.ResumeLayout(false);
            this.grpDanhSachSanPham.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinSanPham)).EndInit();
            this.grpThongTinSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVangCat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaCat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhCat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl grpHoaDon;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.DateEdit detNgayBan;
        private DevExpress.XtraGrid.GridControl grcHoaDon;
        private DevExpress.XtraGrid.Views.Grid.GridView grvHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham_grcHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaBan_grcHoaDon;
        private DevExpress.XtraEditors.TextEdit txtSoHoaDon;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton btnNhapLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnThanhToan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.GroupControl grpDanhSachSanPham;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private System.Windows.Forms.Splitter splitter1;
        private DevExpress.XtraGrid.GridControl grcSanPham;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclTem;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclTiLeHot;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaBan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl grpThongTinSanPham;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnNhapLai;
        private DevExpress.XtraEditors.SimpleButton btnThemVaoHoaDon;
        private DevExpress.XtraEditors.TextEdit txtGiaBan;
        private DevExpress.XtraEditors.TextEdit txtHamLuongVang;
        private DevExpress.XtraEditors.TextEdit txtTiLeHot;
        private DevExpress.XtraEditors.TextEdit txtTenSanPham;
        private DevExpress.XtraEditors.TextEdit txtMaTem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraGrid.Columns.GridColumn grclTem_grcHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongVang;
        private DevExpress.XtraEditors.TextEdit txtNhanVien;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.Columns.GridColumn grclHanhDong;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteItem;
        private DevExpress.XtraEditors.TextEdit txtThanhTien;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnInTemMotSanPham;
        private DevExpress.XtraEditors.TextEdit txtLyTinh;
        private DevExpress.XtraEditors.TextEdit txtChiVang;
        private DevExpress.XtraEditors.TextEdit txtPhanVang;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.TextEdit txtLyVang;
        private DevExpress.XtraEditors.TextEdit txtChiDa;
        private DevExpress.XtraEditors.TextEdit txtPhanDa;
        private DevExpress.XtraEditors.TextEdit txtLyDa;
        private DevExpress.XtraEditors.TextEdit txtChiTinh;
        private DevExpress.XtraEditors.TextEdit txtPhanTinh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.LookUpEdit lueKhachHang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.Columns.GridColumn MaChuyenKho;
        private DevExpress.XtraEditors.SimpleButton btnCatVang;
        private DevExpress.XtraEditors.TextEdit txtTinhCat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit txtVangCat;
        private DevExpress.XtraEditors.TextEdit txtDaCat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.TextEdit txtTienCong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private System.Windows.Forms.Label lbTongSoLuongSanPham;
        private DevExpress.XtraEditors.TextEdit txtTienBot;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
    }
}