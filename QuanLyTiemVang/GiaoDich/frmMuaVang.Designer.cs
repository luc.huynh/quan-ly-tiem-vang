﻿namespace QuanLyTiemVang.GiaoDich
{
    partial class frmMuaVang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txtDonGiaMua = new DevExpress.XtraEditors.TextEdit();
            this.btnThemVaoHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhapLaiSanPham = new DevExpress.XtraEditors.SimpleButton();
            this.txtGiaMua = new DevExpress.XtraEditors.TextEdit();
            this.txtLyVang = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVang = new DevExpress.XtraEditors.TextEdit();
            this.txtChiVang = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDa = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDa = new DevExpress.XtraEditors.TextEdit();
            this.txtChiDa = new DevExpress.XtraEditors.TextEdit();
            this.txtLyTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinh = new DevExpress.XtraEditors.TextEdit();
            this.lueHamLuongVang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtTiLeHot = new DevExpress.XtraEditors.TextEdit();
            this.txtTenSanPham = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTienCongThem = new DevExpress.XtraEditors.TextEdit();
            this.btnThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhapLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.txtThanhTien = new DevExpress.XtraEditors.TextEdit();
            this.grcHoaDonMua = new DevExpress.XtraGrid.GridControl();
            this.grvHoaDonMua = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclButtonXoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoa = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.txtNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.txtSoHoaDon = new DevExpress.XtraEditors.TextEdit();
            this.detNgayNhap = new DevExpress.XtraEditors.DateEdit();
            this.lueKhachHang = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxerHamLuongVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyTinh = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyDa = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerTiLeHot = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerTenSanPham = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerGiaMua = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonGiaMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCongThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcHoaDonMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHoaDonMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHoaDon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerGiaMua)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.btnLamMoiDuLieu});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 2;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(944, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 647);
            this.barDockControlBottom.Size = new System.Drawing.Size(944, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 607);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(944, 40);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 607);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Nhập lại";
            this.barButtonItem1.Glyph = global::QuanLyTiemVang.Properties.Resources.Clear_icon;
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(0, 40);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(538, 607);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Thông tin vàng mua vào";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.txtDonGiaMua);
            this.layoutControl2.Controls.Add(this.btnThemVaoHoaDon);
            this.layoutControl2.Controls.Add(this.btnNhapLaiSanPham);
            this.layoutControl2.Controls.Add(this.txtGiaMua);
            this.layoutControl2.Controls.Add(this.txtLyVang);
            this.layoutControl2.Controls.Add(this.txtPhanVang);
            this.layoutControl2.Controls.Add(this.txtChiVang);
            this.layoutControl2.Controls.Add(this.txtLyDa);
            this.layoutControl2.Controls.Add(this.txtPhanDa);
            this.layoutControl2.Controls.Add(this.txtChiDa);
            this.layoutControl2.Controls.Add(this.txtLyTinh);
            this.layoutControl2.Controls.Add(this.txtPhanTinh);
            this.layoutControl2.Controls.Add(this.txtChiTinh);
            this.layoutControl2.Controls.Add(this.lueHamLuongVang);
            this.layoutControl2.Controls.Add(this.txtTiLeHot);
            this.layoutControl2.Controls.Add(this.txtTenSanPham);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(2, 20);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(677, 139, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(534, 232);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txtDonGiaMua
            // 
            this.txtDonGiaMua.Location = new System.Drawing.Point(93, 156);
            this.txtDonGiaMua.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDonGiaMua.MenuManager = this.barManager1;
            this.txtDonGiaMua.Name = "txtDonGiaMua";
            this.txtDonGiaMua.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtDonGiaMua.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDonGiaMua.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDonGiaMua.Properties.ReadOnly = true;
            this.txtDonGiaMua.Size = new System.Drawing.Size(412, 20);
            this.txtDonGiaMua.StyleController = this.layoutControl2;
            this.txtDonGiaMua.TabIndex = 19;
            // 
            // btnThemVaoHoaDon
            // 
            this.btnThemVaoHoaDon.Location = new System.Drawing.Point(260, 204);
            this.btnThemVaoHoaDon.Name = "btnThemVaoHoaDon";
            this.btnThemVaoHoaDon.Size = new System.Drawing.Size(245, 22);
            this.btnThemVaoHoaDon.StyleController = this.layoutControl2;
            this.btnThemVaoHoaDon.TabIndex = 18;
            this.btnThemVaoHoaDon.Text = "Thêm vào hóa đơn";
            this.btnThemVaoHoaDon.Click += new System.EventHandler(this.btnThemVaoHoaDon_Click);
            // 
            // btnNhapLaiSanPham
            // 
            this.btnNhapLaiSanPham.Location = new System.Drawing.Point(12, 204);
            this.btnNhapLaiSanPham.Name = "btnNhapLaiSanPham";
            this.btnNhapLaiSanPham.Size = new System.Drawing.Size(244, 22);
            this.btnNhapLaiSanPham.StyleController = this.layoutControl2;
            this.btnNhapLaiSanPham.TabIndex = 17;
            this.btnNhapLaiSanPham.Text = "Nhập lại";
            this.btnNhapLaiSanPham.Click += new System.EventHandler(this.btnNhapLaiSanPham_Click);
            // 
            // txtGiaMua
            // 
            this.txtGiaMua.Location = new System.Drawing.Point(93, 180);
            this.txtGiaMua.MenuManager = this.barManager1;
            this.txtGiaMua.Name = "txtGiaMua";
            this.txtGiaMua.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtGiaMua.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaMua.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGiaMua.Properties.MaxLength = 9;
            this.txtGiaMua.Properties.ReadOnly = true;
            this.txtGiaMua.Size = new System.Drawing.Size(412, 20);
            this.txtGiaMua.StyleController = this.layoutControl2;
            this.txtGiaMua.TabIndex = 16;
            // 
            // txtLyVang
            // 
            this.txtLyVang.Location = new System.Drawing.Point(428, 132);
            this.txtLyVang.MenuManager = this.barManager1;
            this.txtLyVang.Name = "txtLyVang";
            this.txtLyVang.Properties.MaxLength = 3;
            this.txtLyVang.Properties.NullText = "0";
            this.txtLyVang.Properties.ReadOnly = true;
            this.txtLyVang.Size = new System.Drawing.Size(77, 20);
            this.txtLyVang.StyleController = this.layoutControl2;
            this.txtLyVang.TabIndex = 15;
            // 
            // txtPhanVang
            // 
            this.txtPhanVang.Location = new System.Drawing.Point(263, 132);
            this.txtPhanVang.MenuManager = this.barManager1;
            this.txtPhanVang.Name = "txtPhanVang";
            this.txtPhanVang.Properties.Mask.EditMask = "d";
            this.txtPhanVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanVang.Properties.MaxLength = 1;
            this.txtPhanVang.Properties.NullText = "0";
            this.txtPhanVang.Properties.ReadOnly = true;
            this.txtPhanVang.Size = new System.Drawing.Size(80, 20);
            this.txtPhanVang.StyleController = this.layoutControl2;
            this.txtPhanVang.TabIndex = 14;
            // 
            // txtChiVang
            // 
            this.txtChiVang.Location = new System.Drawing.Point(93, 132);
            this.txtChiVang.MenuManager = this.barManager1;
            this.txtChiVang.Name = "txtChiVang";
            this.txtChiVang.Properties.Mask.EditMask = "d";
            this.txtChiVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiVang.Properties.MaxLength = 9;
            this.txtChiVang.Properties.NullText = "0";
            this.txtChiVang.Properties.ReadOnly = true;
            this.txtChiVang.Size = new System.Drawing.Size(85, 20);
            this.txtChiVang.StyleController = this.layoutControl2;
            this.txtChiVang.TabIndex = 13;
            // 
            // txtLyDa
            // 
            this.txtLyDa.Location = new System.Drawing.Point(428, 108);
            this.txtLyDa.MenuManager = this.barManager1;
            this.txtLyDa.Name = "txtLyDa";
            this.txtLyDa.Properties.Mask.EditMask = "f1";
            this.txtLyDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLyDa.Properties.MaxLength = 3;
            this.txtLyDa.Properties.NullText = "0";
            this.txtLyDa.Size = new System.Drawing.Size(77, 20);
            this.txtLyDa.StyleController = this.layoutControl2;
            this.txtLyDa.TabIndex = 12;
            this.txtLyDa.EditValueChanged += new System.EventHandler(this.txtLyDa_EditValueChanged);
            // 
            // txtPhanDa
            // 
            this.txtPhanDa.Location = new System.Drawing.Point(263, 108);
            this.txtPhanDa.MenuManager = this.barManager1;
            this.txtPhanDa.Name = "txtPhanDa";
            this.txtPhanDa.Properties.Mask.EditMask = "d";
            this.txtPhanDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanDa.Properties.MaxLength = 1;
            this.txtPhanDa.Properties.NullText = "0";
            this.txtPhanDa.Size = new System.Drawing.Size(80, 20);
            this.txtPhanDa.StyleController = this.layoutControl2;
            this.txtPhanDa.TabIndex = 11;
            this.txtPhanDa.EditValueChanged += new System.EventHandler(this.txtPhanDa_EditValueChanged);
            // 
            // txtChiDa
            // 
            this.txtChiDa.Location = new System.Drawing.Point(93, 108);
            this.txtChiDa.MenuManager = this.barManager1;
            this.txtChiDa.Name = "txtChiDa";
            this.txtChiDa.Properties.Mask.EditMask = "d";
            this.txtChiDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiDa.Properties.MaxLength = 9;
            this.txtChiDa.Properties.NullText = "0";
            this.txtChiDa.Size = new System.Drawing.Size(85, 20);
            this.txtChiDa.StyleController = this.layoutControl2;
            this.txtChiDa.TabIndex = 10;
            this.txtChiDa.EditValueChanged += new System.EventHandler(this.txtChiDa_EditValueChanged);
            // 
            // txtLyTinh
            // 
            this.txtLyTinh.Location = new System.Drawing.Point(428, 84);
            this.txtLyTinh.MenuManager = this.barManager1;
            this.txtLyTinh.Name = "txtLyTinh";
            this.txtLyTinh.Properties.Mask.EditMask = "f1";
            this.txtLyTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLyTinh.Properties.MaxLength = 3;
            this.txtLyTinh.Properties.NullText = "0";
            this.txtLyTinh.Size = new System.Drawing.Size(77, 20);
            this.txtLyTinh.StyleController = this.layoutControl2;
            this.txtLyTinh.TabIndex = 9;
            this.txtLyTinh.EditValueChanged += new System.EventHandler(this.txtLyTinh_EditValueChanged);
            // 
            // txtPhanTinh
            // 
            this.txtPhanTinh.Location = new System.Drawing.Point(263, 84);
            this.txtPhanTinh.MenuManager = this.barManager1;
            this.txtPhanTinh.Name = "txtPhanTinh";
            this.txtPhanTinh.Properties.Mask.EditMask = "d";
            this.txtPhanTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanTinh.Properties.MaxLength = 1;
            this.txtPhanTinh.Properties.NullText = "0";
            this.txtPhanTinh.Size = new System.Drawing.Size(80, 20);
            this.txtPhanTinh.StyleController = this.layoutControl2;
            this.txtPhanTinh.TabIndex = 8;
            this.txtPhanTinh.EditValueChanged += new System.EventHandler(this.txtPhanTinh_EditValueChanged);
            // 
            // txtChiTinh
            // 
            this.txtChiTinh.Location = new System.Drawing.Point(93, 84);
            this.txtChiTinh.MenuManager = this.barManager1;
            this.txtChiTinh.Name = "txtChiTinh";
            this.txtChiTinh.Properties.Mask.EditMask = "d";
            this.txtChiTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiTinh.Properties.MaxLength = 9;
            this.txtChiTinh.Properties.NullText = "0";
            this.txtChiTinh.Size = new System.Drawing.Size(85, 20);
            this.txtChiTinh.StyleController = this.layoutControl2;
            this.txtChiTinh.TabIndex = 7;
            this.txtChiTinh.EditValueChanged += new System.EventHandler(this.txtChiTinh_EditValueChanged);
            // 
            // lueHamLuongVang
            // 
            this.lueHamLuongVang.Location = new System.Drawing.Point(93, 36);
            this.lueHamLuongVang.MenuManager = this.barManager1;
            this.lueHamLuongVang.Name = "lueHamLuongVang";
            this.lueHamLuongVang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVang.Properties.NullText = "";
            this.lueHamLuongVang.Size = new System.Drawing.Size(412, 20);
            this.lueHamLuongVang.StyleController = this.layoutControl2;
            this.lueHamLuongVang.TabIndex = 6;
            this.lueHamLuongVang.EditValueChanged += new System.EventHandler(this.lueHamLuongVang_EditValueChanged);
            // 
            // txtTiLeHot
            // 
            this.txtTiLeHot.Location = new System.Drawing.Point(93, 60);
            this.txtTiLeHot.MenuManager = this.barManager1;
            this.txtTiLeHot.Name = "txtTiLeHot";
            this.txtTiLeHot.Properties.MaxLength = 9;
            this.txtTiLeHot.Properties.NullText = "0";
            this.txtTiLeHot.Size = new System.Drawing.Size(412, 20);
            this.txtTiLeHot.StyleController = this.layoutControl2;
            this.txtTiLeHot.TabIndex = 5;
            // 
            // txtTenSanPham
            // 
            this.txtTenSanPham.Location = new System.Drawing.Point(93, 12);
            this.txtTenSanPham.MenuManager = this.barManager1;
            this.txtTenSanPham.Name = "txtTenSanPham";
            this.txtTenSanPham.Size = new System.Drawing.Size(412, 20);
            this.txtTenSanPham.StyleController = this.layoutControl2;
            this.txtTenSanPham.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem25});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(517, 238);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtTenSanPham;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(497, 24);
            this.layoutControlItem9.Text = "Tên sản phẩm";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtTiLeHot;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(497, 24);
            this.layoutControlItem10.Text = "Tỉ lệ hột";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.lueHamLuongVang;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(497, 24);
            this.layoutControlItem11.Text = "Hàm lượng vàng";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtChiTinh;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(170, 24);
            this.layoutControlItem12.Text = "KL Tổng:  Chỉ";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtPhanTinh;
            this.layoutControlItem13.Location = new System.Drawing.Point(170, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem13.Text = "Phân";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtLyTinh;
            this.layoutControlItem14.Location = new System.Drawing.Point(335, 72);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem14.Text = "Ly";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtChiDa;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(170, 24);
            this.layoutControlItem15.Text = "KL Đá:     Chỉ";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtPhanDa;
            this.layoutControlItem16.Location = new System.Drawing.Point(170, 96);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem16.Text = "Phân";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtLyDa;
            this.layoutControlItem17.Location = new System.Drawing.Point(335, 96);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem17.Text = "Ly";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtChiVang;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(170, 24);
            this.layoutControlItem18.Text = "KL Vàng: Chỉ";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtPhanVang;
            this.layoutControlItem19.Location = new System.Drawing.Point(170, 120);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem19.Text = "Phân";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtLyVang;
            this.layoutControlItem20.Location = new System.Drawing.Point(335, 120);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem20.Text = "Ly";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtGiaMua;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(497, 24);
            this.layoutControlItem21.Text = "Thành tiền";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.btnNhapLaiSanPham;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(248, 26);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnThemVaoHoaDon;
            this.layoutControlItem23.Location = new System.Drawing.Point(248, 192);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(249, 26);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.txtDonGiaMua;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(497, 24);
            this.layoutControlItem25.Text = "Đơn giá mua";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(78, 13);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(538, 40);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 607);
            this.splitterControl1.TabIndex = 5;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.layoutControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(543, 40);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(401, 607);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Hóa đơn";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtTienCongThem);
            this.layoutControl1.Controls.Add(this.btnThanhToan);
            this.layoutControl1.Controls.Add(this.btnNhapLaiHoaDon);
            this.layoutControl1.Controls.Add(this.txtThanhTien);
            this.layoutControl1.Controls.Add(this.grcHoaDonMua);
            this.layoutControl1.Controls.Add(this.txtNhanVien);
            this.layoutControl1.Controls.Add(this.txtSoHoaDon);
            this.layoutControl1.Controls.Add(this.detNgayNhap);
            this.layoutControl1.Controls.Add(this.lueKhachHang);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 20);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(397, 585);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtTienCongThem
            // 
            this.txtTienCongThem.Location = new System.Drawing.Point(88, 503);
            this.txtTienCongThem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienCongThem.MenuManager = this.barManager1;
            this.txtTienCongThem.Name = "txtTienCongThem";
            this.txtTienCongThem.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienCongThem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienCongThem.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienCongThem.Properties.MaxLength = 9;
            this.txtTienCongThem.Size = new System.Drawing.Size(297, 20);
            this.txtTienCongThem.StyleController = this.layoutControl1;
            this.txtTienCongThem.TabIndex = 12;
            this.txtTienCongThem.EditValueChanged += new System.EventHandler(this.txtTienCongThem_EditValueChanged);
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Location = new System.Drawing.Point(200, 551);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(185, 22);
            this.btnThanhToan.StyleController = this.layoutControl1;
            this.btnThanhToan.TabIndex = 11;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // btnNhapLaiHoaDon
            // 
            this.btnNhapLaiHoaDon.Location = new System.Drawing.Point(12, 551);
            this.btnNhapLaiHoaDon.Name = "btnNhapLaiHoaDon";
            this.btnNhapLaiHoaDon.Size = new System.Drawing.Size(184, 22);
            this.btnNhapLaiHoaDon.StyleController = this.layoutControl1;
            this.btnNhapLaiHoaDon.TabIndex = 10;
            this.btnNhapLaiHoaDon.Text = "Nhập lại";
            this.btnNhapLaiHoaDon.Click += new System.EventHandler(this.btnNhapLaiHoaDon_Click);
            // 
            // txtThanhTien
            // 
            this.txtThanhTien.Location = new System.Drawing.Point(88, 527);
            this.txtThanhTien.MenuManager = this.barManager1;
            this.txtThanhTien.Name = "txtThanhTien";
            this.txtThanhTien.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtThanhTien.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThanhTien.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtThanhTien.Properties.ReadOnly = true;
            this.txtThanhTien.Size = new System.Drawing.Size(297, 20);
            this.txtThanhTien.StyleController = this.layoutControl1;
            this.txtThanhTien.TabIndex = 9;
            // 
            // grcHoaDonMua
            // 
            this.grcHoaDonMua.Location = new System.Drawing.Point(12, 60);
            this.grcHoaDonMua.MainView = this.grvHoaDonMua;
            this.grcHoaDonMua.MenuManager = this.barManager1;
            this.grcHoaDonMua.Name = "grcHoaDonMua";
            this.grcHoaDonMua.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoa});
            this.grcHoaDonMua.Size = new System.Drawing.Size(373, 439);
            this.grcHoaDonMua.TabIndex = 8;
            this.grcHoaDonMua.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvHoaDonMua});
            // 
            // grvHoaDonMua
            // 
            this.grvHoaDonMua.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclTenSanPham,
            this.grclHamLuongVang,
            this.grclKhoiLuongTinh,
            this.grclKhoiLuongDa,
            this.grclKhoiLuongVang,
            this.grclGiaMua,
            this.grclButtonXoa});
            this.grvHoaDonMua.GridControl = this.grcHoaDonMua;
            this.grvHoaDonMua.Name = "grvHoaDonMua";
            this.grvHoaDonMua.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvHoaDonMua.OptionsView.ShowGroupPanel = false;
            // 
            // grclTenSanPham
            // 
            this.grclTenSanPham.Caption = "Tên";
            this.grclTenSanPham.FieldName = "TenSanPham";
            this.grclTenSanPham.Name = "grclTenSanPham";
            this.grclTenSanPham.Visible = true;
            this.grclTenSanPham.VisibleIndex = 0;
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "HLV";
            this.grclHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 1;
            // 
            // grclKhoiLuongTinh
            // 
            this.grclKhoiLuongTinh.Caption = "KLT";
            this.grclKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.grclKhoiLuongTinh.Name = "grclKhoiLuongTinh";
            this.grclKhoiLuongTinh.Visible = true;
            this.grclKhoiLuongTinh.VisibleIndex = 2;
            // 
            // grclKhoiLuongDa
            // 
            this.grclKhoiLuongDa.Caption = "KLĐ";
            this.grclKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.grclKhoiLuongDa.Name = "grclKhoiLuongDa";
            this.grclKhoiLuongDa.Visible = true;
            this.grclKhoiLuongDa.VisibleIndex = 3;
            // 
            // grclKhoiLuongVang
            // 
            this.grclKhoiLuongVang.Caption = "KLV";
            this.grclKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.grclKhoiLuongVang.Name = "grclKhoiLuongVang";
            this.grclKhoiLuongVang.Visible = true;
            this.grclKhoiLuongVang.VisibleIndex = 4;
            // 
            // grclGiaMua
            // 
            this.grclGiaMua.Caption = "Giá mua";
            this.grclGiaMua.DisplayFormat.FormatString = "n0";
            this.grclGiaMua.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaMua.FieldName = "GiaMuaVao";
            this.grclGiaMua.Name = "grclGiaMua";
            this.grclGiaMua.Visible = true;
            this.grclGiaMua.VisibleIndex = 5;
            // 
            // grclButtonXoa
            // 
            this.grclButtonXoa.Caption = "Xóa";
            this.grclButtonXoa.ColumnEdit = this.btnXoa;
            this.grclButtonXoa.Name = "grclButtonXoa";
            this.grclButtonXoa.Visible = true;
            this.grclButtonXoa.VisibleIndex = 6;
            // 
            // btnXoa
            // 
            this.btnXoa.AutoHeight = false;
            this.btnXoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoa.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnXoa_ButtonClick);
            // 
            // txtNhanVien
            // 
            this.txtNhanVien.Location = new System.Drawing.Point(276, 36);
            this.txtNhanVien.MenuManager = this.barManager1;
            this.txtNhanVien.Name = "txtNhanVien";
            this.txtNhanVien.Properties.ReadOnly = true;
            this.txtNhanVien.Size = new System.Drawing.Size(109, 20);
            this.txtNhanVien.StyleController = this.layoutControl1;
            this.txtNhanVien.TabIndex = 7;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(88, 12);
            this.txtSoHoaDon.MenuManager = this.barManager1;
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Properties.ReadOnly = true;
            this.txtSoHoaDon.Size = new System.Drawing.Size(108, 20);
            this.txtSoHoaDon.StyleController = this.layoutControl1;
            this.txtSoHoaDon.TabIndex = 4;
            // 
            // detNgayNhap
            // 
            this.detNgayNhap.EditValue = null;
            this.detNgayNhap.Location = new System.Drawing.Point(276, 12);
            this.detNgayNhap.MenuManager = this.barManager1;
            this.detNgayNhap.Name = "detNgayNhap";
            this.detNgayNhap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayNhap.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayNhap.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.detNgayNhap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayNhap.Properties.EditFormat.FormatString = "";
            this.detNgayNhap.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayNhap.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayNhap.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayNhap.Properties.ReadOnly = true;
            this.detNgayNhap.Size = new System.Drawing.Size(109, 20);
            this.detNgayNhap.StyleController = this.layoutControl1;
            this.detNgayNhap.TabIndex = 5;
            // 
            // lueKhachHang
            // 
            this.lueKhachHang.Location = new System.Drawing.Point(88, 36);
            this.lueKhachHang.MenuManager = this.barManager1;
            this.lueKhachHang.Name = "lueKhachHang";
            this.lueKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKhachHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HoLot", "Họ lót"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Ten", "Tên")});
            this.lueKhachHang.Properties.NullText = "";
            this.lueKhachHang.Size = new System.Drawing.Size(108, 20);
            this.lueKhachHang.StyleController = this.layoutControl1;
            this.lueKhachHang.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem24});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(397, 585);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtSoHoaDon;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(188, 24);
            this.layoutControlItem1.Text = "Số hóa đơn";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.detNgayNhap;
            this.layoutControlItem2.Location = new System.Drawing.Point(188, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem2.Text = "Ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lueKhachHang;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(188, 24);
            this.layoutControlItem3.Text = "Khách hàng";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtNhanVien;
            this.layoutControlItem4.Location = new System.Drawing.Point(188, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem4.Text = "Nhân viên";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.grcHoaDonMua;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(377, 443);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtThanhTien;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 515);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(377, 24);
            this.layoutControlItem6.Text = "Tổng cộng";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnNhapLaiHoaDon;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 539);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(188, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnThanhToan;
            this.layoutControlItem8.Location = new System.Drawing.Point(188, 539);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(189, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtTienCongThem;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 491);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(377, 24);
            this.layoutControlItem24.Text = "Tiền cộng thêm";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(73, 13);
            // 
            // dxerHamLuongVang
            // 
            this.dxerHamLuongVang.ContainerControl = this;
            // 
            // dxerLyTinh
            // 
            this.dxerLyTinh.ContainerControl = this;
            // 
            // dxerLyDa
            // 
            this.dxerLyDa.ContainerControl = this;
            // 
            // dxerLyVang
            // 
            this.dxerLyVang.ContainerControl = this;
            // 
            // dxerTiLeHot
            // 
            this.dxerTiLeHot.ContainerControl = this;
            // 
            // dxerTenSanPham
            // 
            this.dxerTenSanPham.ContainerControl = this;
            // 
            // dxerGiaMua
            // 
            this.dxerGiaMua.ContainerControl = this;
            // 
            // frmMuaVang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 647);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frmMuaVang";
            this.Text = "Mua vàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMuaVang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDonGiaMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCongThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcHoaDonMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHoaDonMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHoaDon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerGiaMua)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtNhanVien;
        private DevExpress.XtraEditors.TextEdit txtSoHoaDon;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DateEdit detNgayNhap;
        private DevExpress.XtraEditors.LookUpEdit lueKhachHang;
        private DevExpress.XtraEditors.TextEdit txtThanhTien;
        private DevExpress.XtraGrid.GridControl grcHoaDonMua;
        private DevExpress.XtraGrid.Views.Grid.GridView grvHoaDonMua;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton btnThanhToan;
        private DevExpress.XtraEditors.SimpleButton btnNhapLaiHoaDon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtTiLeHot;
        private DevExpress.XtraEditors.TextEdit txtTenSanPham;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton btnThemVaoHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnNhapLaiSanPham;
        private DevExpress.XtraEditors.TextEdit txtGiaMua;
        private DevExpress.XtraEditors.TextEdit txtLyVang;
        private DevExpress.XtraEditors.TextEdit txtPhanVang;
        private DevExpress.XtraEditors.TextEdit txtChiVang;
        private DevExpress.XtraEditors.TextEdit txtLyDa;
        private DevExpress.XtraEditors.TextEdit txtPhanDa;
        private DevExpress.XtraEditors.TextEdit txtChiDa;
        private DevExpress.XtraEditors.TextEdit txtLyTinh;
        private DevExpress.XtraEditors.TextEdit txtPhanTinh;
        private DevExpress.XtraEditors.TextEdit txtChiTinh;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHamLuongVang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyTinh;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyDa;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyVang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTiLeHot;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTenSanPham;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerGiaMua;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongVang;
        private DevExpress.XtraEditors.TextEdit txtTienCongThem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaMua;
        private DevExpress.XtraGrid.Columns.GridColumn grclButtonXoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoa;
        private DevExpress.XtraEditors.TextEdit txtDonGiaMua;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
    }
}