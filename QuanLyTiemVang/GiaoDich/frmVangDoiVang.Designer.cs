﻿namespace QuanLyTiemVang.VANGDOIVANG
{
    partial class frmVangDoiVang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTimSanPham = new DevExpress.XtraEditors.TextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclTem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclMaSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTiLeHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclMoTa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.grpHoaDon = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTongTienVangBanRa = new DevExpress.XtraEditors.TextEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.txtSoHoaDon = new DevExpress.XtraEditors.TextEdit();
            this.txtTienConLai = new DevExpress.XtraEditors.TextEdit();
            this.txtTongTienVangCu = new DevExpress.XtraEditors.TextEdit();
            this.txtThanhTienVangDoiVang = new DevExpress.XtraEditors.TextEdit();
            this.txtTienBot = new DevExpress.XtraEditors.TextEdit();
            this.txtTienCongThem = new DevExpress.XtraEditors.TextEdit();
            this.txtKLVMoiTruKLVCu = new DevExpress.XtraEditors.TextEdit();
            this.txtCuoiCung = new DevExpress.XtraEditors.TextEdit();
            this.txtGiaBanMua = new DevExpress.XtraEditors.TextEdit();
            this.btnThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhapLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.grcVangVao = new DevExpress.XtraGrid.GridControl();
            this.grvVangVao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclXoaKhoiMatHangCu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaKhoiMatHangCu = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.grcVangRa = new DevExpress.XtraGrid.GridControl();
            this.grvVangRa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclTenSanPham_grcHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTem_grcHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaBan_grcHoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHanhDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDeleteItem = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.txtNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.lueKhachHang = new DevExpress.XtraEditors.LookUpEdit();
            this.detNgayBan = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.grcThongTinSanPhamMoi = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.lueHamLuongVangMoi = new DevExpress.XtraEditors.LookUpEdit();
            this.txtLyVangMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVangMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDaMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDaMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtLyTinhMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinhMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtTienCong = new DevExpress.XtraEditors.TextEdit();
            this.txtChiVangMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtChiDaMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinhMoi = new DevExpress.XtraEditors.TextEdit();
            this.btnNhapLai = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemSanPhamMoiVaoHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.txtGiaBan = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHotMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtTenSanPhamMoi = new DevExpress.XtraEditors.TextEdit();
            this.txtMaTemMoi = new DevExpress.XtraEditors.TextEdit();
            this.lueLoaiVangBanRa = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.grpDanhSachSanPham = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.grcDanhSachSanPhamTaiKhoMoi = new DevExpress.XtraGrid.GridControl();
            this.grvDanhSachSanPhamTaiKhoMoi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcThongTinSanPhamCu = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.lueLoaiVangMuaVao = new DevExpress.XtraEditors.LookUpEdit();
            this.lueHamLuongVangCu = new DevExpress.XtraEditors.LookUpEdit();
            this.txtLyVangCu = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVangCu = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDaCu = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDaCu = new DevExpress.XtraEditors.TextEdit();
            this.txtLyTinhCu = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinhCu = new DevExpress.XtraEditors.TextEdit();
            this.btnNhapLaiSanPhamCu = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemSanPhamCuVaoHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.txtTenSanPhamCu = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinhCu = new DevExpress.XtraEditors.TextEdit();
            this.txtChiDaCu = new DevExpress.XtraEditors.TextEdit();
            this.txtGiaMua = new DevExpress.XtraEditors.TextEdit();
            this.txtChiVangCu = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHotCu = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.control = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl3 = new DevExpress.XtraEditors.SplitterControl();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dxerHamLuongVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerTiLeHot = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerLyTinhCu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerLyDaCu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerLyVangCu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerTenSanPhamCu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerLoaiVangMuaVao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerLoaiVangBanRa = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.ckeChinhSuaTienCong = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimSanPham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpHoaDon)).BeginInit();
            this.grpHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienVangBanRa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHoaDon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienConLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienVangCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhTienVangDoiVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCongThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKLVMoiTruKLVCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCuoiCung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBanMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaKhoiMatHangCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongTinSanPhamMoi)).BeginInit();
            this.grcThongTinSanPhamMoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVangMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVangMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVangMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDaMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDaMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinhMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinhMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVangMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDaMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinhMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPhamMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTemMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiVangBanRa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachSanPham)).BeginInit();
            this.grpDanhSachSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachSanPhamTaiKhoMoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachSanPhamTaiKhoMoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongTinSanPhamCu)).BeginInit();
            this.grcThongTinSanPhamCu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiVangMuaVao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVangCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVangCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVangCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDaCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDaCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinhCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinhCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPhamCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinhCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDaCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVangCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.control)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinhCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDaCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVangCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPhamCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiVangMuaVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiVangBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckeChinhSuaTienCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.txtTimSanPham);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(2, 20);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = null;
            this.layoutControl4.Size = new System.Drawing.Size(664, 162);
            this.layoutControl4.TabIndex = 0;
            // 
            // txtTimSanPham
            // 
            this.txtTimSanPham.Location = new System.Drawing.Point(80, 12);
            this.txtTimSanPham.Name = "txtTimSanPham";
            this.txtTimSanPham.Size = new System.Drawing.Size(572, 22);
            this.txtTimSanPham.TabIndex = 4;
            // 
            // gridView2
            // 
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            // 
            // grclTem
            // 
            this.grclTem.Caption = "Mã tem";
            this.grclTem.FieldName = "MaTem";
            this.grclTem.Name = "grclTem";
            this.grclTem.Visible = true;
            this.grclTem.VisibleIndex = 5;
            this.grclTem.Width = 64;
            // 
            // grclMaSanPham
            // 
            this.grclMaSanPham.Caption = "Mã sản phẩm";
            this.grclMaSanPham.FieldName = "MaSanPham";
            this.grclMaSanPham.Name = "grclMaSanPham";
            this.grclMaSanPham.Visible = true;
            this.grclMaSanPham.VisibleIndex = 0;
            this.grclMaSanPham.Width = 52;
            // 
            // grclTenSanPham
            // 
            this.grclTenSanPham.Caption = "Tên sản phẩm";
            this.grclTenSanPham.FieldName = "TenSanPham";
            this.grclTenSanPham.Name = "grclTenSanPham";
            this.grclTenSanPham.Visible = true;
            this.grclTenSanPham.VisibleIndex = 1;
            this.grclTenSanPham.Width = 83;
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "HLV";
            this.grclHamLuongVang.FieldName = "HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 2;
            this.grclHamLuongVang.Width = 42;
            // 
            // grclTiLeHot
            // 
            this.grclTiLeHot.Caption = "TLH";
            this.grclTiLeHot.FieldName = "TiLeHot";
            this.grclTiLeHot.Name = "grclTiLeHot";
            this.grclTiLeHot.Visible = true;
            this.grclTiLeHot.VisibleIndex = 3;
            this.grclTiLeHot.Width = 45;
            // 
            // grclGiaBan
            // 
            this.grclGiaBan.Caption = "Giá Bán";
            this.grclGiaBan.FieldName = "GiaBan";
            this.grclGiaBan.Name = "grclGiaBan";
            this.grclGiaBan.Visible = true;
            this.grclGiaBan.VisibleIndex = 4;
            this.grclGiaBan.Width = 63;
            // 
            // grclKhoiLuongTinh
            // 
            this.grclKhoiLuongTinh.Caption = "KLT";
            this.grclKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.grclKhoiLuongTinh.Name = "grclKhoiLuongTinh";
            this.grclKhoiLuongTinh.Visible = true;
            this.grclKhoiLuongTinh.VisibleIndex = 6;
            this.grclKhoiLuongTinh.Width = 64;
            // 
            // grclKhoiLuongDa
            // 
            this.grclKhoiLuongDa.Caption = "KLĐ";
            this.grclKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.grclKhoiLuongDa.Name = "grclKhoiLuongDa";
            this.grclKhoiLuongDa.Visible = true;
            this.grclKhoiLuongDa.VisibleIndex = 7;
            this.grclKhoiLuongDa.Width = 64;
            // 
            // grclKhoiLuongVang
            // 
            this.grclKhoiLuongVang.Caption = "KLV";
            this.grclKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.grclKhoiLuongVang.Name = "grclKhoiLuongVang";
            this.grclKhoiLuongVang.Visible = true;
            this.grclKhoiLuongVang.VisibleIndex = 8;
            this.grclKhoiLuongVang.Width = 64;
            // 
            // grclMoTa
            // 
            this.grclMoTa.Caption = "Mô tả";
            this.grclMoTa.FieldName = "MoTa";
            this.grclMoTa.Name = "grclMoTa";
            this.grclMoTa.Visible = true;
            this.grclMoTa.VisibleIndex = 9;
            this.grclMoTa.Width = 86;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup3";
            this.layoutControlGroup4.Size = new System.Drawing.Size(664, 162);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // grpHoaDon
            // 
            this.grpHoaDon.Controls.Add(this.layoutControl2);
            this.grpHoaDon.Dock = System.Windows.Forms.DockStyle.Right;
            this.grpHoaDon.Location = new System.Drawing.Point(554, 59);
            this.grpHoaDon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpHoaDon.Name = "grpHoaDon";
            this.grpHoaDon.Size = new System.Drawing.Size(622, 668);
            this.grpHoaDon.TabIndex = 4;
            this.grpHoaDon.Text = "Hóa đơn";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.ckeChinhSuaTienCong);
            this.layoutControl2.Controls.Add(this.txtTongTienVangBanRa);
            this.layoutControl2.Controls.Add(this.txtSoHoaDon);
            this.layoutControl2.Controls.Add(this.txtTienConLai);
            this.layoutControl2.Controls.Add(this.txtTongTienVangCu);
            this.layoutControl2.Controls.Add(this.txtThanhTienVangDoiVang);
            this.layoutControl2.Controls.Add(this.txtTienBot);
            this.layoutControl2.Controls.Add(this.txtTienCongThem);
            this.layoutControl2.Controls.Add(this.txtKLVMoiTruKLVCu);
            this.layoutControl2.Controls.Add(this.txtCuoiCung);
            this.layoutControl2.Controls.Add(this.txtGiaBanMua);
            this.layoutControl2.Controls.Add(this.btnThanhToan);
            this.layoutControl2.Controls.Add(this.btnNhapLaiHoaDon);
            this.layoutControl2.Controls.Add(this.grcVangVao);
            this.layoutControl2.Controls.Add(this.grcVangRa);
            this.layoutControl2.Controls.Add(this.txtNhanVien);
            this.layoutControl2.Controls.Add(this.lueKhachHang);
            this.layoutControl2.Controls.Add(this.detNgayBan);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 25);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(618, 641);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txtTongTienVangBanRa
            // 
            this.txtTongTienVangBanRa.Location = new System.Drawing.Point(161, 542);
            this.txtTongTienVangBanRa.MenuManager = this.barManager1;
            this.txtTongTienVangBanRa.Name = "txtTongTienVangBanRa";
            this.txtTongTienVangBanRa.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTongTienVangBanRa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongTienVangBanRa.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongTienVangBanRa.Properties.NullText = "0";
            this.txtTongTienVangBanRa.Properties.ReadOnly = true;
            this.txtTongTienVangBanRa.Size = new System.Drawing.Size(441, 22);
            this.txtTongTienVangBanRa.StyleController = this.layoutControl2;
            this.txtTongTienVangBanRa.TabIndex = 19;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLamMoiDuLieu});
            this.barManager1.MaxItemId = 1;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 0;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barDockControlTop.Size = new System.Drawing.Size(1176, 59);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 727);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barDockControlBottom.Size = new System.Drawing.Size(1176, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 59);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 668);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1176, 59);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 668);
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(161, 16);
            this.txtSoHoaDon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSoHoaDon.MenuManager = this.barManager1;
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Properties.ReadOnly = true;
            this.txtSoHoaDon.Size = new System.Drawing.Size(144, 22);
            this.txtSoHoaDon.StyleController = this.layoutControl2;
            this.txtSoHoaDon.TabIndex = 18;
            // 
            // txtTienConLai
            // 
            this.txtTienConLai.Location = new System.Drawing.Point(161, 458);
            this.txtTienConLai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienConLai.MenuManager = this.barManager1;
            this.txtTienConLai.Name = "txtTienConLai";
            this.txtTienConLai.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienConLai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienConLai.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienConLai.Properties.MaxLength = 9;
            this.txtTienConLai.Properties.ReadOnly = true;
            this.txtTienConLai.Size = new System.Drawing.Size(441, 22);
            this.txtTienConLai.StyleController = this.layoutControl2;
            this.txtTienConLai.TabIndex = 17;
            // 
            // txtTongTienVangCu
            // 
            this.txtTongTienVangCu.Location = new System.Drawing.Point(161, 430);
            this.txtTongTienVangCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTongTienVangCu.MenuManager = this.barManager1;
            this.txtTongTienVangCu.Name = "txtTongTienVangCu";
            this.txtTongTienVangCu.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTongTienVangCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongTienVangCu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongTienVangCu.Properties.MaxLength = 9;
            this.txtTongTienVangCu.Properties.ReadOnly = true;
            this.txtTongTienVangCu.Size = new System.Drawing.Size(441, 22);
            this.txtTongTienVangCu.StyleController = this.layoutControl2;
            this.txtTongTienVangCu.TabIndex = 16;
            // 
            // txtThanhTienVangDoiVang
            // 
            this.txtThanhTienVangDoiVang.Location = new System.Drawing.Point(161, 402);
            this.txtThanhTienVangDoiVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtThanhTienVangDoiVang.MenuManager = this.barManager1;
            this.txtThanhTienVangDoiVang.Name = "txtThanhTienVangDoiVang";
            this.txtThanhTienVangDoiVang.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtThanhTienVangDoiVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThanhTienVangDoiVang.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtThanhTienVangDoiVang.Properties.MaxLength = 9;
            this.txtThanhTienVangDoiVang.Properties.ReadOnly = true;
            this.txtThanhTienVangDoiVang.Size = new System.Drawing.Size(441, 22);
            this.txtThanhTienVangDoiVang.StyleController = this.layoutControl2;
            this.txtThanhTienVangDoiVang.TabIndex = 15;
            // 
            // txtTienBot
            // 
            this.txtTienBot.Location = new System.Drawing.Point(161, 514);
            this.txtTienBot.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienBot.MenuManager = this.barManager1;
            this.txtTienBot.Name = "txtTienBot";
            this.txtTienBot.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienBot.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienBot.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienBot.Properties.MaxLength = 9;
            this.txtTienBot.Size = new System.Drawing.Size(441, 22);
            this.txtTienBot.StyleController = this.layoutControl2;
            this.txtTienBot.TabIndex = 14;
            this.txtTienBot.EditValueChanged += new System.EventHandler(this.txtTienBot_EditValueChanged);
            // 
            // txtTienCongThem
            // 
            this.txtTienCongThem.Location = new System.Drawing.Point(161, 486);
            this.txtTienCongThem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienCongThem.MenuManager = this.barManager1;
            this.txtTienCongThem.Name = "txtTienCongThem";
            this.txtTienCongThem.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienCongThem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienCongThem.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienCongThem.Properties.MaxLength = 9;
            this.txtTienCongThem.Properties.ReadOnly = true;
            this.txtTienCongThem.Size = new System.Drawing.Size(195, 22);
            this.txtTienCongThem.StyleController = this.layoutControl2;
            this.txtTienCongThem.TabIndex = 13;
            this.txtTienCongThem.EditValueChanged += new System.EventHandler(this.txtTienCongThem_EditValueChanged);
            // 
            // txtKLVMoiTruKLVCu
            // 
            this.txtKLVMoiTruKLVCu.Location = new System.Drawing.Point(161, 346);
            this.txtKLVMoiTruKLVCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtKLVMoiTruKLVCu.Name = "txtKLVMoiTruKLVCu";
            this.txtKLVMoiTruKLVCu.Properties.MaxLength = 9;
            this.txtKLVMoiTruKLVCu.Properties.ReadOnly = true;
            this.txtKLVMoiTruKLVCu.Size = new System.Drawing.Size(441, 22);
            this.txtKLVMoiTruKLVCu.StyleController = this.layoutControl2;
            this.txtKLVMoiTruKLVCu.TabIndex = 12;
            // 
            // txtCuoiCung
            // 
            this.txtCuoiCung.Location = new System.Drawing.Point(161, 570);
            this.txtCuoiCung.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCuoiCung.Name = "txtCuoiCung";
            this.txtCuoiCung.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtCuoiCung.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCuoiCung.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCuoiCung.Properties.MaxLength = 9;
            this.txtCuoiCung.Properties.ReadOnly = true;
            this.txtCuoiCung.Size = new System.Drawing.Size(441, 22);
            this.txtCuoiCung.StyleController = this.layoutControl2;
            this.txtCuoiCung.TabIndex = 11;
            // 
            // txtGiaBanMua
            // 
            this.txtGiaBanMua.Location = new System.Drawing.Point(161, 374);
            this.txtGiaBanMua.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGiaBanMua.Name = "txtGiaBanMua";
            this.txtGiaBanMua.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtGiaBanMua.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaBanMua.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGiaBanMua.Properties.MaxLength = 9;
            this.txtGiaBanMua.Properties.ReadOnly = true;
            this.txtGiaBanMua.Size = new System.Drawing.Size(441, 22);
            this.txtGiaBanMua.StyleController = this.layoutControl2;
            this.txtGiaBanMua.TabIndex = 10;
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnThanhToan.Appearance.Options.UseForeColor = true;
            this.btnThanhToan.Location = new System.Drawing.Point(311, 598);
            this.btnThanhToan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(291, 27);
            this.btnThanhToan.StyleController = this.layoutControl2;
            this.btnThanhToan.TabIndex = 8;
            this.btnThanhToan.Text = "Thanh Toán";
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // btnNhapLaiHoaDon
            // 
            this.btnNhapLaiHoaDon.Location = new System.Drawing.Point(16, 598);
            this.btnNhapLaiHoaDon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNhapLaiHoaDon.Name = "btnNhapLaiHoaDon";
            this.btnNhapLaiHoaDon.Size = new System.Drawing.Size(289, 27);
            this.btnNhapLaiHoaDon.StyleController = this.layoutControl2;
            this.btnNhapLaiHoaDon.TabIndex = 7;
            this.btnNhapLaiHoaDon.Text = "Nhập lại tất cả";
            this.btnNhapLaiHoaDon.Click += new System.EventHandler(this.btnNhapLaiHoaDon_Click);
            // 
            // grcVangVao
            // 
            this.grcVangVao.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangVao.Location = new System.Drawing.Point(161, 212);
            this.grcVangVao.MainView = this.grvVangVao;
            this.grcVangVao.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangVao.Name = "grcVangVao";
            this.grcVangVao.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaKhoiMatHangCu});
            this.grcVangVao.Size = new System.Drawing.Size(441, 128);
            this.grcVangVao.TabIndex = 6;
            this.grcVangVao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvVangVao});
            // 
            // grvVangVao
            // 
            this.grvVangVao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn2,
            this.grclGiaMua,
            this.grclXoaKhoiMatHangCu});
            this.grvVangVao.GridControl = this.grcVangVao;
            this.grvVangVao.Name = "grvVangVao";
            this.grvVangVao.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvVangVao.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tên vàng";
            this.gridColumn1.FieldName = "TenSanPham";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "HLV";
            this.gridColumn3.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "KL Vàng";
            this.gridColumn2.FieldName = "KhoiLuongVang";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // grclGiaMua
            // 
            this.grclGiaMua.Caption = "Giá mua";
            this.grclGiaMua.DisplayFormat.FormatString = "n0";
            this.grclGiaMua.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaMua.FieldName = "GiaMuaVao";
            this.grclGiaMua.Name = "grclGiaMua";
            this.grclGiaMua.Visible = true;
            this.grclGiaMua.VisibleIndex = 3;
            // 
            // grclXoaKhoiMatHangCu
            // 
            this.grclXoaKhoiMatHangCu.Caption = "Xóa";
            this.grclXoaKhoiMatHangCu.ColumnEdit = this.btnXoaKhoiMatHangCu;
            this.grclXoaKhoiMatHangCu.Name = "grclXoaKhoiMatHangCu";
            this.grclXoaKhoiMatHangCu.Visible = true;
            this.grclXoaKhoiMatHangCu.VisibleIndex = 4;
            // 
            // btnXoaKhoiMatHangCu
            // 
            this.btnXoaKhoiMatHangCu.AutoHeight = false;
            this.btnXoaKhoiMatHangCu.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xóa", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.btnXoaKhoiMatHangCu.Name = "btnXoaKhoiMatHangCu";
            this.btnXoaKhoiMatHangCu.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaKhoiMatHangCu.Click += new System.EventHandler(this.btnXoaKhoiMatHangCu_Click);
            // 
            // grcVangRa
            // 
            this.grcVangRa.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangRa.Location = new System.Drawing.Point(161, 72);
            this.grcVangRa.MainView = this.grvVangRa;
            this.grcVangRa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangRa.Name = "grcVangRa";
            this.grcVangRa.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDeleteItem});
            this.grcVangRa.Size = new System.Drawing.Size(441, 134);
            this.grcVangRa.TabIndex = 5;
            this.grcVangRa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvVangRa});
            // 
            // grvVangRa
            // 
            this.grvVangRa.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclTenSanPham_grcHoaDon,
            this.grclTem_grcHoaDon,
            this.gridColumn4,
            this.KhoiLuongVang,
            this.grclGiaBan_grcHoaDon,
            this.grclHanhDong});
            this.grvVangRa.GridControl = this.grcVangRa;
            this.grvVangRa.Name = "grvVangRa";
            this.grvVangRa.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvVangRa.OptionsView.ShowGroupPanel = false;
            // 
            // grclTenSanPham_grcHoaDon
            // 
            this.grclTenSanPham_grcHoaDon.Caption = "Tên vàng";
            this.grclTenSanPham_grcHoaDon.FieldName = "TenSanPham";
            this.grclTenSanPham_grcHoaDon.Name = "grclTenSanPham_grcHoaDon";
            this.grclTenSanPham_grcHoaDon.Visible = true;
            this.grclTenSanPham_grcHoaDon.VisibleIndex = 0;
            this.grclTenSanPham_grcHoaDon.Width = 78;
            // 
            // grclTem_grcHoaDon
            // 
            this.grclTem_grcHoaDon.Caption = "Mã tem";
            this.grclTem_grcHoaDon.FieldName = "MaTem";
            this.grclTem_grcHoaDon.Name = "grclTem_grcHoaDon";
            this.grclTem_grcHoaDon.Visible = true;
            this.grclTem_grcHoaDon.VisibleIndex = 1;
            this.grclTem_grcHoaDon.Width = 80;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "HLV";
            this.gridColumn4.FieldName = "HamLuongVang";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            this.gridColumn4.Width = 50;
            // 
            // KhoiLuongVang
            // 
            this.KhoiLuongVang.Caption = "KL Vàng";
            this.KhoiLuongVang.FieldName = "KhoiLuongVang";
            this.KhoiLuongVang.Name = "KhoiLuongVang";
            this.KhoiLuongVang.Visible = true;
            this.KhoiLuongVang.VisibleIndex = 3;
            // 
            // grclGiaBan_grcHoaDon
            // 
            this.grclGiaBan_grcHoaDon.Caption = "Giá Bán";
            this.grclGiaBan_grcHoaDon.DisplayFormat.FormatString = "n0";
            this.grclGiaBan_grcHoaDon.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaBan_grcHoaDon.FieldName = "GiaBan";
            this.grclGiaBan_grcHoaDon.Name = "grclGiaBan_grcHoaDon";
            this.grclGiaBan_grcHoaDon.Visible = true;
            this.grclGiaBan_grcHoaDon.VisibleIndex = 4;
            this.grclGiaBan_grcHoaDon.Width = 70;
            // 
            // grclHanhDong
            // 
            this.grclHanhDong.Caption = "Xóa";
            this.grclHanhDong.ColumnEdit = this.btnDeleteItem;
            this.grclHanhDong.Name = "grclHanhDong";
            this.grclHanhDong.Visible = true;
            this.grclHanhDong.VisibleIndex = 5;
            this.grclHanhDong.Width = 76;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.AutoHeight = false;
            this.btnDeleteItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xóa", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // txtNhanVien
            // 
            this.txtNhanVien.Location = new System.Drawing.Point(456, 44);
            this.txtNhanVien.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNhanVien.Name = "txtNhanVien";
            this.txtNhanVien.Properties.ReadOnly = true;
            this.txtNhanVien.Size = new System.Drawing.Size(146, 22);
            this.txtNhanVien.StyleController = this.layoutControl2;
            this.txtNhanVien.TabIndex = 4;
            // 
            // lueKhachHang
            // 
            this.lueKhachHang.Location = new System.Drawing.Point(161, 44);
            this.lueKhachHang.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueKhachHang.Name = "lueKhachHang";
            this.lueKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKhachHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HoLot", "Họ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Ten", "Tên")});
            this.lueKhachHang.Properties.NullText = "";
            this.lueKhachHang.Size = new System.Drawing.Size(144, 22);
            this.lueKhachHang.StyleController = this.layoutControl2;
            this.lueKhachHang.TabIndex = 3;
            // 
            // detNgayBan
            // 
            this.detNgayBan.EditValue = null;
            this.detNgayBan.Location = new System.Drawing.Point(456, 16);
            this.detNgayBan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detNgayBan.Name = "detNgayBan";
            this.detNgayBan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayBan.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayBan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.detNgayBan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayBan.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayBan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayBan.Properties.ReadOnly = true;
            this.detNgayBan.Size = new System.Drawing.Size(146, 22);
            this.detNgayBan.StyleController = this.layoutControl2;
            this.detNgayBan.TabIndex = 2;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem13,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem17,
            this.layoutControlItem42,
            this.layoutControlItem46,
            this.layoutControlItem3,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem16,
            this.layoutControlItem51,
            this.layoutControlItem52});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(618, 641);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.grcVangRa;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(592, 140);
            this.layoutControlItem11.Text = "Vàng ra";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(142, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.grcVangVao;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(592, 134);
            this.layoutControlItem12.Text = "Vàng vào";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(142, 16);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.btnNhapLaiHoaDon;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 582);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(295, 33);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnThanhToan;
            this.layoutControlItem15.Location = new System.Drawing.Point(295, 582);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(297, 33);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.detNgayBan;
            this.layoutControlItem13.Location = new System.Drawing.Point(295, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(297, 28);
            this.layoutControlItem13.Text = "Ngày";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(142, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtGiaBanMua;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 358);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem4.Text = "Giá bán / mua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(142, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtCuoiCung;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 554);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem5.Text = "Thu tiền 3 + 4 - 5 + 6";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtNhanVien;
            this.layoutControlItem17.Location = new System.Drawing.Point(295, 28);
            this.layoutControlItem17.Name = "layoutControlItem9";
            this.layoutControlItem17.Size = new System.Drawing.Size(297, 28);
            this.layoutControlItem17.Text = "Nhân viên";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(142, 16);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.txtTienCongThem;
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 470);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(346, 28);
            this.layoutControlItem42.Text = "Tiền công (4)";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.txtTienBot;
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 498);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem46.Text = "Tiền bớt (5)";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtKLVMoiTruKLVCu;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 330);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem3.Text = "KLV mới - KLV cũ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.txtThanhTienVangDoiVang;
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 386);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem47.Text = "Thành tiền (1)";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.txtTongTienVangCu;
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 414);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem48.Text = "Tổng tiền vàng cũ (2)";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txtTienConLai;
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 442);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem49.Text = "Còn lại (3) = (1) - (2)";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.txtSoHoaDon;
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(295, 28);
            this.layoutControlItem50.Text = "Số hóa đơn";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(142, 17);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.lueKhachHang;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem16.Name = "layoutControlItem11";
            this.layoutControlItem16.Size = new System.Drawing.Size(295, 28);
            this.layoutControlItem16.Text = "Khách Hàng";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(142, 16);
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.txtTongTienVangBanRa;
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 526);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(592, 28);
            this.layoutControlItem51.Text = "Tổng tiền vàng bán (6)";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(142, 17);
            // 
            // grcThongTinSanPhamMoi
            // 
            this.grcThongTinSanPhamMoi.Controls.Add(this.layoutControl3);
            this.grcThongTinSanPhamMoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.grcThongTinSanPhamMoi.Location = new System.Drawing.Point(2, 246);
            this.grcThongTinSanPhamMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcThongTinSanPhamMoi.Name = "grcThongTinSanPhamMoi";
            this.grcThongTinSanPhamMoi.Size = new System.Drawing.Size(544, 241);
            this.grcThongTinSanPhamMoi.TabIndex = 5;
            this.grcThongTinSanPhamMoi.Text = "Thông tin sản phẩm tại kho mới";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.lueHamLuongVangMoi);
            this.layoutControl3.Controls.Add(this.txtLyVangMoi);
            this.layoutControl3.Controls.Add(this.txtPhanVangMoi);
            this.layoutControl3.Controls.Add(this.txtLyDaMoi);
            this.layoutControl3.Controls.Add(this.txtPhanDaMoi);
            this.layoutControl3.Controls.Add(this.txtLyTinhMoi);
            this.layoutControl3.Controls.Add(this.txtPhanTinhMoi);
            this.layoutControl3.Controls.Add(this.txtTienCong);
            this.layoutControl3.Controls.Add(this.txtChiVangMoi);
            this.layoutControl3.Controls.Add(this.txtChiDaMoi);
            this.layoutControl3.Controls.Add(this.txtChiTinhMoi);
            this.layoutControl3.Controls.Add(this.btnNhapLai);
            this.layoutControl3.Controls.Add(this.btnThemSanPhamMoiVaoHoaDon);
            this.layoutControl3.Controls.Add(this.txtGiaBan);
            this.layoutControl3.Controls.Add(this.txtTiLeHotMoi);
            this.layoutControl3.Controls.Add(this.txtTenSanPhamMoi);
            this.layoutControl3.Controls.Add(this.txtMaTemMoi);
            this.layoutControl3.Controls.Add(this.lueLoaiVangBanRa);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 25);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(429, 148, 250, 350);
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(540, 214);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // lueHamLuongVangMoi
            // 
            this.lueHamLuongVangMoi.Location = new System.Drawing.Point(121, 72);
            this.lueHamLuongVangMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueHamLuongVangMoi.Name = "lueHamLuongVangMoi";
            this.lueHamLuongVangMoi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVangMoi.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVangMoi.Properties.NullText = "";
            this.lueHamLuongVangMoi.Size = new System.Drawing.Size(135, 22);
            this.lueHamLuongVangMoi.StyleController = this.layoutControl3;
            this.lueHamLuongVangMoi.TabIndex = 20;
            this.lueHamLuongVangMoi.EditValueChanged += new System.EventHandler(this.lueHamLuongVangMoi_EditValueChanged);
            // 
            // txtLyVangMoi
            // 
            this.txtLyVangMoi.Location = new System.Drawing.Point(449, 156);
            this.txtLyVangMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyVangMoi.Name = "txtLyVangMoi";
            this.txtLyVangMoi.Properties.ReadOnly = true;
            this.txtLyVangMoi.Size = new System.Drawing.Size(54, 22);
            this.txtLyVangMoi.StyleController = this.layoutControl3;
            this.txtLyVangMoi.TabIndex = 19;
            // 
            // txtPhanVangMoi
            // 
            this.txtPhanVangMoi.Location = new System.Drawing.Point(285, 156);
            this.txtPhanVangMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanVangMoi.Name = "txtPhanVangMoi";
            this.txtPhanVangMoi.Properties.ReadOnly = true;
            this.txtPhanVangMoi.Size = new System.Drawing.Size(53, 22);
            this.txtPhanVangMoi.StyleController = this.layoutControl3;
            this.txtPhanVangMoi.TabIndex = 18;
            // 
            // txtLyDaMoi
            // 
            this.txtLyDaMoi.Location = new System.Drawing.Point(449, 128);
            this.txtLyDaMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDaMoi.Name = "txtLyDaMoi";
            this.txtLyDaMoi.Properties.ReadOnly = true;
            this.txtLyDaMoi.Size = new System.Drawing.Size(54, 22);
            this.txtLyDaMoi.StyleController = this.layoutControl3;
            this.txtLyDaMoi.TabIndex = 17;
            // 
            // txtPhanDaMoi
            // 
            this.txtPhanDaMoi.Location = new System.Drawing.Point(285, 128);
            this.txtPhanDaMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanDaMoi.Name = "txtPhanDaMoi";
            this.txtPhanDaMoi.Properties.ReadOnly = true;
            this.txtPhanDaMoi.Size = new System.Drawing.Size(53, 22);
            this.txtPhanDaMoi.StyleController = this.layoutControl3;
            this.txtPhanDaMoi.TabIndex = 16;
            // 
            // txtLyTinhMoi
            // 
            this.txtLyTinhMoi.Location = new System.Drawing.Point(449, 100);
            this.txtLyTinhMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyTinhMoi.Name = "txtLyTinhMoi";
            this.txtLyTinhMoi.Properties.ReadOnly = true;
            this.txtLyTinhMoi.Size = new System.Drawing.Size(54, 22);
            this.txtLyTinhMoi.StyleController = this.layoutControl3;
            this.txtLyTinhMoi.TabIndex = 15;
            // 
            // txtPhanTinhMoi
            // 
            this.txtPhanTinhMoi.Location = new System.Drawing.Point(285, 100);
            this.txtPhanTinhMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanTinhMoi.Name = "txtPhanTinhMoi";
            this.txtPhanTinhMoi.Properties.ReadOnly = true;
            this.txtPhanTinhMoi.Size = new System.Drawing.Size(53, 22);
            this.txtPhanTinhMoi.StyleController = this.layoutControl3;
            this.txtPhanTinhMoi.TabIndex = 14;
            // 
            // txtTienCong
            // 
            this.txtTienCong.Location = new System.Drawing.Point(367, 184);
            this.txtTienCong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienCong.Name = "txtTienCong";
            this.txtTienCong.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienCong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienCong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienCong.Properties.ReadOnly = true;
            this.txtTienCong.Size = new System.Drawing.Size(136, 22);
            this.txtTienCong.StyleController = this.layoutControl3;
            this.txtTienCong.TabIndex = 13;
            // 
            // txtChiVangMoi
            // 
            this.txtChiVangMoi.Location = new System.Drawing.Point(121, 156);
            this.txtChiVangMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChiVangMoi.Name = "txtChiVangMoi";
            this.txtChiVangMoi.Properties.ReadOnly = true;
            this.txtChiVangMoi.Size = new System.Drawing.Size(53, 22);
            this.txtChiVangMoi.StyleController = this.layoutControl3;
            this.txtChiVangMoi.TabIndex = 5;
            // 
            // txtChiDaMoi
            // 
            this.txtChiDaMoi.Location = new System.Drawing.Point(121, 128);
            this.txtChiDaMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChiDaMoi.Name = "txtChiDaMoi";
            this.txtChiDaMoi.Properties.ReadOnly = true;
            this.txtChiDaMoi.Size = new System.Drawing.Size(53, 22);
            this.txtChiDaMoi.StyleController = this.layoutControl3;
            this.txtChiDaMoi.TabIndex = 7;
            // 
            // txtChiTinhMoi
            // 
            this.txtChiTinhMoi.Location = new System.Drawing.Point(121, 100);
            this.txtChiTinhMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChiTinhMoi.Name = "txtChiTinhMoi";
            this.txtChiTinhMoi.Properties.ReadOnly = true;
            this.txtChiTinhMoi.Size = new System.Drawing.Size(53, 22);
            this.txtChiTinhMoi.StyleController = this.layoutControl3;
            this.txtChiTinhMoi.TabIndex = 8;
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Location = new System.Drawing.Point(16, 212);
            this.btnNhapLai.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.Size = new System.Drawing.Size(227, 27);
            this.btnNhapLai.StyleController = this.layoutControl3;
            this.btnNhapLai.TabIndex = 11;
            this.btnNhapLai.Text = "Nhập lại sản phẩm mới";
            this.btnNhapLai.Click += new System.EventHandler(this.btnNhapLai_Click);
            // 
            // btnThemSanPhamMoiVaoHoaDon
            // 
            this.btnThemSanPhamMoiVaoHoaDon.Location = new System.Drawing.Point(249, 212);
            this.btnThemSanPhamMoiVaoHoaDon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnThemSanPhamMoiVaoHoaDon.Name = "btnThemSanPhamMoiVaoHoaDon";
            this.btnThemSanPhamMoiVaoHoaDon.Size = new System.Drawing.Size(254, 27);
            this.btnThemSanPhamMoiVaoHoaDon.StyleController = this.layoutControl3;
            this.btnThemSanPhamMoiVaoHoaDon.TabIndex = 12;
            this.btnThemSanPhamMoiVaoHoaDon.Text = "Thêm vào hóa đơn";
            this.btnThemSanPhamMoiVaoHoaDon.Click += new System.EventHandler(this.btnThemSanPhamMoiVaoHoaDon_Click);
            // 
            // txtGiaBan
            // 
            this.txtGiaBan.Location = new System.Drawing.Point(121, 184);
            this.txtGiaBan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGiaBan.Name = "txtGiaBan";
            this.txtGiaBan.Properties.DisplayFormat.FormatString = "c2";
            this.txtGiaBan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGiaBan.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtGiaBan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaBan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGiaBan.Properties.ReadOnly = true;
            this.txtGiaBan.Size = new System.Drawing.Size(135, 22);
            this.txtGiaBan.StyleController = this.layoutControl3;
            this.txtGiaBan.TabIndex = 9;
            // 
            // txtTiLeHotMoi
            // 
            this.txtTiLeHotMoi.Location = new System.Drawing.Point(367, 72);
            this.txtTiLeHotMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTiLeHotMoi.Name = "txtTiLeHotMoi";
            this.txtTiLeHotMoi.Properties.ReadOnly = true;
            this.txtTiLeHotMoi.Size = new System.Drawing.Size(136, 22);
            this.txtTiLeHotMoi.StyleController = this.layoutControl3;
            this.txtTiLeHotMoi.TabIndex = 6;
            // 
            // txtTenSanPhamMoi
            // 
            this.txtTenSanPhamMoi.Location = new System.Drawing.Point(367, 44);
            this.txtTenSanPhamMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTenSanPhamMoi.Name = "txtTenSanPhamMoi";
            this.txtTenSanPhamMoi.Properties.ReadOnly = true;
            this.txtTenSanPhamMoi.Size = new System.Drawing.Size(136, 22);
            this.txtTenSanPhamMoi.StyleController = this.layoutControl3;
            this.txtTenSanPhamMoi.TabIndex = 3;
            // 
            // txtMaTemMoi
            // 
            this.txtMaTemMoi.Location = new System.Drawing.Point(121, 44);
            this.txtMaTemMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMaTemMoi.Name = "txtMaTemMoi";
            this.txtMaTemMoi.Properties.Mask.EditMask = "(\\p{Lu}|\\d)+";
            this.txtMaTemMoi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMaTemMoi.Size = new System.Drawing.Size(135, 22);
            this.txtMaTemMoi.StyleController = this.layoutControl3;
            this.txtMaTemMoi.TabIndex = 0;
            this.txtMaTemMoi.TextChanged += new System.EventHandler(this.txtMaTem_TextChanged);
            // 
            // lueLoaiVangBanRa
            // 
            this.lueLoaiVangBanRa.Location = new System.Drawing.Point(121, 16);
            this.lueLoaiVangBanRa.MenuManager = this.barManager1;
            this.lueLoaiVangBanRa.Name = "lueLoaiVangBanRa";
            this.lueLoaiVangBanRa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiVangBanRa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Loại vàng")});
            this.lueLoaiVangBanRa.Properties.NullText = "";
            this.lueLoaiVangBanRa.Size = new System.Drawing.Size(382, 22);
            this.lueLoaiVangBanRa.StyleController = this.layoutControl3;
            this.lueLoaiVangBanRa.TabIndex = 21;
            this.lueLoaiVangBanRa.EditValueChanged += new System.EventHandler(this.lueLoaiVangBanRa_EditValueChanged);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem19,
            this.layoutControlItem24,
            this.layoutControlItem27,
            this.layoutControlItem29,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem28,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem18,
            this.layoutControlItem23,
            this.layoutControlItem37,
            this.layoutControlItem21,
            this.layoutControlItem30});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(519, 255);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txtMaTemMoi;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem22.Name = "layoutControlItem2";
            this.layoutControlItem22.Size = new System.Drawing.Size(246, 28);
            this.layoutControlItem22.Text = "Mã tem";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.btnThemSanPhamMoiVaoHoaDon;
            this.layoutControlItem25.Location = new System.Drawing.Point(233, 196);
            this.layoutControlItem25.Name = "layoutControlItem16";
            this.layoutControlItem25.Size = new System.Drawing.Size(260, 33);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.btnNhapLai;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem26.Name = "layoutControlItem19";
            this.layoutControlItem26.Size = new System.Drawing.Size(233, 33);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtTenSanPhamMoi;
            this.layoutControlItem19.Location = new System.Drawing.Point(246, 28);
            this.layoutControlItem19.Name = "layoutControlItem3";
            this.layoutControlItem19.Size = new System.Drawing.Size(247, 28);
            this.layoutControlItem19.Text = "Tên sản phẩm";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtGiaBan;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem24.Name = "layoutControlItem18";
            this.layoutControlItem24.Size = new System.Drawing.Size(246, 28);
            this.layoutControlItem24.Text = "Giá Bán";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txtChiVangMoi;
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem27.Name = "layoutControlItem22";
            this.layoutControlItem27.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem27.Text = "KLV: Chỉ";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txtChiTinhMoi;
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem29.Name = "layoutControlItem20";
            this.layoutControlItem29.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem29.Text = "KLT: Chỉ";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtTienCong;
            this.layoutControlItem6.Location = new System.Drawing.Point(246, 168);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(247, 28);
            this.layoutControlItem6.Text = "Tiền công";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtPhanTinhMoi;
            this.layoutControlItem7.Location = new System.Drawing.Point(164, 84);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem7.Text = "Phân";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtLyTinhMoi;
            this.layoutControlItem8.Location = new System.Drawing.Point(328, 84);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(165, 28);
            this.layoutControlItem8.Text = "Ly";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txtChiDaMoi;
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem28.Name = "layoutControlItem21";
            this.layoutControlItem28.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem28.Text = "KLĐ: Chỉ";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtPhanDaMoi;
            this.layoutControlItem9.Location = new System.Drawing.Point(164, 112);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem9.Text = "Phân";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtLyDaMoi;
            this.layoutControlItem10.Location = new System.Drawing.Point(328, 112);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(165, 28);
            this.layoutControlItem10.Text = "Ly";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtPhanVangMoi;
            this.layoutControlItem18.Location = new System.Drawing.Point(164, 140);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem18.Text = "Phân";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtLyVangMoi;
            this.layoutControlItem23.Location = new System.Drawing.Point(328, 140);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(165, 28);
            this.layoutControlItem23.Text = "Ly";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.lueHamLuongVangMoi;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(246, 28);
            this.layoutControlItem37.Text = "Hàm lượng vàng";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtTiLeHotMoi;
            this.layoutControlItem21.Location = new System.Drawing.Point(246, 56);
            this.layoutControlItem21.Name = "layoutControlItem4";
            this.layoutControlItem21.Size = new System.Drawing.Size(247, 28);
            this.layoutControlItem21.Text = "Tỉ lệ hột";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.lueLoaiVangBanRa;
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(493, 28);
            this.layoutControlItem30.Text = "Loại vàng";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(102, 17);
            // 
            // grpDanhSachSanPham
            // 
            this.grpDanhSachSanPham.Controls.Add(this.layoutControl5);
            this.grpDanhSachSanPham.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDanhSachSanPham.Location = new System.Drawing.Point(2, 25);
            this.grpDanhSachSanPham.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpDanhSachSanPham.Name = "grpDanhSachSanPham";
            this.grpDanhSachSanPham.Size = new System.Drawing.Size(544, 215);
            this.grpDanhSachSanPham.TabIndex = 6;
            this.grpDanhSachSanPham.Text = "Thông tin sản phẩm bán ra";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.grcDanhSachSanPhamTaiKhoMoi);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(2, 25);
            this.layoutControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup5;
            this.layoutControl5.Size = new System.Drawing.Size(540, 188);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // grcDanhSachSanPhamTaiKhoMoi
            // 
            this.grcDanhSachSanPhamTaiKhoMoi.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcDanhSachSanPhamTaiKhoMoi.Location = new System.Drawing.Point(16, 16);
            this.grcDanhSachSanPhamTaiKhoMoi.MainView = this.grvDanhSachSanPhamTaiKhoMoi;
            this.grcDanhSachSanPhamTaiKhoMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcDanhSachSanPhamTaiKhoMoi.Name = "grcDanhSachSanPhamTaiKhoMoi";
            this.grcDanhSachSanPhamTaiKhoMoi.Size = new System.Drawing.Size(508, 156);
            this.grcDanhSachSanPhamTaiKhoMoi.TabIndex = 8;
            this.grcDanhSachSanPhamTaiKhoMoi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDanhSachSanPhamTaiKhoMoi});
            this.grcDanhSachSanPhamTaiKhoMoi.DoubleClick += new System.EventHandler(this.grcDanhSachSanPhamTaiKhoMoi_DoubleClick);
            // 
            // grvDanhSachSanPhamTaiKhoMoi
            // 
            this.grvDanhSachSanPhamTaiKhoMoi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn8,
            this.gridColumn11});
            this.grvDanhSachSanPhamTaiKhoMoi.GridControl = this.grcDanhSachSanPhamTaiKhoMoi;
            this.grvDanhSachSanPhamTaiKhoMoi.Name = "grvDanhSachSanPhamTaiKhoMoi";
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsBehavior.Editable = false;
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsFind.AlwaysVisible = true;
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsFind.FindDelay = 100;
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsFind.FindNullPrompt = "";
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsFind.ShowFindButton = false;
            this.grvDanhSachSanPhamTaiKhoMoi.OptionsView.ShowGroupPanel = false;
            this.grvDanhSachSanPhamTaiKhoMoi.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvDanhSachSanPhamTaiKhoMoi_CustomDrawRowIndicator);
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Mã tem";
            this.gridColumn6.FieldName = "MaTem";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 64;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Tên sản phẩm";
            this.gridColumn8.FieldName = "TenSanPham";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 83;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Giá Bán";
            this.gridColumn11.DisplayFormat.FormatString = "n0";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn11.FieldName = "GiaBan";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 63;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem31});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup3";
            this.layoutControlGroup5.Size = new System.Drawing.Size(540, 188);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.grcDanhSachSanPhamTaiKhoMoi;
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem31.Name = "layoutControlItem17";
            this.layoutControlItem31.Size = new System.Drawing.Size(514, 162);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl1.Location = new System.Drawing.Point(548, 59);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 668);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcThongTinSanPhamCu);
            this.groupControl2.Controls.Add(this.splitterControl3);
            this.groupControl2.Controls.Add(this.grcThongTinSanPhamMoi);
            this.groupControl2.Controls.Add(this.splitterControl2);
            this.groupControl2.Controls.Add(this.grpDanhSachSanPham);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 59);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(548, 668);
            this.groupControl2.TabIndex = 7;
            this.groupControl2.Text = "Sản phẩm";
            // 
            // grcThongTinSanPhamCu
            // 
            this.grcThongTinSanPhamCu.Controls.Add(this.layoutControl6);
            this.grcThongTinSanPhamCu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcThongTinSanPhamCu.Location = new System.Drawing.Point(2, 493);
            this.grcThongTinSanPhamCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcThongTinSanPhamCu.Name = "grcThongTinSanPhamCu";
            this.grcThongTinSanPhamCu.Size = new System.Drawing.Size(544, 173);
            this.grcThongTinSanPhamCu.TabIndex = 9;
            this.grcThongTinSanPhamCu.Text = "Thông tin sản phẩm mua vào";
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.lueLoaiVangMuaVao);
            this.layoutControl6.Controls.Add(this.lueHamLuongVangCu);
            this.layoutControl6.Controls.Add(this.txtLyVangCu);
            this.layoutControl6.Controls.Add(this.txtPhanVangCu);
            this.layoutControl6.Controls.Add(this.txtLyDaCu);
            this.layoutControl6.Controls.Add(this.txtPhanDaCu);
            this.layoutControl6.Controls.Add(this.txtLyTinhCu);
            this.layoutControl6.Controls.Add(this.txtPhanTinhCu);
            this.layoutControl6.Controls.Add(this.btnNhapLaiSanPhamCu);
            this.layoutControl6.Controls.Add(this.btnThemSanPhamCuVaoHoaDon);
            this.layoutControl6.Controls.Add(this.txtTenSanPhamCu);
            this.layoutControl6.Controls.Add(this.txtChiTinhCu);
            this.layoutControl6.Controls.Add(this.txtChiDaCu);
            this.layoutControl6.Controls.Add(this.txtGiaMua);
            this.layoutControl6.Controls.Add(this.txtChiVangCu);
            this.layoutControl6.Controls.Add(this.txtTiLeHotCu);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(2, 25);
            this.layoutControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.Root = this.layoutControlGroup6;
            this.layoutControl6.Size = new System.Drawing.Size(540, 146);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // lueLoaiVangMuaVao
            // 
            this.lueLoaiVangMuaVao.Location = new System.Drawing.Point(121, 16);
            this.lueLoaiVangMuaVao.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueLoaiVangMuaVao.MenuManager = this.barManager1;
            this.lueLoaiVangMuaVao.Name = "lueLoaiVangMuaVao";
            this.lueLoaiVangMuaVao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiVangMuaVao.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Loại vàng")});
            this.lueLoaiVangMuaVao.Properties.NullText = "";
            this.lueLoaiVangMuaVao.Size = new System.Drawing.Size(135, 22);
            this.lueLoaiVangMuaVao.StyleController = this.layoutControl6;
            this.lueLoaiVangMuaVao.TabIndex = 19;
            this.lueLoaiVangMuaVao.EditValueChanged += new System.EventHandler(this.lueLoaiVangMuaVao_EditValueChanged);
            // 
            // lueHamLuongVangCu
            // 
            this.lueHamLuongVangCu.Location = new System.Drawing.Point(121, 44);
            this.lueHamLuongVangCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueHamLuongVangCu.Name = "lueHamLuongVangCu";
            this.lueHamLuongVangCu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVangCu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVangCu.Properties.NullText = "";
            this.lueHamLuongVangCu.Size = new System.Drawing.Size(135, 22);
            this.lueHamLuongVangCu.StyleController = this.layoutControl6;
            this.lueHamLuongVangCu.TabIndex = 18;
            this.lueHamLuongVangCu.EditValueChanged += new System.EventHandler(this.lueHamLuongVangCu_EditValueChanged);
            // 
            // txtLyVangCu
            // 
            this.txtLyVangCu.Location = new System.Drawing.Point(449, 128);
            this.txtLyVangCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyVangCu.Name = "txtLyVangCu";
            this.txtLyVangCu.Properties.NullText = "0";
            this.txtLyVangCu.Properties.ReadOnly = true;
            this.txtLyVangCu.Size = new System.Drawing.Size(54, 22);
            this.txtLyVangCu.StyleController = this.layoutControl6;
            this.txtLyVangCu.TabIndex = 17;
            // 
            // txtPhanVangCu
            // 
            this.txtPhanVangCu.Location = new System.Drawing.Point(285, 128);
            this.txtPhanVangCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanVangCu.Name = "txtPhanVangCu";
            this.txtPhanVangCu.Properties.Mask.EditMask = "d";
            this.txtPhanVangCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanVangCu.Properties.MaxLength = 1;
            this.txtPhanVangCu.Properties.NullText = "0";
            this.txtPhanVangCu.Properties.ReadOnly = true;
            this.txtPhanVangCu.Size = new System.Drawing.Size(53, 22);
            this.txtPhanVangCu.StyleController = this.layoutControl6;
            this.txtPhanVangCu.TabIndex = 16;
            // 
            // txtLyDaCu
            // 
            this.txtLyDaCu.Location = new System.Drawing.Point(449, 100);
            this.txtLyDaCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDaCu.Name = "txtLyDaCu";
            this.txtLyDaCu.Properties.NullText = "0";
            this.txtLyDaCu.Size = new System.Drawing.Size(54, 22);
            this.txtLyDaCu.StyleController = this.layoutControl6;
            this.txtLyDaCu.TabIndex = 15;
            this.txtLyDaCu.EditValueChanged += new System.EventHandler(this.txtLyDaCu_EditValueChanged);
            // 
            // txtPhanDaCu
            // 
            this.txtPhanDaCu.Location = new System.Drawing.Point(285, 100);
            this.txtPhanDaCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanDaCu.Name = "txtPhanDaCu";
            this.txtPhanDaCu.Properties.Mask.EditMask = "d";
            this.txtPhanDaCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanDaCu.Properties.MaxLength = 1;
            this.txtPhanDaCu.Properties.NullText = "0";
            this.txtPhanDaCu.Size = new System.Drawing.Size(53, 22);
            this.txtPhanDaCu.StyleController = this.layoutControl6;
            this.txtPhanDaCu.TabIndex = 14;
            this.txtPhanDaCu.EditValueChanged += new System.EventHandler(this.txtPhanDaCu_EditValueChanged);
            // 
            // txtLyTinhCu
            // 
            this.txtLyTinhCu.Location = new System.Drawing.Point(449, 72);
            this.txtLyTinhCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyTinhCu.Name = "txtLyTinhCu";
            this.txtLyTinhCu.Properties.NullText = "0";
            this.txtLyTinhCu.Size = new System.Drawing.Size(54, 22);
            this.txtLyTinhCu.StyleController = this.layoutControl6;
            this.txtLyTinhCu.TabIndex = 13;
            this.txtLyTinhCu.EditValueChanged += new System.EventHandler(this.txtLyTinhCu_EditValueChanged);
            // 
            // txtPhanTinhCu
            // 
            this.txtPhanTinhCu.Location = new System.Drawing.Point(285, 72);
            this.txtPhanTinhCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanTinhCu.Name = "txtPhanTinhCu";
            this.txtPhanTinhCu.Properties.Mask.EditMask = "d";
            this.txtPhanTinhCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanTinhCu.Properties.MaxLength = 1;
            this.txtPhanTinhCu.Properties.NullText = "0";
            this.txtPhanTinhCu.Size = new System.Drawing.Size(53, 22);
            this.txtPhanTinhCu.StyleController = this.layoutControl6;
            this.txtPhanTinhCu.TabIndex = 12;
            this.txtPhanTinhCu.EditValueChanged += new System.EventHandler(this.txtPhanTinhCu_EditValueChanged);
            // 
            // btnNhapLaiSanPhamCu
            // 
            this.btnNhapLaiSanPhamCu.Location = new System.Drawing.Point(16, 184);
            this.btnNhapLaiSanPhamCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNhapLaiSanPhamCu.Name = "btnNhapLaiSanPhamCu";
            this.btnNhapLaiSanPhamCu.Size = new System.Drawing.Size(240, 27);
            this.btnNhapLaiSanPhamCu.StyleController = this.layoutControl6;
            this.btnNhapLaiSanPhamCu.TabIndex = 11;
            this.btnNhapLaiSanPhamCu.Text = "Nhập lại sản phẩm cũ";
            this.btnNhapLaiSanPhamCu.Click += new System.EventHandler(this.btnNhapLaiSanPhamCu_Click);
            // 
            // btnThemSanPhamCuVaoHoaDon
            // 
            this.btnThemSanPhamCuVaoHoaDon.Location = new System.Drawing.Point(262, 184);
            this.btnThemSanPhamCuVaoHoaDon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnThemSanPhamCuVaoHoaDon.Name = "btnThemSanPhamCuVaoHoaDon";
            this.btnThemSanPhamCuVaoHoaDon.Size = new System.Drawing.Size(241, 27);
            this.btnThemSanPhamCuVaoHoaDon.StyleController = this.layoutControl6;
            this.btnThemSanPhamCuVaoHoaDon.TabIndex = 10;
            this.btnThemSanPhamCuVaoHoaDon.Text = "Thêm vào hóa đơn";
            this.btnThemSanPhamCuVaoHoaDon.Click += new System.EventHandler(this.btnThemSanPhamCuVaoHoaDon_Click);
            // 
            // txtTenSanPhamCu
            // 
            this.txtTenSanPhamCu.Location = new System.Drawing.Point(367, 16);
            this.txtTenSanPhamCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTenSanPhamCu.Name = "txtTenSanPhamCu";
            this.txtTenSanPhamCu.Size = new System.Drawing.Size(136, 22);
            this.txtTenSanPhamCu.StyleController = this.layoutControl6;
            this.txtTenSanPhamCu.TabIndex = 0;
            // 
            // txtChiTinhCu
            // 
            this.txtChiTinhCu.Location = new System.Drawing.Point(121, 72);
            this.txtChiTinhCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChiTinhCu.Name = "txtChiTinhCu";
            this.txtChiTinhCu.Properties.Mask.EditMask = "d";
            this.txtChiTinhCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiTinhCu.Properties.NullText = "0";
            this.txtChiTinhCu.Size = new System.Drawing.Size(53, 22);
            this.txtChiTinhCu.StyleController = this.layoutControl6;
            this.txtChiTinhCu.TabIndex = 3;
            this.txtChiTinhCu.EditValueChanged += new System.EventHandler(this.txtChiTinhCu_EditValueChanged);
            // 
            // txtChiDaCu
            // 
            this.txtChiDaCu.Location = new System.Drawing.Point(121, 100);
            this.txtChiDaCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChiDaCu.Name = "txtChiDaCu";
            this.txtChiDaCu.Properties.Mask.EditMask = "d";
            this.txtChiDaCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiDaCu.Properties.NullText = "0";
            this.txtChiDaCu.Size = new System.Drawing.Size(53, 22);
            this.txtChiDaCu.StyleController = this.layoutControl6;
            this.txtChiDaCu.TabIndex = 5;
            this.txtChiDaCu.EditValueChanged += new System.EventHandler(this.txtChiDaCu_EditValueChanged);
            // 
            // txtGiaMua
            // 
            this.txtGiaMua.Location = new System.Drawing.Point(121, 156);
            this.txtGiaMua.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGiaMua.Name = "txtGiaMua";
            this.txtGiaMua.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtGiaMua.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaMua.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGiaMua.Properties.ReadOnly = true;
            this.txtGiaMua.Size = new System.Drawing.Size(382, 22);
            this.txtGiaMua.StyleController = this.layoutControl6;
            this.txtGiaMua.TabIndex = 9;
            // 
            // txtChiVangCu
            // 
            this.txtChiVangCu.Location = new System.Drawing.Point(121, 128);
            this.txtChiVangCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChiVangCu.Name = "txtChiVangCu";
            this.txtChiVangCu.Properties.Mask.EditMask = "d";
            this.txtChiVangCu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiVangCu.Properties.NullText = "0";
            this.txtChiVangCu.Properties.ReadOnly = true;
            this.txtChiVangCu.Size = new System.Drawing.Size(53, 22);
            this.txtChiVangCu.StyleController = this.layoutControl6;
            this.txtChiVangCu.TabIndex = 6;
            // 
            // txtTiLeHotCu
            // 
            this.txtTiLeHotCu.Location = new System.Drawing.Point(367, 44);
            this.txtTiLeHotCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTiLeHotCu.Name = "txtTiLeHotCu";
            this.txtTiLeHotCu.Properties.NullText = "0";
            this.txtTiLeHotCu.Size = new System.Drawing.Size(136, 22);
            this.txtTiLeHotCu.StyleController = this.layoutControl6;
            this.txtTiLeHotCu.TabIndex = 7;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem33,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem36,
            this.layoutControlItem41,
            this.layoutControlItem39,
            this.layoutControlItem35,
            this.layoutControlItem32,
            this.layoutControlItem34,
            this.layoutControlItem38,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem20,
            this.control,
            this.layoutControlItem40});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(519, 227);
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.txtTenSanPhamCu;
            this.layoutControlItem33.CustomizationFormText = "Tên sản phẩm";
            this.layoutControlItem33.Location = new System.Drawing.Point(246, 0);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(247, 28);
            this.layoutControlItem33.Text = "Tên sản phẩm";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnThemSanPhamCuVaoHoaDon;
            this.layoutControlItem1.Location = new System.Drawing.Point(246, 168);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(247, 33);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnNhapLaiSanPhamCu;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(246, 33);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.txtChiDaCu;
            this.layoutControlItem36.CustomizationFormText = "Khối lượng đá";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem36.Text = "KLĐ: Chỉ";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.txtTiLeHotCu;
            this.layoutControlItem41.CustomizationFormText = "Tỉ lệ hột";
            this.layoutControlItem41.Location = new System.Drawing.Point(246, 28);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(247, 28);
            this.layoutControlItem41.Text = "Tỉ lệ hột";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.txtChiVangCu;
            this.layoutControlItem39.CustomizationFormText = "Khối lượng vàng";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem39.Text = "KLV: Chỉ";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.txtChiTinhCu;
            this.layoutControlItem35.CustomizationFormText = "Khối lượng tịnh";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem35.Text = "KLT: Chỉ";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txtPhanTinhCu;
            this.layoutControlItem32.Location = new System.Drawing.Point(164, 56);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem32.Text = "Phân";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.txtLyTinhCu;
            this.layoutControlItem34.Location = new System.Drawing.Point(328, 56);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(165, 28);
            this.layoutControlItem34.Text = "Ly";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.txtPhanDaCu;
            this.layoutControlItem38.Location = new System.Drawing.Point(164, 84);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem38.Text = "Phân";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.txtLyDaCu;
            this.layoutControlItem43.Location = new System.Drawing.Point(328, 84);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(165, 28);
            this.layoutControlItem43.Text = "Ly";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.txtPhanVangCu;
            this.layoutControlItem44.Location = new System.Drawing.Point(164, 112);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(164, 28);
            this.layoutControlItem44.Text = "Phân";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.txtLyVangCu;
            this.layoutControlItem45.Location = new System.Drawing.Point(328, 112);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(165, 28);
            this.layoutControlItem45.Text = "Ly";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.lueHamLuongVangCu;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(246, 28);
            this.layoutControlItem20.Text = "Hàm lượng vàng";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(102, 17);
            // 
            // control
            // 
            this.control.Control = this.txtGiaMua;
            this.control.CustomizationFormText = "Giá mua";
            this.control.Location = new System.Drawing.Point(0, 140);
            this.control.Name = "control";
            this.control.Size = new System.Drawing.Size(493, 28);
            this.control.Text = "Giá mua";
            this.control.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.lueLoaiVangMuaVao;
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(246, 28);
            this.layoutControlItem40.Text = "Loại vàng";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(102, 17);
            // 
            // splitterControl3
            // 
            this.splitterControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl3.Location = new System.Drawing.Point(2, 487);
            this.splitterControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl3.Name = "splitterControl3";
            this.splitterControl3.Size = new System.Drawing.Size(544, 6);
            this.splitterControl3.TabIndex = 8;
            this.splitterControl3.TabStop = false;
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl2.Location = new System.Drawing.Point(2, 240);
            this.splitterControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(544, 6);
            this.splitterControl2.TabIndex = 7;
            this.splitterControl2.TabStop = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 180);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(477, 22);
            this.simpleButton1.TabIndex = 10;
            this.simpleButton1.Text = "Thêm vào danh sách nhập";
            // 
            // dxerHamLuongVang
            // 
            this.dxerHamLuongVang.ContainerControl = this;
            // 
            // dxerTiLeHot
            // 
            this.dxerTiLeHot.ContainerControl = this;
            // 
            // dxerLyTinhCu
            // 
            this.dxerLyTinhCu.ContainerControl = this;
            // 
            // dxerLyDaCu
            // 
            this.dxerLyDaCu.ContainerControl = this;
            // 
            // dxerLyVangCu
            // 
            this.dxerLyVangCu.ContainerControl = this;
            // 
            // dxerTenSanPhamCu
            // 
            this.dxerTenSanPhamCu.ContainerControl = this;
            // 
            // dxerLoaiVangMuaVao
            // 
            this.dxerLoaiVangMuaVao.ContainerControl = this;
            // 
            // dxerLoaiVangBanRa
            // 
            this.dxerLoaiVangBanRa.ContainerControl = this;
            // 
            // ckeChinhSuaTienCong
            // 
            this.ckeChinhSuaTienCong.Location = new System.Drawing.Point(362, 486);
            this.ckeChinhSuaTienCong.MenuManager = this.barManager1;
            this.ckeChinhSuaTienCong.Name = "ckeChinhSuaTienCong";
            this.ckeChinhSuaTienCong.Properties.Caption = "Chỉnh sửa tiền công";
            this.ckeChinhSuaTienCong.Size = new System.Drawing.Size(240, 21);
            this.ckeChinhSuaTienCong.StyleController = this.layoutControl2;
            this.ckeChinhSuaTienCong.TabIndex = 20;
            this.ckeChinhSuaTienCong.CheckedChanged += new System.EventHandler(this.ckeChinhSuaTienCong_CheckedChanged);
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.ckeChinhSuaTienCong;
            this.layoutControlItem52.Location = new System.Drawing.Point(346, 470);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(246, 28);
            this.layoutControlItem52.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem52.TextVisible = false;
            // 
            // frmVangDoiVang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 752);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.grpHoaDon);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVangDoiVang";
            this.Text = "Vàng đổi vàng";
            this.Load += new System.EventHandler(this.frmVangDoiVang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTimSanPham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpHoaDon)).EndInit();
            this.grpHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienVangBanRa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHoaDon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienConLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienVangCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhTienVangDoiVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCongThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKLVMoiTruKLVCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCuoiCung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBanMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaKhoiMatHangCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongTinSanPhamMoi)).EndInit();
            this.grcThongTinSanPhamMoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVangMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVangMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVangMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDaMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDaMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinhMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinhMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVangMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDaMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinhMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPhamMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTemMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiVangBanRa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachSanPham)).EndInit();
            this.grpDanhSachSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachSanPhamTaiKhoMoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachSanPhamTaiKhoMoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcThongTinSanPhamCu)).EndInit();
            this.grcThongTinSanPhamCu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiVangMuaVao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVangCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVangCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVangCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDaCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDaCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinhCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinhCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPhamCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinhCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDaCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVangCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.control)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinhCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDaCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVangCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPhamCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiVangMuaVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiVangBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckeChinhSuaTienCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.TextEdit txtTimSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclTem;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclTiLeHot;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaBan;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclMoTa;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.GroupControl grpHoaDon;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit txtNhanVien;
        private DevExpress.XtraEditors.LookUpEdit lueKhachHang;
        private DevExpress.XtraEditors.DateEdit detNgayBan;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraGrid.GridControl grcVangRa;
        private DevExpress.XtraGrid.Views.Grid.GridView grvVangRa;
        private DevExpress.XtraGrid.Columns.GridColumn grclTem_grcHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham_grcHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaBan_grcHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn grclHanhDong;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.GridControl grcVangVao;
        private DevExpress.XtraGrid.Views.Grid.GridView grvVangVao;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaMua;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton btnThanhToan;
        private DevExpress.XtraEditors.SimpleButton btnNhapLaiHoaDon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.GroupControl grcThongTinSanPhamMoi;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.TextEdit txtChiVangMoi;
        private DevExpress.XtraEditors.TextEdit txtChiDaMoi;
        private DevExpress.XtraEditors.TextEdit txtChiTinhMoi;
        private DevExpress.XtraEditors.SimpleButton btnNhapLai;
        private DevExpress.XtraEditors.SimpleButton btnThemSanPhamMoiVaoHoaDon;
        private DevExpress.XtraEditors.TextEdit txtGiaBan;
        private DevExpress.XtraEditors.TextEdit txtTiLeHotMoi;
        private DevExpress.XtraEditors.TextEdit txtTenSanPhamMoi;
        private DevExpress.XtraEditors.TextEdit txtMaTemMoi;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.GroupControl grpDanhSachSanPham;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraGrid.GridControl grcDanhSachSanPhamTaiKhoMoi;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDanhSachSanPhamTaiKhoMoi;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl grcThongTinSanPhamCu;
        private DevExpress.XtraEditors.SplitterControl splitterControl3;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraEditors.TextEdit txtTenSanPhamCu;
        private DevExpress.XtraEditors.TextEdit txtChiTinhCu;
        private DevExpress.XtraEditors.TextEdit txtChiDaCu;
        private DevExpress.XtraEditors.TextEdit txtGiaMua;
        private DevExpress.XtraEditors.TextEdit txtChiVangCu;
        private DevExpress.XtraEditors.TextEdit txtTiLeHotCu;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem control;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnNhapLaiSanPhamCu;
        private DevExpress.XtraEditors.SimpleButton btnThemSanPhamCuVaoHoaDon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtCuoiCung;
        private DevExpress.XtraEditors.TextEdit txtGiaBanMua;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Columns.GridColumn grclXoaKhoiMatHangCu;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaKhoiMatHangCu;
        private DevExpress.XtraEditors.TextEdit txtKLVMoiTruKLVCu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit txtLyVangMoi;
        private DevExpress.XtraEditors.TextEdit txtPhanVangMoi;
        private DevExpress.XtraEditors.TextEdit txtLyDaMoi;
        private DevExpress.XtraEditors.TextEdit txtPhanDaMoi;
        private DevExpress.XtraEditors.TextEdit txtLyTinhMoi;
        private DevExpress.XtraEditors.TextEdit txtPhanTinhMoi;
        private DevExpress.XtraEditors.TextEdit txtTienCong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.TextEdit txtLyVangCu;
        private DevExpress.XtraEditors.TextEdit txtPhanVangCu;
        private DevExpress.XtraEditors.TextEdit txtLyDaCu;
        private DevExpress.XtraEditors.TextEdit txtPhanDaCu;
        private DevExpress.XtraEditors.TextEdit txtLyTinhCu;
        private DevExpress.XtraEditors.TextEdit txtPhanTinhCu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVangMoi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVangCu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHamLuongVang;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTiLeHot;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyTinhCu;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyDaCu;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyVangCu;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTenSanPhamCu;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiVangMuaVao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraEditors.TextEdit txtTienCongThem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraEditors.TextEdit txtTienBot;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraEditors.TextEdit txtTienConLai;
        private DevExpress.XtraEditors.TextEdit txtTongTienVangCu;
        private DevExpress.XtraEditors.TextEdit txtThanhTienVangDoiVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLoaiVangMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn KhoiLuongVang;
        private DevExpress.XtraEditors.TextEdit txtSoHoaDon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiVangBanRa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLoaiVangBanRa;
        private DevExpress.XtraEditors.TextEdit txtTongTienVangBanRa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraEditors.CheckEdit ckeChinhSuaTienCong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
    }
}