﻿namespace QuanLyTiemVang
{
    partial class frmNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnDoiMatKhau = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtNhapLaiMatKhau = new System.Windows.Forms.MaskedTextBox();
            this.txtMatKhau = new System.Windows.Forms.MaskedTextBox();
            this.ceDuocPhepNhapKho = new DevExpress.XtraEditors.CheckEdit();
            this.ceQuyen = new DevExpress.XtraEditors.CheckEdit();
            this.txtTaiKhoan = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.detNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.txtSoDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.rgGioiTinh = new DevExpress.XtraEditors.RadioGroup();
            this.txtChungMinhNhanDan = new DevExpress.XtraEditors.TextEdit();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.txtHo = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.grcNhanVien = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclHo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclChungMinhNhanDan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclSoDienThoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclDiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTaiKhoan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclQuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclQuyenNhapKho = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceDuocPhepNhapKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceQuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaiKhoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChungMinhNhanDan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.barButtonItem4,
            this.btnDoiMatKhau,
            this.btnNhapLai});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 6;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnSua, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDoiMatKhau, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnThem
            // 
            this.btnThem.Caption = "Thêm";
            this.btnThem.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThem.Id = 0;
            this.btnThem.Name = "btnThem";
            this.btnThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThem_ItemClick);
            // 
            // btnSua
            // 
            this.btnSua.Caption = "Sửa";
            this.btnSua.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnSua.Id = 1;
            this.btnSua.Name = "btnSua";
            this.btnSua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSua_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 2;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 5;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnDoiMatKhau
            // 
            this.btnDoiMatKhau.Caption = "Đổi mật khẩu";
            this.btnDoiMatKhau.Glyph = global::QuanLyTiemVang.Properties.Resources.key_solid_icon;
            this.btnDoiMatKhau.Id = 4;
            this.btnDoiMatKhau.Name = "btnDoiMatKhau";
            this.btnDoiMatKhau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDoiMatKhau_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1067, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 543);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1067, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 493);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1067, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 493);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Đổi mật khẩu";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 50);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1067, 160);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtNhapLaiMatKhau);
            this.layoutControl1.Controls.Add(this.txtMatKhau);
            this.layoutControl1.Controls.Add(this.ceDuocPhepNhapKho);
            this.layoutControl1.Controls.Add(this.ceQuyen);
            this.layoutControl1.Controls.Add(this.txtTaiKhoan);
            this.layoutControl1.Controls.Add(this.txtDiaChi);
            this.layoutControl1.Controls.Add(this.detNgaySinh);
            this.layoutControl1.Controls.Add(this.txtSoDienThoai);
            this.layoutControl1.Controls.Add(this.rgGioiTinh);
            this.layoutControl1.Controls.Add(this.txtChungMinhNhanDan);
            this.layoutControl1.Controls.Add(this.txtTen);
            this.layoutControl1.Controls.Add(this.txtHo);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1063, 156);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtNhapLaiMatKhau
            // 
            this.txtNhapLaiMatKhau.Location = new System.Drawing.Point(943, 45);
            this.txtNhapLaiMatKhau.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNhapLaiMatKhau.Name = "txtNhapLaiMatKhau";
            this.txtNhapLaiMatKhau.PasswordChar = '•';
            this.txtNhapLaiMatKhau.Size = new System.Drawing.Size(104, 21);
            this.txtNhapLaiMatKhau.TabIndex = 5;
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(678, 45);
            this.txtMatKhau.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '•';
            this.txtMatKhau.Size = new System.Drawing.Size(129, 21);
            this.txtMatKhau.TabIndex = 4;
            // 
            // ceDuocPhepNhapKho
            // 
            this.ceDuocPhepNhapKho.Location = new System.Drawing.Point(677, 100);
            this.ceDuocPhepNhapKho.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ceDuocPhepNhapKho.MenuManager = this.barManager1;
            this.ceDuocPhepNhapKho.Name = "ceDuocPhepNhapKho";
            this.ceDuocPhepNhapKho.Properties.Caption = "";
            this.ceDuocPhepNhapKho.Size = new System.Drawing.Size(370, 19);
            this.ceDuocPhepNhapKho.StyleController = this.layoutControl1;
            this.ceDuocPhepNhapKho.TabIndex = 12;
            // 
            // ceQuyen
            // 
            this.ceQuyen.Location = new System.Drawing.Point(146, 100);
            this.ceQuyen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ceQuyen.MenuManager = this.barManager1;
            this.ceQuyen.Name = "ceQuyen";
            this.ceQuyen.Properties.Caption = "";
            this.ceQuyen.Size = new System.Drawing.Size(395, 19);
            this.ceQuyen.StyleController = this.layoutControl1;
            this.ceQuyen.TabIndex = 11;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(146, 44);
            this.txtTaiKhoan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTaiKhoan.MenuManager = this.barManager1;
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(396, 22);
            this.txtTaiKhoan.StyleController = this.layoutControl1;
            this.txtTaiKhoan.TabIndex = 5;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(146, 72);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDiaChi.MenuManager = this.barManager1;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(396, 22);
            this.txtDiaChi.StyleController = this.layoutControl1;
            this.txtDiaChi.TabIndex = 8;
            // 
            // detNgaySinh
            // 
            this.detNgaySinh.EditValue = null;
            this.detNgaySinh.Location = new System.Drawing.Point(678, 72);
            this.detNgaySinh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detNgaySinh.MenuManager = this.barManager1;
            this.detNgaySinh.Name = "detNgaySinh";
            this.detNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgaySinh.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgaySinh.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgaySinh.Size = new System.Drawing.Size(129, 22);
            this.detNgaySinh.StyleController = this.layoutControl1;
            this.detNgaySinh.TabIndex = 9;
            // 
            // txtSoDienThoai
            // 
            this.txtSoDienThoai.Location = new System.Drawing.Point(943, 72);
            this.txtSoDienThoai.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSoDienThoai.MenuManager = this.barManager1;
            this.txtSoDienThoai.Name = "txtSoDienThoai";
            this.txtSoDienThoai.Properties.Mask.EditMask = "(\\d?\\d?\\d?) \\d\\d\\d-\\d\\d\\d\\d";
            this.txtSoDienThoai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtSoDienThoai.Size = new System.Drawing.Size(104, 22);
            this.txtSoDienThoai.StyleController = this.layoutControl1;
            this.txtSoDienThoai.TabIndex = 10;
            // 
            // rgGioiTinh
            // 
            this.rgGioiTinh.Location = new System.Drawing.Point(678, 16);
            this.rgGioiTinh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rgGioiTinh.MenuManager = this.barManager1;
            this.rgGioiTinh.Name = "rgGioiTinh";
            this.rgGioiTinh.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rgGioiTinh.Properties.Appearance.Options.UseBackColor = true;
            this.rgGioiTinh.Properties.Columns = 2;
            this.rgGioiTinh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Nam"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Nữ")});
            this.rgGioiTinh.Size = new System.Drawing.Size(129, 23);
            this.rgGioiTinh.StyleController = this.layoutControl1;
            this.rgGioiTinh.TabIndex = 3;
            // 
            // txtChungMinhNhanDan
            // 
            this.txtChungMinhNhanDan.Location = new System.Drawing.Point(943, 16);
            this.txtChungMinhNhanDan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChungMinhNhanDan.MenuManager = this.barManager1;
            this.txtChungMinhNhanDan.Name = "txtChungMinhNhanDan";
            this.txtChungMinhNhanDan.Properties.Mask.EditMask = "d";
            this.txtChungMinhNhanDan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChungMinhNhanDan.Size = new System.Drawing.Size(104, 22);
            this.txtChungMinhNhanDan.StyleController = this.layoutControl1;
            this.txtChungMinhNhanDan.TabIndex = 4;
            // 
            // txtTen
            // 
            this.txtTen.Location = new System.Drawing.Point(410, 16);
            this.txtTen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTen.MenuManager = this.barManager1;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(132, 22);
            this.txtTen.StyleController = this.layoutControl1;
            this.txtTen.TabIndex = 2;
            // 
            // txtHo
            // 
            this.txtHo.Location = new System.Drawing.Point(146, 16);
            this.txtHo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtHo.MenuManager = this.barManager1;
            this.txtHo.Name = "txtHo";
            this.txtHo.Size = new System.Drawing.Size(128, 22);
            this.txtHo.StyleController = this.layoutControl1;
            this.txtHo.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem2,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1063, 156);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtHo;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(264, 28);
            this.layoutControlItem1.Text = "Họ lót";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtTaiKhoan;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(532, 28);
            this.layoutControlItem8.Text = "Tài khoản (*)";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ceQuyen;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(531, 46);
            this.layoutControlItem9.Text = "Quản lý";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTen;
            this.layoutControlItem2.Location = new System.Drawing.Point(264, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(268, 28);
            this.layoutControlItem2.Text = "Tên";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(127, 16);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtMatKhau;
            this.layoutControlItem11.Location = new System.Drawing.Point(532, 29);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(265, 27);
            this.layoutControlItem11.Text = "Mật khẩu (*)";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtNhapLaiMatKhau;
            this.layoutControlItem12.Location = new System.Drawing.Point(797, 29);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(240, 27);
            this.layoutControlItem12.Text = "Nhập lại mật khẩu(*)";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtChungMinhNhanDan;
            this.layoutControlItem3.Location = new System.Drawing.Point(797, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(240, 29);
            this.layoutControlItem3.Text = "CMND";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(127, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.rgGioiTinh;
            this.layoutControlItem4.Location = new System.Drawing.Point(532, 0);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(151, 29);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(265, 29);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Giới tính";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtDiaChi;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(532, 28);
            this.layoutControlItem7.Text = "Địa chỉ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtSoDienThoai;
            this.layoutControlItem5.Location = new System.Drawing.Point(797, 56);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(240, 28);
            this.layoutControlItem5.Text = "Số điện thoại";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(127, 17);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.detNgaySinh;
            this.layoutControlItem6.Location = new System.Drawing.Point(532, 56);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(265, 28);
            this.layoutControlItem6.Text = "Ngày sinh";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(127, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.ceDuocPhepNhapKho;
            this.layoutControlItem10.Location = new System.Drawing.Point(531, 84);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(506, 46);
            this.layoutControlItem10.Text = "Được nhập kho";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(127, 17);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 210);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1067, 6);
            this.splitterControl1.TabIndex = 5;
            this.splitterControl1.TabStop = false;
            // 
            // grcNhanVien
            // 
            this.grcNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcNhanVien.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcNhanVien.Location = new System.Drawing.Point(0, 216);
            this.grcNhanVien.MainView = this.gridView1;
            this.grcNhanVien.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcNhanVien.MenuManager = this.barManager1;
            this.grcNhanVien.Name = "grcNhanVien";
            this.grcNhanVien.Size = new System.Drawing.Size(1067, 327);
            this.grcNhanVien.TabIndex = 6;
            this.grcNhanVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.grcNhanVien.DoubleClick += new System.EventHandler(this.grcNhanVien_DoubleClick);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclHo,
            this.grclTen,
            this.grclGioiTinh,
            this.grclChungMinhNhanDan,
            this.grclSoDienThoai,
            this.grclNgaySinh,
            this.grclDiaChi,
            this.grclTaiKhoan,
            this.grclQuyen,
            this.grclQuyenNhapKho});
            this.gridView1.GridControl = this.grcNhanVien;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 100;
            this.gridView1.OptionsFind.FindNullPrompt = "";
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // grclHo
            // 
            this.grclHo.Caption = "Họ";
            this.grclHo.FieldName = "HoLot";
            this.grclHo.Name = "grclHo";
            this.grclHo.Visible = true;
            this.grclHo.VisibleIndex = 0;
            // 
            // grclTen
            // 
            this.grclTen.Caption = "Tên";
            this.grclTen.FieldName = "Ten";
            this.grclTen.Name = "grclTen";
            this.grclTen.Visible = true;
            this.grclTen.VisibleIndex = 1;
            // 
            // grclGioiTinh
            // 
            this.grclGioiTinh.Caption = "Giới tính";
            this.grclGioiTinh.FieldName = "GioiTinh";
            this.grclGioiTinh.Name = "grclGioiTinh";
            this.grclGioiTinh.Visible = true;
            this.grclGioiTinh.VisibleIndex = 2;
            // 
            // grclChungMinhNhanDan
            // 
            this.grclChungMinhNhanDan.Caption = "CMND";
            this.grclChungMinhNhanDan.FieldName = "ChungMinhNhanDan";
            this.grclChungMinhNhanDan.Name = "grclChungMinhNhanDan";
            this.grclChungMinhNhanDan.Visible = true;
            this.grclChungMinhNhanDan.VisibleIndex = 3;
            // 
            // grclSoDienThoai
            // 
            this.grclSoDienThoai.Caption = "Số điện thoại";
            this.grclSoDienThoai.FieldName = "SoDienThoai";
            this.grclSoDienThoai.Name = "grclSoDienThoai";
            this.grclSoDienThoai.Visible = true;
            this.grclSoDienThoai.VisibleIndex = 4;
            // 
            // grclNgaySinh
            // 
            this.grclNgaySinh.Caption = "Ngày sinh";
            this.grclNgaySinh.FieldName = "NgaySinh";
            this.grclNgaySinh.Name = "grclNgaySinh";
            this.grclNgaySinh.Visible = true;
            this.grclNgaySinh.VisibleIndex = 5;
            // 
            // grclDiaChi
            // 
            this.grclDiaChi.Caption = "Địa chỉ";
            this.grclDiaChi.FieldName = "DiaChi";
            this.grclDiaChi.Name = "grclDiaChi";
            this.grclDiaChi.Visible = true;
            this.grclDiaChi.VisibleIndex = 6;
            // 
            // grclTaiKhoan
            // 
            this.grclTaiKhoan.Caption = "Tài khoản";
            this.grclTaiKhoan.FieldName = "TaiKhoan";
            this.grclTaiKhoan.Name = "grclTaiKhoan";
            this.grclTaiKhoan.Visible = true;
            this.grclTaiKhoan.VisibleIndex = 7;
            // 
            // grclQuyen
            // 
            this.grclQuyen.Caption = "Chức vụ";
            this.grclQuyen.FieldName = "Quyen";
            this.grclQuyen.Name = "grclQuyen";
            this.grclQuyen.Visible = true;
            this.grclQuyen.VisibleIndex = 8;
            // 
            // grclQuyenNhapKho
            // 
            this.grclQuyenNhapKho.Caption = "Nhập kho";
            this.grclQuyenNhapKho.FieldName = "QuyenNhapKho";
            this.grclQuyenNhapKho.Name = "grclQuyenNhapKho";
            this.grclQuyenNhapKho.Visible = true;
            this.grclQuyenNhapKho.VisibleIndex = 9;
            // 
            // frmNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 543);
            this.Controls.Add(this.grcNhanVien);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmNhanVien";
            this.Text = "Nhân Viên";
            this.Load += new System.EventHandler(this.frmNhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceDuocPhepNhapKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceQuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaiKhoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChungMinhNhanDan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraGrid.GridControl grcNhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.RadioGroup rgGioiTinh;
        private DevExpress.XtraEditors.TextEdit txtChungMinhNhanDan;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.TextEdit txtHo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtTaiKhoan;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.DateEdit detNgaySinh;
        private DevExpress.XtraEditors.TextEdit txtSoDienThoai;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn grclHo;
        private DevExpress.XtraGrid.Columns.GridColumn grclTen;
        private DevExpress.XtraGrid.Columns.GridColumn grclGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclChungMinhNhanDan;
        private DevExpress.XtraGrid.Columns.GridColumn grclSoDienThoai;
        private DevExpress.XtraGrid.Columns.GridColumn grclNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclDiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn grclTaiKhoan;
        private DevExpress.XtraGrid.Columns.GridColumn grclQuyen;
        private DevExpress.XtraGrid.Columns.GridColumn grclQuyenNhapKho;
        private DevExpress.XtraEditors.CheckEdit ceDuocPhepNhapKho;
        private DevExpress.XtraEditors.CheckEdit ceQuyen;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.MaskedTextBox txtMatKhau;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraBars.BarButtonItem btnDoiMatKhau;
        private System.Windows.Forms.MaskedTextBox txtNhapLaiMatKhau;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
    }
}