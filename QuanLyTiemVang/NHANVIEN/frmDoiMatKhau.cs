﻿using BUS;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang.NHANVIEN
{
    public partial class frmDoiMatKhau : Form
    {
        private NhanVien nhanvien;
        public frmDoiMatKhau(NhanVien nv)
        {
            InitializeComponent();
            this.nhanvien = nv;
            this.Text = "Đổi mật khẩu " + nhanvien.Ten;
        }

        private void btnDoiMatKhau_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtMatKhauMoi.Text))
            {
                PopupService.Instance.Error(ErrorUltils.PASS_NULL);
            }
            else if (txtMatKhauMoi.Text != txtNhapLaiMatKhau.Text)
            {
                PopupService.Instance.Error(ErrorUltils.PASS_EQUAL);
            }
            else
            {
                if (NhanVienBUS.Instance.changePassword(nhanvien, txtMatKhauMoi.Text))
                {
                    PopupService.Instance.Success(ErrorUltils.SUCCESS);
                    this.Hide();
                }
                else
                {
                    PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                }
            }
        }
    }
}
