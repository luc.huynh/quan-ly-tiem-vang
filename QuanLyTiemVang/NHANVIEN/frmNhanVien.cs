﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using DevExpress.XtraLayout.Utils;
using QuanLyTiemVang.NHANVIEN;
using QuanLyTiemVang.Constants.StringUltils;
using Share.Constant;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang
{
    public partial class frmNhanVien : DevExpress.XtraEditors.XtraForm
    {
        public frmNhanVien()
        {
            InitializeComponent();
        }

        private void frmNhanVien_Load(object sender, EventArgs e)
        {
            NhanVienBUS.Instance.GetList(grcNhanVien);
            btnDoiMatKhau.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        }

        private void grcNhanVien_DoubleClick(object sender, EventArgs e)
        {
            //Đẩy dữ liệu lên
            NhanVien nhanVien = (NhanVien)gridView1.GetFocusedRow();
            btnSua.Tag = nhanVien;
            btnXoa.Tag = nhanVien;
            btnThem.Enabled = false;
            txtHo.Text = nhanVien.HoLot;
            txtTen.Text = nhanVien.Ten;
            txtSoDienThoai.Text = nhanVien.SoDienThoai;
            txtTaiKhoan.Text = nhanVien.TaiKhoan;
            txtChungMinhNhanDan.Text = nhanVien.ChungMinhNhanDan;
            detNgaySinh.Text = nhanVien.NgaySinh.ToString();
            txtDiaChi.Text = nhanVien.DiaChi;
            rgGioiTinh.SelectedIndex = nhanVien.GioiTinh == Constant.MALE ? 0 : 1;
            ceQuyen.Checked = nhanVien.Quyen == EmployeePermission.QUAN_LY ? true : false;
            ceDuocPhepNhapKho.Checked = nhanVien.QuyenNhapKho == EmployeePermission.NHAP_KHO_YES ? true : false;
            btnDoiMatKhau.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            txtMatKhau.Enabled = false;
            txtNhapLaiMatKhau.Enabled = false;
            txtTaiKhoan.Enabled = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnSua.Tag != null)
            {
                NhanVien nhanVien = (NhanVien)btnSua.Tag;
                nhanVien.TaiKhoan = txtTaiKhoan.Text;
                nhanVien.HoLot = txtHo.Text;
                nhanVien.Ten = txtTen.Text;
                nhanVien.SoDienThoai = txtSoDienThoai.Text;
                nhanVien.TaiKhoan = txtTaiKhoan.Text;
                nhanVien.ChungMinhNhanDan = txtChungMinhNhanDan.Text;
                if (string.IsNullOrEmpty(detNgaySinh.Text))
                {
                    nhanVien.NgaySinh = null;
                }
                else
                {
                    nhanVien.NgaySinh = DateTime.Parse(detNgaySinh.Text);
                }
                nhanVien.DiaChi = txtDiaChi.Text;
                nhanVien.GioiTinh = rgGioiTinh.SelectedIndex == 0 ? Constant.MALE : Constant.FEMALE;
                nhanVien.Quyen = ceQuyen.Checked ? EmployeePermission.QUAN_LY : EmployeePermission.NHAN_VIEN;
                nhanVien.QuyenNhapKho = ceDuocPhepNhapKho.Checked ? EmployeePermission.NHAP_KHO_YES : EmployeePermission.NHAP_KHO_NO;

                if (NhanVienBUS.Instance.Update(nhanVien))
                {
                    PopupService.Instance.Error(ErrorUltils.SUCCESS);
                    LoadDataSoure();
                }

            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }
        }

        private bool checkValidate()
        {
            if (string.IsNullOrWhiteSpace(txtTaiKhoan.Text))
            {
                PopupService.Instance.Error(ErrorUltils.USER_NULL);
                return false;
            }
            else if (NhanVienBUS.Instance.CheckExist(txtTaiKhoan.Text))
            {
                PopupService.Instance.Error(ErrorUltils.USER_DULICATE);
                return false;
            }
            else if (string.IsNullOrEmpty(txtMatKhau.Text))
            {
                PopupService.Instance.Error(ErrorUltils.PASS_NULL);
                return false;
            }
            else if (!txtMatKhau.Text.Equals(txtNhapLaiMatKhau.Text))
            {
                PopupService.Instance.Error(ErrorUltils.PASS_EQUAL);
                return false;
            }
            return true;
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnSua.Tag == null && btnXoa.Tag == null)
            {
                //Check validate tài khoản
                if (checkValidate())
                {
                    string ho = txtHo.Text;
                    string ten = txtTen.Text;
                    string soDienThoai = txtSoDienThoai.Text;
                    string tenDangNhap = txtTaiKhoan.Text;
                    string matKhau = txtMatKhau.Text;
                    string gioiTinh = rgGioiTinh.SelectedIndex == 0 ? Constant.MALE : Constant.FEMALE;
                    string chungMinhNhanDan = txtChungMinhNhanDan.Text;
                    string ngaySinh = detNgaySinh.Text;
                    string diaChi = txtDiaChi.Text;
                    string quyen = ceQuyen.Checked ? EmployeePermission.QUAN_LY : EmployeePermission.NHAN_VIEN;
                    string duocPhepNhapKho = ceDuocPhepNhapKho.Checked ? EmployeePermission.NHAP_KHO_YES : EmployeePermission.NHAP_KHO_NO;
                    if (NhanVienBUS.Instance.Insert(ho, ten, soDienThoai, tenDangNhap, matKhau, gioiTinh, chungMinhNhanDan, ngaySinh, diaChi, quyen, duocPhepNhapKho))
                    {

                        PopupService.Instance.Success(ErrorUltils.SUCCESS);
                        LoadDataSoure();
                    }
                }
            }
            else
            {
                PopupService.Instance.Error("Bạn đang ở chế độ chỉnh sửa. Ấn \"Nhập lại\" để thoát khỏi chế độ chỉnh sửa");
            }

        }

        private void LoadDataSoure()
        {
            ClearForm();
            NhanVienBUS.Instance.GetList(grcNhanVien);
            grcNhanVien.RefreshDataSource();
        }

        private void ClearForm()
        {
            //Xóa thông tin trong form đồng nghĩa với bỏ chọn nhân viên đó
            btnThem.Enabled = true;
            btnSua.Tag = null;
            btnXoa.Tag = null;
            txtHo.Text = null;
            txtTen.Text = null;
            txtSoDienThoai.Text = null;
            txtTaiKhoan.Text = null;
            txtMatKhau.Text = null;
            txtChungMinhNhanDan.Text = null;
            detNgaySinh.Text = null;
            txtDiaChi.Text = null;
            ceQuyen.Checked = false;
            ceDuocPhepNhapKho.Checked = false;
            txtMatKhau.Enabled = true;
            txtNhapLaiMatKhau.Enabled = true;
            btnDoiMatKhau.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            txtTaiKhoan.Enabled = true;
            txtNhapLaiMatKhau.Text = null;
        }

        private void btnNhapLai_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (btnXoa.Tag != null)
            {
                string tenNhanVien = string.IsNullOrEmpty(((NhanVien)btnXoa.Tag).Ten) ? "này" : ((NhanVien)btnXoa.Tag).Ten;
                DialogResult dialogResult = PopupService.Instance.Question("Bạn chắc muốn xóa nhân viên " + tenNhanVien + " không ? ");

                if (dialogResult == DialogResult.Yes)
                {
                    NhanVienBUS.Instance.Delete(((NhanVien)btnXoa.Tag).MaNhanVien);
                    LoadDataSoure();
                    ClearForm();
                }
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }


        }

        private void btnDoiMatKhau_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDoiMatKhau f = new frmDoiMatKhau((NhanVien)btnSua.Tag);
            f.ShowDialog();
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearForm();
        }
    }
}