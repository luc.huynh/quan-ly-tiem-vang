﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyTiemVang.Constants.StringUltils;
using DTO.Models;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using BUS;

namespace QuanLyTiemVang.QuanLyGiaCong
{
    public partial class frmPhieuGiaCong : DevExpress.XtraEditors.XtraForm
    {
        public frmPhieuGiaCong()
        {
            InitializeComponent();
        }

        private List<string> listOfFormChiTiet = new List<string> {
            "detNgayGiao",
            "txtTiLeHotXong",
            "txtKhoiLuongTongXong",
            "txtKhoiLuongVangXong",
            "txtKhoiLuongDaXong",
            "txtTiLeHotTruHao",
            "txtKhoiLuongVangTruHao",
            "txtKhoiLuongTongTruHao",
            "txtKhoiLuongDaTruHao",
            "txtLyDoTruHao"
        };

        private List<string> listOfFormPhieuGiaCong = new List<string> {
            "detNgayBatDau",
            "detNgayKetThuc",
            "lueHoTenTho",
            "lueHamLuongVang",
            "txtTiLeHotGiao",
            "txtKhoiLuongTongGiao",
            "txtKhoiLuongVangGiao",
            "txtKhoiLuongDaGiao",
            "txtTiLeHotTruHaoTong",
            "txtKhoiLuongTongTruHaoTong",
            "txtKhoiLuongVangTruHaoTong",
            "txtKhoiLuongDaTruHaoTong"
        };

        private void frmPhieuGiaCong_Load(object sender, EventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            grcPhieuGiaCong.DataSource = PhieuGiaCongBUS.Instance.GetList();
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVang);
            ThoGiaCongBUS.Instance.GetLookupEdit(lueHoTenTho);
        }

        #region Validation
        private bool ValidateKhoiLuongBanGiao()
        {
            if ((double.Parse(txtKhoiLuongTongGiao.Text) - double.Parse(txtKhoiLuongDaGiao.Text))
                .CompareTo(double.Parse(txtKhoiLuongVangGiao.Text)) != 0)
            {
                return false;
            }

            return true;
        }

        private bool ValidateKhoiLuongHoanThanh()
        {
            if ((double.Parse(txtKhoiLuongTongXong.Text) - double.Parse(txtKhoiLuongDaXong.Text))
                .CompareTo(double.Parse(txtKhoiLuongVangXong.Text)) != 0)
            {
                return false;
            }

            if ((double.Parse(txtKhoiLuongTongTruHao.Text) - double.Parse(txtKhoiLuongDaTruHao.Text))
                .CompareTo(double.Parse(txtKhoiLuongVangTruHao.Text)) != 0)
            {
                return false;
            }

            return true;
        }

        private bool IsValidPhieuGiaCong()
        {
            double outValue;
            bool isValid = true;

            dxerHamLuongVang.Dispose();
            dxerHoTenTho.Dispose();
            dxerKhoiLuongDaGiao.Dispose();
            dxerKhoiLuongTongGiao.Dispose();
            dxerKhoiLuongVangGiao.Dispose();
            dxerNgayBatDau.Dispose();
            dxerTiLeHotGiao.Dispose();

            if (!double.TryParse(txtTiLeHotGiao.Text, out outValue))
            {
                dxerTiLeHotGiao.SetError(txtTiLeHotGiao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongTongGiao.Text, out outValue))
            {
                dxerKhoiLuongTongGiao.SetError(txtKhoiLuongTongGiao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongVangGiao.Text, out outValue))
            {
                dxerKhoiLuongVangGiao.SetError(txtKhoiLuongVangGiao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongDaGiao.Text, out outValue))
            {
                dxerKhoiLuongDaGiao.SetError(txtKhoiLuongDaGiao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (detNgayBatDau.EditValue == null)
            {
                dxerNgayBatDau.SetError(detNgayBatDau, ValidationUltils.INPUT_NULL);
                isValid = false;
            }

            if (lueHamLuongVang.EditValue == null)
            {
                dxerHamLuongVang.SetError(lueHamLuongVang, ValidationUltils.INPUT_NULL);
                isValid = false;
            }

            if (lueHoTenTho.EditValue == null)
            {
                dxerHoTenTho.SetError(lueHoTenTho, ValidationUltils.INPUT_NULL);
                isValid = false;
            }

            return isValid;
        }

        private bool IsValidChiTietPhieuGiaCong()
        {
            double outValue;
            bool isValid = true;

            dxerKhoiLuongDaXong.Dispose();
            dxerKhoiLuongTongXong.Dispose();
            dxerKhoiLuongVangXong.Dispose();
            dxerNgayGiao.Dispose();
            dxerTiLeHotXong.Dispose();
            dxerKhoiLuongTongTruHao.Dispose();
            dxerTiLeHotTruHao.Dispose();
            dxerKhoiLuongDaTruHao.Dispose();
            dxerKhoiLuongVangTruHao.Dispose();

            if (!double.TryParse(txtKhoiLuongDaXong.Text, out outValue))
            {
                dxerKhoiLuongDaXong.SetError(txtKhoiLuongDaXong, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongVangXong.Text, out outValue))
            {
                dxerKhoiLuongVangXong.SetError(txtKhoiLuongVangXong, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongTongXong.Text, out outValue))
            {
                dxerKhoiLuongTongXong.SetError(txtKhoiLuongTongXong, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtTiLeHotXong.Text, out outValue))
            {
                dxerKhoiLuongDaGiao.SetError(txtTiLeHotXong, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (detNgayGiao.EditValue == null)
            {
                dxerNgayGiao.SetError(detNgayGiao, ValidationUltils.INPUT_NULL);
                isValid = false;
            }

            if (!double.TryParse(txtTiLeHotTruHao.Text, out outValue))
            {
                dxerTiLeHotTruHao.SetError(txtTiLeHotTruHao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongTongTruHao.Text, out outValue))
            {
                dxerKhoiLuongTongTruHao.SetError(txtKhoiLuongTongTruHao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongVangTruHao.Text, out outValue))
            {
                dxerKhoiLuongVangTruHao.SetError(txtKhoiLuongVangTruHao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            if (!double.TryParse(txtKhoiLuongDaTruHao.Text, out outValue))
            {
                dxerKhoiLuongDaTruHao.SetError(txtKhoiLuongDaTruHao, ValidationUltils.TI_LE_HOT_NAN);
                isValid = false;
            }

            return isValid;
        }

        #endregion

        #region GetInforFromForm
        private PhieuGiaCong GetPhieuGiaCongOfForm()
        {
            if (!IsValidPhieuGiaCong()) return null;

            if (!ValidateKhoiLuongBanGiao())
            {
                PopupService.Instance.Error(MessageError.TOTAL_MASS_INVALID);
                return null;
            }

            return new PhieuGiaCong
            {
                NgayGiao = detNgayBatDau.DateTime,
                KhoiLuongDaBanGiao = double.Parse(txtKhoiLuongDaGiao.Text),
                KhoiLuongTongBanGiao = double.Parse(txtKhoiLuongTongGiao.Text),
                KhoiLuongVangBanGiao = double.Parse(txtKhoiLuongVangGiao.Text),
                MaHamLuongVang = int.Parse(lueHamLuongVang.EditValue.ToString()),
                MaThoGiaCong = int.Parse(lueHoTenTho.EditValue.ToString()),
                TiLeHotBanGiao = double.Parse(txtTiLeHotGiao.Text)
            };
        }

        private ChiTietPhieuGiaCong GetChiTietPhieuGiaCongOfForm()
        {
            if (!IsValidChiTietPhieuGiaCong()) return null;

            if (btnChinhSua.Tag == null)
            {
                PopupService.Instance.Warning(MessageError.UPDATE_RECORD_NOT_SELECTED);
                return null;
            }

            if (!ValidateKhoiLuongHoanThanh())
            {
                PopupService.Instance.Error(MessageError.TOTAL_MASS_INVALID);
                return null;
            }

            return new ChiTietPhieuGiaCong
            {
                NgayHoanTra = detNgayGiao.DateTime,
                KhoiLuongTongHoanThanh = double.Parse(txtKhoiLuongTongXong.Text),
                KhoiLuongDaHoanThanh = double.Parse(txtKhoiLuongDaXong.Text),
                KhoiLuongVangHoanThanh = double.Parse(txtKhoiLuongVangXong.Text),
                TiLeHotHoanThanh = double.Parse(txtTiLeHotXong.Text),
                KhoiLuongTongTruHao = double.Parse(txtKhoiLuongTongTruHao.Text),
                KhoiLuongDaTruHao = double.Parse(txtKhoiLuongDaTruHao.Text),
                KhoiLuongVangTruHao = double.Parse(txtKhoiLuongVangTruHao.Text),
                TiLeHotTruHao = double.Parse(txtTiLeHotTruHao.Text),
                LyDoTruHao = txtLyDoTruHao.Text,
                MaPhieuGiaCong = ((PhieuGiaCong)btnChinhSua.Tag).MaPhieuGiaCong
            };
        }
        #endregion

        #region ShowDataToForm
        private void ShowPhieuGiaCong(PhieuGiaCong phieuGiaCong)
        {
            if (phieuGiaCong.NgayKetThuc != null)
            {
                detNgayKetThuc.DateTime = phieuGiaCong.NgayKetThuc.Value;
            }

            detNgayBatDau.DateTime = phieuGiaCong.NgayGiao;
            lueHoTenTho.EditValue = phieuGiaCong.MaThoGiaCong;
            lueHamLuongVang.EditValue = phieuGiaCong.MaHamLuongVang;
            txtTiLeHotGiao.EditValue = phieuGiaCong.TiLeHotBanGiao;
            txtKhoiLuongTongGiao.EditValue = phieuGiaCong.KhoiLuongTongBanGiao;
            txtKhoiLuongDaGiao.EditValue = phieuGiaCong.KhoiLuongDaBanGiao;
            txtKhoiLuongVangGiao.EditValue = phieuGiaCong.KhoiLuongVangBanGiao;
            txtTiLeHotTruHaoTong.EditValue = phieuGiaCong.TiLeHotTruHao;
            txtKhoiLuongTongTruHaoTong.EditValue = phieuGiaCong.KhoiLuongTongTruHao;
            txtKhoiLuongVangTruHaoTong.EditValue = phieuGiaCong.KhoiLuongVangTruHao;
            txtKhoiLuongDaTruHaoTong.EditValue = phieuGiaCong.KhoiLuongDaTruHao;
        }

        private void ShowChiTietPhieuGiaCong(ChiTietPhieuGiaCong chiTietPhieuGiaCong)
        {
            detNgayGiao.DateTime = chiTietPhieuGiaCong.NgayHoanTra;
            txtTiLeHotXong.EditValue = chiTietPhieuGiaCong.TiLeHotHoanThanh;
            txtKhoiLuongTongXong.EditValue = chiTietPhieuGiaCong.KhoiLuongTongHoanThanh;
            txtKhoiLuongVangXong.EditValue = chiTietPhieuGiaCong.KhoiLuongVangHoanThanh;
            txtKhoiLuongDaXong.EditValue = chiTietPhieuGiaCong.KhoiLuongDaHoanThanh;
            txtTiLeHotTruHao.EditValue = chiTietPhieuGiaCong.TiLeHotTruHao;
            txtKhoiLuongTongTruHao.EditValue = chiTietPhieuGiaCong.KhoiLuongTongTruHao;
            txtKhoiLuongVangTruHao.EditValue = chiTietPhieuGiaCong.KhoiLuongVangTruHao;
            txtKhoiLuongDaTruHao.EditValue = chiTietPhieuGiaCong.KhoiLuongDaTruHao;
            txtLyDoTruHao.Text = chiTietPhieuGiaCong.LyDoTruHao;
        }
        #endregion

        private void btnXoaPhieuGiaCong_Click(object sender, EventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            PhieuGiaCong phieuGiaCong = (PhieuGiaCong)grvPhieuGiaCong.GetFocusedRow();

            if (phieuGiaCong == null) return;

            if (PhieuGiaCongBUS.Instance.DeleteById(phieuGiaCong.MaPhieuGiaCong) != null)
            {
                PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                GetData();
                ClearFormService.ClearAllForm(this, new List<string>());
                btnChinhSua.Tag = null;
                grcChiTietGiaCong.DataSource = null;
            }
            else
            {
                PopupService.Instance.Error(MessageError.DELETE_ERROR);
            }
        }

        private void btnXoaChiTietPhieuGiaCong_Click(object sender, EventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            ChiTietPhieuGiaCong chiTietPhieuGiaCong = (ChiTietPhieuGiaCong)grvChiTietGiaCong.GetFocusedRow();

            if (chiTietPhieuGiaCong == null) return;

            if (ChiTietPhieuGiaCongBUS.Instance.DeleteById(chiTietPhieuGiaCong.MaChiTietPhieuGiaCong) != null)
            {
                PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                GetData();
                ClearFormService.ClearAllForm(this, listOfFormPhieuGiaCong);
                grcChiTietGiaCong.DataSource = ChiTietPhieuGiaCongBUS.Instance.GetListByMaPhieuGiaCong(chiTietPhieuGiaCong.MaPhieuGiaCong);
            }
            else
            {
                PopupService.Instance.Error(MessageError.DELETE_ERROR);
            }
        }

        private void btnThemMoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PhieuGiaCong phieuGiaCong = GetPhieuGiaCongOfForm();

            if (phieuGiaCong == null) return;

            if (PhieuGiaCongBUS.Instance.Insert(phieuGiaCong) != null)
            {
                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                GetData();
                ClearFormService.ClearAllForm(this, new List<string>());
                btnChinhSua.Tag = null;
            }
            else
            {
                PopupService.Instance.Error(MessageError.INSERT_ERROR);
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetData();
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearFormService.ClearAllForm(this, new List<string>());
            btnChinhSua.Tag = null;
            grcChiTietGiaCong.DataSource = null;
        }

        private void grcPhieuGiaCong_Click(object sender, EventArgs e)
        {
            PhieuGiaCong phieuGiaCong = (PhieuGiaCong)grvPhieuGiaCong.GetFocusedRow();

            if (phieuGiaCong == null) return;

            ShowPhieuGiaCong(phieuGiaCong);
            btnChinhSua.Tag = phieuGiaCong;
            grcChiTietGiaCong.DataSource = ChiTietPhieuGiaCongBUS.Instance.GetListByMaPhieuGiaCong(phieuGiaCong.MaPhieuGiaCong);
        }

        private void grcChiTietGiaCong_Click(object sender, EventArgs e)
        {
            ChiTietPhieuGiaCong chiTietPhieuGiaCong = (ChiTietPhieuGiaCong)grvChiTietGiaCong.GetFocusedRow();

            if (chiTietPhieuGiaCong == null) return;

            btnChinhSuaThongTinChiTiet.Tag = chiTietPhieuGiaCong;
            ShowChiTietPhieuGiaCong(chiTietPhieuGiaCong);
        }

        private void btnThemThongTinChiTiet_Click(object sender, EventArgs e)
        {
            ChiTietPhieuGiaCong chiTietPhieuGiaCong = GetChiTietPhieuGiaCongOfForm();

            if (chiTietPhieuGiaCong == null) return;

            if (ChiTietPhieuGiaCongBUS.Instance.Insert(chiTietPhieuGiaCong) != null)
            {
                if (btnChinhSua.Tag != null)
                {
                    PhieuGiaCong phieuGiaCong = (PhieuGiaCong)btnChinhSua.Tag;
                    grcChiTietGiaCong.DataSource = ChiTietPhieuGiaCongBUS.Instance.GetListByMaPhieuGiaCong(phieuGiaCong.MaPhieuGiaCong);
                }

                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                ClearFormService.ClearAllForm(this, listOfFormPhieuGiaCong);
                GetData();
            }
            else
            {
                PopupService.Instance.Error(MessageError.INSERT_ERROR);
            }
        }

        private void btnChinhSuaThongTinChiTiet_Click(object sender, EventArgs e)
        {
            ChiTietPhieuGiaCong chiTietPhieuGiaCong = GetChiTietPhieuGiaCongOfForm();
            ChiTietPhieuGiaCong chiTietPhieuOfButtonTag = (ChiTietPhieuGiaCong)btnChinhSuaThongTinChiTiet.Tag;

            if (chiTietPhieuGiaCong == null || chiTietPhieuOfButtonTag == null) return;

            chiTietPhieuGiaCong.MaChiTietPhieuGiaCong = chiTietPhieuOfButtonTag.MaChiTietPhieuGiaCong;

            if (ChiTietPhieuGiaCongBUS.Instance.Update(chiTietPhieuGiaCong) != null)
            {
                if (btnChinhSua.Tag != null)
                {
                    PhieuGiaCong phieuGiaCong = (PhieuGiaCong)btnChinhSua.Tag;
                    grcChiTietGiaCong.DataSource = ChiTietPhieuGiaCongBUS.Instance.GetListByMaPhieuGiaCong(phieuGiaCong.MaPhieuGiaCong);
                }

                PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                ClearFormService.ClearAllForm(this, listOfFormPhieuGiaCong);
                GetData();
            }
            else
            {
                PopupService.Instance.Error(MessageError.UPDATE_ERROR);
            }
        }
    }
}