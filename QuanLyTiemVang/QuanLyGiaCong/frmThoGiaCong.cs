﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyTiemVang.Constants.StringUltils;
using DTO.Models;
using BUS;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;

namespace QuanLyTiemVang.QuanLyGiaCong
{
    public partial class frmThoGiaCong : DevExpress.XtraEditors.XtraForm
    {
        public frmThoGiaCong()
        {
            InitializeComponent();
        }

        private void frmThoGiaCong_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            grcThoGiaCong.DataSource = ThoGiaCongBUS.Instance.GetListAllThoGiaCong();
        }

        private bool IsValidForm()
        {
            dxerHoTen.Dispose();

            bool check = true;

            if (string.IsNullOrWhiteSpace(txtHoTen.Text))
            {
                dxerHoTen.SetError(txtHoTen, ValidationUltils.INPUT_NULL);
                check = false;
            }

            return check;
        }

        private ThoGiaCong LayThongTinTuForm()
        {
            if (!IsValidForm()) return null;

            return new ThoGiaCong
            {
                HoTen = txtHoTen.Text,
                ChungMinhNhanDan = txtChungMinhNhanDan.Text,
                DiaChi = txtDiaChi.Text,
                SoDienThoai = txtSoDienThoai.Text
            };
        }

        private void HienThiThongTinLenForm(ThoGiaCong thoGiaCong)
        {
            txtHoTen.Text = thoGiaCong.HoTen;
            txtChungMinhNhanDan.Text = thoGiaCong.ChungMinhNhanDan;
            txtDiaChi.Text = thoGiaCong.DiaChi;
            txtSoDienThoai.Text = thoGiaCong.SoDienThoai;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            ThoGiaCong thoGiaCong = (ThoGiaCong)grvThoGiaCong.GetFocusedRow();
            
            if (ThoGiaCongBUS.Instance.DeleteById(thoGiaCong.MaThoGiaCong) != null)
            {
                PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                LoadData();
            }
            else
            {
                PopupService.Instance.Error(MessageError.DELETE_ERROR);
            }
        }

        private void btnThemMoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ThoGiaCong thoGiaCong = LayThongTinTuForm();

            if (thoGiaCong != null)
            {
                if (ThoGiaCongBUS.Instance.Insert(thoGiaCong) != null)
                {
                    PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                    ClearFormService.ClearAllForm(this, new List<string>());
                    LoadData();
                }
                else
                {
                    PopupService.Instance.Error(MessageError.INSERT_ERROR);
                }
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearFormService.ClearAllForm(this, new List<string>());
            btnChinhSua.Tag = null;
        }

        private void btnChinhSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnChinhSua.Tag == null)
            {
                PopupService.Instance.Warning(MessageError.UPDATE_RECORD_NOT_SELECTED);
                return;
            }

            ThoGiaCong thoGiaCongUpdate = LayThongTinTuForm();

            thoGiaCongUpdate.MaThoGiaCong = ((ThoGiaCong)btnChinhSua.Tag).MaThoGiaCong;

            if (ThoGiaCongBUS.Instance.Update(thoGiaCongUpdate) != null)
            {
                PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                ClearFormService.ClearAllForm(this, new List<string>());
                btnChinhSua.Tag = null;
                LoadData();
            }
            else
            {
                PopupService.Instance.Error(MessageError.UPDATE_ERROR);
            }
        }

        private void grcThoGiaCong_Click(object sender, EventArgs e)
        {
            try
            {
                ThoGiaCong thoGiaCong = (ThoGiaCong)grvThoGiaCong.GetFocusedRow();
                btnChinhSua.Tag = thoGiaCong;

                HienThiThongTinLenForm(thoGiaCong);
            }
            catch { }
        }
    }
}