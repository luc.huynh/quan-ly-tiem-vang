﻿namespace QuanLyTiemVang.QuanLyGiaCong
{
    partial class frmPhieuGiaCong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnThemMoi = new DevExpress.XtraBars.BarButtonItem();
            this.btnChinhSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lueHamLuongVang = new DevExpress.XtraEditors.LookUpEdit();
            this.detNgayKetThuc = new DevExpress.XtraEditors.DateEdit();
            this.txtKhoiLuongDaTruHaoTong = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongVangTruHaoTong = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongTongTruHaoTong = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHotTruHaoTong = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongDaGiao = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongVangGiao = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongTongGiao = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHotGiao = new DevExpress.XtraEditors.TextEdit();
            this.lueHoTenTho = new DevExpress.XtraEditors.LookUpEdit();
            this.detNgayBatDau = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcPhieuGiaCong = new DevExpress.XtraGrid.GridControl();
            this.phieuGiaCongBindingSource = new System.Windows.Forms.BindingSource();
            this.grvPhieuGiaCong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNgayGiao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTongBanGiao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTongTruHao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayKetThuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThoGiaCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaPhieuGiaCong = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.grcChiTietGiaCong = new DevExpress.XtraGrid.GridControl();
            this.chiTietPhieuGiaCongBindingSource = new System.Windows.Forms.BindingSource();
            this.grvChiTietGiaCong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNgayHoanTra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTongHoanThanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTongTruHao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoaChiTietPhieuGiaCong = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.btnChinhSuaThongTinChiTiet = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemThongTinChiTiet = new DevExpress.XtraEditors.SimpleButton();
            this.txtTiLeHotTruHao = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongTongTruHao = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDoTruHao = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongDaXong = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongVangXong = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongDaTruHao = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongTongXong = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoiLuongVangTruHao = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHotXong = new DevExpress.XtraEditors.TextEdit();
            this.detNgayGiao = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxerNgayBatDau = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerHoTenTho = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerHamLuongVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerTiLeHotGiao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongTongGiao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongVangGiao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongDaGiao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerNgayGiao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerTiLeHotXong = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongTongXong = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongVangXong = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongDaXong = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerTiLeHotTruHao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongTongTruHao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongVangTruHao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dxerKhoiLuongDaTruHao = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayKetThuc.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayKetThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaTruHaoTong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangTruHaoTong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongTruHaoTong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotTruHaoTong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHoTenTho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBatDau.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBatDau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcPhieuGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phieuGiaCongBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPhieuGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaPhieuGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcChiTietGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chiTietPhieuGiaCongBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvChiTietGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaChiTietPhieuGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotTruHao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongTruHao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDoTruHao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaXong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangXong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaTruHao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongXong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangTruHao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotXong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayGiao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayBatDau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHoTenTho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHotGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongTongGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongVangGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongDaGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHotXong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongTongXong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongVangXong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongDaXong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHotTruHao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongTongTruHao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongVangTruHao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongDaTruHao)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnThemMoi,
            this.btnChinhSua});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 11;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar2.FloatSize = new System.Drawing.Size(198, 82);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThemMoi, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnChinhSua, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnThemMoi
            // 
            this.btnThemMoi.Caption = "Thêm mới";
            this.btnThemMoi.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThemMoi.Id = 9;
            this.btnThemMoi.Name = "btnThemMoi";
            this.btnThemMoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThemMoi_ItemClick);
            // 
            // btnChinhSua
            // 
            this.btnChinhSua.Caption = "Chỉnh sửa";
            this.btnChinhSua.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnChinhSua.Id = 10;
            this.btnChinhSua.Name = "btnChinhSua";
            this.btnChinhSua.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1200, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 627);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1200, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 577);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(1200, 50);
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl1.Size = new System.Drawing.Size(0, 577);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 6;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 7;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1200, 137);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Thông tin phiếu gia công";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lueHamLuongVang);
            this.layoutControl1.Controls.Add(this.detNgayKetThuc);
            this.layoutControl1.Controls.Add(this.txtKhoiLuongDaTruHaoTong);
            this.layoutControl1.Controls.Add(this.txtKhoiLuongVangTruHaoTong);
            this.layoutControl1.Controls.Add(this.txtKhoiLuongTongTruHaoTong);
            this.layoutControl1.Controls.Add(this.txtTiLeHotTruHaoTong);
            this.layoutControl1.Controls.Add(this.txtKhoiLuongDaGiao);
            this.layoutControl1.Controls.Add(this.txtKhoiLuongVangGiao);
            this.layoutControl1.Controls.Add(this.txtKhoiLuongTongGiao);
            this.layoutControl1.Controls.Add(this.txtTiLeHotGiao);
            this.layoutControl1.Controls.Add(this.lueHoTenTho);
            this.layoutControl1.Controls.Add(this.detNgayBatDau);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1196, 110);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lueHamLuongVang
            // 
            this.lueHamLuongVang.Location = new System.Drawing.Point(999, 16);
            this.lueHamLuongVang.MenuManager = this.barManager1;
            this.lueHamLuongVang.Name = "lueHamLuongVang";
            this.lueHamLuongVang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVang.Properties.NullText = "";
            this.lueHamLuongVang.Size = new System.Drawing.Size(181, 22);
            this.lueHamLuongVang.StyleController = this.layoutControl1;
            this.lueHamLuongVang.TabIndex = 15;
            // 
            // detNgayKetThuc
            // 
            this.detNgayKetThuc.EditValue = null;
            this.detNgayKetThuc.Location = new System.Drawing.Point(414, 16);
            this.detNgayKetThuc.MenuManager = this.barManager1;
            this.detNgayKetThuc.Name = "detNgayKetThuc";
            this.detNgayKetThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayKetThuc.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayKetThuc.Properties.ReadOnly = true;
            this.detNgayKetThuc.Size = new System.Drawing.Size(181, 22);
            this.detNgayKetThuc.StyleController = this.layoutControl1;
            this.detNgayKetThuc.TabIndex = 14;
            // 
            // txtKhoiLuongDaTruHaoTong
            // 
            this.txtKhoiLuongDaTruHaoTong.Location = new System.Drawing.Point(999, 72);
            this.txtKhoiLuongDaTruHaoTong.MenuManager = this.barManager1;
            this.txtKhoiLuongDaTruHaoTong.Name = "txtKhoiLuongDaTruHaoTong";
            this.txtKhoiLuongDaTruHaoTong.Properties.MaxLength = 9;
            this.txtKhoiLuongDaTruHaoTong.Properties.NullText = "0";
            this.txtKhoiLuongDaTruHaoTong.Properties.ReadOnly = true;
            this.txtKhoiLuongDaTruHaoTong.Size = new System.Drawing.Size(181, 22);
            this.txtKhoiLuongDaTruHaoTong.StyleController = this.layoutControl1;
            this.txtKhoiLuongDaTruHaoTong.TabIndex = 13;
            // 
            // txtKhoiLuongVangTruHaoTong
            // 
            this.txtKhoiLuongVangTruHaoTong.Location = new System.Drawing.Point(707, 72);
            this.txtKhoiLuongVangTruHaoTong.MenuManager = this.barManager1;
            this.txtKhoiLuongVangTruHaoTong.Name = "txtKhoiLuongVangTruHaoTong";
            this.txtKhoiLuongVangTruHaoTong.Properties.MaxLength = 9;
            this.txtKhoiLuongVangTruHaoTong.Properties.NullText = "0";
            this.txtKhoiLuongVangTruHaoTong.Properties.ReadOnly = true;
            this.txtKhoiLuongVangTruHaoTong.Size = new System.Drawing.Size(180, 22);
            this.txtKhoiLuongVangTruHaoTong.StyleController = this.layoutControl1;
            this.txtKhoiLuongVangTruHaoTong.TabIndex = 12;
            // 
            // txtKhoiLuongTongTruHaoTong
            // 
            this.txtKhoiLuongTongTruHaoTong.Location = new System.Drawing.Point(414, 72);
            this.txtKhoiLuongTongTruHaoTong.MenuManager = this.barManager1;
            this.txtKhoiLuongTongTruHaoTong.Name = "txtKhoiLuongTongTruHaoTong";
            this.txtKhoiLuongTongTruHaoTong.Properties.MaxLength = 9;
            this.txtKhoiLuongTongTruHaoTong.Properties.NullText = "0";
            this.txtKhoiLuongTongTruHaoTong.Properties.ReadOnly = true;
            this.txtKhoiLuongTongTruHaoTong.Size = new System.Drawing.Size(181, 22);
            this.txtKhoiLuongTongTruHaoTong.StyleController = this.layoutControl1;
            this.txtKhoiLuongTongTruHaoTong.TabIndex = 11;
            // 
            // txtTiLeHotTruHaoTong
            // 
            this.txtTiLeHotTruHaoTong.Location = new System.Drawing.Point(122, 72);
            this.txtTiLeHotTruHaoTong.MenuManager = this.barManager1;
            this.txtTiLeHotTruHaoTong.Name = "txtTiLeHotTruHaoTong";
            this.txtTiLeHotTruHaoTong.Properties.MaxLength = 9;
            this.txtTiLeHotTruHaoTong.Properties.NullText = "0";
            this.txtTiLeHotTruHaoTong.Properties.ReadOnly = true;
            this.txtTiLeHotTruHaoTong.Size = new System.Drawing.Size(180, 22);
            this.txtTiLeHotTruHaoTong.StyleController = this.layoutControl1;
            this.txtTiLeHotTruHaoTong.TabIndex = 10;
            // 
            // txtKhoiLuongDaGiao
            // 
            this.txtKhoiLuongDaGiao.Location = new System.Drawing.Point(999, 44);
            this.txtKhoiLuongDaGiao.MenuManager = this.barManager1;
            this.txtKhoiLuongDaGiao.Name = "txtKhoiLuongDaGiao";
            this.txtKhoiLuongDaGiao.Properties.MaxLength = 9;
            this.txtKhoiLuongDaGiao.Properties.NullText = "0";
            this.txtKhoiLuongDaGiao.Size = new System.Drawing.Size(181, 22);
            this.txtKhoiLuongDaGiao.StyleController = this.layoutControl1;
            this.txtKhoiLuongDaGiao.TabIndex = 9;
            // 
            // txtKhoiLuongVangGiao
            // 
            this.txtKhoiLuongVangGiao.Location = new System.Drawing.Point(707, 44);
            this.txtKhoiLuongVangGiao.MenuManager = this.barManager1;
            this.txtKhoiLuongVangGiao.Name = "txtKhoiLuongVangGiao";
            this.txtKhoiLuongVangGiao.Properties.MaxLength = 9;
            this.txtKhoiLuongVangGiao.Properties.NullText = "0";
            this.txtKhoiLuongVangGiao.Size = new System.Drawing.Size(180, 22);
            this.txtKhoiLuongVangGiao.StyleController = this.layoutControl1;
            this.txtKhoiLuongVangGiao.TabIndex = 8;
            // 
            // txtKhoiLuongTongGiao
            // 
            this.txtKhoiLuongTongGiao.Location = new System.Drawing.Point(414, 44);
            this.txtKhoiLuongTongGiao.MenuManager = this.barManager1;
            this.txtKhoiLuongTongGiao.Name = "txtKhoiLuongTongGiao";
            this.txtKhoiLuongTongGiao.Properties.MaxLength = 9;
            this.txtKhoiLuongTongGiao.Properties.NullText = "0";
            this.txtKhoiLuongTongGiao.Size = new System.Drawing.Size(181, 22);
            this.txtKhoiLuongTongGiao.StyleController = this.layoutControl1;
            this.txtKhoiLuongTongGiao.TabIndex = 7;
            // 
            // txtTiLeHotGiao
            // 
            this.txtTiLeHotGiao.Location = new System.Drawing.Point(122, 44);
            this.txtTiLeHotGiao.MenuManager = this.barManager1;
            this.txtTiLeHotGiao.Name = "txtTiLeHotGiao";
            this.txtTiLeHotGiao.Properties.MaxLength = 9;
            this.txtTiLeHotGiao.Properties.NullText = "0";
            this.txtTiLeHotGiao.Size = new System.Drawing.Size(180, 22);
            this.txtTiLeHotGiao.StyleController = this.layoutControl1;
            this.txtTiLeHotGiao.TabIndex = 6;
            // 
            // lueHoTenTho
            // 
            this.lueHoTenTho.Location = new System.Drawing.Point(707, 16);
            this.lueHoTenTho.MenuManager = this.barManager1;
            this.lueHoTenTho.Name = "lueHoTenTho";
            this.lueHoTenTho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHoTenTho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HoTen", "Họ và tên")});
            this.lueHoTenTho.Properties.NullText = "";
            this.lueHoTenTho.Size = new System.Drawing.Size(180, 22);
            this.lueHoTenTho.StyleController = this.layoutControl1;
            this.lueHoTenTho.TabIndex = 5;
            // 
            // detNgayBatDau
            // 
            this.detNgayBatDau.EditValue = null;
            this.detNgayBatDau.Location = new System.Drawing.Point(122, 16);
            this.detNgayBatDau.MenuManager = this.barManager1;
            this.detNgayBatDau.Name = "detNgayBatDau";
            this.detNgayBatDau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayBatDau.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayBatDau.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.detNgayBatDau.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayBatDau.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.detNgayBatDau.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayBatDau.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayBatDau.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detNgayBatDau.Size = new System.Drawing.Size(180, 22);
            this.detNgayBatDau.StyleController = this.layoutControl1;
            this.detNgayBatDau.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1196, 110);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.detNgayBatDau;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(292, 28);
            this.layoutControlItem1.Text = "Ngày bắt đầu";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueHoTenTho;
            this.layoutControlItem2.Location = new System.Drawing.Point(585, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(292, 28);
            this.layoutControlItem2.Text = "Họ tên thợ";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtTiLeHotGiao;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(292, 28);
            this.layoutControlItem3.Text = "Tỉ lệ hột giao";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtKhoiLuongTongGiao;
            this.layoutControlItem4.Location = new System.Drawing.Point(292, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem4.Text = "KLT giao";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtKhoiLuongVangGiao;
            this.layoutControlItem5.Location = new System.Drawing.Point(585, 28);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(292, 28);
            this.layoutControlItem5.Text = "KLV giao";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtKhoiLuongDaGiao;
            this.layoutControlItem6.Location = new System.Drawing.Point(877, 28);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem6.Text = "KLĐ giao";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(102, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtTiLeHotTruHaoTong;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(292, 28);
            this.layoutControlItem7.Text = "Tỉ lệ hột trừ hao";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtKhoiLuongTongTruHaoTong;
            this.layoutControlItem8.Location = new System.Drawing.Point(292, 56);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem8.Text = "KLT trừ hao";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtKhoiLuongVangTruHaoTong;
            this.layoutControlItem9.Location = new System.Drawing.Point(585, 56);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(292, 28);
            this.layoutControlItem9.Text = "KLV trừ hao";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtKhoiLuongDaTruHaoTong;
            this.layoutControlItem10.Location = new System.Drawing.Point(877, 56);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem10.Text = "KLĐ trừ hao";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.detNgayKetThuc;
            this.layoutControlItem11.Location = new System.Drawing.Point(292, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem11.Text = "Ngày kết thúc";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.lueHamLuongVang;
            this.layoutControlItem12.Location = new System.Drawing.Point(877, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem12.Text = "Hàm lượng vàng";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(102, 17);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcPhieuGiaCong);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl2.Location = new System.Drawing.Point(0, 187);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(574, 440);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Danh sách phiếu gia công";
            // 
            // grcPhieuGiaCong
            // 
            this.grcPhieuGiaCong.DataSource = this.phieuGiaCongBindingSource;
            this.grcPhieuGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcPhieuGiaCong.Location = new System.Drawing.Point(2, 25);
            this.grcPhieuGiaCong.MainView = this.grvPhieuGiaCong;
            this.grcPhieuGiaCong.MenuManager = this.barManager1;
            this.grcPhieuGiaCong.Name = "grcPhieuGiaCong";
            this.grcPhieuGiaCong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaPhieuGiaCong});
            this.grcPhieuGiaCong.Size = new System.Drawing.Size(570, 413);
            this.grcPhieuGiaCong.TabIndex = 0;
            this.grcPhieuGiaCong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvPhieuGiaCong});
            this.grcPhieuGiaCong.Click += new System.EventHandler(this.grcPhieuGiaCong_Click);
            // 
            // phieuGiaCongBindingSource
            // 
            this.phieuGiaCongBindingSource.DataSource = typeof(DTO.Models.PhieuGiaCong);
            // 
            // grvPhieuGiaCong
            // 
            this.grvPhieuGiaCong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNgayGiao,
            this.colKhoiLuongTongBanGiao,
            this.colKhoiLuongTongTruHao1,
            this.colNgayKetThuc,
            this.colKiHieuHamLuongVang,
            this.colThoGiaCong,
            this.gridColumn1});
            this.grvPhieuGiaCong.GridControl = this.grcPhieuGiaCong;
            this.grvPhieuGiaCong.Name = "grvPhieuGiaCong";
            this.grvPhieuGiaCong.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvPhieuGiaCong.OptionsView.ShowGroupPanel = false;
            // 
            // colNgayGiao
            // 
            this.colNgayGiao.Caption = "Ngày giao";
            this.colNgayGiao.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayGiao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayGiao.FieldName = "NgayGiao";
            this.colNgayGiao.Name = "colNgayGiao";
            this.colNgayGiao.Visible = true;
            this.colNgayGiao.VisibleIndex = 0;
            // 
            // colKhoiLuongTongBanGiao
            // 
            this.colKhoiLuongTongBanGiao.Caption = "KLT bàn giao";
            this.colKhoiLuongTongBanGiao.FieldName = "KhoiLuongTongBanGiao";
            this.colKhoiLuongTongBanGiao.Name = "colKhoiLuongTongBanGiao";
            this.colKhoiLuongTongBanGiao.Visible = true;
            this.colKhoiLuongTongBanGiao.VisibleIndex = 2;
            this.colKhoiLuongTongBanGiao.Width = 88;
            // 
            // colKhoiLuongTongTruHao1
            // 
            this.colKhoiLuongTongTruHao1.Caption = "KLT trừ hao";
            this.colKhoiLuongTongTruHao1.FieldName = "KhoiLuongTongTruHao";
            this.colKhoiLuongTongTruHao1.Name = "colKhoiLuongTongTruHao1";
            this.colKhoiLuongTongTruHao1.Visible = true;
            this.colKhoiLuongTongTruHao1.VisibleIndex = 3;
            this.colKhoiLuongTongTruHao1.Width = 87;
            // 
            // colNgayKetThuc
            // 
            this.colNgayKetThuc.Caption = "Ngày kết thúc";
            this.colNgayKetThuc.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayKetThuc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayKetThuc.FieldName = "NgayKetThuc";
            this.colNgayKetThuc.Name = "colNgayKetThuc";
            this.colNgayKetThuc.Visible = true;
            this.colNgayKetThuc.VisibleIndex = 5;
            this.colNgayKetThuc.Width = 98;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "HLV";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 1;
            this.colKiHieuHamLuongVang.Width = 56;
            // 
            // colThoGiaCong
            // 
            this.colThoGiaCong.Caption = "Họ tên thợ";
            this.colThoGiaCong.FieldName = "ThoGiaCong.HoTen";
            this.colThoGiaCong.Name = "colThoGiaCong";
            this.colThoGiaCong.Visible = true;
            this.colThoGiaCong.VisibleIndex = 4;
            this.colThoGiaCong.Width = 76;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Xóa";
            this.gridColumn1.ColumnEdit = this.btnXoaPhieuGiaCong;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            this.gridColumn1.Width = 50;
            // 
            // btnXoaPhieuGiaCong
            // 
            this.btnXoaPhieuGiaCong.AutoHeight = false;
            this.btnXoaPhieuGiaCong.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xóa", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, true)});
            this.btnXoaPhieuGiaCong.Name = "btnXoaPhieuGiaCong";
            this.btnXoaPhieuGiaCong.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaPhieuGiaCong.Click += new System.EventHandler(this.btnXoaPhieuGiaCong_Click);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(574, 187);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 440);
            this.splitterControl1.TabIndex = 7;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.groupControl5);
            this.groupControl3.Controls.Add(this.groupControl4);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(580, 187);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(620, 440);
            this.groupControl3.TabIndex = 8;
            this.groupControl3.Text = "Thông tin chi tiết của mỗi phiếu gia công";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.grcChiTietGiaCong);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl5.Location = new System.Drawing.Point(2, 208);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(616, 230);
            this.groupControl5.TabIndex = 1;
            this.groupControl5.Text = "Danh sách chi tiết bàn giao ";
            // 
            // grcChiTietGiaCong
            // 
            this.grcChiTietGiaCong.DataSource = this.chiTietPhieuGiaCongBindingSource;
            this.grcChiTietGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcChiTietGiaCong.Location = new System.Drawing.Point(2, 25);
            this.grcChiTietGiaCong.MainView = this.grvChiTietGiaCong;
            this.grcChiTietGiaCong.MenuManager = this.barManager1;
            this.grcChiTietGiaCong.Name = "grcChiTietGiaCong";
            this.grcChiTietGiaCong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXoaChiTietPhieuGiaCong});
            this.grcChiTietGiaCong.Size = new System.Drawing.Size(612, 203);
            this.grcChiTietGiaCong.TabIndex = 0;
            this.grcChiTietGiaCong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvChiTietGiaCong});
            this.grcChiTietGiaCong.Click += new System.EventHandler(this.grcChiTietGiaCong_Click);
            // 
            // chiTietPhieuGiaCongBindingSource
            // 
            this.chiTietPhieuGiaCongBindingSource.DataSource = typeof(DTO.Models.ChiTietPhieuGiaCong);
            // 
            // grvChiTietGiaCong
            // 
            this.grvChiTietGiaCong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNgayHoanTra,
            this.colKhoiLuongTongHoanThanh,
            this.colKhoiLuongTongTruHao,
            this.gridColumn2});
            this.grvChiTietGiaCong.GridControl = this.grcChiTietGiaCong;
            this.grvChiTietGiaCong.Name = "grvChiTietGiaCong";
            this.grvChiTietGiaCong.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvChiTietGiaCong.OptionsView.ShowGroupPanel = false;
            // 
            // colNgayHoanTra
            // 
            this.colNgayHoanTra.Caption = "Ngày hoàn trả";
            this.colNgayHoanTra.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayHoanTra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayHoanTra.FieldName = "NgayHoanTra";
            this.colNgayHoanTra.Name = "colNgayHoanTra";
            this.colNgayHoanTra.Visible = true;
            this.colNgayHoanTra.VisibleIndex = 0;
            // 
            // colKhoiLuongTongHoanThanh
            // 
            this.colKhoiLuongTongHoanThanh.Caption = "KLT hoàn thành";
            this.colKhoiLuongTongHoanThanh.FieldName = "KhoiLuongTongHoanThanh";
            this.colKhoiLuongTongHoanThanh.Name = "colKhoiLuongTongHoanThanh";
            this.colKhoiLuongTongHoanThanh.Visible = true;
            this.colKhoiLuongTongHoanThanh.VisibleIndex = 1;
            // 
            // colKhoiLuongTongTruHao
            // 
            this.colKhoiLuongTongTruHao.Caption = "KLT trừ hao";
            this.colKhoiLuongTongTruHao.FieldName = "KhoiLuongTongTruHao";
            this.colKhoiLuongTongTruHao.Name = "colKhoiLuongTongTruHao";
            this.colKhoiLuongTongTruHao.Visible = true;
            this.colKhoiLuongTongTruHao.VisibleIndex = 2;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Xóa";
            this.gridColumn2.ColumnEdit = this.btnXoaChiTietPhieuGiaCong;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            // 
            // btnXoaChiTietPhieuGiaCong
            // 
            this.btnXoaChiTietPhieuGiaCong.AutoHeight = false;
            this.btnXoaChiTietPhieuGiaCong.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xóa", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.btnXoaChiTietPhieuGiaCong.Name = "btnXoaChiTietPhieuGiaCong";
            this.btnXoaChiTietPhieuGiaCong.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnXoaChiTietPhieuGiaCong.Click += new System.EventHandler(this.btnXoaChiTietPhieuGiaCong_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.layoutControl3);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(2, 25);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(616, 183);
            this.groupControl4.TabIndex = 0;
            this.groupControl4.Text = "Thông tin bàn giao của thợ";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.btnChinhSuaThongTinChiTiet);
            this.layoutControl3.Controls.Add(this.btnThemThongTinChiTiet);
            this.layoutControl3.Controls.Add(this.txtTiLeHotTruHao);
            this.layoutControl3.Controls.Add(this.txtKhoiLuongTongTruHao);
            this.layoutControl3.Controls.Add(this.txtLyDoTruHao);
            this.layoutControl3.Controls.Add(this.txtKhoiLuongDaXong);
            this.layoutControl3.Controls.Add(this.txtKhoiLuongVangXong);
            this.layoutControl3.Controls.Add(this.txtKhoiLuongDaTruHao);
            this.layoutControl3.Controls.Add(this.txtKhoiLuongTongXong);
            this.layoutControl3.Controls.Add(this.txtKhoiLuongVangTruHao);
            this.layoutControl3.Controls.Add(this.txtTiLeHotXong);
            this.layoutControl3.Controls.Add(this.detNgayGiao);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 25);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(612, 156);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // btnChinhSuaThongTinChiTiet
            // 
            this.btnChinhSuaThongTinChiTiet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChinhSuaThongTinChiTiet.Image = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnChinhSuaThongTinChiTiet.Location = new System.Drawing.Point(309, 100);
            this.btnChinhSuaThongTinChiTiet.Name = "btnChinhSuaThongTinChiTiet";
            this.btnChinhSuaThongTinChiTiet.Size = new System.Drawing.Size(287, 40);
            this.btnChinhSuaThongTinChiTiet.StyleController = this.layoutControl3;
            this.btnChinhSuaThongTinChiTiet.TabIndex = 15;
            this.btnChinhSuaThongTinChiTiet.Text = "Chỉnh sửa thông tin chi tiết";
            this.btnChinhSuaThongTinChiTiet.Click += new System.EventHandler(this.btnChinhSuaThongTinChiTiet_Click);
            // 
            // btnThemThongTinChiTiet
            // 
            this.btnThemThongTinChiTiet.Image = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThemThongTinChiTiet.Location = new System.Drawing.Point(16, 100);
            this.btnThemThongTinChiTiet.Name = "btnThemThongTinChiTiet";
            this.btnThemThongTinChiTiet.Size = new System.Drawing.Size(287, 40);
            this.btnThemThongTinChiTiet.StyleController = this.layoutControl3;
            this.btnThemThongTinChiTiet.TabIndex = 14;
            this.btnThemThongTinChiTiet.Text = "Thêm thông tin chi tiết";
            this.btnThemThongTinChiTiet.Click += new System.EventHandler(this.btnThemThongTinChiTiet_Click);
            // 
            // txtTiLeHotTruHao
            // 
            this.txtTiLeHotTruHao.Location = new System.Drawing.Point(388, 16);
            this.txtTiLeHotTruHao.MenuManager = this.barManager1;
            this.txtTiLeHotTruHao.Name = "txtTiLeHotTruHao";
            this.txtTiLeHotTruHao.Properties.MaxLength = 9;
            this.txtTiLeHotTruHao.Properties.NullText = "0";
            this.txtTiLeHotTruHao.Size = new System.Drawing.Size(61, 22);
            this.txtTiLeHotTruHao.StyleController = this.layoutControl3;
            this.txtTiLeHotTruHao.TabIndex = 13;
            // 
            // txtKhoiLuongTongTruHao
            // 
            this.txtKhoiLuongTongTruHao.Location = new System.Drawing.Point(534, 16);
            this.txtKhoiLuongTongTruHao.MenuManager = this.barManager1;
            this.txtKhoiLuongTongTruHao.Name = "txtKhoiLuongTongTruHao";
            this.txtKhoiLuongTongTruHao.Properties.MaxLength = 9;
            this.txtKhoiLuongTongTruHao.Properties.NullText = "0";
            this.txtKhoiLuongTongTruHao.Size = new System.Drawing.Size(62, 22);
            this.txtKhoiLuongTongTruHao.StyleController = this.layoutControl3;
            this.txtKhoiLuongTongTruHao.TabIndex = 12;
            // 
            // txtLyDoTruHao
            // 
            this.txtLyDoTruHao.Location = new System.Drawing.Point(388, 72);
            this.txtLyDoTruHao.MenuManager = this.barManager1;
            this.txtLyDoTruHao.Name = "txtLyDoTruHao";
            this.txtLyDoTruHao.Size = new System.Drawing.Size(208, 22);
            this.txtLyDoTruHao.StyleController = this.layoutControl3;
            this.txtLyDoTruHao.TabIndex = 11;
            // 
            // txtKhoiLuongDaXong
            // 
            this.txtKhoiLuongDaXong.Location = new System.Drawing.Point(241, 72);
            this.txtKhoiLuongDaXong.MenuManager = this.barManager1;
            this.txtKhoiLuongDaXong.Name = "txtKhoiLuongDaXong";
            this.txtKhoiLuongDaXong.Properties.MaxLength = 9;
            this.txtKhoiLuongDaXong.Properties.NullText = "0";
            this.txtKhoiLuongDaXong.Size = new System.Drawing.Size(62, 22);
            this.txtKhoiLuongDaXong.StyleController = this.layoutControl3;
            this.txtKhoiLuongDaXong.TabIndex = 10;
            // 
            // txtKhoiLuongVangXong
            // 
            this.txtKhoiLuongVangXong.Location = new System.Drawing.Point(95, 72);
            this.txtKhoiLuongVangXong.MenuManager = this.barManager1;
            this.txtKhoiLuongVangXong.Name = "txtKhoiLuongVangXong";
            this.txtKhoiLuongVangXong.Properties.MaxLength = 9;
            this.txtKhoiLuongVangXong.Properties.NullText = "0";
            this.txtKhoiLuongVangXong.Size = new System.Drawing.Size(61, 22);
            this.txtKhoiLuongVangXong.StyleController = this.layoutControl3;
            this.txtKhoiLuongVangXong.TabIndex = 9;
            // 
            // txtKhoiLuongDaTruHao
            // 
            this.txtKhoiLuongDaTruHao.Location = new System.Drawing.Point(534, 44);
            this.txtKhoiLuongDaTruHao.MenuManager = this.barManager1;
            this.txtKhoiLuongDaTruHao.Name = "txtKhoiLuongDaTruHao";
            this.txtKhoiLuongDaTruHao.Properties.NullText = "0";
            this.txtKhoiLuongDaTruHao.Size = new System.Drawing.Size(62, 22);
            this.txtKhoiLuongDaTruHao.StyleController = this.layoutControl3;
            this.txtKhoiLuongDaTruHao.TabIndex = 8;
            // 
            // txtKhoiLuongTongXong
            // 
            this.txtKhoiLuongTongXong.Location = new System.Drawing.Point(241, 44);
            this.txtKhoiLuongTongXong.MenuManager = this.barManager1;
            this.txtKhoiLuongTongXong.Name = "txtKhoiLuongTongXong";
            this.txtKhoiLuongTongXong.Properties.MaxLength = 9;
            this.txtKhoiLuongTongXong.Properties.NullText = "0";
            this.txtKhoiLuongTongXong.Size = new System.Drawing.Size(62, 22);
            this.txtKhoiLuongTongXong.StyleController = this.layoutControl3;
            this.txtKhoiLuongTongXong.TabIndex = 7;
            // 
            // txtKhoiLuongVangTruHao
            // 
            this.txtKhoiLuongVangTruHao.Location = new System.Drawing.Point(388, 44);
            this.txtKhoiLuongVangTruHao.MenuManager = this.barManager1;
            this.txtKhoiLuongVangTruHao.Name = "txtKhoiLuongVangTruHao";
            this.txtKhoiLuongVangTruHao.Properties.NullText = "0";
            this.txtKhoiLuongVangTruHao.Size = new System.Drawing.Size(61, 22);
            this.txtKhoiLuongVangTruHao.StyleController = this.layoutControl3;
            this.txtKhoiLuongVangTruHao.TabIndex = 6;
            // 
            // txtTiLeHotXong
            // 
            this.txtTiLeHotXong.Location = new System.Drawing.Point(95, 44);
            this.txtTiLeHotXong.MenuManager = this.barManager1;
            this.txtTiLeHotXong.Name = "txtTiLeHotXong";
            this.txtTiLeHotXong.Properties.MaxLength = 9;
            this.txtTiLeHotXong.Properties.NullText = "0";
            this.txtTiLeHotXong.Size = new System.Drawing.Size(61, 22);
            this.txtTiLeHotXong.StyleController = this.layoutControl3;
            this.txtTiLeHotXong.TabIndex = 5;
            // 
            // detNgayGiao
            // 
            this.detNgayGiao.EditValue = null;
            this.detNgayGiao.Location = new System.Drawing.Point(95, 16);
            this.detNgayGiao.MenuManager = this.barManager1;
            this.detNgayGiao.Name = "detNgayGiao";
            this.detNgayGiao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayGiao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayGiao.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.detNgayGiao.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayGiao.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.detNgayGiao.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayGiao.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detNgayGiao.Size = new System.Drawing.Size(208, 22);
            this.detNgayGiao.StyleController = this.layoutControl3;
            this.detNgayGiao.TabIndex = 4;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem21,
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(612, 156);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.detNgayGiao;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem13.Text = "Ngày giao";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtTiLeHotXong;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(146, 28);
            this.layoutControlItem14.Text = "TLH xong";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtKhoiLuongVangTruHao;
            this.layoutControlItem15.Location = new System.Drawing.Point(293, 28);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(146, 28);
            this.layoutControlItem15.Text = "KLV trừ hao";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(75, 17);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtKhoiLuongTongXong;
            this.layoutControlItem16.Location = new System.Drawing.Point(146, 28);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(147, 28);
            this.layoutControlItem16.Text = "KLT xong";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtKhoiLuongDaTruHao;
            this.layoutControlItem17.Location = new System.Drawing.Point(439, 28);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(147, 28);
            this.layoutControlItem17.Text = "KLĐ trừ hao";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(75, 17);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtKhoiLuongVangXong;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(146, 28);
            this.layoutControlItem18.Text = "KLV xong";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtKhoiLuongDaXong;
            this.layoutControlItem19.Location = new System.Drawing.Point(146, 56);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(147, 28);
            this.layoutControlItem19.Text = "KLĐ xong";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtLyDoTruHao;
            this.layoutControlItem20.Location = new System.Drawing.Point(293, 56);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(293, 28);
            this.layoutControlItem20.Text = "Lý do ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txtTiLeHotTruHao;
            this.layoutControlItem22.Location = new System.Drawing.Point(293, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(146, 28);
            this.layoutControlItem22.Text = "TLH trừ hao";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(75, 17);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtKhoiLuongTongTruHao;
            this.layoutControlItem21.Location = new System.Drawing.Point(439, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(147, 28);
            this.layoutControlItem21.Text = "KLT trừ hao";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(75, 17);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnThemThongTinChiTiet;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(293, 46);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.btnChinhSuaThongTinChiTiet;
            this.layoutControlItem24.Location = new System.Drawing.Point(293, 84);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(293, 46);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // dxerNgayBatDau
            // 
            this.dxerNgayBatDau.ContainerControl = this;
            // 
            // dxerHoTenTho
            // 
            this.dxerHoTenTho.ContainerControl = this;
            // 
            // dxerHamLuongVang
            // 
            this.dxerHamLuongVang.ContainerControl = this;
            // 
            // dxerTiLeHotGiao
            // 
            this.dxerTiLeHotGiao.ContainerControl = this;
            // 
            // dxerKhoiLuongTongGiao
            // 
            this.dxerKhoiLuongTongGiao.ContainerControl = this;
            // 
            // dxerKhoiLuongVangGiao
            // 
            this.dxerKhoiLuongVangGiao.ContainerControl = this;
            // 
            // dxerKhoiLuongDaGiao
            // 
            this.dxerKhoiLuongDaGiao.ContainerControl = this;
            // 
            // dxerNgayGiao
            // 
            this.dxerNgayGiao.ContainerControl = this;
            // 
            // dxerTiLeHotXong
            // 
            this.dxerTiLeHotXong.ContainerControl = this;
            // 
            // dxerKhoiLuongTongXong
            // 
            this.dxerKhoiLuongTongXong.ContainerControl = this;
            // 
            // dxerKhoiLuongVangXong
            // 
            this.dxerKhoiLuongVangXong.ContainerControl = this;
            // 
            // dxerKhoiLuongDaXong
            // 
            this.dxerKhoiLuongDaXong.ContainerControl = this;
            // 
            // dxerTiLeHotTruHao
            // 
            this.dxerTiLeHotTruHao.ContainerControl = this;
            // 
            // dxerKhoiLuongTongTruHao
            // 
            this.dxerKhoiLuongTongTruHao.ContainerControl = this;
            // 
            // dxerKhoiLuongVangTruHao
            // 
            this.dxerKhoiLuongVangTruHao.ContainerControl = this;
            // 
            // dxerKhoiLuongDaTruHao
            // 
            this.dxerKhoiLuongDaTruHao.ContainerControl = this;
            // 
            // frmPhieuGiaCong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 627);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frmPhieuGiaCong";
            this.Text = "Quản lý phiếu gia công";
            this.Load += new System.EventHandler(this.frmPhieuGiaCong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayKetThuc.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayKetThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaTruHaoTong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangTruHaoTong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongTruHaoTong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotTruHaoTong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHoTenTho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBatDau.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayBatDau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcPhieuGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phieuGiaCongBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPhieuGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaPhieuGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcChiTietGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chiTietPhieuGiaCongBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvChiTietGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXoaChiTietPhieuGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotTruHao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongTruHao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDoTruHao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaXong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangXong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaTruHao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTongXong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongVangTruHao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHotXong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayGiao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayBatDau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHoTenTho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHotGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongTongGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongVangGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongDaGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHotXong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongTongXong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongVangXong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongDaXong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTiLeHotTruHao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongTongTruHao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongVangTruHao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKhoiLuongDaTruHao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnThemMoi;
        private DevExpress.XtraBars.BarButtonItem btnChinhSua;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LookUpEdit lueHoTenTho;
        private DevExpress.XtraEditors.DateEdit detNgayBatDau;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongDaGiao;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongVangGiao;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongTongGiao;
        private DevExpress.XtraEditors.TextEdit txtTiLeHotGiao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongDaTruHaoTong;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongVangTruHaoTong;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongTongTruHaoTong;
        private DevExpress.XtraEditors.TextEdit txtTiLeHotTruHaoTong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVang;
        private DevExpress.XtraEditors.DateEdit detNgayKetThuc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.DateEdit detNgayGiao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraGrid.GridControl grcChiTietGiaCong;
        private DevExpress.XtraGrid.Views.Grid.GridView grvChiTietGiaCong;
        private DevExpress.XtraEditors.SimpleButton btnChinhSuaThongTinChiTiet;
        private DevExpress.XtraEditors.SimpleButton btnThemThongTinChiTiet;
        private DevExpress.XtraEditors.TextEdit txtTiLeHotTruHao;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongTongTruHao;
        private DevExpress.XtraEditors.TextEdit txtLyDoTruHao;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongDaXong;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongVangXong;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongDaTruHao;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongTongXong;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongVangTruHao;
        private DevExpress.XtraEditors.TextEdit txtTiLeHotXong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraGrid.GridControl grcPhieuGiaCong;
        private DevExpress.XtraGrid.Views.Grid.GridView grvPhieuGiaCong;
        private System.Windows.Forms.BindingSource chiTietPhieuGiaCongBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayHoanTra;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTongHoanThanh;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTongTruHao;
        private System.Windows.Forms.BindingSource phieuGiaCongBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayGiao;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTongBanGiao;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTongTruHao1;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayKetThuc;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colThoGiaCong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNgayBatDau;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHoTenTho;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHamLuongVang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTiLeHotGiao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongTongGiao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongVangGiao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongDaGiao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNgayGiao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTiLeHotXong;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongTongXong;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongVangXong;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongDaXong;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTiLeHotTruHao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongTongTruHao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongVangTruHao;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKhoiLuongDaTruHao;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaPhieuGiaCong;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXoaChiTietPhieuGiaCong;
    }
}