﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.Data.Entity;
using DTO.Models;
using DTO.Migrations;

namespace QuanLyTiemVang
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<QuanLyTiemVangDbContext, Configuration>());
            CreateDabase();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            Application.Run(new DANGNHAP.frmDangNhap());
        }

        public static void CreateDabase()
        {
            var context = new QuanLyTiemVangDbContext();
            context.Database.Initialize(true);
        }
    }
}
