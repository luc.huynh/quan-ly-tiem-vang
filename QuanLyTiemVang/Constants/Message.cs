﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Constants
{
    public class MessageQuestion
    {
        public static string THANH_TOAN_CAM_DO = "Xác nhận thanh toán hợp đồng cầm đồ đang chọn?";
        public static string THANH_LY_CAM_DO = "Xác nhận thanh lý hợp đồng đang chọn?";
        public static string XOA_MOI_SAN_PHAM_VANG_DOI_RA = "Nếu xóa hết sản phẩm vàng đổi ngang ra, thì mọi sản phẩm vàng đổi ngang vào cũng sẽ bị xóa theo?";
    }

    public class MessageWarning
    {
        public static string DELETE_DATA_ROW = "Bạn có chắc chắn xóa dòng dữ liệu đang chọn?";
        public static string DELETE_HOA_DON = "Bạn có chắc chắn xóa hóa đơn đang chọn?";
        public static string UPDATE_DATA_ROW = "Bạn có chắc chắn cập nhật dữ liệu hiện tại?";
        public static string WARNING_TITLE = "Cảnh báo";
        public static string CAT_VANG = "Bạn có chắc chắn cắt vàng của sản phẩm này không?";
        public static string THANH_TOAN_BAN_VANG = "Xác nhận bán các mặt hàng này và in hóa đơn?";
        public static string THANH_TOAN_DOI_VANG = "Xác nhận trao đổi các mặt hàng này và in hóa đơn?";
        public static string THANH_TOAN_MUA_VANG = "Xác nhận mua các mặt hàng này và in hóa đơn?";
        public static string INSERT_HOP_DONG_CAM_DO = "Xác nhận thêm mới và in hợp đồng cầm đồ?";
        public static string GIA_HAN_HOP_DONG_CAM_DO = "Xác nhận gia hạn và in lại hợp đồng cầm đồ?";
        public static string CHANGE_NGAY_DAU_KI = "Dữ liệu đầu kì thay đổi sẽ ảnh hưởng đến dữ liệu thống kê, xác nhận thay đổi?";
        public static string NGAY_DAU_KI_NULL = "Bạn chưa nhập ngày đầu kì đầu tiên!";
        public static string DONG_LAI_CAM_DO = "Hợp đồng cầm đồ này phải được thanh toán hết tiền lãi trước khi thanh toán hợp đồng!";
        public static string XUAT_KHO_EXCEL = "Xác nhận xuất tất cả sản phẩm trong kho ra file excel?";
        public static string XUAT_KHO_EXCEL_MA_SAN_PHAM = "Lưu ý: Không được thay đổi dữ liệu trong cột 'Mã SP'";
        public static string DUPLICATE_HAM_LUONG_VANG = "Hàm lượng vàng này đã tồn tại!";
        public static string CHUA_CHON_PHIEU_IN = "Bạn chưa chọn phiếu để in!";
    }

    public class MessageSucces
    {
        public static string NHAP_KHO_MOI = "Nhập kho mới thành công!";
        public static string NHAP_KHO_CU = "Nhập kho cũ thành công!";
        public static string DELETE_DATA_ROW = "Xóa dữ liệu thành công!";
        public static string UPDATE_DATA_ROW = "Cập nhật dữ liệu thành công!";
        public static string INSERT_DATA_ROW = "Thêm dữ liệu thành công!";
        public static string CHANGE_DATA_ROW = "Chuyển kho thành công!";
        public static string CAT_VANG = "Ok! Nhấn nút Làm mới dữ liệu để cập nhập";
        public static string GIAO_DICH_SUCCES = "Giao dịch thành công!";
        public static string THANH_TOAN_CAM_DO = "Thanh toán hợp đồng cầm đồ thành công!";
        public static string THANH_LY_CAM_DO = "Thanh lý hợp đồng thành công, sản phẩm đã được chuyển qua kho cũ!";
        public static string TINH_TOAN_THONG_KE = "Hoàn tất tính toán dữ liệu thống kê!";
    }

    public class MessageError
    {
        public static string DATA_OF_FORM_NULL = "Lỗi khi lấy dữ liệu từ form!";
        public static string SAN_PHAM_NULL = "Lỗi sản phẩm không nhập đủ dữ liệu cần thiết!";
        public static string NHAP_KHO = "Có lỗi xảy ra khi tạo mã nhập kho";
        public static string NHAP_KHO_MOI = "Nhập kho mới bị lỗi!";
        public static string NHAP_KHO_CU = "Nhập kho cũ bị lỗi!";
        public static string DATA_EMPTY = "Không được để trống dữ liệu";
        public static string DATA_ROW_INVALID = "Dữ liệu của bảng ghi này bị lỗi!";
        public static string UPDATE_ERROR = "Cập nhật dữ liệu thất bại!";
        public static string DELETE_ERROR = "Xóa dữ liệu thất bại!";
        public static string CHANGE_ERROR = "Chuyển kho thất bại!";
        public static string INSERT_ERROR = "Thêm mới dữ liệu thất bại!";
        public static string UPDATE_SP_ERROR = "Bạn phải chọn sản phẩm để sửa!";
        public static string DELETE_SP_ERROR = "Bạn phải chọn sản phẩm để xóa!";
        public static string CAT_VANG_INSERT = "Bị lỗi khi cắt vàng vào kho cũ!";
        public static string CAT_VANG_UPDATE = "Bị lỗi khi cập nhật vàng trong kho mới!";
        public static string SO_LUONG_ERROR = "Số lương phải lớn hơn 0!";
        public static string SO_LUONG_MA_TEM_ERROR = "Số lượng lớn hơn 1 thì không được nhập mã tem";
        public static string DU_LIEU_DAU_KI_ERROR = "Đã xảy ra lỗi trong quá trình tạo dữ liệu đầu kì!";
        public static string TINH_TOAN_THONG_KE = "Đã xảy ra lỗi trong quá trình tính toán dữ liệu thống kê!";
        public static string GIA_VANG_LOI = "Xảy ra lỗi khi tính toán giá bán!";
        public static string UPDATE_RECORD_NOT_SELECTED = "Bạn chưa chọn bản ghi để chỉnh sửa!";
        public static string TOTAL_MASS_INVALID = "Tổng khối lượng không trùng khớp!";
    }
}
