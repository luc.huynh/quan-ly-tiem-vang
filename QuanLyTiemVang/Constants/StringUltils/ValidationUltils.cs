﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Constants.StringUltils
{
    public class ValidationUltils
    {
        public static string TEN_SAN_PHAM = "Vui lòng nhập tên sản phẩm";
        public static string GIA_MUA = "Vui lòng nhập giá mua";
        public static string MA_TEM_KHO_MOI = "Mã tem không được để trống";
        public static string KI_HIEU = "Vui lòng nhập kí hiệu";
        public static string NHA_CUNG_CAP = "Vui lòng chọn nhà cung cấp";
        public static string TIEN_CONG = "Vui lòng nhập tiền công";
        public static string HAM_LUONG_VANG = "Vui lòng chọn hàm lượng vàng";
        public static string NGAY_NHAP_KHO = "Vui lòng nhập ngày nhập kho";
        public static string TI_LE_HOT_NAN = "Vui lòng chỉ nhập số hoặc dùng dấu chấm để nhập số thập phân";
        public static string SO_LUONG_LON_HON_50 = "Bạn chỉ được nhập số lượng ít hơn 50";
        public static string SO_LUONG_LON_HON_1_KHI_CO_MA_TEM = "Số lượng sản phẩm có cùng mã tem không được lớn hơn 1";
        public static string NHAP_TEM_BAN_HANG = "Nhập tem của sản phẩm hoặc nhấn 2 lần vào sản phẩm cần chọn!";
        public static string NHAP_KHOI_LUONG_CAT_VANG = "Nhập khối lượng cần để cắt vàng!";
        public static string KHOI_LUONG_CAT_KHONG_DU = "Khối lượng cắt không được lớn hơn khối lượng của sản phẩm!";
        public static string KHONG_TIM_THAY_MA_SAN_PHAM = "Mã sản phẩm này không tồn tại";
        public static string TRUNG_MA_TEM = "Mã tem này đã tồn tại!";
        public static string MA_TEM_SAI_CU_PHAP = "Mã tem bị sai cú pháp!";
        public static string MA_TEM_INVALID_LENGHT = "Mã tem phải có độ dài từ 5 kí tự!";
        public static string MA_TEM_INVALID_CHAR_0 = "Mã tem phải có kí tự đầu trùng với chữ cái đầu của tên sản phẩm!";
        public static string MA_TEM_EMPTY = "Mã tem không được để trống khi in tem!";
        public static string TEN_LOAI_TIEN = "Vui lòng nhập tên loại tiền";
        public static string KI_HIEU_CHUYEN_KHO_SAI_FORMAT = "Kí hiệu chuyển kho là * (nếu có)!";
        public static string DANH_SACH_VANG_DOI_VANG = "Danh sách bán và mua vào đều phải có ít nhất một sản phẩm!";
        public static string VALID_DANH_SACH_DOI_NGANG_RA_VAO = "Danh sách vàng đổi ngang vào và ra bị lỗi!";
        public static string LOAI_VANG_MUA_VAO = "Bạn phải chọn loại vàng mua vào!";
        public static string LOAI_VANG_BAN_RA = "Bạn phải chọn loại vàng bán ra!";
        public static string INPUT_NULL = "Dữ liệu này là bắt buộc!";
        public static string DANH_SACH_SAN_PHAM_CAM_DO = "Danh sách sản phẩm phải có ít nhất một sản phẩm!";
        public static string KHONG_TRUNG_HAM_LUONG_VANG = "Danh sách vàng đổi ra phải có cùng một hàm lượng vàng!";
        public static string VANG_BAN_RA_TRUNG_HAM_LUONG_VANG_DOI_RA = "Vàng bán ra phải có hàm lượng vàng khác với hàm lượng vàng đổi ngang";
        public static string VANG_DOI_RA_TRUNG_HLV_BAN_RA = "Vàng đổi ngang ra phải khác với hàm lượng vàng trong danh sách bán ra";
        public static string NOT_EXIST_SAN_PHAM_DOI_RA = "Danh sách sản phẩm đổi ra phải có ít nhất một sản phẩm!";
    }
}
