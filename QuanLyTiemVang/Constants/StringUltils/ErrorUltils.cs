﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Constants.StringUltils
{
    class ErrorUltils
    {
        
        public static string SOMETHING_ERROR = "Đã có lỗi xảy ra";
        public static string TEM_NULL = "Chưa có mã tem";
        public static string PASS_NULL = "Chưa nhập mật khẩu";
        public static string PASS_EQUAL = "Mật khẩu không giống nhau";
        public static string USER_NULL = "Cần nhập tài khoản";
        public static string USER_DULICATE = "Tài khoản đã tồn tại trong hệ thống";
        public static string NOTHING_CHOOSE = "Bạn chưa chọn gì cả";
        public static string PRICE_NULL = "Lỗi giá bán";
        public static string SUCCESS = "Thành công";
        public static string ENOUGH_DATA = "Chưa đủ dữ liệu";
        public static string CANNOT_CONVERT = "Sai kiểu dữ liệu";
        public static string LAI_SUAT_FAIL = "Lãi suất phải nằm trong khoảng từ 0 - 100";
        public static string NAME_ERROR = "Chưa có tên";
        public static string HOA_DON_INVALID = "Hóa đơn không có mặt hàng nào!";
    }
}
