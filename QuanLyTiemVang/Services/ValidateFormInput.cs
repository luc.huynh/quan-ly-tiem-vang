﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class ValidateFormInput
    {
        private static ValidateFormInput instance;

        public static ValidateFormInput Instance
        {
            get
            {
                if (instance == null)
                    instance = new ValidateFormInput();
                return instance;
            }
        }

        public bool IsNumber(string stringInput)
        {
            try
            {
                double.Parse(stringInput);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
