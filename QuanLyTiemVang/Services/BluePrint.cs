﻿using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class BluePrint
    {
        private static BluePrint instance;

        public static BluePrint Instance
        {
            get
            {
                if (instance == null)
                    instance = new BluePrint();
                return instance;
            }
        }

        public SanPhamInTem ConvertToSanPhamInTem(SanPham sanPham)
        {
            return new SanPhamInTem
            {
                TenSanPham = sanPham.TenSanPham,
                HamLuongVang = sanPham.HamLuongVang,
                KhoiLuongDa = Calculator.Instance.ConvertKhoiLuongToFormatInTem(sanPham.KhoiLuongDa),
                KhoiLuongVang = Calculator.Instance.ConvertKhoiLuongToFormatInTem(sanPham.KhoiLuongVang),
                KhoiLuongTinh = Calculator.Instance.ConvertKhoiLuongToFormatInTem(sanPham.KhoiLuongTinh),
                TienCong = Calculator.Instance.ConvertTienCongToFormatInTem(sanPham.TienCong),
                KiHieu = sanPham.KiHieu,
                MaTem = sanPham.MaTem,
                KiHieuChuyenKho = sanPham.IsSanPhamChuyenKho ? "*" : "",
                NhaCungCap = sanPham.NhaCungCap.TenNhaCungCap
            };
        }
    }
}
