﻿using BUS;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.Services
{
    public class AutoCompleteText
    {
        private static AutoCompleteText instance;

        public static AutoCompleteText Instance
        {
            get
            {
                if (instance == null)
                    instance = new AutoCompleteText();
                return instance;
            }
        }

        public void Config(TextEdit textEdit)
        {
            textEdit.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textEdit.MaskBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
        }

        public void SetData(TextEdit textEdit, AutoCompleteStringCollection collection)
        {
            textEdit.MaskBox.AutoCompleteCustomSource = collection;
        }

        public void LoadAutoCompleteTenSanPham(TextEdit txtTenSanPham)
        {
            AutoCompleteStringCollection collectionTenSanPham = new AutoCompleteStringCollection();
            var listSanPham = SanPhamBUS.Instance.GetListSanPhamChuaBan().GroupBy(x => x.TenSanPham);
            var listSanPhamKhoCu = SanPhamKhoCuBUS.Instance.GetListSanPhamKhoCu().GroupBy(x => x.TenSanPham);

            foreach (var sanPham in listSanPham)
            {
                collectionTenSanPham.Add(sanPham.Key);
            }

            foreach (var sanPham in listSanPhamKhoCu)
            {
                collectionTenSanPham.Add(sanPham.Key);
            }

            Config(txtTenSanPham);
            SetData(txtTenSanPham, collectionTenSanPham);
        }
    }

}
