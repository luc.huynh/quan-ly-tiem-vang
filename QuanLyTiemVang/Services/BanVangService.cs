﻿using BUS;
using DevExpress.XtraEditors;
using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class BanVangService
    {
        private static BanVangService instance;

        public static BanVangService Instance
        {
            get
            {
                if (instance == null)
                    instance = new BanVangService();
                return instance;
            }
        }

        public List<HoaDonBanVang> GetListHoaDonBanVang(
            List<SanPham> listSanPhamBanRa,
            LookUpEdit lueKhachHang,
            TextEdit txtTienBot,
            string maLichSuBan,
            DateTime ngayBan)
        {
            List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();
            List<HoaDonBanVang> listHoaDonBanVang = new List<HoaDonBanVang>();
            KhachHang khachHang = (KhachHang)lueKhachHang.GetSelectedDataRow();
            string hoLotKhachHang = "";
            string tenKhachHang = "";
            string diaChiKhachHang = "";
            int tongTien = 0;
            int tienBot = 0;

            if (!string.IsNullOrWhiteSpace(khachHang.HoLot))
            {
                hoLotKhachHang = khachHang.HoLot.Trim();
            }

            if (!string.IsNullOrWhiteSpace(khachHang.Ten))
            {
                tenKhachHang = khachHang.Ten.Trim();
            }

            if (!string.IsNullOrWhiteSpace(khachHang.DiaChi))
            {
                diaChiKhachHang = khachHang.DiaChi.Trim();
            }

            if (txtTienBot.EditValue != null)
            {
                tienBot = (int)txtTienBot.EditValue;
            }

            foreach (var sanPham in listSanPhamBanRa)
            {
                tongTien += sanPham.GiaBan;
            }

            foreach (var sanPham in listSanPhamBanRa)
            {
                HoaDonBanVang hoaDon = new HoaDonBanVang
                {
                    TenKhachHang = hoLotKhachHang + " " + tenKhachHang,
                    DiaChi = diaChiKhachHang,
                    MaTem = sanPham.MaTem,
                    TenSanPham = sanPham.TenSanPham,
                    HamLuongVang = sanPham.HamLuongVang,
                    KhoiLuongTinh = sanPham.KhoiLuongTinh.ToString(),
                    KhoiLuongDa = sanPham.KhoiLuongDa.ToString(),
                    KhoiLuongVang = sanPham.KhoiLuongVang.ToString(),
                    GiaBan = Calculator.Instance.ConvertTienToFormatInHoaDon(
                        listGiaVangHienTai.First(x => x.KiHieuHamLuongVang.HamLuongVang == sanPham.HamLuongVang).GiaVangBanRa),
                    TienCong = Calculator.Instance.ConvertTienToFormatInHoaDon(sanPham.TienCong),
                    ThanhTien = Calculator.Instance.ConvertTienToFormatInHoaDon(sanPham.GiaBan),
                    TongTien = Calculator.Instance.ConvertTienToFormatInHoaDon(tongTien),
                    TienBot = Calculator.Instance.ConvertTienToFormatInHoaDon(tienBot),
                    ThanhTienHoaDon = Calculator.Instance.ConvertTienToFormatInHoaDon(tongTien - tienBot),
                    LoaiGiaoDich = "Bán vàng",
                    MaHoaDon = maLichSuBan,
                    ThoiGianGiaoDich = ngayBan,
                    ThuTienKhachHangBangChu = TienSoThanhChuService.Instance.NumberToTextVN(tongTien - tienBot)
                };

                listHoaDonBanVang.Add(hoaDon);
            }
            return listHoaDonBanVang;
        }

        public LichSuBan TinhToanLichSuBan(List<SanPham> listSanPhamBanRa, TextEdit txtTienBot, LookUpEdit lueKhachHang)
        {
            int tienBot = 0;
            int tongTienVang = 0;

            if (txtTienBot.EditValue != null)
            {
                tienBot = (int)txtTienBot.EditValue;
            }

            foreach (SanPham sanPham in listSanPhamBanRa)
            {
                tongTienVang += sanPham.GiaBan;
            }

            return new LichSuBan
            {
                MaNhanVien = Form1.nhanVien.MaNhanVien,
                MaKhachHang = (int)lueKhachHang.EditValue,
                NgayGiaoDich = DateTime.Now,
                TienBot = tienBot,
                TongTienVang = tongTienVang,
                ThanhTienHoaDon = tongTienVang - tienBot
            };
        }
    }
}
