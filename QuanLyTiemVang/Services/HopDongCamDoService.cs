﻿using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class HopDongCamDoService
    {
        private static HopDongCamDoService instance;

        public static HopDongCamDoService Instance
        {
            get
            {
                if (instance == null)
                    instance = new HopDongCamDoService();
                return instance;
            }
        }

        public List<HopDongCamDoTemp> GetListHopDongCamDoPrint(HopDongCamDo hopDong)
        {
            List<HopDongCamDoTemp> listHopDongTemp = new List<HopDongCamDoTemp>();
            string camVang = "";
            string tenMonHang = "";
            string canNang = "";

            foreach (SanPhamCamDo sanPham in hopDong.SanPhamCamDoes)
            {
                camVang += sanPham.KiHieuHamLuongVang.HamLuongVang + ", ";
                tenMonHang += sanPham.TenMonHang + " (" + sanPham.KiHieuHamLuongVang.HamLuongVang + "), ";
                canNang += sanPham.TenMonHang + " (" + sanPham.KiHieuHamLuongVang.HamLuongVang + ") " + Calculator.Instance.ConvertKhoiLuongToFormatInTem(sanPham.CanNang) + ", ";
            }

            HopDongCamDoTemp hopDongTemp = new HopDongCamDoTemp
            {
                MaHopDongCamDo = hopDong.MaHopDongCamDo,
                TenKhachHang = hopDong.HoTenKhachHang,
                DiaChi = hopDong.DiaChiKhachHang,
                SoDienThoai = hopDong.SoDienThoaiKhachHang,
                CamVang = camVang.TrimEnd(',', ' '),
                TenMonHang = tenMonHang.TrimEnd(',', ' '),
                CanNangTiem = canNang,
                CanNangKhach = canNang.TrimEnd(',', ' ').Replace("c", " chỉ ").Replace("p", " phân ").Replace("l", " ly"),
                SoTienCam = hopDong.SoTienCam,
                NgayBatDau = hopDong.NgayBatDau,
                SoNamHienTai = hopDong.NgayBatDau.Year % 10,
                SoTienCamBangChu = TienSoThanhChuService.Instance.NumberToTextVN(hopDong.SoTienCam)
            };

            listHopDongTemp.Add(hopDongTemp);

            return listHopDongTemp;
        }
    }
}