﻿using BUS;
using DevExpress.XtraEditors;
using DTO.Models;
using DTO.TemporaryModel;
using QuanLyTiemVang.Constants;
using Share.Constant;
using Share.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class VangDoiVangService
    {
        private static VangDoiVangService instance;

        public static VangDoiVangService Instance
        {
            get
            {
                if (instance == null)
                    instance = new VangDoiVangService();
                return instance;
            }
        }

        public LichSuDoiVang TinhToanVangDoiVang(
            List<SanPham> listVangBanRa,
            List<SanPham> listVangDoiNgangRa,
            List<SanPhamKhoCu> listSanPhamMuaVao,
            TextEdit txtTienCongThem,
            TextEdit txtTienBot,
            bool tienCongEdited = false)
        {
            List<LichSuGiaVang> listGiaVangMoiNhat = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();
            double khoiLuongVangDoiNgangMuaVao = 0;
            double tongKhoiLuongVangDoiNgangRa = 0;
            double tongKhoiLuongVangDoiNgangRaKhongCoMaChuyenKho = 0;
            double tongKhoiLuongVangDoiNgangRaCoMaChuyenKho = 0;
            int giaBanTemp = 0;
            int giaMuaTemp = 0;
            int giaBanMua = 0;
            int giaMuaVangCu = 0;
            double tienMuaVangCu = 0;
            int tienCongThem = 0;

            // lấy giá bán
            if (listVangDoiNgangRa.Count > 0)
            {
                foreach (LichSuGiaVang giaVangHienTai in listGiaVangMoiNhat)
                {
                    if (giaVangHienTai.KiHieuHamLuongVang.HamLuongVang.Equals(listVangDoiNgangRa[0].HamLuongVang))
                    {
                        giaBanTemp = giaVangHienTai.GiaVangBanRa;
                        giaMuaTemp = giaVangHienTai.GiaVangMuaVao;
                    }
                }
            }

            // lấy tổng khối lượng vàng đổi ngang ra và tổng tiền công
            tongKhoiLuongVangDoiNgangRa = listVangDoiNgangRa.Sum(x => x.KhoiLuongVang);
            tongKhoiLuongVangDoiNgangRaCoMaChuyenKho = listVangDoiNgangRa.Where(x => x.IsSanPhamChuyenKho).Sum(x => x.KhoiLuongVang);
            tongKhoiLuongVangDoiNgangRaKhongCoMaChuyenKho = listVangDoiNgangRa.Where(x => !x.IsSanPhamChuyenKho).Sum(x => x.KhoiLuongVang);
            tienCongThem += listVangDoiNgangRa.Sum(x => x.TienCong) + listVangBanRa.Sum(x => x.TienCong);

            if (tienCongEdited && txtTienCongThem.EditValue != null)
            {
                tienCongThem = (int)txtTienCongThem.EditValue;
            }

            // lấy tổng khối lượng vàng đổi ngang mua vào, giá mua vàng cũ vào tính theo chỉ, tổng tiền mua vàng cũ
            foreach (SanPhamKhoCu sanPham in listSanPhamMuaVao)
            {
                if (sanPham.LoaiVangMuaVao == Constant.VANG_CU_DOI_NGANG)
                {
                    khoiLuongVangDoiNgangMuaVao += sanPham.KhoiLuongVang;
                }
                else
                {
                    foreach (LichSuGiaVang giaVangHienTai in listGiaVangMoiNhat)
                    {
                        if (giaVangHienTai.MaHamLuongVang == sanPham.MaHamLuongVang)
                        {
                            giaMuaVangCu = giaVangHienTai.GiaVangMuaVao;
                            tienMuaVangCu += Math.Round(sanPham.KhoiLuongVang * (giaMuaVangCu / 100.0));
                        }
                    }
                }
            }

            double klVangMoiTruKLVangCu = tongKhoiLuongVangDoiNgangRa - khoiLuongVangDoiNgangMuaVao;

            if (klVangMoiTruKLVangCu >= 0)
            {
                giaBanMua = giaBanTemp;
            }
            else
            {
                giaBanMua = giaMuaTemp;
            }

            int thanhTienVangMoi = (int)(Math.Round(klVangMoiTruKLVangCu * (giaBanMua / 100.0)));
            int conLai = thanhTienVangMoi - (int)Math.Round(tienMuaVangCu);
            int tienBot = 0;

            if (txtTienBot.EditValue != null)
            {
                tienBot = (int)txtTienBot.EditValue;
            }

            int tongTienVangBanRa = listVangBanRa.Sum(x => x.GiaBan - x.TienCong);
            int thuTienKhachHang = conLai + tienCongThem - tienBot + tongTienVangBanRa;

            return new LichSuDoiVang
            {
                KhoiLuongVangMoi = Math.Round(tongKhoiLuongVangDoiNgangRa, 1),
                KhoiLuongVangCu = Math.Round(khoiLuongVangDoiNgangMuaVao, 1),
                KLVMoiTruKLVCu = Math.Round(klVangMoiTruKLVangCu, 1),
                GiaBan = giaBanMua,
                ThanhTienVangMoi = TienUtil.LamTronTienViet(thanhTienVangMoi),
                TongTienVangCu = TienUtil.LamTronTienViet((int)Math.Round(tienMuaVangCu)),
                TienConLai = TienUtil.LamTronTienViet(conLai),
                TienCongThem = TienUtil.LamTronTienViet(tienCongThem),
                TienBot = TienUtil.LamTronTienViet(tienBot),
                ThuTienKhachHang = TienUtil.LamTronTienViet(thuTienKhachHang),
                NgayGiaoDich = DateTime.Now,
                KhoiLuongVangMoiCoMaChuyenKho = Math.Round(tongKhoiLuongVangDoiNgangRaCoMaChuyenKho, 1),
                KhoiLuongVangMoiKhongCoMaChuyenKho = Math.Round(tongKhoiLuongVangDoiNgangRaKhongCoMaChuyenKho, 1),
                TongTienVangBanRa = tongTienVangBanRa
            };
        }

        public List<HoaDonVangDoiVang> ConvertToListHoaDonBanVang(
            List<SanPham> listSanPhamBanRa,
            List<SanPhamKhoCu> listSanPhamMuaVao,
            LichSuDoiVang lichSuDoiVang,
            LookUpEdit lueKhachHang)
        {
            KhachHang khachHang = (KhachHang)lueKhachHang.GetSelectedDataRow();
            string hoLotKhachHang = "";
            string tenKhachHang = "";
            string diaChiKhachHang = "";
            string tieuDeThuTienKhachHang = Constant.TIEU_DE_THU_TIEN_KHACH_HANG;

            if (lichSuDoiVang.ThuTienKhachHang < 0)
            {
                tieuDeThuTienKhachHang = Constant.TIEU_DE_TRA_TIEN_KHACH_HANG;
            }

            if (!string.IsNullOrWhiteSpace(khachHang.HoLot))
            {
                hoLotKhachHang = khachHang.HoLot.Trim();
            }

            if (!string.IsNullOrWhiteSpace(khachHang.Ten))
            {
                tenKhachHang = khachHang.Ten.Trim();
            }

            if (!string.IsNullOrWhiteSpace(khachHang.DiaChi))
            {
                diaChiKhachHang = khachHang.DiaChi.Trim();
            }

            List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();
            List<HoaDonVangDoiVang> listHoaDonBanVang = new List<HoaDonVangDoiVang>();

            foreach (SanPham sanPham in listSanPhamBanRa)
            {
                HoaDonVangDoiVang hoaDonVangMoi = new HoaDonVangDoiVang
                {
                    TenKhachHang = hoLotKhachHang + " " + tenKhachHang,
                    DiaChi = diaChiKhachHang,
                    MaTem = sanPham.MaTem,
                    TenSanPham = sanPham.TenSanPham,
                    HamLuongVang = sanPham.HamLuongVang,
                    KhoiLuongTinh = sanPham.KhoiLuongTinh,
                    KhoiLuongDa = sanPham.KhoiLuongDa,
                    KhoiLuongVang = sanPham.KhoiLuongVang,
                    GiaMua = 0,
                    GiaBan = listGiaVangHienTai.First(x => x.KiHieuHamLuongVang.HamLuongVang == sanPham.HamLuongVang).GiaVangBanRa,
                    TienCong = sanPham.TienCong,
                    ThanhTien = sanPham.GiaBan,
                    KLVMoiTruKLVCu = lichSuDoiVang.KhoiLuongVangMoi.ToString() +
                        " - " + lichSuDoiVang.KhoiLuongVangCu.ToString() +
                        " = " + lichSuDoiVang.KLVMoiTruKLVCu.ToString(),
                    GiaBanVaMua = lichSuDoiVang.GiaBan,
                    ThanhTienHoaDon = lichSuDoiVang.ThanhTienVangMoi,
                    TongTienVangCu = lichSuDoiVang.TongTienVangCu,
                    TongTienConLai = lichSuDoiVang.TienConLai,
                    TienCongThem = lichSuDoiVang.TienCongThem,
                    TienBot = lichSuDoiVang.TienBot,
                    ThuTienKhachHang = Math.Abs(lichSuDoiVang.ThuTienKhachHang),
                    LoaiGiaoDich = "Mua - bán",
                    MaHoaDon = lichSuDoiVang.MaLichSuBan,
                    TieuDeThuTienKhachHang = tieuDeThuTienKhachHang,
                    ThoiGianGiaoDich = lichSuDoiVang.NgayGiaoDich != null ? lichSuDoiVang.NgayGiaoDich : DateTime.Now,
                    ThuTienKhachHangBangChu = TienSoThanhChuService.Instance.NumberToTextVN(Math.Abs(lichSuDoiVang.ThuTienKhachHang)),
                    TongTienVangBanRa = lichSuDoiVang.TongTienVangBanRa
                };

                listHoaDonBanVang.Add(hoaDonVangMoi);
            }

            foreach (SanPhamKhoCu sanPham in listSanPhamMuaVao)
            {
                if (sanPham.LoaiVangMuaVao == Constant.VANG_CU_MUA_VAO)
                {
                    HoaDonVangDoiVang hoaDonVangCu = new HoaDonVangDoiVang
                    {
                        TenKhachHang = hoLotKhachHang + " " + tenKhachHang,
                        DiaChi = diaChiKhachHang,
                        MaTem = "",
                        TenSanPham = sanPham.TenSanPham,
                        HamLuongVang = listGiaVangHienTai.First(x => x.MaHamLuongVang == sanPham.MaHamLuongVang).KiHieuHamLuongVang.HamLuongVang,
                        KhoiLuongTinh = sanPham.KhoiLuongTinh,
                        KhoiLuongDa = sanPham.KhoiLuongDa,
                        KhoiLuongVang = sanPham.KhoiLuongVang,
                        GiaMua = listGiaVangHienTai.First(x => x.MaHamLuongVang == sanPham.MaHamLuongVang).GiaVangMuaVao,
                        GiaBan = 0,
                        TienCong = 0,
                        ThanhTien = sanPham.GiaMuaVao,
                        KLVMoiTruKLVCu = lichSuDoiVang.KhoiLuongVangMoi.ToString() +
                            " - " + lichSuDoiVang.KhoiLuongVangCu.ToString() +
                            " = " + lichSuDoiVang.KLVMoiTruKLVCu.ToString(),
                        GiaBanVaMua = lichSuDoiVang.GiaBan,
                        ThanhTienHoaDon = lichSuDoiVang.ThanhTienVangMoi,
                        TongTienVangCu = lichSuDoiVang.TongTienVangCu,
                        TongTienConLai = lichSuDoiVang.TienConLai,
                        TienCongThem = lichSuDoiVang.TienCongThem,
                        TienBot = lichSuDoiVang.TienBot,
                        ThuTienKhachHang = Math.Abs(lichSuDoiVang.ThuTienKhachHang),
                        LoaiGiaoDich = "Mua - bán",
                        MaHoaDon = lichSuDoiVang.MaLichSuBan,
                        TieuDeThuTienKhachHang = tieuDeThuTienKhachHang,
                        ThoiGianGiaoDich = lichSuDoiVang.NgayGiaoDich != null ? lichSuDoiVang.NgayGiaoDich : DateTime.Now,
                        ThuTienKhachHangBangChu = TienSoThanhChuService.Instance.NumberToTextVN(Math.Abs(lichSuDoiVang.ThuTienKhachHang)),
                        TongTienVangBanRa = lichSuDoiVang.TongTienVangBanRa
                    };

                    listHoaDonBanVang.Add(hoaDonVangCu);
                }
            }

            return listHoaDonBanVang;
        }
    }
}
