﻿using BUS;
using DevExpress.XtraEditors;
using DTO.Models;
using DTO.TemporaryModel;
using QuanLyTiemVang.Constants;
using Share.Constant;
using Share.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class Calculator
    {
        private static Calculator instance;

        public static Calculator Instance
        {
            get
            {
                if (instance == null)
                    instance = new Calculator();
                return instance;
            }
        }

        public double ConvertKhoiLuongTuFormToDouble(TextEdit txtChi, TextEdit txtPhan, TextEdit txtLy)
        {
            double chi = txtChi.EditValue == null ? 0 : double.Parse(txtChi.EditValue.ToString());
            double phan = txtPhan.EditValue == null ? 0 : double.Parse(txtPhan.EditValue.ToString());
            double ly = txtLy.EditValue == null ? 0 : double.Parse(txtLy.EditValue.ToString());
            return chi * 100 + phan * 10 + ly;
        }

        public string TinhSoLyToString(double khoiLuong)
        {
            double soDuThapPhan = khoiLuong - (int)khoiLuong;
            int doDaiThapPhan = khoiLuong.ToString().Length - ((int)khoiLuong).ToString().Length;
            switch (doDaiThapPhan)
            {
                case 1:
                    soDuThapPhan = Math.Round(soDuThapPhan, 0);
                    break;
                case 2:
                    soDuThapPhan = Math.Round(soDuThapPhan, 1);
                    break;
                case 3:
                    soDuThapPhan = Math.Round(soDuThapPhan, 2);
                    break;
                default:
                    soDuThapPhan = Math.Round(soDuThapPhan, 3);
                    break;
            }
            return (((int)khoiLuong % 10) + soDuThapPhan).ToString();
        }

        public string TinhSoPhanToString(double khoiLuong)
        {
            return (((int)khoiLuong / 10) % 10).ToString();
        }

        public string TinhSoChiToString(double khoiLuong)
        {
            return (((int)khoiLuong / 100)).ToString();
        }

        public void HienThiKhoiLuongTheoLyLenForm(
            double? khoiLuongTheoLy,
            TextEdit txtChi,
            TextEdit txtPhan,
            TextEdit txtLy)
        {
            double khoiLuong = khoiLuongTheoLy.HasValue ? khoiLuongTheoLy.Value : 0;

            txtLy.Text = TinhSoLyToString(khoiLuong);
            txtPhan.Text = TinhSoPhanToString(khoiLuong);
            txtChi.Text = TinhSoChiToString(khoiLuong);
        }

        public string ConvertKhoiLuongToFormatInTem(double? khoiLuongKho)
        {
            if (khoiLuongKho == null)
            {
                khoiLuongKho = 0;
            }

            double khoiLuongKhoDouble = (khoiLuongKho.HasValue) ? khoiLuongKho.Value : 0;
            int khoiLuong = (int)double.Parse(khoiLuongKho.ToString());
            string ly = Math.Round((khoiLuongKhoDouble % 10), 1).ToString();
            string phan = ((khoiLuong / 10) % 10).ToString();
            string chi = (khoiLuong / 100).ToString();
            return chi + "c" + phan + "p" + ly + "l";
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public string ConvertTienCongToFormatInTem(double? tienCong)
        {
            bool isNegative = false;

            if (tienCong == null)
            {
                tienCong = 0;
            }

            if (tienCong < 0)
            {
                isNegative = true;
            }

            string strTienCong = Math.Abs((double)tienCong).ToString();
            int lenghtTienCong = strTienCong.Length;
            string tienCongInTem = "";
            int indexTemp = 0;

            for (int i = lenghtTienCong - 1; i >= 0; i--)
            {
                indexTemp++;
                tienCongInTem += strTienCong[i];
                if (indexTemp == 3 && i != 0)
                {
                    tienCongInTem += ",";
                    indexTemp = 0;
                }
            }

            if (isNegative)
            {
                return "-" + Reverse(tienCongInTem) + " VNĐ";
            }

            return Reverse(tienCongInTem) + " VNĐ";
        }

        public string ConvertTienCongToFormatInTem(int? tienCong)
        {
            bool isNegative = false;

            if (tienCong == null)
            {
                tienCong = 0;
            }

            if (tienCong < 0)
            {
                isNegative = true;
            }

            string strTienCong = Math.Abs((int)tienCong).ToString();
            int lenghtTienCong = strTienCong.Length;
            string tienCongInTem = "";
            int indexTemp = 0;

            for (int i = lenghtTienCong - 1; i >= 0; i--)
            {
                indexTemp++;
                tienCongInTem += strTienCong[i];
                if (indexTemp == 3 && i != 0)
                {
                    tienCongInTem += ",";
                    indexTemp = 0;
                }
            }

            if (isNegative)
            {
                return "-" + Reverse(tienCongInTem) + " VNĐ";
            }

            return Reverse(tienCongInTem) + " VNĐ";
        }

        public string ConvertTienToFormatInHoaDon(string tien)
        {
            double outValue;
            double tienConvert = double.TryParse(tien, out outValue) ? outValue : 0;
            string tienAfterConvert = ConvertTienCongToFormatInTem(tienConvert);

            return tienAfterConvert.Substring(0, tienAfterConvert.Length - 4);
        }

        public string ConvertTienToFormatInHoaDon(int? tien)
        {
            string tienAfterConvert = ConvertTienCongToFormatInTem(tien);

            return tienAfterConvert.Substring(0, tienAfterConvert.Length - 4);
        }

        public double TinhTongKhoiLuongTheoLy(TextEdit txtChi, TextEdit txtPhan, TextEdit txtLy)
        {
            double outValue;
            double chiDa = double.TryParse(txtChi.Text, out outValue) ? outValue : 0;
            double phanDa = double.TryParse(txtPhan.Text, out outValue) ? outValue : 0;
            double lyDa = double.TryParse(txtLy.Text, out outValue) ? outValue : 0;
            return chiDa * 100 + phanDa * 10 + lyDa;
        }

        public void HienThiKhoiLuongVang(
            double tongKhoiLuongTinh,
            double tongKhoiLuongDa,
            TextEdit txtChiVang, TextEdit txtPhanVang, TextEdit txtLyVang)
        {
            double khoiLuongVang = Math.Round(tongKhoiLuongTinh - tongKhoiLuongDa, 1);
            int khoiLuongVangInt = (int)khoiLuongVang;
            int soChi = khoiLuongVangInt / 100;
            txtChiVang.Text = soChi.ToString();
            
            int soPhan = (khoiLuongVangInt - soChi * 100) / 10;
            txtPhanVang.Text = soPhan.ToString();

            double soLy = khoiLuongVang % 10;
            txtLyVang.Text = Math.Round(soLy, 1).ToString();
        }

        public SanPham TinhGiaVangBanTheoKhoiLuongVang(SanPham sanPham)
        {
            List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();
            LichSuGiaVang giaVangHienTai = listGiaVangHienTai
                .Where(x => x.KiHieuHamLuongVang.HamLuongVang.Equals(sanPham.HamLuongVang)).FirstOrDefault();

            if (giaVangHienTai == null)
            {
                return null;
            }

            double? giaBanTemp = sanPham.KhoiLuongVang * (giaVangHienTai.GiaVangBanRa / 100.0) + sanPham.TienCong;
            double giaBan = giaBanTemp.HasValue ? giaBanTemp.Value : 0;

            sanPham.GiaBan = TienUtil.LamTronTienViet((int)Math.Round(giaBan));

            return sanPham;
        }

        public void HienThiGiaMuaTheoKLTinhVaKLDa(
            TextEdit txtGiaMua,
            LookUpEdit lueHamLuongVang,
            double khoiLuongTinh,
            double khoiLuongDa)
        {
            List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();

            string hamLuongVang = "";
            double khoiLuongVang = Math.Round(khoiLuongTinh - khoiLuongDa, 1);
            
            if (lueHamLuongVang.EditValue != null)
            {
                hamLuongVang = lueHamLuongVang.Text;
            }

            foreach (var giaVangTheoLoai in listGiaVangHienTai)
            {
                if (hamLuongVang.Equals(giaVangTheoLoai.KiHieuHamLuongVang.HamLuongVang))
                {
                    double? giaMua = khoiLuongVang * (giaVangTheoLoai.GiaVangMuaVao / 100.0);
                    txtGiaMua.EditValue = Math.Round(giaMua.HasValue ? giaMua.Value : 0);
                    return;
                }
            }
        }

        public void HienThiGiaMuaTheoKLTinhVaKLDa(
            TextEdit txtGiaMua,
            LookUpEdit lueHamLuongVang,
            double khoiLuongTinh,
            double khoiLuongDa,
            LookUpEdit lueLoaiVangMuaVao)
        {
            List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangBUS.Instance.GetAllLichSuGiaVang();

            string hamLuongVang = "";
            double khoiLuongVang = Math.Round(khoiLuongTinh - khoiLuongDa, 1);

            if (lueHamLuongVang.EditValue != null)
            {
                hamLuongVang = lueHamLuongVang.Text;
            }

            foreach (var giaVangTheoLoai in listGiaVangHienTai)
            {
                if (hamLuongVang.Equals(giaVangTheoLoai.KiHieuHamLuongVang.HamLuongVang))
                {
                    double? giaMua = 0;

                    if (lueLoaiVangMuaVao.EditValue != null && (int)lueLoaiVangMuaVao.EditValue == Constant.VANG_CU_DOI_NGANG)
                    {
                        giaMua = khoiLuongVang * (giaVangTheoLoai.GiaVangBanRa / 100.0);
                    }
                    else if (lueLoaiVangMuaVao.EditValue != null && (int)lueLoaiVangMuaVao.EditValue == Constant.VANG_CU_MUA_VAO)
                    {
                        giaMua = khoiLuongVang * (giaVangTheoLoai.GiaVangMuaVao / 100.0);
                    }
                    
                    txtGiaMua.EditValue = Math.Round(giaMua.HasValue ? giaMua.Value : 0);
                    return;
                }
            }
        }
    }
}
