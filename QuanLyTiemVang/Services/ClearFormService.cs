﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.Services
{
    public static class ClearFormService
    {
        public static void ClearAllForm(Control Ctrl, List<string> listNameNotNeedClear)
        {
            foreach (Control ctrl in Ctrl.Controls)
            {
                BaseEdit editor = ctrl as BaseEdit;
                if (editor != null && listNameNotNeedClear.IndexOf(editor.Name) == -1)
                    editor.EditValue = null;

                ClearAllForm(ctrl, listNameNotNeedClear);//Recursive
            }
        }
    }
}
