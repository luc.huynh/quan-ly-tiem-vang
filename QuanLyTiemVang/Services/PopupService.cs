﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.Services
{
    public class PopupService
    {
        private static PopupService instance;

        public static PopupService Instance
        {
            get
            {
                if (instance == null)
                    instance = new PopupService();
                return instance;
            }
        }

        public void Error(string message)
        {
            XtraMessageBox.Show(message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void Success(string message)
        {
            XtraMessageBox.Show(message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void Warning(string message)
        {
            XtraMessageBox.Show(message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public DialogResult Question(string message)
        {
            return XtraMessageBox.Show(message, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        internal void Success(object messageSuccess)
        {
            throw new NotImplementedException();
        }
    }
}
