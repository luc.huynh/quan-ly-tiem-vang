﻿using BUS;
using DevExpress.XtraEditors;
using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class MuaVangService
    {
        private static MuaVangService instance;

        public static MuaVangService Instance
        {
            get
            {
                if (instance == null)
                    instance = new MuaVangService();
                return instance;
            }
        }

        public List<HoaDonMuaVang> GetListHoaDonMuaVang(
            List<SanPhamKhoCu> listSanPhamMuaVao,
            LookUpEdit lueKhachHang,
            LichSuMua lichSuMua)
        {
            List<KiHieuHamLuongVang> listHamLuongVang = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang();
            List<HoaDonMuaVang> listHoaDonMuaVang = new List<HoaDonMuaVang>();
            KhachHang khachHang = (KhachHang)lueKhachHang.GetSelectedDataRow();
            string hoLotKhachHang = "";
            string tenKhachHang = "";
            string diaChiKhachHang = "";

            if (!string.IsNullOrWhiteSpace(khachHang.HoLot))
            {
                hoLotKhachHang = khachHang.HoLot.Trim();
            }

            if (!string.IsNullOrWhiteSpace(khachHang.Ten))
            {
                tenKhachHang = khachHang.Ten.Trim();
            }

            if (!string.IsNullOrWhiteSpace(khachHang.DiaChi))
            {
                diaChiKhachHang = khachHang.DiaChi.Trim();
            }

            foreach (var sanPham in listSanPhamMuaVao)
            {
                HoaDonMuaVang hoaDon = new HoaDonMuaVang
                {
                    TenKhachHang = hoLotKhachHang + " " + tenKhachHang,
                    DiaChi = diaChiKhachHang,
                    TenSanPham = sanPham.TenSanPham,
                    HamLuongVang = listHamLuongVang.FirstOrDefault(x => x.MaHamLuongVang == sanPham.MaHamLuongVang).HamLuongVang,
                    KhoiLuongVang = sanPham.KhoiLuongVang.ToString(),
                    TiLeHot =(Math.Round(sanPham.TiLeHot, 1)).ToString(),
                    DonGiaMua = Calculator.Instance.ConvertTienToFormatInHoaDon(sanPham.DonGiaMuaVao),
                    ThanhTien = Calculator.Instance.ConvertTienToFormatInHoaDon(sanPham.GiaMuaVao),
                    TongTien = Calculator.Instance.ConvertTienToFormatInHoaDon(lichSuMua.TongTienVang),
                    TienCongThem = Calculator.Instance.ConvertTienToFormatInHoaDon(lichSuMua.TienCongThem),
                    TongTienHoaDon = Calculator.Instance.ConvertTienToFormatInHoaDon(lichSuMua.ThanhTienHoaDon),
                    LoaiGiaoDich = "Mua vàng",
                    MaHoaDon = lichSuMua.MaLichSuMua,
                    NgayGiaoDich = lichSuMua.NgayGiaoDich != null ? lichSuMua.NgayGiaoDich : DateTime.Now,
                    TongTienHoaDonBangChu = TienSoThanhChuService.Instance.NumberToTextVN(lichSuMua.ThanhTienHoaDon)
                };

                listHoaDonMuaVang.Add(hoaDon);
            }
            return listHoaDonMuaVang;
        }

        public LichSuMua TinhToanLichSuMua(List<SanPhamKhoCu> listSanPhamMuaVao, TextEdit txtTienCongThem, LookUpEdit lueKhachHang)
        {
            int tienCongThem = 0;
            int tongTienVang = 0;

            if (txtTienCongThem.EditValue != null)
            {
                tienCongThem = (int)txtTienCongThem.EditValue;
            }

            foreach (SanPhamKhoCu sanPham in listSanPhamMuaVao)
            {
                tongTienVang += sanPham.GiaMuaVao;
            }

            return new LichSuMua
            {
                MaNhanVien = Form1.nhanVien.MaNhanVien,
                MaKhachHang = (int)lueKhachHang.EditValue,
                NgayGiaoDich = DateTime.Now,
                TienCongThem = tienCongThem,
                TongTienVang = tongTienVang,
                ThanhTienHoaDon = tongTienVang + tienCongThem
            };
        }
    }
}
