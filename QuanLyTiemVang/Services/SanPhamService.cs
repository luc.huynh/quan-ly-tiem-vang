﻿using BUS;
using DevExpress.XtraEditors;
using DTO.Models;
using QuanLyTiemVang.Constants;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTiemVang.Services
{
    public class SanPhamService
    {
        #region instance
        private static SanPhamService instance;

        public static SanPhamService Instance
        {
            get
            {
                if (instance == null)
                    instance = new SanPhamService();
                return instance;
            }
        }
        #endregion

        public SanPham LayThongTinSanPhamTuForm(
            TextEdit txtTenSanPham,
            TextEdit txtMaTem,
            LookUpEdit lueHamLuongVang,
            TextEdit txtChiTinh, TextEdit txtPhanTinh, TextEdit txtLyTinh,
            TextEdit txtChiDa, TextEdit txtPhanDa, TextEdit txtLyDa,
            TextEdit txtChiVang, TextEdit txtPhanVang, TextEdit txtLyVang,
            TextEdit txtTiLeHot,
            TextEdit txtTienCong,
            LookUpEdit lueKiHieu,
            LookUpEdit lueNhaCungCap)
        {
            int tienCong = txtTienCong.EditValue == null ? 0 : int.Parse(txtTienCong.EditValue.ToString());
            double tiLeHot = txtTiLeHot.EditValue == null ? 0 : double.Parse(txtTiLeHot.EditValue.ToString());
            double khoiLuongVang = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiVang, txtPhanVang, txtLyVang);
            double khoiLuongTinh = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiTinh, txtPhanTinh, txtLyTinh);
            double khoiLuongDa = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiDa, txtPhanDa, txtLyDa);
            int? maNhaCungCap = null;

            if (lueNhaCungCap.EditValue != null)
            {
                maNhaCungCap = (int)lueNhaCungCap.EditValue;
            }

            return new SanPham
            {
                MaTem = txtMaTem.Text,
                TenSanPham = txtTenSanPham.Text,
                MaHamLuongVang = Convert.ToInt32(lueHamLuongVang.EditValue),
                TienCong = tienCong,
                TiLeHot = tiLeHot,
                KhoiLuongTinh = khoiLuongTinh,
                KhoiLuongDa = khoiLuongDa,
                KhoiLuongVang = khoiLuongVang,
                HamLuongVang = lueHamLuongVang.Text,
                KiHieu = lueKiHieu.Text,
                MaNhaCungCap = maNhaCungCap
            };
        }
    }
}
