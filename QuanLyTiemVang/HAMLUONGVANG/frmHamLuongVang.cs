﻿using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.HAMLUONGVANG
{
    public partial class frmHamLuongVang : Form
    {

        public frmHamLuongVang()
        {
            InitializeComponent();
        }

        private void frmHamLuongVang_Load(object sender, EventArgs e)
        {
            grcDanhSachHamLuongVang.DataSource = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang();
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtHamLuongVang.Text))
            {
                PopupService.Instance.Error(ErrorUltils.ENOUGH_DATA);
            }
            else
            {
                string hamLuongVang = txtHamLuongVang.Text.ToUpper();

                if (KiHieuHamLuongVangBUS.Instance.HaveHamLuongVang(hamLuongVang))
                {
                    PopupService.Instance.Warning(MessageWarning.DUPLICATE_HAM_LUONG_VANG);
                    return;
                }

                if (KiHieuHamLuongVangBUS.Instance.Insert(hamLuongVang) != null)
                {
                    PopupService.Instance.Success(ErrorUltils.SUCCESS);
                    ReloadData();
                }
                else
                {
                    PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                }
            }
        }

        private void ReloadData()
        {
            grcDanhSachHamLuongVang.DataSource = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang();
            btnSua.Tag = null;
            btnXoa.Tag = null;
            txtHamLuongVang.Text = null;
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReloadData();
        }

        private void grcDanhSachHamLuongVang_DoubleClick(object sender, EventArgs e)
        {
            var hamLuongVang = (KiHieuHamLuongVang)grvDanhSachHamLuongVang.GetFocusedRow();
            btnSua.Tag = btnXoa.Tag = hamLuongVang;
            txtHamLuongVang.Text = hamLuongVang.HamLuongVang;
            btnThem.Enabled = false;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.UPDATE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            var kiHieuHamLuongVang = (KiHieuHamLuongVang)btnSua.Tag;

            if (kiHieuHamLuongVang != null)
            {
                kiHieuHamLuongVang.HamLuongVang = txtHamLuongVang.Text.ToUpper();
                bool ketQuaSua = KiHieuHamLuongVangBUS.Instance.Update(kiHieuHamLuongVang);
                if (ketQuaSua)
                {
                    PopupService.Instance.Success(ErrorUltils.SUCCESS);
                    ReloadData();
                }
                else
                {
                    PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                }
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }

        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            var kiHieuHamLuongVang = (KiHieuHamLuongVang)btnXoa.Tag;

            if (kiHieuHamLuongVang != null)
            {
                bool ketQuaXoa = KiHieuHamLuongVangBUS.Instance.DeleteById(kiHieuHamLuongVang.MaHamLuongVang);
                if (ketQuaXoa)
                {
                    PopupService.Instance.Success(ErrorUltils.SUCCESS);
                    ReloadData();
                }
                else
                {
                    PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                }
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }

        }
    }
}
