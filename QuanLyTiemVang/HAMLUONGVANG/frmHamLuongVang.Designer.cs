﻿namespace QuanLyTiemVang.HAMLUONGVANG
{
    partial class frmHamLuongVang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.grpChiTietHamLuongVang = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtHamLuongVang = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.grpDanhSachHamLuongVang = new DevExpress.XtraEditors.GroupControl();
            this.grcDanhSachHamLuongVang = new DevExpress.XtraGrid.GridControl();
            this.grvDanhSachHamLuongVang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpChiTietHamLuongVang)).BeginInit();
            this.grpChiTietHamLuongVang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachHamLuongVang)).BeginInit();
            this.grpDanhSachHamLuongVang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachHamLuongVang)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 4;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnSua, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnThem
            // 
            this.btnThem.Caption = "Thêm";
            this.btnThem.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThem.Id = 0;
            this.btnThem.Name = "btnThem";
            this.btnThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThem_ItemClick);
            // 
            // btnSua
            // 
            this.btnSua.Caption = "Sửa";
            this.btnSua.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnSua.Id = 1;
            this.btnSua.Name = "btnSua";
            this.btnSua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSua_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 2;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlTop.Size = new System.Drawing.Size(877, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlBottom.Size = new System.Drawing.Size(877, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 382);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(877, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 382);
            // 
            // grpChiTietHamLuongVang
            // 
            this.grpChiTietHamLuongVang.Controls.Add(this.layoutControl1);
            this.grpChiTietHamLuongVang.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpChiTietHamLuongVang.Location = new System.Drawing.Point(0, 50);
            this.grpChiTietHamLuongVang.Margin = new System.Windows.Forms.Padding(4);
            this.grpChiTietHamLuongVang.Name = "grpChiTietHamLuongVang";
            this.grpChiTietHamLuongVang.Size = new System.Drawing.Size(877, 92);
            this.grpChiTietHamLuongVang.TabIndex = 4;
            this.grpChiTietHamLuongVang.Text = "Hàm lượng vàng";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtHamLuongVang);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(873, 65);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtHamLuongVang
            // 
            this.txtHamLuongVang.Location = new System.Drawing.Point(147, 16);
            this.txtHamLuongVang.Margin = new System.Windows.Forms.Padding(4);
            this.txtHamLuongVang.MenuManager = this.barManager1;
            this.txtHamLuongVang.Name = "txtHamLuongVang";
            this.txtHamLuongVang.Size = new System.Drawing.Size(710, 22);
            this.txtHamLuongVang.StyleController = this.layoutControl1;
            this.txtHamLuongVang.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(873, 65);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtHamLuongVang;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(847, 39);
            this.layoutControlItem1.Text = "Tên hàm lượng vàng";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(128, 17);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 142);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(877, 6);
            this.splitterControl1.TabIndex = 5;
            this.splitterControl1.TabStop = false;
            // 
            // grpDanhSachHamLuongVang
            // 
            this.grpDanhSachHamLuongVang.Controls.Add(this.grcDanhSachHamLuongVang);
            this.grpDanhSachHamLuongVang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDanhSachHamLuongVang.Location = new System.Drawing.Point(0, 148);
            this.grpDanhSachHamLuongVang.Margin = new System.Windows.Forms.Padding(4);
            this.grpDanhSachHamLuongVang.Name = "grpDanhSachHamLuongVang";
            this.grpDanhSachHamLuongVang.Size = new System.Drawing.Size(877, 284);
            this.grpDanhSachHamLuongVang.TabIndex = 6;
            this.grpDanhSachHamLuongVang.Text = "Danh sách hàm lượng vàng";
            // 
            // grcDanhSachHamLuongVang
            // 
            this.grcDanhSachHamLuongVang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachHamLuongVang.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grcDanhSachHamLuongVang.Location = new System.Drawing.Point(2, 25);
            this.grcDanhSachHamLuongVang.MainView = this.grvDanhSachHamLuongVang;
            this.grcDanhSachHamLuongVang.Margin = new System.Windows.Forms.Padding(4);
            this.grcDanhSachHamLuongVang.MenuManager = this.barManager1;
            this.grcDanhSachHamLuongVang.Name = "grcDanhSachHamLuongVang";
            this.grcDanhSachHamLuongVang.Size = new System.Drawing.Size(873, 257);
            this.grcDanhSachHamLuongVang.TabIndex = 0;
            this.grcDanhSachHamLuongVang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDanhSachHamLuongVang});
            this.grcDanhSachHamLuongVang.DoubleClick += new System.EventHandler(this.grcDanhSachHamLuongVang_DoubleClick);
            // 
            // grvDanhSachHamLuongVang
            // 
            this.grvDanhSachHamLuongVang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.grvDanhSachHamLuongVang.GridControl = this.grcDanhSachHamLuongVang;
            this.grvDanhSachHamLuongVang.Name = "grvDanhSachHamLuongVang";
            this.grvDanhSachHamLuongVang.OptionsBehavior.Editable = false;
            this.grvDanhSachHamLuongVang.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvDanhSachHamLuongVang.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Hàm lượng vàng";
            this.gridColumn1.FieldName = "HamLuongVang";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // frmHamLuongVang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 432);
            this.Controls.Add(this.grpDanhSachHamLuongVang);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.grpChiTietHamLuongVang);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmHamLuongVang";
            this.Text = "Hàm Lượng Vàng";
            this.Load += new System.EventHandler(this.frmHamLuongVang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpChiTietHamLuongVang)).EndInit();
            this.grpChiTietHamLuongVang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachHamLuongVang)).EndInit();
            this.grpDanhSachHamLuongVang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachHamLuongVang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl grpChiTietHamLuongVang;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtHamLuongVang;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.GroupControl grpDanhSachHamLuongVang;
        private DevExpress.XtraGrid.GridControl grcDanhSachHamLuongVang;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDanhSachHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
    }
}