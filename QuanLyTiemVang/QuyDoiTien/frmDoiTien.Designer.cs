﻿namespace QuanLyTiemVang
{
    partial class frmDoiTien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnDoiTien = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTenNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.detNgayDoi = new DevExpress.XtraEditors.DateEdit();
            this.txtTienDich = new DevExpress.XtraEditors.TextEdit();
            this.txtTienNguon = new DevExpress.XtraEditors.TextEdit();
            this.lueLoaiTienDich = new DevExpress.XtraEditors.LookUpEdit();
            this.lueLoaiTienNguon = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcLichSuDoiTien = new DevExpress.XtraGrid.GridControl();
            this.lichSuDoiNgoaiTeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvLichSuDoiTien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNgayGiaoDich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoTienDoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoTienNhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoiTheoChieuThuanHoacNguoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dxerNgayDoi = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLoaiTienDoi = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerTienNguon = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDoi.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienNguon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiTienDich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiTienNguon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuDoiTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuDoiNgoaiTeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuDoiTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayDoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiTienDoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTienNguon)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLamMoiDuLieu,
            this.btnNhapLai,
            this.btnDoiTien,
            this.btnXoa});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDoiTien, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnDoiTien
            // 
            this.btnDoiTien.Caption = "Đổi tiền";
            this.btnDoiTien.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnDoiTien.Id = 2;
            this.btnDoiTien.Name = "btnDoiTien";
            this.btnDoiTien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDoiTien_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 4;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 1;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 0;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1237, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 583);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1237, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 533);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1237, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 533);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1237, 137);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Quy đổi tiền";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtTenNhanVien);
            this.layoutControl1.Controls.Add(this.detNgayDoi);
            this.layoutControl1.Controls.Add(this.txtTienDich);
            this.layoutControl1.Controls.Add(this.txtTienNguon);
            this.layoutControl1.Controls.Add(this.lueLoaiTienDich);
            this.layoutControl1.Controls.Add(this.lueLoaiTienNguon);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1233, 110);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtTenNhanVien
            // 
            this.txtTenNhanVien.Location = new System.Drawing.Point(703, 16);
            this.txtTenNhanVien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTenNhanVien.MenuManager = this.barManager1;
            this.txtTenNhanVien.Name = "txtTenNhanVien";
            this.txtTenNhanVien.Properties.ReadOnly = true;
            this.txtTenNhanVien.Size = new System.Drawing.Size(514, 22);
            this.txtTenNhanVien.StyleController = this.layoutControl1;
            this.txtTenNhanVien.TabIndex = 10;
            // 
            // detNgayDoi
            // 
            this.detNgayDoi.EditValue = null;
            this.detNgayDoi.Location = new System.Drawing.Point(100, 16);
            this.detNgayDoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detNgayDoi.MenuManager = this.barManager1;
            this.detNgayDoi.Name = "detNgayDoi";
            this.detNgayDoi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayDoi.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayDoi.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayDoi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayDoi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detNgayDoi.Size = new System.Drawing.Size(513, 22);
            this.detNgayDoi.StyleController = this.layoutControl1;
            this.detNgayDoi.TabIndex = 9;
            // 
            // txtTienDich
            // 
            this.txtTienDich.Location = new System.Drawing.Point(703, 72);
            this.txtTienDich.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTienDich.MenuManager = this.barManager1;
            this.txtTienDich.Name = "txtTienDich";
            this.txtTienDich.Properties.Mask.EditMask = "n0";
            this.txtTienDich.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDich.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienDich.Properties.ReadOnly = true;
            this.txtTienDich.Size = new System.Drawing.Size(514, 22);
            this.txtTienDich.StyleController = this.layoutControl1;
            this.txtTienDich.TabIndex = 7;
            // 
            // txtTienNguon
            // 
            this.txtTienNguon.Location = new System.Drawing.Point(100, 72);
            this.txtTienNguon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTienNguon.MenuManager = this.barManager1;
            this.txtTienNguon.Name = "txtTienNguon";
            this.txtTienNguon.Properties.Mask.EditMask = "n0";
            this.txtTienNguon.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienNguon.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienNguon.Size = new System.Drawing.Size(513, 22);
            this.txtTienNguon.StyleController = this.layoutControl1;
            this.txtTienNguon.TabIndex = 6;
            this.txtTienNguon.TextChanged += new System.EventHandler(this.txtTienNguon_TextChanged);
            // 
            // lueLoaiTienDich
            // 
            this.lueLoaiTienDich.Location = new System.Drawing.Point(703, 44);
            this.lueLoaiTienDich.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueLoaiTienDich.MenuManager = this.barManager1;
            this.lueLoaiTienDich.Name = "lueLoaiTienDich";
            this.lueLoaiTienDich.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiTienDich.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiTien", "Tên loại tiền")});
            this.lueLoaiTienDich.Properties.NullText = "Việt Nam đồng";
            this.lueLoaiTienDich.Size = new System.Drawing.Size(514, 22);
            this.lueLoaiTienDich.StyleController = this.layoutControl1;
            this.lueLoaiTienDich.TabIndex = 5;
            this.lueLoaiTienDich.EditValueChanged += new System.EventHandler(this.lueLoaiTienDich_EditValueChanged);
            // 
            // lueLoaiTienNguon
            // 
            this.lueLoaiTienNguon.Location = new System.Drawing.Point(100, 44);
            this.lueLoaiTienNguon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueLoaiTienNguon.MenuManager = this.barManager1;
            this.lueLoaiTienNguon.Name = "lueLoaiTienNguon";
            this.lueLoaiTienNguon.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiTienNguon.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiTien", "Tên loại tiền")});
            this.lueLoaiTienNguon.Properties.NullText = "Việt Nam đồng";
            this.lueLoaiTienNguon.Size = new System.Drawing.Size(513, 22);
            this.lueLoaiTienNguon.StyleController = this.layoutControl1;
            this.lueLoaiTienNguon.TabIndex = 4;
            this.lueLoaiTienNguon.EditValueChanged += new System.EventHandler(this.lueLoaiTienNguon_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1233, 110);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueLoaiTienDich;
            this.layoutControlItem2.Location = new System.Drawing.Point(603, 28);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(604, 28);
            this.layoutControlItem2.Text = "Đổi sang";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(81, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtTienNguon;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(603, 28);
            this.layoutControlItem3.Text = "Từ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(81, 17);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueLoaiTienNguon;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(603, 28);
            this.layoutControlItem1.Text = "Loại tiền";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(81, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTienDich;
            this.layoutControlItem4.Location = new System.Drawing.Point(603, 56);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(604, 28);
            this.layoutControlItem4.Text = "Thành";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(81, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.detNgayDoi;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(603, 28);
            this.layoutControlItem6.Text = "Ngày đổi";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(81, 17);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtTenNhanVien;
            this.layoutControlItem7.Location = new System.Drawing.Point(603, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(604, 28);
            this.layoutControlItem7.Text = "Tên nhân viên";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(81, 16);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcLichSuDoiTien);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 187);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1237, 396);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Lịch sử đổi tiền";
            // 
            // grcLichSuDoiTien
            // 
            this.grcLichSuDoiTien.DataSource = this.lichSuDoiNgoaiTeBindingSource;
            this.grcLichSuDoiTien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcLichSuDoiTien.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcLichSuDoiTien.Location = new System.Drawing.Point(2, 25);
            this.grcLichSuDoiTien.MainView = this.grvLichSuDoiTien;
            this.grcLichSuDoiTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcLichSuDoiTien.MenuManager = this.barManager1;
            this.grcLichSuDoiTien.Name = "grcLichSuDoiTien";
            this.grcLichSuDoiTien.Size = new System.Drawing.Size(1233, 369);
            this.grcLichSuDoiTien.TabIndex = 0;
            this.grcLichSuDoiTien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichSuDoiTien});
            this.grcLichSuDoiTien.DoubleClick += new System.EventHandler(this.grcLichSuDoiTien_DoubleClick);
            // 
            // lichSuDoiNgoaiTeBindingSource
            // 
            this.lichSuDoiNgoaiTeBindingSource.DataSource = typeof(DTO.Models.LichSuDoiNgoaiTe);
            // 
            // grvLichSuDoiTien
            // 
            this.grvLichSuDoiTien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNgayGiaoDich,
            this.colLoaiTien,
            this.colSoTienDoi,
            this.colSoTienNhan,
            this.colDoiTheoChieuThuanHoacNguoc});
            this.grvLichSuDoiTien.GridControl = this.grcLichSuDoiTien;
            this.grvLichSuDoiTien.Name = "grvLichSuDoiTien";
            this.grvLichSuDoiTien.OptionsBehavior.Editable = false;
            this.grvLichSuDoiTien.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvLichSuDoiTien.OptionsFind.AlwaysVisible = true;
            this.grvLichSuDoiTien.OptionsFind.FindDelay = 100;
            this.grvLichSuDoiTien.OptionsFind.FindNullPrompt = "";
            this.grvLichSuDoiTien.OptionsFind.ShowFindButton = false;
            this.grvLichSuDoiTien.OptionsView.ShowGroupPanel = false;
            // 
            // colNgayGiaoDich
            // 
            this.colNgayGiaoDich.Caption = "Ngày";
            this.colNgayGiaoDich.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayGiaoDich.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayGiaoDich.FieldName = "NgayGiaoDich";
            this.colNgayGiaoDich.Name = "colNgayGiaoDich";
            this.colNgayGiaoDich.Visible = true;
            this.colNgayGiaoDich.VisibleIndex = 0;
            // 
            // colLoaiTien
            // 
            this.colLoaiTien.Caption = "Loại tiền";
            this.colLoaiTien.FieldName = "LichSuTyGiaTien.LoaiTien.TenLoaiTien";
            this.colLoaiTien.Name = "colLoaiTien";
            this.colLoaiTien.Visible = true;
            this.colLoaiTien.VisibleIndex = 1;
            // 
            // colSoTienDoi
            // 
            this.colSoTienDoi.Caption = "Số tiền đổi";
            this.colSoTienDoi.DisplayFormat.FormatString = "n0";
            this.colSoTienDoi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSoTienDoi.FieldName = "SoTienDoi";
            this.colSoTienDoi.Name = "colSoTienDoi";
            this.colSoTienDoi.Visible = true;
            this.colSoTienDoi.VisibleIndex = 3;
            // 
            // colSoTienNhan
            // 
            this.colSoTienNhan.Caption = "Số tiền khách nhận";
            this.colSoTienNhan.DisplayFormat.FormatString = "n0";
            this.colSoTienNhan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSoTienNhan.FieldName = "SoTienKhachNhan";
            this.colSoTienNhan.Name = "colSoTienNhan";
            this.colSoTienNhan.Visible = true;
            this.colSoTienNhan.VisibleIndex = 4;
            // 
            // colDoiTheoChieuThuanHoacNguoc
            // 
            this.colDoiTheoChieuThuanHoacNguoc.Caption = "Chiều đổi";
            this.colDoiTheoChieuThuanHoacNguoc.FieldName = "DoiTheoChieuThuanHoacNguoc";
            this.colDoiTheoChieuThuanHoacNguoc.Name = "colDoiTheoChieuThuanHoacNguoc";
            this.colDoiTheoChieuThuanHoacNguoc.Visible = true;
            this.colDoiTheoChieuThuanHoacNguoc.VisibleIndex = 2;
            // 
            // dxerNgayDoi
            // 
            this.dxerNgayDoi.ContainerControl = this;
            // 
            // dxerLoaiTienDoi
            // 
            this.dxerLoaiTienDoi.ContainerControl = this;
            // 
            // dxerTienNguon
            // 
            this.dxerTienNguon.ContainerControl = this;
            // 
            // frmDoiTien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1237, 583);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmDoiTien";
            this.Text = "Quy đổi tiền";
            this.Load += new System.EventHandler(this.frmDoiTien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDoi.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienNguon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiTienDich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiTienNguon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuDoiTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuDoiNgoaiTeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuDoiTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayDoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLoaiTienDoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTienNguon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTienDich;
        private DevExpress.XtraEditors.TextEdit txtTienNguon;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiTienDich;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiTienNguon;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcLichSuDoiTien;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichSuDoiTien;
        private System.Windows.Forms.BindingSource lichSuDoiNgoaiTeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayGiaoDich;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiTien;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraEditors.TextEdit txtTenNhanVien;
        private DevExpress.XtraEditors.DateEdit detNgayDoi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraBars.BarButtonItem btnDoiTien;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNgayDoi;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLoaiTienDoi;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTienNguon;
        private DevExpress.XtraGrid.Columns.GridColumn colSoTienDoi;
        private DevExpress.XtraGrid.Columns.GridColumn colSoTienNhan;
        private DevExpress.XtraGrid.Columns.GridColumn colDoiTheoChieuThuanHoacNguoc;
    }
}