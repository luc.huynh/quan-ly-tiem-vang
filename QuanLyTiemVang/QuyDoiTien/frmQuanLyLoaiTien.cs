﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;

namespace QuanLyTiemVang
{
    public partial class frmQuanLyLoaiTien : DevExpress.XtraEditors.XtraForm
    {
        public frmQuanLyLoaiTien()
        {
            InitializeComponent();
        }

        private void frmQuanLyLoaiTien_Load(object sender, EventArgs e)
        {
            LoaiTienBUS.Instance.GetList(gctLoaiTien);
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnSua.Tag == null && btnXoa.Tag == null)
            {
                //Check validate tài khoản
                if (checkValidate())
                {
                    string tenLoaiTien = txtTenLoaiTien.Text;
                    if (LoaiTienBUS.Instance.Insert(tenLoaiTien))
                    {
                        PopupService.Instance.Success(ErrorUltils.SUCCESS);
                        LoadDataSoure();
                    }
                }
            }
            else
            {
                PopupService.Instance.Warning("Bạn đang ở chế độ chỉnh sửa. Ấn \"Nhập lại\" để thoát khỏi chế độ chỉnh sửa");
            }
        }

        private void LoadDataSoure()
        {
            ClearForm();
            LoaiTienBUS.Instance.GetList(gctLoaiTien);
            gctLoaiTien.RefreshDataSource();
        }

        private void ClearForm()
        {
            //Xóa thông tin trong form đồng nghĩa với bỏ chọn nhân viên đó
            btnThem.Enabled = true;
            btnSua.Tag = null;
            btnXoa.Tag = null;
            txtTenLoaiTien.Text = null;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private bool checkValidate()
        {
            dxErrorProvider1.Dispose();
            bool check = true;

            if (string.IsNullOrWhiteSpace(txtTenLoaiTien.Text))
            {
                dxErrorProvider1.SetError(txtTenLoaiTien, ValidationUltils.TEN_LOAI_TIEN);
                check = false;
            }

            return check;
        }

        private void gctLoaiTien_DoubleClick(object sender, EventArgs e)
        {
            //Đẩy dữ liệu lên
            LoaiTien loaiTien = (LoaiTien)grvLoaiTien.GetFocusedRow();
            if (loaiTien != null)
            {
                btnSua.Tag = loaiTien;
                btnXoa.Tag = loaiTien;
                btnThem.Enabled = false;
                btnSua.Enabled = true;
                btnXoa.Enabled = true;
                txtTenLoaiTien.Text = loaiTien.TenLoaiTien;
            }
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnSua.Tag != null)
            {
                LoaiTien loaiTien = (LoaiTien)btnSua.Tag;
                loaiTien.TenLoaiTien = txtTenLoaiTien.Text;
                if (LoaiTienBUS.Instance.Update(loaiTien))
                {
                    PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                    ClearForm();
                    LoadDataSoure();
                }
                else
                {
                    PopupService.Instance.Error(MessageError.UPDATE_ERROR);
                }
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnXoa.Tag != null)
            {
                string tenLoaiTien = string.IsNullOrEmpty(((LoaiTien)btnXoa.Tag).TenLoaiTien) ? "này" : ((LoaiTien)btnXoa.Tag).TenLoaiTien;
                DialogResult dialogResult = PopupService.Instance.Question("Bạn chắc muốn xóa loại tiền " + tenLoaiTien + " không ? ");
                if (dialogResult == DialogResult.Yes)
                {
                    if (LoaiTienBUS.Instance.Delete(((LoaiTien)btnXoa.Tag).MaLoaiTien))
                    {
                        LoadDataSoure();
                        ClearForm();
                        PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                    }
                    else
                    {
                        PopupService.Instance.Error(MessageError.DELETE_ERROR);
                    }
                }
            }
            else
            {
                PopupService.Instance.Error(ErrorUltils.NOTHING_CHOOSE);
            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearForm();
        }
    }
}