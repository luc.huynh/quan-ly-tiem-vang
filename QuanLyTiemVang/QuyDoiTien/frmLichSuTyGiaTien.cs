﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.TemporaryModel;
using DTO.Models;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang
{
    public partial class frmLichSuTyGiaTien : DevExpress.XtraEditors.XtraForm
    {
        public frmLichSuTyGiaTien()
        {
            InitializeComponent();
        }

        private List<LichSuTyGiaTien> listTyGiaTienHienTai;

        private void frmLichSuTyGiaTien_Load(object sender, EventArgs e)
        {
            listTyGiaTienHienTai = LichSuTyGiaTienBUS.Instance.GetAllTyGiaTien();
            grcTyGiaTien.DataSource = listTyGiaTienHienTai;
        }

        private void btnNhapGia_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int index = 0;

            foreach (LichSuTyGiaTien tyGiaTienHienTai in listTyGiaTienHienTai)
            {
                tyGiaTienHienTai.GiaMuaVao = ((LichSuTyGiaTien)grvTyGiaTien.GetRow(index)).GiaMuaVao;
                tyGiaTienHienTai.GiaBanRa = ((LichSuTyGiaTien)grvTyGiaTien.GetRow(index)).GiaBanRa;
                index++;
            }
            
            if (LichSuTyGiaTienBUS.Instance.NhapTyGiaTien(listTyGiaTienHienTai, DateTime.Today))
            {
                PopupService.Instance.Success(Constants.StringUltils.ErrorUltils.SUCCESS);
            }
            else
            {
                PopupService.Instance.Error(Constants.StringUltils.ErrorUltils.SOMETHING_ERROR);
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            listTyGiaTienHienTai = LichSuTyGiaTienBUS.Instance.GetAllTyGiaTien();
            grcTyGiaTien.DataSource = listTyGiaTienHienTai;
        }
    }
}