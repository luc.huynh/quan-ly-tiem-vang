﻿namespace QuanLyTiemVang
{
    partial class frmLichSuTyGiaTien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.grcTyGiaTien = new DevExpress.XtraGrid.GridControl();
            this.lichSuTyGiaTienBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvTyGiaTien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclNgayNhap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaBanRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnNhapGia = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTyGiaTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuTyGiaTienBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTyGiaTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.grcTyGiaTien);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(702, 384);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Tỷ giá tiền";
            // 
            // grcTyGiaTien
            // 
            this.grcTyGiaTien.DataSource = this.lichSuTyGiaTienBindingSource;
            this.grcTyGiaTien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTyGiaTien.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTyGiaTien.Location = new System.Drawing.Point(2, 25);
            this.grcTyGiaTien.MainView = this.grvTyGiaTien;
            this.grcTyGiaTien.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTyGiaTien.Name = "grcTyGiaTien";
            this.grcTyGiaTien.Size = new System.Drawing.Size(698, 357);
            this.grcTyGiaTien.TabIndex = 0;
            this.grcTyGiaTien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTyGiaTien});
            // 
            // lichSuTyGiaTienBindingSource
            // 
            this.lichSuTyGiaTienBindingSource.DataSource = typeof(DTO.Models.LichSuTyGiaTien);
            // 
            // grvTyGiaTien
            // 
            this.grvTyGiaTien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclNgayNhap,
            this.grclGiaBanRa,
            this.grclGiaMuaVao,
            this.colLoaiTien});
            this.grvTyGiaTien.GridControl = this.grcTyGiaTien;
            this.grvTyGiaTien.Name = "grvTyGiaTien";
            this.grvTyGiaTien.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvTyGiaTien.OptionsView.ShowGroupPanel = false;
            // 
            // grclNgayNhap
            // 
            this.grclNgayNhap.Caption = "Ngày nhập";
            this.grclNgayNhap.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.grclNgayNhap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclNgayNhap.FieldName = "NgayNhap";
            this.grclNgayNhap.Name = "grclNgayNhap";
            this.grclNgayNhap.OptionsColumn.AllowEdit = false;
            this.grclNgayNhap.Visible = true;
            this.grclNgayNhap.VisibleIndex = 2;
            // 
            // grclGiaBanRa
            // 
            this.grclGiaBanRa.Caption = "Giá bán ra";
            this.grclGiaBanRa.DisplayFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaBanRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaBanRa.FieldName = "GiaBanRa";
            this.grclGiaBanRa.GroupFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaBanRa.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaBanRa.Name = "grclGiaBanRa";
            this.grclGiaBanRa.Visible = true;
            this.grclGiaBanRa.VisibleIndex = 1;
            // 
            // grclGiaMuaVao
            // 
            this.grclGiaMuaVao.Caption = "Giá mua vào";
            this.grclGiaMuaVao.DisplayFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaMuaVao.FieldName = "GiaMuaVao";
            this.grclGiaMuaVao.GroupFormat.FormatString = "###,###,###,###,### vnd";
            this.grclGiaMuaVao.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclGiaMuaVao.Name = "grclGiaMuaVao";
            this.grclGiaMuaVao.Visible = true;
            this.grclGiaMuaVao.VisibleIndex = 0;
            // 
            // colLoaiTien
            // 
            this.colLoaiTien.Caption = "Tên loại tiền";
            this.colLoaiTien.FieldName = "LoaiTien.TenLoaiTien";
            this.colLoaiTien.Name = "colLoaiTien";
            this.colLoaiTien.Visible = true;
            this.colLoaiTien.VisibleIndex = 3;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnNhapGia,
            this.btnLamMoiDuLieu});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapGia, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnNhapGia
            // 
            this.btnNhapGia.Caption = "Nhập giá";
            this.btnNhapGia.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnNhapGia.Id = 0;
            this.btnNhapGia.Name = "btnNhapGia";
            this.btnNhapGia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapGia_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 1;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(702, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 434);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(702, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 384);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(702, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 384);
            // 
            // frmLichSuTyGiaTien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 434);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmLichSuTyGiaTien";
            this.Text = "Giá tiền quy đổi";
            this.Load += new System.EventHandler(this.frmLichSuTyGiaTien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTyGiaTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lichSuTyGiaTienBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTyGiaTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl grcTyGiaTien;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTyGiaTien;
        private DevExpress.XtraGrid.Columns.GridColumn grclNgayNhap;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaBanRa;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaMuaVao;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnNhapGia;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private System.Windows.Forms.BindingSource lichSuTyGiaTienBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiTien;
    }
}