﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.TemporaryModel;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using Share.Constant;

namespace QuanLyTiemVang
{
    public partial class frmDoiTien : DevExpress.XtraEditors.XtraForm
    {
        public frmDoiTien()
        {
            InitializeComponent();
        }

        private List<LichSuTyGiaTien> listTyGiaTienHienTai;

        List<string> listNameNotNeedClear = new List<string>
        {
            "txtTenNhanVien"
        };

        List<LichSuDoiNgoaiTe> listLichSuDoiNgoaiTeDb = new List<LichSuDoiNgoaiTe>();

        private void txtTienNguon_TextChanged(object sender, EventArgs e)
        {
            DoiTien();
        }

        private void frmDoiTien_Load(object sender, EventArgs e)
        {
            txtTenNhanVien.Text = Form1.nhanVien.Ten;
            detNgayDoi.DateTime = DateTime.Today;
            grcLichSuDoiTien.DataSource = listLichSuDoiNgoaiTeDb;

            LoadData();
        }

        private void LoadData()
        {
            detNgayDoi.Properties.MaxValue = DateTime.Today;

            LoaiTienBUS.Instance.getLookupEdit(lueLoaiTienDich);
            LoaiTienBUS.Instance.getLookupEdit(lueLoaiTienNguon);
            listTyGiaTienHienTai = LichSuTyGiaTienBUS.Instance.GetAllTyGiaTien();

            listLichSuDoiNgoaiTeDb.Clear();
            listLichSuDoiNgoaiTeDb.AddRange(LichSuDoiNgoaiTeBUS.Instance.GetList());
            grcLichSuDoiTien.RefreshDataSource();
        }

        private void HienThiDuLieuLenForm(LichSuDoiNgoaiTe lichSuDoiNgoaiTe)
        {
            detNgayDoi.EditValue = lichSuDoiNgoaiTe.NgayGiaoDich;
            txtTenNhanVien.EditValue = lichSuDoiNgoaiTe.NhanVien.Ten;
            txtTienDich.EditValue = lichSuDoiNgoaiTe.SoTienDoi;
            txtTienNguon.EditValue = lichSuDoiNgoaiTe.SoTienKhachNhan;

            if (lichSuDoiNgoaiTe.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.BAN_NGOAI_TE)
            {
                lueLoaiTienDich.EditValue = null;
                lueLoaiTienNguon.EditValue = lichSuDoiNgoaiTe.LichSuTyGiaTien.LoaiTien.TenLoaiTien;
            }
            else
            {
                lueLoaiTienNguon.EditValue = null;
                lueLoaiTienDich.EditValue = lichSuDoiNgoaiTe.LichSuTyGiaTien.LoaiTien.TenLoaiTien;
            }
        }

        private bool IsValidForm()
        {
            dxerLoaiTienDoi.Dispose();
            dxerNgayDoi.Dispose();
            dxerTienNguon.Dispose();

            bool check = true;

            if (lueLoaiTienDich.EditValue == null && lueLoaiTienNguon.EditValue == null)
            {
                dxerLoaiTienDoi.SetError(lueLoaiTienDich, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (detNgayDoi.EditValue == null)
            {
                dxerNgayDoi.SetError(detNgayDoi, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (txtTienNguon.EditValue == null || Convert.ToInt32(txtTienNguon.EditValue) < 1)
            {
                dxerTienNguon.SetError(txtTienNguon, ValidationUltils.INPUT_NULL);
                check = false;
            }

            return check;
        }

        private LichSuDoiNgoaiTe GetDoiTuongTuForm()
        {
            if (IsValidForm())
            {
                LichSuTyGiaTien lichSuTyGiaTien;

                if (lueLoaiTienDich.EditValue != null)
                {
                    lichSuTyGiaTien = listTyGiaTienHienTai.Find(x => x.MaLoaiTien == (int)lueLoaiTienDich.EditValue);
                }
                else
                {
                    lichSuTyGiaTien = listTyGiaTienHienTai.Find(x => x.MaLoaiTien == (int)lueLoaiTienNguon.EditValue);
                }

                return new LichSuDoiNgoaiTe
                {
                    DoiTheoChieuThuanHoacNguoc = lueLoaiTienDich.EditValue == null ? QuyDoiNgoaiTeConstant.THU_MUA_NGOAI_TE : QuyDoiNgoaiTeConstant.BAN_NGOAI_TE,
                    MaLichSuTyGiaTien = lichSuTyGiaTien.MaLichSuTyGiaTien,
                    MaNhanVien = Form1.nhanVien.MaNhanVien,
                    NgayGiaoDich = detNgayDoi.DateTime.Date,
                    SoTienDoi = Convert.ToInt32(txtTienNguon.EditValue),
                    SoTienKhachNhan = Convert.ToInt32(txtTienDich.EditValue)
                };
            }

            return null;
        }

        private void ClearForm()
        {
            ClearFormService.ClearAllForm(this, listNameNotNeedClear);
            txtTenNhanVien.EditValue = Form1.nhanVien.Ten;
            btnXoa.Tag = null;
        }

        private void DoiTien()
        {
            LichSuTyGiaTien loaiTienQuyDoi;
            double tienNguon;

            if (txtTienNguon.EditValue != null)
            {
                tienNguon = Convert.ToDouble(txtTienNguon.EditValue);

                if (lueLoaiTienNguon.EditValue != null)
                {
                    loaiTienQuyDoi = listTyGiaTienHienTai.Find(x => x.MaLoaiTien == (int)lueLoaiTienNguon.EditValue);
                    txtTienDich.EditValue = Math.Round(tienNguon * (loaiTienQuyDoi.GiaMuaVao));
                }
                else if (lueLoaiTienDich.EditValue != null)
                {
                    loaiTienQuyDoi = listTyGiaTienHienTai.Find(x => x.MaLoaiTien == (int)lueLoaiTienDich.EditValue);
                    txtTienDich.EditValue = Math.Round(tienNguon / (loaiTienQuyDoi.GiaBanRa));
                }
            }
        }

        private void lueLoaiTienNguon_EditValueChanged(object sender, EventArgs e)
        {
            lueLoaiTienDich.EditValue = null;
            DoiTien();
        }

        private void lueLoaiTienDich_EditValueChanged(object sender, EventArgs e)
        {
            lueLoaiTienNguon.EditValue = null;
            DoiTien();
        }

        private void btnDoiTien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LichSuDoiNgoaiTe lichSuDoiNgoaiTe = GetDoiTuongTuForm();

            if (lichSuDoiNgoaiTe == null)
            {
                return;
            }

            if (LichSuDoiNgoaiTeBUS.Instance.Insert(lichSuDoiNgoaiTe) != null)
            {
                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.INSERT_ERROR);
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnXoa.Tag == null)
            {
                return;
            }

            LichSuDoiNgoaiTe lichSuDoiNgoaiTe = (LichSuDoiNgoaiTe)btnXoa.Tag;

            var dialogResult = PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW);

            if (dialogResult == DialogResult.No)
            {
                return;
            }

            if (LichSuDoiNgoaiTeBUS.Instance.DeleteById(lichSuDoiNgoaiTe.MaLichSuDoiNgoaiTe) != null)
            {
                PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.DELETE_ERROR);
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearFormService.ClearAllForm(this, listNameNotNeedClear);
            txtTenNhanVien.EditValue = Form1.nhanVien.Ten;
            btnXoa.Tag = null;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void grcLichSuDoiTien_DoubleClick(object sender, EventArgs e)
        {
            LichSuDoiNgoaiTe lichSuDoiNgoaiTe = (LichSuDoiNgoaiTe)grvLichSuDoiTien.GetFocusedRow();

            if (lichSuDoiNgoaiTe != null)
            {
                HienThiDuLieuLenForm(lichSuDoiNgoaiTe);

                btnXoa.Tag = lichSuDoiNgoaiTe;
            }
        }
    }
}