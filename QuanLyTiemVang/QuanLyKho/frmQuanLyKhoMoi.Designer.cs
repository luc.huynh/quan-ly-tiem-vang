﻿namespace QuanLyTiemVang
{
    partial class frmQuanLyKhoMoi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnReLoad = new DevExpress.XtraBars.BarButtonItem();
            this.btnInTemAll = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapKhoMoiExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnXuatFileExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnChuyen = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.lueKiHieu = new DevExpress.XtraEditors.LookUpEdit();
            this.txtGiaBan = new DevExpress.XtraEditors.TextEdit();
            this.btnInTem = new DevExpress.XtraEditors.SimpleButton();
            this.lueNhaCungCap = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lueHamLuongVang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtLyDa = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDa = new DevExpress.XtraEditors.TextEdit();
            this.txtChiDa = new DevExpress.XtraEditors.TextEdit();
            this.txtLyTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinh = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txtChiVang = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVang = new DevExpress.XtraEditors.TextEdit();
            this.txtMaTem = new DevExpress.XtraEditors.TextEdit();
            this.txtLyVang = new DevExpress.XtraEditors.TextEdit();
            this.txtTenSanPham = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHot = new DevExpress.XtraEditors.TextEdit();
            this.txtTienCong = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcSanPham = new DevExpress.XtraGrid.GridControl();
            this.sanPhamBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvSanPham = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTiLeHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGiaMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTienCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclMaTem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclMaChuyenKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayNhap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNhaCungCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbTongSoLuongSanPham = new System.Windows.Forms.Label();
            this.dxerTenSanPham = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerMaTem = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerKiHieu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyTinh = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyDa = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerNhaCungCap = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueKiHieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerMaTem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKiHieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNhaCungCap)).BeginInit();
            this.SuspendLayout();
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSua,
            this.btnXoa,
            this.btnReLoad,
            this.btnChuyen,
            this.btnInTemAll,
            this.btnNhapKhoMoiExcel,
            this.barEditItem1,
            this.btnXuatFileExcel});
            this.barManager1.MainMenu = this.bar3;
            this.barManager1.MaxItemId = 10;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnSua, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnReLoad, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInTemAll, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapKhoMoiExcel, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXuatFileExcel, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // btnSua
            // 
            this.btnSua.Caption = "Sửa";
            this.btnSua.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnSua.Id = 0;
            this.btnSua.Name = "btnSua";
            this.btnSua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSua_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 1;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // btnReLoad
            // 
            this.btnReLoad.Caption = "Làm mới dữ liệu";
            this.btnReLoad.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnReLoad.Id = 2;
            this.btnReLoad.Name = "btnReLoad";
            this.btnReLoad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInTemAll
            // 
            this.btnInTemAll.Caption = "In tem tất cả sản phẩm";
            this.btnInTemAll.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInTemAll.Id = 4;
            this.btnInTemAll.Name = "btnInTemAll";
            this.btnInTemAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInTemAll_ItemClick);
            // 
            // btnNhapKhoMoiExcel
            // 
            this.btnNhapKhoMoiExcel.Caption = "Nhập từ file excel";
            this.btnNhapKhoMoiExcel.Glyph = global::QuanLyTiemVang.Properties.Resources.file_extension_xls_icon;
            this.btnNhapKhoMoiExcel.Id = 5;
            this.btnNhapKhoMoiExcel.Name = "btnNhapKhoMoiExcel";
            this.btnNhapKhoMoiExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapKhoMoiExcel_ItemClick);
            // 
            // btnXuatFileExcel
            // 
            this.btnXuatFileExcel.Caption = "Xuất ra file excel";
            this.btnXuatFileExcel.Glyph = global::QuanLyTiemVang.Properties.Resources.netvibes_icon;
            this.btnXuatFileExcel.Id = 9;
            this.btnXuatFileExcel.Name = "btnXuatFileExcel";
            this.btnXuatFileExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatFileExcel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1370, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 715);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1370, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 665);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1370, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 665);
            // 
            // btnChuyen
            // 
            this.btnChuyen.Id = 8;
            this.btnChuyen.Name = "btnChuyen";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemTextEdit2;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // groupControl1
            // 
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.groupControl1.Controls.Add(this.layoutControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(278, 665);
            this.groupControl1.TabIndex = 30;
            this.groupControl1.Text = "Thông tin sản phẩm";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.lueKiHieu);
            this.layoutControl2.Controls.Add(this.txtGiaBan);
            this.layoutControl2.Controls.Add(this.btnInTem);
            this.layoutControl2.Controls.Add(this.lueNhaCungCap);
            this.layoutControl2.Controls.Add(this.label4);
            this.layoutControl2.Controls.Add(this.label3);
            this.layoutControl2.Controls.Add(this.label2);
            this.layoutControl2.Controls.Add(this.lueHamLuongVang);
            this.layoutControl2.Controls.Add(this.txtLyDa);
            this.layoutControl2.Controls.Add(this.txtPhanDa);
            this.layoutControl2.Controls.Add(this.txtChiDa);
            this.layoutControl2.Controls.Add(this.txtLyTinh);
            this.layoutControl2.Controls.Add(this.txtPhanTinh);
            this.layoutControl2.Controls.Add(this.txtChiTinh);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.txtChiVang);
            this.layoutControl2.Controls.Add(this.txtPhanVang);
            this.layoutControl2.Controls.Add(this.txtMaTem);
            this.layoutControl2.Controls.Add(this.txtLyVang);
            this.layoutControl2.Controls.Add(this.txtTenSanPham);
            this.layoutControl2.Controls.Add(this.txtTiLeHot);
            this.layoutControl2.Controls.Add(this.txtTienCong);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 25);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(274, 638);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // lueKiHieu
            // 
            this.lueKiHieu.Location = new System.Drawing.Point(106, 483);
            this.lueKiHieu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueKiHieu.MenuManager = this.barManager1;
            this.lueKiHieu.Name = "lueKiHieu";
            this.lueKiHieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKiHieu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("KiHieu", "Kí hiệu")});
            this.lueKiHieu.Properties.NullText = "";
            this.lueKiHieu.Size = new System.Drawing.Size(131, 22);
            this.lueKiHieu.StyleController = this.layoutControl2;
            this.lueKiHieu.TabIndex = 33;
            // 
            // txtGiaBan
            // 
            this.txtGiaBan.Location = new System.Drawing.Point(106, 427);
            this.txtGiaBan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGiaBan.MenuManager = this.barManager1;
            this.txtGiaBan.Name = "txtGiaBan";
            this.txtGiaBan.Properties.Mask.EditMask = "###,###,###,###,### vnd";
            this.txtGiaBan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaBan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGiaBan.Properties.NullText = "0";
            this.txtGiaBan.Size = new System.Drawing.Size(131, 22);
            this.txtGiaBan.StyleController = this.layoutControl2;
            this.txtGiaBan.TabIndex = 32;
            // 
            // btnInTem
            // 
            this.btnInTem.Image = global::QuanLyTiemVang.Properties.Resources.barcode_icon;
            this.btnInTem.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnInTem.Location = new System.Drawing.Point(16, 539);
            this.btnInTem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnInTem.Name = "btnInTem";
            this.btnInTem.Size = new System.Drawing.Size(221, 52);
            this.btnInTem.StyleController = this.layoutControl2;
            this.btnInTem.TabIndex = 31;
            this.btnInTem.Text = "In tem";
            this.btnInTem.Click += new System.EventHandler(this.btnInTem_Click);
            // 
            // lueNhaCungCap
            // 
            this.lueNhaCungCap.Location = new System.Drawing.Point(106, 511);
            this.lueNhaCungCap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueNhaCungCap.MenuManager = this.barManager1;
            this.lueNhaCungCap.Name = "lueNhaCungCap";
            this.lueNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNhaCungCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNhaCungCap", "Tên NCC"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DiaChi", "Địa chỉ")});
            this.lueNhaCungCap.Properties.NullText = "";
            this.lueNhaCungCap.Size = new System.Drawing.Size(131, 22);
            this.lueNhaCungCap.StyleController = this.layoutControl2;
            this.lueNhaCungCap.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 25);
            this.label4.TabIndex = 29;
            this.label4.Text = "Khối lượng vàng";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(221, 25);
            this.label3.TabIndex = 28;
            this.label3.Text = "Khối lượng đá";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 25);
            this.label2.TabIndex = 27;
            this.label2.Text = "Khối lượng tịnh";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lueHamLuongVang
            // 
            this.lueHamLuongVang.Location = new System.Drawing.Point(106, 26);
            this.lueHamLuongVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueHamLuongVang.MenuManager = this.barManager1;
            this.lueHamLuongVang.Name = "lueHamLuongVang";
            this.lueHamLuongVang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVang.Properties.NullText = "";
            this.lueHamLuongVang.Size = new System.Drawing.Size(131, 22);
            this.lueHamLuongVang.StyleController = this.layoutControl2;
            this.lueHamLuongVang.TabIndex = 26;
            // 
            // txtLyDa
            // 
            this.txtLyDa.Location = new System.Drawing.Point(106, 256);
            this.txtLyDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDa.MenuManager = this.barManager1;
            this.txtLyDa.Name = "txtLyDa";
            this.txtLyDa.Properties.NullText = "0";
            this.txtLyDa.Size = new System.Drawing.Size(131, 22);
            this.txtLyDa.StyleController = this.layoutControl2;
            this.txtLyDa.TabIndex = 25;
            this.txtLyDa.EditValueChanged += new System.EventHandler(this.txtLyDa_EditValueChanged);
            // 
            // txtPhanDa
            // 
            this.txtPhanDa.Location = new System.Drawing.Point(106, 228);
            this.txtPhanDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanDa.MenuManager = this.barManager1;
            this.txtPhanDa.Name = "txtPhanDa";
            this.txtPhanDa.Properties.Mask.EditMask = "d";
            this.txtPhanDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanDa.Properties.MaxLength = 1;
            this.txtPhanDa.Properties.NullText = "0";
            this.txtPhanDa.Size = new System.Drawing.Size(131, 22);
            this.txtPhanDa.StyleController = this.layoutControl2;
            this.txtPhanDa.TabIndex = 24;
            this.txtPhanDa.EditValueChanged += new System.EventHandler(this.txtPhanDa_EditValueChanged);
            // 
            // txtChiDa
            // 
            this.txtChiDa.Location = new System.Drawing.Point(106, 200);
            this.txtChiDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiDa.MenuManager = this.barManager1;
            this.txtChiDa.Name = "txtChiDa";
            this.txtChiDa.Properties.Mask.EditMask = "d";
            this.txtChiDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiDa.Properties.NullText = "0";
            this.txtChiDa.Size = new System.Drawing.Size(131, 22);
            this.txtChiDa.StyleController = this.layoutControl2;
            this.txtChiDa.TabIndex = 23;
            this.txtChiDa.EditValueChanged += new System.EventHandler(this.txtChiDa_EditValueChanged);
            // 
            // txtLyTinh
            // 
            this.txtLyTinh.Location = new System.Drawing.Point(106, 141);
            this.txtLyTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyTinh.MenuManager = this.barManager1;
            this.txtLyTinh.Name = "txtLyTinh";
            this.txtLyTinh.Properties.NullText = "0";
            this.txtLyTinh.Size = new System.Drawing.Size(131, 22);
            this.txtLyTinh.StyleController = this.layoutControl2;
            this.txtLyTinh.TabIndex = 22;
            this.txtLyTinh.EditValueChanged += new System.EventHandler(this.txtLyTinh_EditValueChanged);
            // 
            // txtPhanTinh
            // 
            this.txtPhanTinh.Location = new System.Drawing.Point(106, 113);
            this.txtPhanTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanTinh.MenuManager = this.barManager1;
            this.txtPhanTinh.Name = "txtPhanTinh";
            this.txtPhanTinh.Properties.Mask.EditMask = "d";
            this.txtPhanTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanTinh.Properties.MaxLength = 1;
            this.txtPhanTinh.Properties.NullText = "0";
            this.txtPhanTinh.Size = new System.Drawing.Size(131, 22);
            this.txtPhanTinh.StyleController = this.layoutControl2;
            this.txtPhanTinh.TabIndex = 21;
            this.txtPhanTinh.EditValueChanged += new System.EventHandler(this.txtPhanTinh_EditValueChanged);
            // 
            // txtChiTinh
            // 
            this.txtChiTinh.Location = new System.Drawing.Point(106, 85);
            this.txtChiTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiTinh.MenuManager = this.barManager1;
            this.txtChiTinh.Name = "txtChiTinh";
            this.txtChiTinh.Properties.Mask.EditMask = "d";
            this.txtChiTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiTinh.Properties.NullText = "0";
            this.txtChiTinh.Size = new System.Drawing.Size(131, 22);
            this.txtChiTinh.StyleController = this.layoutControl2;
            this.txtChiTinh.TabIndex = 20;
            this.txtChiTinh.EditValueChanged += new System.EventHandler(this.txtChiTinh_EditValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 597);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 25);
            this.label1.TabIndex = 16;
            // 
            // txtChiVang
            // 
            this.txtChiVang.Location = new System.Drawing.Point(106, 315);
            this.txtChiVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiVang.MenuManager = this.barManager1;
            this.txtChiVang.Name = "txtChiVang";
            this.txtChiVang.Properties.Mask.EditMask = "d";
            this.txtChiVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiVang.Properties.NullText = "0";
            this.txtChiVang.Properties.ReadOnly = true;
            this.txtChiVang.Size = new System.Drawing.Size(131, 22);
            this.txtChiVang.StyleController = this.layoutControl2;
            this.txtChiVang.TabIndex = 15;
            // 
            // txtPhanVang
            // 
            this.txtPhanVang.Location = new System.Drawing.Point(106, 343);
            this.txtPhanVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanVang.MenuManager = this.barManager1;
            this.txtPhanVang.Name = "txtPhanVang";
            this.txtPhanVang.Properties.Mask.EditMask = "d";
            this.txtPhanVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanVang.Properties.MaxLength = 1;
            this.txtPhanVang.Properties.NullText = "0";
            this.txtPhanVang.Properties.ReadOnly = true;
            this.txtPhanVang.Size = new System.Drawing.Size(131, 22);
            this.txtPhanVang.StyleController = this.layoutControl2;
            this.txtPhanVang.TabIndex = 14;
            // 
            // txtMaTem
            // 
            this.txtMaTem.Location = new System.Drawing.Point(106, -2);
            this.txtMaTem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMaTem.MenuManager = this.barManager1;
            this.txtMaTem.Name = "txtMaTem";
            this.txtMaTem.Properties.Mask.EditMask = "(\\p{Lu}|\\d)+";
            this.txtMaTem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMaTem.Size = new System.Drawing.Size(131, 22);
            this.txtMaTem.StyleController = this.layoutControl2;
            this.txtMaTem.TabIndex = 13;
            // 
            // txtLyVang
            // 
            this.txtLyVang.Location = new System.Drawing.Point(106, 371);
            this.txtLyVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyVang.MenuManager = this.barManager1;
            this.txtLyVang.Name = "txtLyVang";
            this.txtLyVang.Properties.NullText = "0";
            this.txtLyVang.Properties.ReadOnly = true;
            this.txtLyVang.Size = new System.Drawing.Size(131, 22);
            this.txtLyVang.StyleController = this.layoutControl2;
            this.txtLyVang.TabIndex = 12;
            // 
            // txtTenSanPham
            // 
            this.txtTenSanPham.Location = new System.Drawing.Point(106, -30);
            this.txtTenSanPham.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTenSanPham.Name = "txtTenSanPham";
            this.txtTenSanPham.Properties.Mask.EditMask = "f3";
            this.txtTenSanPham.Size = new System.Drawing.Size(131, 22);
            this.txtTenSanPham.StyleController = this.layoutControl2;
            this.txtTenSanPham.TabIndex = 0;
            // 
            // txtTiLeHot
            // 
            this.txtTiLeHot.Location = new System.Drawing.Point(106, 399);
            this.txtTiLeHot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTiLeHot.Name = "txtTiLeHot";
            this.txtTiLeHot.Properties.Mask.EditMask = "n3";
            this.txtTiLeHot.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTiLeHot.Properties.NullText = "0";
            this.txtTiLeHot.Size = new System.Drawing.Size(131, 22);
            this.txtTiLeHot.StyleController = this.layoutControl2;
            this.txtTiLeHot.TabIndex = 6;
            // 
            // txtTienCong
            // 
            this.txtTienCong.Location = new System.Drawing.Point(106, 455);
            this.txtTienCong.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTienCong.MenuManager = this.barManager1;
            this.txtTienCong.Name = "txtTienCong";
            this.txtTienCong.Properties.Mask.EditMask = "###,###,###,###,### vnd";
            this.txtTienCong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienCong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienCong.Properties.NullText = "0";
            this.txtTienCong.Size = new System.Drawing.Size(131, 22);
            this.txtTienCong.StyleController = this.layoutControl2;
            this.txtTienCong.TabIndex = 8;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem13,
            this.layoutControlItem5,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem15,
            this.layoutControlItem14,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem10,
            this.layoutControlItem18,
            this.layoutControlItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, -46);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(253, 684);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtTenSanPham;
            this.layoutControlItem1.CustomizationFormText = "Tên sản phẩm";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem1.Text = "Tên sản phẩm";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(87, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTiLeHot;
            this.layoutControlItem4.CustomizationFormText = "Tỉ lệ hột";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 429);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem4.Text = "Tỉ lệ hột";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(87, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTienCong;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 485);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem2.Text = "Tiền công";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(87, 17);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtMaTem;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem13.Text = "Mã tem";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.label1;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 627);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtChiTinh;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 115);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem19.Text = "Chỉ";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(87, 17);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtPhanTinh;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 143);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem20.Text = "Phân";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtLyTinh;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem21.Text = "Ly";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txtChiDa;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 230);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem22.Text = "Chỉ";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(87, 17);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtPhanDa;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 258);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem23.Text = "Phân";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtLyDa;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 286);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem24.Text = "Ly";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lueHamLuongVang;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem7.Text = "HLV";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.label2;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtChiVang;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 345);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem15.Text = "Chỉ";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(87, 17);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtPhanVang;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 373);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem14.Text = "Phân";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtLyVang;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 401);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem11.Text = "Ly";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.label3;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 199);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.label4;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.lueNhaCungCap;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 541);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem17.Text = "NCC";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnInTem;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 569);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(227, 58);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtGiaBan;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 457);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem18.Text = "Giá bán";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(87, 16);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.lueKiHieu;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 513);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(227, 28);
            this.layoutControlItem9.Text = "Kí hiệu";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(87, 17);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(278, 50);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 665);
            this.splitterControl1.TabIndex = 35;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcSanPham);
            this.groupControl2.Controls.Add(this.lbTongSoLuongSanPham);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(284, 50);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1086, 665);
            this.groupControl2.TabIndex = 37;
            this.groupControl2.Text = "Danh sách sản phẩm trong kho mới";
            // 
            // grcSanPham
            // 
            this.grcSanPham.DataSource = this.sanPhamBindingSource;
            this.grcSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcSanPham.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcSanPham.Location = new System.Drawing.Point(2, 25);
            this.grcSanPham.MainView = this.grvSanPham;
            this.grcSanPham.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcSanPham.MenuManager = this.barManager1;
            this.grcSanPham.Name = "grcSanPham";
            this.grcSanPham.Size = new System.Drawing.Size(1082, 638);
            this.grcSanPham.TabIndex = 24;
            this.grcSanPham.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPham});
            this.grcSanPham.Click += new System.EventHandler(this.gctChuyenKho_Click);
            // 
            // sanPhamBindingSource
            // 
            this.sanPhamBindingSource.DataSource = typeof(DTO.Models.SanPham);
            // 
            // grvSanPham
            // 
            this.grvSanPham.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclTenSanPham,
            this.grclHamLuongVang,
            this.grclTiLeHot,
            this.grclKhoiLuongTinh,
            this.grclKhoiLuongDa,
            this.grclKhoiLuongVang,
            this.grclGiaMua,
            this.grclTienCong,
            this.grclMaTem,
            this.grclMaChuyenKho,
            this.colNgayNhap,
            this.colKiHieu,
            this.colMaSanPham,
            this.colNhaCungCap});
            this.grvSanPham.CustomizationFormBounds = new System.Drawing.Rectangle(628, 255, 210, 172);
            this.grvSanPham.GridControl = this.grcSanPham;
            this.grvSanPham.Name = "grvSanPham";
            this.grvSanPham.OptionsBehavior.Editable = false;
            this.grvSanPham.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPham.OptionsFind.AlwaysVisible = true;
            this.grvSanPham.OptionsFind.FindDelay = 100;
            this.grvSanPham.OptionsFind.FindNullPrompt = "";
            this.grvSanPham.OptionsFind.ShowFindButton = false;
            this.grvSanPham.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.grvSanPham.OptionsView.ShowGroupPanel = false;
            this.grvSanPham.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvSanPham_CustomDrawRowIndicator);
            // 
            // grclTenSanPham
            // 
            this.grclTenSanPham.Caption = "Tên";
            this.grclTenSanPham.FieldName = "TenSanPham";
            this.grclTenSanPham.Name = "grclTenSanPham";
            this.grclTenSanPham.Visible = true;
            this.grclTenSanPham.VisibleIndex = 1;
            this.grclTenSanPham.Width = 84;
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "HLV";
            this.grclHamLuongVang.FieldName = "HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 6;
            // 
            // grclTiLeHot
            // 
            this.grclTiLeHot.Caption = "Tỉ lệ hột";
            this.grclTiLeHot.FieldName = "TiLeHot";
            this.grclTiLeHot.Name = "grclTiLeHot";
            this.grclTiLeHot.Visible = true;
            this.grclTiLeHot.VisibleIndex = 7;
            // 
            // grclKhoiLuongTinh
            // 
            this.grclKhoiLuongTinh.Caption = "KLT";
            this.grclKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.grclKhoiLuongTinh.Name = "grclKhoiLuongTinh";
            this.grclKhoiLuongTinh.Visible = true;
            this.grclKhoiLuongTinh.VisibleIndex = 8;
            // 
            // grclKhoiLuongDa
            // 
            this.grclKhoiLuongDa.Caption = "KLĐ";
            this.grclKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.grclKhoiLuongDa.Name = "grclKhoiLuongDa";
            this.grclKhoiLuongDa.Visible = true;
            this.grclKhoiLuongDa.VisibleIndex = 9;
            // 
            // grclKhoiLuongVang
            // 
            this.grclKhoiLuongVang.Caption = "KLV";
            this.grclKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.grclKhoiLuongVang.Name = "grclKhoiLuongVang";
            this.grclKhoiLuongVang.Visible = true;
            this.grclKhoiLuongVang.VisibleIndex = 10;
            // 
            // grclGiaMua
            // 
            this.grclGiaMua.Caption = "Giá bán";
            this.grclGiaMua.FieldName = "GiaBan";
            this.grclGiaMua.Name = "grclGiaMua";
            this.grclGiaMua.Visible = true;
            this.grclGiaMua.VisibleIndex = 3;
            // 
            // grclTienCong
            // 
            this.grclTienCong.Caption = "Tiền công";
            this.grclTienCong.FieldName = "TienCong";
            this.grclTienCong.Name = "grclTienCong";
            this.grclTienCong.Visible = true;
            this.grclTienCong.VisibleIndex = 4;
            // 
            // grclMaTem
            // 
            this.grclMaTem.Caption = "Mã tem";
            this.grclMaTem.FieldName = "MaTem";
            this.grclMaTem.Name = "grclMaTem";
            this.grclMaTem.Visible = true;
            this.grclMaTem.VisibleIndex = 2;
            // 
            // grclMaChuyenKho
            // 
            this.grclMaChuyenKho.Caption = "Mã chuyển kho";
            this.grclMaChuyenKho.FieldName = "MaChuyenKho";
            this.grclMaChuyenKho.Name = "grclMaChuyenKho";
            this.grclMaChuyenKho.Visible = true;
            this.grclMaChuyenKho.VisibleIndex = 13;
            // 
            // colNgayNhap
            // 
            this.colNgayNhap.Caption = "Ngày nhập";
            this.colNgayNhap.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayNhap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayNhap.FieldName = "NgayNhap";
            this.colNgayNhap.Name = "colNgayNhap";
            this.colNgayNhap.Visible = true;
            this.colNgayNhap.VisibleIndex = 5;
            // 
            // colKiHieu
            // 
            this.colKiHieu.Caption = "Kí hiệu";
            this.colKiHieu.FieldName = "KiHieu";
            this.colKiHieu.Name = "colKiHieu";
            this.colKiHieu.Visible = true;
            this.colKiHieu.VisibleIndex = 11;
            this.colKiHieu.Width = 56;
            // 
            // colMaSanPham
            // 
            this.colMaSanPham.Caption = "Mã SP";
            this.colMaSanPham.FieldName = "MaSanPham";
            this.colMaSanPham.Name = "colMaSanPham";
            this.colMaSanPham.Visible = true;
            this.colMaSanPham.VisibleIndex = 0;
            // 
            // colNhaCungCap
            // 
            this.colNhaCungCap.Caption = "NCC";
            this.colNhaCungCap.FieldName = "NhaCungCap.TenNhaCungCap";
            this.colNhaCungCap.Name = "colNhaCungCap";
            this.colNhaCungCap.Visible = true;
            this.colNhaCungCap.VisibleIndex = 12;
            this.colNhaCungCap.Width = 97;
            // 
            // lbTongSoLuongSanPham
            // 
            this.lbTongSoLuongSanPham.AutoSize = true;
            this.lbTongSoLuongSanPham.Location = new System.Drawing.Point(378, 4);
            this.lbTongSoLuongSanPham.Name = "lbTongSoLuongSanPham";
            this.lbTongSoLuongSanPham.Size = new System.Drawing.Size(42, 17);
            this.lbTongSoLuongSanPham.TabIndex = 25;
            this.lbTongSoLuongSanPham.Text = "label5";
            // 
            // dxerTenSanPham
            // 
            this.dxerTenSanPham.ContainerControl = this;
            // 
            // dxerMaTem
            // 
            this.dxerMaTem.ContainerControl = this;
            // 
            // dxerKiHieu
            // 
            this.dxerKiHieu.ContainerControl = this;
            // 
            // dxerLyTinh
            // 
            this.dxerLyTinh.ContainerControl = this;
            // 
            // dxerLyDa
            // 
            this.dxerLyDa.ContainerControl = this;
            // 
            // dxerLyVang
            // 
            this.dxerLyVang.ContainerControl = this;
            // 
            // dxerNhaCungCap
            // 
            this.dxerNhaCungCap.ContainerControl = this;
            // 
            // frmQuanLyKhoMoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 715);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmQuanLyKhoMoi";
            this.Text = "Quản lý kho mới";
            this.Load += new System.EventHandler(this.frmQuanLyKhoMoi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueKiHieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerMaTem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKiHieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNhaCungCap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem btnReLoad;
        private DevExpress.XtraBars.BarButtonItem btnChuyen;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit txtTienCong;
        private DevExpress.XtraEditors.TextEdit txtTenSanPham;
        private DevExpress.XtraEditors.TextEdit txtTiLeHot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcSanPham;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclGiaMua;
        private DevExpress.XtraGrid.Columns.GridColumn grclTiLeHot;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclTienCong;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaTem;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTenSanPham;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerMaTem;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKiHieu;
        private DevExpress.XtraBars.BarButtonItem btnInTemAll;
        private DevExpress.XtraEditors.TextEdit txtChiVang;
        private DevExpress.XtraEditors.TextEdit txtPhanVang;
        private DevExpress.XtraEditors.TextEdit txtMaTem;
        private DevExpress.XtraEditors.TextEdit txtLyVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit txtLyDa;
        private DevExpress.XtraEditors.TextEdit txtPhanDa;
        private DevExpress.XtraEditors.TextEdit txtChiDa;
        private DevExpress.XtraEditors.TextEdit txtLyTinh;
        private DevExpress.XtraEditors.TextEdit txtPhanTinh;
        private DevExpress.XtraEditors.TextEdit txtChiTinh;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraBars.BarButtonItem btnNhapKhoMoiExcel;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaChuyenKho;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyTinh;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyDa;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyVang;
        private DevExpress.XtraEditors.LookUpEdit lueNhaCungCap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SimpleButton btnInTem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.TextEdit txtGiaBan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.LookUpEdit lueKiHieu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.Label lbTongSoLuongSanPham;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarButtonItem btnXuatFileExcel;
        private System.Windows.Forms.BindingSource sanPhamBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayNhap;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieu;
        private DevExpress.XtraGrid.Columns.GridColumn colMaSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn colNhaCungCap;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNhaCungCap;
    }
}