﻿namespace QuanLyTiemVang
{
    partial class frmNhapKho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.lueKiHieu = new DevExpress.XtraEditors.LookUpEdit();
            this.txtKiHieuChuyenKho = new DevExpress.XtraEditors.TextEdit();
            this.lueNhaCungCap = new DevExpress.XtraEditors.LookUpEdit();
            this.txtSoLuong = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.lueHamLuongVang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtChiDa = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDa = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDa = new DevExpress.XtraEditors.TextEdit();
            this.txtLyTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinh = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaTem = new DevExpress.XtraEditors.TextEdit();
            this.txtLyVang = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVang = new DevExpress.XtraEditors.TextEdit();
            this.txtChiVang = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTienCong = new DevExpress.XtraEditors.TextEdit();
            this.txtTenSanPham = new DevExpress.XtraEditors.TextEdit();
            this.txtMoTa = new DevExpress.XtraEditors.TextEdit();
            this.txtTiLeHot = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnNhapKhoCu = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnInTem = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhapKhoMoiVaInTem = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhapKhoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.detNgayNhap = new DevExpress.XtraEditors.DateEdit();
            this.txtTenNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.barDockControl9 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl10 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl11 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl12 = new DevExpress.XtraBars.BarDockControl();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.barDockControl21 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl22 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl23 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl24 = new DevExpress.XtraBars.BarDockControl();
            this.dxerTenSanPham = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerMaTem = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerKiHieu = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerNgayNhapKho = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerHamLuongVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerKiHieuChuyenKho = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyTinh = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyDa = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerNhaCungCap = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.txtKhoiLuongDaCan = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtKhoiLuongTinhCan = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueKiHieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKiHieuChuyenKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoTa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerMaTem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKiHieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayNhapKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKiHieuChuyenKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNhaCungCap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaCan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTinhCan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 9;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar2.FloatSize = new System.Drawing.Size(198, 82);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1370, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 750);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1370, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 700);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1370, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 700);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 50);
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl1.Size = new System.Drawing.Size(0, 700);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1370, 50);
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl2.Size = new System.Drawing.Size(0, 700);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(0, 750);
            this.barDockControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl3.Size = new System.Drawing.Size(1370, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 50);
            this.barDockControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControl4.Size = new System.Drawing.Size(1370, 0);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(854, 700);
            this.groupControl1.TabIndex = 21;
            this.groupControl1.Text = "Thông tin sản phẩm";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.txtKhoiLuongTinhCan);
            this.layoutControl2.Controls.Add(this.txtKhoiLuongDaCan);
            this.layoutControl2.Controls.Add(this.lueKiHieu);
            this.layoutControl2.Controls.Add(this.txtKiHieuChuyenKho);
            this.layoutControl2.Controls.Add(this.lueNhaCungCap);
            this.layoutControl2.Controls.Add(this.txtSoLuong);
            this.layoutControl2.Controls.Add(this.label5);
            this.layoutControl2.Controls.Add(this.lueHamLuongVang);
            this.layoutControl2.Controls.Add(this.txtChiDa);
            this.layoutControl2.Controls.Add(this.txtLyDa);
            this.layoutControl2.Controls.Add(this.txtPhanDa);
            this.layoutControl2.Controls.Add(this.txtLyTinh);
            this.layoutControl2.Controls.Add(this.txtChiTinh);
            this.layoutControl2.Controls.Add(this.txtPhanTinh);
            this.layoutControl2.Controls.Add(this.label4);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.txtMaTem);
            this.layoutControl2.Controls.Add(this.txtLyVang);
            this.layoutControl2.Controls.Add(this.txtPhanVang);
            this.layoutControl2.Controls.Add(this.txtChiVang);
            this.layoutControl2.Controls.Add(this.label2);
            this.layoutControl2.Controls.Add(this.txtTienCong);
            this.layoutControl2.Controls.Add(this.txtTenSanPham);
            this.layoutControl2.Controls.Add(this.txtMoTa);
            this.layoutControl2.Controls.Add(this.txtTiLeHot);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 25);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(609, 501, 312, 437);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(850, 673);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // lueKiHieu
            // 
            this.lueKiHieu.Location = new System.Drawing.Point(137, 305);
            this.lueKiHieu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueKiHieu.MenuManager = this.barManager1;
            this.lueKiHieu.Name = "lueKiHieu";
            this.lueKiHieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKiHieu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("KiHieu", "Kí hiệu")});
            this.lueKiHieu.Properties.NullText = "";
            this.lueKiHieu.Size = new System.Drawing.Size(697, 22);
            this.lueKiHieu.StyleController = this.layoutControl2;
            this.lueKiHieu.TabIndex = 36;
            // 
            // txtKiHieuChuyenKho
            // 
            this.txtKiHieuChuyenKho.Location = new System.Drawing.Point(137, 72);
            this.txtKiHieuChuyenKho.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtKiHieuChuyenKho.MenuManager = this.barManager1;
            this.txtKiHieuChuyenKho.Name = "txtKiHieuChuyenKho";
            this.txtKiHieuChuyenKho.Size = new System.Drawing.Size(697, 22);
            this.txtKiHieuChuyenKho.StyleController = this.layoutControl2;
            this.txtKiHieuChuyenKho.TabIndex = 35;
            // 
            // lueNhaCungCap
            // 
            this.lueNhaCungCap.Location = new System.Drawing.Point(137, 333);
            this.lueNhaCungCap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueNhaCungCap.MenuManager = this.barManager1;
            this.lueNhaCungCap.Name = "lueNhaCungCap";
            this.lueNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNhaCungCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNhaCungCap", "Tên nhà cung cấp"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DiaChi", "Địa Chỉ")});
            this.lueNhaCungCap.Properties.NullText = "";
            this.lueNhaCungCap.Size = new System.Drawing.Size(697, 22);
            this.lueNhaCungCap.StyleController = this.layoutControl2;
            this.lueNhaCungCap.TabIndex = 34;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Location = new System.Drawing.Point(137, 389);
            this.txtSoLuong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSoLuong.MenuManager = this.barManager1;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Properties.Mask.EditMask = "d";
            this.txtSoLuong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoLuong.Properties.MaxLength = 2;
            this.txtSoLuong.Properties.NullText = "1";
            this.txtSoLuong.Size = new System.Drawing.Size(697, 22);
            this.txtSoLuong.StyleController = this.layoutControl2;
            this.txtSoLuong.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 25);
            this.label5.TabIndex = 32;
            this.label5.Text = "Khối lượng vàng";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lueHamLuongVang
            // 
            this.lueHamLuongVang.Location = new System.Drawing.Point(137, 100);
            this.lueHamLuongVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueHamLuongVang.MenuManager = this.barManager1;
            this.lueHamLuongVang.Name = "lueHamLuongVang";
            this.lueHamLuongVang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVang.Properties.NullText = "";
            this.lueHamLuongVang.Size = new System.Drawing.Size(697, 22);
            this.lueHamLuongVang.StyleController = this.layoutControl2;
            this.lueHamLuongVang.TabIndex = 31;
            // 
            // txtChiDa
            // 
            this.txtChiDa.Location = new System.Drawing.Point(305, 187);
            this.txtChiDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiDa.MenuManager = this.barManager1;
            this.txtChiDa.Name = "txtChiDa";
            this.txtChiDa.Properties.Mask.EditMask = "d";
            this.txtChiDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiDa.Properties.NullText = "0";
            this.txtChiDa.Size = new System.Drawing.Size(80, 22);
            this.txtChiDa.StyleController = this.layoutControl2;
            this.txtChiDa.TabIndex = 30;
            this.txtChiDa.EditValueChanged += new System.EventHandler(this.txtChiDa_EditValueChanged);
            // 
            // txtLyDa
            // 
            this.txtLyDa.Location = new System.Drawing.Point(738, 187);
            this.txtLyDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDa.MenuManager = this.barManager1;
            this.txtLyDa.Name = "txtLyDa";
            this.txtLyDa.Properties.NullText = "0";
            this.txtLyDa.Size = new System.Drawing.Size(96, 22);
            this.txtLyDa.StyleController = this.layoutControl2;
            this.txtLyDa.TabIndex = 29;
            this.txtLyDa.EditValueChanged += new System.EventHandler(this.txtLyDa_EditValueChanged);
            // 
            // txtPhanDa
            // 
            this.txtPhanDa.Location = new System.Drawing.Point(512, 187);
            this.txtPhanDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanDa.MenuManager = this.barManager1;
            this.txtPhanDa.Name = "txtPhanDa";
            this.txtPhanDa.Properties.Mask.EditMask = "d";
            this.txtPhanDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanDa.Properties.MaxLength = 1;
            this.txtPhanDa.Properties.NullText = "0";
            this.txtPhanDa.Size = new System.Drawing.Size(99, 22);
            this.txtPhanDa.StyleController = this.layoutControl2;
            this.txtPhanDa.TabIndex = 28;
            this.txtPhanDa.EditValueChanged += new System.EventHandler(this.txtPhanDa_EditValueChanged);
            // 
            // txtLyTinh
            // 
            this.txtLyTinh.Location = new System.Drawing.Point(738, 156);
            this.txtLyTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyTinh.MenuManager = this.barManager1;
            this.txtLyTinh.Name = "txtLyTinh";
            this.txtLyTinh.Properties.NullText = "0";
            this.txtLyTinh.Size = new System.Drawing.Size(96, 22);
            this.txtLyTinh.StyleController = this.layoutControl2;
            this.txtLyTinh.TabIndex = 27;
            this.txtLyTinh.EditValueChanged += new System.EventHandler(this.txtLyTinh_EditValueChanged);
            // 
            // txtChiTinh
            // 
            this.txtChiTinh.Location = new System.Drawing.Point(305, 156);
            this.txtChiTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiTinh.MenuManager = this.barManager1;
            this.txtChiTinh.Name = "txtChiTinh";
            this.txtChiTinh.Properties.Mask.EditMask = "d";
            this.txtChiTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiTinh.Properties.NullText = "0";
            this.txtChiTinh.Size = new System.Drawing.Size(80, 22);
            this.txtChiTinh.StyleController = this.layoutControl2;
            this.txtChiTinh.TabIndex = 26;
            this.txtChiTinh.EditValueChanged += new System.EventHandler(this.txtChiTinh_EditValueChanged);
            // 
            // txtPhanTinh
            // 
            this.txtPhanTinh.Location = new System.Drawing.Point(512, 156);
            this.txtPhanTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanTinh.MenuManager = this.barManager1;
            this.txtPhanTinh.Name = "txtPhanTinh";
            this.txtPhanTinh.Properties.Mask.EditMask = "d";
            this.txtPhanTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanTinh.Properties.MaxLength = 1;
            this.txtPhanTinh.Properties.NullText = "0";
            this.txtPhanTinh.Size = new System.Drawing.Size(99, 22);
            this.txtPhanTinh.StyleController = this.layoutControl2;
            this.txtPhanTinh.TabIndex = 25;
            this.txtPhanTinh.EditValueChanged += new System.EventHandler(this.txtPhanTinh_EditValueChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 25);
            this.label4.TabIndex = 24;
            this.label4.Text = "Khối lượng đá";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 25);
            this.label1.TabIndex = 23;
            this.label1.Text = "Khối lượng tịnh";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMaTem
            // 
            this.txtMaTem.Location = new System.Drawing.Point(137, 44);
            this.txtMaTem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMaTem.MenuManager = this.barManager1;
            this.txtMaTem.Name = "txtMaTem";
            this.txtMaTem.Properties.Mask.EditMask = "(\\p{Lu}|\\d)+";
            this.txtMaTem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMaTem.Size = new System.Drawing.Size(697, 22);
            this.txtMaTem.StyleController = this.layoutControl2;
            this.txtMaTem.TabIndex = 22;
            // 
            // txtLyVang
            // 
            this.txtLyVang.Location = new System.Drawing.Point(738, 218);
            this.txtLyVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyVang.MenuManager = this.barManager1;
            this.txtLyVang.Name = "txtLyVang";
            this.txtLyVang.Properties.NullText = "0";
            this.txtLyVang.Properties.ReadOnly = true;
            this.txtLyVang.Size = new System.Drawing.Size(96, 22);
            this.txtLyVang.StyleController = this.layoutControl2;
            this.txtLyVang.TabIndex = 21;
            // 
            // txtPhanVang
            // 
            this.txtPhanVang.Location = new System.Drawing.Point(511, 218);
            this.txtPhanVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanVang.MenuManager = this.barManager1;
            this.txtPhanVang.Name = "txtPhanVang";
            this.txtPhanVang.Properties.Mask.EditMask = "d";
            this.txtPhanVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanVang.Properties.MaxLength = 1;
            this.txtPhanVang.Properties.NullText = "0";
            this.txtPhanVang.Properties.ReadOnly = true;
            this.txtPhanVang.Size = new System.Drawing.Size(100, 22);
            this.txtPhanVang.StyleController = this.layoutControl2;
            this.txtPhanVang.TabIndex = 20;
            // 
            // txtChiVang
            // 
            this.txtChiVang.Location = new System.Drawing.Point(305, 218);
            this.txtChiVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiVang.MenuManager = this.barManager1;
            this.txtChiVang.Name = "txtChiVang";
            this.txtChiVang.Properties.Mask.EditMask = "d";
            this.txtChiVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiVang.Properties.NullText = "0";
            this.txtChiVang.Properties.ReadOnly = true;
            this.txtChiVang.Size = new System.Drawing.Size(79, 22);
            this.txtChiVang.StyleController = this.layoutControl2;
            this.txtChiVang.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 417);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(818, 240);
            this.label2.TabIndex = 17;
            // 
            // txtTienCong
            // 
            this.txtTienCong.Location = new System.Drawing.Point(137, 277);
            this.txtTienCong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTienCong.MenuManager = this.barManager1;
            this.txtTienCong.Name = "txtTienCong";
            this.txtTienCong.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienCong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienCong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienCong.Properties.NullText = "0";
            this.txtTienCong.Size = new System.Drawing.Size(697, 22);
            this.txtTienCong.StyleController = this.layoutControl2;
            this.txtTienCong.TabIndex = 16;
            // 
            // txtTenSanPham
            // 
            this.txtTenSanPham.Location = new System.Drawing.Point(137, 16);
            this.txtTenSanPham.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTenSanPham.Name = "txtTenSanPham";
            this.txtTenSanPham.Size = new System.Drawing.Size(697, 22);
            this.txtTenSanPham.StyleController = this.layoutControl2;
            this.txtTenSanPham.TabIndex = 6;
            // 
            // txtMoTa
            // 
            this.txtMoTa.Location = new System.Drawing.Point(137, 361);
            this.txtMoTa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMoTa.Name = "txtMoTa";
            this.txtMoTa.Size = new System.Drawing.Size(697, 22);
            this.txtMoTa.StyleController = this.layoutControl2;
            this.txtMoTa.TabIndex = 5;
            // 
            // txtTiLeHot
            // 
            this.txtTiLeHot.Location = new System.Drawing.Point(137, 249);
            this.txtTiLeHot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTiLeHot.Name = "txtTiLeHot";
            this.txtTiLeHot.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTiLeHot.Properties.NullText = "0";
            this.txtTiLeHot.Size = new System.Drawing.Size(697, 22);
            this.txtTiLeHot.StyleController = this.layoutControl2;
            this.txtTiLeHot.TabIndex = 8;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem16,
            this.layoutControlItem33,
            this.layoutControlItem32,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem35,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem34,
            this.layoutControlItem10,
            this.layoutControlItem6,
            this.layoutControlItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(850, 673);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtTenSanPham;
            this.layoutControlItem1.CustomizationFormText = "Tên sản phẩm";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem1.Text = "Tên sản phẩm";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMoTa;
            this.layoutControlItem3.CustomizationFormText = "Mô tả (nếu có)";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 345);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem3.Text = "Mô tả";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtTienCong;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 261);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem16.Text = "Tiền công";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.label2;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 401);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(824, 246);
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txtMaTem;
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem32.Text = "Mã tem";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.label1;
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(168, 31);
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.label4;
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(168, 31);
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.txtPhanTinh;
            this.layoutControlItem40.Location = new System.Drawing.Point(375, 140);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(226, 31);
            this.layoutControlItem40.Text = "Phân";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.txtChiTinh;
            this.layoutControlItem41.Location = new System.Drawing.Point(168, 140);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(207, 31);
            this.layoutControlItem41.Text = "Chỉ";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.txtLyTinh;
            this.layoutControlItem42.Location = new System.Drawing.Point(601, 140);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(223, 31);
            this.layoutControlItem42.Text = "Ly";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.txtPhanDa;
            this.layoutControlItem43.Location = new System.Drawing.Point(375, 171);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(226, 31);
            this.layoutControlItem43.Text = "Phân";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.txtLyDa;
            this.layoutControlItem44.Location = new System.Drawing.Point(601, 171);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(223, 31);
            this.layoutControlItem44.Text = "Ly";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.txtChiDa;
            this.layoutControlItem45.Location = new System.Drawing.Point(168, 171);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(207, 31);
            this.layoutControlItem45.Text = "Chỉ";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lueHamLuongVang;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem7.Text = "Hàm lượng vàng";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTiLeHot;
            this.layoutControlItem4.CustomizationFormText = "Tỉ lệ hột";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 233);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem4.Text = "Tỉ lệ hột";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.label5;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(168, 31);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.txtPhanVang;
            this.layoutControlItem36.Location = new System.Drawing.Point(374, 202);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem36.Text = "Phân";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.txtLyVang;
            this.layoutControlItem37.Location = new System.Drawing.Point(601, 202);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(223, 31);
            this.layoutControlItem37.Text = "Ly";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(118, 16);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.txtChiVang;
            this.layoutControlItem35.Location = new System.Drawing.Point(168, 202);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(206, 31);
            this.layoutControlItem35.Text = "Chỉ";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtSoLuong;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 373);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem12.Text = "Số lượng";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.lueNhaCungCap;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 317);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem13.Text = "Nhà cung cấp";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.txtKiHieuChuyenKho;
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem34.Text = "Kí hiệu chuyển kho";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(118, 17);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.lueKiHieu;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 289);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(824, 28);
            this.layoutControlItem10.Text = "Kí hiệu";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(118, 17);
            // 
            // btnNhapKhoCu
            // 
            this.btnNhapKhoCu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNhapKhoCu.Location = new System.Drawing.Point(16, 72);
            this.btnNhapKhoCu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNhapKhoCu.Name = "btnNhapKhoCu";
            this.btnNhapKhoCu.Size = new System.Drawing.Size(474, 27);
            this.btnNhapKhoCu.StyleController = this.layoutControl1;
            this.btnNhapKhoCu.TabIndex = 16;
            this.btnNhapKhoCu.Text = "Nhập vào kho cũ";
            this.btnNhapKhoCu.Click += new System.EventHandler(this.btnNhapKhoCu_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnInTem);
            this.layoutControl1.Controls.Add(this.btnNhapKhoMoiVaInTem);
            this.layoutControl1.Controls.Add(this.btnNhapKhoCu);
            this.layoutControl1.Controls.Add(this.btnNhapKhoMoi);
            this.layoutControl1.Controls.Add(this.detNgayNhap);
            this.layoutControl1.Controls.Add(this.txtTenNhanVien);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(506, 673);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnInTem
            // 
            this.btnInTem.Location = new System.Drawing.Point(16, 171);
            this.btnInTem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnInTem.Name = "btnInTem";
            this.btnInTem.Size = new System.Drawing.Size(474, 27);
            this.btnInTem.StyleController = this.layoutControl1;
            this.btnInTem.TabIndex = 19;
            this.btnInTem.Text = "In tem";
            this.btnInTem.Click += new System.EventHandler(this.btnInTem_Click);
            // 
            // btnNhapKhoMoiVaInTem
            // 
            this.btnNhapKhoMoiVaInTem.Location = new System.Drawing.Point(16, 138);
            this.btnNhapKhoMoiVaInTem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNhapKhoMoiVaInTem.Name = "btnNhapKhoMoiVaInTem";
            this.btnNhapKhoMoiVaInTem.Size = new System.Drawing.Size(474, 27);
            this.btnNhapKhoMoiVaInTem.StyleController = this.layoutControl1;
            this.btnNhapKhoMoiVaInTem.TabIndex = 18;
            this.btnNhapKhoMoiVaInTem.Text = "Nhập kho mới và in tem";
            this.btnNhapKhoMoiVaInTem.Click += new System.EventHandler(this.btnNhapKhoMoiVaInTem_Click);
            // 
            // btnNhapKhoMoi
            // 
            this.btnNhapKhoMoi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNhapKhoMoi.Location = new System.Drawing.Point(16, 105);
            this.btnNhapKhoMoi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNhapKhoMoi.Name = "btnNhapKhoMoi";
            this.btnNhapKhoMoi.Size = new System.Drawing.Size(474, 27);
            this.btnNhapKhoMoi.StyleController = this.layoutControl1;
            this.btnNhapKhoMoi.TabIndex = 17;
            this.btnNhapKhoMoi.Text = "Nhập kho mới";
            this.btnNhapKhoMoi.Click += new System.EventHandler(this.btnNhapKhoMoi_Click);
            // 
            // detNgayNhap
            // 
            this.detNgayNhap.EditValue = null;
            this.detNgayNhap.Location = new System.Drawing.Point(86, 16);
            this.detNgayNhap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detNgayNhap.Name = "detNgayNhap";
            this.detNgayNhap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayNhap.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayNhap.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.detNgayNhap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayNhap.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.detNgayNhap.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayNhap.Properties.ReadOnly = true;
            this.detNgayNhap.Size = new System.Drawing.Size(404, 22);
            this.detNgayNhap.StyleController = this.layoutControl1;
            this.detNgayNhap.TabIndex = 10;
            // 
            // txtTenNhanVien
            // 
            this.txtTenNhanVien.Location = new System.Drawing.Point(86, 44);
            this.txtTenNhanVien.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTenNhanVien.MenuManager = this.barManager1;
            this.txtTenNhanVien.Name = "txtTenNhanVien";
            this.txtTenNhanVien.Properties.ReadOnly = true;
            this.txtTenNhanVien.Size = new System.Drawing.Size(404, 22);
            this.txtTenNhanVien.StyleController = this.layoutControl1;
            this.txtTenNhanVien.TabIndex = 4;
            this.txtTenNhanVien.Visible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem23,
            this.layoutControlItem14,
            this.layoutControlItem30,
            this.layoutControlItem5,
            this.layoutControlItem31});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(506, 673);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.detNgayNhap;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(480, 28);
            this.layoutControlItem11.Text = "Ngày nhập";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(67, 17);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnNhapKhoMoi;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(480, 33);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtTenNhanVien;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(480, 28);
            this.layoutControlItem14.Text = "Nhân viên";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.btnNhapKhoCu;
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(480, 33);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnInTem;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(480, 492);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.btnNhapKhoMoiVaInTem;
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(480, 33);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.layoutControl3);
            this.groupControl2.Location = new System.Drawing.Point(55, 202);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(421, 181);
            this.groupControl2.TabIndex = 21;
            this.groupControl2.Text = "groupControl1";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 25);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup4;
            this.layoutControl3.Size = new System.Drawing.Size(417, 154);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl2";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup2";
            this.layoutControlGroup4.Size = new System.Drawing.Size(417, 154);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(319, 216);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(252, 119);
            this.gridControl1.TabIndex = 12;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tên sản phẩm";
            this.gridColumn1.FieldName = "TenSanPham";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Khối lượng vàng";
            this.gridColumn2.FieldName = "KhoiLuongVang";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Hàm lượng vàng";
            this.gridColumn3.FieldName = "HamLuongVang";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Giá mua";
            this.gridColumn4.FieldName = "GiaMua";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Tỉ lệ hột";
            this.gridColumn5.FieldName = "TiLeHot";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Ngày nhập";
            this.gridColumn6.FieldName = "NgayNhap";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Mô tả";
            this.gridColumn7.FieldName = "MoTa";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl4.Location = new System.Drawing.Point(0, 32);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup5;
            this.layoutControl4.Size = new System.Drawing.Size(618, 164);
            this.layoutControl4.TabIndex = 11;
            this.layoutControl4.Text = "layoutControl1";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "Root";
            this.layoutControlGroup5.Size = new System.Drawing.Size(618, 164);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // barDockControl9
            // 
            this.barDockControl9.CausesValidation = false;
            this.barDockControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl9.Location = new System.Drawing.Point(0, 32);
            this.barDockControl9.Size = new System.Drawing.Size(0, 303);
            // 
            // barDockControl10
            // 
            this.barDockControl10.CausesValidation = false;
            this.barDockControl10.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl10.Location = new System.Drawing.Point(618, 32);
            this.barDockControl10.Size = new System.Drawing.Size(0, 303);
            // 
            // barDockControl11
            // 
            this.barDockControl11.CausesValidation = false;
            this.barDockControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl11.Location = new System.Drawing.Point(0, 335);
            this.barDockControl11.Size = new System.Drawing.Size(618, 0);
            // 
            // barDockControl12
            // 
            this.barDockControl12.CausesValidation = false;
            this.barDockControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl12.Location = new System.Drawing.Point(0, 32);
            this.barDockControl12.Size = new System.Drawing.Size(618, 0);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(854, 50);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 700);
            this.splitterControl1.TabIndex = 30;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.layoutControl1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(860, 50);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(510, 700);
            this.groupControl4.TabIndex = 44;
            this.groupControl4.Text = "Thông tin nhập kho";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.layoutControl5);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl5.Location = new System.Drawing.Point(255, 32);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(818, 385);
            this.groupControl5.TabIndex = 44;
            this.groupControl5.Text = "groupControl4";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.lookUpEdit2);
            this.layoutControl5.Controls.Add(this.groupControl6);
            this.layoutControl5.Controls.Add(this.textEdit2);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(2, 25);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup6;
            this.layoutControl5.Size = new System.Drawing.Size(814, 358);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl1";
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.Location = new System.Drawing.Point(134, 44);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Size = new System.Drawing.Size(664, 22);
            this.lookUpEdit2.StyleController = this.layoutControl5;
            this.lookUpEdit2.TabIndex = 33;
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.gridControl2);
            this.groupControl6.Location = new System.Drawing.Point(16, 72);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(782, 270);
            this.groupControl6.TabIndex = 31;
            this.groupControl6.Text = "Danh sach sản phẩm nhập kho";
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 25);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(778, 243);
            this.gridControl2.TabIndex = 12;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Tên sản phẩm";
            this.gridColumn8.FieldName = "TenSanPham";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Khối lượng vàng";
            this.gridColumn9.FieldName = "KhoiLuongVang";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Hàm lượng vàng";
            this.gridColumn10.FieldName = "HamLuongVang";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Khối lượng đá";
            this.gridColumn11.FieldName = "KhoiLuongDa";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Khối lượng tính";
            this.gridColumn12.FieldName = "KhoiLuongTinh";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Tỉ lệ hột";
            this.gridColumn13.FieldName = "TiLeHot";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Giá mua";
            this.gridColumn14.FieldName = "GiaMua";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 6;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Kí Hiêu";
            this.gridColumn15.FieldName = "KiHieu";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 7;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Nhà cung cấp";
            this.gridColumn16.FieldName = "NhaCungCap";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 8;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ngày nhập";
            this.gridColumn17.FieldName = "NgayNhap";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 9;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Mô tả";
            this.gridColumn18.FieldName = "MoTa";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 10;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(134, 16);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(664, 22);
            this.textEdit2.StyleController = this.layoutControl5;
            this.textEdit2.TabIndex = 4;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem15,
            this.layoutControlItem17});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup1";
            this.layoutControlGroup6.Size = new System.Drawing.Size(814, 358);
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit2;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem14";
            this.layoutControlItem2.Size = new System.Drawing.Size(788, 28);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.groupControl6;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem15.Name = "layoutControlItem11";
            this.layoutControlItem15.Size = new System.Drawing.Size(788, 276);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.lookUpEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem17.Name = "layoutControlItem16";
            this.layoutControlItem17.Size = new System.Drawing.Size(788, 28);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(115, 16);
            // 
            // splitterControl2
            // 
            this.splitterControl2.Location = new System.Drawing.Point(250, 32);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(6, 385);
            this.splitterControl2.TabIndex = 30;
            this.splitterControl2.TabStop = false;
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.layoutControl6);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl7.Location = new System.Drawing.Point(0, 32);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(250, 385);
            this.groupControl7.TabIndex = 21;
            this.groupControl7.Text = "Thông tin sản phẩm";
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.simpleButton1);
            this.layoutControl6.Controls.Add(this.textEdit3);
            this.layoutControl6.Controls.Add(this.textEdit4);
            this.layoutControl6.Controls.Add(this.textEdit5);
            this.layoutControl6.Controls.Add(this.textEdit6);
            this.layoutControl6.Controls.Add(this.textEdit7);
            this.layoutControl6.Controls.Add(this.textEdit8);
            this.layoutControl6.Controls.Add(this.textEdit9);
            this.layoutControl6.Controls.Add(this.textEdit10);
            this.layoutControl6.Controls.Add(this.textEdit11);
            this.layoutControl6.Controls.Add(this.textEdit12);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(2, 25);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.Root = this.layoutControlGroup7;
            this.layoutControl6.Size = new System.Drawing.Size(246, 358);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl2";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(16, 296);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(214, 27);
            this.simpleButton1.StyleController = this.layoutControl6;
            this.simpleButton1.TabIndex = 16;
            this.simpleButton1.Text = "Nhập Kho";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(121, 72);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Mask.EditMask = "n3";
            this.textEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit3.Size = new System.Drawing.Size(109, 22);
            this.textEdit3.StyleController = this.layoutControl6;
            this.textEdit3.TabIndex = 4;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(121, 16);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Mask.EditMask = "f3";
            this.textEdit4.Size = new System.Drawing.Size(109, 22);
            this.textEdit4.StyleController = this.layoutControl6;
            this.textEdit4.TabIndex = 6;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(121, 268);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(109, 22);
            this.textEdit5.StyleController = this.layoutControl6;
            this.textEdit5.TabIndex = 5;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(121, 156);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Mask.EditMask = "n3";
            this.textEdit6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit6.Size = new System.Drawing.Size(109, 22);
            this.textEdit6.StyleController = this.layoutControl6;
            this.textEdit6.TabIndex = 8;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(121, 44);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Mask.EditMask = "n3";
            this.textEdit7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit7.Size = new System.Drawing.Size(109, 22);
            this.textEdit7.StyleController = this.layoutControl6;
            this.textEdit7.TabIndex = 7;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(121, 184);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(109, 22);
            this.textEdit8.StyleController = this.layoutControl6;
            this.textEdit8.TabIndex = 9;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(121, 100);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(109, 22);
            this.textEdit9.StyleController = this.layoutControl6;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(121, 128);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(109, 22);
            this.textEdit10.StyleController = this.layoutControl6;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(121, 212);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(109, 22);
            this.textEdit11.StyleController = this.layoutControl6;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(121, 240);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(109, 22);
            this.textEdit12.StyleController = this.layoutControl6;
            this.textEdit12.TabIndex = 14;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "Root";
            this.layoutControlGroup7.Size = new System.Drawing.Size(246, 358);
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.CustomizationFormText = "Hàm lượng vàng";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem18.Name = "layoutControlItem12";
            this.layoutControlItem18.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem18.Text = "Hàm lượng vàng";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.CustomizationFormText = "Tên sản phẩm";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem1";
            this.layoutControlItem19.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem19.Text = "Tên sản phẩm";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit7;
            this.layoutControlItem20.CustomizationFormText = "Khối lượng vàng";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem20.Name = "layoutControlItem5";
            this.layoutControlItem20.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem20.Text = "Khối lượng vàng";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit9;
            this.layoutControlItem21.CustomizationFormText = "Khối lượng tính";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem21.Name = "layoutControlItem7";
            this.layoutControlItem21.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem21.Text = "Khối lượng tính";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit11;
            this.layoutControlItem22.CustomizationFormText = "Kí hiệu";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem22.Name = "layoutControlItem9";
            this.layoutControlItem22.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem22.Text = "Kí hiệu";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.textEdit12;
            this.layoutControlItem24.CustomizationFormText = "Nhà cung cấp";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 224);
            this.layoutControlItem24.Name = "layoutControlItem10";
            this.layoutControlItem24.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem24.Text = "Nhà cung cấp";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.textEdit5;
            this.layoutControlItem25.CustomizationFormText = "Mô tả (nếu có)";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 252);
            this.layoutControlItem25.Name = "layoutControlItem3";
            this.layoutControlItem25.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem25.Text = "Mô tả (nếu có)";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.simpleButton1;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 280);
            this.layoutControlItem26.Name = "layoutControlItem13";
            this.layoutControlItem26.Size = new System.Drawing.Size(220, 52);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.textEdit10;
            this.layoutControlItem27.CustomizationFormText = "Khối lượng đá";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem27.Name = "layoutControlItem8";
            this.layoutControlItem27.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem27.Text = "Khối lượng đá";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit6;
            this.layoutControlItem28.CustomizationFormText = "Tỉ lệ hột";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem28.Name = "layoutControlItem4";
            this.layoutControlItem28.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem28.Text = "Tỉ lệ hột";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(102, 17);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit8;
            this.layoutControlItem29.CustomizationFormText = "Giá mua";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem29.Name = "layoutControlItem6";
            this.layoutControlItem29.Size = new System.Drawing.Size(220, 28);
            this.layoutControlItem29.Text = "Giá mua";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(102, 16);
            // 
            // barDockControl21
            // 
            this.barDockControl21.CausesValidation = false;
            this.barDockControl21.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl21.Location = new System.Drawing.Point(0, 32);
            this.barDockControl21.Size = new System.Drawing.Size(0, 385);
            // 
            // barDockControl22
            // 
            this.barDockControl22.CausesValidation = false;
            this.barDockControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl22.Location = new System.Drawing.Point(1073, 32);
            this.barDockControl22.Size = new System.Drawing.Size(0, 385);
            // 
            // barDockControl23
            // 
            this.barDockControl23.CausesValidation = false;
            this.barDockControl23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl23.Location = new System.Drawing.Point(0, 417);
            this.barDockControl23.Size = new System.Drawing.Size(1073, 0);
            // 
            // barDockControl24
            // 
            this.barDockControl24.CausesValidation = false;
            this.barDockControl24.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl24.Location = new System.Drawing.Point(0, 32);
            this.barDockControl24.Size = new System.Drawing.Size(1073, 0);
            // 
            // dxerTenSanPham
            // 
            this.dxerTenSanPham.ContainerControl = this;
            // 
            // dxerMaTem
            // 
            this.dxerMaTem.ContainerControl = this;
            // 
            // dxerKiHieu
            // 
            this.dxerKiHieu.ContainerControl = this;
            // 
            // dxerNgayNhapKho
            // 
            this.dxerNgayNhapKho.ContainerControl = this;
            // 
            // dxerHamLuongVang
            // 
            this.dxerHamLuongVang.ContainerControl = this;
            // 
            // dxerKiHieuChuyenKho
            // 
            this.dxerKiHieuChuyenKho.ContainerControl = this;
            // 
            // dxerLyTinh
            // 
            this.dxerLyTinh.ContainerControl = this;
            // 
            // dxerLyDa
            // 
            this.dxerLyDa.ContainerControl = this;
            // 
            // dxerLyVang
            // 
            this.dxerLyVang.ContainerControl = this;
            // 
            // dxerNhaCungCap
            // 
            this.dxerNhaCungCap.ContainerControl = this;
            // 
            // txtKhoiLuongDaCan
            // 
            this.txtKhoiLuongDaCan.Location = new System.Drawing.Point(549, 128);
            this.txtKhoiLuongDaCan.MenuManager = this.barManager1;
            this.txtKhoiLuongDaCan.Name = "txtKhoiLuongDaCan";
            this.txtKhoiLuongDaCan.Size = new System.Drawing.Size(285, 22);
            this.txtKhoiLuongDaCan.StyleController = this.layoutControl2;
            this.txtKhoiLuongDaCan.TabIndex = 37;
            this.txtKhoiLuongDaCan.EditValueChanged += new System.EventHandler(this.txtKhoiLuongDaCan_EditValueChanged);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtKhoiLuongDaCan;
            this.layoutControlItem6.Location = new System.Drawing.Point(412, 112);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(412, 28);
            this.layoutControlItem6.Text = "Khối lượng đá cân";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(118, 17);
            // 
            // txtKhoiLuongTinhCan
            // 
            this.txtKhoiLuongTinhCan.Location = new System.Drawing.Point(137, 128);
            this.txtKhoiLuongTinhCan.MenuManager = this.barManager1;
            this.txtKhoiLuongTinhCan.Name = "txtKhoiLuongTinhCan";
            this.txtKhoiLuongTinhCan.Size = new System.Drawing.Size(285, 22);
            this.txtKhoiLuongTinhCan.StyleController = this.layoutControl2;
            this.txtKhoiLuongTinhCan.TabIndex = 38;
            this.txtKhoiLuongTinhCan.EditValueChanged += new System.EventHandler(this.txtKhoiLuongTinhCan_EditValueChanged);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtKhoiLuongTinhCan;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(412, 28);
            this.layoutControlItem9.Text = "Khối lương tịnh cân";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(118, 17);
            // 
            // frmNhapKho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 750);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmNhapKho";
            this.Text = "Nhập kho";
            this.Load += new System.EventHandler(this.frmNhapKho_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueKiHieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKiHieuChuyenKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenSanPham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoTa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiLeHot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerMaTem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKiHieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNgayNhapKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerKiHieuChuyenKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerNhaCungCap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongDaCan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoiLuongTinhCan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.TextEdit txtTenSanPham;
        private DevExpress.XtraEditors.TextEdit txtMoTa;
        private DevExpress.XtraEditors.TextEdit txtTiLeHot;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraBars.BarDockControl barDockControl9;
        private DevExpress.XtraBars.BarDockControl barDockControl10;
        private DevExpress.XtraBars.BarDockControl barDockControl11;
        private DevExpress.XtraBars.BarDockControl barDockControl12;
        private DevExpress.XtraEditors.SimpleButton btnNhapKhoCu;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTenNhanVien;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit detNgayNhap;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraBars.BarDockControl barDockControl21;
        private DevExpress.XtraBars.BarDockControl barDockControl22;
        private DevExpress.XtraBars.BarDockControl barDockControl23;
        private DevExpress.XtraBars.BarDockControl barDockControl24;
        private DevExpress.XtraEditors.SimpleButton btnNhapKhoMoi;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTenSanPham;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerMaTem;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKiHieu;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNgayNhapKho;
        private DevExpress.XtraEditors.TextEdit txtTienCong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraEditors.SimpleButton btnInTem;
        private DevExpress.XtraEditors.SimpleButton btnNhapKhoMoiVaInTem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.TextEdit txtLyVang;
        private DevExpress.XtraEditors.TextEdit txtPhanVang;
        private DevExpress.XtraEditors.TextEdit txtChiVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.TextEdit txtMaTem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.TextEdit txtChiDa;
        private DevExpress.XtraEditors.TextEdit txtLyDa;
        private DevExpress.XtraEditors.TextEdit txtPhanDa;
        private DevExpress.XtraEditors.TextEdit txtLyTinh;
        private DevExpress.XtraEditors.TextEdit txtChiTinh;
        private DevExpress.XtraEditors.TextEdit txtPhanTinh;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHamLuongVang;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtSoLuong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.LookUpEdit lueNhaCungCap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txtKiHieuChuyenKho;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerKiHieuChuyenKho;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyTinh;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyDa;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyVang;
        private DevExpress.XtraEditors.LookUpEdit lueKiHieu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerNhaCungCap;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongTinhCan;
        private DevExpress.XtraEditors.TextEdit txtKhoiLuongDaCan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}