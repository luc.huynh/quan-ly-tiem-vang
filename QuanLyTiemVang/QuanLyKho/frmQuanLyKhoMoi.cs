﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.InTem;
using DevExpress.XtraReports.UI;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants.StringUltils;
using DTO.TemporaryModel;
using QuanLyDieuXe.DialogCustom;
using QuanLyTiemVang.Constants;
using Share.Util;

namespace QuanLyTiemVang
{
    public partial class frmQuanLyKhoMoi : DevExpress.XtraEditors.XtraForm
    {
        List<SanPham> listSanPhamChuaBan = new List<SanPham>(); // Danh sách tất cả các sản phẩm chưa bán

        List<SanPham> listSanPhamTimKiem = new List<SanPham>(); // Danh sách các sản phẩm chưa bán thõa điều kiện tìm kiếm

        public frmQuanLyKhoMoi()
        {
            InitializeComponent();
            grvSanPham.IndicatorWidth = 70;
        }

        private void frmQuanLyKhoMoi_Load(object sender, EventArgs e)
        {
            SetDataSourceForAllGridControl();
            LoadData();
        }

        private void SetDataSourceForAllGridControl()
        {
            grcSanPham.DataSource = listSanPhamChuaBan;
        }

        private void LoadData()
        {
            KiHieuSanPhamBUS.Instance.GetLookupEdit(lueKiHieu);
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVang);
            NhaCungCapBUS.Instance.GetLookupEdit(lueNhaCungCap);
            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
            GetDataForGridSanPhamChuaBan();
        }

        private void GetDataForGridSanPhamChuaBan()
        {
            List<SanPham> listSanPhamTemp = SanPhamBUS.Instance.GetListSanPhamChuaBan();

            listSanPhamChuaBan.Clear();

            foreach (var sanPham in listSanPhamTemp)
            {
                listSanPhamChuaBan.Add(sanPham);
            }

            grcSanPham.RefreshDataSource();

            HienThiTongSoSanPham();
        }

        private void HienThiTongSoSanPham()
        {
            lbTongSoLuongSanPham.Text = "Tổng số sản phẩm: " + listSanPhamChuaBan.Count.ToString();
        }

        private void ReloadAllGridControl()
        {
            grcSanPham.RefreshDataSource();
        }

        private SanPham LayThongTinSanPhamTuForm()
        {
            if (IsValidSanPham())
            {
                return SanPhamService.Instance.LayThongTinSanPhamTuForm(
                    txtTenSanPham,
                    txtMaTem,
                    lueHamLuongVang,
                    txtChiTinh, txtPhanTinh, txtLyTinh,
                    txtChiDa, txtPhanDa, txtLyDa,
                    txtChiVang, txtPhanVang, txtLyVang,
                    txtTiLeHot,
                    txtTienCong,
                    lueKiHieu,
                    lueNhaCungCap);
            }
            return null;
        }

        private void HienThiSanPhamLenForm(SanPham sanPham)
        {
            lueNhaCungCap.EditValue = sanPham.MaNhaCungCap;
            txtTenSanPham.Text = sanPham.TenSanPham;
            lueHamLuongVang.Text = sanPham.HamLuongVang.ToString();
            txtTiLeHot.Text = sanPham.TiLeHot.ToString();
            txtTienCong.Text = sanPham.TienCong.ToString();
            lueKiHieu.Text = sanPham.KiHieu;
            txtMaTem.Text = sanPham.MaTem;
            txtGiaBan.EditValue = sanPham.GiaBan;

            Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    sanPham.KhoiLuongVang,
                    txtChiVang, txtPhanVang, txtLyVang);

            Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                sanPham.KhoiLuongVang,
                txtChiTinh, txtPhanTinh, txtLyTinh);

            Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                sanPham.KhoiLuongVang,
                txtChiDa, txtPhanDa, txtLyDa);
        }

        private void gctChuyenKho_Click(object sender, EventArgs e)
        {
            try
            {
                SanPham sanPham = (SanPham)grvSanPham.GetFocusedRow();
                btnSua.Tag = sanPham;
                btnXoa.Tag = sanPham;

                HienThiSanPhamLenForm(sanPham);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private bool IsValidSanPham()
        {
            dxerTenSanPham.Dispose();
            dxerMaTem.Dispose();
            dxerKiHieu.Dispose();
            dxerLyDa.Dispose();
            dxerLyTinh.Dispose();
            dxerLyVang.Dispose();
            dxerNhaCungCap.Dispose();

            double outValue;
            bool check = true;

            if (string.IsNullOrWhiteSpace(txtTenSanPham.Text))
            {
                dxerTenSanPham.SetError(txtTenSanPham, ValidationUltils.TEN_SAN_PHAM);
                check = false;
            }

            if (lueNhaCungCap.EditValue == null)
            {
                dxerNhaCungCap.SetError(lueNhaCungCap, ValidationUltils.NHA_CUNG_CAP);
                check = false;
            }

            if (lueKiHieu.EditValue == null)
            {
                dxerKiHieu.SetError(lueKiHieu, ValidationUltils.KI_HIEU);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(txtMaTem.Text))
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_KHO_MOI);
                check = false;
            }
            else if (txtMaTem.Text.Length < 5)
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_INVALID_LENGHT);
                check = false;
            }
            else if (!string.IsNullOrWhiteSpace(txtMaTem.Text)
                && !Util.CheckMaTemValid(txtMaTem.Text))
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_SAI_CU_PHAP);
                check = false;
            }
            else if (!string.IsNullOrWhiteSpace(txtTenSanPham.Text)
                && !txtMaTem.Text[0].ToString().ToUpper().Equals(txtTenSanPham.Text[0].ToString().ToUpper()))
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_INVALID_CHAR_0);
                check = false;
            }

            if (!double.TryParse(txtLyTinh.Text, out outValue))
            {
                dxerLyTinh.SetError(txtLyTinh, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyDa.Text, out outValue))
            {
                dxerLyDa.SetError(txtLyDa, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyVang.Text, out outValue))
            {
                dxerLyVang.SetError(txtLyVang, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            return check;
        }

        private void ClearFormSanPham()
        {
            lueHamLuongVang.EditValue = null;
            lueNhaCungCap.EditValue = null;
            txtTenSanPham.Text = null;
            txtTiLeHot.Text = null;
            lueKiHieu.EditValue = null;
            txtTienCong.Text = null;
            txtChiVang.Text = null;
            txtPhanVang.Text = null;
            txtLyVang.Text = null;
            txtMaTem.Text = null;

            txtChiTinh.Text = null;
            txtPhanTinh.Text = null;
            txtLyTinh.Text = null;

            txtChiDa.Text = null;
            txtPhanDa.Text = null;
            txtLyDa.Text = null;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SanPham sanPhamCu = (SanPham)grvSanPham.GetFocusedRow();

            if (btnSua.Tag == null)
            {
                PopupService.Instance.Error(MessageError.UPDATE_SP_ERROR);
            }
            else
            {
                if (IsValidSanPham())
                {
                    if (!txtMaTem.Text.Equals(sanPhamCu.MaTem) && SanPhamBUS.Instance.IsDuplicateMaTem(txtMaTem.Text))
                    {
                        PopupService.Instance.Warning(ValidationUltils.TRUNG_MA_TEM);
                        return;
                    }

                    SanPham sanPhamMoi = LayThongTinSanPhamTuForm();

                    sanPhamMoi = Calculator.Instance.TinhGiaVangBanTheoKhoiLuongVang(sanPhamMoi);

                    if (SanPhamBUS.Instance.UpdateSanPham(sanPhamMoi, sanPhamCu) != null)
                    {
                        PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                        GetDataForGridSanPhamChuaBan();
                        ClearFormSanPham();
                    }
                    else
                    {
                        PopupService.Instance.Error(MessageError.UPDATE_ERROR);
                    }
                }
            }
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SanPham sanPham = (SanPham)grvSanPham.GetFocusedRow();

            if (btnXoa.Tag == null)
            {
                PopupService.Instance.Error(MessageError.DELETE_SP_ERROR);
            }
            else
            {
                DialogResult dialogResult = PopupService.Instance.Question("Bạn chắc muốn xóa sản phẩm " + sanPham.TenSanPham + " không ? ");

                if (dialogResult == DialogResult.Yes)
                {
                    if (SanPhamBUS.Instance.DeleteSanPham(sanPham))
                    {
                        PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                    }
                    else
                    {
                        PopupService.Instance.Error(MessageError.DELETE_ERROR);
                    }
                }
            }
        }

        private void btnInTemAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TemVang temVang = new TemVang();
            List<SanPham> sanPhams = SanPhamBUS.Instance.GetListSanPhamChuaBan();
            List<SanPhamInTem> sanPhamInTems = new List<SanPhamInTem>();

            foreach (SanPham sanPham in sanPhams)
            {
                sanPhamInTems.Add(BluePrint.Instance.ConvertToSanPhamInTem(sanPham));
            }

            temVang.DataSource = sanPhamInTems;
            temVang.ShowPreviewDialog();
        }

        #region Tính KVL khi nhập KLĐ và KLT
        double tongKhoiLuongTinh = 0;
        double tongKhoiLuongDa = 0;

        private void txtChiTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtPhanTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtLyTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtChiDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtPhanDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtLyDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }
        #endregion

        private void btnInTem_Click(object sender, EventArgs e)
        {
            SanPham sanPham = (SanPham)grvSanPham.GetFocusedRow();

            if (sanPham == null)
            {
                PopupService.Instance.Warning(ValidationUltils.KHONG_TIM_THAY_MA_SAN_PHAM);
            }
            else
            {
                TemVang temVang = new TemVang();
                temVang.DataSource = new SanPhamInTem[] { BluePrint.Instance.ConvertToSanPhamInTem(sanPham) };
                temVang.ShowPreviewDialog();
            }
        }

        private void grvSanPham_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnNhapKhoMoiExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmNhapKhoMoiExcel f = new frmNhapKhoMoiExcel();
            f.ShowDialog();
            LoadData();
        }

        private void btnXuatFileExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var resultDialog = PopupService.Instance.Question(MessageWarning.XUAT_KHO_EXCEL);

            if (resultDialog == DialogResult.No)
            {
                return;
            }

            string pathSave;
            string fileName = "";
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.ShowNewFolderButton = true;
            folder.Description = "Chọn vị trí lưu file!";
            if (folder.ShowDialog() == DialogResult.OK)
            {
                pathSave = folder.SelectedPath + "\\";
                if (InputBox.Show("Thông báo!",
                "&Nhập tên file:", ref fileName) == DialogResult.OK)
                {
                    pathSave += (fileName + ".xls");
                    grcSanPham.ExportToXls(pathSave);

                    SanPhamBUS.Instance.XoaListSanPhamXuatKho();
                    LoadData();
                    PopupService.Instance.Warning(MessageWarning.XUAT_KHO_EXCEL_MA_SAN_PHAM);
                }
            }
        }
    }
}