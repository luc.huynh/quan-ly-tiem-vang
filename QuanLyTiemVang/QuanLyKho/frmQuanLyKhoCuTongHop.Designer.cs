﻿namespace QuanLyTiemVang.QuanLyKho
{
    partial class frmQuanLyKhoCuTongHop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnDemDiGiaCong = new DevExpress.XtraBars.BarButtonItem();
            this.btnCapNhap = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoaLichSu = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtMaLichSuGiaCong = new DevExpress.XtraEditors.TextEdit();
            this.lueHamLuongVang = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLyVang = new DevExpress.XtraEditors.TextEdit();
            this.txtChiVang = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanVang = new DevExpress.XtraEditors.TextEdit();
            this.txtLyDa = new DevExpress.XtraEditors.TextEdit();
            this.txtChiDa = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanDa = new DevExpress.XtraEditors.TextEdit();
            this.txtLyTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtChiTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtPhanTinh = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.grcKhoCuTongHop = new DevExpress.XtraGrid.GridControl();
            this.grvKhoCuTongHop = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTongKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTongKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTongKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcLichSuGiaCong = new DevExpress.XtraGrid.GridControl();
            this.khoiLuongGiaCongBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvLichSuGiaCong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclNgayChuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclHamLuongVangGiaCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongTinhTieuHao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongDaTieuHao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhoiLuongVangTieuHao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dxerHamLuongVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyTinh = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyDa = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLyVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.btnXuatFileExcel = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaLichSuGiaCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcKhoCuTongHop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvKhoCuTongHop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoiLuongGiaCongBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnDemDiGiaCong,
            this.btnLamMoiDuLieu,
            this.btnSua,
            this.btnXoa,
            this.barButtonItem3,
            this.btnCapNhap,
            this.btnXoaLichSu,
            this.btnNhapLai,
            this.btnXuatFileExcel});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 9;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDemDiGiaCong, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnCapNhap, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoaLichSu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXuatFileExcel, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 7;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnDemDiGiaCong
            // 
            this.btnDemDiGiaCong.Caption = "Đem đi gia công";
            this.btnDemDiGiaCong.CausesValidation = true;
            this.btnDemDiGiaCong.Glyph = global::QuanLyTiemVang.Properties.Resources.Extract_todays_changes_icon;
            this.btnDemDiGiaCong.Id = 0;
            this.btnDemDiGiaCong.Name = "btnDemDiGiaCong";
            this.btnDemDiGiaCong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDemDiGiaCong_ItemClick);
            // 
            // btnCapNhap
            // 
            this.btnCapNhap.Caption = "Cập nhập lịch sử";
            this.btnCapNhap.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnCapNhap.Id = 5;
            this.btnCapNhap.Name = "btnCapNhap";
            this.btnCapNhap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCapNhap_ItemClick);
            // 
            // btnXoaLichSu
            // 
            this.btnXoaLichSu.Caption = "Xóa lịch sử";
            this.btnXoaLichSu.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoaLichSu.Id = 6;
            this.btnXoaLichSu.Name = "btnXoaLichSu";
            this.btnXoaLichSu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoaLichSu_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 1;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1298, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 912);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1298, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 862);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1298, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 862);
            // 
            // btnSua
            // 
            this.btnSua.Caption = "Sửa";
            this.btnSua.Glyph = global::QuanLyTiemVang.Properties.Resources.Pencil_icon;
            this.btnSua.Id = 2;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.Button_Close_icon;
            this.btnXoa.Id = 3;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Nhập từ file excel";
            this.barButtonItem3.Glyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtMaLichSuGiaCong);
            this.layoutControl1.Controls.Add(this.lueHamLuongVang);
            this.layoutControl1.Controls.Add(this.label6);
            this.layoutControl1.Controls.Add(this.txtLyVang);
            this.layoutControl1.Controls.Add(this.txtChiVang);
            this.layoutControl1.Controls.Add(this.txtPhanVang);
            this.layoutControl1.Controls.Add(this.txtLyDa);
            this.layoutControl1.Controls.Add(this.txtChiDa);
            this.layoutControl1.Controls.Add(this.txtPhanDa);
            this.layoutControl1.Controls.Add(this.txtLyTinh);
            this.layoutControl1.Controls.Add(this.txtChiTinh);
            this.layoutControl1.Controls.Add(this.txtPhanTinh);
            this.layoutControl1.Controls.Add(this.label5);
            this.layoutControl1.Controls.Add(this.label4);
            this.layoutControl1.Controls.Add(this.label3);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 50);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(69, 273, 829, 437);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 204);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtMaLichSuGiaCong
            // 
            this.txtMaLichSuGiaCong.Location = new System.Drawing.Point(135, 47);
            this.txtMaLichSuGiaCong.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMaLichSuGiaCong.MenuManager = this.barManager1;
            this.txtMaLichSuGiaCong.Name = "txtMaLichSuGiaCong";
            this.txtMaLichSuGiaCong.Properties.ReadOnly = true;
            this.txtMaLichSuGiaCong.Size = new System.Drawing.Size(1126, 22);
            this.txtMaLichSuGiaCong.StyleController = this.layoutControl1;
            this.txtMaLichSuGiaCong.TabIndex = 32;
            // 
            // lueHamLuongVang
            // 
            this.lueHamLuongVang.Location = new System.Drawing.Point(135, 75);
            this.lueHamLuongVang.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueHamLuongVang.MenuManager = this.barManager1;
            this.lueHamLuongVang.Name = "lueHamLuongVang";
            this.lueHamLuongVang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHamLuongVang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueHamLuongVang.Properties.NullText = "";
            this.lueHamLuongVang.Size = new System.Drawing.Size(1126, 22);
            this.lueHamLuongVang.StyleController = this.layoutControl1;
            this.lueHamLuongVang.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(16, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1245, 25);
            this.label6.TabIndex = 30;
            this.label6.Text = "Thông tin khối lượng vàng đem đi gia công";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLyVang
            // 
            this.txtLyVang.Location = new System.Drawing.Point(1071, 165);
            this.txtLyVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyVang.MenuManager = this.barManager1;
            this.txtLyVang.Name = "txtLyVang";
            this.txtLyVang.Properties.MaxLength = 3;
            this.txtLyVang.Properties.NullText = "0";
            this.txtLyVang.Properties.ReadOnly = true;
            this.txtLyVang.Size = new System.Drawing.Size(190, 22);
            this.txtLyVang.StyleController = this.layoutControl1;
            this.txtLyVang.TabIndex = 28;
            // 
            // txtChiVang
            // 
            this.txtChiVang.Location = new System.Drawing.Point(447, 165);
            this.txtChiVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiVang.MenuManager = this.barManager1;
            this.txtChiVang.Name = "txtChiVang";
            this.txtChiVang.Properties.Mask.EditMask = "d";
            this.txtChiVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiVang.Properties.MaxLength = 8;
            this.txtChiVang.Properties.NullText = "0";
            this.txtChiVang.Properties.ReadOnly = true;
            this.txtChiVang.Size = new System.Drawing.Size(188, 22);
            this.txtChiVang.StyleController = this.layoutControl1;
            this.txtChiVang.TabIndex = 27;
            // 
            // txtPhanVang
            // 
            this.txtPhanVang.Location = new System.Drawing.Point(760, 165);
            this.txtPhanVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanVang.MenuManager = this.barManager1;
            this.txtPhanVang.Name = "txtPhanVang";
            this.txtPhanVang.Properties.Mask.EditMask = "d";
            this.txtPhanVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanVang.Properties.MaxLength = 1;
            this.txtPhanVang.Properties.NullText = "0";
            this.txtPhanVang.Properties.ReadOnly = true;
            this.txtPhanVang.Size = new System.Drawing.Size(186, 22);
            this.txtPhanVang.StyleController = this.layoutControl1;
            this.txtPhanVang.TabIndex = 26;
            // 
            // txtLyDa
            // 
            this.txtLyDa.Location = new System.Drawing.Point(1071, 134);
            this.txtLyDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyDa.MenuManager = this.barManager1;
            this.txtLyDa.Name = "txtLyDa";
            this.txtLyDa.Properties.MaxLength = 3;
            this.txtLyDa.Properties.NullText = "0";
            this.txtLyDa.Size = new System.Drawing.Size(190, 22);
            this.txtLyDa.StyleController = this.layoutControl1;
            this.txtLyDa.TabIndex = 25;
            this.txtLyDa.EditValueChanged += new System.EventHandler(this.txtLyDa_EditValueChanged);
            // 
            // txtChiDa
            // 
            this.txtChiDa.Location = new System.Drawing.Point(447, 134);
            this.txtChiDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiDa.MenuManager = this.barManager1;
            this.txtChiDa.Name = "txtChiDa";
            this.txtChiDa.Properties.Mask.EditMask = "d";
            this.txtChiDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiDa.Properties.MaxLength = 8;
            this.txtChiDa.Properties.NullText = "0";
            this.txtChiDa.Size = new System.Drawing.Size(188, 22);
            this.txtChiDa.StyleController = this.layoutControl1;
            this.txtChiDa.TabIndex = 24;
            this.txtChiDa.EditValueChanged += new System.EventHandler(this.txtChiDa_EditValueChanged);
            // 
            // txtPhanDa
            // 
            this.txtPhanDa.Location = new System.Drawing.Point(760, 134);
            this.txtPhanDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanDa.MenuManager = this.barManager1;
            this.txtPhanDa.Name = "txtPhanDa";
            this.txtPhanDa.Properties.Mask.EditMask = "d";
            this.txtPhanDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanDa.Properties.MaxLength = 1;
            this.txtPhanDa.Properties.NullText = "0";
            this.txtPhanDa.Size = new System.Drawing.Size(186, 22);
            this.txtPhanDa.StyleController = this.layoutControl1;
            this.txtPhanDa.TabIndex = 23;
            this.txtPhanDa.EditValueChanged += new System.EventHandler(this.txtPhanDa_EditValueChanged);
            // 
            // txtLyTinh
            // 
            this.txtLyTinh.Location = new System.Drawing.Point(1071, 103);
            this.txtLyTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLyTinh.MenuManager = this.barManager1;
            this.txtLyTinh.Name = "txtLyTinh";
            this.txtLyTinh.Properties.MaxLength = 3;
            this.txtLyTinh.Properties.NullText = "0";
            this.txtLyTinh.Size = new System.Drawing.Size(190, 22);
            this.txtLyTinh.StyleController = this.layoutControl1;
            this.txtLyTinh.TabIndex = 22;
            this.txtLyTinh.EditValueChanged += new System.EventHandler(this.txtLyTinh_EditValueChanged);
            // 
            // txtChiTinh
            // 
            this.txtChiTinh.Location = new System.Drawing.Point(447, 103);
            this.txtChiTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChiTinh.MenuManager = this.barManager1;
            this.txtChiTinh.Name = "txtChiTinh";
            this.txtChiTinh.Properties.Mask.EditMask = "d";
            this.txtChiTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiTinh.Properties.MaxLength = 8;
            this.txtChiTinh.Properties.NullText = "0";
            this.txtChiTinh.Size = new System.Drawing.Size(188, 22);
            this.txtChiTinh.StyleController = this.layoutControl1;
            this.txtChiTinh.TabIndex = 21;
            this.txtChiTinh.EditValueChanged += new System.EventHandler(this.txtChiTinh_EditValueChanged);
            // 
            // txtPhanTinh
            // 
            this.txtPhanTinh.Location = new System.Drawing.Point(760, 103);
            this.txtPhanTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhanTinh.MenuManager = this.barManager1;
            this.txtPhanTinh.Name = "txtPhanTinh";
            this.txtPhanTinh.Properties.Mask.EditMask = "d";
            this.txtPhanTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhanTinh.Properties.MaxLength = 1;
            this.txtPhanTinh.Properties.NullText = "0";
            this.txtPhanTinh.Size = new System.Drawing.Size(186, 22);
            this.txtPhanTinh.StyleController = this.layoutControl1;
            this.txtPhanTinh.TabIndex = 20;
            this.txtPhanTinh.EditValueChanged += new System.EventHandler(this.txtPhanTinh_EditValueChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(306, 25);
            this.label5.TabIndex = 19;
            this.label5.Text = "Khối lượng vàng";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(306, 25);
            this.label4.TabIndex = 18;
            this.label4.Text = "Khối lượng đá";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(306, 25);
            this.label3.TabIndex = 17;
            this.label3.Text = "Khối lượng tịnh";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1277, 206);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.label3;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 87);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(312, 31);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.label4;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 118);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(312, 31);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.label5;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 149);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(312, 31);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtPhanTinh;
            this.layoutControlItem15.Location = new System.Drawing.Point(625, 87);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(311, 31);
            this.layoutControlItem15.Text = "Phân";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtChiTinh;
            this.layoutControlItem16.Location = new System.Drawing.Point(312, 87);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(313, 31);
            this.layoutControlItem16.Text = "Chỉ";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(115, 17);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtLyTinh;
            this.layoutControlItem17.Location = new System.Drawing.Point(936, 87);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(315, 31);
            this.layoutControlItem17.Text = "Ly";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtPhanDa;
            this.layoutControlItem18.Location = new System.Drawing.Point(625, 118);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(311, 31);
            this.layoutControlItem18.Text = "Phân";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtChiDa;
            this.layoutControlItem19.Location = new System.Drawing.Point(312, 118);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(313, 31);
            this.layoutControlItem19.Text = "Chỉ";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(115, 17);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtLyDa;
            this.layoutControlItem20.Location = new System.Drawing.Point(936, 118);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(315, 31);
            this.layoutControlItem20.Text = "Ly";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtPhanVang;
            this.layoutControlItem21.Location = new System.Drawing.Point(625, 149);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(311, 31);
            this.layoutControlItem21.Text = "Phân";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txtChiVang;
            this.layoutControlItem22.Location = new System.Drawing.Point(312, 149);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(313, 31);
            this.layoutControlItem22.Text = "Chỉ";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(115, 17);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtLyVang;
            this.layoutControlItem23.Location = new System.Drawing.Point(936, 149);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(315, 31);
            this.layoutControlItem23.Text = "Ly";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(115, 16);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.label6;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(1251, 31);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueHamLuongVang;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1251, 28);
            this.layoutControlItem1.Text = "Hàm lượng vàng";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(115, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtMaLichSuGiaCong;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1251, 28);
            this.layoutControlItem2.Text = "Mã lịch sử gia công";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(115, 17);
            // 
            // grcKhoCuTongHop
            // 
            this.grcKhoCuTongHop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcKhoCuTongHop.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcKhoCuTongHop.Location = new System.Drawing.Point(2, 25);
            this.grcKhoCuTongHop.MainView = this.grvKhoCuTongHop;
            this.grcKhoCuTongHop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcKhoCuTongHop.MenuManager = this.barManager1;
            this.grcKhoCuTongHop.Name = "grcKhoCuTongHop";
            this.grcKhoCuTongHop.Size = new System.Drawing.Size(536, 631);
            this.grcKhoCuTongHop.TabIndex = 5;
            this.grcKhoCuTongHop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvKhoCuTongHop});
            // 
            // grvKhoCuTongHop
            // 
            this.grvKhoCuTongHop.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclHamLuongVang,
            this.grclTongKhoiLuongTinh,
            this.grclTongKhoiLuongDa,
            this.grclTongKhoiLuongVang});
            this.grvKhoCuTongHop.GridControl = this.grcKhoCuTongHop;
            this.grvKhoCuTongHop.Name = "grvKhoCuTongHop";
            this.grvKhoCuTongHop.OptionsBehavior.Editable = false;
            this.grvKhoCuTongHop.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvKhoCuTongHop.OptionsFind.AlwaysVisible = true;
            this.grvKhoCuTongHop.OptionsFind.FindDelay = 100;
            this.grvKhoCuTongHop.OptionsFind.FindNullPrompt = "";
            this.grvKhoCuTongHop.OptionsFind.ShowFindButton = false;
            this.grvKhoCuTongHop.OptionsView.ShowGroupPanel = false;
            this.grvKhoCuTongHop.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvKhoCuTongHop_CustomDrawRowIndicator);
            // 
            // grclHamLuongVang
            // 
            this.grclHamLuongVang.Caption = "Hàm lượng vàng";
            this.grclHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.grclHamLuongVang.Name = "grclHamLuongVang";
            this.grclHamLuongVang.Visible = true;
            this.grclHamLuongVang.VisibleIndex = 0;
            // 
            // grclTongKhoiLuongTinh
            // 
            this.grclTongKhoiLuongTinh.Caption = "Tổng KL tịnh";
            this.grclTongKhoiLuongTinh.DisplayFormat.FormatString = "#,0.#";
            this.grclTongKhoiLuongTinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclTongKhoiLuongTinh.FieldName = "TongKhoiLuongTinh";
            this.grclTongKhoiLuongTinh.Name = "grclTongKhoiLuongTinh";
            this.grclTongKhoiLuongTinh.Visible = true;
            this.grclTongKhoiLuongTinh.VisibleIndex = 1;
            // 
            // grclTongKhoiLuongDa
            // 
            this.grclTongKhoiLuongDa.Caption = "Tổng KL đá";
            this.grclTongKhoiLuongDa.DisplayFormat.FormatString = "#,0.#";
            this.grclTongKhoiLuongDa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclTongKhoiLuongDa.FieldName = "TongKhoiLuongDa";
            this.grclTongKhoiLuongDa.Name = "grclTongKhoiLuongDa";
            this.grclTongKhoiLuongDa.Visible = true;
            this.grclTongKhoiLuongDa.VisibleIndex = 2;
            // 
            // grclTongKhoiLuongVang
            // 
            this.grclTongKhoiLuongVang.Caption = "Tổng KL vàng";
            this.grclTongKhoiLuongVang.DisplayFormat.FormatString = "#,0.#";
            this.grclTongKhoiLuongVang.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclTongKhoiLuongVang.FieldName = "TongKhoiLuongVang";
            this.grclTongKhoiLuongVang.Name = "grclTongKhoiLuongVang";
            this.grclTongKhoiLuongVang.Visible = true;
            this.grclTongKhoiLuongVang.VisibleIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.grcKhoCuTongHop);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(0, 254);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(540, 658);
            this.groupControl1.TabIndex = 10;
            this.groupControl1.Text = "Khối lượng vàng trong kho cũ";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(540, 254);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 658);
            this.splitterControl1.TabIndex = 11;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcLichSuGiaCong);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(546, 254);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(752, 658);
            this.groupControl2.TabIndex = 12;
            this.groupControl2.Text = "Lịch sử gia công";
            // 
            // grcLichSuGiaCong
            // 
            this.grcLichSuGiaCong.DataSource = this.khoiLuongGiaCongBindingSource;
            this.grcLichSuGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcLichSuGiaCong.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcLichSuGiaCong.Location = new System.Drawing.Point(2, 25);
            this.grcLichSuGiaCong.MainView = this.grvLichSuGiaCong;
            this.grcLichSuGiaCong.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcLichSuGiaCong.MenuManager = this.barManager1;
            this.grcLichSuGiaCong.Name = "grcLichSuGiaCong";
            this.grcLichSuGiaCong.Size = new System.Drawing.Size(748, 631);
            this.grcLichSuGiaCong.TabIndex = 0;
            this.grcLichSuGiaCong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichSuGiaCong});
            this.grcLichSuGiaCong.DoubleClick += new System.EventHandler(this.grcLichSuGiaCong_DoubleClick);
            // 
            // khoiLuongGiaCongBindingSource
            // 
            this.khoiLuongGiaCongBindingSource.DataSource = typeof(DTO.Models.KhoiLuongGiaCong);
            // 
            // grvLichSuGiaCong
            // 
            this.grvLichSuGiaCong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclNgayChuyen,
            this.grclHamLuongVangGiaCong,
            this.grclKhoiLuongTinhTieuHao,
            this.grclKhoiLuongDaTieuHao,
            this.grclKhoiLuongVangTieuHao});
            this.grvLichSuGiaCong.GridControl = this.grcLichSuGiaCong;
            this.grvLichSuGiaCong.Name = "grvLichSuGiaCong";
            this.grvLichSuGiaCong.OptionsBehavior.Editable = false;
            this.grvLichSuGiaCong.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvLichSuGiaCong.OptionsFind.AlwaysVisible = true;
            this.grvLichSuGiaCong.OptionsFind.FindDelay = 100;
            this.grvLichSuGiaCong.OptionsFind.FindNullPrompt = "";
            this.grvLichSuGiaCong.OptionsFind.ShowFindButton = false;
            this.grvLichSuGiaCong.OptionsView.ShowGroupPanel = false;
            this.grvLichSuGiaCong.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvLichSuGiaCong_CustomDrawRowIndicator);
            // 
            // grclNgayChuyen
            // 
            this.grclNgayChuyen.Caption = "Ngày chuyển";
            this.grclNgayChuyen.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.grclNgayChuyen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.grclNgayChuyen.FieldName = "NgayChuyen";
            this.grclNgayChuyen.Name = "grclNgayChuyen";
            this.grclNgayChuyen.Visible = true;
            this.grclNgayChuyen.VisibleIndex = 0;
            // 
            // grclHamLuongVangGiaCong
            // 
            this.grclHamLuongVangGiaCong.Caption = "Hàm lượng vàng";
            this.grclHamLuongVangGiaCong.FieldName = "KhoCu.KiHieuHamLuongVang.HamLuongVang";
            this.grclHamLuongVangGiaCong.Name = "grclHamLuongVangGiaCong";
            this.grclHamLuongVangGiaCong.Visible = true;
            this.grclHamLuongVangGiaCong.VisibleIndex = 1;
            // 
            // grclKhoiLuongTinhTieuHao
            // 
            this.grclKhoiLuongTinhTieuHao.Caption = "KL Tịnh";
            this.grclKhoiLuongTinhTieuHao.DisplayFormat.FormatString = "#,0.#";
            this.grclKhoiLuongTinhTieuHao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclKhoiLuongTinhTieuHao.FieldName = "KhoiLuongTinhTieuHao";
            this.grclKhoiLuongTinhTieuHao.Name = "grclKhoiLuongTinhTieuHao";
            this.grclKhoiLuongTinhTieuHao.Visible = true;
            this.grclKhoiLuongTinhTieuHao.VisibleIndex = 2;
            // 
            // grclKhoiLuongDaTieuHao
            // 
            this.grclKhoiLuongDaTieuHao.Caption = "KL Đá";
            this.grclKhoiLuongDaTieuHao.DisplayFormat.FormatString = "#,0.#";
            this.grclKhoiLuongDaTieuHao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclKhoiLuongDaTieuHao.FieldName = "KhoiLuongDaTieuHao";
            this.grclKhoiLuongDaTieuHao.Name = "grclKhoiLuongDaTieuHao";
            this.grclKhoiLuongDaTieuHao.Visible = true;
            this.grclKhoiLuongDaTieuHao.VisibleIndex = 3;
            // 
            // grclKhoiLuongVangTieuHao
            // 
            this.grclKhoiLuongVangTieuHao.Caption = "KL Vàng";
            this.grclKhoiLuongVangTieuHao.DisplayFormat.FormatString = "#,0.#";
            this.grclKhoiLuongVangTieuHao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclKhoiLuongVangTieuHao.FieldName = "KhoiLuongVangTieuHao";
            this.grclKhoiLuongVangTieuHao.Name = "grclKhoiLuongVangTieuHao";
            this.grclKhoiLuongVangTieuHao.Visible = true;
            this.grclKhoiLuongVangTieuHao.VisibleIndex = 4;
            // 
            // dxerHamLuongVang
            // 
            this.dxerHamLuongVang.ContainerControl = this;
            // 
            // dxerLyTinh
            // 
            this.dxerLyTinh.ContainerControl = this;
            // 
            // dxerLyDa
            // 
            this.dxerLyDa.ContainerControl = this;
            // 
            // dxerLyVang
            // 
            this.dxerLyVang.ContainerControl = this;
            // 
            // btnXuatFileExcel
            // 
            this.btnXuatFileExcel.Caption = "Xuất ra file excel";
            this.btnXuatFileExcel.Glyph = global::QuanLyTiemVang.Properties.Resources.file_extension_xls_icon;
            this.btnXuatFileExcel.Id = 8;
            this.btnXuatFileExcel.Name = "btnXuatFileExcel";
            this.btnXuatFileExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatFileExcel_ItemClick);
            // 
            // frmQuanLyKhoCuTongHop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 912);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmQuanLyKhoCuTongHop";
            this.Text = "Quản lý kho cũ";
            this.Load += new System.EventHandler(this.frmQuanLyKhoCuTongHop_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaLichSuGiaCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHamLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcKhoCuTongHop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvKhoCuTongHop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcLichSuGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoiLuongGiaCongBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichSuGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerHamLuongVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyTinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLyVang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnDemDiGiaCong;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl grcKhoCuTongHop;
        private DevExpress.XtraGrid.Views.Grid.GridView grvKhoCuTongHop;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txtLyVang;
        private DevExpress.XtraEditors.TextEdit txtChiVang;
        private DevExpress.XtraEditors.TextEdit txtPhanVang;
        private DevExpress.XtraEditors.TextEdit txtLyDa;
        private DevExpress.XtraEditors.TextEdit txtChiDa;
        private DevExpress.XtraEditors.TextEdit txtPhanDa;
        private DevExpress.XtraEditors.TextEdit txtLyTinh;
        private DevExpress.XtraEditors.TextEdit txtChiTinh;
        private DevExpress.XtraEditors.TextEdit txtPhanTinh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcLichSuGiaCong;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichSuGiaCong;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraBars.BarButtonItem btnCapNhap;
        private DevExpress.XtraBars.BarButtonItem btnXoaLichSu;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclTongKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclTongKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn grclTongKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclNgayChuyen;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongTinhTieuHao;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongDaTieuHao;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhoiLuongVangTieuHao;
        private DevExpress.XtraGrid.Columns.GridColumn grclHamLuongVangGiaCong;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerHamLuongVang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyTinh;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyDa;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLyVang;
        private DevExpress.XtraEditors.LookUpEdit lueHamLuongVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtMaLichSuGiaCong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private System.Windows.Forms.BindingSource khoiLuongGiaCongBindingSource;
        private DevExpress.XtraBars.BarButtonItem btnXuatFileExcel;
    }
}