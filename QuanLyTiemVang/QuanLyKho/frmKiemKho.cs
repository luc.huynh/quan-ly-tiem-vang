﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.Models;
using BUS;

namespace QuanLyTiemVang.QuanLyKho
{
    public partial class frmKiemKho : DevExpress.XtraEditors.XtraForm
    {
        public frmKiemKho()
        {
            InitializeComponent();
            grvSanPhamChuaKiem.IndicatorWidth = 70;
            grvSanPhamDaKiem.IndicatorWidth = 70;
        }

        private List<SanPham> listSanPhamChuaKiem = new List<SanPham>();

        private List<SanPham> listSanPhamChuaKiemTimKiem = new List<SanPham>(); //Sản phẩm khi được tìm kiếm

        private List<SanPham> listSanPhamDaKiem = new List<SanPham>();

        private List<SanPham> listSanPhamDaKiemTimKiem = new List<SanPham>(); //Sản phẩm khi được tìm kiếm

        private void frmKiemKho_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            grcSanPhamChuaKiem.DataSource = listSanPhamChuaKiem;
            grcSanPhamDaKiem.DataSource = listSanPhamDaKiem;

            listSanPhamChuaKiem.Clear();
            listSanPhamDaKiem.Clear();
            listSanPhamChuaKiem.AddRange(SanPhamBUS.Instance.GetListSanPhamChuaBan());

            grcSanPhamChuaKiem.RefreshDataSource();
            grcSanPhamDaKiem.RefreshDataSource();
        }

        private void TinhTongKhiKiemSanPham()
        {
            txtSoSanPhamDaKiem.EditValue = listSanPhamDaKiem.Count;
            txtTongTienCong.EditValue = listSanPhamDaKiem.Sum(x => x.TienCong);
            txtTongKhoiLuongTinh.EditValue = listSanPhamDaKiem.Sum(x => x.KhoiLuongTinh);
            txtTongKhoiLuongDa.EditValue = listSanPhamDaKiem.Sum(x => x.KhoiLuongDa);
            txtTongKhoiLuongVang.EditValue = listSanPhamDaKiem.Sum(x => x.KhoiLuongVang);
        }

        private void btnHuySanPhamKiemKho_Click(object sender, EventArgs e)
        {
            SanPham sanPham = (SanPham)grvSanPhamDaKiem.GetFocusedRow();

            if (sanPham != null)
            {
                listSanPhamDaKiem.Remove(sanPham);
                listSanPhamChuaKiem.Add(sanPham);

                txtTimKiemSanPhamDaKiem.EditValue = null;

                grcSanPhamChuaKiem.RefreshDataSource();
                grcSanPhamDaKiem.RefreshDataSource();

                TinhTongKhiKiemSanPham();
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtTimKiemSanPhamChuaKiem.EditValue = null;
            txtTimKiemSanPhamDaKiem.EditValue = null;

            LoadData();
        }

        private void btnKiemHang_Click(object sender, EventArgs e)
        {
            SanPham sanPham = (SanPham)grvSanPhamChuaKiem.GetFocusedRow();

            if (sanPham != null)
            {
                listSanPhamChuaKiem.Remove(sanPham);
                listSanPhamDaKiem.Add(sanPham);

                txtTimKiemSanPhamChuaKiem.EditValue = null;

                grcSanPhamChuaKiem.RefreshDataSource();
                grcSanPhamDaKiem.RefreshDataSource();

                TinhTongKhiKiemSanPham();
            }
        }

        private void txtTimKiemSanPhamChuaKiem_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtTimKiemSanPhamChuaKiem.Text))
            {
                string searchText = txtTimKiemSanPhamChuaKiem.Text;

                listSanPhamChuaKiemTimKiem.Clear();

                foreach (SanPham item in listSanPhamChuaKiem)
                {
                    if (item.HamLuongVang.Contains(searchText) ||
                        item.MaTem.Contains(searchText) ||
                        item.TenSanPham.Contains(searchText))
                    {
                        listSanPhamChuaKiemTimKiem.Add(item);
                    }
                }

                grcSanPhamChuaKiem.DataSource = listSanPhamChuaKiemTimKiem;
            }
            else
            {
                grcSanPhamChuaKiem.DataSource = listSanPhamChuaKiem;
            }

            grcSanPhamChuaKiem.RefreshDataSource();
        }

        private void txtTimKiemSanPhamDaKiem_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtTimKiemSanPhamDaKiem.Text))
            {
                string searchText = txtTimKiemSanPhamDaKiem.Text;

                listSanPhamDaKiemTimKiem.Clear();

                foreach (SanPham item in listSanPhamDaKiem)
                {
                    if (item.HamLuongVang.Contains(searchText) ||
                        item.MaTem.Contains(searchText) ||
                        item.TenSanPham.Contains(searchText))
                    {
                        listSanPhamDaKiemTimKiem.Add(item);
                    }
                }

                grcSanPhamDaKiem.DataSource = listSanPhamDaKiemTimKiem;
            }
            else
            {
                grcSanPhamDaKiem.DataSource = listSanPhamDaKiem;
            }

            grcSanPhamDaKiem.RefreshDataSource();
        }

        private void grvSanPhamChuaKiem_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void grvSanPhamDaKiem_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void txtTimKiemSanPhamChuaKiem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (listSanPhamChuaKiemTimKiem.Count == 1)
                {
                    SanPham sanPham = (SanPham)grvSanPhamChuaKiem.GetFocusedRow();

                    if (sanPham != null)
                    {
                        listSanPhamChuaKiem.Remove(sanPham);
                        listSanPhamDaKiem.Add(sanPham);

                        txtTimKiemSanPhamChuaKiem.EditValue = null;

                        grcSanPhamChuaKiem.RefreshDataSource();
                        grcSanPhamDaKiem.RefreshDataSource();

                        TinhTongKhiKiemSanPham();
                    }
                }
            }
        }

        private void txtTimKiemSanPhamDaKiem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (listSanPhamDaKiemTimKiem.Count == 1)
                {
                    SanPham sanPham = (SanPham)grvSanPhamDaKiem.GetFocusedRow();

                    if (sanPham != null)
                    {
                        listSanPhamDaKiem.Remove(sanPham);
                        listSanPhamChuaKiem.Add(sanPham);

                        txtTimKiemSanPhamDaKiem.EditValue = null;

                        grcSanPhamChuaKiem.RefreshDataSource();
                        grcSanPhamDaKiem.RefreshDataSource();

                        TinhTongKhiKiemSanPham();
                    }
                }
            }
        }
    }
}