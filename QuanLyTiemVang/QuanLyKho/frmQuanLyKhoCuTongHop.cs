﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyTiemVang.Constants.StringUltils;
using DTO.Models;
using QuanLyTiemVang.Services;
using BUS;
using QuanLyTiemVang.Constants;
using QuanLyDieuXe.DialogCustom;

namespace QuanLyTiemVang.QuanLyKho
{
    public partial class frmQuanLyKhoCuTongHop : DevExpress.XtraEditors.XtraForm
    {
        public frmQuanLyKhoCuTongHop()
        {
            InitializeComponent();
            grvKhoCuTongHop.IndicatorWidth = 70;
            grvLichSuGiaCong.IndicatorWidth = 70;
        }

        private void frmQuanLyKhoCuTongHop_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVang);
            KhoiLuongGiaCongBUS.Instance.SetDataForGridControl(grcLichSuGiaCong);
            KhoCuBUS.Instance.SetDataForGridControl(grcKhoCuTongHop);
        }

        private void ClearForm()
        {
            txtMaLichSuGiaCong.Text = null;
            lueHamLuongVang.EditValue = null;
            txtChiTinh.Text = null;
            txtPhanTinh.Text = null;
            txtLyTinh.Text = null;
            txtChiDa.Text = null;
            txtPhanDa.Text = null;
            txtLyDa.Text = null;
            txtChiVang.Text = null;
            txtPhanVang.Text = null;
            txtLyVang.Text = null;
        }

        private bool IsValidForm()
        {
            dxerHamLuongVang.Dispose();
            dxerLyTinh.Dispose();
            dxerLyVang.Dispose();
            dxerLyDa.Dispose();

            bool check = true;
            double outValue;

            if (lueHamLuongVang.EditValue == null)
            {
                dxerHamLuongVang.SetError(lueHamLuongVang, ValidationUltils.HAM_LUONG_VANG);
                check = false;
            }

            if (!double.TryParse(txtLyTinh.Text, out outValue))
            {
                dxerLyTinh.SetError(txtLyTinh, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyDa.Text, out outValue))
            {
                dxerLyDa.SetError(txtLyDa, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyVang.Text, out outValue))
            {
                dxerLyVang.SetError(txtLyVang, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            return check;
        }

        private KhoiLuongGiaCong GetKhoiLuongGiaCongOfForm()
        {
            int outValue;

            if (IsValidForm())
            {
                return new KhoiLuongGiaCong
                {
                    MaKhoiLuongGiaCong = int.TryParse(txtMaLichSuGiaCong.Text, out outValue) ? outValue : 0,
                    KhoiLuongTinhTieuHao = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiTinh, txtPhanTinh, txtLyTinh),
                    KhoiLuongDaTieuHao = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiDa, txtPhanDa, txtLyDa),
                    KhoiLuongVangTieuHao = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiVang, txtPhanVang, txtLyVang),
                    MaNhanVien = Form1.nhanVien.MaNhanVien,
                    NgayChuyen = DateTime.Now,
                    MaKhoCu = KhoCuBUS.Instance.GetMaKhoCuByMaHamLuongVang((int)lueHamLuongVang.EditValue)
                };
            }
            return null;
        }

        private bool IsValidMaKhoiLuongGiaCong()
        {
            return string.IsNullOrWhiteSpace(txtMaLichSuGiaCong.Text);
        }

        private void HienThiChiTietKhoiLuongGiaCong(KhoiLuongGiaCong khoiLuongGiaCong)
        {
            double khoiLuongVang, khoiLuongTinh, khoiLuongDa;

            try
            {
                txtMaLichSuGiaCong.Text = khoiLuongGiaCong.MaKhoiLuongGiaCong.ToString();
                lueHamLuongVang.EditValue = khoiLuongGiaCong.KhoCu.MaHamLuongVang;

                khoiLuongVang = double.Parse(khoiLuongGiaCong.KhoiLuongVangTieuHao.ToString());

                txtLyVang.Text = Calculator.Instance.TinhSoLyToString(khoiLuongVang);
                txtPhanVang.Text = Calculator.Instance.TinhSoPhanToString(khoiLuongVang);
                txtChiVang.Text = Calculator.Instance.TinhSoChiToString(khoiLuongVang);

                khoiLuongTinh = double.Parse(khoiLuongGiaCong.KhoiLuongTinhTieuHao.ToString());

                txtLyTinh.Text = Calculator.Instance.TinhSoLyToString(khoiLuongTinh);
                txtPhanTinh.Text = Calculator.Instance.TinhSoPhanToString(khoiLuongTinh);
                txtChiTinh.Text = Calculator.Instance.TinhSoChiToString(khoiLuongTinh);

                khoiLuongDa = double.Parse(khoiLuongGiaCong.KhoiLuongDaTieuHao.ToString());

                txtLyDa.Text = Calculator.Instance.TinhSoLyToString(khoiLuongDa);
                txtPhanDa.Text = Calculator.Instance.TinhSoPhanToString(khoiLuongDa);
                txtChiDa.Text = Calculator.Instance.TinhSoChiToString(khoiLuongDa);
            }
            catch (Exception ex)
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                Console.WriteLine(ex.ToString());
            }
        }

        private void btnDemDiGiaCong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhoiLuongGiaCong khoiLuongGiaCong = GetKhoiLuongGiaCongOfForm();

            if (khoiLuongGiaCong == null)
            {
                PopupService.Instance.Error(MessageError.DATA_OF_FORM_NULL);
                return;
            }

            if (KhoiLuongGiaCongBUS.Instance.Insert(khoiLuongGiaCong) != null)
            {
                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.INSERT_ERROR);
        }

        private void btnCapNhap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhoiLuongGiaCong khoiLuongGiaCong = GetKhoiLuongGiaCongOfForm();

            if (khoiLuongGiaCong == null || IsValidMaKhoiLuongGiaCong())
            {
                PopupService.Instance.Error(MessageError.DATA_OF_FORM_NULL);
                return;
            }

            if (PopupService.Instance.Question(MessageWarning.UPDATE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            if (KhoiLuongGiaCongBUS.Instance.Update(khoiLuongGiaCong) != null)
            {
                PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.UPDATE_ERROR);
        }

        private void btnXoaLichSu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhoiLuongGiaCong khoiLuongGiaCong = GetKhoiLuongGiaCongOfForm();

            if (khoiLuongGiaCong == null || IsValidMaKhoiLuongGiaCong())
            {
                PopupService.Instance.Error(MessageError.DATA_OF_FORM_NULL);
                return;
            }

            if (PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW) == DialogResult.No)
            {
                return;
            }

            if (KhoiLuongGiaCongBUS.Instance.DeleteById(int.Parse(txtMaLichSuGiaCong.Text)) != null)
            {
                PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                ClearForm();
                LoadData();
                return;
            }

            PopupService.Instance.Error(MessageError.DELETE_ERROR);
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearForm();
        }

        #region Tính KVL khi nhập KLĐ và KLT
        double tongKhoiLuongTinh = 0;
        double tongKhoiLuongDa = 0;

        private void txtChiTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtPhanTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtLyTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtChiDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtPhanDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtLyDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }
        #endregion

        private void grcLichSuGiaCong_DoubleClick(object sender, EventArgs e)
        {
            KhoiLuongGiaCong khoiLuongGiaCong = (KhoiLuongGiaCong)grvLichSuGiaCong.GetFocusedRow();
            HienThiChiTietKhoiLuongGiaCong(khoiLuongGiaCong);
        }

        private void grvLichSuGiaCong_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void grvKhoCuTongHop_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnXuatFileExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string pathSave;
            string fileName = "";
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.ShowNewFolderButton = true;
            folder.Description = "Chọn vị trí lưu file!";
            if (folder.ShowDialog() == DialogResult.OK)
            {
                pathSave = folder.SelectedPath + "\\";
                if (InputBox.Show("Thông báo!",
                "&Nhập tên file:", ref fileName) == DialogResult.OK)
                {
                    grcKhoCuTongHop.ExportToXls(pathSave + fileName + "-KhoiLuongVang-" + DateTime.Now.ToString("dd/MM/yyyy") + ".xls");
                    grcLichSuGiaCong.ExportToXls(pathSave + fileName + "-LichSuGiaCong-" + DateTime.Now.ToString("dd/MM/yyyy") + ".xls");
                }
            }
        }
    }
}