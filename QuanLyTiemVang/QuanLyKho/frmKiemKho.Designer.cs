﻿namespace QuanLyTiemVang.QuanLyKho
{
    partial class frmKiemKho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtTongKhoiLuongVang = new DevExpress.XtraEditors.TextEdit();
            this.txtTongKhoiLuongDa = new DevExpress.XtraEditors.TextEdit();
            this.txtTongKhoiLuongTinh = new DevExpress.XtraEditors.TextEdit();
            this.txtTongTienCong = new DevExpress.XtraEditors.TextEdit();
            this.txtSoSanPhamDaKiem = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.grcSanPhamChuaKiem = new DevExpress.XtraGrid.GridControl();
            this.sanPhamBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvSanPhamChuaKiem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenSanPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaTem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTiLeHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaChuyenKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnKiemHang = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.txtTimKiemSanPhamChuaKiem = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.grcSanPhamDaKiem = new DevExpress.XtraGrid.GridControl();
            this.grvSanPhamDaKiem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenSanPham1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaTem1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTienCong1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHamLuongVang1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTiLeHot1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongTinh1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongDa1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVang1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieu1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaChuyenKho1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnHuySanPhamKiemKho = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.txtTimKiemSanPhamDaKiem = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongKhoiLuongVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongKhoiLuongDa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongKhoiLuongTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienCong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoSanPhamDaKiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamChuaKiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamChuaKiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnKiemHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimKiemSanPhamChuaKiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamDaKiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamDaKiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHuySanPhamKiemKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimKiemSanPhamDaKiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnLamMoiDuLieu,
            this.barButtonItem14});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 10;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(922, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 525);
            this.barDockControlBottom.Size = new System.Drawing.Size(922, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 485);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(922, 40);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 485);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 40);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(922, 89);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Thông tin kiểm kho";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtTongKhoiLuongVang);
            this.layoutControl1.Controls.Add(this.txtTongKhoiLuongDa);
            this.layoutControl1.Controls.Add(this.txtTongKhoiLuongTinh);
            this.layoutControl1.Controls.Add(this.txtTongTienCong);
            this.layoutControl1.Controls.Add(this.txtSoSanPhamDaKiem);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 20);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(918, 67);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtTongKhoiLuongVang
            // 
            this.txtTongKhoiLuongVang.Location = new System.Drawing.Point(709, 36);
            this.txtTongKhoiLuongVang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTongKhoiLuongVang.MenuManager = this.barManager1;
            this.txtTongKhoiLuongVang.Name = "txtTongKhoiLuongVang";
            this.txtTongKhoiLuongVang.Properties.Mask.EditMask = "f1";
            this.txtTongKhoiLuongVang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongKhoiLuongVang.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongKhoiLuongVang.Properties.NullText = "0";
            this.txtTongKhoiLuongVang.Properties.ReadOnly = true;
            this.txtTongKhoiLuongVang.Size = new System.Drawing.Size(180, 20);
            this.txtTongKhoiLuongVang.StyleController = this.layoutControl1;
            this.txtTongKhoiLuongVang.TabIndex = 9;
            // 
            // txtTongKhoiLuongDa
            // 
            this.txtTongKhoiLuongDa.Location = new System.Drawing.Point(418, 36);
            this.txtTongKhoiLuongDa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTongKhoiLuongDa.MenuManager = this.barManager1;
            this.txtTongKhoiLuongDa.Name = "txtTongKhoiLuongDa";
            this.txtTongKhoiLuongDa.Properties.Mask.EditMask = "f1";
            this.txtTongKhoiLuongDa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongKhoiLuongDa.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongKhoiLuongDa.Properties.NullText = "0";
            this.txtTongKhoiLuongDa.Properties.ReadOnly = true;
            this.txtTongKhoiLuongDa.Size = new System.Drawing.Size(180, 20);
            this.txtTongKhoiLuongDa.StyleController = this.layoutControl1;
            this.txtTongKhoiLuongDa.TabIndex = 8;
            // 
            // txtTongKhoiLuongTinh
            // 
            this.txtTongKhoiLuongTinh.Location = new System.Drawing.Point(119, 36);
            this.txtTongKhoiLuongTinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTongKhoiLuongTinh.MenuManager = this.barManager1;
            this.txtTongKhoiLuongTinh.Name = "txtTongKhoiLuongTinh";
            this.txtTongKhoiLuongTinh.Properties.Mask.EditMask = "f1";
            this.txtTongKhoiLuongTinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongKhoiLuongTinh.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongKhoiLuongTinh.Properties.NullText = "0";
            this.txtTongKhoiLuongTinh.Properties.ReadOnly = true;
            this.txtTongKhoiLuongTinh.Size = new System.Drawing.Size(188, 20);
            this.txtTongKhoiLuongTinh.StyleController = this.layoutControl1;
            this.txtTongKhoiLuongTinh.TabIndex = 6;
            // 
            // txtTongTienCong
            // 
            this.txtTongTienCong.Location = new System.Drawing.Point(559, 12);
            this.txtTongTienCong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTongTienCong.MenuManager = this.barManager1;
            this.txtTongTienCong.Name = "txtTongTienCong";
            this.txtTongTienCong.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTongTienCong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongTienCong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongTienCong.Properties.NullText = "0";
            this.txtTongTienCong.Properties.ReadOnly = true;
            this.txtTongTienCong.Size = new System.Drawing.Size(330, 20);
            this.txtTongTienCong.StyleController = this.layoutControl1;
            this.txtTongTienCong.TabIndex = 5;
            // 
            // txtSoSanPhamDaKiem
            // 
            this.txtSoSanPhamDaKiem.Location = new System.Drawing.Point(119, 12);
            this.txtSoSanPhamDaKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSoSanPhamDaKiem.MenuManager = this.barManager1;
            this.txtSoSanPhamDaKiem.Name = "txtSoSanPhamDaKiem";
            this.txtSoSanPhamDaKiem.Properties.Mask.EditMask = "n0";
            this.txtSoSanPhamDaKiem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoSanPhamDaKiem.Properties.NullText = "0";
            this.txtSoSanPhamDaKiem.Properties.ReadOnly = true;
            this.txtSoSanPhamDaKiem.Size = new System.Drawing.Size(329, 20);
            this.txtSoSanPhamDaKiem.StyleController = this.layoutControl1;
            this.txtSoSanPhamDaKiem.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(901, 68);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtSoSanPhamDaKiem;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem1.Text = "Số sản phẩm đã kiểm";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTongTienCong;
            this.layoutControlItem2.Location = new System.Drawing.Point(440, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItem2.Text = "Tổng tiền công";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtTongKhoiLuongTinh;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(299, 24);
            this.layoutControlItem3.Text = "Tổng khối lượng tịnh";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtTongKhoiLuongDa;
            this.layoutControlItem5.Location = new System.Drawing.Point(299, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(291, 24);
            this.layoutControlItem5.Text = "Tổng khối lượng đá";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTongKhoiLuongVang;
            this.layoutControlItem4.Location = new System.Drawing.Point(590, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(291, 24);
            this.layoutControlItem4.Text = "Tổng khối lượng vàng";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(103, 13);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.layoutControl2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 129);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(922, 226);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Sản phẩm chưa kiểm";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.grcSanPhamChuaKiem);
            this.layoutControl2.Controls.Add(this.txtTimKiemSanPhamChuaKiem);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 20);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(918, 204);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // grcSanPhamChuaKiem
            // 
            this.grcSanPhamChuaKiem.DataSource = this.sanPhamBindingSource;
            this.grcSanPhamChuaKiem.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamChuaKiem.Location = new System.Drawing.Point(12, 36);
            this.grcSanPhamChuaKiem.MainView = this.grvSanPhamChuaKiem;
            this.grcSanPhamChuaKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamChuaKiem.MenuManager = this.barManager1;
            this.grcSanPhamChuaKiem.Name = "grcSanPhamChuaKiem";
            this.grcSanPhamChuaKiem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnKiemHang});
            this.grcSanPhamChuaKiem.Size = new System.Drawing.Size(894, 156);
            this.grcSanPhamChuaKiem.TabIndex = 5;
            this.grcSanPhamChuaKiem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamChuaKiem});
            // 
            // sanPhamBindingSource
            // 
            this.sanPhamBindingSource.DataSource = typeof(DTO.Models.SanPham);
            // 
            // grvSanPhamChuaKiem
            // 
            this.grvSanPhamChuaKiem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenSanPham,
            this.colMaTem,
            this.colTienCong,
            this.colHamLuongVang,
            this.colTiLeHot,
            this.colKhoiLuongTinh,
            this.colKhoiLuongDa,
            this.colKhoiLuongVang,
            this.colKiHieu,
            this.colMaChuyenKho,
            this.gridColumn1});
            this.grvSanPhamChuaKiem.GridControl = this.grcSanPhamChuaKiem;
            this.grvSanPhamChuaKiem.Name = "grvSanPhamChuaKiem";
            this.grvSanPhamChuaKiem.OptionsBehavior.ReadOnly = true;
            this.grvSanPhamChuaKiem.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamChuaKiem.OptionsView.ShowGroupPanel = false;
            this.grvSanPhamChuaKiem.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvSanPhamChuaKiem_CustomDrawRowIndicator);
            // 
            // colTenSanPham
            // 
            this.colTenSanPham.Caption = "Tên";
            this.colTenSanPham.FieldName = "TenSanPham";
            this.colTenSanPham.Name = "colTenSanPham";
            this.colTenSanPham.Visible = true;
            this.colTenSanPham.VisibleIndex = 0;
            this.colTenSanPham.Width = 95;
            // 
            // colMaTem
            // 
            this.colMaTem.Caption = "Mã tem";
            this.colMaTem.FieldName = "MaTem";
            this.colMaTem.Name = "colMaTem";
            this.colMaTem.Visible = true;
            this.colMaTem.VisibleIndex = 1;
            this.colMaTem.Width = 95;
            // 
            // colTienCong
            // 
            this.colTienCong.Caption = "Tiền công";
            this.colTienCong.FieldName = "TienCong";
            this.colTienCong.Name = "colTienCong";
            this.colTienCong.Visible = true;
            this.colTienCong.VisibleIndex = 2;
            this.colTienCong.Width = 95;
            // 
            // colHamLuongVang
            // 
            this.colHamLuongVang.Caption = "HLV";
            this.colHamLuongVang.FieldName = "HamLuongVang";
            this.colHamLuongVang.Name = "colHamLuongVang";
            this.colHamLuongVang.Visible = true;
            this.colHamLuongVang.VisibleIndex = 3;
            this.colHamLuongVang.Width = 84;
            // 
            // colTiLeHot
            // 
            this.colTiLeHot.Caption = "Tỉ lệ hột";
            this.colTiLeHot.FieldName = "TiLeHot";
            this.colTiLeHot.Name = "colTiLeHot";
            this.colTiLeHot.Visible = true;
            this.colTiLeHot.VisibleIndex = 4;
            this.colTiLeHot.Width = 95;
            // 
            // colKhoiLuongTinh
            // 
            this.colKhoiLuongTinh.Caption = "KLT";
            this.colKhoiLuongTinh.FieldName = "KhoiLuongTinh";
            this.colKhoiLuongTinh.Name = "colKhoiLuongTinh";
            this.colKhoiLuongTinh.Visible = true;
            this.colKhoiLuongTinh.VisibleIndex = 5;
            this.colKhoiLuongTinh.Width = 95;
            // 
            // colKhoiLuongDa
            // 
            this.colKhoiLuongDa.Caption = "KLĐ";
            this.colKhoiLuongDa.FieldName = "KhoiLuongDa";
            this.colKhoiLuongDa.Name = "colKhoiLuongDa";
            this.colKhoiLuongDa.Visible = true;
            this.colKhoiLuongDa.VisibleIndex = 6;
            this.colKhoiLuongDa.Width = 95;
            // 
            // colKhoiLuongVang
            // 
            this.colKhoiLuongVang.Caption = "KLV";
            this.colKhoiLuongVang.FieldName = "KhoiLuongVang";
            this.colKhoiLuongVang.Name = "colKhoiLuongVang";
            this.colKhoiLuongVang.Visible = true;
            this.colKhoiLuongVang.VisibleIndex = 7;
            this.colKhoiLuongVang.Width = 95;
            // 
            // colKiHieu
            // 
            this.colKiHieu.Caption = "Kí hiệu";
            this.colKiHieu.FieldName = "KiHieu";
            this.colKiHieu.Name = "colKiHieu";
            this.colKiHieu.Visible = true;
            this.colKiHieu.VisibleIndex = 8;
            this.colKiHieu.Width = 95;
            // 
            // colMaChuyenKho
            // 
            this.colMaChuyenKho.Caption = "KH chuyển kho";
            this.colMaChuyenKho.FieldName = "MaChuyenKho";
            this.colMaChuyenKho.Name = "colMaChuyenKho";
            this.colMaChuyenKho.Visible = true;
            this.colMaChuyenKho.VisibleIndex = 9;
            this.colMaChuyenKho.Width = 99;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Duyệt";
            this.gridColumn1.ColumnEdit = this.btnKiemHang;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            this.gridColumn1.Width = 77;
            // 
            // btnKiemHang
            // 
            this.btnKiemHang.AutoHeight = false;
            this.btnKiemHang.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.accept_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnKiemHang.Name = "btnKiemHang";
            this.btnKiemHang.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnKiemHang.Click += new System.EventHandler(this.btnKiemHang_Click);
            // 
            // txtTimKiemSanPhamChuaKiem
            // 
            this.txtTimKiemSanPhamChuaKiem.Location = new System.Drawing.Point(105, 12);
            this.txtTimKiemSanPhamChuaKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTimKiemSanPhamChuaKiem.MenuManager = this.barManager1;
            this.txtTimKiemSanPhamChuaKiem.Name = "txtTimKiemSanPhamChuaKiem";
            this.txtTimKiemSanPhamChuaKiem.Properties.Mask.EditMask = "(\\p{Lu}|\\d)+";
            this.txtTimKiemSanPhamChuaKiem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTimKiemSanPhamChuaKiem.Size = new System.Drawing.Size(801, 20);
            this.txtTimKiemSanPhamChuaKiem.StyleController = this.layoutControl2;
            this.txtTimKiemSanPhamChuaKiem.TabIndex = 4;
            this.txtTimKiemSanPhamChuaKiem.EditValueChanged += new System.EventHandler(this.txtTimKiemSanPhamChuaKiem_EditValueChanged);
            this.txtTimKiemSanPhamChuaKiem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimKiemSanPhamChuaKiem_KeyDown);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(918, 204);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.grcSanPhamChuaKiem;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(898, 160);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtTimKiemSanPhamChuaKiem;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(898, 24);
            this.layoutControlItem6.Text = "Tìm kiếm sản phẩm";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(89, 13);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 355);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(922, 5);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.layoutControl3);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 360);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(922, 165);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "Sản phẩm đã kiểm";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.grcSanPhamDaKiem);
            this.layoutControl3.Controls.Add(this.txtTimKiemSanPhamDaKiem);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 20);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(918, 143);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // grcSanPhamDaKiem
            // 
            this.grcSanPhamDaKiem.DataSource = this.sanPhamBindingSource;
            this.grcSanPhamDaKiem.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamDaKiem.Location = new System.Drawing.Point(12, 36);
            this.grcSanPhamDaKiem.MainView = this.grvSanPhamDaKiem;
            this.grcSanPhamDaKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamDaKiem.MenuManager = this.barManager1;
            this.grcSanPhamDaKiem.Name = "grcSanPhamDaKiem";
            this.grcSanPhamDaKiem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnHuySanPhamKiemKho});
            this.grcSanPhamDaKiem.Size = new System.Drawing.Size(894, 95);
            this.grcSanPhamDaKiem.TabIndex = 5;
            this.grcSanPhamDaKiem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamDaKiem});
            // 
            // grvSanPhamDaKiem
            // 
            this.grvSanPhamDaKiem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenSanPham1,
            this.colMaTem1,
            this.colTienCong1,
            this.colHamLuongVang1,
            this.colTiLeHot1,
            this.colKhoiLuongTinh1,
            this.colKhoiLuongDa1,
            this.colKhoiLuongVang1,
            this.colKiHieu1,
            this.colMaChuyenKho1,
            this.gridColumn2});
            this.grvSanPhamDaKiem.GridControl = this.grcSanPhamDaKiem;
            this.grvSanPhamDaKiem.Name = "grvSanPhamDaKiem";
            this.grvSanPhamDaKiem.OptionsBehavior.ReadOnly = true;
            this.grvSanPhamDaKiem.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamDaKiem.OptionsView.ShowGroupPanel = false;
            this.grvSanPhamDaKiem.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvSanPhamDaKiem_CustomDrawRowIndicator);
            // 
            // colTenSanPham1
            // 
            this.colTenSanPham1.Caption = "Tên";
            this.colTenSanPham1.FieldName = "TenSanPham";
            this.colTenSanPham1.Name = "colTenSanPham1";
            this.colTenSanPham1.Visible = true;
            this.colTenSanPham1.VisibleIndex = 0;
            // 
            // colMaTem1
            // 
            this.colMaTem1.Caption = "Mã tem";
            this.colMaTem1.FieldName = "MaTem";
            this.colMaTem1.Name = "colMaTem1";
            this.colMaTem1.Visible = true;
            this.colMaTem1.VisibleIndex = 1;
            // 
            // colTienCong1
            // 
            this.colTienCong1.Caption = "Tiền công";
            this.colTienCong1.FieldName = "TienCong";
            this.colTienCong1.Name = "colTienCong1";
            this.colTienCong1.Visible = true;
            this.colTienCong1.VisibleIndex = 2;
            // 
            // colHamLuongVang1
            // 
            this.colHamLuongVang1.Caption = "HLV";
            this.colHamLuongVang1.FieldName = "HamLuongVang";
            this.colHamLuongVang1.Name = "colHamLuongVang1";
            this.colHamLuongVang1.Visible = true;
            this.colHamLuongVang1.VisibleIndex = 3;
            // 
            // colTiLeHot1
            // 
            this.colTiLeHot1.Caption = "Tỉ lệ hột";
            this.colTiLeHot1.FieldName = "TiLeHot";
            this.colTiLeHot1.Name = "colTiLeHot1";
            this.colTiLeHot1.Visible = true;
            this.colTiLeHot1.VisibleIndex = 4;
            // 
            // colKhoiLuongTinh1
            // 
            this.colKhoiLuongTinh1.Caption = "KLT";
            this.colKhoiLuongTinh1.FieldName = "KhoiLuongTinh";
            this.colKhoiLuongTinh1.Name = "colKhoiLuongTinh1";
            this.colKhoiLuongTinh1.Visible = true;
            this.colKhoiLuongTinh1.VisibleIndex = 5;
            // 
            // colKhoiLuongDa1
            // 
            this.colKhoiLuongDa1.Caption = "KLĐ";
            this.colKhoiLuongDa1.FieldName = "KhoiLuongDa";
            this.colKhoiLuongDa1.Name = "colKhoiLuongDa1";
            this.colKhoiLuongDa1.Visible = true;
            this.colKhoiLuongDa1.VisibleIndex = 6;
            // 
            // colKhoiLuongVang1
            // 
            this.colKhoiLuongVang1.Caption = "KLV";
            this.colKhoiLuongVang1.FieldName = "KhoiLuongVang";
            this.colKhoiLuongVang1.Name = "colKhoiLuongVang1";
            this.colKhoiLuongVang1.Visible = true;
            this.colKhoiLuongVang1.VisibleIndex = 7;
            // 
            // colKiHieu1
            // 
            this.colKiHieu1.Caption = "Kí hiệu";
            this.colKiHieu1.FieldName = "KiHieu";
            this.colKiHieu1.Name = "colKiHieu1";
            this.colKiHieu1.Visible = true;
            this.colKiHieu1.VisibleIndex = 8;
            // 
            // colMaChuyenKho1
            // 
            this.colMaChuyenKho1.Caption = "KH chuyển kho";
            this.colMaChuyenKho1.FieldName = "MaChuyenKho";
            this.colMaChuyenKho1.Name = "colMaChuyenKho1";
            this.colMaChuyenKho1.Visible = true;
            this.colMaChuyenKho1.VisibleIndex = 9;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Hủy";
            this.gridColumn2.ColumnEdit = this.btnHuySanPhamKiemKho;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 10;
            // 
            // btnHuySanPhamKiemKho
            // 
            this.btnHuySanPhamKiemKho.AutoHeight = false;
            this.btnHuySanPhamKiemKho.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnHuySanPhamKiemKho.Name = "btnHuySanPhamKiemKho";
            this.btnHuySanPhamKiemKho.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnHuySanPhamKiemKho.Click += new System.EventHandler(this.btnHuySanPhamKiemKho_Click);
            // 
            // txtTimKiemSanPhamDaKiem
            // 
            this.txtTimKiemSanPhamDaKiem.Location = new System.Drawing.Point(105, 12);
            this.txtTimKiemSanPhamDaKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTimKiemSanPhamDaKiem.MenuManager = this.barManager1;
            this.txtTimKiemSanPhamDaKiem.Name = "txtTimKiemSanPhamDaKiem";
            this.txtTimKiemSanPhamDaKiem.Properties.Mask.EditMask = "(\\p{Lu}|\\d)+";
            this.txtTimKiemSanPhamDaKiem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTimKiemSanPhamDaKiem.Size = new System.Drawing.Size(801, 20);
            this.txtTimKiemSanPhamDaKiem.StyleController = this.layoutControl3;
            this.txtTimKiemSanPhamDaKiem.TabIndex = 4;
            this.txtTimKiemSanPhamDaKiem.EditValueChanged += new System.EventHandler(this.txtTimKiemSanPhamDaKiem_EditValueChanged);
            this.txtTimKiemSanPhamDaKiem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimKiemSanPhamDaKiem_KeyDown);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(918, 143);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtTimKiemSanPhamDaKiem;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(898, 24);
            this.layoutControlItem8.Text = "Tìm kiếm sản phẩm";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(89, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.grcSanPhamDaKiem;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(898, 99);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // frmKiemKho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 525);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmKiemKho";
            this.Text = "Kiểm kho";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmKiemKho_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongKhoiLuongVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongKhoiLuongDa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongKhoiLuongTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienCong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoSanPhamDaKiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamChuaKiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamChuaKiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnKiemHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimKiemSanPhamChuaKiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamDaKiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamDaKiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHuySanPhamKiemKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimKiemSanPhamDaKiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl grcSanPhamDaKiem;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamDaKiem;
        private DevExpress.XtraEditors.TextEdit txtTimKiemSanPhamDaKiem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl grcSanPhamChuaKiem;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamChuaKiem;
        private DevExpress.XtraEditors.TextEdit txtTimKiemSanPhamChuaKiem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTongKhoiLuongVang;
        private DevExpress.XtraEditors.TextEdit txtTongKhoiLuongDa;
        private DevExpress.XtraEditors.TextEdit txtTongKhoiLuongTinh;
        private DevExpress.XtraEditors.TextEdit txtTongTienCong;
        private DevExpress.XtraEditors.TextEdit txtSoSanPhamDaKiem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource sanPhamBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTenSanPham1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaTem1;
        private DevExpress.XtraGrid.Columns.GridColumn colTienCong1;
        private DevExpress.XtraGrid.Columns.GridColumn colHamLuongVang1;
        private DevExpress.XtraGrid.Columns.GridColumn colTiLeHot1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTinh1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongDa1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVang1;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieu1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaChuyenKho1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnHuySanPhamKiemKho;
        private DevExpress.XtraGrid.Columns.GridColumn colTenSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn colMaTem;
        private DevExpress.XtraGrid.Columns.GridColumn colTienCong;
        private DevExpress.XtraGrid.Columns.GridColumn colHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colTiLeHot;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongDa;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieu;
        private DevExpress.XtraGrid.Columns.GridColumn colMaChuyenKho;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnKiemHang;
    }
}