﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.InTem;
using DevExpress.XtraReports.UI;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using DTO.TemporaryModel;
using Share.Constant;
using QuanLyTiemVang.Constants;
using Share.Util;

namespace QuanLyTiemVang
{
    public partial class frmNhapKho : DevExpress.XtraEditors.XtraForm
    {
        public frmNhapKho()
        {
            InitializeComponent();
        }

        List<string> listNameNotNeedClear = new List<string>
        {
            "detNgayNhap", "txtTenNhanVien"
        };

        private bool IsValidSanPhamChung()
        {
            dxerTenSanPham.Dispose();
            dxerHamLuongVang.Dispose();
            dxerNgayNhapKho.Dispose();
            dxerKiHieuChuyenKho.Dispose();
            dxerLyTinh.Dispose();
            dxerLyVang.Dispose();
            dxerLyDa.Dispose();

            bool check = true;
            double outValue;

            if (string.IsNullOrWhiteSpace(txtTenSanPham.Text))
            {
                dxerTenSanPham.SetError(txtTenSanPham, ValidationUltils.TEN_SAN_PHAM);
                check = false;
            }

            if (lueHamLuongVang.EditValue == null)
            {
                dxerHamLuongVang.SetError(lueHamLuongVang, ValidationUltils.HAM_LUONG_VANG);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(detNgayNhap.Text))
            {
                dxerNgayNhapKho.SetError(detNgayNhap, ValidationUltils.NGAY_NHAP_KHO);
                check = false;
            }

            if (!string.IsNullOrWhiteSpace(txtKiHieuChuyenKho.Text) && !txtKiHieuChuyenKho.Text.Equals("*"))
            {
                dxerKiHieuChuyenKho.SetError(txtKiHieuChuyenKho, ValidationUltils.KI_HIEU_CHUYEN_KHO_SAI_FORMAT);
                check = false;
            }

            if (!double.TryParse(txtLyTinh.Text, out outValue))
            {
                dxerLyTinh.SetError(txtLyTinh, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyDa.Text, out outValue))
            {
                dxerLyDa.SetError(txtLyDa, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            if (!double.TryParse(txtLyVang.Text, out outValue))
            {
                dxerLyVang.SetError(txtLyVang, ValidationUltils.TI_LE_HOT_NAN);
                check = false;
            }

            return check;
        }

        private bool IsValidSanPham()
        {
            dxerMaTem.Dispose();
            dxerKiHieu.Dispose();
            dxerNhaCungCap.Dispose();

            bool check = IsValidSanPhamChung();

            if (lueKiHieu.EditValue == null)
            {
                dxerKiHieu.SetError(lueKiHieu, ValidationUltils.KI_HIEU);
                check = false;
            }

            if (lueNhaCungCap.EditValue == null)
            {
                dxerNhaCungCap.SetError(lueNhaCungCap, ValidationUltils.NHA_CUNG_CAP);
                check = false;
            }

            if (!string.IsNullOrWhiteSpace(txtMaTem.Text) && txtMaTem.Text.Length < 5)
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_INVALID_LENGHT);
                check = false;
            }
            else if (!string.IsNullOrWhiteSpace(txtMaTem.Text)
                && !Util.CheckMaTemValid(txtMaTem.Text))
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_SAI_CU_PHAP);
                check = false;
            }
            else if (!string.IsNullOrWhiteSpace(txtMaTem.Text) 
                && !string.IsNullOrWhiteSpace(txtTenSanPham.Text)
                && !txtMaTem.Text[0].ToString().ToUpper().Equals(txtTenSanPham.Text[0].ToString().ToUpper()))
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.MA_TEM_INVALID_CHAR_0);
                check = false;
            }
            else if (!string.IsNullOrWhiteSpace(txtMaTem.Text)
                && SanPhamBUS.Instance.IsDuplicateMaTem(txtMaTem.Text))
            {
                dxerMaTem.SetError(txtMaTem, ValidationUltils.TRUNG_MA_TEM);
                check = false;
            }

            return check;
        }

        private bool IsValidSanPhamKhoCu()
        {
            return IsValidSanPhamChung();
        }

        private bool IsValidInTem()
        {
            if (string.IsNullOrWhiteSpace(txtMaTem.Text))
            {
                PopupService.Instance.Error(ValidationUltils.MA_TEM_EMPTY);
                return false;
            }

            return true;
        }

        private bool IsValidNhapKho()
        {
            int soluong = 1;

            if (!string.IsNullOrWhiteSpace(txtSoLuong.Text))
            {
                soluong = int.Parse(txtSoLuong.Text);
            }

            if (soluong > 50)
            {
                PopupService.Instance.Error(ValidationUltils.SO_LUONG_LON_HON_50);
                return false;
            }

            if (soluong > 1 && !string.IsNullOrWhiteSpace(txtMaTem.Text))
            {
                PopupService.Instance.Error(ValidationUltils.SO_LUONG_LON_HON_1_KHI_CO_MA_TEM);
                return false;
            }

            if (soluong > 10)
            {
                var result = PopupService.Instance.Question("Bạn có muốn nhập " + txtSoLuong.Text + " sản phẩm cùng loại?");

                if (result == DialogResult.No)
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsValidNhapKhoCu()
        {
            int soluong = 1;

            if (!string.IsNullOrWhiteSpace(txtSoLuong.Text))
            {
                soluong = int.Parse(txtSoLuong.Text);
            }

            if (soluong > 50)
            {
                PopupService.Instance.Error(ValidationUltils.SO_LUONG_LON_HON_50);
                return false;
            }

            if (soluong > 10)
            {
                var result = PopupService.Instance.Question("Bạn có muốn nhập " + txtSoLuong.Text + " sản phẩm cùng loại?");

                if (result == DialogResult.No)
                {
                    return false;
                }
            }

            return true;
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearFormService.ClearAllForm(this, listNameNotNeedClear);
        }

        private void frmNhapKho_Load_1(object sender, EventArgs e)
        {
            txtTenNhanVien.Text = Form1.nhanVien.Ten;
            detNgayNhap.EditValue = DateTime.Now;
            LoadData();
        }

        private void LoadData()
        {
            KiHieuSanPhamBUS.Instance.GetLookupEdit(lueKiHieu);
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVang);
            NhaCungCapBUS.Instance.GetLookupEdit(lueNhaCungCap);
            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
        }

        private SanPham LayThongTinSanPhamTuForm()
        {
            if (IsValidSanPham())
            {
                return new SanPham
                {
                    HamLuongVang = lueHamLuongVang.Text,
                    MaHamLuongVang = Convert.ToInt32(lueHamLuongVang.EditValue),
                    KhoiLuongDa = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiDa, txtPhanDa, txtLyDa),
                    KhoiLuongTinh = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiTinh, txtPhanTinh, txtLyTinh),
                    KhoiLuongVang = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiVang, txtPhanVang, txtLyVang),
                    KiHieu = lueKiHieu.Text,
                    MaNhaCungCap = lueNhaCungCap.EditValue == null ? (int?)null : (int)lueNhaCungCap.EditValue,
                    MaNhanVien = Form1.nhanVien.MaNhanVien,
                    MaTem = txtMaTem.Text,
                    TenSanPham = txtTenSanPham.Text,
                    TienCong = txtTienCong.EditValue == null ? 0 : (int)txtTienCong.EditValue,
                    TiLeHot = txtTiLeHot.EditValue == null ? 0 : Convert.ToDouble(txtTiLeHot.EditValue),
                    NgayNhap = detNgayNhap.DateTime.Date
                };
            }

            return null;
        }

        private SanPhamKhoCu LayThongTinSanPhamKhoCuTuForm()
        {
            if (IsValidSanPhamKhoCu())
            {
                int? maNhaCungCap = null;

                if (lueNhaCungCap.EditValue != null)
                {
                    maNhaCungCap = (int)lueNhaCungCap.EditValue;
                }

                return new SanPhamKhoCu
                {
                    TenSanPham = txtTenSanPham.Text,
                    NgayNhap = detNgayNhap.DateTime,
                    KhoiLuongTinh = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiTinh, txtPhanTinh, txtLyTinh),
                    KhoiLuongDa = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiDa, txtPhanDa, txtLyDa),
                    KhoiLuongVang = Calculator.Instance.ConvertKhoiLuongTuFormToDouble(txtChiVang, txtPhanVang, txtLyVang),
                    GiaMuaVao = 0,
                    MaHamLuongVang = (int)lueHamLuongVang.EditValue,
                    LoaiVangMuaVao = Constant.VANG_CU_MUA_VAO,
                    MaNhaCungCap = maNhaCungCap,
                    MaNhanVien = Form1.nhanVien.MaNhanVien,
                    DonGiaMuaVao = 0,
                    TiLeHot = 0
                };
            }

            return null;
        }

        public SanPham CheckKiHieuChuyenKho(SanPham sanPham)
        {
            if (!string.IsNullOrWhiteSpace(txtKiHieuChuyenKho.Text))
            {
                sanPham.IsSanPhamChuyenKho = true;
            }

            return sanPham;
        }

        private bool NhapSanPhamVaoKhoCu()
        {
            if (IsValidNhapKhoCu())
            {
                SanPhamKhoCu sanPhamNhapKho = LayThongTinSanPhamKhoCuTuForm();
                int soLuong = int.Parse(txtSoLuong.Text);

                if (soLuong < 1)
                {
                    soLuong = 1;
                }

                if (sanPhamNhapKho == null)
                {
                    PopupService.Instance.Error(MessageError.SAN_PHAM_NULL);
                    return false;
                }

                if (SanPhamKhoCuBUS.Instance.Insert(sanPhamNhapKho, soLuong))
                {
                    PopupService.Instance.Success(MessageSucces.NHAP_KHO_CU);
                    ClearFormService.ClearAllForm(this, listNameNotNeedClear);

                    return true;
                }
                else
                {
                    PopupService.Instance.Error(MessageError.NHAP_KHO_CU);
                }
            }

            return false;
        }

        private List<SanPham> NhapSanPhamVaoKho()
        {
            if (IsValidNhapKho())
            {
                SanPham sanPhamNhapKho = LayThongTinSanPhamTuForm();
                int soLuong = int.Parse(txtSoLuong.Text);

                if (soLuong < 1)
                {
                    soLuong = 1;
                }

                if (sanPhamNhapKho == null)
                {
                    PopupService.Instance.Error(Constants.MessageError.SAN_PHAM_NULL);
                    return null;
                }

                sanPhamNhapKho = CheckKiHieuChuyenKho(sanPhamNhapKho);
                sanPhamNhapKho = Calculator.Instance.TinhGiaVangBanTheoKhoiLuongVang(sanPhamNhapKho);

                if (sanPhamNhapKho == null)
                {
                    PopupService.Instance.Error(Constants.MessageError.GIA_VANG_LOI);
                    return null;
                }

                List<SanPham> listInserted = SanPhamBUS.Instance.NhapSanPhamVaoKho(sanPhamNhapKho, soLuong);

                if (listInserted != null)
                {
                    PopupService.Instance.Success(MessageSucces.NHAP_KHO_MOI);
                    ClearFormService.ClearAllForm(this, listNameNotNeedClear);
                }
                else
                {
                    PopupService.Instance.Error(MessageError.NHAP_KHO_MOI);
                }

                return listInserted;
            }

            return null;
        }

        private void btnNhapKhoMoi_Click(object sender, EventArgs e)
        {
            if (NhapSanPhamVaoKho() == null)
            {
                return;
            }

            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnNhapKhoCu_Click(object sender, EventArgs e)
        {
            if (!NhapSanPhamVaoKhoCu())
            {
                return;
            }

            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
        }

        private void btnInTem_Click(object sender, EventArgs e)
        {
            SanPham sanPham = LayThongTinSanPhamTuForm();
            
            sanPham.NhaCungCap = NhaCungCapBUS.Instance.GetById((int)sanPham.MaNhaCungCap);

            if (IsValidInTem() && sanPham != null)
            {
                TemVang temVang = new TemVang();
                temVang.DataSource = new SanPhamInTem[] { BluePrint.Instance.ConvertToSanPhamInTem(sanPham) };
                temVang.ShowPreviewDialog();
            }
        }

        private void btnNhapKhoMoiVaInTem_Click(object sender, EventArgs e)
        {
            List<SanPham> listSanPhamInserted = NhapSanPhamVaoKho();

            if (listSanPhamInserted == null)
            {
                return;
            }

            List<SanPhamInTem> sanPhamInTems = new List<SanPhamInTem>();

            foreach (SanPham sanPham in listSanPhamInserted)
            {
                sanPham.NhaCungCap = NhaCungCapBUS.Instance.GetById((int)sanPham.MaNhaCungCap);

                sanPhamInTems.Add(BluePrint.Instance.ConvertToSanPhamInTem(sanPham));
            }

            TemVang temVang = new TemVang();
            temVang.DataSource = sanPhamInTems;
            temVang.ShowPreviewDialog();
            AutoCompleteText.Instance.LoadAutoCompleteTenSanPham(txtTenSanPham);
        }

        #region Tính KVL khi nhập KLĐ và KLT
        double tongKhoiLuongTinh = 0;
        double tongKhoiLuongDa = 0;

        private void txtChiTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtPhanTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtLyTinh_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongTinh = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiTinh, txtPhanTinh, txtLyTinh);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtChiDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtPhanDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }

        private void txtLyDa_EditValueChanged(object sender, EventArgs e)
        {
            tongKhoiLuongDa = Calculator.Instance.TinhTongKhoiLuongTheoLy(txtChiDa, txtPhanDa, txtLyDa);
            Calculator.Instance.HienThiKhoiLuongVang(
                tongKhoiLuongTinh,
                tongKhoiLuongDa,
                txtChiVang, txtPhanVang, txtLyVang);
        }
        #endregion

        private void txtKhoiLuongTinhCan_EditValueChanged(object sender, EventArgs e)
        {
            double khoiLuongTinh = 0;

            if (double.TryParse(txtKhoiLuongTinhCan.Text, out khoiLuongTinh))
            {
                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    Util.LamTronKhoiLuongCan(khoiLuongTinh),
                    txtChiTinh, txtPhanTinh, txtLyTinh);
            }
        }

        private void txtKhoiLuongDaCan_EditValueChanged(object sender, EventArgs e)
        {
            double khoiLuongDa = 0;

            if (double.TryParse(txtKhoiLuongDaCan.Text, out khoiLuongDa))
            {
                Calculator.Instance.HienThiKhoiLuongTheoLyLenForm(
                    Util.LamTronKhoiLuongCan(khoiLuongDa),
                    txtChiDa, txtPhanDa, txtLyDa);
            }
        }
    }
}