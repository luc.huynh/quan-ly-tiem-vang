﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using ExcelDataReader;
using BUS;
using DTO.Models;
using Share.Constant;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using Share.Util;

namespace QuanLyTiemVang
{
    public partial class frmNhapKhoMoiExcel : DevExpress.XtraEditors.XtraForm
    {
        public frmNhapKhoMoiExcel()
        {
            InitializeComponent();
        }

        private void frmNhapKhoMoiExcel_Load(object sender, EventArgs e)
        {

        }

        DataSet result;

        private void btnChonTepTin_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Excel Workbook|*.xls", ValidateNames = true })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    FileStream fileStream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read);
                    IExcelDataReader reader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                    result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });

                    lueSheet.Properties.DataSource = null;
                    var listSheet = new List<string>();
                    foreach (DataTable dataTable in result.Tables)
                    {
                        listSheet.Add(dataTable.TableName);
                    }
                    lueSheet.Properties.DataSource = listSheet;
                    reader.Close();
                }
            }
        }

        private void lueSheet_EditValueChanged(object sender, EventArgs e)
        {
            grvSanPham.DataSource = result.Tables[lueSheet.EditValue.ToString()];
        }

        private double convertString(string a)
        {
            if (string.IsNullOrWhiteSpace(a))
            {
                return 0;
            }
            return double.Parse(a);
        }

        private int convertStringToInt(string a)
        {
            if (string.IsNullOrWhiteSpace(a))
            {
                return 0;
            }
            return int.Parse(a);
        }

        private string convertText(string a)
        {
            if (string.IsNullOrWhiteSpace(a))
            {
                return "";
            }
            return a;
        }

        private bool IsValid()
        {
            string sanPhamTrungMaTem = "";
            string sanPhamTrungMaSanPham = "";
            string sanPhamInvalidHamLuongVang = "";
            string hamLuongVangNotFound = "";
            string nhaCungCapNotFound = "";
            string sanPhamInvalidTenSanPham = "";
            string sanPhamInvalidKiHieu = "";
            string sanPhamInvalidKhoiLuongTinh = "";
            string sanPhamInvalidKhoiLuongDa = "";
            string sanPhamInvalidKhoiLuongVang = "";
            string sanPhamTrungMaSanPhamExcel = "";
            string sanPhamTrungMaTemExcel = "";
            string sanPhamSaiCuPhapMaExcel = "";
            bool isValid = true;
            List<string> listMaSanPhamExcel = new List<string>();
            List<string> listMaTemExcel = new List<string>();

            for (int i = 0; i < grcSanPham.DataRowCount; i++)
            {
                var item = grcSanPham.GetDataRow(i).ItemArray;
                string maSanPhamTemp = item[IndexNhapExcelConstant.MA_SP].ToString();
                string maTemTemp = item[IndexNhapExcelConstant.MA_TEM].ToString();

                if (!string.IsNullOrWhiteSpace(maSanPhamTemp))
                {
                    listMaSanPhamExcel.Add(maSanPhamTemp);
                }

                if (!string.IsNullOrWhiteSpace(maTemTemp))
                {
                    listMaTemExcel.Add(maTemTemp);
                }
            }

            for (int i = 0; i < grcSanPham.DataRowCount; i++)
            {
                var item = grcSanPham.GetDataRow(i).ItemArray;
                double outValue;
                string maSanPham = item[IndexNhapExcelConstant.MA_SP].ToString();
                string maTem = item[IndexNhapExcelConstant.MA_TEM].ToString();
                string hamLuongVang = item[IndexNhapExcelConstant.HAM_LUONG_VANG].ToString().ToUpper();
                string tenNhaCungCap = item[IndexNhapExcelConstant.NHA_CUNG_CAP].ToString();

                if ((!string.IsNullOrWhiteSpace(maSanPham) && !Util.CheckMaTemValid(maSanPham))
                    || (!string.IsNullOrWhiteSpace(maTem) && !Util.CheckMaTemValid(maTem)))
                {
                    sanPhamSaiCuPhapMaExcel += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (!string.IsNullOrWhiteSpace(maSanPham)
                    && SanPhamBUS.Instance.IsDuplicateMaSanPham(maSanPham))
                {
                    sanPhamTrungMaSanPham += maSanPham + ", ";
                    isValid = false;
                }

                if (!string.IsNullOrWhiteSpace(maSanPham)
                    && listMaSanPhamExcel.Where(x => x.Equals(maSanPham)).Count() > 1)
                {
                    sanPhamTrungMaSanPhamExcel += sanPhamTrungMaSanPhamExcel.Contains(maSanPham) ? "" : maSanPham + ", ";
                    isValid = false;
                }

                if (!string.IsNullOrWhiteSpace(maTem)
                   && listMaTemExcel.Where(x => x.Equals(maTem)).Count() > 1)
                {
                    sanPhamTrungMaTemExcel += sanPhamTrungMaTemExcel.Contains(maTem) ? "" : maTem + ", ";
                    isValid = false;
                }

                if (!string.IsNullOrWhiteSpace(maTem)
                    && SanPhamBUS.Instance.IsDuplicateMaTem(maTem))
                {
                    sanPhamTrungMaTem += maTem + ", ";
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(hamLuongVang))
                {
                    sanPhamInvalidHamLuongVang += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (!string.IsNullOrWhiteSpace(hamLuongVang)
                    && !KiHieuHamLuongVangBUS.Instance.HaveHamLuongVang(hamLuongVang))
                {
                    hamLuongVangNotFound += hamLuongVangNotFound.Contains(hamLuongVang) ? "" : hamLuongVang + ", ";
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(item[IndexNhapExcelConstant.TEN_SP].ToString()))
                {
                    sanPhamInvalidTenSanPham += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(item[IndexNhapExcelConstant.KI_HIEU].ToString()))
                {
                    sanPhamInvalidKiHieu += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (!double.TryParse(item[IndexNhapExcelConstant.KHOI_LUONG_TINH].ToString(), out outValue))
                {
                    sanPhamInvalidKhoiLuongTinh += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (!double.TryParse(item[IndexNhapExcelConstant.KHOI_LUONG_DA].ToString(), out outValue))
                {
                    sanPhamInvalidKhoiLuongDa += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (!double.TryParse(item[IndexNhapExcelConstant.KHOI_LUONG_VANG].ToString(), out outValue))
                {
                    sanPhamInvalidKhoiLuongVang += (i + 1).ToString() + ", ";
                    isValid = false;
                }

                if (!string.IsNullOrWhiteSpace(tenNhaCungCap)
                    && !NhaCungCapBUS.Instance.IsHaveNhaCungCap(tenNhaCungCap))
                {
                    nhaCungCapNotFound += nhaCungCapNotFound.Contains(tenNhaCungCap) ? "" : tenNhaCungCap + ", ";
                    isValid = false;
                }
            }

            if (sanPhamTrungMaSanPham.Length > 0)
            {
                PopupService.Instance.Error("Các mã sản phẩm: " + sanPhamTrungMaSanPham.TrimEnd(' ', ',') + "\nĐã tồn tại trong phần mềm");
            }

            if (sanPhamTrungMaTem.Length > 0)
            {
                PopupService.Instance.Error("Các mã tem: " + sanPhamTrungMaTem.TrimEnd(' ', ',') + "\nĐã tồn tại trong phần mềm");
            }

            if (sanPhamInvalidHamLuongVang.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamInvalidHamLuongVang.TrimEnd(' ', ',') + "\nChưa nhập \"Hàm lượng vàng\"");
            }

            if (hamLuongVangNotFound.Length > 0)
            {
                PopupService.Instance.Error("Các hàm lượng vàng: " + hamLuongVangNotFound.TrimEnd(' ', ',') + "\nChưa được nhập trong phần mềm");
            }

            if (sanPhamInvalidTenSanPham.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamInvalidTenSanPham.TrimEnd(' ', ',') + "\nChưa nhập \"Tên sản phẩm\"");
            }

            if (sanPhamInvalidKiHieu.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamInvalidKiHieu.TrimEnd(' ', ',') + "\nChưa nhập \"Kí hiệu\"");
            }

            if (sanPhamInvalidKhoiLuongTinh.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamInvalidKhoiLuongTinh.TrimEnd(' ', ',') + "\nCó \"Khối lượng tịnh\" không hợp lệ");
            }

            if (sanPhamInvalidKhoiLuongDa.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamInvalidKhoiLuongDa.TrimEnd(' ', ',') + "\nCó \"Khối lượng đá\" không hợp lệ");
            }

            if (sanPhamInvalidKhoiLuongVang.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamInvalidKhoiLuongVang.TrimEnd(' ', ',') + "\nCó \"Khối lượng vàng\" không hợp lệ");
            }

            if (sanPhamTrungMaSanPhamExcel.Length > 0)
            {
                PopupService.Instance.Error("Các mã sản phẩm: " + sanPhamTrungMaSanPhamExcel.TrimEnd(' ', ',') + "\nBị trùng mã sản phẩm trong file excel");
            }

            if (sanPhamTrungMaTemExcel.Length > 0)
            {
                PopupService.Instance.Error("Các mã sản phẩm: " + sanPhamTrungMaTemExcel.TrimEnd(' ', ',') + "\nBị trùng mã tem trong file excel");
            }

            if (nhaCungCapNotFound.Length > 0)
            {
                PopupService.Instance.Error("Các nhà cung cấp: " + nhaCungCapNotFound.TrimEnd(' ', ',') + "\nChưa được nhập trong phần mềm");
            }

            if (sanPhamSaiCuPhapMaExcel.Length > 0)
            {
                PopupService.Instance.Error("Các sản phẩm ở vị trí: " + sanPhamSaiCuPhapMaExcel.TrimEnd(' ', ',') + "\nCó \"Mã sản phẩm\" hoặc \"Mã tem\" không hợp lệ");
            }

            return isValid;
        }

        private void btnNhapKho_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!IsValid())
            {
                return;
            }

            if (grcSanPham.DataRowCount == 0)
            {
                return;
            }

            List<SanPham> listNhapKhoExcel = new List<SanPham>();
            List<KiHieuHamLuongVang> listHamLuongVangDb = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang();

            for (int i = 0; i < grcSanPham.DataRowCount; i++)
            {
                var item = grcSanPham.GetDataRow(i).ItemArray;
                bool isSanPhamChuyenKho = false;
                DateTime ngayNhapSanPham;
                int maHamLuongVang = listHamLuongVangDb
                    .Where(x => x.HamLuongVang.Equals(item[IndexNhapExcelConstant.HAM_LUONG_VANG].ToString().ToUpper())).First()
                    .MaHamLuongVang;

                if (!string.IsNullOrWhiteSpace(item[IndexNhapExcelConstant.MA_CHUYEN_KHO].ToString()))
                {
                    if (item[IndexNhapExcelConstant.MA_CHUYEN_KHO].ToString() == "X"
                        || item[IndexNhapExcelConstant.MA_CHUYEN_KHO].ToString() == "x"
                        || item[IndexNhapExcelConstant.MA_CHUYEN_KHO].ToString() == "1"
                        || item[IndexNhapExcelConstant.MA_CHUYEN_KHO].ToString().ToLower()  == "true")
                    {
                        isSanPhamChuyenKho = true;
                    }
                }

                try
                {
                    ngayNhapSanPham = DateTime.Parse(item[IndexNhapExcelConstant.NGAY_NHAP].ToString());
                }
                catch
                {
                    ngayNhapSanPham = DateTime.Today;
                }

                SanPham sanPham = new SanPham
                {
                    MaSanPham = item[IndexNhapExcelConstant.MA_SP].ToString(),
                    TenSanPham = item[IndexNhapExcelConstant.TEN_SP].ToString(),
                    MaTem = item[IndexNhapExcelConstant.MA_TEM].ToString(),
                    NgayNhap = ngayNhapSanPham,
                    HamLuongVang = item[IndexNhapExcelConstant.HAM_LUONG_VANG].ToString().ToUpper(),
                    MaHamLuongVang = maHamLuongVang,
                    TiLeHot = convertString(item[IndexNhapExcelConstant.TI_LE_HOT].ToString()),
                    GiaBan = convertStringToInt(item[IndexNhapExcelConstant.GIA_BAN].ToString()),
                    KhoiLuongTinh = convertString(item[IndexNhapExcelConstant.KHOI_LUONG_TINH].ToString()),
                    KhoiLuongDa = convertString(item[IndexNhapExcelConstant.KHOI_LUONG_DA].ToString()),
                    KhoiLuongVang = convertString(item[IndexNhapExcelConstant.KHOI_LUONG_VANG].ToString()),
                    KiHieu = convertText(item[IndexNhapExcelConstant.KI_HIEU].ToString()),
                    TienCong = convertStringToInt(item[IndexNhapExcelConstant.TIEN_CONG].ToString()),
                    IsSanPhamChuyenKho = isSanPhamChuyenKho,
                    MaNhanVien = Form1.nhanVien.MaNhanVien
                };

                listNhapKhoExcel.Add(sanPham);
            }

            List<SanPham> listInserted = SanPhamBUS.Instance.NhapSanPhamVaoKho(listNhapKhoExcel);

            if (listInserted != null)
            {
                PopupService.Instance.Success(MessageSucces.NHAP_KHO_MOI + "\n" + listInserted.Count + " sản phẩm");

                if (!SanPhamBUS.Instance.TinhGiaBanAllSanPham())
                {
                    PopupService.Instance.Error(MessageError.GIA_VANG_LOI);
                }

                Hide();
            }
            else
            {
                PopupService.Instance.Error(MessageError.NHAP_KHO_MOI);
            }
        }
    }
}