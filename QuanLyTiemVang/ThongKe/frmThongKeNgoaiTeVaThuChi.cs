﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.TemporaryModel;
using Share.Constant;
using BUS;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using DTO.Models;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;
using QuanLyDieuXe.DialogCustom;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeNgoaiTeVaThuChi : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeNgoaiTeVaThuChi()
        {
            InitializeComponent();
        }

        private DateTime thongKeTuNgay;
        private DateTime thongKeDenNgay;

        private void frmThongKeNgoaiTeVaThuChi_Load(object sender, EventArgs e)
        {
            SetDataSourceForThoiGian();
            detTuNgay.Properties.MaxValue = DateTime.Today;
            detDenNgay.Properties.MaxValue = DateTime.Today;
        }

        private void SetDataSourceForThoiGian()
        {
            List<LookUpEditItem> listThoiGianThongKe = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_HOM_NAY
                },
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_KHOAN_THOI_GIAN
                }
            };

            lueThoiGianThongKe.Properties.DataSource = listThoiGianThongKe;
            lueThoiGianThongKe.Properties.DisplayMember = "DisplayMember";
            lueThoiGianThongKe.Properties.ValueMember = "ValueMember";
        }

        private bool IsHaveNgayDauKi()
        {
            if (NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast() == null)
            {
                PopupService.Instance.Warning(MessageWarning.NGAY_DAU_KI_NULL);

                return false;
            }

            return true;
        }

        private void GetThongKeByDate(DateTime ngayThongKe)
        {
            List<BangThongKeDoiNgoaiTe> listThongKeDoiNgoaiTe = BangThongKeDoiNgoaiTeBUS.Instance.GetBangThongKeByDate(ngayThongKe);
            List<BangThongKeTienMat> listThongKeTienMat = BangThongKeTienMatBUS.Instance.GetBangThongKeByDate(ngayThongKe);

            LoadData(listThongKeDoiNgoaiTe, listThongKeTienMat);
        }

        private void GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            List<BangThongKeDoiNgoaiTe> listThongKeDoiNgoaiTe = BangThongKeDoiNgoaiTeBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
            List<BangThongKeTienMat> listThongKeTienMat = BangThongKeTienMatBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);

            LoadData(listThongKeDoiNgoaiTe, listThongKeTienMat);
        }

        private void LoadData(List<BangThongKeDoiNgoaiTe> listThongKeDoiNgoaiTe, List<BangThongKeTienMat> listThongKeTienMat)
        {
            List<ThuChiTienMatTemp> listThongKeTienMatTemp = new List<ThuChiTienMatTemp>();

            if (listThongKeTienMat.Count > 0)
            {
                foreach (BangThongKeTienMat bangThongKe in listThongKeTienMat)
                {
                    listThongKeTienMatTemp.Add(new ThuChiTienMatTemp
                    {
                        TongChi = bangThongKe.TongTienChiRa,
                        TongThu = bangThongKe.TongTienThuVao,
                        ConLai = bangThongKe.TongTienThuVao - bangThongKe.TongTienChiRa
                    });
                }
            }

            listThongKeDoiNgoaiTe.Add(new BangThongKeDoiNgoaiTe
            {
                LoaiTien = new LoaiTien
                {
                    MaLoaiTien = 0,
                    TenLoaiTien = "TỔNG CỘNG"
                },
                TongTienVietDoiRa = listThongKeDoiNgoaiTe.Sum(x => x.TongTienVietDoiRa),
                TongTienVietThuVao = listThongKeDoiNgoaiTe.Sum(x => x.TongTienVietThuVao)
            });

            grcThuChiTienMat.DataSource = listThongKeTienMatTemp;
            grcTongNgoaiTeBanRa.DataSource = listThongKeDoiNgoaiTe;
            grcTongNgoaiTeMuaVao.DataSource = listThongKeDoiNgoaiTe;
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcTongNgoaiTeBanRa.DataSource = null;
            grcTongNgoaiTeMuaVao.DataSource = null;
            lueThoiGianThongKe.EditValue = null;
            detDenNgay.EditValue = null;
            detTuNgay.EditValue = null;
            detTuNgay.ReadOnly = true;
            detDenNgay.ReadOnly = true;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!BangThongKeDoiNgoaiTeBUS.Instance.TinhToanThongKe() || !BangThongKeTienMatBUS.Instance.TinhToanThongKe())
            {
                PopupService.Instance.Error(MessageError.TINH_TOAN_THONG_KE);
            }
            else
            {
                PopupService.Instance.Success(MessageSucces.TINH_TOAN_THONG_KE);
            }
        }

        private void btnInThongKe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<BangThongKeDoiNgoaiTe> listThongKeDoiNgoaiTe = (List<BangThongKeDoiNgoaiTe>)grcTongNgoaiTeBanRa.DataSource;
            List<BangInThongKeNgoaiTeTemp> bangInNgoaiTeMuaVao = new List<BangInThongKeNgoaiTeTemp>();
            List<BangInThongKeNgoaiTeTemp> bangInNgoaiTeBanRa = new List<BangInThongKeNgoaiTeTemp>();
            List<ThuChiTienMatTemp> listThuChiTienMatTemp = (List<ThuChiTienMatTemp>)grcThuChiTienMat.DataSource;

            if (listThuChiTienMatTemp != null)
            {
                ThongKeThuChiPrint thongKePrint = new ThongKeThuChiPrint();

                thongKePrint.DataSource = listThuChiTienMatTemp;
                thongKePrint.ShowPreviewDialog();
            }

            if (listThongKeDoiNgoaiTe != null)
            {
                foreach(BangThongKeDoiNgoaiTe bangThongKe in listThongKeDoiNgoaiTe)
                {
                    bangInNgoaiTeMuaVao.Add(new BangInThongKeNgoaiTeTemp
                    {
                        TieuDeThongKe = "TỔNG NGOẠI TỆ MUA VÀO",
                        TenNgoaiTe = bangThongKe.LoaiTien.TenLoaiTien,
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        TongTienNgoaiTe = bangThongKe.TongTienNgoaiTeThuVao,
                        ThanhTienVND = bangThongKe.TongTienVietDoiRa
                    });

                    bangInNgoaiTeBanRa.Add(new BangInThongKeNgoaiTeTemp
                    {
                        TieuDeThongKe = "TỔNG NGOẠI TỆ BÁN RA",
                        TenNgoaiTe = bangThongKe.LoaiTien.TenLoaiTien,
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        TongTienNgoaiTe = bangThongKe.TongTienNgoaiTeDoiRa,
                        ThanhTienVND = bangThongKe.TongTienVietThuVao
                    });
                }

                ThongKeQuyDoiNgoaiTePrint thongKeMuaVaoPrint = new ThongKeQuyDoiNgoaiTePrint();

                thongKeMuaVaoPrint.DataSource = bangInNgoaiTeMuaVao;
                thongKeMuaVaoPrint.ShowPreviewDialog();

                ThongKeQuyDoiNgoaiTePrint thongKeBanRaPrint = new ThongKeQuyDoiNgoaiTePrint();

                thongKeBanRaPrint.DataSource = bangInNgoaiTeBanRa;
                thongKeBanRaPrint.ShowPreviewDialog();
            }
        }

        private void lueNgayThongKe_EditValueChanged(object sender, EventArgs e)
        {
            if (lueThoiGianThongKe.EditValue == null)
            {
                return;
            }

            if ((int)lueThoiGianThongKe.EditValue == TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN)
            {
                detTuNgay.ReadOnly = false;
                detDenNgay.ReadOnly = false;
            }
            else
            {
                detTuNgay.ReadOnly = true;
                detDenNgay.ReadOnly = true;
            }
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (!IsHaveNgayDauKi())
            {
                return;
            }

            if (lueThoiGianThongKe.EditValue == null || TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY == Convert.ToInt32(lueThoiGianThongKe.EditValue))
            {
                GetThongKeByDate(DateTime.Today);
                thongKeTuNgay = DateTime.Today;
                thongKeDenNgay = DateTime.Today;
            }
            else if (detTuNgay.DateTime.Date == detDenNgay.DateTime.Date)
            {
                GetThongKeByDate(detTuNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
            else
            {
                GetThongKeByTwoDifferenceDate(detTuNgay.DateTime, detDenNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
        }

        private void btnXuatExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string pathSave;
            string fileName = "";
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.ShowNewFolderButton = true;
            folder.Description = "Chọn vị trí lưu file!";
            if (folder.ShowDialog() == DialogResult.OK)
            {
                pathSave = folder.SelectedPath + "\\";
                if (InputBox.Show("Thông báo!",
                "&Nhập tên file:", ref fileName) == DialogResult.OK)
                {
                    string pathSave1 = pathSave + (fileName + " 1" + ".xls");
                    string pathSave2 = pathSave + (fileName + " 2" + ".xls");
                    string pathSave3 = pathSave + (fileName + " 3" + ".xls");
                    grcTongNgoaiTeBanRa.ExportToXls(pathSave1);
                    grcTongNgoaiTeMuaVao.ExportToXls(pathSave2);
                    grcThuChiTienMat.ExportToXls(pathSave3);
                }
            }
        }
    }
}