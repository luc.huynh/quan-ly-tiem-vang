﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeKhoCu : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeKhoCu()
        {
            InitializeComponent();
        }

        private void frmThongKeKhoCu_Load(object sender, EventArgs e)
        {
            detNgayThongKe.Properties.MaxValue = DateTime.Today;
        }

        private bool IsHaveNgayDauKi()
        {
            if (NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast() == null)
            {
                PopupService.Instance.Warning(MessageWarning.NGAY_DAU_KI_NULL);

                return false;
            }

            return true;
        }

        private void GetThongKeByDate(DateTime ngayThongKe)
        {
            grcThongKeKhoCu.DataSource = BangThongKeKhoCuBUS.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (!IsHaveNgayDauKi())
            {
                return;
            }

            if (detNgayThongKe.EditValue == null)
            {
                GetThongKeByDate(DateTime.Today);
            }
            else
            {
                GetThongKeByDate(detNgayThongKe.DateTime);
            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcThongKeKhoCu.DataSource = null;
            detNgayThongKe.EditValue = null;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!BangThongKeKhoCuBUS.Instance.TinhToanThongKe())
            {
                PopupService.Instance.Error(MessageError.TINH_TOAN_THONG_KE);
            }
            else
            {
                PopupService.Instance.Success(MessageSucces.TINH_TOAN_THONG_KE);
            }
        }
    }
}