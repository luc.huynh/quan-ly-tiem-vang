﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.TemporaryModel;
using BUS;
using QuanLyTiemVang.Services;
using DTO.Models;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;
using Share.Constant;
using QuanLyTiemVang.Constants;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeTienMat : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeTienMat()
        {
            InitializeComponent();
        }

        private DateTime thongKeTuNgay;
        private DateTime thongKeDenNgay;

        private void frmThongKeTienMat_Load(object sender, EventArgs e)
        {
            SetDataSourceForThoiGian();
            detTuNgay.Properties.MaxValue = DateTime.Today;
            detDenNgay.Properties.MaxValue = DateTime.Today;
        }

        private void SetDataSourceForThoiGian()
        {
            List<LookUpEditItem> listThoiGianThongKe = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_HOM_NAY
                },
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_KHOAN_THOI_GIAN
                }
            };

            lueThoiGianThongKe.Properties.DataSource = listThoiGianThongKe;
            lueThoiGianThongKe.Properties.DisplayMember = "DisplayMember";
            lueThoiGianThongKe.Properties.ValueMember = "ValueMember";
        }

        private bool IsHaveNgayDauKi()
        {
            if (NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast() == null)
            {
                PopupService.Instance.Warning(MessageWarning.NGAY_DAU_KI_NULL);

                return false;
            }

            return true;
        }

        private void GetThongKeByDate(DateTime ngayThongKe)
        {
            BindingDataThongKe(
                BangThongKeTienMatBUS.Instance.GetBangThongKeByDate(ngayThongKe),
                BangThongKeDoiNgoaiTeBUS.Instance.GetBangThongKeByDate(ngayThongKe));
        }

        private void GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            BindingDataThongKe(
                BangThongKeTienMatBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay),
                BangThongKeDoiNgoaiTeBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay));
        }

        private void BindingDataThongKe(List<BangThongKeTienMat> listBangThongKeTienMat, List<BangThongKeDoiNgoaiTe> listBangThongKeDoiNgoaiTe)
        {
            List<ThongKeTienMatTemp> listThongKeTemp = new List<ThongKeTienMatTemp>();

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 1,
                NoiDungThongKe = "SỐ DƯ ĐẦU KÌ",
                DauKi = listBangThongKeTienMat.Sum(x => x.SoDuDauKi)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 2,
                NoiDungThongKe = "Tổng tiền vàng bán ra",
                Thu = listBangThongKeTienMat.Sum(x => x.TongTienVangBanRa)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 3,
                NoiDungThongKe = "Tổng tiền vàng mua vào",
                Chi = listBangThongKeTienMat.Sum(x => x.TongTienVangMuaVao)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 4,
                NoiDungThongKe = "Tổng tiền công",
                Thu = listBangThongKeTienMat.Sum(x => x.TongTienCong)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 5,
                NoiDungThongKe = "Tổng tiền bớt",
                Chi = listBangThongKeTienMat.Sum(x => x.TongTienBot)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 6,
                NoiDungThongKe = "Tổng chi tiền cầm đồ",
                Chi = listBangThongKeTienMat.Sum(x => x.TongChiTienCamDo)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 7,
                NoiDungThongKe = "Tổng thu tiền lãi + tiền gốc cầm đồ",
                Thu = listBangThongKeTienMat.Sum(x => x.TongThuTienLaiCamDo + x.TongThuNoGocCamDo)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 8,
                NoiDungThongKe = "Tổng tiền đổi ngoại tệ",
                Thu = listBangThongKeTienMat.Sum(x => x.TongTienDoiNgoaiTeThu),
                Chi = listBangThongKeTienMat.Sum(x => x.TongTienDoiNgoaiTeChi)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 9,
                NoiDungThongKe = "Tiền ngoại tệ"
            });

            foreach (BangThongKeDoiNgoaiTe bangThongKe in listBangThongKeDoiNgoaiTe)
            {
                listThongKeTemp.Add(new ThongKeTienMatTemp
                {
                    NoiDungThongKe = bangThongKe.LoaiTien.TenLoaiTien,
                    Thu = bangThongKe.TongTienNgoaiTeThuVao,
                    Chi = bangThongKe.TongTienNgoaiTeDoiRa
                });
            }

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 10,
                NoiDungThongKe = "Tổng phát sinh thu chi khác",
                Thu = listBangThongKeTienMat.Sum(x => x.TongPhatSinhKhacThuVao),
                Chi = listBangThongKeTienMat.Sum(x => x.TongPhatSinhKhacChiRa)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                SoThuTu = 11,
                NoiDungThongKe = "SỐ DƯ CUỐI KÌ",
                CuoiKi = listBangThongKeTienMat.Sum(x => x.SoDuCuoiKi)
            });

            listThongKeTemp.Add(new ThongKeTienMatTemp
            {
                NoiDungThongKe = "TỔNG",
                DauKi = listBangThongKeTienMat.Sum(x => x.SoDuDauKi),
                Thu = listBangThongKeTienMat.Sum(x => x.TongTienThuVao),
                Chi = listBangThongKeTienMat.Sum(x => x.TongTienChiRa),
                CuoiKi = listBangThongKeTienMat.Sum(x => x.SoDuCuoiKi)
            });

            grcThongKeTienMat.DataSource = listThongKeTemp;
        }

        private void lueThoiGianThongKe_EditValueChanged(object sender, EventArgs e)
        {
            if (lueThoiGianThongKe.EditValue == null)
            {
                return;
            }

            if ((int)lueThoiGianThongKe.EditValue == TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN)
            {
                detTuNgay.ReadOnly = false;
                detDenNgay.ReadOnly = false;
            }
            else
            {
                detTuNgay.ReadOnly = true;
                detDenNgay.ReadOnly = true;
            }
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (!IsHaveNgayDauKi())
            {
                return;
            }

            if (lueThoiGianThongKe.EditValue == null || TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY == Convert.ToInt32(lueThoiGianThongKe.EditValue))
            {
                GetThongKeByDate(DateTime.Today);
                thongKeTuNgay = DateTime.Today;
                thongKeDenNgay = DateTime.Today;
            }
            else if (detTuNgay.DateTime.Date == detDenNgay.DateTime.Date)
            {
                GetThongKeByDate(detTuNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
            else
            {
                GetThongKeByTwoDifferenceDate(detTuNgay.DateTime, detDenNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcThongKeTienMat.DataSource = null;
            lueThoiGianThongKe.EditValue = null;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!BangThongKeTienMatBUS.Instance.TinhToanThongKe() || !BangThongKeDoiNgoaiTeBUS.Instance.TinhToanThongKe())
            {
                PopupService.Instance.Error(MessageError.TINH_TOAN_THONG_KE);
            }
            else
            {
                PopupService.Instance.Success(MessageSucces.TINH_TOAN_THONG_KE);
            }
        }

        private void btnInThongKe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<ThongKeTienMatTemp> listThongKeTemp = (List<ThongKeTienMatTemp>)grcThongKeTienMat.DataSource;

            if (listThongKeTemp != null)
            {
                ThongKeTienMatPrint thongKePrint = new ThongKeTienMatPrint();

                foreach(ThongKeTienMatTemp bangThongKe in listThongKeTemp)
                {
                    bangThongKe.ThongKeTuNgay = thongKeTuNgay;
                    bangThongKe.ThongKeDenNgay = thongKeDenNgay;
                }

                thongKePrint.DataSource = listThongKeTemp;

                thongKePrint.ShowPreviewDialog();
            }
        }
    }
}