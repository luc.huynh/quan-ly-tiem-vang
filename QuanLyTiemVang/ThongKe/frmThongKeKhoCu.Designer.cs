﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmThongKeKhoCu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.detNgayThongKe = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcThongKeKhoCu = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeKhoCuBindingSource = new System.Windows.Forms.BindingSource();
            this.grvThongKeKhoCu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangDauKi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangDemDiGiaCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangCuoiKi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangNhapVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.thongKeKhoCuTempBindingSource = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayThongKe.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayThongKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeKhoCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeKhoCuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeKhoCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeKhoCuTempBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 9;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1036, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 690);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1036, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 640);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1036, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 640);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1036, 89);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Tiêu chí thống kê";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnThongKe);
            this.layoutControl1.Controls.Add(this.detNgayThongKe);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1032, 62);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnThongKe
            // 
            this.btnThongKe.Location = new System.Drawing.Point(519, 16);
            this.btnThongKe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(497, 27);
            this.btnThongKe.StyleController = this.layoutControl1;
            this.btnThongKe.TabIndex = 5;
            this.btnThongKe.Text = "Thống kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // detNgayThongKe
            // 
            this.detNgayThongKe.EditValue = null;
            this.detNgayThongKe.Location = new System.Drawing.Point(88, 16);
            this.detNgayThongKe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detNgayThongKe.MenuManager = this.barManager1;
            this.detNgayThongKe.Name = "detNgayThongKe";
            this.detNgayThongKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayThongKe.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayThongKe.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayThongKe.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayThongKe.Properties.MaxValue = new System.DateTime(((long)(0)));
            this.detNgayThongKe.Properties.NullText = "Hôm nay";
            this.detNgayThongKe.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detNgayThongKe.Size = new System.Drawing.Size(425, 22);
            this.detNgayThongKe.StyleController = this.layoutControl1;
            this.detNgayThongKe.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1032, 62);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.detNgayThongKe;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(503, 36);
            this.layoutControlItem1.Text = "Chọn ngày";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(68, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnThongKe;
            this.layoutControlItem2.Location = new System.Drawing.Point(503, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(503, 36);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcThongKeKhoCu);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 139);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1036, 551);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Kết quả thống kê";
            // 
            // grcThongKeKhoCu
            // 
            this.grcThongKeKhoCu.DataSource = this.bangThongKeKhoCuBindingSource;
            this.grcThongKeKhoCu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcThongKeKhoCu.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeKhoCu.Location = new System.Drawing.Point(2, 25);
            this.grcThongKeKhoCu.MainView = this.grvThongKeKhoCu;
            this.grcThongKeKhoCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeKhoCu.MenuManager = this.barManager1;
            this.grcThongKeKhoCu.Name = "grcThongKeKhoCu";
            this.grcThongKeKhoCu.Size = new System.Drawing.Size(1032, 524);
            this.grcThongKeKhoCu.TabIndex = 0;
            this.grcThongKeKhoCu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvThongKeKhoCu});
            // 
            // bangThongKeKhoCuBindingSource
            // 
            this.bangThongKeKhoCuBindingSource.DataSource = typeof(DTO.Models.BangThongKeKhoCu);
            // 
            // grvThongKeKhoCu
            // 
            this.grvThongKeKhoCu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKiHieuHamLuongVang,
            this.colKhoiLuongVangDauKi,
            this.colKhoiLuongVangDemDiGiaCong,
            this.colKhoiLuongVangCuoiKi,
            this.colKhoiLuongVangNhapVao});
            this.grvThongKeKhoCu.GridControl = this.grcThongKeKhoCu;
            this.grvThongKeKhoCu.Name = "grvThongKeKhoCu";
            this.grvThongKeKhoCu.OptionsBehavior.ReadOnly = true;
            this.grvThongKeKhoCu.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvThongKeKhoCu.OptionsView.ShowGroupPanel = false;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 0;
            // 
            // colKhoiLuongVangDauKi
            // 
            this.colKhoiLuongVangDauKi.Caption = "KLV đầu kì";
            this.colKhoiLuongVangDauKi.FieldName = "KhoiLuongVangDauKi";
            this.colKhoiLuongVangDauKi.Name = "colKhoiLuongVangDauKi";
            this.colKhoiLuongVangDauKi.Visible = true;
            this.colKhoiLuongVangDauKi.VisibleIndex = 1;
            // 
            // colKhoiLuongVangDemDiGiaCong
            // 
            this.colKhoiLuongVangDemDiGiaCong.Caption = "KLV đem đi gia công";
            this.colKhoiLuongVangDemDiGiaCong.FieldName = "KhoiLuongVangDemDiGiaCong";
            this.colKhoiLuongVangDemDiGiaCong.Name = "colKhoiLuongVangDemDiGiaCong";
            this.colKhoiLuongVangDemDiGiaCong.Visible = true;
            this.colKhoiLuongVangDemDiGiaCong.VisibleIndex = 3;
            // 
            // colKhoiLuongVangCuoiKi
            // 
            this.colKhoiLuongVangCuoiKi.Caption = "KLV cuối kì";
            this.colKhoiLuongVangCuoiKi.FieldName = "KhoiLuongVangCuoiKi";
            this.colKhoiLuongVangCuoiKi.Name = "colKhoiLuongVangCuoiKi";
            this.colKhoiLuongVangCuoiKi.Visible = true;
            this.colKhoiLuongVangCuoiKi.VisibleIndex = 4;
            // 
            // colKhoiLuongVangNhapVao
            // 
            this.colKhoiLuongVangNhapVao.Caption = "KLV nhập vào";
            this.colKhoiLuongVangNhapVao.FieldName = "KhoiLuongVangNhapVao";
            this.colKhoiLuongVangNhapVao.Name = "colKhoiLuongVangNhapVao";
            this.colKhoiLuongVangNhapVao.Visible = true;
            this.colKhoiLuongVangNhapVao.VisibleIndex = 2;
            // 
            // thongKeKhoCuTempBindingSource
            // 
            this.thongKeKhoCuTempBindingSource.DataSource = typeof(DTO.TemporaryModel.ThongKeKhoCuTemp);
            // 
            // frmThongKeKhoCu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 690);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmThongKeKhoCu";
            this.Text = "Thống kê kho cũ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmThongKeKhoCu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detNgayThongKe.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayThongKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeKhoCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeKhoCuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeKhoCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeKhoCuTempBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcThongKeKhoCu;
        private DevExpress.XtraGrid.Views.Grid.GridView grvThongKeKhoCu;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnThongKe;
        private DevExpress.XtraEditors.DateEdit detNgayThongKe;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource thongKeKhoCuTempBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDauKi;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDemDiGiaCong;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCuoiKi;
        private System.Windows.Forms.BindingSource bangThongKeKhoCuBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangNhapVao;
    }
}