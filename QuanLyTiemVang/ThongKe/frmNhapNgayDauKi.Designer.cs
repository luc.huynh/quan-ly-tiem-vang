﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmNhapNgayDauKi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnNhapNgaydauKi = new DevExpress.XtraEditors.SimpleButton();
            this.detNgayDauKi = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtTienDauKi = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDauKi.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDauKi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDauKi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtTienDauKi);
            this.layoutControl1.Controls.Add(this.btnNhapNgaydauKi);
            this.layoutControl1.Controls.Add(this.detNgayDauKi);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(584, 124);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnNhapNgaydauKi
            // 
            this.btnNhapNgaydauKi.Location = new System.Drawing.Point(16, 72);
            this.btnNhapNgaydauKi.Name = "btnNhapNgaydauKi";
            this.btnNhapNgaydauKi.Size = new System.Drawing.Size(552, 27);
            this.btnNhapNgaydauKi.StyleController = this.layoutControl1;
            this.btnNhapNgaydauKi.TabIndex = 5;
            this.btnNhapNgaydauKi.Text = "Nhập thông tin đầu kì";
            this.btnNhapNgaydauKi.Click += new System.EventHandler(this.btnNhapNgaydauKi_Click);
            // 
            // detNgayDauKi
            // 
            this.detNgayDauKi.EditValue = null;
            this.detNgayDauKi.Location = new System.Drawing.Point(92, 16);
            this.detNgayDauKi.Name = "detNgayDauKi";
            this.detNgayDauKi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayDauKi.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayDauKi.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayDauKi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayDauKi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detNgayDauKi.Size = new System.Drawing.Size(476, 22);
            this.detNgayDauKi.StyleController = this.layoutControl1;
            this.detNgayDauKi.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(584, 124);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.detNgayDauKi;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(558, 28);
            this.layoutControlItem1.Text = "Ngày đầu kì";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnNhapNgaydauKi;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(558, 42);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // txtTienDauKi
            // 
            this.txtTienDauKi.Location = new System.Drawing.Point(92, 44);
            this.txtTienDauKi.Name = "txtTienDauKi";
            this.txtTienDauKi.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtTienDauKi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDauKi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienDauKi.Properties.NullText = "0 VNĐ";
            this.txtTienDauKi.Size = new System.Drawing.Size(476, 22);
            this.txtTienDauKi.StyleController = this.layoutControl1;
            this.txtTienDauKi.TabIndex = 6;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtTienDauKi;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(558, 28);
            this.layoutControlItem3.Text = "Tiền đầu kì";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(72, 17);
            // 
            // frmNhapNgayDauKi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 124);
            this.Controls.Add(this.layoutControl1);
            this.Name = "frmNhapNgayDauKi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nhập ngày đầu kì";
            this.Load += new System.EventHandler(this.frmNhapNgayDauKi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDauKi.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayDauKi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDauKi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnNhapNgaydauKi;
        private DevExpress.XtraEditors.DateEdit detNgayDauKi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtTienDauKi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}