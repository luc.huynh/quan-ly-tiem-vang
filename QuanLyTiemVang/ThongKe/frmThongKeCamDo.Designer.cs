﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmThongKeCamDo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnInThongKe = new DevExpress.XtraBars.BarButtonItem();
            this.btnXuatExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.detTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.detDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.lueThoiGianThongKe = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcTongTienVangKhachCam = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvTongTienVangKhachCam = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongTrongLuongCamDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienCamDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.grcTongTienVangKhachChuoc = new DevExpress.XtraGrid.GridControl();
            this.grvTongTienVangKhachChuoc = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongTrongLuongThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienLaiThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.grcTongTienVangThanhLy = new DevExpress.XtraGrid.GridControl();
            this.grvTongTienVangThanhLy = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongTrongLuongThanhLy1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienLaiThanhLy1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienThanhLy1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl3 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.grcGiaHanHopDong = new DevExpress.XtraGrid.GridControl();
            this.grvGiaHanHopDong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongTrongLuongGiaHan2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienCamGiaHan2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienLaiGiaHan2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTongTienVangKhachCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongTienVangKhachCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTongTienVangKhachChuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongTienVangKhachChuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTongTienVangThanhLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongTienVangThanhLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcGiaHanHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvGiaHanHopDong)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnInThongKe,
            this.btnXuatExcel});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 11;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInThongKe, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXuatExcel, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInThongKe
            // 
            this.btnInThongKe.Caption = "In thống kê";
            this.btnInThongKe.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInThongKe.Id = 9;
            this.btnInThongKe.Name = "btnInThongKe";
            this.btnInThongKe.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInThongKe_ItemClick);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Caption = "Xuất excel";
            this.btnXuatExcel.Glyph = global::QuanLyTiemVang.Properties.Resources.netvibes_icon;
            this.btnXuatExcel.Id = 10;
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatExcel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1127, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 730);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1127, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 680);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1127, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 680);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1127, 85);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tiêu chí thống kê";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnThongKe);
            this.layoutControl1.Controls.Add(this.detTuNgay);
            this.layoutControl1.Controls.Add(this.detDenNgay);
            this.layoutControl1.Controls.Add(this.lueThoiGianThongKe);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1123, 58);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnThongKe
            // 
            this.btnThongKe.Location = new System.Drawing.Point(822, 16);
            this.btnThongKe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(264, 27);
            this.btnThongKe.StyleController = this.layoutControl1;
            this.btnThongKe.TabIndex = 4;
            this.btnThongKe.Text = "Thống kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // detTuNgay
            // 
            this.detTuNgay.EditValue = null;
            this.detTuNgay.Location = new System.Drawing.Point(378, 16);
            this.detTuNgay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detTuNgay.MenuManager = this.barManager1;
            this.detTuNgay.Name = "detTuNgay";
            this.detTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detTuNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detTuNgay.Properties.ReadOnly = true;
            this.detTuNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detTuNgay.Size = new System.Drawing.Size(170, 22);
            this.detTuNgay.StyleController = this.layoutControl1;
            this.detTuNgay.TabIndex = 2;
            // 
            // detDenNgay
            // 
            this.detDenNgay.EditValue = null;
            this.detDenNgay.Location = new System.Drawing.Point(648, 16);
            this.detDenNgay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detDenNgay.MenuManager = this.barManager1;
            this.detDenNgay.Name = "detDenNgay";
            this.detDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detDenNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detDenNgay.Properties.ReadOnly = true;
            this.detDenNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detDenNgay.Size = new System.Drawing.Size(168, 22);
            this.detDenNgay.StyleController = this.layoutControl1;
            this.detDenNgay.TabIndex = 3;
            // 
            // lueThoiGianThongKe
            // 
            this.lueThoiGianThongKe.Location = new System.Drawing.Point(110, 16);
            this.lueThoiGianThongKe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueThoiGianThongKe.MenuManager = this.barManager1;
            this.lueThoiGianThongKe.Name = "lueThoiGianThongKe";
            this.lueThoiGianThongKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueThoiGianThongKe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Thống kê theo")});
            this.lueThoiGianThongKe.Properties.NullText = "Hôm nay";
            this.lueThoiGianThongKe.Size = new System.Drawing.Size(168, 22);
            this.lueThoiGianThongKe.StyleController = this.layoutControl1;
            this.lueThoiGianThongKe.TabIndex = 0;
            this.lueThoiGianThongKe.EditValueChanged += new System.EventHandler(this.lueNgayThongKe_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1102, 59);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueThoiGianThongKe;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(268, 33);
            this.layoutControlItem1.Text = "Ngày thống kê";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.detDenNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(538, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(268, 33);
            this.layoutControlItem2.Text = "Đến ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.detTuNgay;
            this.layoutControlItem3.Location = new System.Drawing.Point(268, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(270, 33);
            this.layoutControlItem3.Text = "Từ ngày";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnThongKe;
            this.layoutControlItem4.Location = new System.Drawing.Point(806, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(270, 33);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcTongTienVangKhachCam);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 135);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1127, 146);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Tổng tiền - vàng khách cầm";
            // 
            // grcTongTienVangKhachCam
            // 
            this.grcTongTienVangKhachCam.DataSource = this.bangThongKeCamDoBindingSource;
            this.grcTongTienVangKhachCam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTongTienVangKhachCam.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongTienVangKhachCam.Location = new System.Drawing.Point(2, 25);
            this.grcTongTienVangKhachCam.MainView = this.grvTongTienVangKhachCam;
            this.grcTongTienVangKhachCam.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongTienVangKhachCam.MenuManager = this.barManager1;
            this.grcTongTienVangKhachCam.Name = "grcTongTienVangKhachCam";
            this.grcTongTienVangKhachCam.Size = new System.Drawing.Size(1123, 119);
            this.grcTongTienVangKhachCam.TabIndex = 0;
            this.grcTongTienVangKhachCam.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTongTienVangKhachCam});
            // 
            // bangThongKeCamDoBindingSource
            // 
            this.bangThongKeCamDoBindingSource.DataSource = typeof(DTO.Models.BangThongKeCamDo);
            // 
            // grvTongTienVangKhachCam
            // 
            this.grvTongTienVangKhachCam.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongTrongLuongCamDo,
            this.colTongTienCamDo,
            this.colKiHieuHamLuongVang});
            this.grvTongTienVangKhachCam.GridControl = this.grcTongTienVangKhachCam;
            this.grvTongTienVangKhachCam.Name = "grvTongTienVangKhachCam";
            this.grvTongTienVangKhachCam.OptionsBehavior.Editable = false;
            this.grvTongTienVangKhachCam.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvTongTienVangKhachCam.OptionsView.ShowGroupPanel = false;
            // 
            // colTongTrongLuongCamDo
            // 
            this.colTongTrongLuongCamDo.Caption = "Tổng trọng lượng";
            this.colTongTrongLuongCamDo.DisplayFormat.FormatString = "#,0.#";
            this.colTongTrongLuongCamDo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTrongLuongCamDo.FieldName = "TongTrongLuongCamDo";
            this.colTongTrongLuongCamDo.Name = "colTongTrongLuongCamDo";
            this.colTongTrongLuongCamDo.Visible = true;
            this.colTongTrongLuongCamDo.VisibleIndex = 1;
            // 
            // colTongTienCamDo
            // 
            this.colTongTienCamDo.Caption = "Tổng tiền cầm";
            this.colTongTienCamDo.DisplayFormat.FormatString = "n0";
            this.colTongTienCamDo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienCamDo.FieldName = "TongTienCamDo";
            this.colTongTienCamDo.Name = "colTongTienCamDo";
            this.colTongTienCamDo.Visible = true;
            this.colTongTienCamDo.VisibleIndex = 2;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 0;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 281);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1127, 6);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.grcTongTienVangKhachChuoc);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 287);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1127, 142);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "Tổng tiền - vàng khách chuộc";
            // 
            // grcTongTienVangKhachChuoc
            // 
            this.grcTongTienVangKhachChuoc.DataSource = this.bangThongKeCamDoBindingSource;
            this.grcTongTienVangKhachChuoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTongTienVangKhachChuoc.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongTienVangKhachChuoc.Location = new System.Drawing.Point(2, 25);
            this.grcTongTienVangKhachChuoc.MainView = this.grvTongTienVangKhachChuoc;
            this.grcTongTienVangKhachChuoc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongTienVangKhachChuoc.MenuManager = this.barManager1;
            this.grcTongTienVangKhachChuoc.Name = "grcTongTienVangKhachChuoc";
            this.grcTongTienVangKhachChuoc.Size = new System.Drawing.Size(1123, 115);
            this.grcTongTienVangKhachChuoc.TabIndex = 0;
            this.grcTongTienVangKhachChuoc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTongTienVangKhachChuoc});
            // 
            // grvTongTienVangKhachChuoc
            // 
            this.grvTongTienVangKhachChuoc.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongTrongLuongThanhToan,
            this.colTongTienThanhToan,
            this.colTongTienLaiThanhToan,
            this.colKiHieuHamLuongVang1});
            this.grvTongTienVangKhachChuoc.GridControl = this.grcTongTienVangKhachChuoc;
            this.grvTongTienVangKhachChuoc.Name = "grvTongTienVangKhachChuoc";
            this.grvTongTienVangKhachChuoc.OptionsBehavior.Editable = false;
            this.grvTongTienVangKhachChuoc.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvTongTienVangKhachChuoc.OptionsView.ShowGroupPanel = false;
            // 
            // colTongTrongLuongThanhToan
            // 
            this.colTongTrongLuongThanhToan.Caption = "Tổng trọng lượng";
            this.colTongTrongLuongThanhToan.DisplayFormat.FormatString = "#,0.#";
            this.colTongTrongLuongThanhToan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTrongLuongThanhToan.FieldName = "TongTrongLuongThanhToan";
            this.colTongTrongLuongThanhToan.Name = "colTongTrongLuongThanhToan";
            this.colTongTrongLuongThanhToan.Visible = true;
            this.colTongTrongLuongThanhToan.VisibleIndex = 1;
            // 
            // colTongTienThanhToan
            // 
            this.colTongTienThanhToan.Caption = "Tổng tiền chuộc";
            this.colTongTienThanhToan.DisplayFormat.FormatString = "n0";
            this.colTongTienThanhToan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienThanhToan.FieldName = "TongTienThanhToan";
            this.colTongTienThanhToan.Name = "colTongTienThanhToan";
            this.colTongTienThanhToan.Visible = true;
            this.colTongTienThanhToan.VisibleIndex = 2;
            // 
            // colTongTienLaiThanhToan
            // 
            this.colTongTienLaiThanhToan.Caption = "Tổng tiền lãi";
            this.colTongTienLaiThanhToan.DisplayFormat.FormatString = "n0";
            this.colTongTienLaiThanhToan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienLaiThanhToan.FieldName = "TongTienLaiThanhToan";
            this.colTongTienLaiThanhToan.Name = "colTongTienLaiThanhToan";
            this.colTongTienLaiThanhToan.Visible = true;
            this.colTongTienLaiThanhToan.VisibleIndex = 3;
            // 
            // colKiHieuHamLuongVang1
            // 
            this.colKiHieuHamLuongVang1.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang1.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang1.Name = "colKiHieuHamLuongVang1";
            this.colKiHieuHamLuongVang1.Visible = true;
            this.colKiHieuHamLuongVang1.VisibleIndex = 0;
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl2.Location = new System.Drawing.Point(0, 429);
            this.splitterControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(1127, 6);
            this.splitterControl2.TabIndex = 8;
            this.splitterControl2.TabStop = false;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.grcTongTienVangThanhLy);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(0, 435);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1127, 146);
            this.groupControl4.TabIndex = 9;
            this.groupControl4.Text = "Tổng tiền - vàng thanh lý";
            // 
            // grcTongTienVangThanhLy
            // 
            this.grcTongTienVangThanhLy.DataSource = this.bangThongKeCamDoBindingSource;
            this.grcTongTienVangThanhLy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTongTienVangThanhLy.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongTienVangThanhLy.Location = new System.Drawing.Point(2, 25);
            this.grcTongTienVangThanhLy.MainView = this.grvTongTienVangThanhLy;
            this.grcTongTienVangThanhLy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongTienVangThanhLy.MenuManager = this.barManager1;
            this.grcTongTienVangThanhLy.Name = "grcTongTienVangThanhLy";
            this.grcTongTienVangThanhLy.Size = new System.Drawing.Size(1123, 119);
            this.grcTongTienVangThanhLy.TabIndex = 0;
            this.grcTongTienVangThanhLy.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTongTienVangThanhLy});
            // 
            // grvTongTienVangThanhLy
            // 
            this.grvTongTienVangThanhLy.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongTrongLuongThanhLy1,
            this.colTongTienLaiThanhLy1,
            this.colTongTienThanhLy1,
            this.colKiHieuHamLuongVang2});
            this.grvTongTienVangThanhLy.GridControl = this.grcTongTienVangThanhLy;
            this.grvTongTienVangThanhLy.Name = "grvTongTienVangThanhLy";
            this.grvTongTienVangThanhLy.OptionsBehavior.Editable = false;
            this.grvTongTienVangThanhLy.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvTongTienVangThanhLy.OptionsView.ShowGroupPanel = false;
            // 
            // colTongTrongLuongThanhLy1
            // 
            this.colTongTrongLuongThanhLy1.Caption = "Tổng trọng lượng";
            this.colTongTrongLuongThanhLy1.DisplayFormat.FormatString = "#,0.#";
            this.colTongTrongLuongThanhLy1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTrongLuongThanhLy1.FieldName = "TongTrongLuongThanhLy";
            this.colTongTrongLuongThanhLy1.Name = "colTongTrongLuongThanhLy1";
            this.colTongTrongLuongThanhLy1.Visible = true;
            this.colTongTrongLuongThanhLy1.VisibleIndex = 1;
            // 
            // colTongTienLaiThanhLy1
            // 
            this.colTongTienLaiThanhLy1.Caption = "Tổng tiền lãi";
            this.colTongTienLaiThanhLy1.DisplayFormat.FormatString = "n0";
            this.colTongTienLaiThanhLy1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienLaiThanhLy1.FieldName = "TongTienLaiThanhLy";
            this.colTongTienLaiThanhLy1.Name = "colTongTienLaiThanhLy1";
            this.colTongTienLaiThanhLy1.Visible = true;
            this.colTongTienLaiThanhLy1.VisibleIndex = 3;
            // 
            // colTongTienThanhLy1
            // 
            this.colTongTienThanhLy1.Caption = "Tổng tiền thanh lý";
            this.colTongTienThanhLy1.DisplayFormat.FormatString = "n0";
            this.colTongTienThanhLy1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienThanhLy1.FieldName = "TongTienThanhLy";
            this.colTongTienThanhLy1.Name = "colTongTienThanhLy1";
            this.colTongTienThanhLy1.Visible = true;
            this.colTongTienThanhLy1.VisibleIndex = 2;
            // 
            // colKiHieuHamLuongVang2
            // 
            this.colKiHieuHamLuongVang2.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang2.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang2.Name = "colKiHieuHamLuongVang2";
            this.colKiHieuHamLuongVang2.Visible = true;
            this.colKiHieuHamLuongVang2.VisibleIndex = 0;
            // 
            // splitterControl3
            // 
            this.splitterControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl3.Location = new System.Drawing.Point(0, 581);
            this.splitterControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl3.Name = "splitterControl3";
            this.splitterControl3.Size = new System.Drawing.Size(1127, 6);
            this.splitterControl3.TabIndex = 10;
            this.splitterControl3.TabStop = false;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.grcGiaHanHopDong);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl5.Location = new System.Drawing.Point(0, 587);
            this.groupControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(1127, 143);
            this.groupControl5.TabIndex = 11;
            this.groupControl5.Text = "Gia hạn hợp đồng";
            // 
            // grcGiaHanHopDong
            // 
            this.grcGiaHanHopDong.DataSource = this.bangThongKeCamDoBindingSource;
            this.grcGiaHanHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcGiaHanHopDong.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcGiaHanHopDong.Location = new System.Drawing.Point(2, 25);
            this.grcGiaHanHopDong.MainView = this.grvGiaHanHopDong;
            this.grcGiaHanHopDong.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcGiaHanHopDong.MenuManager = this.barManager1;
            this.grcGiaHanHopDong.Name = "grcGiaHanHopDong";
            this.grcGiaHanHopDong.Size = new System.Drawing.Size(1123, 116);
            this.grcGiaHanHopDong.TabIndex = 0;
            this.grcGiaHanHopDong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvGiaHanHopDong});
            // 
            // grvGiaHanHopDong
            // 
            this.grvGiaHanHopDong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongTrongLuongGiaHan2,
            this.colTongTienCamGiaHan2,
            this.colTongTienLaiGiaHan2,
            this.colKiHieuHamLuongVang3});
            this.grvGiaHanHopDong.GridControl = this.grcGiaHanHopDong;
            this.grvGiaHanHopDong.Name = "grvGiaHanHopDong";
            this.grvGiaHanHopDong.OptionsBehavior.Editable = false;
            this.grvGiaHanHopDong.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvGiaHanHopDong.OptionsView.ShowGroupPanel = false;
            // 
            // colTongTrongLuongGiaHan2
            // 
            this.colTongTrongLuongGiaHan2.Caption = "Tổng trọng lượng";
            this.colTongTrongLuongGiaHan2.DisplayFormat.FormatString = "#,0.#";
            this.colTongTrongLuongGiaHan2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTrongLuongGiaHan2.FieldName = "TongTrongLuongGiaHan";
            this.colTongTrongLuongGiaHan2.Name = "colTongTrongLuongGiaHan2";
            this.colTongTrongLuongGiaHan2.Visible = true;
            this.colTongTrongLuongGiaHan2.VisibleIndex = 1;
            // 
            // colTongTienCamGiaHan2
            // 
            this.colTongTienCamGiaHan2.Caption = "Tổng tiền cầm";
            this.colTongTienCamGiaHan2.DisplayFormat.FormatString = "n0";
            this.colTongTienCamGiaHan2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienCamGiaHan2.FieldName = "TongTienCamGiaHan";
            this.colTongTienCamGiaHan2.Name = "colTongTienCamGiaHan2";
            this.colTongTienCamGiaHan2.Visible = true;
            this.colTongTienCamGiaHan2.VisibleIndex = 2;
            // 
            // colTongTienLaiGiaHan2
            // 
            this.colTongTienLaiGiaHan2.Caption = "Tổng tiền lãi";
            this.colTongTienLaiGiaHan2.DisplayFormat.FormatString = "n0";
            this.colTongTienLaiGiaHan2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienLaiGiaHan2.FieldName = "TongTienLaiGiaHan";
            this.colTongTienLaiGiaHan2.Name = "colTongTienLaiGiaHan2";
            this.colTongTienLaiGiaHan2.Visible = true;
            this.colTongTienLaiGiaHan2.VisibleIndex = 3;
            // 
            // colKiHieuHamLuongVang3
            // 
            this.colKiHieuHamLuongVang3.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang3.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang3.Name = "colKiHieuHamLuongVang3";
            this.colKiHieuHamLuongVang3.Visible = true;
            this.colKiHieuHamLuongVang3.VisibleIndex = 0;
            // 
            // frmThongKeCamDo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 730);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.splitterControl3);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.splitterControl2);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmThongKeCamDo";
            this.Text = "Thống kê cầm đồ";
            this.Load += new System.EventHandler(this.frmThongKeCamDo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTongTienVangKhachCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongTienVangKhachCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTongTienVangKhachChuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongTienVangKhachChuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTongTienVangThanhLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongTienVangThanhLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcGiaHanHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvGiaHanHopDong)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnInThongKe;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnThongKe;
        private DevExpress.XtraEditors.DateEdit detTuNgay;
        private DevExpress.XtraEditors.DateEdit detDenNgay;
        private DevExpress.XtraEditors.LookUpEdit lueThoiGianThongKe;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.SplitterControl splitterControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl grcTongTienVangKhachChuoc;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTongTienVangKhachChuoc;
        private DevExpress.XtraGrid.GridControl grcTongTienVangKhachCam;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTongTienVangKhachCam;
        private DevExpress.XtraGrid.GridControl grcTongTienVangThanhLy;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTongTienVangThanhLy;
        private DevExpress.XtraGrid.GridControl grcGiaHanHopDong;
        private DevExpress.XtraGrid.Views.Grid.GridView grvGiaHanHopDong;
        private System.Windows.Forms.BindingSource bangThongKeCamDoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTrongLuongGiaHan2;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienCamGiaHan2;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienLaiGiaHan2;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang3;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTrongLuongThanhLy1;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienLaiThanhLy1;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienThanhLy1;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang2;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTrongLuongThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienLaiThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang1;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTrongLuongCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraBars.BarButtonItem btnXuatExcel;
    }
}