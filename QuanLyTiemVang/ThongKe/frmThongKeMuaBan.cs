﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyTiemVang.Constants;
using DTO.TemporaryModel;
using Share.Constant;
using BUS;
using QuanLyTiemVang.Services;
using DTO.Models;
using QuanLyDieuXe.DialogCustom;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeMuaBan : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeMuaBan()
        {
            InitializeComponent();
        }

        private DateTime thongKeTuNgay;
        private DateTime thongKeDenNgay;

        private void frmThongKeTongHop_Load(object sender, EventArgs e)
        {
            SetDataSourceForThoiGian();
            detTuNgay.Properties.MaxValue = DateTime.Today;
            detDenNgay.Properties.MaxValue = DateTime.Today;
        }

        private void SetDataSourceForThoiGian()
        {
            List<LookUpEditItem> listThoiGianThongKe = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_HOM_NAY
                },
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_KHOAN_THOI_GIAN
                }
            };

            lueThoiGianThongKe.Properties.DataSource = listThoiGianThongKe;
            lueThoiGianThongKe.Properties.DisplayMember = "DisplayMember";
            lueThoiGianThongKe.Properties.ValueMember = "ValueMember";
        }

        private bool IsHaveNgayDauKi()
        {
            if (NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast() == null)
            {
                PopupService.Instance.Warning(MessageWarning.NGAY_DAU_KI_NULL);

                return false;
            }

            return true;
        }

        private void GetThongKeByDate(DateTime ngayThongKe)
        {
            List<BangThongKeTheoHamLuongVang> listThongKeKhoMoi = BangThongKeTheoHamLuongVangBUS.Instance.GetBangThongKeByDate(ngayThongKe);
            List<BangThongKeKhoCu> listThongKeKhoCu = BangThongKeKhoCuBUS.Instance.GetBangThongKeByDate(ngayThongKe);

            LoadData(listThongKeKhoMoi, listThongKeKhoCu);
        }

        private void GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            List<BangThongKeTheoHamLuongVang> listThongKeKhoMoi = BangThongKeTheoHamLuongVangBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
            List<BangThongKeKhoCu> listThongKeKhoCu = BangThongKeKhoCuBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);

            LoadData(listThongKeKhoMoi, listThongKeKhoCu);
        }

        private void LoadData(List<BangThongKeTheoHamLuongVang> listThongKeKhoMoi, List<BangThongKeKhoCu> listThongKeKhoCu)
        {
            List<KiHieuHamLuongVang> listHamLuongVang = KiHieuHamLuongVangBUS.Instance.GetListAllHamLuongVang();
            List<ThongKeVangDoiNgangTemp> listThongKeVangDoiVang = new List<ThongKeVangDoiNgangTemp>();

            foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
            {
                listThongKeVangDoiVang.Add(new ThongKeVangDoiNgangTemp
                {
                    HamLuongVang = hamLuongVang.HamLuongVang,
                    TongKhoiLuongVangThuVao = listThongKeKhoCu != null ? listThongKeKhoCu.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangDoiNgangVao) : 0,
                    TongKhoiLuongVangXuatRa = listThongKeKhoMoi != null ? listThongKeKhoMoi.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangDoiNgangRa) : 0
                });
            }

            grcVangBanRa.DataSource = listThongKeKhoMoi;
            grcVangMuaVao.DataSource = listThongKeKhoCu;
            grcVangDoiNgang.DataSource = listThongKeVangDoiVang;
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcVangBanRa.DataSource = null;
            grcVangDoiNgang.DataSource = null;
            grcVangMuaVao.DataSource = null;
            lueThoiGianThongKe.EditValue = null;
            detDenNgay.EditValue = null;
            detTuNgay.EditValue = null;
            detTuNgay.ReadOnly = true;
            detDenNgay.ReadOnly = true;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!BangThongKeTheoHamLuongVangBUS.Instance.TinhToanThongKe() || !BangThongKeKhoCuBUS.Instance.TinhToanThongKe())
            {
                PopupService.Instance.Error(MessageError.TINH_TOAN_THONG_KE);
            }
            else
            {
                PopupService.Instance.Success(MessageSucces.TINH_TOAN_THONG_KE);
            }
        }

        private void btnInThongKe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<BangThongKeTheoHamLuongVang> listThongKeKhoMoi = (List<BangThongKeTheoHamLuongVang>)grcVangBanRa.DataSource;
            List<ThongKeVangDoiNgangTemp> listThongKeVangDoiVang = (List<ThongKeVangDoiNgangTemp>)grcVangDoiNgang.DataSource;
            List<BangThongKeKhoCu> listThongKeKhoCu = (List<BangThongKeKhoCu>)grcVangMuaVao.DataSource;

            if (listThongKeKhoMoi != null)
            {
                List<BangInThongKeMuaBanTemp> listBangInVangBanRa = new List<BangInThongKeMuaBanTemp>();

                foreach (BangThongKeTheoHamLuongVang bangThongKe in listThongKeKhoMoi)
                {
                    listBangInVangBanRa.Add(new BangInThongKeMuaBanTemp
                    {
                        TieuDeThongKe = "VÀNG BÁN RA",
                        TieuChiThongKeHai = "Tổng trọng lượng",
                        TieuChiThongKeBa = "Thành tiền",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.KiHieuHamLuongVang.HamLuongVang,
                        GiaTriThongKeHai = bangThongKe.TongKhoiLuongVangBanRa,
                        GiaTriThongKeBa = bangThongKe.TongTienVangBanRa
                    });
                }

                ThongKeMuaBanPrint thongKePrint = new ThongKeMuaBanPrint();

                thongKePrint.DataSource = listBangInVangBanRa;
                thongKePrint.ShowPreviewDialog();
            }

            if (listThongKeKhoCu != null)
            {
                List<BangInThongKeMuaBanTemp> listBangInVangMuaVao = new List<BangInThongKeMuaBanTemp>();

                foreach (BangThongKeKhoCu bangThongKe in listThongKeKhoCu)
                {
                    listBangInVangMuaVao.Add(new BangInThongKeMuaBanTemp
                    {
                        TieuDeThongKe = "VÀNG MUA VÀO",
                        TieuChiThongKeHai = "Tổng trọng lượng",
                        TieuChiThongKeBa = "Thành tiền",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.KiHieuHamLuongVang.HamLuongVang,
                        GiaTriThongKeHai = bangThongKe.KhoiLuongVangCuMuaVao,
                        GiaTriThongKeBa = bangThongKe.TongTienVangMuaVao
                    });
                }

                ThongKeMuaBanPrint thongKePrint = new ThongKeMuaBanPrint();

                thongKePrint.DataSource = listBangInVangMuaVao;
                thongKePrint.ShowPreviewDialog();
            }

            if (listThongKeVangDoiVang != null)
            {
                List<BangInThongKeMuaBanTemp> listBangInVangDoiNgang = new List<BangInThongKeMuaBanTemp>();

                foreach (ThongKeVangDoiNgangTemp bangThongKe in listThongKeVangDoiVang)
                {
                    listBangInVangDoiNgang.Add(new BangInThongKeMuaBanTemp
                    {
                        TieuDeThongKe = "VÀNG ĐỔI NGANG",
                        TieuChiThongKeHai = "Tổng KLV xuất ra",
                        TieuChiThongKeBa = "Tổng KLV thu vào",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.HamLuongVang,
                        GiaTriThongKeHai = bangThongKe.TongKhoiLuongVangXuatRa,
                        GiaTriThongKeBa = bangThongKe.TongKhoiLuongVangThuVao
                    });
                }

                ThongKeMuaBanPrint thongKePrint = new ThongKeMuaBanPrint();

                thongKePrint.DataSource = listBangInVangDoiNgang;
                thongKePrint.ShowPreviewDialog();
            }
        }

        private void lueThoiGianThongKe_EditValueChanged(object sender, EventArgs e)
        {
            if (lueThoiGianThongKe.EditValue == null)
            {
                return;
            }

            if ((int)lueThoiGianThongKe.EditValue == TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN)
            {
                detTuNgay.ReadOnly = false;
                detDenNgay.ReadOnly = false;
            }
            else
            {
                detTuNgay.ReadOnly = true;
                detDenNgay.ReadOnly = true;
            }
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (!IsHaveNgayDauKi())
            {
                return;
            }

            if (lueThoiGianThongKe.EditValue == null || TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY == Convert.ToInt32(lueThoiGianThongKe.EditValue))
            {
                GetThongKeByDate(DateTime.Today);
                thongKeTuNgay = DateTime.Today;
                thongKeDenNgay = DateTime.Today;
            }
            else if (detTuNgay.DateTime.Date == detDenNgay.DateTime)
            {
                GetThongKeByDate(detTuNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
            else
            {
                GetThongKeByTwoDifferenceDate(detTuNgay.DateTime, detDenNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
        }

        private void btnXuatExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string pathSave;
            string fileName = "";
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.ShowNewFolderButton = true;
            folder.Description = "Chọn vị trí lưu file!";
            if (folder.ShowDialog() == DialogResult.OK)
            {
                pathSave = folder.SelectedPath + "\\";
                if (InputBox.Show("Thông báo!",
                "&Nhập tên file:", ref fileName) == DialogResult.OK)
                {
                    string pathSave1 = pathSave + (fileName + " 1" + ".xls");
                    string pathSave2 = pathSave + (fileName + " 2" + ".xls");
                    string pathSave3 = pathSave + (fileName + " 3" + ".xls");
                    grcVangBanRa.ExportToXls(pathSave1);
                    grcVangMuaVao.ExportToXls(pathSave2);
                    grcVangDoiNgang.ExportToXls(pathSave3);
                }
            }
        }
    }
}