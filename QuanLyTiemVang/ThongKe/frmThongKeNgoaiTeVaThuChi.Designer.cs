﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmThongKeNgoaiTeVaThuChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnInThongKe = new DevExpress.XtraBars.BarButtonItem();
            this.btnXuatExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.detTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.detDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.lueThoiGianThongKe = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcTongNgoaiTeMuaVao = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeDoiNgoaiTeBindingSource = new System.Windows.Forms.BindingSource();
            this.grvTongNgoaiTeMuaVao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongTienNgoaiTeThuVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienVietDoiRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.grcTongNgoaiTeBanRa = new DevExpress.XtraGrid.GridControl();
            this.grvTongNgoaiTeBanRa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongTienNgoaiTeDoiRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienVietThuVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiTien1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.grcThuChiTienMat = new DevExpress.XtraGrid.GridControl();
            this.thuChiTienMatTempBindingSource = new System.Windows.Forms.BindingSource();
            this.grvThuChiTienMat = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colConLai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongThu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bangThongKeTienMatBindingSource = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTongNgoaiTeMuaVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeDoiNgoaiTeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongNgoaiTeMuaVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTongNgoaiTeBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongNgoaiTeBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcThuChiTienMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thuChiTienMatTempBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThuChiTienMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeTienMatBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnInThongKe,
            this.btnXuatExcel});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 11;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInThongKe, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXuatExcel, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInThongKe
            // 
            this.btnInThongKe.Caption = "In thống kê";
            this.btnInThongKe.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInThongKe.Id = 9;
            this.btnInThongKe.Name = "btnInThongKe";
            this.btnInThongKe.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInThongKe_ItemClick);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Caption = "Xuất excel";
            this.btnXuatExcel.Glyph = global::QuanLyTiemVang.Properties.Resources.netvibes_icon;
            this.btnXuatExcel.Id = 10;
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatExcel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1035, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 747);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 697);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1035, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 697);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1035, 85);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tiêu chí thống kê";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnThongKe);
            this.layoutControl1.Controls.Add(this.detTuNgay);
            this.layoutControl1.Controls.Add(this.detDenNgay);
            this.layoutControl1.Controls.Add(this.lueThoiGianThongKe);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1031, 58);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnThongKe
            // 
            this.btnThongKe.Location = new System.Drawing.Point(752, 16);
            this.btnThongKe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(242, 27);
            this.btnThongKe.StyleController = this.layoutControl1;
            this.btnThongKe.TabIndex = 7;
            this.btnThongKe.Text = "Thống kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // detTuNgay
            // 
            this.detTuNgay.EditValue = null;
            this.detTuNgay.Location = new System.Drawing.Point(355, 16);
            this.detTuNgay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detTuNgay.MenuManager = this.barManager1;
            this.detTuNgay.Name = "detTuNgay";
            this.detTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detTuNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detTuNgay.Properties.ReadOnly = true;
            this.detTuNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detTuNgay.Size = new System.Drawing.Size(145, 22);
            this.detTuNgay.StyleController = this.layoutControl1;
            this.detTuNgay.TabIndex = 6;
            // 
            // detDenNgay
            // 
            this.detDenNgay.EditValue = null;
            this.detDenNgay.Location = new System.Drawing.Point(600, 16);
            this.detDenNgay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detDenNgay.MenuManager = this.barManager1;
            this.detDenNgay.Name = "detDenNgay";
            this.detDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detDenNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detDenNgay.Properties.ReadOnly = true;
            this.detDenNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detDenNgay.Size = new System.Drawing.Size(146, 22);
            this.detDenNgay.StyleController = this.layoutControl1;
            this.detDenNgay.TabIndex = 5;
            // 
            // lueThoiGianThongKe
            // 
            this.lueThoiGianThongKe.Location = new System.Drawing.Point(110, 16);
            this.lueThoiGianThongKe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueThoiGianThongKe.MenuManager = this.barManager1;
            this.lueThoiGianThongKe.Name = "lueThoiGianThongKe";
            this.lueThoiGianThongKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueThoiGianThongKe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Thống kê theo")});
            this.lueThoiGianThongKe.Properties.NullText = "Hôm nay";
            this.lueThoiGianThongKe.Size = new System.Drawing.Size(145, 22);
            this.lueThoiGianThongKe.StyleController = this.layoutControl1;
            this.lueThoiGianThongKe.TabIndex = 4;
            this.lueThoiGianThongKe.EditValueChanged += new System.EventHandler(this.lueNgayThongKe_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1010, 59);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueThoiGianThongKe;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(245, 33);
            this.layoutControlItem1.Text = "Ngày thống kê";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.detDenNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(490, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(246, 33);
            this.layoutControlItem2.Text = "Đến ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.detTuNgay;
            this.layoutControlItem3.Location = new System.Drawing.Point(245, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(245, 33);
            this.layoutControlItem3.Text = "Từ ngày";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnThongKe;
            this.layoutControlItem4.Location = new System.Drawing.Point(736, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(248, 33);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcTongNgoaiTeMuaVao);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 135);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1035, 209);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Tổng ngoại tệ mua vào";
            // 
            // grcTongNgoaiTeMuaVao
            // 
            this.grcTongNgoaiTeMuaVao.DataSource = this.bangThongKeDoiNgoaiTeBindingSource;
            this.grcTongNgoaiTeMuaVao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTongNgoaiTeMuaVao.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongNgoaiTeMuaVao.Location = new System.Drawing.Point(2, 25);
            this.grcTongNgoaiTeMuaVao.MainView = this.grvTongNgoaiTeMuaVao;
            this.grcTongNgoaiTeMuaVao.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongNgoaiTeMuaVao.MenuManager = this.barManager1;
            this.grcTongNgoaiTeMuaVao.Name = "grcTongNgoaiTeMuaVao";
            this.grcTongNgoaiTeMuaVao.Size = new System.Drawing.Size(1031, 182);
            this.grcTongNgoaiTeMuaVao.TabIndex = 0;
            this.grcTongNgoaiTeMuaVao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTongNgoaiTeMuaVao});
            // 
            // bangThongKeDoiNgoaiTeBindingSource
            // 
            this.bangThongKeDoiNgoaiTeBindingSource.DataSource = typeof(DTO.Models.BangThongKeDoiNgoaiTe);
            // 
            // grvTongNgoaiTeMuaVao
            // 
            this.grvTongNgoaiTeMuaVao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongTienNgoaiTeThuVao,
            this.colTongTienVietDoiRa,
            this.colLoaiTien});
            this.grvTongNgoaiTeMuaVao.GridControl = this.grcTongNgoaiTeMuaVao;
            this.grvTongNgoaiTeMuaVao.Name = "grvTongNgoaiTeMuaVao";
            this.grvTongNgoaiTeMuaVao.OptionsBehavior.Editable = false;
            this.grvTongNgoaiTeMuaVao.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvTongNgoaiTeMuaVao.OptionsView.ShowGroupPanel = false;
            // 
            // colTongTienNgoaiTeThuVao
            // 
            this.colTongTienNgoaiTeThuVao.Caption = "Tổng tiền ngoại tệ";
            this.colTongTienNgoaiTeThuVao.DisplayFormat.FormatString = "n0";
            this.colTongTienNgoaiTeThuVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienNgoaiTeThuVao.FieldName = "TongTienNgoaiTeThuVao";
            this.colTongTienNgoaiTeThuVao.Name = "colTongTienNgoaiTeThuVao";
            this.colTongTienNgoaiTeThuVao.Visible = true;
            this.colTongTienNgoaiTeThuVao.VisibleIndex = 1;
            // 
            // colTongTienVietDoiRa
            // 
            this.colTongTienVietDoiRa.Caption = "Thành tiền VNĐ";
            this.colTongTienVietDoiRa.DisplayFormat.FormatString = "n0";
            this.colTongTienVietDoiRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienVietDoiRa.FieldName = "TongTienVietDoiRa";
            this.colTongTienVietDoiRa.Name = "colTongTienVietDoiRa";
            this.colTongTienVietDoiRa.Visible = true;
            this.colTongTienVietDoiRa.VisibleIndex = 2;
            // 
            // colLoaiTien
            // 
            this.colLoaiTien.Caption = "Tên ngoại tệ";
            this.colLoaiTien.FieldName = "LoaiTien.TenLoaiTien";
            this.colLoaiTien.Name = "colLoaiTien";
            this.colLoaiTien.Visible = true;
            this.colLoaiTien.VisibleIndex = 0;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 344);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1035, 6);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.grcTongNgoaiTeBanRa);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 350);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1035, 220);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "Tổng ngoại tệ bán ra";
            // 
            // grcTongNgoaiTeBanRa
            // 
            this.grcTongNgoaiTeBanRa.DataSource = this.bangThongKeDoiNgoaiTeBindingSource;
            this.grcTongNgoaiTeBanRa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTongNgoaiTeBanRa.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongNgoaiTeBanRa.Location = new System.Drawing.Point(2, 25);
            this.grcTongNgoaiTeBanRa.MainView = this.grvTongNgoaiTeBanRa;
            this.grcTongNgoaiTeBanRa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcTongNgoaiTeBanRa.MenuManager = this.barManager1;
            this.grcTongNgoaiTeBanRa.Name = "grcTongNgoaiTeBanRa";
            this.grcTongNgoaiTeBanRa.Size = new System.Drawing.Size(1031, 193);
            this.grcTongNgoaiTeBanRa.TabIndex = 0;
            this.grcTongNgoaiTeBanRa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTongNgoaiTeBanRa});
            // 
            // grvTongNgoaiTeBanRa
            // 
            this.grvTongNgoaiTeBanRa.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongTienNgoaiTeDoiRa,
            this.colTongTienVietThuVao,
            this.colLoaiTien1});
            this.grvTongNgoaiTeBanRa.GridControl = this.grcTongNgoaiTeBanRa;
            this.grvTongNgoaiTeBanRa.Name = "grvTongNgoaiTeBanRa";
            this.grvTongNgoaiTeBanRa.OptionsBehavior.Editable = false;
            this.grvTongNgoaiTeBanRa.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvTongNgoaiTeBanRa.OptionsView.ShowGroupPanel = false;
            // 
            // colTongTienNgoaiTeDoiRa
            // 
            this.colTongTienNgoaiTeDoiRa.Caption = "Tổng tiền ngoại tệ";
            this.colTongTienNgoaiTeDoiRa.DisplayFormat.FormatString = "n0";
            this.colTongTienNgoaiTeDoiRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienNgoaiTeDoiRa.FieldName = "TongTienNgoaiTeDoiRa";
            this.colTongTienNgoaiTeDoiRa.Name = "colTongTienNgoaiTeDoiRa";
            this.colTongTienNgoaiTeDoiRa.Visible = true;
            this.colTongTienNgoaiTeDoiRa.VisibleIndex = 1;
            // 
            // colTongTienVietThuVao
            // 
            this.colTongTienVietThuVao.Caption = "Thành tiền VNĐ";
            this.colTongTienVietThuVao.DisplayFormat.FormatString = "n0";
            this.colTongTienVietThuVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienVietThuVao.FieldName = "TongTienVietThuVao";
            this.colTongTienVietThuVao.Name = "colTongTienVietThuVao";
            this.colTongTienVietThuVao.Visible = true;
            this.colTongTienVietThuVao.VisibleIndex = 2;
            // 
            // colLoaiTien1
            // 
            this.colLoaiTien1.Caption = "Tên ngoại tệ";
            this.colLoaiTien1.FieldName = "LoaiTien.TenLoaiTien";
            this.colLoaiTien1.Name = "colLoaiTien1";
            this.colLoaiTien1.Visible = true;
            this.colLoaiTien1.VisibleIndex = 0;
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl2.Location = new System.Drawing.Point(0, 570);
            this.splitterControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(1035, 6);
            this.splitterControl2.TabIndex = 8;
            this.splitterControl2.TabStop = false;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.grcThuChiTienMat);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(0, 576);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1035, 171);
            this.groupControl4.TabIndex = 9;
            this.groupControl4.Text = "Thu - chi tiền mặt";
            // 
            // grcThuChiTienMat
            // 
            this.grcThuChiTienMat.DataSource = this.thuChiTienMatTempBindingSource;
            this.grcThuChiTienMat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcThuChiTienMat.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcThuChiTienMat.Location = new System.Drawing.Point(2, 25);
            this.grcThuChiTienMat.MainView = this.grvThuChiTienMat;
            this.grcThuChiTienMat.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcThuChiTienMat.MenuManager = this.barManager1;
            this.grcThuChiTienMat.Name = "grcThuChiTienMat";
            this.grcThuChiTienMat.Size = new System.Drawing.Size(1031, 144);
            this.grcThuChiTienMat.TabIndex = 0;
            this.grcThuChiTienMat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvThuChiTienMat});
            // 
            // thuChiTienMatTempBindingSource
            // 
            this.thuChiTienMatTempBindingSource.DataSource = typeof(DTO.TemporaryModel.ThuChiTienMatTemp);
            // 
            // grvThuChiTienMat
            // 
            this.grvThuChiTienMat.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colConLai,
            this.colTongChi,
            this.colTongThu});
            this.grvThuChiTienMat.GridControl = this.grcThuChiTienMat;
            this.grvThuChiTienMat.Name = "grvThuChiTienMat";
            this.grvThuChiTienMat.OptionsBehavior.Editable = false;
            this.grvThuChiTienMat.OptionsBehavior.ReadOnly = true;
            this.grvThuChiTienMat.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvThuChiTienMat.OptionsView.ShowGroupPanel = false;
            // 
            // colConLai
            // 
            this.colConLai.Caption = "Còn lại";
            this.colConLai.DisplayFormat.FormatString = "n0";
            this.colConLai.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colConLai.FieldName = "ConLai";
            this.colConLai.Name = "colConLai";
            this.colConLai.Visible = true;
            this.colConLai.VisibleIndex = 2;
            // 
            // colTongChi
            // 
            this.colTongChi.Caption = "Tổng chi";
            this.colTongChi.DisplayFormat.FormatString = "n0";
            this.colTongChi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongChi.FieldName = "TongChi";
            this.colTongChi.Name = "colTongChi";
            this.colTongChi.Visible = true;
            this.colTongChi.VisibleIndex = 1;
            // 
            // colTongThu
            // 
            this.colTongThu.Caption = "Tổng thu";
            this.colTongThu.DisplayFormat.FormatString = "n0";
            this.colTongThu.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongThu.FieldName = "TongThu";
            this.colTongThu.Name = "colTongThu";
            this.colTongThu.Visible = true;
            this.colTongThu.VisibleIndex = 0;
            // 
            // bangThongKeTienMatBindingSource
            // 
            this.bangThongKeTienMatBindingSource.DataSource = typeof(DTO.Models.BangThongKeTienMat);
            // 
            // frmThongKeNgoaiTeVaThuChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 747);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.splitterControl2);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmThongKeNgoaiTeVaThuChi";
            this.Text = "Thống kê trao đổi ngoại tệ & thu - chi";
            this.Load += new System.EventHandler(this.frmThongKeNgoaiTeVaThuChi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTongNgoaiTeMuaVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeDoiNgoaiTeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongNgoaiTeMuaVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTongNgoaiTeBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTongNgoaiTeBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcThuChiTienMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thuChiTienMatTempBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThuChiTienMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeTienMatBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnInThongKe;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnThongKe;
        private DevExpress.XtraEditors.DateEdit detTuNgay;
        private DevExpress.XtraEditors.DateEdit detDenNgay;
        private DevExpress.XtraEditors.LookUpEdit lueThoiGianThongKe;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridControl grcThuChiTienMat;
        private DevExpress.XtraGrid.Views.Grid.GridView grvThuChiTienMat;
        private DevExpress.XtraGrid.GridControl grcTongNgoaiTeBanRa;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTongNgoaiTeBanRa;
        private DevExpress.XtraGrid.GridControl grcTongNgoaiTeMuaVao;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTongNgoaiTeMuaVao;
        private System.Windows.Forms.BindingSource bangThongKeDoiNgoaiTeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienNgoaiTeDoiRa;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienVietThuVao;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiTien1;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienNgoaiTeThuVao;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienVietDoiRa;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiTien;
        private DevExpress.XtraBars.BarButtonItem btnXuatExcel;
        private System.Windows.Forms.BindingSource bangThongKeTienMatBindingSource;
        private System.Windows.Forms.BindingSource thuChiTienMatTempBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colConLai;
        private DevExpress.XtraGrid.Columns.GridColumn colTongChi;
        private DevExpress.XtraGrid.Columns.GridColumn colTongThu;
    }
}