﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmThongKeHamLuongVang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnInThongKe = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.detDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.detTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.btnThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.lueThoiGianThongKe = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcThongKeKhoMoi = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeTheoHamLuongVangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvThongKeKhoMoi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKhoiLuongVangDauKi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangMoiBanRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangCuBanRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangCuoiKi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangNhapVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangDoiNgangRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.thongKeTheoHamLuongVangTempBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.grcThongKeKhoCu = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeKhoCuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvThongKeKhoCu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKhoiLuongVangDauKi1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangNhapVao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangCuMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangDemDiGiaCong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangCuoiKi1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoiLuongVangDoiNgangVao = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeKhoMoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeTheoHamLuongVangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeKhoMoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeTheoHamLuongVangTempBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeKhoCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeKhoCuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeKhoCu)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnInThongKe});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 10;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInThongKe, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInThongKe
            // 
            this.btnInThongKe.Caption = "In thống kê";
            this.btnInThongKe.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInThongKe.Id = 9;
            this.btnInThongKe.Name = "btnInThongKe";
            this.btnInThongKe.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInThongKe_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1130, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 678);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1130, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 628);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1130, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 628);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1130, 121);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tiêu chí thống kê";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.detDenNgay);
            this.layoutControl1.Controls.Add(this.detTuNgay);
            this.layoutControl1.Controls.Add(this.btnThongKe);
            this.layoutControl1.Controls.Add(this.lueThoiGianThongKe);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1126, 94);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // detDenNgay
            // 
            this.detDenNgay.EditValue = null;
            this.detDenNgay.Location = new System.Drawing.Point(823, 16);
            this.detDenNgay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detDenNgay.MenuManager = this.barManager1;
            this.detDenNgay.Name = "detDenNgay";
            this.detDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detDenNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detDenNgay.Properties.ReadOnly = true;
            this.detDenNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detDenNgay.Size = new System.Drawing.Size(287, 22);
            this.detDenNgay.StyleController = this.layoutControl1;
            this.detDenNgay.TabIndex = 10;
            // 
            // detTuNgay
            // 
            this.detTuNgay.EditValue = null;
            this.detTuNgay.Location = new System.Drawing.Point(473, 16);
            this.detTuNgay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detTuNgay.MenuManager = this.barManager1;
            this.detTuNgay.Name = "detTuNgay";
            this.detTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detTuNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detTuNgay.Properties.ReadOnly = true;
            this.detTuNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detTuNgay.Size = new System.Drawing.Size(249, 22);
            this.detTuNgay.StyleController = this.layoutControl1;
            this.detTuNgay.TabIndex = 9;
            // 
            // btnThongKe
            // 
            this.btnThongKe.Location = new System.Drawing.Point(16, 44);
            this.btnThongKe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(1094, 27);
            this.btnThongKe.StyleController = this.layoutControl1;
            this.btnThongKe.TabIndex = 7;
            this.btnThongKe.Text = "Thống kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // lueThoiGianThongKe
            // 
            this.lueThoiGianThongKe.Location = new System.Drawing.Point(111, 16);
            this.lueThoiGianThongKe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueThoiGianThongKe.MenuManager = this.barManager1;
            this.lueThoiGianThongKe.Name = "lueThoiGianThongKe";
            this.lueThoiGianThongKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueThoiGianThongKe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Thống kê theo")});
            this.lueThoiGianThongKe.Properties.DisplayFormat.FormatString = "d";
            this.lueThoiGianThongKe.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lueThoiGianThongKe.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.lueThoiGianThongKe.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lueThoiGianThongKe.Properties.NullText = "Hôm nay";
            this.lueThoiGianThongKe.Size = new System.Drawing.Size(261, 22);
            this.lueThoiGianThongKe.StyleController = this.layoutControl1;
            this.lueThoiGianThongKe.TabIndex = 8;
            this.lueThoiGianThongKe.EditValueChanged += new System.EventHandler(this.lueThoiGianThongKe_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1126, 94);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnThongKe;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1100, 40);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueThoiGianThongKe;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(362, 28);
            this.layoutControlItem1.Text = "Ngày thống kê";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.detTuNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(362, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(350, 28);
            this.layoutControlItem2.Text = "Từ ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.detDenNgay;
            this.layoutControlItem3.Location = new System.Drawing.Point(712, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(388, 28);
            this.layoutControlItem3.Text = "Đến ngày";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(91, 17);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcThongKeKhoMoi);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 171);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1130, 297);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Kết quả thống kê kho mới";
            // 
            // grcThongKeKhoMoi
            // 
            this.grcThongKeKhoMoi.DataSource = this.bangThongKeTheoHamLuongVangBindingSource;
            this.grcThongKeKhoMoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcThongKeKhoMoi.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeKhoMoi.Location = new System.Drawing.Point(2, 25);
            this.grcThongKeKhoMoi.MainView = this.grvThongKeKhoMoi;
            this.grcThongKeKhoMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeKhoMoi.MenuManager = this.barManager1;
            this.grcThongKeKhoMoi.Name = "grcThongKeKhoMoi";
            this.grcThongKeKhoMoi.Size = new System.Drawing.Size(1126, 270);
            this.grcThongKeKhoMoi.TabIndex = 0;
            this.grcThongKeKhoMoi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvThongKeKhoMoi});
            // 
            // bangThongKeTheoHamLuongVangBindingSource
            // 
            this.bangThongKeTheoHamLuongVangBindingSource.DataSource = typeof(DTO.Models.BangThongKeTheoHamLuongVang);
            // 
            // grvThongKeKhoMoi
            // 
            this.grvThongKeKhoMoi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKhoiLuongVangDauKi,
            this.colKhoiLuongVangMoiBanRa,
            this.colKhoiLuongVangCuBanRa,
            this.colKhoiLuongVangCuoiKi,
            this.colKiHieuHamLuongVang,
            this.colKhoiLuongVangNhapVao,
            this.colKhoiLuongVangDoiNgangRa});
            this.grvThongKeKhoMoi.GridControl = this.grcThongKeKhoMoi;
            this.grvThongKeKhoMoi.Name = "grvThongKeKhoMoi";
            this.grvThongKeKhoMoi.OptionsBehavior.Editable = false;
            this.grvThongKeKhoMoi.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvThongKeKhoMoi.OptionsView.ShowGroupPanel = false;
            // 
            // colKhoiLuongVangDauKi
            // 
            this.colKhoiLuongVangDauKi.Caption = "KLV đầu kì";
            this.colKhoiLuongVangDauKi.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangDauKi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangDauKi.FieldName = "KhoiLuongVangDauKi";
            this.colKhoiLuongVangDauKi.Name = "colKhoiLuongVangDauKi";
            this.colKhoiLuongVangDauKi.Visible = true;
            this.colKhoiLuongVangDauKi.VisibleIndex = 1;
            // 
            // colKhoiLuongVangMoiBanRa
            // 
            this.colKhoiLuongVangMoiBanRa.Caption = "KLV mới bán ra";
            this.colKhoiLuongVangMoiBanRa.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangMoiBanRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangMoiBanRa.FieldName = "KhoiLuongVangMoiBanRa";
            this.colKhoiLuongVangMoiBanRa.Name = "colKhoiLuongVangMoiBanRa";
            this.colKhoiLuongVangMoiBanRa.Visible = true;
            this.colKhoiLuongVangMoiBanRa.VisibleIndex = 3;
            // 
            // colKhoiLuongVangCuBanRa
            // 
            this.colKhoiLuongVangCuBanRa.Caption = "KLV cũ bán ra";
            this.colKhoiLuongVangCuBanRa.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangCuBanRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangCuBanRa.FieldName = "KhoiLuongVangCuBanRa";
            this.colKhoiLuongVangCuBanRa.Name = "colKhoiLuongVangCuBanRa";
            this.colKhoiLuongVangCuBanRa.Visible = true;
            this.colKhoiLuongVangCuBanRa.VisibleIndex = 4;
            // 
            // colKhoiLuongVangCuoiKi
            // 
            this.colKhoiLuongVangCuoiKi.Caption = "KLV cuối kì";
            this.colKhoiLuongVangCuoiKi.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangCuoiKi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangCuoiKi.FieldName = "KhoiLuongVangCuoiKi";
            this.colKhoiLuongVangCuoiKi.Name = "colKhoiLuongVangCuoiKi";
            this.colKhoiLuongVangCuoiKi.Visible = true;
            this.colKhoiLuongVangCuoiKi.VisibleIndex = 6;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "HLV";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 0;
            // 
            // colKhoiLuongVangNhapVao
            // 
            this.colKhoiLuongVangNhapVao.Caption = "KLV nhập vào";
            this.colKhoiLuongVangNhapVao.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangNhapVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangNhapVao.FieldName = "KhoiLuongVangNhapVao";
            this.colKhoiLuongVangNhapVao.Name = "colKhoiLuongVangNhapVao";
            this.colKhoiLuongVangNhapVao.Visible = true;
            this.colKhoiLuongVangNhapVao.VisibleIndex = 2;
            // 
            // colKhoiLuongVangDoiNgangRa
            // 
            this.colKhoiLuongVangDoiNgangRa.Caption = "KLV đổi ngang";
            this.colKhoiLuongVangDoiNgangRa.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangDoiNgangRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangDoiNgangRa.FieldName = "KhoiLuongVangDoiNgangRa";
            this.colKhoiLuongVangDoiNgangRa.Name = "colKhoiLuongVangDoiNgangRa";
            this.colKhoiLuongVangDoiNgangRa.Visible = true;
            this.colKhoiLuongVangDoiNgangRa.VisibleIndex = 5;
            // 
            // thongKeTheoHamLuongVangTempBindingSource
            // 
            this.thongKeTheoHamLuongVangTempBindingSource.DataSource = typeof(DTO.TemporaryModel.ThongKeTheoHamLuongVangTemp);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 468);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1130, 6);
            this.splitterControl1.TabIndex = 10;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.grcThongKeKhoCu);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 474);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1130, 204);
            this.groupControl3.TabIndex = 11;
            this.groupControl3.Text = "Kết quả thống kê kho cũ";
            // 
            // grcThongKeKhoCu
            // 
            this.grcThongKeKhoCu.DataSource = this.bangThongKeKhoCuBindingSource;
            this.grcThongKeKhoCu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcThongKeKhoCu.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeKhoCu.Location = new System.Drawing.Point(2, 25);
            this.grcThongKeKhoCu.MainView = this.grvThongKeKhoCu;
            this.grcThongKeKhoCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeKhoCu.MenuManager = this.barManager1;
            this.grcThongKeKhoCu.Name = "grcThongKeKhoCu";
            this.grcThongKeKhoCu.Size = new System.Drawing.Size(1126, 177);
            this.grcThongKeKhoCu.TabIndex = 0;
            this.grcThongKeKhoCu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvThongKeKhoCu});
            // 
            // bangThongKeKhoCuBindingSource
            // 
            this.bangThongKeKhoCuBindingSource.DataSource = typeof(DTO.Models.BangThongKeKhoCu);
            // 
            // grvThongKeKhoCu
            // 
            this.grvThongKeKhoCu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKhoiLuongVangDauKi1,
            this.colKhoiLuongVangNhapVao1,
            this.colKhoiLuongVangCuMuaVao,
            this.colKhoiLuongVangDemDiGiaCong,
            this.colKhoiLuongVangCuoiKi1,
            this.colKiHieuHamLuongVang1,
            this.colKhoiLuongVangDoiNgangVao});
            this.grvThongKeKhoCu.GridControl = this.grcThongKeKhoCu;
            this.grvThongKeKhoCu.Name = "grvThongKeKhoCu";
            this.grvThongKeKhoCu.OptionsBehavior.Editable = false;
            this.grvThongKeKhoCu.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvThongKeKhoCu.OptionsView.ShowGroupPanel = false;
            // 
            // colKhoiLuongVangDauKi1
            // 
            this.colKhoiLuongVangDauKi1.Caption = "KLV đầu kì";
            this.colKhoiLuongVangDauKi1.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangDauKi1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangDauKi1.FieldName = "KhoiLuongVangDauKi";
            this.colKhoiLuongVangDauKi1.Name = "colKhoiLuongVangDauKi1";
            this.colKhoiLuongVangDauKi1.Visible = true;
            this.colKhoiLuongVangDauKi1.VisibleIndex = 1;
            // 
            // colKhoiLuongVangNhapVao1
            // 
            this.colKhoiLuongVangNhapVao1.Caption = "KLV nhập vào";
            this.colKhoiLuongVangNhapVao1.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangNhapVao1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangNhapVao1.FieldName = "KhoiLuongVangNhapVao";
            this.colKhoiLuongVangNhapVao1.Name = "colKhoiLuongVangNhapVao1";
            this.colKhoiLuongVangNhapVao1.Visible = true;
            this.colKhoiLuongVangNhapVao1.VisibleIndex = 2;
            // 
            // colKhoiLuongVangCuMuaVao
            // 
            this.colKhoiLuongVangCuMuaVao.Caption = "KLV cũ mua vào";
            this.colKhoiLuongVangCuMuaVao.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangCuMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangCuMuaVao.FieldName = "KhoiLuongVangCuMuaVao";
            this.colKhoiLuongVangCuMuaVao.Name = "colKhoiLuongVangCuMuaVao";
            this.colKhoiLuongVangCuMuaVao.Visible = true;
            this.colKhoiLuongVangCuMuaVao.VisibleIndex = 3;
            // 
            // colKhoiLuongVangDemDiGiaCong
            // 
            this.colKhoiLuongVangDemDiGiaCong.Caption = "KLV gia công";
            this.colKhoiLuongVangDemDiGiaCong.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangDemDiGiaCong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangDemDiGiaCong.FieldName = "KhoiLuongVangDemDiGiaCong";
            this.colKhoiLuongVangDemDiGiaCong.Name = "colKhoiLuongVangDemDiGiaCong";
            this.colKhoiLuongVangDemDiGiaCong.Visible = true;
            this.colKhoiLuongVangDemDiGiaCong.VisibleIndex = 4;
            // 
            // colKhoiLuongVangCuoiKi1
            // 
            this.colKhoiLuongVangCuoiKi1.Caption = "KLV cuối kì";
            this.colKhoiLuongVangCuoiKi1.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangCuoiKi1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangCuoiKi1.FieldName = "KhoiLuongVangCuoiKi";
            this.colKhoiLuongVangCuoiKi1.Name = "colKhoiLuongVangCuoiKi1";
            this.colKhoiLuongVangCuoiKi1.Visible = true;
            this.colKhoiLuongVangCuoiKi1.VisibleIndex = 6;
            // 
            // colKiHieuHamLuongVang1
            // 
            this.colKiHieuHamLuongVang1.Caption = "HLV";
            this.colKiHieuHamLuongVang1.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang1.Name = "colKiHieuHamLuongVang1";
            this.colKiHieuHamLuongVang1.Visible = true;
            this.colKiHieuHamLuongVang1.VisibleIndex = 0;
            // 
            // colKhoiLuongVangDoiNgangVao
            // 
            this.colKhoiLuongVangDoiNgangVao.Caption = "KLV đổi ngang";
            this.colKhoiLuongVangDoiNgangVao.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangDoiNgangVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangDoiNgangVao.FieldName = "KhoiLuongVangDoiNgangVao";
            this.colKhoiLuongVangDoiNgangVao.Name = "colKhoiLuongVangDoiNgangVao";
            this.colKhoiLuongVangDoiNgangVao.Visible = true;
            this.colKhoiLuongVangDoiNgangVao.VisibleIndex = 5;
            // 
            // frmThongKeHamLuongVang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 678);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmThongKeHamLuongVang";
            this.Text = "Thống kê giao dịch khối lượng vàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmThongKeHamLuongVang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeKhoMoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeTheoHamLuongVangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeKhoMoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeTheoHamLuongVangTempBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeKhoCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeKhoCuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeKhoCu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcThongKeKhoMoi;
        private DevExpress.XtraGrid.Views.Grid.GridView grvThongKeKhoMoi;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnThongKe;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private System.Windows.Forms.BindingSource thongKeTheoHamLuongVangTempBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDauKi;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangMoiBanRa;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCuBanRa;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCuoiKi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource bangThongKeTheoHamLuongVangBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangNhapVao;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl grcThongKeKhoCu;
        private DevExpress.XtraGrid.Views.Grid.GridView grvThongKeKhoCu;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private System.Windows.Forms.BindingSource bangThongKeKhoCuBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDauKi1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangNhapVao1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCuMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDemDiGiaCong;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCuoiKi1;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang1;
        private DevExpress.XtraBars.BarButtonItem btnInThongKe;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDoiNgangVao;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangDoiNgangRa;
        private DevExpress.XtraEditors.DateEdit detDenNgay;
        private DevExpress.XtraEditors.DateEdit detTuNgay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lueThoiGianThongKe;
    }
}