﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmThongKeTienMat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnInThongKe = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.detDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.detTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.lueThoiGianThongKe = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcThongKeTienMat = new DevExpress.XtraGrid.GridControl();
            this.thongKeTienMatTempBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvThongKeTienMat = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSoThuTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoiDungThongKe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDauKi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCuoiKi = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeTienMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeTienMatTempBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeTienMat)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnInThongKe});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 10;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInThongKe, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInThongKe
            // 
            this.btnInThongKe.Caption = "In thống kê";
            this.btnInThongKe.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInThongKe.Id = 9;
            this.btnInThongKe.Name = "btnInThongKe";
            this.btnInThongKe.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInThongKe_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1058, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 583);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1058, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 533);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1058, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 533);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1058, 114);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tiêu chí thống kê";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnThongKe);
            this.layoutControl1.Controls.Add(this.detDenNgay);
            this.layoutControl1.Controls.Add(this.detTuNgay);
            this.layoutControl1.Controls.Add(this.lueThoiGianThongKe);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1054, 87);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnThongKe
            // 
            this.btnThongKe.Location = new System.Drawing.Point(16, 44);
            this.btnThongKe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(1022, 27);
            this.btnThongKe.StyleController = this.layoutControl1;
            this.btnThongKe.TabIndex = 7;
            this.btnThongKe.Text = "Thống kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // detDenNgay
            // 
            this.detDenNgay.EditValue = null;
            this.detDenNgay.Location = new System.Drawing.Point(803, 16);
            this.detDenNgay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detDenNgay.MenuManager = this.barManager1;
            this.detDenNgay.Name = "detDenNgay";
            this.detDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detDenNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detDenNgay.Properties.ReadOnly = true;
            this.detDenNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detDenNgay.Size = new System.Drawing.Size(235, 22);
            this.detDenNgay.StyleController = this.layoutControl1;
            this.detDenNgay.TabIndex = 6;
            // 
            // detTuNgay
            // 
            this.detTuNgay.EditValue = null;
            this.detTuNgay.Location = new System.Drawing.Point(484, 16);
            this.detTuNgay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.detTuNgay.MenuManager = this.barManager1;
            this.detTuNgay.Name = "detTuNgay";
            this.detTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detTuNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detTuNgay.Properties.ReadOnly = true;
            this.detTuNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detTuNgay.Size = new System.Drawing.Size(218, 22);
            this.detTuNgay.StyleController = this.layoutControl1;
            this.detTuNgay.TabIndex = 5;
            // 
            // lueThoiGianThongKe
            // 
            this.lueThoiGianThongKe.Location = new System.Drawing.Point(111, 16);
            this.lueThoiGianThongKe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueThoiGianThongKe.MenuManager = this.barManager1;
            this.lueThoiGianThongKe.Name = "lueThoiGianThongKe";
            this.lueThoiGianThongKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueThoiGianThongKe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Thống kê theo")});
            this.lueThoiGianThongKe.Properties.NullText = "Hôm nay";
            this.lueThoiGianThongKe.Size = new System.Drawing.Size(272, 22);
            this.lueThoiGianThongKe.StyleController = this.layoutControl1;
            this.lueThoiGianThongKe.TabIndex = 4;
            this.lueThoiGianThongKe.EditValueChanged += new System.EventHandler(this.lueThoiGianThongKe_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1054, 87);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueThoiGianThongKe;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(373, 28);
            this.layoutControlItem1.Text = "Ngày thống kê";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.detTuNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(373, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(319, 28);
            this.layoutControlItem2.Text = "Từ ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.detDenNgay;
            this.layoutControlItem3.Location = new System.Drawing.Point(692, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(336, 28);
            this.layoutControlItem3.Text = "Đến ngày";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnThongKe;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1028, 33);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcThongKeTienMat);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 164);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1058, 419);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Kết quả thống kê";
            // 
            // grcThongKeTienMat
            // 
            this.grcThongKeTienMat.DataSource = this.thongKeTienMatTempBindingSource;
            this.grcThongKeTienMat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcThongKeTienMat.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeTienMat.Location = new System.Drawing.Point(2, 25);
            this.grcThongKeTienMat.MainView = this.grvThongKeTienMat;
            this.grcThongKeTienMat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcThongKeTienMat.MenuManager = this.barManager1;
            this.grcThongKeTienMat.Name = "grcThongKeTienMat";
            this.grcThongKeTienMat.Size = new System.Drawing.Size(1054, 392);
            this.grcThongKeTienMat.TabIndex = 0;
            this.grcThongKeTienMat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvThongKeTienMat});
            // 
            // thongKeTienMatTempBindingSource
            // 
            this.thongKeTienMatTempBindingSource.DataSource = typeof(DTO.TemporaryModel.ThongKeTienMatTemp);
            // 
            // grvThongKeTienMat
            // 
            this.grvThongKeTienMat.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSoThuTu,
            this.colNoiDungThongKe,
            this.colDauKi,
            this.colThu,
            this.colChi,
            this.colCuoiKi});
            this.grvThongKeTienMat.GridControl = this.grcThongKeTienMat;
            this.grvThongKeTienMat.Name = "grvThongKeTienMat";
            this.grvThongKeTienMat.OptionsBehavior.Editable = false;
            this.grvThongKeTienMat.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvThongKeTienMat.OptionsView.ShowGroupPanel = false;
            // 
            // colSoThuTu
            // 
            this.colSoThuTu.Caption = "STT";
            this.colSoThuTu.FieldName = "SoThuTu";
            this.colSoThuTu.Name = "colSoThuTu";
            this.colSoThuTu.Visible = true;
            this.colSoThuTu.VisibleIndex = 0;
            this.colSoThuTu.Width = 54;
            // 
            // colNoiDungThongKe
            // 
            this.colNoiDungThongKe.Caption = "Nội dung thống kê";
            this.colNoiDungThongKe.FieldName = "NoiDungThongKe";
            this.colNoiDungThongKe.Name = "colNoiDungThongKe";
            this.colNoiDungThongKe.Visible = true;
            this.colNoiDungThongKe.VisibleIndex = 1;
            this.colNoiDungThongKe.Width = 304;
            // 
            // colDauKi
            // 
            this.colDauKi.Caption = "Đầu kì";
            this.colDauKi.DisplayFormat.FormatString = "n0";
            this.colDauKi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDauKi.FieldName = "DauKi";
            this.colDauKi.Name = "colDauKi";
            this.colDauKi.Visible = true;
            this.colDauKi.VisibleIndex = 2;
            this.colDauKi.Width = 167;
            // 
            // colThu
            // 
            this.colThu.Caption = "Thu";
            this.colThu.DisplayFormat.FormatString = "n0";
            this.colThu.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colThu.FieldName = "Thu";
            this.colThu.Name = "colThu";
            this.colThu.Visible = true;
            this.colThu.VisibleIndex = 3;
            this.colThu.Width = 167;
            // 
            // colChi
            // 
            this.colChi.Caption = "Chi";
            this.colChi.DisplayFormat.FormatString = "n0";
            this.colChi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colChi.FieldName = "Chi";
            this.colChi.Name = "colChi";
            this.colChi.Visible = true;
            this.colChi.VisibleIndex = 4;
            this.colChi.Width = 167;
            // 
            // colCuoiKi
            // 
            this.colCuoiKi.Caption = "Cuối kì";
            this.colCuoiKi.DisplayFormat.FormatString = "n0";
            this.colCuoiKi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuoiKi.FieldName = "CuoiKi";
            this.colCuoiKi.Name = "colCuoiKi";
            this.colCuoiKi.Visible = true;
            this.colCuoiKi.VisibleIndex = 5;
            this.colCuoiKi.Width = 175;
            // 
            // frmThongKeTienMat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 583);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmThongKeTienMat";
            this.Text = "Thống kê tiền mặt";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmThongKeTienMat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcThongKeTienMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeTienMatTempBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvThongKeTienMat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnInThongKe;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcThongKeTienMat;
        private DevExpress.XtraGrid.Views.Grid.GridView grvThongKeTienMat;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnThongKe;
        private DevExpress.XtraEditors.DateEdit detDenNgay;
        private DevExpress.XtraEditors.DateEdit detTuNgay;
        private DevExpress.XtraEditors.LookUpEdit lueThoiGianThongKe;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource thongKeTienMatTempBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSoThuTu;
        private DevExpress.XtraGrid.Columns.GridColumn colNoiDungThongKe;
        private DevExpress.XtraGrid.Columns.GridColumn colDauKi;
        private DevExpress.XtraGrid.Columns.GridColumn colThu;
        private DevExpress.XtraGrid.Columns.GridColumn colChi;
        private DevExpress.XtraGrid.Columns.GridColumn colCuoiKi;
    }
}