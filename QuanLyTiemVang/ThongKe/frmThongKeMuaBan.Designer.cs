﻿namespace QuanLyTiemVang.ThongKe
{
    partial class frmThongKeMuaBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnInThongKe = new DevExpress.XtraBars.BarButtonItem();
            this.btnXuatExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnThongKe = new DevExpress.XtraEditors.SimpleButton();
            this.detDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.detTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.lueThoiGianThongKe = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grcVangBanRa = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeTheoHamLuongVangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvVangBanRa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTongKhoiLuongVangBanRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienVangBanRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.grcVangMuaVao = new DevExpress.XtraGrid.GridControl();
            this.bangThongKeKhoCuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvVangMuaVao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKhoiLuongVangCuMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongTienVangMuaVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.grcVangDoiNgang = new DevExpress.XtraGrid.GridControl();
            this.thongKeVangDoiNgangTempBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvVangDoiNgang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongKhoiLuongVangXuatRa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTongKhoiLuongVangThuVao = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeTheoHamLuongVangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangBanRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangMuaVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeKhoCuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangMuaVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcVangDoiNgang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeVangDoiNgangTempBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangDoiNgang)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai,
            this.btnLamMoiDuLieu,
            this.barButtonItem14,
            this.btnInThongKe,
            this.btnXuatExcel});
            this.barManager1.MainMenu = this.bar1;
            this.barManager1.MaxItemId = 11;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(246, 156);
            this.bar1.FloatSize = new System.Drawing.Size(198, 82);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInThongKe, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXuatExcel, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập Lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnInThongKe
            // 
            this.btnInThongKe.Caption = "In thống kê";
            this.btnInThongKe.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInThongKe.Id = 9;
            this.btnInThongKe.Name = "btnInThongKe";
            this.btnInThongKe.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInThongKe_ItemClick);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Caption = "Xuất excel";
            this.btnXuatExcel.Glyph = global::QuanLyTiemVang.Properties.Resources.netvibes_icon;
            this.btnXuatExcel.Id = 10;
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatExcel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1143, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 681);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 631);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1143, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 631);
            // 
            // btnThem
            // 
            this.btnThem.Id = 5;
            this.btnThem.Name = "btnThem";
            // 
            // btnSua
            // 
            this.btnSua.Id = 6;
            this.btnSua.Name = "btnSua";
            // 
            // btnXoa
            // 
            this.btnXoa.Id = 7;
            this.btnXoa.Name = "btnXoa";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Nhập file excel";
            this.barButtonItem14.Id = 8;
            this.barButtonItem14.LargeGlyph = global::QuanLyTiemVang.Properties.Resources.Mimetypes_application_vnd_ms_excel_icon;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1143, 87);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tiêu chí thống kê";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnThongKe);
            this.layoutControl1.Controls.Add(this.detDenNgay);
            this.layoutControl1.Controls.Add(this.detTuNgay);
            this.layoutControl1.Controls.Add(this.lueThoiGianThongKe);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1139, 60);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnThongKe
            // 
            this.btnThongKe.Location = new System.Drawing.Point(898, 16);
            this.btnThongKe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(225, 27);
            this.btnThongKe.StyleController = this.layoutControl1;
            this.btnThongKe.TabIndex = 8;
            this.btnThongKe.Text = "Thống kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // detDenNgay
            // 
            this.detDenNgay.EditValue = null;
            this.detDenNgay.Location = new System.Drawing.Point(721, 16);
            this.detDenNgay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detDenNgay.MenuManager = this.barManager1;
            this.detDenNgay.Name = "detDenNgay";
            this.detDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detDenNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detDenNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detDenNgay.Properties.ReadOnly = true;
            this.detDenNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detDenNgay.Size = new System.Drawing.Size(171, 22);
            this.detDenNgay.StyleController = this.layoutControl1;
            this.detDenNgay.TabIndex = 6;
            // 
            // detTuNgay
            // 
            this.detTuNgay.EditValue = null;
            this.detTuNgay.Location = new System.Drawing.Point(427, 16);
            this.detTuNgay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.detTuNgay.MenuManager = this.barManager1;
            this.detTuNgay.Name = "detTuNgay";
            this.detTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detTuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detTuNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detTuNgay.Properties.ReadOnly = true;
            this.detTuNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.detTuNgay.Size = new System.Drawing.Size(194, 22);
            this.detTuNgay.StyleController = this.layoutControl1;
            this.detTuNgay.TabIndex = 5;
            // 
            // lueThoiGianThongKe
            // 
            this.lueThoiGianThongKe.Location = new System.Drawing.Point(110, 16);
            this.lueThoiGianThongKe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueThoiGianThongKe.MenuManager = this.barManager1;
            this.lueThoiGianThongKe.Name = "lueThoiGianThongKe";
            this.lueThoiGianThongKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueThoiGianThongKe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Thống kê theo")});
            this.lueThoiGianThongKe.Properties.NullText = "Hôm nay";
            this.lueThoiGianThongKe.Size = new System.Drawing.Size(217, 22);
            this.lueThoiGianThongKe.StyleController = this.layoutControl1;
            this.lueThoiGianThongKe.TabIndex = 4;
            this.lueThoiGianThongKe.EditValueChanged += new System.EventHandler(this.lueThoiGianThongKe_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1139, 60);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueThoiGianThongKe;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(317, 34);
            this.layoutControlItem1.Text = "Ngày thống kê";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.detTuNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(317, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(294, 34);
            this.layoutControlItem2.Text = "Từ ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.detDenNgay;
            this.layoutControlItem3.Location = new System.Drawing.Point(611, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(271, 34);
            this.layoutControlItem3.Text = "Đến ngày";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(91, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnThongKe;
            this.layoutControlItem5.Location = new System.Drawing.Point(882, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(231, 34);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.grcVangBanRa);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 137);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1143, 181);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Vàng bán ra";
            // 
            // grcVangBanRa
            // 
            this.grcVangBanRa.DataSource = this.bangThongKeTheoHamLuongVangBindingSource;
            this.grcVangBanRa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcVangBanRa.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangBanRa.Location = new System.Drawing.Point(2, 25);
            this.grcVangBanRa.MainView = this.grvVangBanRa;
            this.grcVangBanRa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangBanRa.MenuManager = this.barManager1;
            this.grcVangBanRa.Name = "grcVangBanRa";
            this.grcVangBanRa.Size = new System.Drawing.Size(1139, 154);
            this.grcVangBanRa.TabIndex = 0;
            this.grcVangBanRa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvVangBanRa});
            // 
            // bangThongKeTheoHamLuongVangBindingSource
            // 
            this.bangThongKeTheoHamLuongVangBindingSource.DataSource = typeof(DTO.Models.BangThongKeTheoHamLuongVang);
            // 
            // grvVangBanRa
            // 
            this.grvVangBanRa.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTongKhoiLuongVangBanRa,
            this.colTongTienVangBanRa,
            this.colKiHieuHamLuongVang});
            this.grvVangBanRa.GridControl = this.grcVangBanRa;
            this.grvVangBanRa.Name = "grvVangBanRa";
            this.grvVangBanRa.OptionsBehavior.Editable = false;
            this.grvVangBanRa.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvVangBanRa.OptionsView.ShowGroupPanel = false;
            // 
            // colTongKhoiLuongVangBanRa
            // 
            this.colTongKhoiLuongVangBanRa.Caption = "Tổng trọng lượng";
            this.colTongKhoiLuongVangBanRa.DisplayFormat.FormatString = "#,0.#";
            this.colTongKhoiLuongVangBanRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongKhoiLuongVangBanRa.FieldName = "TongKhoiLuongVangBanRa";
            this.colTongKhoiLuongVangBanRa.Name = "colTongKhoiLuongVangBanRa";
            this.colTongKhoiLuongVangBanRa.Visible = true;
            this.colTongKhoiLuongVangBanRa.VisibleIndex = 1;
            // 
            // colTongTienVangBanRa
            // 
            this.colTongTienVangBanRa.Caption = "Thành tiền";
            this.colTongTienVangBanRa.DisplayFormat.FormatString = "n0";
            this.colTongTienVangBanRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienVangBanRa.FieldName = "TongTienVangBanRa";
            this.colTongTienVangBanRa.Name = "colTongTienVangBanRa";
            this.colTongTienVangBanRa.Visible = true;
            this.colTongTienVangBanRa.VisibleIndex = 2;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 0;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 318);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1143, 6);
            this.splitterControl1.TabIndex = 6;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.grcVangMuaVao);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 324);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1143, 172);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "Vàng mua vào";
            // 
            // grcVangMuaVao
            // 
            this.grcVangMuaVao.DataSource = this.bangThongKeKhoCuBindingSource;
            this.grcVangMuaVao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcVangMuaVao.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangMuaVao.Location = new System.Drawing.Point(2, 25);
            this.grcVangMuaVao.MainView = this.grvVangMuaVao;
            this.grcVangMuaVao.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangMuaVao.MenuManager = this.barManager1;
            this.grcVangMuaVao.Name = "grcVangMuaVao";
            this.grcVangMuaVao.Size = new System.Drawing.Size(1139, 145);
            this.grcVangMuaVao.TabIndex = 0;
            this.grcVangMuaVao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvVangMuaVao});
            // 
            // bangThongKeKhoCuBindingSource
            // 
            this.bangThongKeKhoCuBindingSource.DataSource = typeof(DTO.Models.BangThongKeKhoCu);
            // 
            // grvVangMuaVao
            // 
            this.grvVangMuaVao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKhoiLuongVangCuMuaVao,
            this.colTongTienVangMuaVao,
            this.colKiHieuHamLuongVang1});
            this.grvVangMuaVao.GridControl = this.grcVangMuaVao;
            this.grvVangMuaVao.Name = "grvVangMuaVao";
            this.grvVangMuaVao.OptionsBehavior.Editable = false;
            this.grvVangMuaVao.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvVangMuaVao.OptionsView.ShowGroupPanel = false;
            // 
            // colKhoiLuongVangCuMuaVao
            // 
            this.colKhoiLuongVangCuMuaVao.Caption = "Tổng trọng lượng";
            this.colKhoiLuongVangCuMuaVao.DisplayFormat.FormatString = "#,0.#";
            this.colKhoiLuongVangCuMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKhoiLuongVangCuMuaVao.FieldName = "KhoiLuongVangCuMuaVao";
            this.colKhoiLuongVangCuMuaVao.Name = "colKhoiLuongVangCuMuaVao";
            this.colKhoiLuongVangCuMuaVao.Visible = true;
            this.colKhoiLuongVangCuMuaVao.VisibleIndex = 1;
            // 
            // colTongTienVangMuaVao
            // 
            this.colTongTienVangMuaVao.Caption = "Thành tiền";
            this.colTongTienVangMuaVao.DisplayFormat.FormatString = "n0";
            this.colTongTienVangMuaVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongTienVangMuaVao.FieldName = "TongTienVangMuaVao";
            this.colTongTienVangMuaVao.Name = "colTongTienVangMuaVao";
            this.colTongTienVangMuaVao.Visible = true;
            this.colTongTienVangMuaVao.VisibleIndex = 2;
            // 
            // colKiHieuHamLuongVang1
            // 
            this.colKiHieuHamLuongVang1.Caption = "Hàm lượng vàng";
            this.colKiHieuHamLuongVang1.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang1.Name = "colKiHieuHamLuongVang1";
            this.colKiHieuHamLuongVang1.Visible = true;
            this.colKiHieuHamLuongVang1.VisibleIndex = 0;
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl2.Location = new System.Drawing.Point(0, 496);
            this.splitterControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(1143, 6);
            this.splitterControl2.TabIndex = 8;
            this.splitterControl2.TabStop = false;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.grcVangDoiNgang);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(0, 502);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1143, 179);
            this.groupControl4.TabIndex = 9;
            this.groupControl4.Text = "Vàng đổi ngang";
            // 
            // grcVangDoiNgang
            // 
            this.grcVangDoiNgang.DataSource = this.thongKeVangDoiNgangTempBindingSource;
            this.grcVangDoiNgang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcVangDoiNgang.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangDoiNgang.Location = new System.Drawing.Point(2, 25);
            this.grcVangDoiNgang.MainView = this.grvVangDoiNgang;
            this.grcVangDoiNgang.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grcVangDoiNgang.MenuManager = this.barManager1;
            this.grcVangDoiNgang.Name = "grcVangDoiNgang";
            this.grcVangDoiNgang.Size = new System.Drawing.Size(1139, 152);
            this.grcVangDoiNgang.TabIndex = 0;
            this.grcVangDoiNgang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvVangDoiNgang});
            // 
            // thongKeVangDoiNgangTempBindingSource
            // 
            this.thongKeVangDoiNgangTempBindingSource.DataSource = typeof(DTO.TemporaryModel.ThongKeVangDoiNgangTemp);
            // 
            // grvVangDoiNgang
            // 
            this.grvVangDoiNgang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHamLuongVang,
            this.colTongKhoiLuongVangXuatRa,
            this.colTongKhoiLuongVangThuVao});
            this.grvVangDoiNgang.GridControl = this.grcVangDoiNgang;
            this.grvVangDoiNgang.Name = "grvVangDoiNgang";
            this.grvVangDoiNgang.OptionsBehavior.Editable = false;
            this.grvVangDoiNgang.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvVangDoiNgang.OptionsView.ShowGroupPanel = false;
            // 
            // colHamLuongVang
            // 
            this.colHamLuongVang.Caption = "Hàm lượng vàng";
            this.colHamLuongVang.FieldName = "HamLuongVang";
            this.colHamLuongVang.Name = "colHamLuongVang";
            this.colHamLuongVang.Visible = true;
            this.colHamLuongVang.VisibleIndex = 0;
            // 
            // colTongKhoiLuongVangXuatRa
            // 
            this.colTongKhoiLuongVangXuatRa.Caption = "Tổng KLV xuất ra";
            this.colTongKhoiLuongVangXuatRa.DisplayFormat.FormatString = "#,0.#";
            this.colTongKhoiLuongVangXuatRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongKhoiLuongVangXuatRa.FieldName = "TongKhoiLuongVangXuatRa";
            this.colTongKhoiLuongVangXuatRa.Name = "colTongKhoiLuongVangXuatRa";
            this.colTongKhoiLuongVangXuatRa.Visible = true;
            this.colTongKhoiLuongVangXuatRa.VisibleIndex = 1;
            // 
            // colTongKhoiLuongVangThuVao
            // 
            this.colTongKhoiLuongVangThuVao.Caption = "Tổng KLV thu vào";
            this.colTongKhoiLuongVangThuVao.DisplayFormat.FormatString = "#,0.#";
            this.colTongKhoiLuongVangThuVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTongKhoiLuongVangThuVao.FieldName = "TongKhoiLuongVangThuVao";
            this.colTongKhoiLuongVangThuVao.Name = "colTongKhoiLuongVangThuVao";
            this.colTongKhoiLuongVangThuVao.Visible = true;
            this.colTongKhoiLuongVangThuVao.VisibleIndex = 2;
            // 
            // frmThongKeMuaBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 681);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.splitterControl2);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmThongKeMuaBan";
            this.Text = "Thống kê mua bán";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmThongKeTongHop_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueThoiGianThongKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcVangBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeTheoHamLuongVangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangBanRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcVangMuaVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bangThongKeKhoCuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangMuaVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcVangDoiNgang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thongKeVangDoiNgangTempBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvVangDoiNgang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnInThongKe;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl grcVangDoiNgang;
        private DevExpress.XtraGrid.Views.Grid.GridView grvVangDoiNgang;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl grcVangMuaVao;
        private DevExpress.XtraGrid.Views.Grid.GridView grvVangMuaVao;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grcVangBanRa;
        private DevExpress.XtraGrid.Views.Grid.GridView grvVangBanRa;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit detDenNgay;
        private DevExpress.XtraEditors.DateEdit detTuNgay;
        private DevExpress.XtraEditors.LookUpEdit lueThoiGianThongKe;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton btnThongKe;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.BindingSource bangThongKeTheoHamLuongVangBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTongKhoiLuongVangBanRa;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienVangBanRa;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private System.Windows.Forms.BindingSource bangThongKeKhoCuBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoiLuongVangCuMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colTongTienVangMuaVao;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang1;
        private System.Windows.Forms.BindingSource thongKeVangDoiNgangTempBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colHamLuongVang;
        private DevExpress.XtraGrid.Columns.GridColumn colTongKhoiLuongVangXuatRa;
        private DevExpress.XtraGrid.Columns.GridColumn colTongKhoiLuongVangThuVao;
        private DevExpress.XtraBars.BarButtonItem btnXuatExcel;
    }
}