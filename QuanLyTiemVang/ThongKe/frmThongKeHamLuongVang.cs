﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.TemporaryModel;
using BUS;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;
using Share.Constant;
using DTO.Models;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeHamLuongVang : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeHamLuongVang()
        {
            InitializeComponent();
        }

        private DateTime thongKeTuNgay = DateTime.Today;
        private DateTime thongKeDenNgay = DateTime.Today;

        private void frmThongKeHamLuongVang_Load(object sender, EventArgs e)
        {
            SetDataSourceForThoiGian();
            detTuNgay.Properties.MaxValue = DateTime.Today;
            detDenNgay.Properties.MaxValue = DateTime.Today;
        }

        private void SetDataSourceForThoiGian()
        {
            List<LookUpEditItem> listThoiGianThongKe = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_HOM_NAY
                },
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_KHOAN_THOI_GIAN
                }
            };

            lueThoiGianThongKe.Properties.DataSource = listThoiGianThongKe;
            lueThoiGianThongKe.Properties.DisplayMember = "DisplayMember";
            lueThoiGianThongKe.Properties.ValueMember = "ValueMember";
        }

        private bool IsHaveNgayDauKi()
        {
            if (NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast() == null)
            {
                PopupService.Instance.Warning(MessageWarning.NGAY_DAU_KI_NULL);

                return false;
            }

            return true;
        }

        private void GetThongKeByDate(DateTime ngayThongKe)
        {
            grcThongKeKhoMoi.DataSource = BangThongKeTheoHamLuongVangBUS.Instance.GetBangThongKeByDate(ngayThongKe);
            grcThongKeKhoCu.DataSource = BangThongKeKhoCuBUS.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        private void GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            grcThongKeKhoMoi.DataSource = BangThongKeTheoHamLuongVangBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
            grcThongKeKhoCu.DataSource = BangThongKeKhoCuBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (!IsHaveNgayDauKi())
            {
                return;
            }

            if (lueThoiGianThongKe.EditValue == null || TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY ==  Convert.ToInt32(lueThoiGianThongKe.EditValue))
            {
                GetThongKeByDate(DateTime.Today);
                thongKeTuNgay = DateTime.Today;
                thongKeDenNgay = DateTime.Today;
            }
            else if (detTuNgay.DateTime.Date == detDenNgay.DateTime.Date)
            {
                GetThongKeByDate(detTuNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
            else
            {
                GetThongKeByTwoDifferenceDate(detTuNgay.DateTime, detDenNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!BangThongKeTheoHamLuongVangBUS.Instance.TinhToanThongKe() || !BangThongKeKhoCuBUS.Instance.TinhToanThongKe())
            {
                PopupService.Instance.Error(MessageError.TINH_TOAN_THONG_KE);
            }
            else
            {
                PopupService.Instance.Success(MessageSucces.TINH_TOAN_THONG_KE);
            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcThongKeKhoCu.DataSource = null;
            grcThongKeKhoMoi.DataSource = null;
            lueThoiGianThongKe.EditValue = null;
            detDenNgay.ReadOnly = true;
            detTuNgay.ReadOnly = true;
        }

        private void btnInThongKe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<BangThongKeTheoHamLuongVang> listThongKeKhoMoi = (List<BangThongKeTheoHamLuongVang>)grcThongKeKhoMoi.DataSource;
            List<BangThongKeKhoCu> listThongKeKhoCu = (List<BangThongKeKhoCu>)grcThongKeKhoCu.DataSource;

            if (listThongKeKhoMoi != null)
            {
                ThongKeKhoMoiPrint thongKeKhoMoiPrint = new ThongKeKhoMoiPrint(thongKeTuNgay, thongKeDenNgay);

                thongKeKhoMoiPrint.DataSource = listThongKeKhoMoi;
                thongKeKhoMoiPrint.ShowPreviewDialog();
            }

            if (listThongKeKhoCu != null)
            {
                ThongKeKhoCuPrint thongKeKhoCuPrint = new ThongKeKhoCuPrint(thongKeTuNgay, thongKeDenNgay);

                thongKeKhoCuPrint.DataSource = listThongKeKhoCu;
                thongKeKhoCuPrint.ShowPreviewDialog();
            }
        }

        private void lueThoiGianThongKe_EditValueChanged(object sender, EventArgs e)
        {
            if (lueThoiGianThongKe.EditValue == null)
            {
                return;
            }

            if ((int)lueThoiGianThongKe.EditValue == TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN)
            {
                detTuNgay.ReadOnly = false;
                detDenNgay.ReadOnly = false;
            }
            else
            {
                detTuNgay.ReadOnly = true;
                detDenNgay.ReadOnly = true;
            }
        }
    }
}