﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using BUS;
using DTO.Models;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmNhapNgayDauKi : DevExpress.XtraEditors.XtraForm
    {
        public frmNhapNgayDauKi()
        {
            InitializeComponent();
        }

        private void frmNhapNgayDauKi_Load(object sender, EventArgs e)
        {
            detNgayDauKi.Properties.MaxValue = DateTime.Today;
            txtTienDauKi.EditValue = 0;
            GetNgayDauKi();
        }

        private void GetNgayDauKi()
        {
            NgayDauKiThongKe ngayDauKi = NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast();

            if (ngayDauKi == null)
            {
                return;
            }

            detNgayDauKi.DateTime = NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast().NgayDauKi;
            txtTienDauKi.EditValue = ngayDauKi.SoTienDauKi;
        }

        private void btnNhapNgaydauKi_Click(object sender, EventArgs e)
        {
            if (PopupService.Instance.Question(MessageWarning.CHANGE_NGAY_DAU_KI) == DialogResult.No)
            {
                return;
            }

            NgayDauKiThongKe ngayDauKiInsert = NgayDauKiThongKeBUS.Instance.Insert(detNgayDauKi, txtTienDauKi);

            if (ngayDauKiInsert != null)
            {
                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
                this.Hide();
            }
            else
            {
                PopupService.Instance.Error(MessageError.INSERT_ERROR);
            }
        }
    }
}