﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.TemporaryModel;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeKhoMoi : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeKhoMoi()
        {
            InitializeComponent();
        }

        private void frmThongKeKhoMoi_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueHamLuongVang);
        }

        private void ClearFormThongKe()
        {
            lueHamLuongVang.EditValue = null;
        }

        private void ClearFormChiTiet()
        {
            txtTongSoLuong.Text = null;
            txtTongTienCong.Text = null;
            txtTongKLVang.Text = null;
            txtTongKLTinh.Text = null;
            txtTongKLDa.Text = null;
        }

        private void HienThiChiTietThongKe(ThongKeKhoMoiTemp thongKeKhoMoi)
        {
            txtTongSoLuong.EditValue = thongKeKhoMoi.TongSoLuongSanPham;
            txtTongTienCong.EditValue = thongKeKhoMoi.TongTienCong;
            txtTongKLVang.EditValue = thongKeKhoMoi.TongKhoiLuongVang;
            txtTongKLTinh.EditValue = thongKeKhoMoi.TongKhoiLuongTinh;
            txtTongKLDa.EditValue = thongKeKhoMoi.TongKhoiLuongDa;
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (lueHamLuongVang.EditValue != null)
            {
                HienThiChiTietThongKe(ThongKeKhoMoiBUS.Instance.GetThongKeKhoMoiData(lueHamLuongVang.Text));
            }
            else
            {
                HienThiChiTietThongKe(ThongKeKhoMoiBUS.Instance.GetThongKeKhoMoiData());
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearFormChiTiet();
            ClearFormThongKe();
        }

        private void btnInThongKe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int outInt;
            double outDouble;

            ThongKeKhoMoiTemp thongKeKhoMoiTemp = new ThongKeKhoMoiTemp
            {
                TongSoLuongSanPham = int.TryParse(txtTongSoLuong.EditValue.ToString(), out outInt) ? outInt : 0,
                TongKhoiLuongTinh = double.TryParse(txtTongKLTinh.EditValue.ToString(), out outDouble) ? outDouble : 0,
                TongKhoiLuongDa = double.TryParse(txtTongKLDa.EditValue.ToString(), out outDouble) ? outDouble : 0,
                TongKhoiLuongVang = double.TryParse(txtTongKLVang.EditValue.ToString(), out outDouble) ? outDouble : 0,
                TongTienCong = int.TryParse(txtTongTienCong.EditValue.ToString(), out outInt) ? outInt : 0
            };

            ThongKeSanPhamKhoMoiPrint thongKePrint = new ThongKeSanPhamKhoMoiPrint();

            thongKePrint.DataSource = new List<ThongKeKhoMoiTemp> { thongKeKhoMoiTemp };
            thongKePrint.ShowPreviewDialog();
        }
    }
}