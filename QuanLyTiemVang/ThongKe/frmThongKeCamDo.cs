﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.TemporaryModel;
using Share.Constant;
using BUS;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using DTO.Models;
using QuanLyTiemVang.BluePrints;
using DevExpress.XtraReports.UI;
using QuanLyDieuXe.DialogCustom;

namespace QuanLyTiemVang.ThongKe
{
    public partial class frmThongKeCamDo : DevExpress.XtraEditors.XtraForm
    {
        public frmThongKeCamDo()
        {
            InitializeComponent();
        }

        private DateTime? thongKeTuNgay;
        private DateTime? thongKeDenNgay;

        private void frmThongKeCamDo_Load(object sender, EventArgs e)
        {
            SetDataSourceForThoiGian();
            detTuNgay.Properties.MaxValue = DateTime.Today;
            detDenNgay.Properties.MaxValue = DateTime.Today;
        }

        private void SetDataSourceForThoiGian()
        {
            List<LookUpEditItem> listThoiGianThongKe = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_HOM_NAY
                },
                new LookUpEditItem {
                    ValueMember = TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN,
                    DisplayMember = TieuChiThongKeConstant.DISPLAY_THOI_GIAN_KHOAN_THOI_GIAN
                }
            };

            lueThoiGianThongKe.Properties.DataSource = listThoiGianThongKe;
            lueThoiGianThongKe.Properties.DisplayMember = "DisplayMember";
            lueThoiGianThongKe.Properties.ValueMember = "ValueMember";
        }

        private bool IsHaveNgayDauKi()
        {
            if (NgayDauKiThongKeBUS.Instance.GetNgayDauKiLast() == null)
            {
                PopupService.Instance.Warning(MessageWarning.NGAY_DAU_KI_NULL);

                return false;
            }

            return true;
        }

        private void GetThongKeByDate(DateTime ngayThongKe)
        {
            List<BangThongKeCamDo> listThongKeCamDo = BangThongKeCamDoBUS.Instance.GetBangThongKeByDate(ngayThongKe);

            LoadData(listThongKeCamDo);
        }

        private void GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            List<BangThongKeCamDo> listThongKeCamDo = BangThongKeCamDoBUS.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);

            LoadData(listThongKeCamDo);
        }

        private void LoadData(List<BangThongKeCamDo> listThongKeCamDo)
        {
            if (listThongKeCamDo == null || listThongKeCamDo.Count == 0)
            {
                return;
            }

            BangThongKeCamDo bangThongKeTong = new BangThongKeCamDo
            {
                KiHieuHamLuongVang = new KiHieuHamLuongVang
                {
                    MaHamLuongVang = 0,
                    HamLuongVang = "TỔNG CỘNG"
                },
                TongTienCamDo = listThongKeCamDo.First().TongTienCamDo,
                TongTienThanhToan = listThongKeCamDo.First().TongTienThanhToan,
                TongTienLaiThanhToan = listThongKeCamDo.First().TongTienLaiThanhToan,
                TongTienThanhLy = listThongKeCamDo.First().TongTienThanhLy,
                TongTienLaiThanhLy = listThongKeCamDo.First().TongTienLaiThanhLy,
                TongTienCamGiaHan = listThongKeCamDo.First().TongTienCamGiaHan,
                TongTienLaiGiaHan = listThongKeCamDo.First().TongTienLaiGiaHan
            };

            foreach (BangThongKeCamDo bangThongKe in listThongKeCamDo)
            {
                bangThongKe.TongTienCamDo = 0;
                bangThongKe.TongTienThanhToan = 0;
                bangThongKe.TongTienLaiThanhToan = 0;
                bangThongKe.TongTienThanhLy = 0;
                bangThongKe.TongTienLaiThanhLy = 0;
                bangThongKe.TongTienCamGiaHan = 0;
                bangThongKe.TongTienLaiGiaHan = 0;
            }

            listThongKeCamDo.Add(bangThongKeTong);

            grcGiaHanHopDong.DataSource = listThongKeCamDo;
            grcTongTienVangKhachCam.DataSource = listThongKeCamDo;
            grcTongTienVangKhachChuoc.DataSource = listThongKeCamDo;
            grcTongTienVangThanhLy.DataSource = listThongKeCamDo;
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcGiaHanHopDong.DataSource = null;
            grcTongTienVangKhachCam.DataSource = null;
            grcTongTienVangKhachChuoc.DataSource = null;
            grcTongTienVangThanhLy.DataSource = null;
            lueThoiGianThongKe.EditValue = null;
            detDenNgay.EditValue = null;
            detTuNgay.EditValue = null;
            detTuNgay.ReadOnly = true;
            detDenNgay.ReadOnly = true;
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!BangThongKeCamDoBUS.Instance.TinhToanThongKe())
            {
                PopupService.Instance.Error(MessageError.TINH_TOAN_THONG_KE);
            }
            else
            {
                PopupService.Instance.Success(MessageSucces.TINH_TOAN_THONG_KE);
            }
        }

        private void btnInThongKe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<BangThongKeCamDo> listThongKeCamDo = (List<BangThongKeCamDo>)grcTongTienVangKhachCam.DataSource;
            List<BangInThongKeCamDoTongHopTemp> listBangInKhachCam = new List<BangInThongKeCamDoTongHopTemp>();
            List<BangInThongKeCamDoTongHopTemp> listBangInKhachChuoc = new List<BangInThongKeCamDoTongHopTemp>();
            List<BangInThongKeCamDoTongHopTemp> listBangInThanhLy = new List<BangInThongKeCamDoTongHopTemp>();
            List<BangInThongKeCamDoTongHopTemp> listBangInGiaHan = new List<BangInThongKeCamDoTongHopTemp>();

            if (listThongKeCamDo != null)
            {
                foreach (BangThongKeCamDo bangThongKe in listThongKeCamDo)
                {
                    if (listThongKeCamDo.IndexOf(bangThongKe) == listThongKeCamDo.Count - 1)
                    {
                        listBangInKhachCam.Add(new BangInThongKeCamDoTongHopTemp
                        {
                            TieuChiThongKeTongTien = "TỔNG TIỀN CẦM",
                            ThongKeTuNgay = thongKeTuNgay,
                            ThongKeDenNgay = thongKeDenNgay,
                            HamLuongVang = "TỔNG CỘNG",
                            TongTien = bangThongKe.TongTienCamDo
                        });

                        listBangInKhachChuoc.Add(new BangInThongKeCamDoTongHopTemp
                        {
                            TieuDeThongKe = "TỔNG TIỀN - VÀNG KHÁCH CHUỘC",
                            TieuChiThongKeTongTien = "TỔNG TIỀN CHUỘC",
                            ThongKeTuNgay = thongKeTuNgay,
                            ThongKeDenNgay = thongKeDenNgay,
                            HamLuongVang = "TỔNG CỘNG",
                            TongTien = bangThongKe.TongTienThanhToan,
                            TongTienLai = bangThongKe.TongTienLaiThanhToan
                        });

                        listBangInThanhLy.Add(new BangInThongKeCamDoTongHopTemp
                        {
                            TieuDeThongKe = "TỔNG TIỀN - VÀNG THANH LÝ",
                            TieuChiThongKeTongTien = "TỔNG TIỀN THANH LÝ",
                            ThongKeTuNgay = thongKeTuNgay,
                            ThongKeDenNgay = thongKeDenNgay,
                            HamLuongVang = "TỔNG CỘNG",
                            TongTien = bangThongKe.TongTienThanhLy,
                            TongTienLai = bangThongKe.TongTienLaiThanhLy
                        });

                        listBangInGiaHan.Add(new BangInThongKeCamDoTongHopTemp
                        {
                            TieuDeThongKe = "GIA HẠN HỢP ĐỒNG",
                            TieuChiThongKeTongTien = "TỔNG TIỀN CẦM",
                            ThongKeTuNgay = thongKeTuNgay,
                            ThongKeDenNgay = thongKeDenNgay,
                            HamLuongVang = "TỔNG CỘNG",
                            TongTien = bangThongKe.TongTienCamGiaHan,
                            TongTienLai = bangThongKe.TongTienLaiGiaHan
                        });

                        break;
                    }

                    listBangInKhachCam.Add(new BangInThongKeCamDoTongHopTemp
                    {
                        TieuChiThongKeTongTien = "TỔNG TIỀN CẦM",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.KiHieuHamLuongVang.HamLuongVang,
                        TongTrongLuong = bangThongKe.TongTrongLuongCamDo
                    });

                    listBangInKhachChuoc.Add(new BangInThongKeCamDoTongHopTemp
                    {
                        TieuDeThongKe = "TỔNG TIỀN - VÀNG KHÁCH CHUỘC",
                        TieuChiThongKeTongTien = "TỔNG TIỀN KHÁCH CHUỘC",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.KiHieuHamLuongVang.HamLuongVang,
                        TongTrongLuong = bangThongKe.TongTrongLuongThanhToan
                    });

                    listBangInThanhLy.Add(new BangInThongKeCamDoTongHopTemp
                    {
                        TieuDeThongKe = "TỔNG TIỀN - VÀNG THANH LÝ",
                        TieuChiThongKeTongTien = "TỔNG TIỀN THANH LÝ",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.KiHieuHamLuongVang.HamLuongVang,
                        TongTrongLuong = bangThongKe.TongTrongLuongThanhLy
                    });

                    listBangInGiaHan.Add(new BangInThongKeCamDoTongHopTemp
                    {
                        TieuDeThongKe = "GIA HẠN HỢP ĐỒNG",
                        TieuChiThongKeTongTien = "TỔNG TIỀN CẦM",
                        ThongKeTuNgay = thongKeTuNgay,
                        ThongKeDenNgay = thongKeDenNgay,
                        HamLuongVang = bangThongKe.KiHieuHamLuongVang.HamLuongVang,
                        TongTrongLuong = bangThongKe.TongTrongLuongGiaHan
                    });
                }

                ThongKeTongTienCamPrint thongKeTongTienCamPrint = new ThongKeTongTienCamPrint();

                thongKeTongTienCamPrint.DataSource = listBangInKhachCam;
                thongKeTongTienCamPrint.ShowPreviewDialog();

                ThongKeCamDoTongHopPrint thongKeKhachChuocPrint = new ThongKeCamDoTongHopPrint();

                thongKeKhachChuocPrint.DataSource = listBangInKhachChuoc;
                thongKeKhachChuocPrint.ShowPreviewDialog();

                ThongKeCamDoTongHopPrint thongKeThanhLyPrint = new ThongKeCamDoTongHopPrint();

                thongKeThanhLyPrint.DataSource = listBangInThanhLy;
                thongKeThanhLyPrint.ShowPreviewDialog();

                ThongKeCamDoTongHopPrint thongKeGiaHanPrint = new ThongKeCamDoTongHopPrint();

                thongKeGiaHanPrint.DataSource = listBangInGiaHan;
                thongKeGiaHanPrint.ShowPreviewDialog();
            }
        }

        private void lueNgayThongKe_EditValueChanged(object sender, EventArgs e)
        {
            if (lueThoiGianThongKe.EditValue == null)
            {
                return;
            }

            if ((int)lueThoiGianThongKe.EditValue == TieuChiThongKeConstant.VALUE_THOI_GIAN_KHOAN_THOI_GIAN)
            {
                detTuNgay.ReadOnly = false;
                detDenNgay.ReadOnly = false;
            }
            else
            {
                detTuNgay.ReadOnly = true;
                detDenNgay.ReadOnly = true;
            }
        }

        private void btnThongKe_Click(object sender, EventArgs e)
        {
            if (!IsHaveNgayDauKi())
            {
                return;
            }

            if (lueThoiGianThongKe.EditValue == null || TieuChiThongKeConstant.VALUE_THOI_GIAN_HOM_NAY == Convert.ToInt32(lueThoiGianThongKe.EditValue))
            {
                GetThongKeByDate(DateTime.Today);
                thongKeTuNgay = DateTime.Today;
                thongKeDenNgay = DateTime.Today;
            }
            else if (detTuNgay.DateTime.Date == detDenNgay.DateTime.Date)
            {
                GetThongKeByDate(detTuNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
            else
            {
                GetThongKeByTwoDifferenceDate(detTuNgay.DateTime, detDenNgay.DateTime);
                thongKeTuNgay = detTuNgay.DateTime;
                thongKeDenNgay = detDenNgay.DateTime;
            }
        }

        private void btnXuatExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string pathSave;
            string fileName = "";
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.ShowNewFolderButton = true;
            folder.Description = "Chọn vị trí lưu file!";
            if (folder.ShowDialog() == DialogResult.OK)
            {
                pathSave = folder.SelectedPath + "\\";
                if (InputBox.Show("Thông báo!",
                "&Nhập tên file:", ref fileName) == DialogResult.OK)
                {
                    string pathSave1 = pathSave + (fileName + " 1" + ".xls");
                    string pathSave2 = pathSave + (fileName + " 2" + ".xls");
                    string pathSave3 = pathSave + (fileName + " 3" + ".xls");
                    string pathSave4 = pathSave + (fileName + " 4" + ".xls");
                    grcTongTienVangKhachCam.ExportToXls(pathSave1);
                    grcTongTienVangKhachChuoc.ExportToXls(pathSave2);
                    grcTongTienVangThanhLy.ExportToXls(pathSave3);
                    grcGiaHanHopDong.ExportToXls(pathSave4);
                }
            }
        }
    }
}