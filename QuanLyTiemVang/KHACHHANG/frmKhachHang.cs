﻿using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.KHACHHANG
{
    public partial class frmKhachHang : Form
    {
        public frmKhachHang()
        {
            InitializeComponent();
        }

        private void frmKhachHang_Load(object sender, EventArgs e)
        {
            ClearForm();
            LoadData();
        }

        private void LoadData()
        {
            grcDanhSachKhachHang.DataSource = KhachHangBUS.Instance.GetListAllKhachHang();
            grcDanhSachKhachHang.RefreshDataSource();
        }

        private bool IsValidKhachHang()
        {
            dxerTen.Dispose();

            bool check = true;

            if (string.IsNullOrWhiteSpace(txtTen.Text))
            {
                dxerTen.SetError(txtTen, ValidationUltils.INPUT_NULL);
                check = false;
            }

            return check;
        }

        private void ClearForm()
        {
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnSua.Tag = null;
            btnXoa.Tag = null;
            txtTen.Text = txtHoLot.Text = txtChungMinhNhanDan.Text = txtSoDienThoai.Text = null;
            txtDiemTichLuy.Text = "0";
            detNgaySinh.DateTime = DateTime.Today;
        }

        private KhachHang GetKhachHangOfForm()
        {
            if (!IsValidKhachHang())
            {
                return null;
            }

            int diemTichLuy = 0;

            if (!string.IsNullOrWhiteSpace(txtDiemTichLuy.Text))
            {
                diemTichLuy = int.Parse(txtDiemTichLuy.Text);
            }

            return new KhachHang
            {
                HoLot = txtHoLot.Text,
                Ten = txtTen.Text,
                DiaChi = txtDiaChi.Text,
                NgaySinh = detNgaySinh.DateTime,
                GioiTinh = rgGioiTinh.SelectedIndex == 0 ? Constant.MALE : Constant.FEMALE,
                ChungMinhNhanDan = txtChungMinhNhanDan.Text,
                SoDienThoai = txtSoDienThoai.Text,
                DiemTichLuy = diemTichLuy
            };
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhachHang khachHang = GetKhachHangOfForm();

            if (khachHang == null)
            {
                return;
            }

            if (KhachHangBUS.Instance.Insert(khachHang) == null)
            {
                PopupService.Instance.Error(Constants.MessageError.INSERT_ERROR);
            }
            else
            {
                ClearForm();
                LoadData();
            }
        }

        private void grcDanhSachKhachHang_DoubleClick(object sender, EventArgs e)
        {
            KhachHang khachHang = (KhachHang)grvDanhSachKhachHang.GetFocusedRow();
            if (khachHang != null)
            {
                ChoPhepChinhSuaDuLieu();
                btnSua.Tag = btnXoa.Tag = khachHang;
                XemChiTietKhachHang(khachHang);
            }
        }

        private void XemChiTietKhachHang(KhachHang khachHang)
        {
            txtHoLot.Text = khachHang.HoLot;
            txtTen.Text = khachHang.Ten;
            txtSoDienThoai.Text = khachHang.SoDienThoai;
            txtChungMinhNhanDan.Text = khachHang.ChungMinhNhanDan;
            detNgaySinh.DateTime = khachHang.NgaySinh.HasValue ? khachHang.NgaySinh.Value : DateTime.Today;
            txtDiemTichLuy.EditValue = khachHang.DiemTichLuy;
            rgGioiTinh.SelectedIndex = khachHang.GioiTinh == Constant.MALE ? 0 : 1;
        }

        private void ChoPhepChinhSuaDuLieu()
        {
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnThem.Enabled = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhachHang khachHang = (KhachHang)btnSua.Tag;
            KhachHang khachHangUpdate = GetKhachHangOfForm();

            khachHangUpdate.MaKhachHang = khachHang.MaKhachHang;

            if (KhachHangBUS.Instance.Update(khachHang) == null)
            {
                PopupService.Instance.Error(Constants.MessageError.UPDATE_ERROR);
            }
            else
            {
                ClearForm();
                LoadData();
            }
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhachHang khachHang = (KhachHang)btnSua.Tag;
            var result = PopupService.Instance.Question(Constants.MessageWarning.DELETE_DATA_ROW);

            if (result == DialogResult.Yes)
            {
                if (KhachHangBUS.Instance.DeleteById(khachHang.MaKhachHang) == null)
                {
                    PopupService.Instance.Error(MessageError.DELETE_ERROR);
                }
                else
                {
                    PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                    ClearForm();
                    LoadData();
                }

            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearForm();
            LoadData();
        }
    }
}
