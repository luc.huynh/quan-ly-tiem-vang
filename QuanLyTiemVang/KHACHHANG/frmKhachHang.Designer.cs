﻿namespace QuanLyTiemVang.KHACHHANG
{
    partial class frmKhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSua = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.grpThongTinKhachHang = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.rgGioiTinh = new DevExpress.XtraEditors.RadioGroup();
            this.txtDiemTichLuy = new DevExpress.XtraEditors.TextEdit();
            this.txtChungMinhNhanDan = new DevExpress.XtraEditors.TextEdit();
            this.txtSoDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.txtHoLot = new DevExpress.XtraEditors.TextEdit();
            this.detNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.grpDanhSachKhachHang = new DevExpress.XtraEditors.GroupControl();
            this.grcDanhSachKhachHang = new DevExpress.XtraGrid.GridControl();
            this.grvDanhSachKhachHang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclHoLot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclSoDienThoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclChungMinhNhanDan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclDiemTichLuy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dxerTen = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinKhachHang)).BeginInit();
            this.grpThongTinKhachHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemTichLuy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChungMinhNhanDan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachKhachHang)).BeginInit();
            this.grpDanhSachKhachHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachKhachHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachKhachHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTen)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnSua,
            this.btnXoa,
            this.btnNhapLai});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 4;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnSua, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnThem
            // 
            this.btnThem.Caption = "Thêm";
            this.btnThem.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThem.Id = 0;
            this.btnThem.Name = "btnThem";
            this.btnThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThem_ItemClick);
            // 
            // btnSua
            // 
            this.btnSua.Caption = "Sửa";
            this.btnSua.Glyph = global::QuanLyTiemVang.Properties.Resources.edit_icon;
            this.btnSua.Id = 1;
            this.btnSua.Name = "btnSua";
            this.btnSua.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSua_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 2;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 3;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1344, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 585);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1344, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 535);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1344, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 535);
            // 
            // grpThongTinKhachHang
            // 
            this.grpThongTinKhachHang.Controls.Add(this.layoutControl1);
            this.grpThongTinKhachHang.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpThongTinKhachHang.Location = new System.Drawing.Point(0, 50);
            this.grpThongTinKhachHang.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpThongTinKhachHang.Name = "grpThongTinKhachHang";
            this.grpThongTinKhachHang.Size = new System.Drawing.Size(1344, 198);
            this.grpThongTinKhachHang.TabIndex = 4;
            this.grpThongTinKhachHang.Text = "Thông tin khách hàng";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtDiaChi);
            this.layoutControl1.Controls.Add(this.rgGioiTinh);
            this.layoutControl1.Controls.Add(this.txtDiemTichLuy);
            this.layoutControl1.Controls.Add(this.txtChungMinhNhanDan);
            this.layoutControl1.Controls.Add(this.txtSoDienThoai);
            this.layoutControl1.Controls.Add(this.txtTen);
            this.layoutControl1.Controls.Add(this.txtHoLot);
            this.layoutControl1.Controls.Add(this.detNgaySinh);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1340, 171);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(157, 44);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDiaChi.MenuManager = this.barManager1;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(1167, 22);
            this.txtDiaChi.StyleController = this.layoutControl1;
            this.txtDiaChi.TabIndex = 11;
            // 
            // rgGioiTinh
            // 
            this.rgGioiTinh.Location = new System.Drawing.Point(157, 72);
            this.rgGioiTinh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rgGioiTinh.MenuManager = this.barManager1;
            this.rgGioiTinh.Name = "rgGioiTinh";
            this.rgGioiTinh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Nam"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Nữ")});
            this.rgGioiTinh.Size = new System.Drawing.Size(1167, 27);
            this.rgGioiTinh.StyleController = this.layoutControl1;
            this.rgGioiTinh.TabIndex = 10;
            // 
            // txtDiemTichLuy
            // 
            this.txtDiemTichLuy.Location = new System.Drawing.Point(814, 133);
            this.txtDiemTichLuy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDiemTichLuy.MenuManager = this.barManager1;
            this.txtDiemTichLuy.Name = "txtDiemTichLuy";
            this.txtDiemTichLuy.Properties.Mask.EditMask = "d";
            this.txtDiemTichLuy.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiemTichLuy.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDiemTichLuy.Properties.NullText = "0";
            this.txtDiemTichLuy.Properties.ShowNullValuePromptWhenFocused = true;
            this.txtDiemTichLuy.Size = new System.Drawing.Size(510, 22);
            this.txtDiemTichLuy.StyleController = this.layoutControl1;
            this.txtDiemTichLuy.TabIndex = 9;
            // 
            // txtChungMinhNhanDan
            // 
            this.txtChungMinhNhanDan.Location = new System.Drawing.Point(814, 105);
            this.txtChungMinhNhanDan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtChungMinhNhanDan.MenuManager = this.barManager1;
            this.txtChungMinhNhanDan.Name = "txtChungMinhNhanDan";
            this.txtChungMinhNhanDan.Properties.Mask.EditMask = "f0";
            this.txtChungMinhNhanDan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChungMinhNhanDan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtChungMinhNhanDan.Size = new System.Drawing.Size(510, 22);
            this.txtChungMinhNhanDan.StyleController = this.layoutControl1;
            this.txtChungMinhNhanDan.TabIndex = 7;
            // 
            // txtSoDienThoai
            // 
            this.txtSoDienThoai.Location = new System.Drawing.Point(157, 105);
            this.txtSoDienThoai.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSoDienThoai.MenuManager = this.barManager1;
            this.txtSoDienThoai.Name = "txtSoDienThoai";
            this.txtSoDienThoai.Properties.Mask.EditMask = "f0";
            this.txtSoDienThoai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoDienThoai.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSoDienThoai.Size = new System.Drawing.Size(510, 22);
            this.txtSoDienThoai.StyleController = this.layoutControl1;
            this.txtSoDienThoai.TabIndex = 6;
            // 
            // txtTen
            // 
            this.txtTen.Location = new System.Drawing.Point(813, 16);
            this.txtTen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTen.MenuManager = this.barManager1;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(511, 22);
            this.txtTen.StyleController = this.layoutControl1;
            this.txtTen.TabIndex = 5;
            // 
            // txtHoLot
            // 
            this.txtHoLot.Location = new System.Drawing.Point(157, 16);
            this.txtHoLot.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtHoLot.MenuManager = this.barManager1;
            this.txtHoLot.Name = "txtHoLot";
            this.txtHoLot.Size = new System.Drawing.Size(509, 22);
            this.txtHoLot.StyleController = this.layoutControl1;
            this.txtHoLot.TabIndex = 4;
            // 
            // detNgaySinh
            // 
            this.detNgaySinh.EditValue = null;
            this.detNgaySinh.Location = new System.Drawing.Point(157, 133);
            this.detNgaySinh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.detNgaySinh.MenuManager = this.barManager1;
            this.detNgaySinh.Name = "detNgaySinh";
            this.detNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgaySinh.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgaySinh.Properties.DisplayFormat.FormatString = "";
            this.detNgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgaySinh.Properties.EditFormat.FormatString = "";
            this.detNgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgaySinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgaySinh.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgaySinh.Size = new System.Drawing.Size(510, 22);
            this.detNgaySinh.StyleController = this.layoutControl1;
            this.detNgaySinh.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1340, 171);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtHoLot;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(656, 28);
            this.layoutControlItem1.Text = "Họ lót";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(138, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtSoDienThoai;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(657, 28);
            this.layoutControlItem3.Text = "Số điện thoại";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(138, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtChungMinhNhanDan;
            this.layoutControlItem4.Location = new System.Drawing.Point(657, 89);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(657, 28);
            this.layoutControlItem4.Text = "Chứng minh nhân dân";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(138, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.detNgaySinh;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 117);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(657, 28);
            this.layoutControlItem5.Text = "Ngày sinh";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(138, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtDiemTichLuy;
            this.layoutControlItem6.Location = new System.Drawing.Point(657, 117);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(657, 28);
            this.layoutControlItem6.Text = "Điểm tích lũy";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(138, 17);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.rgGioiTinh;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(1314, 33);
            this.layoutControlItem7.Text = "Giới tính";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(138, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtDiaChi;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1314, 28);
            this.layoutControlItem8.Text = "Địa chỉ";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(138, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTen;
            this.layoutControlItem2.Location = new System.Drawing.Point(656, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(658, 28);
            this.layoutControlItem2.Text = "Tên";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(138, 16);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 248);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1344, 6);
            this.splitterControl1.TabIndex = 5;
            this.splitterControl1.TabStop = false;
            // 
            // grpDanhSachKhachHang
            // 
            this.grpDanhSachKhachHang.Controls.Add(this.grcDanhSachKhachHang);
            this.grpDanhSachKhachHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDanhSachKhachHang.Location = new System.Drawing.Point(0, 254);
            this.grpDanhSachKhachHang.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpDanhSachKhachHang.Name = "grpDanhSachKhachHang";
            this.grpDanhSachKhachHang.Size = new System.Drawing.Size(1344, 331);
            this.grpDanhSachKhachHang.TabIndex = 6;
            this.grpDanhSachKhachHang.Text = "Danh Sách khách hàng";
            // 
            // grcDanhSachKhachHang
            // 
            this.grcDanhSachKhachHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachKhachHang.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grcDanhSachKhachHang.Location = new System.Drawing.Point(2, 25);
            this.grcDanhSachKhachHang.MainView = this.grvDanhSachKhachHang;
            this.grcDanhSachKhachHang.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grcDanhSachKhachHang.MenuManager = this.barManager1;
            this.grcDanhSachKhachHang.Name = "grcDanhSachKhachHang";
            this.grcDanhSachKhachHang.Size = new System.Drawing.Size(1340, 304);
            this.grcDanhSachKhachHang.TabIndex = 0;
            this.grcDanhSachKhachHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDanhSachKhachHang});
            this.grcDanhSachKhachHang.DoubleClick += new System.EventHandler(this.grcDanhSachKhachHang_DoubleClick);
            // 
            // grvDanhSachKhachHang
            // 
            this.grvDanhSachKhachHang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclHoLot,
            this.grclTen,
            this.grclSoDienThoai,
            this.grclChungMinhNhanDan,
            this.grclNgaySinh,
            this.grclDiemTichLuy,
            this.grclGioiTinh});
            this.grvDanhSachKhachHang.GridControl = this.grcDanhSachKhachHang;
            this.grvDanhSachKhachHang.Name = "grvDanhSachKhachHang";
            this.grvDanhSachKhachHang.OptionsBehavior.Editable = false;
            this.grvDanhSachKhachHang.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvDanhSachKhachHang.OptionsFind.AlwaysVisible = true;
            this.grvDanhSachKhachHang.OptionsFind.FindDelay = 100;
            this.grvDanhSachKhachHang.OptionsFind.FindNullPrompt = "";
            this.grvDanhSachKhachHang.OptionsFind.ShowFindButton = false;
            this.grvDanhSachKhachHang.OptionsView.ShowGroupPanel = false;
            // 
            // grclHoLot
            // 
            this.grclHoLot.Caption = "Họ";
            this.grclHoLot.FieldName = "HoLot";
            this.grclHoLot.Name = "grclHoLot";
            this.grclHoLot.Visible = true;
            this.grclHoLot.VisibleIndex = 0;
            this.grclHoLot.Width = 72;
            // 
            // grclTen
            // 
            this.grclTen.Caption = "Tên";
            this.grclTen.FieldName = "Ten";
            this.grclTen.Name = "grclTen";
            this.grclTen.Visible = true;
            this.grclTen.VisibleIndex = 1;
            this.grclTen.Width = 151;
            // 
            // grclSoDienThoai
            // 
            this.grclSoDienThoai.Caption = "Số điện thoại";
            this.grclSoDienThoai.FieldName = "SoDienThoai";
            this.grclSoDienThoai.Name = "grclSoDienThoai";
            this.grclSoDienThoai.Visible = true;
            this.grclSoDienThoai.VisibleIndex = 3;
            this.grclSoDienThoai.Width = 162;
            // 
            // grclChungMinhNhanDan
            // 
            this.grclChungMinhNhanDan.Caption = "Chứng minh nhân dân";
            this.grclChungMinhNhanDan.FieldName = "ChungMinhNhanDan";
            this.grclChungMinhNhanDan.Name = "grclChungMinhNhanDan";
            this.grclChungMinhNhanDan.Visible = true;
            this.grclChungMinhNhanDan.VisibleIndex = 4;
            this.grclChungMinhNhanDan.Width = 162;
            // 
            // grclNgaySinh
            // 
            this.grclNgaySinh.Caption = "Ngày sinh";
            this.grclNgaySinh.FieldName = "NgaySinh";
            this.grclNgaySinh.Name = "grclNgaySinh";
            this.grclNgaySinh.Visible = true;
            this.grclNgaySinh.VisibleIndex = 5;
            this.grclNgaySinh.Width = 162;
            // 
            // grclDiemTichLuy
            // 
            this.grclDiemTichLuy.Caption = "Điểm tích lũy";
            this.grclDiemTichLuy.FieldName = "DiemTichLuy";
            this.grclDiemTichLuy.Name = "grclDiemTichLuy";
            this.grclDiemTichLuy.Visible = true;
            this.grclDiemTichLuy.VisibleIndex = 6;
            this.grclDiemTichLuy.Width = 174;
            // 
            // grclGioiTinh
            // 
            this.grclGioiTinh.Caption = "Giới tính";
            this.grclGioiTinh.FieldName = "GioiTinh";
            this.grclGioiTinh.Name = "grclGioiTinh";
            this.grclGioiTinh.Visible = true;
            this.grclGioiTinh.VisibleIndex = 2;
            this.grclGioiTinh.Width = 103;
            // 
            // dxerTen
            // 
            this.dxerTen.ContainerControl = this;
            // 
            // frmKhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 585);
            this.Controls.Add(this.grpDanhSachKhachHang);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.grpThongTinKhachHang);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmKhachHang";
            this.Text = "Khách Hàng";
            this.Load += new System.EventHandler(this.frmKhachHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinKhachHang)).EndInit();
            this.grpThongTinKhachHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemTichLuy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChungMinhNhanDan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachKhachHang)).EndInit();
            this.grpDanhSachKhachHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachKhachHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachKhachHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarButtonItem btnSua;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraEditors.GroupControl grpDanhSachKhachHang;
        private DevExpress.XtraGrid.GridControl grcDanhSachKhachHang;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDanhSachKhachHang;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl grpThongTinKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn grclHoLot;
        private DevExpress.XtraGrid.Columns.GridColumn grclTen;
        private DevExpress.XtraGrid.Columns.GridColumn grclSoDienThoai;
        private DevExpress.XtraGrid.Columns.GridColumn grclChungMinhNhanDan;
        private DevExpress.XtraGrid.Columns.GridColumn grclNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn grclDiemTichLuy;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtDiemTichLuy;
        private DevExpress.XtraEditors.TextEdit txtChungMinhNhanDan;
        private DevExpress.XtraEditors.TextEdit txtSoDienThoai;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.TextEdit txtHoLot;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.RadioGroup rgGioiTinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn grclGioiTinh;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.DateEdit detNgaySinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTen;
    }
}