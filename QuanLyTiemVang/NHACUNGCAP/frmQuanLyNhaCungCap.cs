﻿using System;
using System.Windows.Forms;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;

namespace QuanLyTiemVang.NHACUNGCAP
{
    public partial class frmQuanLyNhaCungCap : DevExpress.XtraEditors.XtraForm
    {
        public frmQuanLyNhaCungCap()
        {
            InitializeComponent();
        }

        private void frmQuanLyNhaCungCap_Load(object sender, EventArgs e)
        {
            refreshData();
        }


        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string tenNhaCungCap = txtTenNhaCungCap.Text;
            string diaChi = txtDiaChiNhaCungCap.Text;

            if (string.IsNullOrWhiteSpace(tenNhaCungCap))
            {
                PopupService.Instance.Error(ErrorUltils.ENOUGH_DATA);
                return;
            }

            if (!NhaCungCapBUS.Instance.Insert(tenNhaCungCap, diaChi))
            {
                PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
            }
            else
            {
                refreshData();
            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            refreshData();
        }

        private void refreshData()
        {
            txtTenNhaCungCap.Text = null;
            txtDiaChiNhaCungCap.Text = null;
            btnThem.Enabled = true;
            btnSua.Tag = null;
            btnXoa.Tag = null;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            grcDanhSachNhaCungCap.DataSource = NhaCungCapBUS.Instance.GetListAllNhaCungCap();
        }

        private void XemChiTietDuLieu(NhaCungCap nhaCungCap)
        {
            if (nhaCungCap != null)
            {
                btnThem.Enabled = false;
                btnSua.Enabled = true;
                btnXoa.Enabled = true;
                btnSua.Tag = nhaCungCap;
                btnXoa.Tag = nhaCungCap;
                txtTenNhaCungCap.Text = nhaCungCap.TenNhaCungCap;
                txtDiaChiNhaCungCap.Text = nhaCungCap.DiaChi;
            }

        }

        private void grcDanhSachNhaCungCap_DoubleClick(object sender, EventArgs e)
        {
            NhaCungCap nhaCungCap = (NhaCungCap)grvDanhSachNhaCungCap.GetFocusedRow();
            XemChiTietDuLieu(nhaCungCap);
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnSua.Tag != null)
            {
                NhaCungCap nhaCungCap = (NhaCungCap)btnSua.Tag;
                nhaCungCap.TenNhaCungCap = txtTenNhaCungCap.Text;
                nhaCungCap.DiaChi = txtDiaChiNhaCungCap.Text;
                if (string.IsNullOrWhiteSpace(nhaCungCap.TenNhaCungCap))
                {
                    PopupService.Instance.Error(ErrorUltils.ENOUGH_DATA);
                    return;
                }
                if (!NhaCungCapBUS.Instance.Update(nhaCungCap))
                {
                    PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                }
                else
                {
                    PopupService.Instance.Success(MessageSucces.UPDATE_DATA_ROW);
                    refreshData();
                }
            }
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (btnXoa.Tag != null)
            {
                NhaCungCap nhaCungCap = (NhaCungCap)btnXoa.Tag;
                DialogResult result = PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW);
                if (result == DialogResult.Yes)
                {
                    if (!NhaCungCapBUS.Instance.Delete(nhaCungCap))
                    {
                        PopupService.Instance.Error(ErrorUltils.SOMETHING_ERROR);
                    }
                    else
                    {
                        PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                        refreshData();
                    }
                }
            }
        }
    }
}