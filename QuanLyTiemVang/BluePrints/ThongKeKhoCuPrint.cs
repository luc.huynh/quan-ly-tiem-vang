﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyTiemVang.BluePrints
{
    public partial class ThongKeKhoCuPrint : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongKeKhoCuPrint(DateTime thongKeTuNgay, DateTime thongKeDenNgay)
        {
            InitializeComponent();
            xrlThongKeTuNgay.Text = thongKeTuNgay.ToString("dd/MM/yyyy");
            xrlThongKeDenNgay.Text = thongKeDenNgay.ToString("dd/MM/yyyy");
        }

    }
}
