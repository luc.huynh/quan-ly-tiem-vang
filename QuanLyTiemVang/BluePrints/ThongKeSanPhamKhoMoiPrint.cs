﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyTiemVang.BluePrints
{
    public partial class ThongKeSanPhamKhoMoiPrint : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongKeSanPhamKhoMoiPrint()
        {
            InitializeComponent();

            xrlDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }

    }
}
