﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.Models;
using System.Diagnostics;
using System.Runtime.InteropServices;
using QuanLyTiemVang.Services;

namespace QuanLyTiemVang.DANGNHAP
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            var user = txtTenDangNhap.Text;
            var pass = txtMatKhau.Text;

            if (isValidate(user, pass))
            {
                splashScreenManager1.ShowWaitForm();
                NhanVien nhanvien = BUS.NhanVienBUS.Instance.Login(user, pass);
                if (nhanvien != null)
                {
                    Hide();
                    var f = new Form1(nhanvien);
                    f.Show();
                    splashScreenManager1.CloseWaitForm();

                }
                else
                {
                    splashScreenManager1.CloseWaitForm();
                    PopupService.Instance.Error("Sai tên đăng nhập hoặc mật khẩu");
                }
            }
        }

        private bool isValidate(string user, string pass)
        {
            if (string.IsNullOrWhiteSpace(user))
            {
                PopupService.Instance.Error(Constants.StringUltils.ErrorUltils.USER_NULL);

                return false;
            }
            else if (string.IsNullOrWhiteSpace(pass))
            {
                PopupService.Instance.Error(Constants.StringUltils.ErrorUltils.PASS_NULL);

                return false;
            }

            return true;
        }

        private void AutoLogin()
        {
            txtTenDangNhap.Text = "admin";
            txtMatKhau.Text = "Admin@@123";
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            AutoLogin();

            //OnInit();
        }

        #region Trial Section :
        // This function does all the work
        [DllImport("Trial.dll", EntryPoint = "ReadSettingsStr", CharSet = CharSet.Ansi)]
        static extern uint InitTrial(String aKeyCode, IntPtr aHWnd);

        // Use this function to register the application when the application is running
        [DllImport("Trial.dll", EntryPoint = "DisplayRegistrationStr", CharSet = CharSet.Ansi)]
        static extern uint DisplayRegistration(String aKeyCode, IntPtr aHWnd);


        // The kLibraryKey is meant to prevent unauthorized use of the library.
        // Do not share this key. Replace this key with your own from Advanced Installer 
        // project > Licensing > Registration > Library Key
        private const string kLibraryKey = "203567A830E8747916AEA29AC9BF419D32524581869DAA3C5F64CBD750B2DF7CCAF7D38DC714";

        private static void OnInit()
        {
            try
            {
                Process process = Process.GetCurrentProcess();
                InitTrial(kLibraryKey, process.MainWindowHandle);
            }
            catch (DllNotFoundException ex)
            {
                // Trial dll is missing close the application immediately.
                PopupService.Instance.Error(ex.ToString());
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception ex1)
            {
                PopupService.Instance.Error(ex1.ToString());
            }
        }

        private void RegisterApp(object sender, EventArgs e)
        {
            try
            {
                Process process = Process.GetCurrentProcess();
                DisplayRegistration(kLibraryKey, process.MainWindowHandle);
            }
            catch (DllNotFoundException ex)
            {
                // Trial dll is missing close the application immediately.
                PopupService.Instance.Error(ex.ToString());
            }
            catch (Exception ex1)
            {
                PopupService.Instance.Error(ex1.ToString());
            }
        }
        #endregion
    }
}