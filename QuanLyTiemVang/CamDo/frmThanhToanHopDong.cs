﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO.Models;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;

namespace QuanLyTiemVang.CamDo
{
    public partial class frmThanhToanHopDong : DevExpress.XtraEditors.XtraForm
    {
        private string maHopDong;
        private HopDongCamDo hopDong;
        private int soTienLai;

        public frmThanhToanHopDong(string maHopDong)
        {
            InitializeComponent();
            this.maHopDong = maHopDong;
        }

        private void frmThanhToanHopDong_Load(object sender, EventArgs e)
        {
            detNgayThanhToan.DateTime = DateTime.Today;
            txtMaHopDong.Text = maHopDong;
            LoadData();
        }

        private void LoadData()
        {
            soTienLai = HopDongCamDoBUS.Instance.TinhSoTienLai(maHopDong, detNgayThanhToan.DateTime);
            txtSoTienLai.EditValue = soTienLai;

            hopDong= HopDongCamDoBUS.Instance.GetHopDongById(maHopDong);
            txtSoTienCam.EditValue = hopDong.SoTienCam;

            txtTongTien.EditValue = soTienLai + hopDong.SoTienCam;
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            var dialogResult = PopupService.Instance.Question(MessageQuestion.THANH_TOAN_CAM_DO);

            if (dialogResult == DialogResult.No)
            {
                return;
            }

            if (hopDong == null)
            {
                PopupService.Instance.Error(MessageError.DATA_ROW_INVALID);
                return;
            }

            if (HopDongCamDoBUS.Instance.GiaHanHopDong(maHopDong, DateTime.Today, soTienLai) == null)
            {
                PopupService.Instance.Error(MessageError.UPDATE_ERROR);
                return;
            }

            HopDongCamDo hopDongUpdate = HopDongCamDoBUS.Instance.GetHopDongById(hopDong.MaHopDongCamDo);

            ChiTietCamDo chiTietCamDo = hopDongUpdate.ChiTietCamDoes.OrderByDescending(x => x.NgayNopTienLai).FirstOrDefault();

            if (chiTietCamDo == null || chiTietCamDo.NgayNopTienLai.Date != DateTime.Today)
            {
                PopupService.Instance.Error(MessageWarning.DONG_LAI_CAM_DO);
                return;
            }

            if (HopDongCamDoBUS.Instance.ThanhToan(hopDongUpdate.MaHopDongCamDo))
            {
                PopupService.Instance.Success(MessageSucces.THANH_TOAN_CAM_DO);
                Close();
            }
            else
            {
                PopupService.Instance.Error(MessageError.UPDATE_ERROR);
            }
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}