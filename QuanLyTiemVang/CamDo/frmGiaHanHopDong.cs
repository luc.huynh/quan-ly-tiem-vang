﻿using BUS;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.CamDo
{
    public partial class frmGiaHanHopDong : Form
    {
        private string nhanVien;
        private string maHopDong;
        private int maNhanVien;

        public frmGiaHanHopDong(int maNhanVien, string tenNhanVien, string maHopDong)
        {
            InitializeComponent();

            nhanVien = tenNhanVien;
            this.maHopDong = maHopDong;
            this.maNhanVien = maNhanVien;
        }

        private void frmGiaHanHopDong_Load(object sender, EventArgs e)
        {
            txtNhanVien.Text = nhanVien;
            txtMaHopDong.Text = maHopDong;
            dtpNgayDongLai.DateTime = DateTime.Today;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var result = PopupService.Instance.Question(MessageWarning.INSERT_HOP_DONG_CAM_DO);

            if (result == DialogResult.No)
            {
                return;
            }

            int soTienDongLai = 0;

            if (txtSoTienDongLai.EditValue != null)
            {
                soTienDongLai = int.Parse(txtSoTienDongLai.EditValue.ToString());
            }

            if (HopDongCamDoBUS.Instance.GiaHanHopDong(maHopDong, dtpNgayDongLai.DateTime, soTienDongLai) != null)
            {
                PopupService.Instance.Success("Gia hạn thành công");
                Hide();
            }
            else
            {
                PopupService.Instance.Error("Có lỗi");
            }
        }

        private void dtpNgayDongLai_EditValueChanged(object sender, EventArgs e)
        {
            txtSoTienDongLai.EditValue = HopDongCamDoBUS.Instance.TinhSoTienLai(maHopDong, dtpNgayDongLai.DateTime);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
