﻿namespace QuanLyTiemVang.CamDo
{
    partial class frmTaoMoiHopDongCamDo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.grpHoaDonCamDo = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txtSoDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnThem = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnGiaHanHopDong = new DevExpress.XtraBars.BarButtonItem();
            this.btnThanhToanHopDong = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnInLaiHopDong = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtHoTenKhachHang = new DevExpress.XtraEditors.TextEdit();
            this.txtLaiSuat = new DevExpress.XtraEditors.TextEdit();
            this.txtSoTienCam = new DevExpress.XtraEditors.TextEdit();
            this.txtNhanVien = new DevExpress.XtraEditors.TextEdit();
            this.txtTrangThaiHopDong = new DevExpress.XtraEditors.TextEdit();
            this.txtMaHopDong = new DevExpress.XtraEditors.TextEdit();
            this.detNgayTaoHopDong = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTrangThaiHopDong = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutNgayTaoHopDong = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.btnThemSanPhamCamDo = new DevExpress.XtraEditors.SimpleButton();
            this.grcSanPhamCamDo = new DevExpress.XtraGrid.GridControl();
            this.sanPhamCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvSanPhamCamDo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCamVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenMonHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCanNang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDeleteSanPhamCamDo = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.txtTenMonHang = new DevExpress.XtraEditors.TextEdit();
            this.txtCanNang = new DevExpress.XtraEditors.TextEdit();
            this.lueCamVang = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.grpDanhSachCamDo = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.grcDanhSachCamDo = new DevExpress.XtraGrid.GridControl();
            this.hopDongCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvDanhSachCamDo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclMaHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclLaiSuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayBatDau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoTienCam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.chiTietCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.dxerCamVang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerTenMonHang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerSoTienCam = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerLaiSuat = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerTenKhachHang = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dxerSoDienThoai = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.ckbKhachQuen = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.colIsKhachQuen = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grpHoaDonCamDo)).BeginInit();
            this.grpHoaDonCamDo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTenKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLaiSuat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTienCam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrangThaiHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayTaoHopDong.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayTaoHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrangThaiHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNgayTaoHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteSanPhamCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenMonHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCanNang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCamVang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachCamDo)).BeginInit();
            this.grpDanhSachCamDo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopDongCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chiTietCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxerCamVang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenMonHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerSoTienCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLaiSuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenKhachHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerSoDienThoai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbKhachQuen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // grpHoaDonCamDo
            // 
            this.grpHoaDonCamDo.Controls.Add(this.layoutControl2);
            this.grpHoaDonCamDo.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpHoaDonCamDo.Location = new System.Drawing.Point(0, 50);
            this.grpHoaDonCamDo.Margin = new System.Windows.Forms.Padding(4);
            this.grpHoaDonCamDo.Name = "grpHoaDonCamDo";
            this.grpHoaDonCamDo.Size = new System.Drawing.Size(1344, 193);
            this.grpHoaDonCamDo.TabIndex = 0;
            this.grpHoaDonCamDo.Text = "Hợp đồng cầm đồ";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.ckbKhachQuen);
            this.layoutControl2.Controls.Add(this.txtSoDienThoai);
            this.layoutControl2.Controls.Add(this.txtDiaChi);
            this.layoutControl2.Controls.Add(this.txtHoTenKhachHang);
            this.layoutControl2.Controls.Add(this.txtLaiSuat);
            this.layoutControl2.Controls.Add(this.txtSoTienCam);
            this.layoutControl2.Controls.Add(this.txtNhanVien);
            this.layoutControl2.Controls.Add(this.txtTrangThaiHopDong);
            this.layoutControl2.Controls.Add(this.txtMaHopDong);
            this.layoutControl2.Controls.Add(this.detNgayTaoHopDong);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 25);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1340, 166);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txtSoDienThoai
            // 
            this.txtSoDienThoai.Location = new System.Drawing.Point(553, 72);
            this.txtSoDienThoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSoDienThoai.MenuManager = this.barManager1;
            this.txtSoDienThoai.Name = "txtSoDienThoai";
            this.txtSoDienThoai.Size = new System.Drawing.Size(263, 22);
            this.txtSoDienThoai.StyleController = this.layoutControl2;
            this.txtSoDienThoai.TabIndex = 15;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnThem,
            this.btnNhapLai,
            this.btnXoa,
            this.btnGiaHanHopDong,
            this.btnLamMoiDuLieu,
            this.btnInLaiHopDong,
            this.btnThanhToanHopDong});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 7;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnGiaHanHopDong, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThanhToanHopDong, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInLaiHopDong, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnThem
            // 
            this.btnThem.Caption = "Tạo mới và in";
            this.btnThem.Glyph = global::QuanLyTiemVang.Properties.Resources.add_icon;
            this.btnThem.Id = 0;
            this.btnThem.Name = "btnThem";
            this.btnThem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThem_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 1;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 4;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // btnGiaHanHopDong
            // 
            this.btnGiaHanHopDong.Caption = "Gia hạn hợp đồng";
            this.btnGiaHanHopDong.Glyph = global::QuanLyTiemVang.Properties.Resources.time_icon;
            this.btnGiaHanHopDong.Id = 3;
            this.btnGiaHanHopDong.Name = "btnGiaHanHopDong";
            this.btnGiaHanHopDong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGiaHanHopDong_ItemClick);
            // 
            // btnThanhToanHopDong
            // 
            this.btnThanhToanHopDong.Caption = "Thanh toán hợp đồng";
            this.btnThanhToanHopDong.Glyph = global::QuanLyTiemVang.Properties.Resources.cash_stack_icon;
            this.btnThanhToanHopDong.Id = 6;
            this.btnThanhToanHopDong.Name = "btnThanhToanHopDong";
            this.btnThanhToanHopDong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThanhToanHopDong_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 2;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // btnInLaiHopDong
            // 
            this.btnInLaiHopDong.Caption = "In lại hợp đồng";
            this.btnInLaiHopDong.Glyph = global::QuanLyTiemVang.Properties.Resources.printer_icon;
            this.btnInLaiHopDong.Id = 5;
            this.btnInLaiHopDong.Name = "btnInLaiHopDong";
            this.btnInLaiHopDong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInLaiHopDong_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlTop.Size = new System.Drawing.Size(1344, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 667);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1344, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 617);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1344, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 617);
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(951, 72);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDiaChi.MenuManager = this.barManager1;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(373, 22);
            this.txtDiaChi.StyleController = this.layoutControl2;
            this.txtDiaChi.TabIndex = 14;
            // 
            // txtHoTenKhachHang
            // 
            this.txtHoTenKhachHang.Location = new System.Drawing.Point(145, 72);
            this.txtHoTenKhachHang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHoTenKhachHang.MenuManager = this.barManager1;
            this.txtHoTenKhachHang.Name = "txtHoTenKhachHang";
            this.txtHoTenKhachHang.Size = new System.Drawing.Size(273, 22);
            this.txtHoTenKhachHang.StyleController = this.layoutControl2;
            this.txtHoTenKhachHang.TabIndex = 13;
            // 
            // txtLaiSuat
            // 
            this.txtLaiSuat.Location = new System.Drawing.Point(145, 44);
            this.txtLaiSuat.Margin = new System.Windows.Forms.Padding(4);
            this.txtLaiSuat.MenuManager = this.barManager1;
            this.txtLaiSuat.Name = "txtLaiSuat";
            this.txtLaiSuat.Properties.DisplayFormat.FormatString = "## %";
            this.txtLaiSuat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtLaiSuat.Properties.EditFormat.FormatString = "## %";
            this.txtLaiSuat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtLaiSuat.Properties.Mask.EditMask = "p";
            this.txtLaiSuat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLaiSuat.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtLaiSuat.Properties.MaxLength = 5;
            this.txtLaiSuat.Properties.NullText = "0";
            this.txtLaiSuat.Size = new System.Drawing.Size(521, 22);
            this.txtLaiSuat.StyleController = this.layoutControl2;
            this.txtLaiSuat.TabIndex = 10;
            // 
            // txtSoTienCam
            // 
            this.txtSoTienCam.Location = new System.Drawing.Point(801, 44);
            this.txtSoTienCam.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoTienCam.MenuManager = this.barManager1;
            this.txtSoTienCam.Name = "txtSoTienCam";
            this.txtSoTienCam.Properties.Mask.EditMask = "###,###,###,###,### VNĐ";
            this.txtSoTienCam.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoTienCam.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSoTienCam.Properties.MaxLength = 12;
            this.txtSoTienCam.Properties.NullText = "0";
            this.txtSoTienCam.Size = new System.Drawing.Size(523, 22);
            this.txtSoTienCam.StyleController = this.layoutControl2;
            this.txtSoTienCam.TabIndex = 5;
            // 
            // txtNhanVien
            // 
            this.txtNhanVien.Location = new System.Drawing.Point(145, 100);
            this.txtNhanVien.Margin = new System.Windows.Forms.Padding(4);
            this.txtNhanVien.MenuManager = this.barManager1;
            this.txtNhanVien.Name = "txtNhanVien";
            this.txtNhanVien.Size = new System.Drawing.Size(1179, 22);
            this.txtNhanVien.StyleController = this.layoutControl2;
            this.txtNhanVien.TabIndex = 9;
            // 
            // txtTrangThaiHopDong
            // 
            this.txtTrangThaiHopDong.Location = new System.Drawing.Point(801, 128);
            this.txtTrangThaiHopDong.Margin = new System.Windows.Forms.Padding(4);
            this.txtTrangThaiHopDong.MenuManager = this.barManager1;
            this.txtTrangThaiHopDong.Name = "txtTrangThaiHopDong";
            this.txtTrangThaiHopDong.Size = new System.Drawing.Size(523, 22);
            this.txtTrangThaiHopDong.StyleController = this.layoutControl2;
            this.txtTrangThaiHopDong.TabIndex = 7;
            // 
            // txtMaHopDong
            // 
            this.txtMaHopDong.Location = new System.Drawing.Point(145, 16);
            this.txtMaHopDong.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaHopDong.MenuManager = this.barManager1;
            this.txtMaHopDong.Name = "txtMaHopDong";
            this.txtMaHopDong.Properties.ReadOnly = true;
            this.txtMaHopDong.Size = new System.Drawing.Size(521, 22);
            this.txtMaHopDong.StyleController = this.layoutControl2;
            this.txtMaHopDong.TabIndex = 0;
            this.txtMaHopDong.EditValueChanged += new System.EventHandler(this.txtMaHopDong_EditValueChanged);
            // 
            // detNgayTaoHopDong
            // 
            this.detNgayTaoHopDong.EditValue = null;
            this.detNgayTaoHopDong.Location = new System.Drawing.Point(145, 128);
            this.detNgayTaoHopDong.Margin = new System.Windows.Forms.Padding(4);
            this.detNgayTaoHopDong.MenuManager = this.barManager1;
            this.detNgayTaoHopDong.Name = "detNgayTaoHopDong";
            this.detNgayTaoHopDong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayTaoHopDong.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.detNgayTaoHopDong.Properties.DisplayFormat.FormatString = "";
            this.detNgayTaoHopDong.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayTaoHopDong.Properties.EditFormat.FormatString = "";
            this.detNgayTaoHopDong.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.detNgayTaoHopDong.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.detNgayTaoHopDong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.detNgayTaoHopDong.Size = new System.Drawing.Size(521, 22);
            this.detNgayTaoHopDong.StyleController = this.layoutControl2;
            this.detNgayTaoHopDong.TabIndex = 11;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem11,
            this.layoutTrangThaiHopDong,
            this.layoutNgayTaoHopDong,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem8,
            this.layoutControlItem12,
            this.layoutControlItem7,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1340, 166);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMaHopDong;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(656, 28);
            this.layoutControlItem3.Text = "Mã Hợp đồng";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtNhanVien;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(1314, 28);
            this.layoutControlItem11.Text = "Nhân viên";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(126, 16);
            // 
            // layoutTrangThaiHopDong
            // 
            this.layoutTrangThaiHopDong.Control = this.txtTrangThaiHopDong;
            this.layoutTrangThaiHopDong.Location = new System.Drawing.Point(656, 112);
            this.layoutTrangThaiHopDong.Name = "layoutTrangThaiHopDong";
            this.layoutTrangThaiHopDong.Size = new System.Drawing.Size(658, 28);
            this.layoutTrangThaiHopDong.Text = "Trạng thái hợp đồng";
            this.layoutTrangThaiHopDong.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutNgayTaoHopDong
            // 
            this.layoutNgayTaoHopDong.Control = this.detNgayTaoHopDong;
            this.layoutNgayTaoHopDong.Location = new System.Drawing.Point(0, 112);
            this.layoutNgayTaoHopDong.Name = "layoutNgayTaoHopDong";
            this.layoutNgayTaoHopDong.Size = new System.Drawing.Size(656, 28);
            this.layoutNgayTaoHopDong.Text = "Ngày tạo hợp đồng";
            this.layoutNgayTaoHopDong.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtDiaChi;
            this.layoutControlItem13.Location = new System.Drawing.Point(806, 56);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(508, 28);
            this.layoutControlItem13.Text = "Địa chỉ";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtSoDienThoai;
            this.layoutControlItem14.Location = new System.Drawing.Point(408, 56);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(398, 28);
            this.layoutControlItem14.Text = "Số điện thoại";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtLaiSuat;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(656, 28);
            this.layoutControlItem8.Text = "Lãi suất";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtHoTenKhachHang;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(408, 28);
            this.layoutControlItem12.Text = "Tên khách hàng";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(126, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtSoTienCam;
            this.layoutControlItem7.Location = new System.Drawing.Point(656, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(658, 28);
            this.layoutControlItem7.Text = "Số tiền cầm";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(126, 17);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.btnThemSanPhamCamDo);
            this.layoutControl3.Controls.Add(this.grcSanPhamCamDo);
            this.layoutControl3.Controls.Add(this.txtTenMonHang);
            this.layoutControl3.Controls.Add(this.txtCanNang);
            this.layoutControl3.Controls.Add(this.lueCamVang);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 25);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1340, 147);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // btnThemSanPhamCamDo
            // 
            this.btnThemSanPhamCamDo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThemSanPhamCamDo.Location = new System.Drawing.Point(16, 100);
            this.btnThemSanPhamCamDo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThemSanPhamCamDo.Name = "btnThemSanPhamCamDo";
            this.btnThemSanPhamCamDo.Size = new System.Drawing.Size(656, 27);
            this.btnThemSanPhamCamDo.StyleController = this.layoutControl3;
            this.btnThemSanPhamCamDo.TabIndex = 7;
            this.btnThemSanPhamCamDo.Text = "Thêm sản phẩm cầm đồ";
            this.btnThemSanPhamCamDo.Click += new System.EventHandler(this.btnThemSanPhamCamDo_Click);
            // 
            // grcSanPhamCamDo
            // 
            this.grcSanPhamCamDo.DataSource = this.sanPhamCamDoBindingSource;
            this.grcSanPhamCamDo.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamCamDo.Location = new System.Drawing.Point(678, 16);
            this.grcSanPhamCamDo.MainView = this.grvSanPhamCamDo;
            this.grcSanPhamCamDo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grcSanPhamCamDo.MenuManager = this.barManager1;
            this.grcSanPhamCamDo.Name = "grcSanPhamCamDo";
            this.grcSanPhamCamDo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDeleteSanPhamCamDo});
            this.grcSanPhamCamDo.Size = new System.Drawing.Size(646, 115);
            this.grcSanPhamCamDo.TabIndex = 6;
            this.grcSanPhamCamDo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamCamDo});
            // 
            // sanPhamCamDoBindingSource
            // 
            this.sanPhamCamDoBindingSource.DataSource = typeof(DTO.Models.SanPhamCamDo);
            // 
            // grvSanPhamCamDo
            // 
            this.grvSanPhamCamDo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCamVang,
            this.colTenMonHang,
            this.colCanNang,
            this.gridColumn1});
            this.grvSanPhamCamDo.GridControl = this.grcSanPhamCamDo;
            this.grvSanPhamCamDo.Name = "grvSanPhamCamDo";
            this.grvSanPhamCamDo.OptionsBehavior.ReadOnly = true;
            this.grvSanPhamCamDo.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamCamDo.OptionsView.ShowGroupPanel = false;
            // 
            // colCamVang
            // 
            this.colCamVang.Caption = "Cầm vàng";
            this.colCamVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colCamVang.Name = "colCamVang";
            this.colCamVang.Visible = true;
            this.colCamVang.VisibleIndex = 0;
            // 
            // colTenMonHang
            // 
            this.colTenMonHang.Caption = "Tên hàng";
            this.colTenMonHang.FieldName = "TenMonHang";
            this.colTenMonHang.Name = "colTenMonHang";
            this.colTenMonHang.Visible = true;
            this.colTenMonHang.VisibleIndex = 1;
            // 
            // colCanNang
            // 
            this.colCanNang.Caption = "Cân nặng";
            this.colCanNang.FieldName = "CanNang";
            this.colCanNang.Name = "colCanNang";
            this.colCanNang.Visible = true;
            this.colCanNang.VisibleIndex = 2;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Xóa";
            this.gridColumn1.ColumnEdit = this.btnDeleteSanPhamCamDo;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            // 
            // btnDeleteSanPhamCamDo
            // 
            this.btnDeleteSanPhamCamDo.AutoHeight = false;
            this.btnDeleteSanPhamCamDo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.delete_icon_24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnDeleteSanPhamCamDo.Name = "btnDeleteSanPhamCamDo";
            this.btnDeleteSanPhamCamDo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnDeleteSanPhamCamDo.Click += new System.EventHandler(this.btnDeleteSanPhamCamDo_Click);
            // 
            // txtTenMonHang
            // 
            this.txtTenMonHang.Location = new System.Drawing.Point(103, 44);
            this.txtTenMonHang.Margin = new System.Windows.Forms.Padding(4);
            this.txtTenMonHang.MenuManager = this.barManager1;
            this.txtTenMonHang.Name = "txtTenMonHang";
            this.txtTenMonHang.Size = new System.Drawing.Size(569, 22);
            this.txtTenMonHang.StyleController = this.layoutControl3;
            this.txtTenMonHang.TabIndex = 3;
            // 
            // txtCanNang
            // 
            this.txtCanNang.Location = new System.Drawing.Point(103, 72);
            this.txtCanNang.Margin = new System.Windows.Forms.Padding(4);
            this.txtCanNang.MenuManager = this.barManager1;
            this.txtCanNang.Name = "txtCanNang";
            this.txtCanNang.Properties.Mask.EditMask = "n1";
            this.txtCanNang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCanNang.Properties.MaxLength = 8;
            this.txtCanNang.Properties.NullText = "0";
            this.txtCanNang.Size = new System.Drawing.Size(569, 22);
            this.txtCanNang.StyleController = this.layoutControl3;
            this.txtCanNang.TabIndex = 4;
            // 
            // lueCamVang
            // 
            this.lueCamVang.Location = new System.Drawing.Point(103, 16);
            this.lueCamVang.Margin = new System.Windows.Forms.Padding(4);
            this.lueCamVang.MenuManager = this.barManager1;
            this.lueCamVang.Name = "lueCamVang";
            this.lueCamVang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCamVang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HamLuongVang", "Hàm lượng vàng")});
            this.lueCamVang.Properties.NullText = "";
            this.lueCamVang.Size = new System.Drawing.Size(569, 22);
            this.lueCamVang.StyleController = this.layoutControl3;
            this.lueCamVang.TabIndex = 2;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(1340, 147);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lueCamVang;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(662, 28);
            this.layoutControlItem4.Text = "Cầm vàng";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(83, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtTenMonHang;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(662, 28);
            this.layoutControlItem5.Text = "Tên món hàng";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(83, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtCanNang;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(662, 28);
            this.layoutControlItem6.Text = "Cân nặng";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(83, 17);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.grcSanPhamCamDo;
            this.layoutControlItem9.Location = new System.Drawing.Point(662, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(652, 121);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnThemSanPhamCamDo;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(662, 37);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // grpDanhSachCamDo
            // 
            this.grpDanhSachCamDo.Controls.Add(this.layoutControl1);
            this.grpDanhSachCamDo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDanhSachCamDo.Location = new System.Drawing.Point(0, 417);
            this.grpDanhSachCamDo.Margin = new System.Windows.Forms.Padding(4);
            this.grpDanhSachCamDo.Name = "grpDanhSachCamDo";
            this.grpDanhSachCamDo.Size = new System.Drawing.Size(1344, 250);
            this.grpDanhSachCamDo.TabIndex = 2;
            this.grpDanhSachCamDo.Text = "Danh sách hợp đồng cầm đồ";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.grcDanhSachCamDo);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1340, 223);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // grcDanhSachCamDo
            // 
            this.grcDanhSachCamDo.DataSource = this.hopDongCamDoBindingSource;
            this.grcDanhSachCamDo.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grcDanhSachCamDo.Location = new System.Drawing.Point(16, 16);
            this.grcDanhSachCamDo.MainView = this.grvDanhSachCamDo;
            this.grcDanhSachCamDo.Margin = new System.Windows.Forms.Padding(4);
            this.grcDanhSachCamDo.Name = "grcDanhSachCamDo";
            this.grcDanhSachCamDo.Size = new System.Drawing.Size(1308, 191);
            this.grcDanhSachCamDo.TabIndex = 5;
            this.grcDanhSachCamDo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDanhSachCamDo});
            this.grcDanhSachCamDo.DoubleClick += new System.EventHandler(this.grcDanhSachCamDo_DoubleClick);
            // 
            // hopDongCamDoBindingSource
            // 
            this.hopDongCamDoBindingSource.DataSource = typeof(DTO.Models.HopDongCamDo);
            // 
            // grvDanhSachCamDo
            // 
            this.grvDanhSachCamDo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclMaHopDong,
            this.grclLaiSuat,
            this.grclKhachHang,
            this.grclNhanVien,
            this.colNgayBatDau,
            this.colSoTienCam,
            this.colIsKhachQuen});
            this.grvDanhSachCamDo.GridControl = this.grcDanhSachCamDo;
            this.grvDanhSachCamDo.Name = "grvDanhSachCamDo";
            this.grvDanhSachCamDo.OptionsBehavior.Editable = false;
            this.grvDanhSachCamDo.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvDanhSachCamDo.OptionsFind.AlwaysVisible = true;
            this.grvDanhSachCamDo.OptionsFind.FindDelay = 100;
            this.grvDanhSachCamDo.OptionsFind.FindNullPrompt = "";
            this.grvDanhSachCamDo.OptionsFind.ShowFindButton = false;
            this.grvDanhSachCamDo.OptionsView.ShowGroupPanel = false;
            this.grvDanhSachCamDo.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvDanhSachCamDo_CustomDrawRowIndicator);
            // 
            // grclMaHopDong
            // 
            this.grclMaHopDong.Caption = "Mã Hợp đồng";
            this.grclMaHopDong.FieldName = "MaHopDongCamDo";
            this.grclMaHopDong.Name = "grclMaHopDong";
            this.grclMaHopDong.Visible = true;
            this.grclMaHopDong.VisibleIndex = 0;
            // 
            // grclLaiSuat
            // 
            this.grclLaiSuat.Caption = "Lãi suất (%)";
            this.grclLaiSuat.DisplayFormat.FormatString = "p2";
            this.grclLaiSuat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclLaiSuat.FieldName = "LaiSuat";
            this.grclLaiSuat.Name = "grclLaiSuat";
            this.grclLaiSuat.Visible = true;
            this.grclLaiSuat.VisibleIndex = 3;
            // 
            // grclKhachHang
            // 
            this.grclKhachHang.Caption = "Khách hàng";
            this.grclKhachHang.FieldName = "HoTenKhachHang";
            this.grclKhachHang.Name = "grclKhachHang";
            this.grclKhachHang.Visible = true;
            this.grclKhachHang.VisibleIndex = 4;
            // 
            // grclNhanVien
            // 
            this.grclNhanVien.Caption = "Nhân viên";
            this.grclNhanVien.FieldName = "NhanVien.Ten";
            this.grclNhanVien.Name = "grclNhanVien";
            this.grclNhanVien.Visible = true;
            this.grclNhanVien.VisibleIndex = 5;
            // 
            // colNgayBatDau
            // 
            this.colNgayBatDau.Caption = "Ngày bắt đầu";
            this.colNgayBatDau.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayBatDau.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayBatDau.FieldName = "NgayBatDau";
            this.colNgayBatDau.Name = "colNgayBatDau";
            this.colNgayBatDau.Visible = true;
            this.colNgayBatDau.VisibleIndex = 1;
            // 
            // colSoTienCam
            // 
            this.colSoTienCam.Caption = "Số tiền";
            this.colSoTienCam.DisplayFormat.FormatString = "n0";
            this.colSoTienCam.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSoTienCam.FieldName = "SoTienCam";
            this.colSoTienCam.Name = "colSoTienCam";
            this.colSoTienCam.Visible = true;
            this.colSoTienCam.VisibleIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1340, 223);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.grcDanhSachCamDo;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1314, 197);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // chiTietCamDoBindingSource
            // 
            this.chiTietCamDoBindingSource.DataSource = typeof(DTO.Models.ChiTietCamDo);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.layoutControl3);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 243);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1344, 174);
            this.groupControl2.TabIndex = 13;
            this.groupControl2.Text = "Sản phẩm cầm đồ";
            // 
            // dxerCamVang
            // 
            this.dxerCamVang.ContainerControl = this;
            // 
            // dxerTenMonHang
            // 
            this.dxerTenMonHang.ContainerControl = this;
            // 
            // dxerSoTienCam
            // 
            this.dxerSoTienCam.ContainerControl = this;
            // 
            // dxerLaiSuat
            // 
            this.dxerLaiSuat.ContainerControl = this;
            // 
            // dxerTenKhachHang
            // 
            this.dxerTenKhachHang.ContainerControl = this;
            // 
            // dxerSoDienThoai
            // 
            this.dxerSoDienThoai.ContainerControl = this;
            // 
            // ckbKhachQuen
            // 
            this.ckbKhachQuen.Location = new System.Drawing.Point(672, 16);
            this.ckbKhachQuen.MenuManager = this.barManager1;
            this.ckbKhachQuen.Name = "ckbKhachQuen";
            this.ckbKhachQuen.Properties.Caption = "Đánh dấu là khách quen ";
            this.ckbKhachQuen.Size = new System.Drawing.Size(652, 21);
            this.ckbKhachQuen.StyleController = this.layoutControl2;
            this.ckbKhachQuen.TabIndex = 16;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.ckbKhachQuen;
            this.layoutControlItem1.Location = new System.Drawing.Point(656, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(658, 28);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // colIsKhachQuen
            // 
            this.colIsKhachQuen.Caption = "Khách quen";
            this.colIsKhachQuen.FieldName = "IsKhachQuen";
            this.colIsKhachQuen.Name = "colIsKhachQuen";
            this.colIsKhachQuen.Visible = true;
            this.colIsKhachQuen.VisibleIndex = 6;
            // 
            // frmTaoMoiHopDongCamDo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 667);
            this.Controls.Add(this.grpDanhSachCamDo);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.grpHoaDonCamDo);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmTaoMoiHopDongCamDo";
            this.Text = "Hợp đồng cầm đồ";
            this.Load += new System.EventHandler(this.frmTaoMoiHopDongCamDo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpHoaDonCamDo)).EndInit();
            this.grpHoaDonCamDo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTenKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLaiSuat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTienCam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrangThaiHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayTaoHopDong.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detNgayTaoHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTrangThaiHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNgayTaoHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteSanPhamCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenMonHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCanNang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCamVang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDanhSachCamDo)).EndInit();
            this.grpDanhSachCamDo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopDongCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chiTietCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dxerCamVang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenMonHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerSoTienCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerLaiSuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerTenKhachHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxerSoDienThoai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbKhachQuen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl grpHoaDonCamDo;
        private DevExpress.XtraEditors.GroupControl grpDanhSachCamDo;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl grcDanhSachCamDo;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDanhSachCamDo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnThem;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit txtTrangThaiHopDong;
        private DevExpress.XtraEditors.TextEdit txtSoTienCam;
        private DevExpress.XtraEditors.TextEdit txtCanNang;
        private DevExpress.XtraEditors.TextEdit txtTenMonHang;
        private DevExpress.XtraEditors.TextEdit txtMaHopDong;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutTrangThaiHopDong;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaHopDong;
        private DevExpress.XtraEditors.TextEdit txtNhanVien;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.Columns.GridColumn grclLaiSuat;
        private DevExpress.XtraGrid.Columns.GridColumn grclKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn grclNhanVien;
        private DevExpress.XtraEditors.TextEdit txtLaiSuat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraLayout.LayoutControlItem layoutNgayTaoHopDong;
        private DevExpress.XtraBars.BarButtonItem btnGiaHanHopDong;
        private DevExpress.XtraEditors.TextEdit txtSoDienThoai;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.TextEdit txtHoTenKhachHang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.BindingSource hopDongCamDoBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnThemSanPhamCamDo;
        private DevExpress.XtraGrid.GridControl grcSanPhamCamDo;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamCamDo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayBatDau;
        private System.Windows.Forms.BindingSource sanPhamCamDoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCamVang;
        private DevExpress.XtraGrid.Columns.GridColumn colTenMonHang;
        private DevExpress.XtraGrid.Columns.GridColumn colCanNang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerCamVang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTenMonHang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerSoTienCam;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerLaiSuat;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerTenKhachHang;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxerSoDienThoai;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteSanPhamCamDo;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private System.Windows.Forms.BindingSource chiTietCamDoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSoTienCam;
        private DevExpress.XtraEditors.DateEdit detNgayTaoHopDong;
        private DevExpress.XtraBars.BarButtonItem btnInLaiHopDong;
        private DevExpress.XtraBars.BarButtonItem btnThanhToanHopDong;
        private DevExpress.XtraEditors.LookUpEdit lueCamVang;
        private DevExpress.XtraEditors.CheckEdit ckbKhachQuen;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsKhachQuen;
    }
}