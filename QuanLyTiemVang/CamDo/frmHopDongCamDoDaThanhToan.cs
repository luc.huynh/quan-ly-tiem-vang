﻿using BUS;
using DevExpress.XtraBars;
using DTO.Models;
using DTO.TemporaryModel;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Services;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.CamDo
{
    public partial class frmHopDongCamDoDaThanhToan : Form
    {
        //List dưới cơ sở dữ liệu
        private List<HopDongCamDo> listHopDongCamDoDb = new List<HopDongCamDo>();

        //List tìm kiếm tại local
        private List<HopDongCamDo> listTimKiem = new List<HopDongCamDo>();

        public frmHopDongCamDoDaThanhToan()
        {
            InitializeComponent();
            
            if(Form1.nhanVien.Quyen == EmployeePermission.QUAN_LY)
            {
                btnXoa.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnXoa.Visibility = BarItemVisibility.Never;
            }
        }

        private void frmHopDongCamDoDaThanhToan_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            listHopDongCamDoDb = HopDongCamDoBUS.Instance.GetListDaThanhToan();
            grcSanPhamCamDo.DataSource = null;
            grcDanhSachCamDo.DataSource = listHopDongCamDoDb;
            grcDanhSachCamDo.RefreshDataSource();
            btnXoa.Enabled = false;
            btnXoa.Tag = null;
        }

        private void btnCapNhatDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void grcDanhSachCamDo_DoubleClick(object sender, EventArgs e)
        {
            HopDongCamDo hopDong = (HopDongCamDo)grvDanhSachCamDo.GetFocusedRow();
            if (hopDong != null)
            {
                btnXoa.Tag = hopDong;
                btnXoa.Enabled = true;
                grcSanPhamCamDo.DataSource = hopDong.SanPhamCamDoes;
            }
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            HopDongCamDo camDoView = (HopDongCamDo)btnXoa.Tag;
            if (camDoView != null)
            {
                DialogResult result = PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW);

                if (result == DialogResult.Yes)
                {
                    if (HopDongCamDoBUS.Instance.Delete(camDoView.MaHopDongCamDo))
                    {
                        PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                        RefreshData();
                    }
                    else
                    {
                        PopupService.Instance.Error(MessageError.DELETE_ERROR);
                    }
                }
            }
        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grcSanPhamCamDo.DataSource = null;
        }
    }
}
