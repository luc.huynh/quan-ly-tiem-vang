﻿namespace QuanLyTiemVang.CamDo
{
    partial class frmThongBaoThanhLyHopDong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLamMoiDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.btnInTemMotSanPham = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.grcHopDongCamDo = new DevExpress.XtraGrid.GridControl();
            this.hopDongCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvHopDongCamDo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaHopDongCamDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLaiSuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayBatDau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoTienCam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoTenKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoDienThoaiKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnThanhLyHopDong = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.colIsKhachQuen = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcHopDongCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopDongCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHopDongCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnThanhLyHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLamMoiDuLieu,
            this.btnInTemMotSanPham});
            this.barManager2.MainMenu = this.bar2;
            this.barManager2.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLamMoiDuLieu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLamMoiDuLieu
            // 
            this.btnLamMoiDuLieu.Caption = "Làm mới dữ liệu";
            this.btnLamMoiDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnLamMoiDuLieu.Id = 0;
            this.btnLamMoiDuLieu.Name = "btnLamMoiDuLieu";
            this.btnLamMoiDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLamMoiDuLieu_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControl1.Size = new System.Drawing.Size(1006, 50);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 663);
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControl2.Size = new System.Drawing.Size(1006, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 50);
            this.barDockControl3.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControl3.Size = new System.Drawing.Size(0, 613);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1006, 50);
            this.barDockControl4.Margin = new System.Windows.Forms.Padding(5);
            this.barDockControl4.Size = new System.Drawing.Size(0, 613);
            // 
            // btnInTemMotSanPham
            // 
            this.btnInTemMotSanPham.Caption = "In tem sản phẩm";
            this.btnInTemMotSanPham.Glyph = global::QuanLyTiemVang.Properties.Resources.barcode_icon;
            this.btnInTemMotSanPham.Id = 1;
            this.btnInTemMotSanPham.Name = "btnInTemMotSanPham";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1006, 613);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Danh sách hợp đồng";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.grcHopDongCamDo);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1002, 586);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // grcHopDongCamDo
            // 
            this.grcHopDongCamDo.DataSource = this.hopDongCamDoBindingSource;
            this.grcHopDongCamDo.Location = new System.Drawing.Point(16, 16);
            this.grcHopDongCamDo.MainView = this.grvHopDongCamDo;
            this.grcHopDongCamDo.MenuManager = this.barManager2;
            this.grcHopDongCamDo.Name = "grcHopDongCamDo";
            this.grcHopDongCamDo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnThanhLyHopDong});
            this.grcHopDongCamDo.Size = new System.Drawing.Size(970, 554);
            this.grcHopDongCamDo.TabIndex = 5;
            this.grcHopDongCamDo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvHopDongCamDo});
            // 
            // hopDongCamDoBindingSource
            // 
            this.hopDongCamDoBindingSource.DataSource = typeof(DTO.Models.HopDongCamDo);
            // 
            // grvHopDongCamDo
            // 
            this.grvHopDongCamDo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaHopDongCamDo,
            this.colLaiSuat,
            this.colNgayBatDau,
            this.colSoTienCam,
            this.colHoTenKhachHang,
            this.colSoDienThoaiKhachHang,
            this.colNhanVien,
            this.gridColumn1,
            this.colIsKhachQuen});
            this.grvHopDongCamDo.GridControl = this.grcHopDongCamDo;
            this.grvHopDongCamDo.Name = "grvHopDongCamDo";
            this.grvHopDongCamDo.OptionsBehavior.ReadOnly = true;
            this.grvHopDongCamDo.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvHopDongCamDo.OptionsFind.AlwaysVisible = true;
            this.grvHopDongCamDo.OptionsFind.FindDelay = 100;
            this.grvHopDongCamDo.OptionsFind.FindNullPrompt = "";
            this.grvHopDongCamDo.OptionsFind.ShowFindButton = false;
            this.grvHopDongCamDo.OptionsView.ShowGroupPanel = false;
            this.grvHopDongCamDo.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvHopDongCamDo_CustomDrawRowIndicator);
            // 
            // colMaHopDongCamDo
            // 
            this.colMaHopDongCamDo.Caption = "Mã hợp đồng";
            this.colMaHopDongCamDo.FieldName = "MaHopDongCamDo";
            this.colMaHopDongCamDo.Name = "colMaHopDongCamDo";
            this.colMaHopDongCamDo.Visible = true;
            this.colMaHopDongCamDo.VisibleIndex = 0;
            this.colMaHopDongCamDo.Width = 118;
            // 
            // colLaiSuat
            // 
            this.colLaiSuat.Caption = "Lãi suất (%)";
            this.colLaiSuat.DisplayFormat.FormatString = "p1";
            this.colLaiSuat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLaiSuat.FieldName = "LaiSuat";
            this.colLaiSuat.Name = "colLaiSuat";
            this.colLaiSuat.Visible = true;
            this.colLaiSuat.VisibleIndex = 2;
            this.colLaiSuat.Width = 118;
            // 
            // colNgayBatDau
            // 
            this.colNgayBatDau.Caption = "Ngày bắt đầu";
            this.colNgayBatDau.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNgayBatDau.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNgayBatDau.FieldName = "NgayBatDau";
            this.colNgayBatDau.Name = "colNgayBatDau";
            this.colNgayBatDau.Visible = true;
            this.colNgayBatDau.VisibleIndex = 1;
            this.colNgayBatDau.Width = 118;
            // 
            // colSoTienCam
            // 
            this.colSoTienCam.Caption = "Số tiền cầm";
            this.colSoTienCam.DisplayFormat.FormatString = "n0";
            this.colSoTienCam.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSoTienCam.FieldName = "SoTienCam";
            this.colSoTienCam.Name = "colSoTienCam";
            this.colSoTienCam.Visible = true;
            this.colSoTienCam.VisibleIndex = 3;
            this.colSoTienCam.Width = 118;
            // 
            // colHoTenKhachHang
            // 
            this.colHoTenKhachHang.Caption = "Tên khách";
            this.colHoTenKhachHang.FieldName = "HoTenKhachHang";
            this.colHoTenKhachHang.Name = "colHoTenKhachHang";
            this.colHoTenKhachHang.Visible = true;
            this.colHoTenKhachHang.VisibleIndex = 4;
            this.colHoTenKhachHang.Width = 118;
            // 
            // colSoDienThoaiKhachHang
            // 
            this.colSoDienThoaiKhachHang.Caption = "Số điện thoại";
            this.colSoDienThoaiKhachHang.FieldName = "SoDienThoaiKhachHang";
            this.colSoDienThoaiKhachHang.Name = "colSoDienThoaiKhachHang";
            this.colSoDienThoaiKhachHang.Visible = true;
            this.colSoDienThoaiKhachHang.VisibleIndex = 5;
            this.colSoDienThoaiKhachHang.Width = 132;
            // 
            // colNhanVien
            // 
            this.colNhanVien.Caption = "Nhân viên";
            this.colNhanVien.FieldName = "NhanVien.Ten";
            this.colNhanVien.Name = "colNhanVien";
            this.colNhanVien.Visible = true;
            this.colNhanVien.VisibleIndex = 6;
            this.colNhanVien.Width = 117;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Thanh lý";
            this.gridColumn1.ColumnEdit = this.btnThanhLyHopDong;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 111;
            // 
            // btnThanhLyHopDong
            // 
            this.btnThanhLyHopDong.AutoHeight = false;
            this.btnThanhLyHopDong.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuanLyTiemVang.Properties.Resources.coins_in_hand_icon, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnThanhLyHopDong.Name = "btnThanhLyHopDong";
            this.btnThanhLyHopDong.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnThanhLyHopDong.Click += new System.EventHandler(this.btnThanhLyHopDong_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1002, 586);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.grcHopDongCamDo;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(976, 560);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // colIsKhachQuen
            // 
            this.colIsKhachQuen.Caption = "Khách quen";
            this.colIsKhachQuen.FieldName = "IsKhachQuen";
            this.colIsKhachQuen.Name = "colIsKhachQuen";
            this.colIsKhachQuen.Visible = true;
            this.colIsKhachQuen.VisibleIndex = 8;
            // 
            // frmThongBaoThanhLyHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 663);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "frmThongBaoThanhLyHopDong";
            this.Text = "Danh sách hợp đồng sắp đến hạn thanh lý";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmThongBaoThanhLyHopDong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcHopDongCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopDongCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHopDongCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnThanhLyHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnLamMoiDuLieu;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarButtonItem btnInTemMotSanPham;
        private DevExpress.XtraGrid.GridControl grcHopDongCamDo;
        private System.Windows.Forms.BindingSource hopDongCamDoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grvHopDongCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn colMaHopDongCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn colLaiSuat;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayBatDau;
        private DevExpress.XtraGrid.Columns.GridColumn colSoTienCam;
        private DevExpress.XtraGrid.Columns.GridColumn colHoTenKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colSoDienThoaiKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnThanhLyHopDong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.Columns.GridColumn colIsKhachQuen;
    }
}