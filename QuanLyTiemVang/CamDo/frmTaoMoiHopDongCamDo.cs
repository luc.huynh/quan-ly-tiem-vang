﻿using BUS;
using DevExpress.XtraBars;
using DevExpress.XtraReports.UI;
using DTO.Models;
using DTO.TemporaryModel;
using QuanLyTiemVang.BluePrints;
using QuanLyTiemVang.Constants;
using QuanLyTiemVang.Constants.StringUltils;
using QuanLyTiemVang.Services;
using Share.Constant;
using Share.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyTiemVang.CamDo
{
    public partial class frmTaoMoiHopDongCamDo : Form
    {
        // List chứa danh sách cầm đồ ở cơ sở dữ liệu
        private List<HopDongCamDo> listHopDongCamDoDB = new List<HopDongCamDo>();

        // List chứa danh sách cầm đồ tìm kiếm được theo mã hóa đơn
        private List<HopDongCamDo> listCamDoTimKiem = new List<HopDongCamDo>();

        private List<SanPhamCamDo> listSanPhamCamDo = new List<SanPhamCamDo>();

        public frmTaoMoiHopDongCamDo()
        {
            InitializeComponent();
            grvDanhSachCamDo.IndicatorWidth = 70;

            if (Form1.nhanVien.Quyen == EmployeePermission.QUAN_LY)
            {
                btnXoa.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnXoa.Visibility = BarItemVisibility.Never;
            }
        }

        private void frmTaoMoiHopDongCamDo_Load(object sender, EventArgs e)
        {
            KhoiTaoView();
            SetDataSourceForAllGridControl();
            GetAllListCamDo();
        }

        private void SetDataSourceForAllGridControl()
        {
            grcDanhSachCamDo.DataSource = listHopDongCamDoDB;
            grcSanPhamCamDo.DataSource = listSanPhamCamDo;
        }

        private void GetAllListCamDo()
        {
            listHopDongCamDoDB.Clear();
            listHopDongCamDoDB.AddRange(HopDongCamDoBUS.Instance.GetListChuaThanhToan());

            grcDanhSachCamDo.RefreshDataSource();
        }

        private void SetReadOnly(bool isRead)
        {
            txtNhanVien.ReadOnly = isRead;
            lueCamVang.ReadOnly = isRead;
            txtTenMonHang.ReadOnly = isRead;
            txtCanNang.ReadOnly = isRead;
            txtSoTienCam.ReadOnly = isRead;
            txtLaiSuat.ReadOnly = isRead;
            txtHoTenKhachHang.ReadOnly = isRead;
            txtDiaChi.ReadOnly = isRead;
            txtSoDienThoai.ReadOnly = isRead;
            detNgayTaoHopDong.ReadOnly = isRead;
            txtTrangThaiHopDong.ReadOnly = isRead;
        }

        private void KhoiTaoView()
        {
            btnDeleteSanPhamCamDo.Enabled = true;
            btnInLaiHopDong.Enabled = false;
            btnThanhToanHopDong.Enabled = false;
            btnThem.Enabled = true;
            btnXoa.Enabled = false;
            btnGiaHanHopDong.Enabled = false;
            grvSanPhamCamDo.OptionsBehavior.Editable = true;

            KiHieuHamLuongVangBUS.Instance.GetLookupEdit(lueCamVang);
            SetReadOnly(false);
            ClearFormHopDong();
            ClearFormSanPhamCamDo();

            layoutTrangThaiHopDong.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            layoutNgayTaoHopDong.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        private void ClearFormHopDong()
        {
            txtMaHopDong.Text = Util.TaoMaTheoNamThangNgayGioPhutGiay();
            txtNhanVien.Text = Form1.nhanVien.Ten;
            txtSoTienCam.EditValue = null;
            txtLaiSuat.EditValue = null;
            txtHoTenKhachHang.Text = null;
            txtDiaChi.Text = null;
            txtSoDienThoai.Text = null;
        }

        private void ClearFormSanPhamCamDo()
        {
            lueCamVang.EditValue = null;
            txtTenMonHang.Text = null;
            txtCanNang.EditValue = null;
        }

        private bool IsValidHopDong()
        {
            dxerTenKhachHang.Dispose();
            dxerSoDienThoai.Dispose();
            dxerLaiSuat.Dispose();
            dxerSoTienCam.Dispose();

            bool check = true;

            if (string.IsNullOrWhiteSpace(txtHoTenKhachHang.Text))
            {
                dxerTenKhachHang.SetError(txtHoTenKhachHang, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(txtSoDienThoai.Text))
            {
                dxerSoDienThoai.SetError(txtSoDienThoai, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (txtLaiSuat.EditValue == null)
            {
                dxerLaiSuat.SetError(txtLaiSuat, ValidationUltils.INPUT_NULL);
                check = false;
            }
            else if (double.Parse(txtLaiSuat.EditValue.ToString()) < 0 || double.Parse(txtLaiSuat.EditValue.ToString()) > 100)
            {
                dxerLaiSuat.SetError(txtLaiSuat, ErrorUltils.LAI_SUAT_FAIL);
                check = false;
            }

            if (listSanPhamCamDo.Count == 0)
            {
                PopupService.Instance.Warning(ValidationUltils.DANH_SACH_SAN_PHAM_CAM_DO);
                check = false;
            }

            if (txtSoTienCam.EditValue == null)
            {
                dxerSoTienCam.SetError(txtSoTienCam, ValidationUltils.INPUT_NULL);
                check = false;
            }

            return check;
        }

        private bool IsValidSanPhamCamDo()
        {
            dxerCamVang.Dispose();
            dxerTenMonHang.Dispose();

            bool check = true;

            if (lueCamVang.EditValue == null)
            {
                dxerCamVang.SetError(lueCamVang, ValidationUltils.INPUT_NULL);
                check = false;
            }

            if (string.IsNullOrWhiteSpace(txtTenMonHang.Text))
            {
                dxerTenMonHang.SetError(txtTenMonHang, ValidationUltils.INPUT_NULL);
                check = false;
            }

            return check;
        }

        private HopDongCamDo GetHopDongCamDoOfForm()
        {
            int outValueInt;
            double outValueDouble;

            return new HopDongCamDo
            {
                MaHopDongCamDo = txtMaHopDong.Text,
                LaiSuat = double.TryParse(txtLaiSuat.EditValue.ToString(), out outValueDouble) ? outValueDouble : 0,
                SoTienCam = int.TryParse(txtSoTienCam.EditValue.ToString(), out outValueInt) ? outValueInt : 0,
                SoTienThanhLy = 0,
                HoTenKhachHang = txtHoTenKhachHang.Text,
                DiaChiKhachHang = txtDiaChi.Text,
                SoDienThoaiKhachHang = txtSoDienThoai.Text,
                MaNhanVien = Form1.nhanVien.MaNhanVien,
                TrangThaiHopDong = HopDongCamDoConstant.CHUA_THANH_TOAN,
                NgayBatDau = DateTime.Today,
                IsKhachQuen = ckbKhachQuen.Checked ? "Yes" : "No"
            };
        }

        private SanPhamCamDo GetSamPhamCamDoOfForm()
        {
            double canNang = 0;

            if (txtCanNang.EditValue != null)
            {
                canNang = double.Parse(txtCanNang.EditValue.ToString());
            }

            return new SanPhamCamDo
            {
                MaHamLuongVang = (int)lueCamVang.EditValue,
                TenMonHang = txtTenMonHang.Text,
                CanNang = canNang
            };
        }

        private void InHopDongCamDo(HopDongCamDo hopDong)
        {
            HopDongCamDoPrint hopDongIn = new HopDongCamDoPrint();

            hopDongIn.DataSource = HopDongCamDoService.Instance.GetListHopDongCamDoPrint(hopDong);
            hopDongIn.ShowPreviewDialog();
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!IsValidHopDong())
            {
                return;
            }

            var result = PopupService.Instance.Question(MessageWarning.INSERT_HOP_DONG_CAM_DO);

            if (result == DialogResult.No)
            {
                return;
            }

            HopDongCamDo hopDong = GetHopDongCamDoOfForm();

            HopDongCamDo hopDongCamDoInserted = HopDongCamDoBUS.Instance.Insert(hopDong, listSanPhamCamDo);

            if (hopDongCamDoInserted != null)
            {
                GetAllListCamDo();
                KhoiTaoView();
                listSanPhamCamDo.Clear();
                grcSanPhamCamDo.RefreshDataSource();

                InHopDongCamDo(HopDongCamDoBUS.Instance.GetHopDongById(hopDongCamDoInserted.MaHopDongCamDo));
                PopupService.Instance.Success(MessageSucces.INSERT_DATA_ROW);
            }
            else
            {
                PopupService.Instance.Error(MessageError.INSERT_ERROR);
            }

        }

        private void btnNhapLai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            KhoiTaoView();
            listSanPhamCamDo.Clear();
            grcSanPhamCamDo.RefreshDataSource();
        }

        private void grcDanhSachCamDo_DoubleClick(object sender, EventArgs e)
        {
            HopDongCamDo hopDong = (HopDongCamDo)grvDanhSachCamDo.GetFocusedRow();

            if (hopDong != null)
            {
                XemListSanPhamCamDo(hopDong);
                XemChiTietHopDong(hopDong);
            }
        }

        private void XemListSanPhamCamDo(HopDongCamDo hopDong)
        {
            ClearFormSanPhamCamDo();

            listSanPhamCamDo.Clear();
            listSanPhamCamDo.AddRange(hopDong.SanPhamCamDoes);
            grcSanPhamCamDo.RefreshDataSource();
            grvSanPhamCamDo.OptionsBehavior.Editable = false;
        }

        private void XemChiTietHopDong(HopDongCamDo hopDong)
        {
            SetReadOnly(true);
            btnGiaHanHopDong.Enabled = true;

            txtNhanVien.Text = hopDong.NhanVien.Ten;
            txtMaHopDong.Text = hopDong.MaHopDongCamDo;
            txtSoTienCam.EditValue = hopDong.SoTienCam;
            txtLaiSuat.EditValue = hopDong.LaiSuat;
            txtHoTenKhachHang.Text = hopDong.HoTenKhachHang;
            txtDiaChi.Text = hopDong.DiaChiKhachHang;
            txtSoDienThoai.Text = hopDong.SoDienThoaiKhachHang;
            if (string.IsNullOrWhiteSpace(hopDong.IsKhachQuen) || hopDong.IsKhachQuen.Equals("No"))
            {
                ckbKhachQuen.Checked = false;
            }
            else
            {
                ckbKhachQuen.Checked = true;
            }

            layoutTrangThaiHopDong.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            layoutNgayTaoHopDong.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            detNgayTaoHopDong.EditValue = hopDong.NgayBatDau;
            txtTrangThaiHopDong.Text = hopDong.TrangThaiHopDong;

            if (Form1.nhanVien.Quyen == EmployeePermission.QUAN_LY)
            {
                btnXoa.Enabled = true;
            }

            btnInLaiHopDong.Enabled = true;
            btnThem.Enabled = false;
            btnThanhToanHopDong.Enabled = true;
            btnXoa.Tag = hopDong;
            btnThanhToanHopDong.Tag = hopDong;
            btnGiaHanHopDong.Tag = hopDong;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            HopDongCamDo hopDong = (HopDongCamDo)btnXoa.Tag;

            if (hopDong != null)
            {
                DialogResult result = PopupService.Instance.Question(MessageWarning.DELETE_DATA_ROW);

                if (result == DialogResult.Yes)
                {
                    if (HopDongCamDoBUS.Instance.Delete(hopDong.MaHopDongCamDo))
                    {
                        PopupService.Instance.Success(MessageSucces.DELETE_DATA_ROW);
                        KhoiTaoView();
                        listSanPhamCamDo.Clear();
                        grcSanPhamCamDo.RefreshDataSource();
                        GetAllListCamDo();
                    }
                    else
                    {
                        PopupService.Instance.Error(MessageError.DELETE_ERROR);
                    }
                }
            }
        }

        private void btnThemSanPhamCamDo_Click(object sender, EventArgs e)
        {
            if (!IsValidSanPhamCamDo())
            {
                return;
            }

            listSanPhamCamDo.Add(GetSamPhamCamDoOfForm());
            grcSanPhamCamDo.RefreshDataSource();
            ClearFormSanPhamCamDo();
        }

        private void btnDeleteSanPhamCamDo_Click(object sender, EventArgs e)
        {
            SanPhamCamDo sanPham = (SanPhamCamDo)grvSanPhamCamDo.GetFocusedRow();

            listSanPhamCamDo.Remove(sanPham);
            grcSanPhamCamDo.RefreshDataSource();
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetAllListCamDo();
        }

        private void txtMaHopDong_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtMaHopDong.Text))
            {
                HopDongCamDo hopDong = listHopDongCamDoDB.FirstOrDefault(x => x.MaHopDongCamDo == txtMaHopDong.Text);

                if (hopDong != null)
                {
                    XemListSanPhamCamDo(hopDong);
                    XemChiTietHopDong(hopDong);
                }
                else
                {
                    KhoiTaoView();
                    listSanPhamCamDo.Clear();
                    grcSanPhamCamDo.RefreshDataSource();
                }
            }
        }

        private void grvDanhSachCamDo_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnInLaiHopDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            HopDongCamDo hopDong = (HopDongCamDo)grvDanhSachCamDo.GetFocusedRow();

            if (hopDong != null)
            {
                InHopDongCamDo(hopDong);
            }
        }

        private void btnThanhToanHopDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            HopDongCamDo hopDong = (HopDongCamDo)btnGiaHanHopDong.Tag;

            if (hopDong != null)
            {
                frmThanhToanHopDong frmThanhToan = new frmThanhToanHopDong(txtMaHopDong.Text);
                frmThanhToan.ShowDialog();

                HopDongCamDo hopDongThanhToan = HopDongCamDoBUS.Instance.GetHopDongById(hopDong.MaHopDongCamDo);

                if (hopDongThanhToan.TrangThaiHopDong.Equals(HopDongCamDoConstant.DA_THANH_TOAN))
                {
                    GetAllListCamDo();
                    KhoiTaoView();
                    listSanPhamCamDo.Clear();
                    grcSanPhamCamDo.RefreshDataSource();
                }
            }
        }

        private void txtTimKiemHopDong_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btnGiaHanHopDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            HopDongCamDo hopDong = (HopDongCamDo)btnGiaHanHopDong.Tag;

            if (hopDong != null)
            {
                frmGiaHanHopDong frmGiaHanHopDong = new frmGiaHanHopDong(Form1.nhanVien.MaNhanVien, Form1.nhanVien.Ten, txtMaHopDong.Text);
                frmGiaHanHopDong.ShowDialog();

                HopDongCamDo hopDongGiaHan = HopDongCamDoBUS.Instance.GetHopDongById(hopDong.MaHopDongCamDo);

                if (hopDongGiaHan.NgayBatDau.Date != hopDong.NgayBatDau.Date)
                {
                    InHopDongCamDo(hopDongGiaHan);
                }

                KhoiTaoView();
                listSanPhamCamDo.Clear();
                grcSanPhamCamDo.RefreshDataSource();
                GetAllListCamDo();
            }
        }
    }
}
