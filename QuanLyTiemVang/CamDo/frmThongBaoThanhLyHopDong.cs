﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO.Models;
using BUS;
using QuanLyTiemVang.Services;
using QuanLyTiemVang.Constants;
using Share.Constant;
using Share.Util;

namespace QuanLyTiemVang.CamDo
{
    public partial class frmThongBaoThanhLyHopDong : DevExpress.XtraEditors.XtraForm
    {
        List<HopDongCamDo> listHopDongCamDo = new List<HopDongCamDo>();
        List<HopDongCamDo> listCamDoTimKiem = new List<HopDongCamDo>();

        public frmThongBaoThanhLyHopDong()
        {
            InitializeComponent();
            grvHopDongCamDo.IndicatorWidth = 70;
        }

        private void frmThongBaoThanhLyHopDong_Load(object sender, EventArgs e)
        {
            grcHopDongCamDo.DataSource = listHopDongCamDo;
            LoadData();
        }

        private void LoadData()
        {
            listHopDongCamDo.Clear();
            listHopDongCamDo.AddRange(HopDongCamDoBUS.Instance.GetListThongBaoThanhLy());
            grcHopDongCamDo.RefreshDataSource();
        }

        private void btnThanhLyHopDong_Click(object sender, EventArgs e)
        {
            var dialogResult = PopupService.Instance.Question(MessageQuestion.THANH_LY_CAM_DO);

            if (dialogResult == DialogResult.No)
            {
                return;
            }

            HopDongCamDo hopDong = (HopDongCamDo)grvHopDongCamDo.GetFocusedRow();

            if (HopDongCamDoBUS.Instance.ThanhLy(hopDong.MaHopDongCamDo, Form1.nhanVien.MaNhanVien, Constant.VANG_THANH_LY_CAM_DO, Util.TaoMaTheoNamThangNgayGioPhutGiay()))
            {
                PopupService.Instance.Success(MessageSucces.THANH_LY_CAM_DO);
                LoadData();
            }
            else
            {
                PopupService.Instance.Error(MessageError.UPDATE_ERROR);
            }
        }

        private void btnLamMoiDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void grvHopDongCamDo_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }
    }
}