﻿namespace QuanLyTiemVang.CamDo
{
    partial class frmHopDongCamDoDaThanhToan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnCapNhatDuLieu = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapLai = new DevExpress.XtraBars.BarButtonItem();
            this.btnXoa = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.grpHopDongCamDoDaThanhToan = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.grcSanPhamCamDo = new DevExpress.XtraGrid.GridControl();
            this.sanPhamCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvSanPhamCamDo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenMonHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCanNang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKiHieuHamLuongVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaHopDongCamDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grcDanhSachCamDo = new DevExpress.XtraGrid.GridControl();
            this.hopDongCamDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvDanhSachCamDo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grclMaHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclCamVang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclSoTienCam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclLaiSuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grclNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoTenKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoDienThoaiKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoTienThanhLy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrangThaiHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.colIsKhachQuen = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpHopDongCamDoDaThanhToan)).BeginInit();
            this.grpHopDongCamDoDaThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopDongCamDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachCamDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnXoa,
            this.btnCapNhatDuLieu,
            this.btnNhapLai});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnCapNhatDuLieu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNhapLai, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXoa, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnCapNhatDuLieu
            // 
            this.btnCapNhatDuLieu.Caption = "Cập nhật dữ liệu";
            this.btnCapNhatDuLieu.Glyph = global::QuanLyTiemVang.Properties.Resources.update_icon;
            this.btnCapNhatDuLieu.Id = 1;
            this.btnCapNhatDuLieu.Name = "btnCapNhatDuLieu";
            this.btnCapNhatDuLieu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCapNhatDuLieu_ItemClick);
            // 
            // btnNhapLai
            // 
            this.btnNhapLai.Caption = "Nhập lại";
            this.btnNhapLai.Glyph = global::QuanLyTiemVang.Properties.Resources.draw_eraser_icon;
            this.btnNhapLai.Id = 2;
            this.btnNhapLai.Name = "btnNhapLai";
            this.btnNhapLai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapLai_ItemClick);
            // 
            // btnXoa
            // 
            this.btnXoa.Caption = "Xóa";
            this.btnXoa.Glyph = global::QuanLyTiemVang.Properties.Resources.delete_icon;
            this.btnXoa.Id = 0;
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXoa_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlTop.Size = new System.Drawing.Size(1344, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 729);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1344, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 679);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1344, 50);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 679);
            // 
            // grpHopDongCamDoDaThanhToan
            // 
            this.grpHopDongCamDoDaThanhToan.Controls.Add(this.layoutControl1);
            this.grpHopDongCamDoDaThanhToan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpHopDongCamDoDaThanhToan.Location = new System.Drawing.Point(0, 50);
            this.grpHopDongCamDoDaThanhToan.Margin = new System.Windows.Forms.Padding(4);
            this.grpHopDongCamDoDaThanhToan.Name = "grpHopDongCamDoDaThanhToan";
            this.grpHopDongCamDoDaThanhToan.Size = new System.Drawing.Size(1344, 679);
            this.grpHopDongCamDoDaThanhToan.TabIndex = 4;
            this.grpHopDongCamDoDaThanhToan.Text = "Danh sách hợp đồng đã thanh toán";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.grcSanPhamCamDo);
            this.layoutControl1.Controls.Add(this.grcDanhSachCamDo);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 25);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1340, 652);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // grcSanPhamCamDo
            // 
            this.grcSanPhamCamDo.DataSource = this.sanPhamCamDoBindingSource;
            this.grcSanPhamCamDo.Location = new System.Drawing.Point(148, 16);
            this.grcSanPhamCamDo.MainView = this.grvSanPhamCamDo;
            this.grcSanPhamCamDo.MenuManager = this.barManager1;
            this.grcSanPhamCamDo.Name = "grcSanPhamCamDo";
            this.grcSanPhamCamDo.Size = new System.Drawing.Size(1176, 214);
            this.grcSanPhamCamDo.TabIndex = 7;
            this.grcSanPhamCamDo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvSanPhamCamDo});
            // 
            // sanPhamCamDoBindingSource
            // 
            this.sanPhamCamDoBindingSource.DataSource = typeof(DTO.Models.SanPhamCamDo);
            // 
            // grvSanPhamCamDo
            // 
            this.grvSanPhamCamDo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenMonHang,
            this.colCanNang,
            this.colKiHieuHamLuongVang,
            this.colMaHopDongCamDo});
            this.grvSanPhamCamDo.GridControl = this.grcSanPhamCamDo;
            this.grvSanPhamCamDo.Name = "grvSanPhamCamDo";
            this.grvSanPhamCamDo.OptionsBehavior.Editable = false;
            this.grvSanPhamCamDo.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvSanPhamCamDo.OptionsView.ShowGroupPanel = false;
            // 
            // colTenMonHang
            // 
            this.colTenMonHang.Caption = "Tên hàng";
            this.colTenMonHang.FieldName = "TenMonHang";
            this.colTenMonHang.Name = "colTenMonHang";
            this.colTenMonHang.Visible = true;
            this.colTenMonHang.VisibleIndex = 1;
            // 
            // colCanNang
            // 
            this.colCanNang.Caption = "Cân nặng";
            this.colCanNang.FieldName = "CanNang";
            this.colCanNang.Name = "colCanNang";
            this.colCanNang.Visible = true;
            this.colCanNang.VisibleIndex = 2;
            // 
            // colKiHieuHamLuongVang
            // 
            this.colKiHieuHamLuongVang.Caption = "Cầm vàng";
            this.colKiHieuHamLuongVang.FieldName = "KiHieuHamLuongVang.HamLuongVang";
            this.colKiHieuHamLuongVang.Name = "colKiHieuHamLuongVang";
            this.colKiHieuHamLuongVang.Visible = true;
            this.colKiHieuHamLuongVang.VisibleIndex = 0;
            // 
            // colMaHopDongCamDo
            // 
            this.colMaHopDongCamDo.Caption = "Mã hợp đồng";
            this.colMaHopDongCamDo.FieldName = "MaHopDongCamDo";
            this.colMaHopDongCamDo.Name = "colMaHopDongCamDo";
            this.colMaHopDongCamDo.Visible = true;
            this.colMaHopDongCamDo.VisibleIndex = 3;
            // 
            // grcDanhSachCamDo
            // 
            this.grcDanhSachCamDo.DataSource = this.hopDongCamDoBindingSource;
            this.grcDanhSachCamDo.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grcDanhSachCamDo.Location = new System.Drawing.Point(16, 236);
            this.grcDanhSachCamDo.MainView = this.grvDanhSachCamDo;
            this.grcDanhSachCamDo.Margin = new System.Windows.Forms.Padding(4);
            this.grcDanhSachCamDo.Name = "grcDanhSachCamDo";
            this.grcDanhSachCamDo.Size = new System.Drawing.Size(1308, 400);
            this.grcDanhSachCamDo.TabIndex = 6;
            this.grcDanhSachCamDo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDanhSachCamDo});
            this.grcDanhSachCamDo.DoubleClick += new System.EventHandler(this.grcDanhSachCamDo_DoubleClick);
            // 
            // hopDongCamDoBindingSource
            // 
            this.hopDongCamDoBindingSource.DataSource = typeof(DTO.Models.HopDongCamDo);
            // 
            // grvDanhSachCamDo
            // 
            this.grvDanhSachCamDo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grclMaHopDong,
            this.grclCamVang,
            this.grclSoTienCam,
            this.grclLaiSuat,
            this.grclNhanVien,
            this.colHoTenKhachHang,
            this.colSoDienThoaiKhachHang,
            this.colSoTienThanhLy,
            this.colTrangThaiHopDong,
            this.colIsKhachQuen});
            this.grvDanhSachCamDo.GridControl = this.grcDanhSachCamDo;
            this.grvDanhSachCamDo.Name = "grvDanhSachCamDo";
            this.grvDanhSachCamDo.OptionsBehavior.Editable = false;
            this.grvDanhSachCamDo.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            this.grvDanhSachCamDo.OptionsFind.AlwaysVisible = true;
            this.grvDanhSachCamDo.OptionsFind.FindDelay = 100;
            this.grvDanhSachCamDo.OptionsFind.FindNullPrompt = "";
            this.grvDanhSachCamDo.OptionsFind.ShowFindButton = false;
            this.grvDanhSachCamDo.OptionsView.ShowGroupPanel = false;
            // 
            // grclMaHopDong
            // 
            this.grclMaHopDong.Caption = "Mã Hợp đồng";
            this.grclMaHopDong.FieldName = "MaHopDongCamDo";
            this.grclMaHopDong.Name = "grclMaHopDong";
            this.grclMaHopDong.Visible = true;
            this.grclMaHopDong.VisibleIndex = 0;
            // 
            // grclCamVang
            // 
            this.grclCamVang.Caption = "Ngày thanh lý";
            this.grclCamVang.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.grclCamVang.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.grclCamVang.FieldName = "NgayThanhLy";
            this.grclCamVang.Name = "grclCamVang";
            this.grclCamVang.Visible = true;
            this.grclCamVang.VisibleIndex = 2;
            // 
            // grclSoTienCam
            // 
            this.grclSoTienCam.Caption = "Số tiền cầm";
            this.grclSoTienCam.DisplayFormat.FormatString = "n0";
            this.grclSoTienCam.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclSoTienCam.FieldName = "SoTienCam";
            this.grclSoTienCam.Name = "grclSoTienCam";
            this.grclSoTienCam.Visible = true;
            this.grclSoTienCam.VisibleIndex = 3;
            // 
            // grclLaiSuat
            // 
            this.grclLaiSuat.Caption = "Lãi suất (%)";
            this.grclLaiSuat.DisplayFormat.FormatString = "p1";
            this.grclLaiSuat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grclLaiSuat.FieldName = "LaiSuat";
            this.grclLaiSuat.Name = "grclLaiSuat";
            this.grclLaiSuat.Visible = true;
            this.grclLaiSuat.VisibleIndex = 5;
            // 
            // grclNhanVien
            // 
            this.grclNhanVien.Caption = "Nhân viên cầm";
            this.grclNhanVien.FieldName = "NhanVien.Ten";
            this.grclNhanVien.Name = "grclNhanVien";
            this.grclNhanVien.Visible = true;
            this.grclNhanVien.VisibleIndex = 6;
            // 
            // colHoTenKhachHang
            // 
            this.colHoTenKhachHang.Caption = "Khách hàng";
            this.colHoTenKhachHang.FieldName = "HoTenKhachHang";
            this.colHoTenKhachHang.Name = "colHoTenKhachHang";
            this.colHoTenKhachHang.Visible = true;
            this.colHoTenKhachHang.VisibleIndex = 7;
            // 
            // colSoDienThoaiKhachHang
            // 
            this.colSoDienThoaiKhachHang.Caption = "SĐT";
            this.colSoDienThoaiKhachHang.FieldName = "SoDienThoaiKhachHang";
            this.colSoDienThoaiKhachHang.Name = "colSoDienThoaiKhachHang";
            this.colSoDienThoaiKhachHang.Visible = true;
            this.colSoDienThoaiKhachHang.VisibleIndex = 8;
            // 
            // colSoTienThanhLy
            // 
            this.colSoTienThanhLy.Caption = "Số tiền thanh toán";
            this.colSoTienThanhLy.DisplayFormat.FormatString = "n0";
            this.colSoTienThanhLy.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSoTienThanhLy.FieldName = "SoTienThanhLy";
            this.colSoTienThanhLy.Name = "colSoTienThanhLy";
            this.colSoTienThanhLy.Visible = true;
            this.colSoTienThanhLy.VisibleIndex = 4;
            // 
            // colTrangThaiHopDong
            // 
            this.colTrangThaiHopDong.Caption = "Trạng thái";
            this.colTrangThaiHopDong.FieldName = "TrangThaiHopDong";
            this.colTrangThaiHopDong.Name = "colTrangThaiHopDong";
            this.colTrangThaiHopDong.Visible = true;
            this.colTrangThaiHopDong.VisibleIndex = 1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1340, 652);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.grcDanhSachCamDo;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 220);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1314, 406);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.grcSanPhamCamDo;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1314, 220);
            this.layoutControlItem3.Text = "Danh sách sản phẩm";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(129, 17);
            // 
            // groupControl1
            // 
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 50);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1344, 679);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "Danh sách hợp đồng đã thanh toán";
            // 
            // colIsKhachQuen
            // 
            this.colIsKhachQuen.Caption = "Khách quen";
            this.colIsKhachQuen.FieldName = "IsKhachQuen";
            this.colIsKhachQuen.Name = "colIsKhachQuen";
            this.colIsKhachQuen.Visible = true;
            this.colIsKhachQuen.VisibleIndex = 9;
            // 
            // frmHopDongCamDoDaThanhToan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 729);
            this.Controls.Add(this.grpHopDongCamDoDaThanhToan);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmHopDongCamDoDaThanhToan";
            this.Text = "Hợp đồng đã thanh toán";
            this.Load += new System.EventHandler(this.frmHopDongCamDoDaThanhToan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpHopDongCamDoDaThanhToan)).EndInit();
            this.grpHopDongCamDoDaThanhToan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcSanPhamCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sanPhamCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSanPhamCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopDongCamDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDanhSachCamDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnCapNhatDuLieu;
        private DevExpress.XtraBars.BarButtonItem btnXoa;
        private DevExpress.XtraEditors.GroupControl grpHopDongCamDoDaThanhToan;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl grcDanhSachCamDo;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDanhSachCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn grclMaHopDong;
        private DevExpress.XtraGrid.Columns.GridColumn grclCamVang;
        private DevExpress.XtraGrid.Columns.GridColumn grclSoTienCam;
        private DevExpress.XtraGrid.Columns.GridColumn grclLaiSuat;
        private DevExpress.XtraGrid.Columns.GridColumn grclNhanVien;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource hopDongCamDoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colHoTenKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colSoDienThoaiKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colSoTienThanhLy;
        private DevExpress.XtraGrid.Columns.GridColumn colTrangThaiHopDong;
        private DevExpress.XtraGrid.GridControl grcSanPhamCamDo;
        private System.Windows.Forms.BindingSource sanPhamCamDoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSanPhamCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn colTenMonHang;
        private DevExpress.XtraGrid.Columns.GridColumn colCanNang;
        private DevExpress.XtraGrid.Columns.GridColumn colKiHieuHamLuongVang;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraBars.BarButtonItem btnNhapLai;
        private DevExpress.XtraGrid.Columns.GridColumn colMaHopDongCamDo;
        private DevExpress.XtraGrid.Columns.GridColumn colIsKhachQuen;
    }
}