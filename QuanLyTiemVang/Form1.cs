﻿using DTO.Models;
using QuanLyTiemVang.CamDo;
using QuanLyTiemVang.GiaoDich;
using QuanLyTiemVang.HAMLUONGVANG;
using QuanLyTiemVang.KHACHHANG;
using QuanLyTiemVang.LichSu;
using QuanLyTiemVang.NHACUNGCAP;
using QuanLyTiemVang.NHAPDAUNGAY;
using QuanLyTiemVang.QuanLyGiaCong;
using QuanLyTiemVang.QuanLyKho;
using QuanLyTiemVang.ThongKe;
using QuanLyTiemVang.ThuChi;
using QuanLyTiemVang.VANGDOIVANG;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace QuanLyTiemVang
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public static NhanVien nhanVien;

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        public Form1(NhanVien nv)
        {
            InitializeComponent();
            nhanVien = nv;
            CheckPermission();
        }

        private void CheckPermission()
        {
            if (nhanVien.Quyen != EmployeePermission.QUAN_LY)
            {
                btnNhanVien.Enabled = false;
            }
            else
            {
                btnNhanVien.Enabled = true;
            }

        }

        /// <summary>
        /// Kiểm tra form đã xuất hiện chưa?
        /// </summary>
        /// <param name="fType"></param>
        /// <returns>
        /// Trả về form đó khi đã xuất hiện
        /// Trả về null nếu chưa xuất hiện
        /// </returns>
        private Form CheckFormExist(Type fType)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == fType) return f;
            }
            return null;
        }

        private void btnVangDoiVang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var frm = this.CheckFormExist(typeof(frmVangDoiVang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                var f = new frmVangDoiVang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnNhanVien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmNhanVien));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                var f = new frmNhanVien();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnChuyenKho_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmQuanLyKhoCuTongHop));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmQuanLyKhoCuTongHop f = new frmQuanLyKhoCuTongHop();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnLichSuMua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmLichSuMua));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmLichSuMua f = new frmLichSuMua();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnNhapDauNgay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmBanVang));
            if (frm != null)
            {
                frm.Close();
            }
            frm = this.CheckFormExist(typeof(VANGDOIVANG.frmVangDoiVang));
            if (frm != null)
            {
                frm.Close();
            }
            frmNhapDauNgay f = new frmNhapDauNgay();
            f.ShowDialog();
        }

        private void btnLichSuBan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmLichSuBan));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmLichSuBan f = new frmLichSuBan();
                f.MdiParent = this;
                f.Show();
            }
        }
        private void btnHamLuongVang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmHamLuongVang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmHamLuongVang f = new frmHamLuongVang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnLichSuNhapKho_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmLichSuNhapKho));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmLichSuNhapKho f = new frmLichSuNhapKho();
                f.MdiParent = this;
                f.Show();
            }
        }
        private void btnKhachHang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmKhachHang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmKhachHang f = new frmKhachHang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThuMuaVang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmMuaVang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmMuaVang f = new frmMuaVang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnNhaCungCap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmQuanLyNhaCungCap));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmQuanLyNhaCungCap f = new frmQuanLyNhaCungCap();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnBanVang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmBanVang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmBanVang f = new frmBanVang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnNhapKho_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmNhapKho));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmNhapKho f = new frmNhapKho();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnQuanLyKhoMoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmQuanLyKhoMoi));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmQuanLyKhoMoi f = new frmQuanLyKhoMoi();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnQuanLyLoaiTien_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmQuanLyLoaiTien));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmQuanLyLoaiTien f = new frmQuanLyLoaiTien();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnTyGiaTien_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmLichSuTyGiaTien));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmLichSuTyGiaTien f = new frmLichSuTyGiaTien();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnQuyDoiTien_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmDoiTien));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmDoiTien f = new frmDoiTien();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnTaoMoiCamDo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmTaoMoiHopDongCamDo));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmTaoMoiHopDongCamDo f = new frmTaoMoiHopDongCamDo();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnHoaDonDaThanhToan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmHopDongCamDoDaThanhToan));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmHopDongCamDoDaThanhToan f = new frmHopDongCamDoDaThanhToan();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThongKeKhoMoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongKeKhoMoi));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongKeKhoMoi f = new frmThongKeKhoMoi();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThongKeGiaoDich_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongKeHamLuongVang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongKeHamLuongVang f = new frmThongKeHamLuongVang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnLichSuVangDoiVang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmLichSuVangDoiVang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmLichSuVangDoiVang f = new frmLichSuVangDoiVang();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnNhapNgayDauKi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmNhapNgayDauKi f = new frmNhapNgayDauKi();
            f.ShowDialog();
        }

        private void btnHopDongDenHanThanhLy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongBaoThanhLyHopDong));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongBaoThanhLyHopDong f = new frmThongBaoThanhLyHopDong();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnPhatSinhThuChi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmPhatSinhThuChi));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmPhatSinhThuChi f = new frmPhatSinhThuChi();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThongKeThuChi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongKeTienMat));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongKeTienMat f = new frmThongKeTienMat();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThongKeCamDo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongKeCamDo));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongKeCamDo f = new frmThongKeCamDo();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThongKeNgoaiTeVaThuChi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongKeNgoaiTeVaThuChi));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongKeNgoaiTeVaThuChi f = new frmThongKeNgoaiTeVaThuChi();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThongKeMuaBan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThongKeMuaBan));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThongKeMuaBan f = new frmThongKeMuaBan();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnKiemKho_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmKiemKho));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmKiemKho f = new frmKiemKho();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnQuanLyGiaCong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmPhieuGiaCong));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmPhieuGiaCong f = new frmPhieuGiaCong();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThoGiaCong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckFormExist(typeof(frmThoGiaCong));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmThoGiaCong f = new frmThoGiaCong();
                f.MdiParent = this;
                f.Show();
            }
        }
    }
}
