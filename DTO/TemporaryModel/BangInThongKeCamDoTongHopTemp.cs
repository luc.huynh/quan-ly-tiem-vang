﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class BangInThongKeCamDoTongHopTemp
    {
        public string TieuDeThongKe { get; set; }

        public string TieuChiThongKeTongTien { get; set; }

        public DateTime? ThongKeTuNgay { get; set; }

        public DateTime? ThongKeDenNgay { get; set; }

        public string HamLuongVang { get; set; }

        public double? TongTrongLuong { get; set; }

        public int? TongTien { get; set; }

        public int? TongTienLai { get; set; }
    }
}
