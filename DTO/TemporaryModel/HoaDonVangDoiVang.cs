﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class HoaDonVangDoiVang
    {
        public string MaTem { get; set; }

        public string TenSanPham { get; set; }

        public string HamLuongVang { get; set; }

        public double KhoiLuongVang { get; set; }

        public double KhoiLuongTinh { get; set; }

        public double KhoiLuongDa { get; set; }

        public int GiaMua { get; set; }

        public int GiaBan { get; set; }

        public int TienCong { get; set; }

        public int ThanhTien { get; set; }

        public string MaHoaDon { get; set; }

        public string LoaiGiaoDich { get; set; }

        public string TenKhachHang { get; set; }

        public string DiaChi { get; set; }

        public string KLVMoiTruKLVCu { get; set; }

        public int GiaBanVaMua { get; set; }

        public int ThanhTienHoaDon { get; set; }

        public int TongTienVangCu { get; set; }

        public int TongTienConLai { get; set; }

        public int TienCongThem { get; set; }

        public int TienBot { get; set; }

        public int ThuTienKhachHang { get; set; }

        public string TieuDeThuTienKhachHang { get; set; }

        public string ThuTienKhachHangBangChu { get; set; }

        public DateTime ThoiGianGiaoDich { get; set; }

        public int TongTienVangBanRa { get; set; }
    }
}
