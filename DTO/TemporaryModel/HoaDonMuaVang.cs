﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class HoaDonMuaVang
    {
        public string TenSanPham { get; set; }

        public string HamLuongVang { get; set; }

        public string KhoiLuongVang { get; set; }

        public string TiLeHot { get; set; }

        public string DonGiaMua { get; set; }

        public string ThanhTien { get; set; }

        public string MaHoaDon { get; set; }

        public string LoaiGiaoDich { get; set; }

        public string TenKhachHang { get; set; }

        public string DiaChi { get; set; }

        public string TongTien { get; set; }

        public string TongTienHoaDon { get; set; }

        public DateTime NgayGiaoDich { get; set; }

        public string TienCongThem { get; set; }

        public string TongTienHoaDonBangChu { get; set; }
    }
}
