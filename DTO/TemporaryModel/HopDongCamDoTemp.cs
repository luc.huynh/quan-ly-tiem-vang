﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class HopDongCamDoTemp
    {
        public string MaHopDongCamDo { get; set; }

        public string CamVang { get; set; }

        public string TenMonHang { get; set; }

        public string CanNangTiem { get; set; } // Cân nặng hiển thị trên phiếu của tiệm giữ

        public string CanNangKhach { get; set; } // Cân nặng hiển thị trên phiếu giao cho khách

        public double? SoTienCam { get; set; }

        public string SoTienCamBangChu { get; set; }

        public DateTime? NgayBatDau { get; set; }

        public int? SoNamHienTai { get; set; }

        public string TenKhachHang { get; set; }

        public string DiaChi { get; set; }

        public string SoDienThoai { get; set; }
    }
}
