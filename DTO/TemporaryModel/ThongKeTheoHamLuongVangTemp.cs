﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ThongKeTheoHamLuongVangTemp
    {
        public int MaHamLuongVang { get; set; }

        public string HamLuongVang { get; set; }

        public double? KhoiLuongVangDauKi { get; set; }

        public double? KhoiLuongVangMoiBanRa { get; set; }

        public double? KhoiLuongVangCuBanRa { get; set; }

        public double? KhoiLuongVangMuaVao { get; set; }

        public double? KhoiLuongVangCuoiKi { get; set; }
    }
}
