﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ChiTietCamDoView
    {
        public string MaChiTietCamDo { get; set; }

        public DateTime? NgayBatDauThang { get; set; }

        public DateTime? NgayKetThucThang { get; set; }

        public DateTime? NgayNopTienLai { get; set; }

        public int? SoTienDongLai { get; set; }

        public string MaHopDongCamDo { get; set; }

        public int? MaNhanVien { get; set; }

        public string TenNhanVien { get; set; }
    }
}
