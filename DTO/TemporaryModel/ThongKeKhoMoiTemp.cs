﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ThongKeKhoMoiTemp
    {
        public int? TongSoLuongSanPham { get; set; }

        public double? TongKhoiLuongTinh { get; set; }

        public double? TongKhoiLuongDa { get; set; }

        public double? TongKhoiLuongVang { get; set; }

        public int? TongTienCong { get; set; }
    }
}
