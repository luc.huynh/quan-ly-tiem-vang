﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class SanPhamInTem
    {
        public string MaTem { get; set; }
        
        public string KhoiLuongTinh { get; set; }

        public string KhoiLuongDa { get; set; }

        public string KhoiLuongVang { get; set; }

        public string TienCong { get; set; }

        public string TenSanPham { get; set; }

        public string HamLuongVang { get; set; }

        public string KiHieu { get; set; }

        public string KiHieuChuyenKho { get; set; }

        public string NhaCungCap { get; set; }
}
}
