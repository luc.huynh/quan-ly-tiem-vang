﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class BangInThongKeNgoaiTeTemp
    {
        public string TieuDeThongKe { get; set; }

        public string TenNgoaiTe { get; set; }

        public int? TongTienNgoaiTe { get; set; }

        public int? ThanhTienVND { get; set; }

        public DateTime? ThongKeTuNgay { get; set; }

        public DateTime? ThongKeDenNgay { get; set; }
    }
}
