﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class LookUpEditItem
    {
        public int ValueMember { get; set; }

        public string DisplayMember { get; set; }
    }
}
