﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ThuChiTienMatTemp
    {
        public int? TongThu { get; set; }

        public int? TongChi { get; set; }

        public int? ConLai { get; set; }

        public DateTime? ThongKeTuNgay { get; set; }

        public DateTime? ThongKeDenNgay { get; set; }
    }
}
