﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ThongKeVangDoiNgangTemp
    {
        public string HamLuongVang { get; set; }

        public double? TongKhoiLuongVangXuatRa { get; set; }

        public double? TongKhoiLuongVangThuVao { get; set; }
    }
}
