﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class BangInThongKeMuaBanTemp
    {
        public string TieuDeThongKe { get; set; }

        public string TieuChiThongKeHai { get; set; }

        public string TieuChiThongKeBa { get; set; }

        public DateTime? ThongKeTuNgay { get; set; }

        public DateTime? ThongKeDenNgay { get; set; }

        public string HamLuongVang { get; set; }

        public double? GiaTriThongKeHai { get; set; }

        public double? GiaTriThongKeBa { get; set; }
    }
}
