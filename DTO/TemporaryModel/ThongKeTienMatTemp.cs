﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ThongKeTienMatTemp
    {
        public int? SoThuTu { get; set; }

        public DateTime? ThongKeTuNgay { get; set; }

        public DateTime? ThongKeDenNgay { get; set; }

        public string NoiDungThongKe { get; set; }

        public int? DauKi { get; set; }

        public int? Thu { get; set; }

        public int? Chi { get; set; }

        public int? CuoiKi { get; set; }
    }
}
