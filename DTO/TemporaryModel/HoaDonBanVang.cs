﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class HoaDonBanVang
    {
        public string MaTem { get; set; }

        public string TenSanPham { get; set; }

        public string HamLuongVang { get; set; }

        public string KhoiLuongVang { get; set; }

        public string KhoiLuongTinh { get; set; }

        public string KhoiLuongDa { get; set; }

        public string GiaBan { get; set; }

        public string TienCong { get; set; }

        public string ThanhTien { get; set; }

        public string MaHoaDon { get; set; }

        public string LoaiGiaoDich { get; set; }

        public string TenKhachHang { get; set; }

        public string DiaChi { get; set; }

        public string TongTien { get; set; }

        public string TienBot { get; set; }

        public string ThanhTienHoaDon { get; set; }

        public string ThuTienKhachHangBangChu { get; set; }

        public DateTime ThoiGianGiaoDich { get; set; }
    }
}
