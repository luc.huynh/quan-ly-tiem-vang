﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class TyGiaTienHienTai
    {
        public int? MaLoaiTien { get; set; }

        public int? GiaMuaVao { get; set; }

        public int? GiaBanRa { get; set; }

        public DateTime? NgayNhap { get; set; }

        public string TenLoaiTien { get; set; }
    }
}
