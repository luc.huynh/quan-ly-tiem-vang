﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.TemporaryModel
{
    public class ThongKeKhoCuTemp
    {
        public string HamLuongVang { get; set; }

        public double? KhoiLuongVangDauKi { get; set; }

        public double? KhoiLuongVangDemDiGiaCong { get; set; }

        public double? KhoiLuongVangCuoiKi { get; set; }
    }
}
