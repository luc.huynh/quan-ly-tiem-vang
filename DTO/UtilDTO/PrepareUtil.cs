﻿using DTO.Models;
using Share.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.UtilDTO
{
    public static class PrepareUtil
    {
        public static void SanPhamPrepare(SanPham sanPham, bool isInsert = false)
        {
            if (isInsert)
            {
                sanPham.NgayNhap = DateTime.Now;
            }

            sanPham.KhoiLuongDa = Math.Round(sanPham.KhoiLuongDa, 1);
            sanPham.KhoiLuongTinh = Math.Round(sanPham.KhoiLuongTinh, 1);
            sanPham.KhoiLuongVang = Math.Round(sanPham.KhoiLuongVang, 1);
            sanPham.TiLeHot = Math.Round(sanPham.TiLeHot, 1);
            sanPham.TienCong = TienUtil.LamTronTienViet(sanPham.TienCong);
            sanPham.GiaBan = TienUtil.LamTronTienViet(sanPham.GiaBan);
        }

        public static void SanPhamKhoCuPrepare(SanPhamKhoCu sanPham, bool isInsert = false)
        {
            if (isInsert)
            {
                sanPham.NgayNhap = DateTime.Now;
            }

            sanPham.KhoiLuongDa = Math.Round(sanPham.KhoiLuongDa, 1);
            sanPham.KhoiLuongTinh = Math.Round(sanPham.KhoiLuongTinh, 1);
            sanPham.KhoiLuongVang = Math.Round(sanPham.KhoiLuongVang, 1);
            sanPham.TiLeHot = Math.Round(sanPham.TiLeHot, 1);
            sanPham.GiaMuaVao = TienUtil.LamTronTienViet(sanPham.GiaMuaVao);
            sanPham.DonGiaMuaVao = TienUtil.LamTronTienViet(sanPham.DonGiaMuaVao);
        }


    }
}
