﻿namespace DTO.Migrations
{
    using Models;
    using Share.Constant;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<DTO.Models.QuanLyTiemVangDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DTO.Models.QuanLyTiemVangDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            //SanPhamSeed(context);
            NhanVienSeed(context);
            KhachHangSeed(context);
            KiHieuSanPhamSeed(context);
            LoaiPhieuPhatSinhThuChiSeed(context);

            context.SaveChanges();
        }

        private void SanPhamSeed(QuanLyTiemVangDbContext context)
        {
            var listSanPham = new List<SanPham>
            {
                new SanPham
                {
                    MaSanPham = "A0000",
                    TenSanPham = "Test",
                    GiaBan=200000,
                    MaHamLuongVang = 1,
                    KhoiLuongDa = 0,
                    KhoiLuongTinh = 5.156,
                    KhoiLuongVang = 5.156,
                    MaTem = "00000",
                    KiHieu = "G.H",
                    HamLuongVang = "9999",
                    NgayNhap = DateTime.Today,
                    MaNhanVien = 1,
                    TienCong = 100000,
                    TiLeHot = 3.256
                }
            };

            listSanPham.ForEach(s => context.SanPhams.AddOrUpdate(p => p.MaSanPham, s));
        }

        private void NhanVienSeed(QuanLyTiemVangDbContext context)
        {
            var listNhanVien = new List<NhanVien>
            {
                new NhanVien {
                    Ten = "Admin",
                    TaiKhoan = "admin",
                    MatKhau = "34e4f75a83a899a2c13be522838c2788", // Admin@@123
                    Quyen = EmployeePermission.QUAN_LY,
                    QuyenNhapKho = EmployeePermission.NHAP_KHO_YES
                }
            };

            listNhanVien.ForEach(s => context.NhanViens.AddOrUpdate(p => p.TaiKhoan, s));
        }

        private void KhachHangSeed(QuanLyTiemVangDbContext context)
        {
            var listKhachHang = new List<KhachHang>
            {
                new KhachHang
                {
                    Ten = "Khách vãng lai",
                    DiemTichLuy = 0
                }
            };

            listKhachHang.ForEach(s => context.KhachHangs.AddOrUpdate(p => p.Ten, s));
        }

        private void HamLuongVangSeed(QuanLyTiemVangDbContext context)
        {
            var listHamLuongVang = new List<KiHieuHamLuongVang>
            {
                new KiHieuHamLuongVang
                {
                    MaHamLuongVang = 1,
                    HamLuongVang = "9999"
                }
            };

            listHamLuongVang.ForEach(s => context.KiHieuHamLuongVangs.AddOrUpdate(p => p.HamLuongVang, s));
        }

        private void KiHieuSanPhamSeed(QuanLyTiemVangDbContext context)
        {
            var listKiHieu = new List<KiHieuSanPham>
            {
                new KiHieuSanPham
                {
                    KiHieu = "G.P"
                },
                new KiHieuSanPham
                {
                    KiHieu = "G.F"
                },
                new KiHieuSanPham
                {
                    KiHieu = "C"
                },
                new KiHieuSanPham
                {
                    KiHieu = "P"
                }
            };

            listKiHieu.ForEach(s => context.KiHieuSanPhams.AddOrUpdate(p => p.KiHieu, s));
        }

        private void LoaiPhieuPhatSinhThuChiSeed(QuanLyTiemVangDbContext context)
        {
            var listLoaiPhieu = new List<LoaiPhieuPhatSinhThuChi>
            {
                new LoaiPhieuPhatSinhThuChi
                {
                    TenLoaiPhieu = "Phiếu thu"
                },
                new LoaiPhieuPhatSinhThuChi
                {
                    TenLoaiPhieu = "Phiếu chi"
                }
            };

            listLoaiPhieu.ForEach(s => context.LoaiPhieuPhatSinhThuChis.AddOrUpdate(p => p.TenLoaiPhieu, s));
        }
    }
}
