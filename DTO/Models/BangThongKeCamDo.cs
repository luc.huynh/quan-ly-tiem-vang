﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class BangThongKeCamDo
    {
        [Key]
        public int MaBangThongKeCamDo { get; set; }

        public DateTime NgayThongKe { get; set; }

        public double TongTrongLuongCamDo { get; set; }

        public int TongTienCamDo { get; set; }

        public double TongTrongLuongThanhToan { get; set; }

        public int TongTienThanhToan { get; set; }

        public int TongTienLaiThanhToan { get; set; }

        public double TongTrongLuongThanhLy { get; set; }

        public int TongTienLaiThanhLy { get; set; }

        public int TongTienThanhLy { get; set; }

        public double TongTrongLuongGiaHan { get; set; }

        public int TongTienCamGiaHan { get; set; }

        public int TongTienLaiGiaHan { get; set; }

        public int MaHamLuongVang { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }
    }
}
