﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class KhoiLuongGiaCong
    {
        [Key]
        public int MaKhoiLuongGiaCong { get; set; }

        public DateTime NgayChuyen { get; set; }

        public double KhoiLuongTinhTieuHao { get; set; }

        public double KhoiLuongDaTieuHao { get; set; }

        public double KhoiLuongVangTieuHao { get; set; }

        public int MaNhanVien { get; set; }

        public int MaKhoCu { get; set; }

        public virtual KhoCu KhoCu { get; set; }

        public virtual NhanVien NhanVien { get; set; }
    }
}
