﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class SanPhamCamDo
    {
        [Key]
        public int MaSanPhamCamDo { get; set; }

        public DateTime NgayNgap { get; set; }

        public string TenMonHang { get; set; }

        public double CanNang { get; set; }

        public int MaHamLuongVang { get; set; } // Cầm vàng

        public string MaHopDongCamDo { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }

        public virtual HopDongCamDo HopDongCamDo { get; set; }
    }
}
