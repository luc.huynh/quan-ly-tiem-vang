﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class NhanVien
    {
        [Key]
        public int MaNhanVien { get; set; }

        public string HoLot { get; set; }

        public string Ten { get; set; }

        public string GioiTinh { get; set; }

        public string ChungMinhNhanDan { get; set; }

        public string SoDienThoai { get; set; }

        public DateTime? NgaySinh { get; set; }

        public string DiaChi { get; set; }

        public string TaiKhoan { get; set; }

        public string MatKhau { get; set; }

        public string Quyen { get; set; }

        public string QuyenNhapKho { get; set; }

        public virtual ICollection<LichSuBan> LichSuBans { get; set; }

        public virtual ICollection<LichSuMua> LichSuMuas { get; set; }

        public virtual ICollection<HopDongCamDo> HopDongCamDoes { get; set; }

        public virtual ICollection<KhoiLuongGiaCong> KhoiLuongGiaCongs { get; set; }

        public virtual ICollection<LichSuGiaVang> LichSuGiaVangs { get; set; }

        public virtual ICollection<SanPhamKhoCu> SanPhamKhoCues { get; set; }

        public virtual ICollection<SanPham> SanPhams { get; set; }

        public virtual ICollection<PhieuPhatSinhThuChi> PhieuPhatSinhThuChis { get; set; }

        public virtual ICollection<LichSuDoiNgoaiTe> LichSuDoiNgoaiTes { get; set; }
    }
}
