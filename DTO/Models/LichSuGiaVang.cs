﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LichSuGiaVang
    {
        [Key]
        public int MaLichSuGiaVang { get; set; }

        public DateTime NgayBan { get; set; }

        public int GiaVangBanRa { get; set; }

        public int GiaVangMuaVao { get; set; }

        public int MaNhanVien { get; set; }

        public int MaHamLuongVang { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }
    }
}
