﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LoaiPhieuPhatSinhThuChi
    {
        [Key]
        public int MaLoaiPhieuPhatSinhThuChi { get; set; }

        public string TenLoaiPhieu { get; set; }

        public virtual ICollection<PhieuPhatSinhThuChi> PhieuPhatSinhThuChis { get; set; }
}
}
