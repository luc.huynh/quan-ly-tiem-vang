﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LichSuBan
    {
        [Key]
        public string MaLichSuBan { get; set; }

        public DateTime NgayGiaoDich { get; set; }

        public int TongTienVang { get; set; }

        public int TienBot { get; set; }

        public int ThanhTienHoaDon { get; set; }

        public int MaNhanVien { get; set; }

        public int MaKhachHang { get; set; }

        public virtual ICollection<SanPham> SanPhams { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual KhachHang KhachHang { get; set; }

        public virtual ICollection<LichSuDoiVang> LichSuDoiVangs { get; set; }
    }
}
