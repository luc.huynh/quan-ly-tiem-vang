﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class SanPham
    {
        [Key]
        public string MaSanPham { get; set; }

        public string TenSanPham { get; set; }

        public string MaTem { get; set; }

        public int GiaBan { get; set; }

        public int TienCong { get; set; }

        public DateTime NgayNhap { get; set; }

        public string HamLuongVang { get; set; }

        public double TiLeHot { get; set; }

        public double KhoiLuongTinh { get; set; }

        public double KhoiLuongDa { get; set; }

        public double KhoiLuongVang { get; set; }

        public string KiHieu { get; set; }

        public bool IsSanPhamChuyenKho { get; set; }

        public int? MaNhaCungCap { get; set; }

        public string MaLichSuBan { get; set; }

        public int MaNhanVien { get; set; } // nhân viên nhập kho

        public int MaHamLuongVang { get; set; }

        public virtual LichSuBan LichSuBan { get; set; }

        public virtual NhaCungCap NhaCungCap { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }
    }
}
