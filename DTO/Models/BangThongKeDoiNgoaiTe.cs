﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class BangThongKeDoiNgoaiTe
    {
        [Key]
        public int MaBangThongKeDoiNgoaiTe { get; set; }

        public DateTime NgayThongKe { get; set; }

        public int TongTienNgoaiTeThuVao { get; set; }

        public int TongTienVietDoiRa { get; set; }

        public int TongTienNgoaiTeDoiRa { get; set; }

        public int TongTienVietThuVao { get; set; }

        public int MaLoaiTien { get; set; }

        public virtual LoaiTien LoaiTien { get; set; }
    }
}
