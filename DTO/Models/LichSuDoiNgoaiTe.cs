﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LichSuDoiNgoaiTe
    {
        [Key]
        public int MaLichSuDoiNgoaiTe { get; set; }

        public DateTime NgayGiaoDich { get; set; }

        public int SoTienDoi { get; set; } // Tiền đổi

        public int SoTienKhachNhan { get; set; } // Tiền khách nhận

        public string DoiTheoChieuThuanHoacNguoc { get; set; } // Đổi tiền theo chiều thuận hoặc chiều ngược (THUAN: ngoại tệ đổi sang tiền Việt, NGUOC: tiền Việt đổi sang ngoại tệ)

        public int MaLichSuTyGiaTien { get; set; }

        public int MaNhanVien { get; set; }

        public virtual LichSuTyGiaTien LichSuTyGiaTien { get; set; }

        public virtual NhanVien NhanVien { get; set; }
    }
}
