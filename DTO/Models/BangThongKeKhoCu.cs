﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class BangThongKeKhoCu
    {
        [Key]
        public int MaBangThongKeKhoCu { get; set; }

        public DateTime NgayThongKe { get; set; }

        public double KhoiLuongVangDauKi { get; set; }

        public double KhoiLuongVangNhapVao { get; set; }

        public double KhoiLuongVangCuMuaVao { get; set; }

        public double KhoiLuongVangDoiNgangVao { get; set; }

        public double KhoiLuongVangDemDiGiaCong { get; set; }

        public double KhoiLuongVangCuoiKi { get; set; }

        public int TongTienVangMuaVao { get; set; }

        public int MaHamLuongVang { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }
    }
}
