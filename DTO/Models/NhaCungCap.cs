﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class NhaCungCap
    {
        [Key]
        public int MaNhaCungCap { get; set; }

        public string TenNhaCungCap { get; set; }

        public string DiaChi { get; set; }

        public virtual ICollection<SanPhamKhoCu> SanPhamKhoCues { get; set; }

        public virtual ICollection<SanPham> SanPhams { get; set; }
    }
}
