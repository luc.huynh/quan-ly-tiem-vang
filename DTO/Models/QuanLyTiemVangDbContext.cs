﻿using DTO.Seed;
using System.Data.Entity;

namespace DTO.Models
{
    public class QuanLyTiemVangDbContext : DbContext
    {
        public QuanLyTiemVangDbContext() : base("QuanLyTiemVang")
        {
        }

        public DbSet<KhachHang> KhachHangs { get; set; }

        public DbSet<LichSuBan> LichSuBans { get; set; }

        public DbSet<LichSuMua> LichSuMuas { get; set; }

        public DbSet<NhanVien> NhanViens { get; set; }

        public DbSet<SanPham> SanPhams { get; set; }

        public DbSet<LichSuDoiVang> LichSuDoiVangs { get; set; }

        public DbSet<LichSuGiaVang> LichSuGiaVangs { get; set; }

        public DbSet<KiHieuHamLuongVang> KiHieuHamLuongVangs { get; set; }

        public DbSet<NhaCungCap> NhaCungCaps { get; set; }

        public DbSet<HopDongCamDo> HopDongCamDoes { get; set; }

        public DbSet<ChiTietCamDo> ChiTietCamDoes { get; set; }

        public DbSet<LichSuTyGiaTien> LichSuTyGiaTiens { get; set; }

        public DbSet<LoaiTien> LoaiTiens { get; set; }

        public DbSet<KiHieuSanPham> KiHieuSanPhams { get; set; }

        public DbSet<KhoCu> KhoCues { get; set; }

        public DbSet<SanPhamKhoCu> SanPhamKhoCues { get; set; }

        public DbSet<KhoiLuongGiaCong> KhoiLuongGiaCongs { get; set; }

        public DbSet<NgayDauKiThongKe> NgayDauKiThongKes { get; set; }

        public DbSet<BangThongKeKhoCu> BangThongKeKhoCues { get; set; }

        public DbSet<BangThongKeTheoHamLuongVang> BangThongKeTheoHamLuongVangs { get; set; }

        public DbSet<SanPhamCamDo> SanPhamCamDoes { get; set; }

        public DbSet<LichSuDoiNgoaiTe> LichSuDoiNgoaiTes { get; set; }

        public DbSet<LoaiPhieuPhatSinhThuChi> LoaiPhieuPhatSinhThuChis { get; set; }

        public DbSet<PhieuPhatSinhThuChi> PhieuPhatSinhThuChis { get; set; }

        public DbSet<BangThongKeTienMat> BangThongKeTienMats { get; set; }

        public DbSet<BangThongKeDoiNgoaiTe> BangThongKeDoiNgoaiTes { get; set; }

        public DbSet<BangThongKeCamDo> BangThongKeCamDoes { get; set; }

        public DbSet<ThoGiaCong> ThoGiaCongs { get; set; }

        public DbSet<PhieuGiaCong> PhieuGiaCongs { get; set; }
        
        public DbSet<ChiTietPhieuGiaCong> ChiTietPhieuGiaCongs { get; set; }
    }
}
