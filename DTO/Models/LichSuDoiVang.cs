﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LichSuDoiVang
    {
        [Key]
        public string MaLichSuDoiVang { get; set; }
        
        public double KhoiLuongVangMoiKhongCoMaChuyenKho { get; set; }

        public double KhoiLuongVangMoiCoMaChuyenKho { get; set; }

        public double KhoiLuongVangMoi { get; set; }

        public double KhoiLuongVangCu { get; set; }

        public double KLVMoiTruKLVCu { get; set; } // vàng đổi ngang

        public int ThanhTienVangMoi { get; set; }

        public int TongTienVangCu { get; set; }

        public int GiaBan { get; set; }

        public int TienConLai { get; set; }

        public int TienCongThem { get; set; }

        public int TienBot { get; set; }

        public int TongTienVangBanRa { get; set; }

        public int ThuTienKhachHang { get; set; }

        public DateTime NgayGiaoDich { get; set; }

        public string MaLichSuMua { get; set; }

        public string MaLichSuBan { get; set; }

        public virtual LichSuMua LichSuMua { get; set; }

        public virtual LichSuBan LichSuBan { get; set; }
    }
}
