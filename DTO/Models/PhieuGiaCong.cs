﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class PhieuGiaCong
    {
        [Key]
        public int MaPhieuGiaCong { get; set; }

        public DateTime NgayGiao { get; set; }

        public double TiLeHotBanGiao { get; set; }

        public double KhoiLuongTongBanGiao { get; set; }

        public double KhoiLuongVangBanGiao { get; set; }

        public double KhoiLuongDaBanGiao { get; set; }

        public double TiLeHotTruHao { get; set; }

        public double KhoiLuongTongTruHao { get; set; }

        public double KhoiLuongVangTruHao { get; set; }

        public double KhoiLuongDaTruHao { get; set; }

        public DateTime? NgayKetThuc { get; set; }

        public int MaThoGiaCong { get; set; }

        public int MaHamLuongVang { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }

        public virtual ThoGiaCong ThoGiaCong { get; set; }

        public virtual ICollection<ChiTietPhieuGiaCong> ChiTietPhieuGiaCongs { get; set; }
    }
}
