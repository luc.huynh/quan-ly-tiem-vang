﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LoaiTien
    {
        [Key]
        public int MaLoaiTien { get; set; }

        public string TenLoaiTien { get; set; }

        public virtual ICollection<LichSuTyGiaTien> LichSuTyGiaTiens { get; set; }
    }
}
