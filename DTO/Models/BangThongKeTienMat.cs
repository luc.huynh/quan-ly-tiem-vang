﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class BangThongKeTienMat
    {
        [Key]
        public int MaBangThongKeTienMat { get; set; }

        public DateTime NgayThongKe { get; set; }

        public int SoDuDauKi { get; set; }

        public int TongTienVangBanRa { get; set; }

        public int TongTienVangMuaVao { get; set; }

        public int TongTienCong { get; set; }

        public int TongTienBot { get; set; }

        public int TongChiTienCamDo { get; set; }

        public int TongThuTienLaiCamDo { get; set; }

        public int TongThuNoGocCamDo { get; set; }

        public int TongTienDoiNgoaiTeThu { get; set; }

        public int TongTienDoiNgoaiTeChi { get; set; }

        public int TongPhatSinhKhacThuVao { get; set; }

        public int TongPhatSinhKhacChiRa { get; set; }

        public int SoDuCuoiKi { get; set; }

        public int TongTienThuVao { get; set; }

        public int TongTienChiRa { get; set; }
    }
}
