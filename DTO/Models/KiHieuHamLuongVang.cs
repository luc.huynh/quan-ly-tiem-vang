﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class KiHieuHamLuongVang
    {
        [Key]
        public int MaHamLuongVang { get; set; }

        public string HamLuongVang { get; set; }

        public virtual ICollection<LichSuGiaVang> LichSuGiaVangs { get; set; }

        public virtual ICollection<SanPhamKhoCu> SanPhamKhoCues { get; set; }

        public virtual ICollection<BangThongKeKhoCu> BangThongKeKhoCues { get; set; }

        public virtual ICollection<BangThongKeTheoHamLuongVang> BangThongKeTheoHamLuongVangs { get; set; }

        public virtual ICollection<BangThongKeCamDo> BangThongKeCamDoes { get; set; }

        public virtual ICollection<SanPhamCamDo> SanPhamCamDoes { get; set; }

        public virtual ICollection<SanPham> SanPhams { get; set; }
    }
}
