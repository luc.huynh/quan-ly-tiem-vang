﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class KhachHang
    {
        [Key]
        public int MaKhachHang { get; set; }

        public string HoLot { get; set; }

        public string Ten { get; set; }

        public string GioiTinh { get; set; }

        public string DiaChi { get; set; }

        public string ChungMinhNhanDan { get; set; }

        public string SoDienThoai { get; set; }

        public DateTime? NgaySinh { get; set; }

        public int DiemTichLuy { get; set; }

        public virtual ICollection<LichSuMua> LichSuMuas { get; set; }

        public virtual ICollection<LichSuBan> LichSuBans { get; set; }
    }
}
