﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class KhoCu
    {
        [Key]
        public int MaKhoCu { get; set; }

        public double TongKhoiLuongTinh { get; set; }

        public double TongKhoiLuongVang { get; set; }

        public double TongKhoiLuongDa { get; set; }

        public int MaHamLuongVang { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }

        public virtual ICollection<KhoiLuongGiaCong> KhoiLuongGiaCongs { get; set; }
    }
}
