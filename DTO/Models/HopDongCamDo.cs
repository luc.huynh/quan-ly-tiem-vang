﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class HopDongCamDo
    {
        [Key]
        public string MaHopDongCamDo { get; set; }

        public double LaiSuat { get; set; }

        public string TrangThaiHopDong { get; set; }

        public DateTime NgayInMoiHopDong { get; set; } // Ngày in hợp đồng lần đầu tiên

        public DateTime NgayBatDau { get; set; } // Ngày bắt đầu sau mỗi lần gia hạn thành công

        public DateTime? NgayThanhLy { get; set; }

        public int SoTienThanhLy { get; set; }

        public int SoTienCam { get; set; }

        public string HoTenKhachHang { get; set; }

        public string DiaChiKhachHang { get; set; }

        public string SoDienThoaiKhachHang { get; set; }

        public string IsKhachQuen { get; set; }

        public int MaNhanVien { get; set; }
        
        public virtual NhanVien NhanVien { get; set; }

        public virtual ICollection<ChiTietCamDo> ChiTietCamDoes { get; set; }

        public virtual ICollection<SanPhamCamDo> SanPhamCamDoes { get; set; }
    }
}
