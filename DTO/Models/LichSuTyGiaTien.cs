﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class LichSuTyGiaTien
    {
        [Key]
        public int MaLichSuTyGiaTien { get; set; }
        
        public int GiaMuaVao { get; set; }

        public int GiaBanRa { get; set; }

        public DateTime NgayNhap { get; set; }

        public int MaLoaiTien { get; set; }

        public virtual LoaiTien LoaiTien { get; set; }

        public virtual ICollection<LichSuDoiNgoaiTe> LichSuDoiNgoaiTes { get; set; }
    }
}
