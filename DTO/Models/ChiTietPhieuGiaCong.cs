﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class ChiTietPhieuGiaCong
    {
        [Key]
        public int MaChiTietPhieuGiaCong { get; set; }

        public DateTime NgayHoanTra { get; set; }

        public double TiLeHotHoanThanh { get; set; }

        public double KhoiLuongTongHoanThanh { get; set; }

        public double KhoiLuongVangHoanThanh { get; set; }

        public double KhoiLuongDaHoanThanh { get; set; }

        public double TiLeHotTruHao { get; set; }

        public double KhoiLuongTongTruHao { get; set; }

        public double KhoiLuongVangTruHao { get; set; }

        public double KhoiLuongDaTruHao { get; set; }

        public string LyDoTruHao { get; set; }

        public int MaPhieuGiaCong { get; set; }

        public virtual PhieuGiaCong PhieuGiaCong { get; set; }
    }
}
