﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class ChiTietCamDo
    {
        [Key]
        public string MaChiTietCamDo { get; set; }

        public DateTime NgayBatDauThang { get; set; }

        public DateTime NgayNopTienLai { get; set; }

        public int SoTienDongLai { get; set; }

        public string MaHopDongCamDo { get; set; }

        public int MaNhanVien { get; set; }

        public virtual HopDongCamDo HopDongCamDo { get; set; }

        public virtual NhanVien NhanVien { get; set; }
    }
}
