﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class NgayDauKiThongKe
    {
        [Key]
        public int MaNgayDauKi { get; set; }

        public DateTime NgayDauKi { get; set; }

        public int SoTienDauKi { get; set; }

        public DateTime NgayChinhSua { get; set; }
    }
}
