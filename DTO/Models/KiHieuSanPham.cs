﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class KiHieuSanPham
    {
        [Key]
        public int MaKiHieuSanPham { get; set; }

        public string KiHieu { get; set; }
    }
}
