﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class PhieuPhatSinhThuChi
    {
        [Key]
        public int MaPhieuPhatSinhThuChi { get; set; }

        public DateTime NgayXuatPhieu { get; set; }

        public int SoTien { get; set; }

        public string HoVaTen { get; set; }

        public string LyDo { get; set; }

        public string GhiChu { get; set; }

        public int MaNhanVien { get; set; }

        public int MaLoaiPhieuPhatSinhThuChi { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual LoaiPhieuPhatSinhThuChi LoaiPhieuPhatSinhThuChi { get; set; }
}
}
