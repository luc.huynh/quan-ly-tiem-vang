﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class BangThongKeTheoHamLuongVang
    {
        [Key]
        public int MaBangThongKeTheoHamLuongVang { get; set; }

        public DateTime NgayThongKe { get; set; }

        public double KhoiLuongVangDauKi { get; set; }

        public double KhoiLuongVangNhapVao { get; set; }

        public double KhoiLuongVangMoiBanRa { get; set; }

        public double KhoiLuongVangDoiNgangRa { get; set; }

        public double KhoiLuongVangCuBanRa { get; set; }

        public double TongKhoiLuongVangBanRa { get; set; }

        public double KhoiLuongVangMuaVao { get; set; }

        public double KhoiLuongVangCuoiKi { get; set; }

        public int TongTienVangMoiBanRa { get; set; }

        public int TongTienVangCuBanRa { get; set; }

        public int TongTienVangBanRa { get; set; }

        public int MaHamLuongVang { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }
    }
}
