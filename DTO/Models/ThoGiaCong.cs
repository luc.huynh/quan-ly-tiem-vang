﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class ThoGiaCong
    {
        [Key]
        public int MaThoGiaCong { get; set; }

        public string HoTen { get; set; }

        public string ChungMinhNhanDan { get; set; }

        public string SoDienThoai { get; set; }

        public string DiaChi { get; set; }

        public virtual ICollection<PhieuGiaCong> PhieuGiaCongs { get; set; }
    }
}
