﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class SanPhamKhoCu
    {
        [Key]
        public int MaSanPhamKhoCu { get; set; }

        public string TenSanPham { get; set; }

        public DateTime NgayNhap { get; set; }

        public double TiLeHot { get; set; }

        public double KhoiLuongTinh { get; set; }
        
        public double KhoiLuongDa { get; set; }

        public double KhoiLuongVang { get; set; }

        public int LoaiVangMuaVao { get; set; }

        public int DonGiaMuaVao { get; set; }

        public int GiaMuaVao { get; set; }

        public int MaHamLuongVang { get; set; }

        public int? MaNhaCungCap { get; set; }

        public int MaNhanVien { get; set; }

        public string MaLichSuMua { get; set; }

        public virtual KiHieuHamLuongVang KiHieuHamLuongVang { get; set; }

        public virtual NhaCungCap NhaCungCap { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual LichSuMua LichSuMua { get; set; }
    }
}
