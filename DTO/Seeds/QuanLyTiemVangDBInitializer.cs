﻿using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Seed
{
    public class QuanLyTiemVangDBInitializer : CreateDatabaseIfNotExists<QuanLyTiemVangDbContext>
    {
        protected override void Seed(QuanLyTiemVangDbContext context)
        {
            IList<NhanVien> defaultNhanViens = new List<NhanVien>();

            defaultNhanViens.Add(new NhanVien()
            {
                Ten = "Admin",
                TaiKhoan = "admin",
                MatKhau = "34e4f75a83a899a2c13be522838c2788", // Admin@@123
                Quyen = EmployeePermission.QUAN_LY,
                QuyenNhapKho = EmployeePermission.NHAP_KHO_YES
            });

            context.NhanViens.AddRange(defaultNhanViens);

            IList<KhachHang> defaultKhachHangs = new List<KhachHang>();

            defaultKhachHangs.Add(new KhachHang()
            {
                Ten = "Khách vãng lai",
                DiemTichLuy = 0
            });

            context.KhachHangs.AddRange(defaultKhachHangs);

            base.Seed(context);
        }
    }
}
