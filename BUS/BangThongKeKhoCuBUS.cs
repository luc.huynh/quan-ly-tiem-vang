﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BangThongKeKhoCuBUS
    {
        private static BangThongKeKhoCuBUS instance;

        public static BangThongKeKhoCuBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new BangThongKeKhoCuBUS();
                return instance;
            }
        }

        public bool TinhToanThongKe()
        {
            return BangThongKeKhoCuDAO.Instance.TinhToanThongKe();
        }

        public List<BangThongKeKhoCu> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            return BangThongKeKhoCuDAO.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        public List<BangThongKeKhoCu> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            return BangThongKeKhoCuDAO.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
        }
    }
}
