﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KiHieuSanPhamBUS
    {
        private static KiHieuSanPhamBUS instance;

        public static KiHieuSanPhamBUS Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KiHieuSanPhamBUS();
                }
                return instance;
            }
        }

        public List<KiHieuSanPham> GetListAllKiHieuSanPham()
        {
            return KiHieuSanPhamDAO.Instance.GetListAllKiHieuSanPham();
        }

        public void GetLookupEdit(LookUpEdit lookUpEdit)
        {
            lookUpEdit.Properties.DataSource = KiHieuSanPhamDAO.Instance.GetListAllKiHieuSanPham();
            lookUpEdit.Properties.DisplayMember = "KiHieu";
            lookUpEdit.Properties.ValueMember = "MaKiHieuSanPham";
        }
    }
}
