﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BangThongKeTheoHamLuongVangBUS
    {
        private static BangThongKeTheoHamLuongVangBUS instance;

        public static BangThongKeTheoHamLuongVangBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new BangThongKeTheoHamLuongVangBUS();
                return instance;
            }
        }

        public bool TinhToanThongKe()
        {
            return BangThongKeTheoHamLuongVangDAO.Instance.TinhToanThongKe();
        }

        public List<BangThongKeTheoHamLuongVang> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            return BangThongKeTheoHamLuongVangDAO.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        public List<BangThongKeTheoHamLuongVang> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            return BangThongKeTheoHamLuongVangDAO.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
        }

    }
}
