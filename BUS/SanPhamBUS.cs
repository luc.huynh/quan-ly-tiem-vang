﻿using DevExpress.XtraGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO.Models;
using DevExpress.XtraEditors;
using DTO.TemporaryModel;
using Share.Constant;

namespace BUS
{
    public class SanPhamBUS
    {
        private static SanPhamBUS instance;

        public static SanPhamBUS Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SanPhamBUS();
                }
                return instance;
            }
        }

        public List<SanPham> GetListSanPhamChuaBan()
        {
            return SanPhamDAO.Instance.GetListSanPhamChuaBan();
        }

        private string convertText(string a)
        {
            if (string.IsNullOrWhiteSpace(a))
            {
                return "";
            }
            return a;
        }

        public SanPham ChuyenThongTinFormThanhSanPham(
            string maTem,
            string tenSanPham,
            string hamLuongVang,
            double khoiLuongVang,
            double tiLeHot,
            string ngayNhap,
            string moTa,
            int maNhaCungCap,
            double khoiLuongDa,
            double khoiLuongTinh,
            string kiHieu,
            int tienCong)
        {
            int? maNhaCungCapTemp = null;

            if (maNhaCungCap != Constant.BAD_VALUE)
            {
                maNhaCungCapTemp = maNhaCungCap;
            }

            return new SanPham
            {
                TenSanPham = tenSanPham,
                HamLuongVang = hamLuongVang,
                KhoiLuongVang = khoiLuongVang,
                TienCong = tienCong,
                TiLeHot = tiLeHot,
                MaNhaCungCap = maNhaCungCapTemp,
                KhoiLuongDa = khoiLuongDa,
                KhoiLuongTinh = khoiLuongTinh,
                KiHieu = kiHieu,
                MaTem = maTem
            };
        }

        public SanPham GetSanPhamById(string maSanPham)
        {
            return SanPhamDAO.Instance.GetSanPhamById(maSanPham);
        }

        public SanPham GetSanPhamByMaTem(string maTem)
        {
            return SanPhamDAO.Instance.GetSanPhamByMaTem(maTem);
        }

        public List<SanPham> GetListSanPhamByLichSuBan(LichSuBan lichSuBan)
        {
            return SanPhamDAO.Instance.GetListSanPhamByLichSuBan(lichSuBan);
        }

        public bool IsDuplicateMaTem(string maTem)
        {
            if (SanPhamDAO.Instance.GetSanPhamByMaTem(maTem) != null)
            {
                return true;
            }
            return false;
        }

        public bool IsDuplicateMaSanPham(string maSanPham)
        {
            if (SanPhamDAO.Instance.GetSanPhamById(maSanPham) != null)
            {
                return true;
            }
            return false;
        }

        public void GetLookupEditOfLoaiVangMuaVao(LookUpEdit lueLoaiVangMuaVao)
        {
            List<LookUpEditItem> dataSource = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = Constant.VANG_CU_DOI_NGANG,
                    DisplayMember = Constant.VANG_CU_DOI_NGANG_DISPLAY
                },
                new LookUpEditItem {
                    ValueMember = Constant.VANG_CU_MUA_VAO,
                    DisplayMember = Constant.VANG_CU_MUA_VAO_DISPLAY
                }
            };

            lueLoaiVangMuaVao.Properties.DataSource = dataSource;
            lueLoaiVangMuaVao.Properties.DisplayMember = "DisplayMember";
            lueLoaiVangMuaVao.Properties.ValueMember = "ValueMember";
        }

        public void GetLookupEditOfLoaiVangBanRa(LookUpEdit lueLoaiVangBanRa)
        {
            List<LookUpEditItem> dataSource = new List<LookUpEditItem>
            {
                new LookUpEditItem {
                    ValueMember = Constant.VANG_BAN_RA,
                    DisplayMember = Constant.VANG_BAN_RA_DISPLAY
                },
                new LookUpEditItem {
                    ValueMember = Constant.VANG_DOI_NGANG_RA,
                    DisplayMember = Constant.VANG_DOI_NGANG_RA_DISPLAY
                }
            };

            lueLoaiVangBanRa.Properties.DataSource = dataSource;
            lueLoaiVangBanRa.Properties.DisplayMember = "DisplayMember";
            lueLoaiVangBanRa.Properties.ValueMember = "ValueMember";
        }

        public List<SanPham> NhapSanPhamVaoKho(SanPham sanPham, int soLuong)
        {
            List<SanPham> listSanPham = new List<SanPham>();

            for(int i = 0; i < soLuong; i++)
            {
                listSanPham.Add(new SanPham
                {
                    GiaBan = sanPham.GiaBan,
                    HamLuongVang = sanPham.HamLuongVang,
                    KhoiLuongDa = sanPham.KhoiLuongDa,
                    KhoiLuongVang = sanPham.KhoiLuongVang,
                    KhoiLuongTinh = sanPham.KhoiLuongTinh,
                    KiHieu = sanPham.KiHieu,
                    IsSanPhamChuyenKho = sanPham.IsSanPhamChuyenKho,
                    MaLichSuBan = sanPham.MaLichSuBan,
                    MaNhaCungCap = sanPham.MaNhaCungCap,
                    MaNhanVien = sanPham.MaNhanVien,
                    MaTem = sanPham.MaTem,
                    NgayNhap = sanPham.NgayNhap,
                    TenSanPham = sanPham.TenSanPham,
                    TienCong = sanPham.TienCong,
                    TiLeHot = sanPham.TiLeHot,
                    MaHamLuongVang = sanPham.MaHamLuongVang
                });
            }

            return SanPhamDAO.Instance.NhapSanPhamVaoKho(listSanPham);
        }

        public List<SanPham> NhapSanPhamVaoKho(List<SanPham> listSanPham)
        {
            return SanPhamDAO.Instance.NhapSanPhamVaoKho(listSanPham);
        }

        public bool TinhGiaBanAllSanPham()
        {
            return SanPhamDAO.Instance.TinhGiaBanAllSanPham();
        }

        public SanPham UpdateSanPham(SanPham sanPhamUpdate, SanPham sanPhamCu)
        {
            sanPhamUpdate.MaSanPham = sanPhamCu.MaSanPham;
            sanPhamUpdate.IsSanPhamChuyenKho = sanPhamCu.IsSanPhamChuyenKho;
            sanPhamUpdate.MaLichSuBan = sanPhamCu.MaLichSuBan;
            sanPhamUpdate.NgayNhap = sanPhamCu.NgayNhap;
            sanPhamUpdate.MaNhanVien = sanPhamCu.MaNhanVien;

            return SanPhamDAO.Instance.Update(sanPhamUpdate);
        }

        public SanPham UpdateSanPham(SanPham sanPham)
        {
            return SanPhamDAO.Instance.Update(sanPham);
        }

        public bool DeleteSanPham(SanPham sanPham)
        {
            return SanPhamDAO.Instance.DeleteSanPham(sanPham);
        }
        
        public List<SanPham> GetAllSanPham()
        {
            return SanPhamDAO.Instance.GetList();
        }

        public bool XoaMaLichSuBan(List<SanPham> listSanPham)
        {
            return SanPhamDAO.Instance.XoaMaLichSuBan(listSanPham);
        }

        public bool XoaListSanPhamXuatKho()
        {
            return SanPhamDAO.Instance.XoaListSanPhamXuatKho();
        }
    }
}
