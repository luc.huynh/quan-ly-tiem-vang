﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LoaiPhieuPhatSinhThuChiBUS
    {
        private static LoaiPhieuPhatSinhThuChiBUS instance;

        public static LoaiPhieuPhatSinhThuChiBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LoaiPhieuPhatSinhThuChiBUS();
                return instance;
            }
        }

        public void GetLookupEdit(LookUpEdit lookUpEdit)
        {
            var listLookup = LoaiPhieuPhatSinhThuChiDAO.Instance.GetList();
            lookUpEdit.Properties.DataSource = listLookup;
            lookUpEdit.Properties.DisplayMember = "TenLoaiPhieu";
            lookUpEdit.Properties.ValueMember = "MaLoaiPhieuPhatSinhThuChi";

            if (listLookup.Count > 0)
            {
                lookUpEdit.EditValue = listLookup[0].MaLoaiPhieuPhatSinhThuChi;
            }
        }

        public List<LoaiPhieuPhatSinhThuChi> GetList()
        {
            return LoaiPhieuPhatSinhThuChiDAO.Instance.GetList();
        }

        public LoaiPhieuPhatSinhThuChi Insert(LoaiPhieuPhatSinhThuChi loaiPhieuPhatSinhThuChi)
        {
            return LoaiPhieuPhatSinhThuChiDAO.Instance.Insert(loaiPhieuPhatSinhThuChi);
        }

        public LoaiPhieuPhatSinhThuChi Update(LoaiPhieuPhatSinhThuChi loaiPhieuPhatSinhThuChi)
        {
            return LoaiPhieuPhatSinhThuChiDAO.Instance.Update(loaiPhieuPhatSinhThuChi);
        }

        public LoaiPhieuPhatSinhThuChi DeleteById(int maLoaiPhieuPhatSinhThuChi)
        {
            return LoaiPhieuPhatSinhThuChiDAO.Instance.DeleteById(maLoaiPhieuPhatSinhThuChi);
        }
    }
}
