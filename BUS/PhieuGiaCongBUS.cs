﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class PhieuGiaCongBUS
    {
        #region instance
        private static PhieuGiaCongBUS instance;

        public static PhieuGiaCongBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new PhieuGiaCongBUS();
                return instance;
            }
        }
        #endregion

        public List<PhieuGiaCong> GetList()
        {
            return PhieuGiaCongDAO.Instance.GetList();
        }

        public PhieuGiaCong Insert(PhieuGiaCong phieuGiaCong)
        {
            return PhieuGiaCongDAO.Instance.Insert(phieuGiaCong);
        }

        public PhieuGiaCong DeleteById(int maPhieuGiaCong)
        {
            return PhieuGiaCongDAO.Instance.DeleteById(maPhieuGiaCong);
        }
    }
}
