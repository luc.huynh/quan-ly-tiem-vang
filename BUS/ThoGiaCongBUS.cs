﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ThoGiaCongBUS
    {
        #region instance
        private static ThoGiaCongBUS instance;

        public static ThoGiaCongBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new ThoGiaCongBUS();
                return instance;
            }
        }
        #endregion

        public void GetLookupEdit(LookUpEdit lookUpEdit)
        {
            var listThoGiaCong = ThoGiaCongDAO.Instance.GetList();
            lookUpEdit.Properties.DataSource = listThoGiaCong;
            lookUpEdit.Properties.DisplayMember = "HoTen";
            lookUpEdit.Properties.ValueMember = "MaThoGiaCong";

            if (listThoGiaCong.Count > 0)
            {
                lookUpEdit.EditValue = listThoGiaCong[0].MaThoGiaCong;
            }
        }

        public List<ThoGiaCong> GetListAllThoGiaCong()
        {
            return ThoGiaCongDAO.Instance.GetList();
        }

        public ThoGiaCong Insert(ThoGiaCong thoGiaCong)
        {
            return ThoGiaCongDAO.Instance.Insert(thoGiaCong);
        }

        public ThoGiaCong Update(ThoGiaCong thoGiaCong)
        {
            return ThoGiaCongDAO.Instance.Update(thoGiaCong);
        }

        public ThoGiaCong DeleteById(int maThoGiaCong)
        {
            return ThoGiaCongDAO.Instance.DeleteById(maThoGiaCong);
        }
    }
}
