﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using DevExpress.XtraGrid;
using DTO.Models;
using BUS.Ultils;
using DAO;

namespace BUS
{
    public class NhanVienBUS
    {
        #region instance
        private static NhanVienBUS instance;

        public static NhanVienBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new NhanVienBUS();
                return instance;
            }
        }
        #endregion

        public NhanVien Login(string user,string pass)
        {
            return NhanVienDAO.Instance.Login(user, Ultils.NhanVienUltils.EncryptPass(pass));
        }

        public bool changePassword(NhanVien nhanvien, string pass)
        {
            nhanvien.MatKhau = NhanVienUltils.EncryptPass(pass);
            return NhanVienDAO.Instance.Update(nhanvien);
        }

        public void GetList(GridControl grcNhanVien)
        {
            grcNhanVien.DataSource = NhanVienDAO.Instance.GetList();
        }

        public bool Update(NhanVien nhanVien)
        {
            return NhanVienDAO.Instance.Update(nhanVien);
        }

        /// <summary>
        /// Kiểm tra tài khoản có tồn tại trong hệ thống chưa
        /// </summary>
        /// <param name="taiKhoan"></param>
        /// <returns>true nếu tồn tại</returns>
        public bool CheckExist(string taiKhoan)
        {
            if (NhanVienDAO.Instance.CheckUser(taiKhoan) > 0)
            {
                return true;
            }
            return false;
        }

        public bool Insert(string firstName, string lastName, string phone, string username, string password, string gender, string passport, string dateOfBirth, string address, string isQuyen, string isNhapKho)
        {
            NhanVien nhanVien = new NhanVien();
            try
            {
                nhanVien.HoLot = firstName;
                nhanVien.Ten = lastName;
                nhanVien.SoDienThoai = phone;
                nhanVien.TaiKhoan = username;
                nhanVien.MatKhau = Ultils.NhanVienUltils.EncryptPass(password);
                nhanVien.GioiTinh = gender;
                nhanVien.ChungMinhNhanDan = passport;
                nhanVien.DiaChi = address;
                nhanVien.Quyen = isQuyen;
                nhanVien.QuyenNhapKho = isNhapKho;
                nhanVien.NgaySinh = DateTime.Parse(dateOfBirth);
            }
            catch(Exception e)
            {
                nhanVien.NgaySinh = null;
                Console.WriteLine("Sai định dạng ngày sinh",e.Message);
            }

            return NhanVienDAO.Instance.Insert(nhanVien);
        }

        public bool Delete(int maNhanVien)
        {
            return NhanVienDAO.Instance.Delete(maNhanVien);
        }
    }
}
