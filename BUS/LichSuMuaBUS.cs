﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraGrid;

namespace BUS
{
    public class LichSuMuaBUS
    {
        private static LichSuMuaBUS instance;

        public static LichSuMuaBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuMuaBUS();
                return instance;
            }
        }

        public string Insert(
            string maLichSuMua,
            DateTime ngayNhap,
            int maNhanVien,
            int tongTienVang,
            int maKhachHang,
            int tienCongThem = 0)
        {
            LichSuMua lichSuMua = new LichSuMua {
                MaLichSuMua = maLichSuMua,
                NgayGiaoDich = ngayNhap,
                MaNhanVien = maNhanVien,
                TongTienVang = tongTienVang,
                TienCongThem = tienCongThem,
                MaKhachHang = maKhachHang
            };

            return LichSuMuaDAO.Instance.Insert(lichSuMua);
        }
        
        public void GetListGridControl(GridControl grcLichSuMua)
        {
            grcLichSuMua.DataSource = LichSuMuaDAO.Instance.GetListLichSuMua();
        }

        public LichSuMua GetLichSuMuaById(string maLichSuMua)
        {
            return LichSuMuaDAO.Instance.GetLichSuMuaById(maLichSuMua);
        }

        public LichSuMua ThanhToanHoaDon(LichSuMua lichSuMua, List<SanPhamKhoCu> listSanPhamMuaVao)
        {
            foreach(SanPhamKhoCu sanPham in listSanPhamMuaVao)
            {
                sanPham.KiHieuHamLuongVang = null;
                sanPham.MaLichSuMua = lichSuMua.MaLichSuMua;
                sanPham.NgayNhap = lichSuMua.NgayGiaoDich;
            }

            return LichSuMuaDAO.Instance.ThanhToanHoaDon(lichSuMua, listSanPhamMuaVao);
        }

        public bool XoaHoaDon(LichSuMua lichSuMua)
        {
            return LichSuMuaDAO.Instance.XoaHoaDon(lichSuMua);
        }
    }
}
