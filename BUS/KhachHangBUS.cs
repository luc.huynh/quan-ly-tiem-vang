﻿using DAO;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KhachHangBUS
    {
        #region instance
        private static KhachHangBUS instance;

        public static KhachHangBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new KhachHangBUS();
                return instance;
            }
        }
        #endregion

        public void GetList(LookUpEdit lookup)
        {
            var List = KhachHangDAO.Instance.GetList();
            lookup.Properties.DataSource = List;
            lookup.Properties.DisplayMember = "Ten";
            lookup.Properties.ValueMember = "MaKhachHang";

            if (List.Count > 0)
            {
                lookup.EditValue = List[0].MaKhachHang;
            }
        }

        public void GetLookupEdit(LookUpEdit lookUpEdit)
        {
            var listKhachHang = KhachHangDAO.Instance.GetList();
            lookUpEdit.Properties.DataSource = listKhachHang;
            lookUpEdit.Properties.DisplayMember = "Ten";
            lookUpEdit.Properties.ValueMember = "MaKhachHang";

            if (listKhachHang.Count > 0)
            {
                lookUpEdit.EditValue = listKhachHang[0].MaKhachHang;
            }
        }

        public List<KhachHang> GetListAllKhachHang()
        {
            return KhachHangDAO.Instance.GetList();
        }

        public KhachHang Insert(KhachHang khachHang)
        {
            return KhachHangDAO.Instance.Insert(khachHang);
        }

        public KhachHang Update(KhachHang khachHang)
        {
            return KhachHangDAO.Instance.Update(khachHang);
        }

        public KhachHang DeleteById(int maKhachHang)
        {
            return KhachHangDAO.Instance.DeleteById(maKhachHang);
        }
    }
}
