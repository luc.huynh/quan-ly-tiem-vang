﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class NgayDauKiThongKeBUS
    {
        private static NgayDauKiThongKeBUS instance;

        public static NgayDauKiThongKeBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new NgayDauKiThongKeBUS();
                return instance;
            }
        }

        public NgayDauKiThongKe GetNgayDauKiLast()
        {
            return NgayDauKiThongKeDAO.Instance.GetNgayDauKiLast();
        }

        public NgayDauKiThongKe Insert(DateEdit detNgayDauKi, TextEdit txtTienDauKi)
        {
            NgayDauKiThongKe ngayDauKi = new NgayDauKiThongKe
            {
                NgayDauKi = detNgayDauKi.DateTime,
                SoTienDauKi = (int)txtTienDauKi.EditValue,
                NgayChinhSua = DateTime.Today
            };

            return NgayDauKiThongKeDAO.Instance.Insert(ngayDauKi);
        }
    }
}
