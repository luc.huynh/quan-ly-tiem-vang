﻿using DevExpress.XtraEditors;
using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ThongKeKhoMoiBUS
    {
        private static ThongKeKhoMoiBUS instance;

        public static ThongKeKhoMoiBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new ThongKeKhoMoiBUS();
                return instance;
            }
        }

        public ThongKeKhoMoiTemp GetThongKeKhoMoiData(string hamLuongVang = null)
        {
            List<SanPham> listSanPham = SanPhamBUS.Instance.GetListSanPhamChuaBan();

            if (!string.IsNullOrWhiteSpace(hamLuongVang))
            {
                listSanPham = listSanPham.Where(x => x.HamLuongVang.Equals(hamLuongVang)).ToList();
            }

            return new ThongKeKhoMoiTemp
            {
                TongSoLuongSanPham = listSanPham.Count,
                TongTienCong = listSanPham.Sum(x => x.TienCong),
                TongKhoiLuongDa = Math.Round((double)listSanPham.Sum(x => x.KhoiLuongDa), 1),
                TongKhoiLuongTinh = Math.Round((double)listSanPham.Sum(x => x.KhoiLuongTinh), 1),
                TongKhoiLuongVang = Math.Round((double)listSanPham.Sum(x => x.KhoiLuongVang), 1)
            };
        }
    }
}
