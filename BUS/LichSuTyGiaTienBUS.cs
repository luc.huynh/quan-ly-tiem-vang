﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.TemporaryModel;
using DAO;
using DTO.Models;

namespace BUS
{
    public class LichSuTyGiaTienBUS
    {
        private static LichSuTyGiaTienBUS instance;

        public static LichSuTyGiaTienBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuTyGiaTienBUS();
                return instance;
            }
        }

        public List<LichSuTyGiaTien> GetAllTyGiaTien()
        {
            return LichSuTyGiaTienDAO.Instance.GetAllTyGiaTien();
        }

        public bool NhapTyGiaTien(List<LichSuTyGiaTien> listTyGiaTienHienTai, DateTime today)
        {
            return LichSuTyGiaTienDAO.Instance.NhapTyGiaTien(listTyGiaTienHienTai, today);
        }
    }
}
