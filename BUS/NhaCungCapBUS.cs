﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class NhaCungCapBUS
    {
        #region instance
        private static NhaCungCapBUS instance;

        public static NhaCungCapBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new NhaCungCapBUS();
                return instance;
            }
        }
        #endregion

        public List<NhaCungCap> GetListAllNhaCungCap()
        {
            return NhaCungCapDAO.Instance.GetListAllNhaCungCap();
        }

        public void GetLookupEdit(LookUpEdit lookUpEdit)
        {
            lookUpEdit.Properties.DataSource = NhaCungCapDAO.Instance.GetListAllNhaCungCap();
            lookUpEdit.Properties.DisplayMember = "TenNhaCungCap";
            lookUpEdit.Properties.ValueMember = "MaNhaCungCap";
        }

        public bool Insert(string tenNhaCungCap, string diaChi)
        {
            NhaCungCap nhaCungCap = new NhaCungCap();
            nhaCungCap.TenNhaCungCap = tenNhaCungCap;
            nhaCungCap.DiaChi = diaChi;
            return NhaCungCapDAO.Instance.Insert(nhaCungCap);
        }

        public bool Update(NhaCungCap nhaCungCap)
        {
            return NhaCungCapDAO.Instance.Update(nhaCungCap);
        }

        public bool Delete(NhaCungCap nhaCungCap)
        {
            return NhaCungCapDAO.Instance.Delete(nhaCungCap.MaNhaCungCap);
        }

        public bool IsHaveNhaCungCap(string tenNhaCungCap)
        {
            return NhaCungCapDAO.Instance.IsHaveNhaCungCap(tenNhaCungCap);
        }

        public NhaCungCap GetById(int id)
        {
            return NhaCungCapDAO.Instance.GetById(id);
        }
    }
}
