﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraGrid;
using DAO;
using DTO.Models;
using DevExpress.XtraEditors;

namespace BUS
{
    public class LoaiTienBUS
    {
        private static LoaiTienBUS instance;

        public static LoaiTienBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LoaiTienBUS();
                return instance;
            }
        }

        public void GetList(GridControl gctLoaiTien)
        {
            gctLoaiTien.DataSource = LoaiTienDAO.Instance.GetList();
        }

        public void getLookupEdit(LookUpEdit lueLoaiTien)
        {
            lueLoaiTien.Properties.DataSource = LoaiTienDAO.Instance.GetList();
            lueLoaiTien.Properties.DisplayMember = "TenLoaiTien";
            lueLoaiTien.Properties.ValueMember = "MaLoaiTien";
        }

        public bool Insert(string tenLoaiTien)
        {
            LoaiTien loaiTien = new LoaiTien();
            try
            {
                loaiTien.TenLoaiTien = tenLoaiTien;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return LoaiTienDAO.Instance.Insert(loaiTien);
        }

        public bool Update(LoaiTien loaiTien)
        {
            return LoaiTienDAO.Instance.Update(loaiTien);
        }

        public bool Delete(int maLoaiTien)
        {
            return LoaiTienDAO.Instance.Delete(maLoaiTien);
        }
    }
}
