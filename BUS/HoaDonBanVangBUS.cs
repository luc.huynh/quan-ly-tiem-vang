﻿using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class HoaDonBanVangBUS
    {
        private static HoaDonBanVangBUS instance;

        public static HoaDonBanVangBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new HoaDonBanVangBUS();
                return instance;
            }
        }

        public List<HoaDonVangDoiVang> GetListHoaDonBanVang(string maLichSuBan)
        {
            List<HoaDonVangDoiVang> listHoaDonBanVang = new List<HoaDonVangDoiVang>();
            LichSuBan lichSuBan = LichSuBanBUS.Instance.GetLichSuBanById(maLichSuBan);
            string hoLotKhach = "";
            string tenKhach = "";
            string diaChiKhach = "";

            if (!string.IsNullOrWhiteSpace(lichSuBan.KhachHang.HoLot))
            {
                hoLotKhach = lichSuBan.KhachHang.HoLot.Trim();
            }

            if (!string.IsNullOrWhiteSpace(lichSuBan.KhachHang.Ten))
            {
                tenKhach = lichSuBan.KhachHang.Ten.Trim();
            }

            if (!string.IsNullOrWhiteSpace(lichSuBan.KhachHang.DiaChi))
            {
                diaChiKhach = lichSuBan.KhachHang.DiaChi.Trim();
            }

            foreach (var sanPham in lichSuBan.SanPhams)
            {
                HoaDonVangDoiVang hoaDon = new HoaDonVangDoiVang
                {
                    //MaTem = sanPham.MaTem,
                    //GiaBan = sanPham.GiaBan.ToString(),
                    //GiaMua = "0",
                    //TenKhachHang = hoLotKhach + " " + tenKhach,
                    //HamLuongVang = sanPham.HamLuongVang,
                    //LoaiGiaoDich = "Bán hàng",
                    //LoaiMatHang = "Hàng mới",
                    //MaHoaDon = maLichSuBan,
                    //TenSanPham = sanPham.TenSanPham,
                    //ThanhTien = sanPham.GiaBan.ToString(),
                    //TienCong = sanPham.TienCong.ToString(),
                    //TLHot = sanPham.KhoiLuongDa.ToString(),
                    //TLVang = sanPham.KhoiLuongVang.ToString(),
                    //TongTienVangMoi = lichSuBan.TongTien.ToString(),
                    //TongTienVangCu = "0",
                    //CuaHangBuLo = "0",
                    //DiaChi = diaChiKhach,
                    //ThanhToan =  lichSuBan.TongTien.ToString()
                };
                listHoaDonBanVang.Add(hoaDon);
            }
            return listHoaDonBanVang;
        }
    }
}
