﻿using DAO;
using DevExpress.XtraGrid;
using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KhoCuBUS
    {
        private static KhoCuBUS instance;

        public static KhoCuBUS Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KhoCuBUS();
                }
                return instance;
            }
        }

        public void SetDataForGridControl(GridControl grcKhoCu)
        {
            grcKhoCu.DataSource = KhoCuDAO.Instance.GetListAllKhoCu();
        }

        public int GetMaKhoCuByMaHamLuongVang(int maHamLuongVang)
        {
            KhoCu khoCu = KhoCuDAO.Instance.GetKhoCuByMaHamLuongVang(maHamLuongVang);

            if (khoCu != null)
            {
                return khoCu.MaKhoCu;
            }

            return Constant.BAD_VALUE;
        }
        
    }
}
