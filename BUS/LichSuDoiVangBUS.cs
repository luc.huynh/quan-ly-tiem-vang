﻿using DAO;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LichSuDoiVangBUS
    {
        private static LichSuDoiVangBUS instance;

        public static LichSuDoiVangBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuDoiVangBUS();
                return instance;
            }
        }

        public LichSuDoiVang Insert(
            string maHoaDonBan,
            string maHoaDonMua,
            LichSuDoiVang lichSuDoiVang)
        {
            lichSuDoiVang.MaLichSuBan = maHoaDonBan;
            lichSuDoiVang.MaLichSuMua = maHoaDonMua;

            return LichSuDoiVangDAO.Instance.Insert(lichSuDoiVang);
        }

        public LichSuDoiVang GetLichSuDoiVangById(string maLichSuVangDoiVang)
        {
            return LichSuDoiVangDAO.Instance.GetLichSuDoiVangById(maLichSuVangDoiVang);
        }

        public LichSuDoiVang ThanhToanHoaDon(
            LichSuDoiVang lichSuDoiVang,
            List<SanPham> listSanPhamBanRa,
            List<SanPhamKhoCu> listSanPhamMuaVao,
            LookUpEdit lueKhachHang,
            int maNhanVien)
        {
            LichSuMua lichSuMua = new LichSuMua
            {
                MaLichSuMua = lichSuDoiVang.MaLichSuMua,
                MaKhachHang = (int)lueKhachHang.EditValue,
                MaNhanVien = maNhanVien,
                NgayGiaoDich = lichSuDoiVang.NgayGiaoDich,
                TienCongThem = 0,
                TongTienVang = listSanPhamMuaVao.Sum(x => x.GiaMuaVao),
                ThanhTienHoaDon = listSanPhamMuaVao.Sum(x => x.GiaMuaVao)
            };

            LichSuBan lichSuBan = new LichSuBan
            {
                MaLichSuBan = lichSuDoiVang.MaLichSuBan,
                MaKhachHang = (int)lueKhachHang.EditValue,
                MaNhanVien = maNhanVien,
                NgayGiaoDich = lichSuDoiVang.NgayGiaoDich,
                TienBot = lichSuDoiVang.TienBot,
                TongTienVang = listSanPhamBanRa.Sum(x => x.GiaBan),
                ThanhTienHoaDon = listSanPhamBanRa.Sum(x => x.GiaBan) - lichSuDoiVang.TienBot
            };

            foreach(SanPham sanPham in listSanPhamBanRa)
            {
                sanPham.MaLichSuBan = lichSuDoiVang.MaLichSuBan;
            }

            foreach(SanPhamKhoCu sanPham in listSanPhamMuaVao)
            {
                sanPham.MaLichSuMua = lichSuDoiVang.MaLichSuMua;
                sanPham.NgayNhap = lichSuDoiVang.NgayGiaoDich;
                sanPham.KiHieuHamLuongVang = null;
            }

            return LichSuDoiVangDAO.Instance.ThanhToanHoaDon(
                lichSuDoiVang,
                lichSuBan,
                lichSuMua,
                listSanPhamBanRa,
                listSanPhamMuaVao);
        }

        public List<LichSuDoiVang> GetListLichSuDoiVang()
        {
            return LichSuDoiVangDAO.Instance.GetListLichSuDoiVang();
        }

        public bool XoaHoaDon(LichSuDoiVang lichSuDoiVang)
        {
            return LichSuDoiVangDAO.Instance.XoaHoaDon(lichSuDoiVang);
        }
    }
}
