﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ChiTietPhieuGiaCongBUS
    {
        #region instance
        private static ChiTietPhieuGiaCongBUS instance;

        public static ChiTietPhieuGiaCongBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new ChiTietPhieuGiaCongBUS();
                return instance;
            }
        }
        #endregion

        public List<ChiTietPhieuGiaCong> GetListByMaPhieuGiaCong(int maPhieuGiaCong)
        {
            return ChiTietPhieuGiaCongDAO.Instance.GetListByMaPhieuGiaCong(maPhieuGiaCong);
        }

        public ChiTietPhieuGiaCong Insert(ChiTietPhieuGiaCong chiTietPhieuGiaCong)
        {
            return ChiTietPhieuGiaCongDAO.Instance.Insert(chiTietPhieuGiaCong);
        }

        public ChiTietPhieuGiaCong Update(ChiTietPhieuGiaCong chiTietPhieuGiaCong)
        {
            return ChiTietPhieuGiaCongDAO.Instance.Update(chiTietPhieuGiaCong);
        }

        public ChiTietPhieuGiaCong DeleteById(int maChiTietPhieuGiaCong)
        {
            return ChiTietPhieuGiaCongDAO.Instance.DeleteById(maChiTietPhieuGiaCong);
        }
    }
}
