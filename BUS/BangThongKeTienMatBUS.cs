﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BangThongKeTienMatBUS
    {
        private static BangThongKeTienMatBUS instance;

        public static BangThongKeTienMatBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new BangThongKeTienMatBUS();
                return instance;
            }
        }

        public bool TinhToanThongKe()
        {
            return BangThongKeTienMatDAO.Instance.TinhToanThongKe();
        }

        public List<BangThongKeTienMat> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            return BangThongKeTienMatDAO.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        public List<BangThongKeTienMat> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            return BangThongKeTienMatDAO.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
        }
    }
}
