﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KiHieuHamLuongVangBUS
    {
        private static KiHieuHamLuongVangBUS instance;

        public static KiHieuHamLuongVangBUS Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KiHieuHamLuongVangBUS();
                }
                return instance;
            }
        }

        public List<KiHieuHamLuongVang> GetListAllHamLuongVang()
        {
            return KiHieuHamLuongVangDAO.Instance.GetListAllHamLuongVang();
        }

        public void GetLookupEdit(LookUpEdit lookUpEdit)
        {
            lookUpEdit.Properties.DataSource = KiHieuHamLuongVangDAO.Instance.GetListAllHamLuongVang();
            lookUpEdit.Properties.DisplayMember = "HamLuongVang";
            lookUpEdit.Properties.ValueMember = "MaHamLuongVang";
        }

        public KiHieuHamLuongVang Insert(string hamLuongVang)
        {
            KiHieuHamLuongVang kiHieuHamLuongVang = new KiHieuHamLuongVang();
            kiHieuHamLuongVang.HamLuongVang = hamLuongVang;
            return KiHieuHamLuongVangDAO.Instance.Insert(kiHieuHamLuongVang);
        }

        public bool Update(KiHieuHamLuongVang hamLuongVang)
        {
            return KiHieuHamLuongVangDAO.Instance.Update(hamLuongVang);
        }

        public bool DeleteById(int maHamLuongVang)
        {
            return KiHieuHamLuongVangDAO.Instance.DelelteById(maHamLuongVang);
        }

        public bool HaveHamLuongVang(string hamLuongVang)
        {
            return KiHieuHamLuongVangDAO.Instance.HaveHamLuongVang(hamLuongVang);
        }
    }
}
