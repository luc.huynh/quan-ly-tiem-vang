﻿using DAO;
using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class HopDongCamDoBUS
    {
        private static HopDongCamDoBUS instance;

        public static HopDongCamDoBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new HopDongCamDoBUS();
                return instance;
            }
        }
        
        public HopDongCamDo Insert(HopDongCamDo hopDong, List<SanPhamCamDo> listSanPham)
        {
            foreach(SanPhamCamDo sanPham in listSanPham)
            {
                sanPham.MaHopDongCamDo = hopDong.MaHopDongCamDo;
                sanPham.NgayNgap = DateTime.Today;
            }

            hopDong.NgayInMoiHopDong = DateTime.Today;

            return HopDongCamDoDAO.Instance.Insert(hopDong, listSanPham);
        }

        public int TinhSoTienLai(string maHopDong, DateTime ngayDongLai)
        {
            return HopDongCamDoDAO.Instance.TinhSotienLai(maHopDong, ngayDongLai);
        }

        public List<HopDongCamDo> GetListDaThanhToan()
        {
            return HopDongCamDoDAO.Instance.GetListDaThanhToan();
        }

        public List<HopDongCamDo> GetListChuaThanhToan()
        {
            return HopDongCamDoDAO.Instance.GetListChuaThanhToan();
        }

        public List<HopDongCamDo> GetList()
        {
            return HopDongCamDoDAO.Instance.GetAllHopDong();
        }

        public bool ThanhToan(string maHopDong)
        {
            return HopDongCamDoDAO.Instance.ThanhToan(maHopDong);
        }

        public bool ThanhLy(string maHopDong, int maNhanVien, int loaiVangMua, string maLichSuMua)
        {
            return HopDongCamDoDAO.Instance.ThanhLy(maHopDong, maNhanVien, loaiVangMua, maLichSuMua);
        }

        public bool Delete(string maHopDongCamDo)
        {
            return HopDongCamDoDAO.Instance.Delete(maHopDongCamDo);
        }

        public HopDongCamDo GetHopDongById(string maHopDongCamDo)
        {
            return HopDongCamDoDAO.Instance.GetHopDongById(maHopDongCamDo);
        }

        public HopDongCamDo GiaHanHopDong(string maHopDong, DateTime ngayGiaHan, int soTienDongLai)
        {
            return HopDongCamDoDAO.Instance.GiaHanHopDong(maHopDong, ngayGiaHan.Date, soTienDongLai);
        }

        public List<HopDongCamDo> GetListThongBaoThanhLy()
        {
            return HopDongCamDoDAO.Instance.GetListThongBaoThanhLy();
        }
    }
}
