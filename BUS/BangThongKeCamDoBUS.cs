﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BangThongKeCamDoBUS
    {
        private static BangThongKeCamDoBUS instance;

        public static BangThongKeCamDoBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new BangThongKeCamDoBUS();
                return instance;
            }
        }

        public bool TinhToanThongKe()
        {
            return BangThongKeCamDoDAO.Instance.TinhToanThongKe();
        }

        public List<BangThongKeCamDo> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            return BangThongKeCamDoDAO.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        public List<BangThongKeCamDo> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            return BangThongKeCamDoDAO.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
        }
    }
}
