﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraGrid;
using DAO;
using DTO.Models;

namespace BUS
{
    public class LichSuBanBUS
    {
        private static LichSuBanBUS instance;

        public static LichSuBanBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuBanBUS();
                return instance;
            }
        }

        public void GetListGridControl(GridControl grcLichSuBan)
        {
            grcLichSuBan.DataSource = LichSuBanDAO.Instance.GetListLichSuBan();
        }

        public LichSuBan GetLichSuBanById(string maLichSuBan)
        {
            return LichSuBanDAO.Instance.GetLichSuBanById(maLichSuBan);
        }

        public LichSuBan ThanhToanHoaDon(LichSuBan lichSuBan, List<SanPham> listSanPhamBanRa)
        {
            return LichSuBanDAO.Instance.ThanhToanHoaDon(lichSuBan, listSanPhamBanRa);
        }

        public bool XoaHoaDon(LichSuBan lichSuBan)
        {
            return LichSuBanDAO.Instance.XoaHoaDon(lichSuBan);
        }
    }
}
