﻿using DAO;
using DevExpress.XtraEditors;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class PhieuPhatSinhThuChiBUS
    {
        private static PhieuPhatSinhThuChiBUS instance;

        public static PhieuPhatSinhThuChiBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new PhieuPhatSinhThuChiBUS();
                return instance;
            }
        }

        public List<PhieuPhatSinhThuChi> GetList()
        {
            return PhieuPhatSinhThuChiDAO.Instance.GetList();
        }

        public PhieuPhatSinhThuChi Insert(PhieuPhatSinhThuChi phieuPhatSinhThuChi)
        {
            return PhieuPhatSinhThuChiDAO.Instance.Insert(phieuPhatSinhThuChi);
        }

        public PhieuPhatSinhThuChi Update(PhieuPhatSinhThuChi phieuPhatSinhThuChi)
        {
            return PhieuPhatSinhThuChiDAO.Instance.Update(phieuPhatSinhThuChi);
        }

        public PhieuPhatSinhThuChi DeleteById(int maPhieuPhatSinhThuChi)
        {
            return PhieuPhatSinhThuChiDAO.Instance.DeleteById(maPhieuPhatSinhThuChi);
        }

        public PhieuPhatSinhThuChi GetById(int id)
        {
            return PhieuPhatSinhThuChiDAO.Instance.GetById(id);
        }
    }
}
