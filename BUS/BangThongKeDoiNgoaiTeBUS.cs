﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BangThongKeDoiNgoaiTeBUS
    {
        private static BangThongKeDoiNgoaiTeBUS instance;

        public static BangThongKeDoiNgoaiTeBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new BangThongKeDoiNgoaiTeBUS();
                return instance;
            }
        }

        public bool TinhToanThongKe()
        {
            return BangThongKeDoiNgoaiTeDAO.Instance.TinhToanThongKe();
        }

        public List<BangThongKeDoiNgoaiTe> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            return BangThongKeDoiNgoaiTeDAO.Instance.GetBangThongKeByDate(ngayThongKe);
        }

        public List<BangThongKeDoiNgoaiTe> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            return BangThongKeDoiNgoaiTeDAO.Instance.GetThongKeByTwoDifferenceDate(tuNgay, denNgay);
        }
    }
}
