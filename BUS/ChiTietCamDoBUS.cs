﻿using DAO;
using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ChiTietCamDoBUS
    {
        private static ChiTietCamDoBUS instance;

        public static ChiTietCamDoBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new ChiTietCamDoBUS();
                return instance;
            }
        }

        public bool GiaHanHopDong(
            string maChiTietGiaHan, 
            string maHopDong, 
            int maNhanVien, 
            string soTienDongLai, 
            string ngayDongLai, 
            string ngayDauThang, 
            string ngayKetThucThang,
            bool isThanhToan)
        {
            ChiTietCamDo chiTietCamDo = new ChiTietCamDo();
            chiTietCamDo.MaChiTietCamDo = maChiTietGiaHan;
            chiTietCamDo.MaHopDongCamDo = maHopDong;
            chiTietCamDo.MaNhanVien = maNhanVien;
            DateTime d_ngayDongLai = DateTime.Parse(ngayDongLai);
            DateTime d_ngayDauThang = DateTime.Parse(ngayDauThang);
            DateTime d_ngayKetThucThang = DateTime.Parse(ngayKetThucThang);
            chiTietCamDo.NgayNopTienLai = d_ngayDongLai;
            chiTietCamDo.NgayBatDauThang = d_ngayDauThang;
            if (isThanhToan)
            {
                return HopDongCamDoBUS.Instance.ThanhToan(maHopDong) && ChiTietCamDoDAO.Instance.GiaHanHopDong(chiTietCamDo);
            }
            return ChiTietCamDoDAO.Instance.GiaHanHopDong(chiTietCamDo);

        }

        public List<ChiTietCamDoView> getChiTietGiaHan(string maHopDongCamDo)
        {
            return ChiTietCamDoDAO.Instance.getListChiTietCamDo(maHopDongCamDo);
        }

        public bool Delete(string maHopDongCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                var chiTiet = dbContext.ChiTietCamDoes.Where(x => x.MaHopDongCamDo == maHopDongCamDo).ToList();
                foreach (var item in chiTiet)
                {
                    dbContext.ChiTietCamDoes.Remove(item);
                }
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
