﻿using DAO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LichSuDoiNgoaiTeBUS
    {
        private static LichSuDoiNgoaiTeBUS instance;

        public static LichSuDoiNgoaiTeBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuDoiNgoaiTeBUS();
                return instance;
            }
        }

        public List<LichSuDoiNgoaiTe> GetList()
        {
            return LichSuDoiNgoaiTeDAO.Instance.GetList();
        }

        public LichSuDoiNgoaiTe Insert(LichSuDoiNgoaiTe lichSuDoiNgoaiTe)
        {
            return LichSuDoiNgoaiTeDAO.Instance.Insert(lichSuDoiNgoaiTe);
        }

        public LichSuDoiNgoaiTe Update(LichSuDoiNgoaiTe lichSuDoiNgoaiTe)
        {
            return LichSuDoiNgoaiTeDAO.Instance.Update(lichSuDoiNgoaiTe);
        }

        public LichSuDoiNgoaiTe DeleteById(int maLichSuDoiNgoaiTe)
        {
            return LichSuDoiNgoaiTeDAO.Instance.DeleteById(maLichSuDoiNgoaiTe);
        }
    }
}
