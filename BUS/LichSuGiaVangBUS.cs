﻿using DAO;
using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LichSuGiaVangBUS
    {
        private static LichSuGiaVangBUS instance;

        public static LichSuGiaVangBUS Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuGiaVangBUS();
                return instance;
            }
        }

        public List<LichSuGiaVang> GetAllLichSuGiaVang()
        {
            return LichSuGiaVangDAO.Instance.GetAllGiaVangHienTai();
        }

        public bool NhapGiaVangDauNgay(List<LichSuGiaVang> listGiaVangHienTai, int maNhanVien, DateTime now)
        {
            return LichSuGiaVangDAO.Instance.NhapGiaVangDauNgay(listGiaVangHienTai, maNhanVien, now);
        }
    }
}
