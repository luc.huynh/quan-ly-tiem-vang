﻿using DAO;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class SanPhamKhoCuBUS
    {
        private static SanPhamKhoCuBUS instance;

        public static SanPhamKhoCuBUS Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SanPhamKhoCuBUS();
                }
                return instance;
            }
        }

        public void SetDataForGridControl(GridControl grcSanPhamKhoCu)
        {
            grcSanPhamKhoCu.DataSource = SanPhamKhoCuDAO.Instance.GetListAllSanPhamKhoCu();
        }

        public bool Insert(SanPhamKhoCu sanPhamKhoCu, int soLuong)
        {
            return SanPhamKhoCuDAO.Instance.Insert(sanPhamKhoCu, soLuong);
        }

        public bool ThemDanhSachSanPhamMuaVaoKhoCu(List<SanPhamKhoCu> listSanPhamKhoCu, string maLichSuMua)
        {
            foreach (SanPhamKhoCu sanPham in listSanPhamKhoCu)
            {
                sanPham.MaLichSuMua = maLichSuMua;
                sanPham.KiHieuHamLuongVang = null;
            }

            return SanPhamKhoCuDAO.Instance.ThemDanhSachSanPhamMuaVaoKhoCu(listSanPhamKhoCu);
        }

        public List<SanPhamKhoCu> GetListSanPhamKhoCu()
        {
            return SanPhamKhoCuDAO.Instance.GetListAllSanPhamKhoCu();
        }

        public List<SanPhamKhoCu> GetListSanPhamByLichSuMua(LichSuMua lichSuMua)
        {
            return SanPhamKhoCuDAO.Instance.GetListSanPhamByLichSuMua(lichSuMua);
        }

        public bool XoaSanPhamTheoLichSuMua(List<SanPhamKhoCu> listSanPhamKhoCu)
        {
            return SanPhamKhoCuDAO.Instance.XoaSanPhamTheoLichSuMua(listSanPhamKhoCu);
        }
    }
}
