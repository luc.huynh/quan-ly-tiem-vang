﻿using DAO;
using DevExpress.XtraGrid;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KhoiLuongGiaCongBUS
    {
        private static KhoiLuongGiaCongBUS instance;

        public static KhoiLuongGiaCongBUS Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KhoiLuongGiaCongBUS();
                }
                return instance;
            }
        }

        public void SetDataForGridControl(GridControl grcKhoiLuongGiaCong)
        {
            grcKhoiLuongGiaCong.DataSource = KhoiLuongGiaCongDAO.Instance.GetListAllKhoiLuongGiaCong();
        }

        public KhoiLuongGiaCong Insert(KhoiLuongGiaCong khoiLuongGiaCong)
        {
            return KhoiLuongGiaCongDAO.Instance.Insert(khoiLuongGiaCong);
        }

        public KhoiLuongGiaCong Update(KhoiLuongGiaCong khoiLuongGiaCong)
        {
            return KhoiLuongGiaCongDAO.Instance.Update(khoiLuongGiaCong);
        }

        public KhoiLuongGiaCong DeleteById(int maKhoiLuongGiaCong)
        {
            return KhoiLuongGiaCongDAO.Instance.DelelteById(maKhoiLuongGiaCong);
        }
    }
}
