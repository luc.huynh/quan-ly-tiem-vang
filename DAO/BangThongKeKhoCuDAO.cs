﻿using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangThongKeKhoCuDAO
    {
        private static BangThongKeKhoCuDAO instance;

        public static BangThongKeKhoCuDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BangThongKeKhoCuDAO();
                }
                return instance;
            }
        }

        public List<BangThongKeKhoCu> _listOfBangThongKeKhoCu = new List<BangThongKeKhoCu>();

        public BangThongKeKhoCu Insert(BangThongKeKhoCu bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeKhoCues.Add(bangThongKe);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeKhoCu Update(BangThongKeKhoCu bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeKhoCues.Find(bangThongKe.MaBangThongKeKhoCu);

                dbContext.Entry(entity).CurrentValues.SetValues(bangThongKe);
                dbContext.SaveChanges();

                return bangThongKe;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeKhoCu DelelteById(int maBangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeKhoCues.Remove(dbContext.BangThongKeKhoCues.Find(maBangThongKe));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        // Tạo những bản thống kê đầu tiên theo từng hàm lượng vàng đã có, được bắt đầu từ ngày đầu kì đầu tiên
        public bool TaoDuLieuDauKiDauTien(List<BangThongKeKhoCu> listOfBangThongKeKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                if (dbContext.NgayDauKiThongKes.ToList().Count == 0)
                {
                    return false;
                }

                NgayDauKiThongKe ngayDauKiDb = dbContext.NgayDauKiThongKes.ToList().First();
                List<KiHieuHamLuongVang> listHamLuongVangDb = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPhamKhoCu> listSanPhamKhoCuDb = dbContext.SanPhamKhoCues.ToList();
                List<KhoiLuongGiaCong> listKhoiLuongGiaCongDb = dbContext.KhoiLuongGiaCongs.ToList();

                foreach (var hamLuongVang in listHamLuongVangDb)
                {
                    double soDuDauKi = listSanPhamKhoCuDb
                        .Where(x => x.NgayNhap.Date < ngayDauKiDb.NgayDauKi.Date
                        && x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .Sum(x => x.KhoiLuongVang);
                    double khoiLuongGiaCongTruocNgayDauKi = listKhoiLuongGiaCongDb
                        .Where(x => x.NgayChuyen.Date < ngayDauKiDb.NgayDauKi.Date
                        && x.KhoCu.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .Sum(x => x.KhoiLuongVangTieuHao);

                    BangThongKeKhoCu bangThongKe = new BangThongKeKhoCu
                    {
                        MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                        KhoiLuongVangDauKi = Math.Round(soDuDauKi - khoiLuongGiaCongTruocNgayDauKi, 1),
                        KhoiLuongVangNhapVao = 0,
                        KhoiLuongVangCuMuaVao = 0,
                        KhoiLuongVangDoiNgangVao = 0,
                        KhoiLuongVangDemDiGiaCong = 0,
                        KhoiLuongVangCuoiKi = Math.Round(soDuDauKi - khoiLuongGiaCongTruocNgayDauKi, 1),
                        TongTienVangMuaVao = 0,
                        NgayThongKe = ngayDauKiDb.NgayDauKi
                    };

                    listOfBangThongKeKhoCu.Add(bangThongKe);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TuDongCapNhapThongKeThieu(List<BangThongKeKhoCu> listOfBangThongKeKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeKhoCu> listThongKeBiThieu = new List<BangThongKeKhoCu>();
                List<KiHieuHamLuongVang> listHamLuongVangDb = dbContext.KiHieuHamLuongVangs.ToList();

                // Kiểm tra nếu có hàm lượng vàng nào chưa từng có bảng thống kê kho cũ
                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    if (listOfBangThongKeKhoCu.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Count() == 0)
                    {
                        return false;
                    }
                }

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    List<BangThongKeKhoCu> listThongKeTheoHamLuongVang = listOfBangThongKeKhoCu
                        .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .OrderBy(x => x.NgayThongKe).ToList();
                    DateTime firstDay = listThongKeTheoHamLuongVang.First().NgayThongKe;
                    int totalDay = (int)(DateTime.Today.Date - firstDay.Date).TotalDays;

                    for (int i = 0; i <= totalDay; i++)
                    {
                        if (listThongKeTheoHamLuongVang.Where(x => x.NgayThongKe.Date == firstDay.AddDays(i).Date).Count() == 0)
                        {
                            BangThongKeKhoCu bangThongKeAdd = new BangThongKeKhoCu
                            {
                                KhoiLuongVangDauKi = 0,
                                KhoiLuongVangCuoiKi = 0,
                                KhoiLuongVangNhapVao = 0,
                                KhoiLuongVangDoiNgangVao = 0,
                                KhoiLuongVangCuMuaVao = 0,
                                KhoiLuongVangDemDiGiaCong = 0,
                                TongTienVangMuaVao = 0,
                                MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                                NgayThongKe = firstDay.AddDays(i)
                            };

                            listThongKeBiThieu.Add(bangThongKeAdd);
                        }
                    }
                }

                listOfBangThongKeKhoCu.AddRange(listThongKeBiThieu);

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TinhToanThongKe()
        {
            try
            {
                List<BangThongKeKhoCu> listOfBangThongKeKhoCu = new List<BangThongKeKhoCu>();

                if(!TaoDuLieuDauKiDauTien(listOfBangThongKeKhoCu) || !TuDongCapNhapThongKeThieu(listOfBangThongKeKhoCu))
                {
                    return false;
                }

                var dbContext = new QuanLyTiemVangDbContext();

                DateTime ngayThongKeHomNay = DateTime.Today;
                List<KiHieuHamLuongVang> listHamLuongVangDb = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPhamKhoCu> listSanPhamKhoCuDb = dbContext.SanPhamKhoCues.ToList();
                //List<BangThongKeKhoCu> listThongKeKhoCuDb = dbContext.BangThongKeKhoCues.ToList();
                List<KhoiLuongGiaCong> listKhoiLuongGiaCongDb = dbContext.KhoiLuongGiaCongs.ToList();
                List<LichSuDoiVang> listLichSuDoiVangDb = dbContext.LichSuDoiVangs.ToList();

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    List<BangThongKeKhoCu> listThongKeTheoHamLuongVang = listOfBangThongKeKhoCu
                        .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .OrderBy(x => x.NgayThongKe).ToList();
                    double khoiLuongVangCuoiKiCache = 0;

                    foreach (BangThongKeKhoCu bangThongKe in listThongKeTheoHamLuongVang)
                    {
                        double khoiLuongNhapVao = listSanPhamKhoCuDb
                            .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                            && x.NgayNhap.Date == bangThongKe.NgayThongKe.Date
                            && x.MaLichSuMua == null)
                            .Sum(x => x.KhoiLuongVang);

                        double khoiLuongVangCuMuaVao = listSanPhamKhoCuDb
                            .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                            && x.NgayNhap.Date == bangThongKe.NgayThongKe.Date
                            && x.MaLichSuMua != null
                            && x.LichSuMua.LichSuDoiVangs.Count == 0)
                            .Sum(x => x.KhoiLuongVang);

                        double khoiLuongDeLaMuaVao = listSanPhamKhoCuDb
                           .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                           && x.NgayNhap.Date == bangThongKe.NgayThongKe.Date
                           && x.MaLichSuMua != null
                           && x.LoaiVangMuaVao == Constant.VANG_CU_MUA_VAO
                           && x.LichSuMua.LichSuDoiVangs.Count > 0)
                           .Sum(x => x.KhoiLuongVang);

                        int tongTienDeLaMuaVao = listSanPhamKhoCuDb
                           .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                           && x.NgayNhap.Date == bangThongKe.NgayThongKe.Date
                           && x.MaLichSuMua != null
                           && x.LoaiVangMuaVao == Constant.VANG_CU_MUA_VAO
                           && x.LichSuMua.LichSuDoiVangs.Count > 0)
                           .Sum(x => x.GiaMuaVao);

                        double khoiLuongGiaCong = listKhoiLuongGiaCongDb
                            .Where(x => x.KhoCu.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                            && x.NgayChuyen.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.KhoiLuongVangTieuHao);

                        int tongTienVangMuaVao = listSanPhamKhoCuDb
                            .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                            && x.NgayNhap.Date == bangThongKe.NgayThongKe.Date
                            && x.MaLichSuMua != null
                            && x.LichSuMua.LichSuDoiVangs.Count == 0)
                            .Sum(x => x.GiaMuaVao);

                        List<LichSuDoiVang> listLichSuDoiVangTemp = listLichSuDoiVangDb
                            .Where(x => x.LichSuBan.SanPhams.Count > 0
                            && x.LichSuBan.SanPhams.FirstOrDefault().HamLuongVang == hamLuongVang.HamLuongVang
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date)
                            .ToList();

                        double khoiLuongVangCuDoiNgangMuaVao = 0;
                        int tongTienVangCuDoiNgangMuaVao = 0;

                        if (listLichSuDoiVangTemp.Count > 0)
                        {
                            foreach (LichSuDoiVang lichSuDoiVang in listLichSuDoiVangTemp)
                            {
                                double KLVMoiTruKLVCu = lichSuDoiVang.KLVMoiTruKLVCu;

                                if (KLVMoiTruKLVCu < 0)
                                {
                                    khoiLuongVangCuDoiNgangMuaVao += Math.Abs(KLVMoiTruKLVCu);
                                    tongTienVangCuDoiNgangMuaVao += Math.Abs(lichSuDoiVang.ThanhTienVangMoi);
                                }
                            }
                        }

                        double khoiLuongVangDoiNgangVao = listLichSuDoiVangDb
                            .Where(x => x.LichSuBan.SanPhams.Count > 0
                            && x.LichSuBan.SanPhams.FirstOrDefault().HamLuongVang == hamLuongVang.HamLuongVang
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => Math.Min(x.KhoiLuongVangCu, x.KhoiLuongVangMoi));

                        if (listThongKeTheoHamLuongVang.IndexOf(bangThongKe) > 0)
                        {
                            bangThongKe.KhoiLuongVangDauKi = Math.Round(khoiLuongVangCuoiKiCache, 1);
                        }

                        double khoiLuongDauKiHienTai = bangThongKe.KhoiLuongVangDauKi;

                        khoiLuongVangCuoiKiCache = khoiLuongDauKiHienTai
                            - khoiLuongGiaCong
                            + khoiLuongNhapVao
                            + khoiLuongVangCuMuaVao
                            + khoiLuongVangDoiNgangVao
                            + khoiLuongVangCuDoiNgangMuaVao
                            + khoiLuongDeLaMuaVao;

                        bangThongKe.KhoiLuongVangCuMuaVao = Math.Round(khoiLuongVangCuMuaVao + khoiLuongVangCuDoiNgangMuaVao + khoiLuongDeLaMuaVao, 1);
                        bangThongKe.KhoiLuongVangDoiNgangVao = Math.Round(khoiLuongVangDoiNgangVao, 1);
                        bangThongKe.KhoiLuongVangNhapVao = Math.Round(khoiLuongNhapVao, 1);
                        bangThongKe.KhoiLuongVangDemDiGiaCong = Math.Round(khoiLuongGiaCong, 1);
                        bangThongKe.KhoiLuongVangCuoiKi = Math.Round(khoiLuongVangCuoiKiCache, 1);
                        bangThongKe.TongTienVangMuaVao = tongTienVangMuaVao + tongTienVangCuDoiNgangMuaVao + tongTienDeLaMuaVao;
                    }
                }

                _listOfBangThongKeKhoCu = listOfBangThongKeKhoCu;

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<BangThongKeKhoCu> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var resultListOfBangThongKe = _listOfBangThongKeKhoCu.Where(x => x.NgayThongKe.Date == ngayThongKe.Date).ToList();

                foreach (var item in resultListOfBangThongKe)
                {
                    item.KiHieuHamLuongVang = dbContext.KiHieuHamLuongVangs.Find(item.MaHamLuongVang);
                }

                return resultListOfBangThongKe;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public List<BangThongKeKhoCu> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeKhoCu> listBangThongKe = _listOfBangThongKeKhoCu
                    .Where(x => tuNgay.Date <= x.NgayThongKe.Date && x.NgayThongKe.Date <= denNgay.Date)
                    .OrderBy(x => x.NgayThongKe)
                    .ToList();
                List<BangThongKeKhoCu> listBangThongKeReturn = new List<BangThongKeKhoCu>();
                List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
                {
                    BangThongKeKhoCu bangThongKe = new BangThongKeKhoCu
                    {
                        KhoiLuongVangCuMuaVao = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangCuMuaVao),
                        KhoiLuongVangCuoiKi = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Last().KhoiLuongVangCuoiKi,
                        KhoiLuongVangDauKi = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).First().KhoiLuongVangDauKi,
                        KhoiLuongVangDemDiGiaCong = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangDemDiGiaCong),
                        KhoiLuongVangDoiNgangVao = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangDoiNgangVao),
                        KhoiLuongVangNhapVao = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangNhapVao),
                        KiHieuHamLuongVang = hamLuongVang,
                        MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                        TongTienVangMuaVao = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienVangMuaVao)
                    };

                    listBangThongKeReturn.Add(bangThongKe);
                }

                return listBangThongKeReturn;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
