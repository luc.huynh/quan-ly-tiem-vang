﻿using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangThongKeTienMatDAO
    {
        private static BangThongKeTienMatDAO instance;

        public static BangThongKeTienMatDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BangThongKeTienMatDAO();
                }
                return instance;
            }
        }

        private List<BangThongKeTienMat> _listOfBangThongKeTienMat = new List<BangThongKeTienMat>();

        public BangThongKeTienMat Insert(BangThongKeTienMat bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeTienMats.Add(bangThongKe);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeTienMat Update(BangThongKeTienMat bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeTienMats.Find(bangThongKe.MaBangThongKeTienMat);

                dbContext.Entry(entity).CurrentValues.SetValues(bangThongKe);
                dbContext.SaveChanges();

                return bangThongKe;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeTienMat DelelteById(int maBangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeTienMats.Remove(dbContext.BangThongKeTienMats.Find(maBangThongKe));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public bool TaoDuLieuDauKiDauTien(List<BangThongKeTienMat> listOfBangThongKeTienMat)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                if (dbContext.NgayDauKiThongKes.ToList().Count == 0)
                {
                    return false;
                }

                NgayDauKiThongKe ngayDauKiDb = dbContext.NgayDauKiThongKes.ToList().First();

                BangThongKeTienMat bangThongKe = new BangThongKeTienMat
                {
                    NgayThongKe = ngayDauKiDb.NgayDauKi.Date,
                    SoDuDauKi = ngayDauKiDb.SoTienDauKi,
                    SoDuCuoiKi = ngayDauKiDb.SoTienDauKi,
                    TongChiTienCamDo = 0,
                    TongPhatSinhKhacChiRa = 0,
                    TongPhatSinhKhacThuVao = 0,
                    TongThuNoGocCamDo = 0,
                    TongThuTienLaiCamDo = 0,
                    TongTienBot = 0,
                    TongTienChiRa = 0,
                    TongTienCong = 0,
                    TongTienDoiNgoaiTeChi = 0,
                    TongTienDoiNgoaiTeThu = 0,
                    TongTienThuVao = 0,
                    TongTienVangBanRa = 0,
                    TongTienVangMuaVao = 0
                };

                listOfBangThongKeTienMat.Add(bangThongKe);

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TuDongCapNhapThongKeThieu(List<BangThongKeTienMat> listOfBangThongKeTienMat)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                List<BangThongKeTienMat> listThongKeBiThieu = new List<BangThongKeTienMat>();

                NgayDauKiThongKe ngayDauKi = dbContext.NgayDauKiThongKes.OrderByDescending(x => x.NgayChinhSua).First();
                DateTime firstDay = ngayDauKi.NgayDauKi;
                int totalDay = (int)(DateTime.Today.Date - firstDay.Date).TotalDays;

                for (int i = 0; i <= totalDay; i++)
                {
                    if (listOfBangThongKeTienMat.Where(x => x.NgayThongKe.Date == firstDay.AddDays(i).Date).Count() == 0)
                    {
                        BangThongKeTienMat bangThongKeAdd = new BangThongKeTienMat
                        {
                            NgayThongKe = firstDay.AddDays(i),
                            SoDuDauKi = ngayDauKi.SoTienDauKi,
                            SoDuCuoiKi = 0,
                            TongChiTienCamDo = 0,
                            TongPhatSinhKhacChiRa = 0,
                            TongPhatSinhKhacThuVao = 0,
                            TongThuNoGocCamDo = 0,
                            TongThuTienLaiCamDo = 0,
                            TongTienBot = 0,
                            TongTienChiRa = 0,
                            TongTienCong = 0,
                            TongTienDoiNgoaiTeChi = 0,
                            TongTienDoiNgoaiTeThu = 0,
                            TongTienThuVao = 0,
                            TongTienVangBanRa = 0,
                            TongTienVangMuaVao = 0
                        };

                        listThongKeBiThieu.Add(bangThongKeAdd);
                    }
                }

                listOfBangThongKeTienMat.AddRange(listThongKeBiThieu);

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TinhToanThongKe()
        {
            try
            {
                List<BangThongKeTienMat> listOfBangThongKeTienMat = new List<BangThongKeTienMat>();

                if (!TaoDuLieuDauKiDauTien(listOfBangThongKeTienMat) || !TuDongCapNhapThongKeThieu(listOfBangThongKeTienMat))
                {
                    return false;
                }

                var dbContext = new QuanLyTiemVangDbContext();
                DateTime ngayThongKeHomNay = DateTime.Today;

                listOfBangThongKeTienMat = listOfBangThongKeTienMat.OrderBy(x => x.NgayThongKe).ToList();
                List<SanPham> listSanPhamDb = dbContext.SanPhams.ToList();
                List<SanPhamKhoCu> listSanPhamKhoCuDb = dbContext.SanPhamKhoCues.ToList();
                List<LichSuBan> listLichSuBanDb = dbContext.LichSuBans.ToList();
                List<LichSuMua> listLichSuMuaDb = dbContext.LichSuMuas.ToList();
                List<LichSuDoiVang> listLichSuDoiVangDb = dbContext.LichSuDoiVangs.ToList();
                List<HopDongCamDo> listHopDongCamDoDb = dbContext.HopDongCamDoes.ToList();
                List<ChiTietCamDo> listChiTietCamDoDb = dbContext.ChiTietCamDoes.ToList();
                List<LichSuDoiNgoaiTe> listLichSuDoiNgoaiTeDb = dbContext.LichSuDoiNgoaiTes.ToList();
                List<PhieuPhatSinhThuChi> listPhieuPhatSinhThuChi = dbContext.PhieuPhatSinhThuChis.ToList();

                if (!BangThongKeKhoCuDAO.Instance.TinhToanThongKe() || !BangThongKeTheoHamLuongVangDAO.Instance.TinhToanThongKe())
                {
                    return false;
                }

                List<BangThongKeKhoCu> listBangThongKeKhoCuDb = BangThongKeKhoCuDAO.Instance._listOfBangThongKeKhoCu;
                List<BangThongKeTheoHamLuongVang> listBangThongKeHamLuongVangDb = BangThongKeTheoHamLuongVangDAO.Instance._listOfBangThongKeTheoHamLuongVang;
                
                int soDuDauKiCache = 0;

                foreach (BangThongKeTienMat bangThongKe in listOfBangThongKeTienMat)
                {
                    int tongTienVangBanRa = listBangThongKeHamLuongVangDb
                        .Where(x => x.NgayThongKe.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.TongTienVangBanRa);

                    int tongTienVangMuaVao = listBangThongKeKhoCuDb
                        .Where(x => x.NgayThongKe.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.TongTienVangMuaVao);

                    int tongTienCong = listSanPhamDb
                        .Where(x => x.MaLichSuBan != null
                        && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                        && x.LichSuBan.LichSuDoiVangs.Count == 0)
                        .Sum(x => x.TienCong)
                        +
                        listLichSuDoiVangDb
                        .Where(x => x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.TienCongThem);

                    int tongTienCongThemKhiMuaVang = listLichSuMuaDb
                        .Where(x => x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.TienCongThem);

                    int tongTienBotBanVang = listLichSuBanDb
                        .Where(x => x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.TienBot);

                    int tongChiTienCamDo = listHopDongCamDoDb
                        .Where(x => x.NgayInMoiHopDong.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.SoTienCam);

                    int tongThuTienLaiCamDo = listChiTietCamDoDb
                        .Where(x => x.NgayNopTienLai.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.SoTienDongLai);

                    int tongThuNoGocCamDo = listHopDongCamDoDb
                        .Where(x => x.NgayThanhLy != null && x.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date)
                        .Sum(x => x.SoTienThanhLy);

                    int tongTienDoiNgoaiTeThuVao = listLichSuDoiNgoaiTeDb
                        .Where(x => x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                        && x.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.BAN_NGOAI_TE)
                        .Sum(x => x.SoTienDoi);

                    int tongTienDoiNgoaiTeChiRa = listLichSuDoiNgoaiTeDb
                        .Where(x => x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                        && x.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.THU_MUA_NGOAI_TE)
                        .Sum(x => x.SoTienKhachNhan);

                    int tongPhatSinhKhacThuVao = listPhieuPhatSinhThuChi
                        .Where(x => x.NgayXuatPhieu.Date == bangThongKe.NgayThongKe.Date
                        && x.LoaiPhieuPhatSinhThuChi.TenLoaiPhieu.Equals(PhatSinhThuChiConstant.PHIEU_THU))
                        .Sum(x => x.SoTien);

                    int tongPhatSinhKhacChiRa = listPhieuPhatSinhThuChi
                        .Where(x => x.NgayXuatPhieu.Date == bangThongKe.NgayThongKe.Date
                        && x.LoaiPhieuPhatSinhThuChi.TenLoaiPhieu.Equals(PhatSinhThuChiConstant.PHIEU_CHI))
                        .Sum(x => x.SoTien);

                    if (listOfBangThongKeTienMat.IndexOf(bangThongKe) > 0)
                    {
                        bangThongKe.SoDuDauKi = soDuDauKiCache;
                    }

                    int soDuDauKiHienTai = bangThongKe.SoDuDauKi;
                    int tongThu = tongTienVangBanRa + tongTienCong + tongThuTienLaiCamDo + tongThuNoGocCamDo + tongTienDoiNgoaiTeThuVao + tongPhatSinhKhacThuVao;
                    int tongChi = tongTienVangMuaVao + tongTienBotBanVang + tongChiTienCamDo + tongTienDoiNgoaiTeChiRa + tongPhatSinhKhacChiRa + tongTienCongThemKhiMuaVang;

                    soDuDauKiCache = soDuDauKiHienTai + tongThu - tongChi;

                    bangThongKe.SoDuCuoiKi = soDuDauKiCache;
                    bangThongKe.TongChiTienCamDo = tongChiTienCamDo;
                    bangThongKe.TongPhatSinhKhacChiRa = tongPhatSinhKhacChiRa;
                    bangThongKe.TongPhatSinhKhacThuVao = tongPhatSinhKhacThuVao;
                    bangThongKe.TongThuNoGocCamDo = tongThuNoGocCamDo;
                    bangThongKe.TongThuTienLaiCamDo = tongThuTienLaiCamDo;
                    bangThongKe.TongTienBot = tongTienBotBanVang + tongTienCongThemKhiMuaVang;
                    bangThongKe.TongTienChiRa = tongChi;
                    bangThongKe.TongTienCong = tongTienCong;
                    bangThongKe.TongTienDoiNgoaiTeChi = tongTienDoiNgoaiTeChiRa;
                    bangThongKe.TongTienDoiNgoaiTeThu = tongTienDoiNgoaiTeThuVao;
                    bangThongKe.TongTienThuVao = tongThu;
                    bangThongKe.TongTienVangBanRa = tongTienVangBanRa;
                    bangThongKe.TongTienVangMuaVao = tongTienVangMuaVao;
                }

                _listOfBangThongKeTienMat = listOfBangThongKeTienMat;

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<BangThongKeTienMat> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            try
            {
                return _listOfBangThongKeTienMat.Where(x => x.NgayThongKe.Date == ngayThongKe.Date).ToList();
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public List<BangThongKeTienMat> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeTienMat> listBangThongKe = _listOfBangThongKeTienMat
                    .Where(x => tuNgay.Date <= x.NgayThongKe.Date && x.NgayThongKe.Date <= denNgay.Date)
                    .OrderBy(x => x.NgayThongKe)
                    .ToList();
                List<BangThongKeTienMat> listBangThongKeReturn = new List<BangThongKeTienMat>();

                BangThongKeTienMat bangThongKe = new BangThongKeTienMat
                {
                    SoDuCuoiKi = listBangThongKe.Last().SoDuCuoiKi,
                    SoDuDauKi = listBangThongKe.First().SoDuDauKi,
                    TongChiTienCamDo = listBangThongKe.Sum(x => x.TongChiTienCamDo),
                    TongPhatSinhKhacChiRa = listBangThongKe.Sum(x => x.TongPhatSinhKhacChiRa),
                    TongPhatSinhKhacThuVao = listBangThongKe.Sum(x => x.TongPhatSinhKhacThuVao),
                    TongThuNoGocCamDo = listBangThongKe.Sum(x => x.TongThuNoGocCamDo),
                    TongThuTienLaiCamDo = listBangThongKe.Sum(x => x.TongThuTienLaiCamDo),
                    TongTienBot = listBangThongKe.Sum(x => x.TongTienBot),
                    TongTienChiRa = listBangThongKe.Sum(x => x.TongTienChiRa),
                    TongTienCong = listBangThongKe.Sum(x => x.TongTienCong),
                    TongTienDoiNgoaiTeChi = listBangThongKe.Sum(x => x.TongTienDoiNgoaiTeChi),
                    TongTienDoiNgoaiTeThu = listBangThongKe.Sum(x => x.TongTienDoiNgoaiTeThu),
                    TongTienThuVao = listBangThongKe.Sum(x => x.TongTienThuVao),
                    TongTienVangBanRa = listBangThongKe.Sum(x => x.TongTienVangBanRa),
                    TongTienVangMuaVao = listBangThongKe.Sum(x => x.TongTienVangMuaVao)
                };

                listBangThongKeReturn.Add(bangThongKe);

                return listBangThongKeReturn;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
