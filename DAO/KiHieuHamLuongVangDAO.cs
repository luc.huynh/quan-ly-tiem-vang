﻿using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class KiHieuHamLuongVangDAO
    {
        private static KiHieuHamLuongVangDAO instance;

        public static KiHieuHamLuongVangDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new KiHieuHamLuongVangDAO();
                return instance;
            }
        }

        public List<KiHieuHamLuongVang> GetListAllHamLuongVang()
        {
            return new QuanLyTiemVangDbContext().KiHieuHamLuongVangs.ToList();
        }

        public bool HaveHamLuongVang(string hamLuongVang)
        {
            if (new QuanLyTiemVangDbContext().KiHieuHamLuongVangs.Where(x => x.HamLuongVang.Equals(hamLuongVang)).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public KiHieuHamLuongVang Insert(KiHieuHamLuongVang hamLuongVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KiHieuHamLuongVangs.Add(hamLuongVang);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public bool Update(KiHieuHamLuongVang hamLuongVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KiHieuHamLuongVangs.Find(hamLuongVang.MaHamLuongVang);
                dbContext.Entry(entity).CurrentValues.SetValues(hamLuongVang);
                dbContext.SaveChanges();
                if (entity != null || entity.MaHamLuongVang > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool DelelteById(int idHamLuongVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.KiHieuHamLuongVangs.Remove(dbContext.KiHieuHamLuongVangs.Find(idHamLuongVang));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }
    }
}
