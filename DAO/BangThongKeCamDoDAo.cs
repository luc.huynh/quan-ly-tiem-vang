﻿using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangThongKeCamDoDAO
    {
        private static BangThongKeCamDoDAO instance;

        public static BangThongKeCamDoDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BangThongKeCamDoDAO();
                }
                return instance;
            }
        }

        public List<BangThongKeCamDo> _listOfBangThongKeCamDo = new List<BangThongKeCamDo>();

        public BangThongKeCamDo Insert(BangThongKeCamDo bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeCamDoes.Add(bangThongKe);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeCamDo Update(BangThongKeCamDo bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeCamDoes.Find(bangThongKe.MaBangThongKeCamDo);

                dbContext.Entry(entity).CurrentValues.SetValues(bangThongKe);
                dbContext.SaveChanges();

                return bangThongKe;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeCamDo DelelteById(int maBangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeCamDoes.Remove(dbContext.BangThongKeCamDoes.Find(maBangThongKe));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public bool TaoDuLieuDauKiDauTien(List<BangThongKeCamDo> listOfBangThongKeCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                if (dbContext.NgayDauKiThongKes.ToList().Count == 0)
                {
                    return false;
                }

                NgayDauKiThongKe ngayDauKiDb = dbContext.NgayDauKiThongKes.ToList().First();

                List<KiHieuHamLuongVang> listHamLuongVangDb = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPhamCamDo> listSanPhamCamDoDb = dbContext.SanPhamCamDoes.ToList().Where(x => x.NgayNgap.Date < ngayDauKiDb.NgayDauKi.Date).ToList();

                foreach (var hamLuongVang in listHamLuongVangDb)
                {
                    BangThongKeCamDo bangThongKe = new BangThongKeCamDo
                    {
                        MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                        NgayThongKe = ngayDauKiDb.NgayDauKi.Date,
                        TongTienCamDo = 0,
                        TongTienCamGiaHan = 0,
                        TongTienLaiGiaHan = 0,
                        TongTienLaiThanhLy = 0,
                        TongTienThanhLy = 0,
                        TongTienThanhToan = 0,
                        TongTienLaiThanhToan = 0,
                        TongTrongLuongCamDo = 0,
                        TongTrongLuongGiaHan = 0,
                        TongTrongLuongThanhLy = 0,
                        TongTrongLuongThanhToan = 0
                    };

                    listOfBangThongKeCamDo.Add(bangThongKe);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TuDongCapNhapThongKeThieu(List<BangThongKeCamDo> listOfBangThongKeCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeCamDo> listThongKeBiThieu = new List<BangThongKeCamDo>();
                List<KiHieuHamLuongVang> listHamLuongVangDb = dbContext.KiHieuHamLuongVangs.ToList();

                // Kiểm tra nếu có hàm lượng vàng nào chưa từng có bảng thống kê kho cũ
                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    if (listOfBangThongKeCamDo.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Count() == 0)
                    {
                        return false;
                    }
                }

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    List<BangThongKeCamDo> listThongKeTheoHamLuongVang = listOfBangThongKeCamDo
                        .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .OrderBy(x => x.NgayThongKe).ToList();
                    DateTime firstDay = listThongKeTheoHamLuongVang.First().NgayThongKe;
                    int totalDay = (int)(DateTime.Today.Date - firstDay.Date).TotalDays;

                    for (int i = 0; i <= totalDay; i++)
                    {
                        if (listThongKeTheoHamLuongVang.Where(x => x.NgayThongKe.Date == firstDay.AddDays(i).Date).Count() == 0)
                        {
                            BangThongKeCamDo bangThongKeAdd = new BangThongKeCamDo
                            {
                                TongTienCamDo = 0,
                                TongTienCamGiaHan = 0,
                                TongTienLaiGiaHan = 0,
                                TongTienLaiThanhLy = 0,
                                TongTienThanhLy = 0,
                                TongTienThanhToan = 0,
                                TongTienLaiThanhToan = 0,
                                TongTrongLuongCamDo = 0,
                                TongTrongLuongGiaHan = 0,
                                TongTrongLuongThanhLy = 0,
                                TongTrongLuongThanhToan = 0,
                                MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                                NgayThongKe = firstDay.AddDays(i)
                            };

                            listThongKeBiThieu.Add(bangThongKeAdd);
                        }
                    }
                }

                listOfBangThongKeCamDo.AddRange(listThongKeBiThieu);

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TinhToanThongKe()
        {
            try
            {
                List<BangThongKeCamDo> listOfBangThongKeCamDo = new List<BangThongKeCamDo>();
                var dbContext = new QuanLyTiemVangDbContext();

                if (!TaoDuLieuDauKiDauTien(listOfBangThongKeCamDo) || !TuDongCapNhapThongKeThieu(listOfBangThongKeCamDo))
                {
                    return false;
                }

                DateTime ngayThongKeHomNay = DateTime.Today;
                List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPhamCamDo> listSanPhamCamDoDb = dbContext.SanPhamCamDoes.ToList();
                List<HopDongCamDo> listHopDongCamDoDb = dbContext.HopDongCamDoes.ToList();
                List<ChiTietCamDo> listChiTietCamDoDb = dbContext.ChiTietCamDoes.ToList();

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
                {
                    List<BangThongKeCamDo> listThongKeTheoHamLuongVang = listOfBangThongKeCamDo
                        .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .OrderBy(x => x.NgayThongKe).ToList();

                    foreach (BangThongKeCamDo bangThongKe in listThongKeTheoHamLuongVang)
                    {
                        double tongTrongLuongCamDo = listSanPhamCamDoDb
                            .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                            && x.NgayNgap.Date == bangThongKe.NgayThongKe.Date
                            && x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.CHUA_THANH_TOAN)
                            .Sum(x => x.CanNang);

                        int tongTienCamDo = listHopDongCamDoDb
                            .Where(x => x.NgayInMoiHopDong.Date == bangThongKe.NgayThongKe.Date
                            && (x.TrangThaiHopDong.Equals(HopDongCamDoConstant.CHUA_THANH_TOAN)
                            || (x.TrangThaiHopDong.Equals(HopDongCamDoConstant.DA_THANH_TOAN) && x.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date)))
                            .Sum(x => x.SoTienCam);

                        double tongTrongLuongThanhToan = listSanPhamCamDoDb
                            .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang
                            && x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.DA_THANH_TOAN
                            && x.HopDongCamDo.NgayThanhLy != null
                            && x.HopDongCamDo.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.CanNang);

                        int tongTienThanhToan = listHopDongCamDoDb
                            .Where(x => x.TrangThaiHopDong == HopDongCamDoConstant.DA_THANH_TOAN
                            && x.NgayThanhLy != null
                            && x.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.SoTienCam);

                        int tongTienLaiThanhToan = listChiTietCamDoDb
                            .Where(x => x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.DA_THANH_TOAN
                            && x.HopDongCamDo.NgayThanhLy != null
                            && x.HopDongCamDo.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date
                            && x.NgayNopTienLai.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.SoTienDongLai);

                        double tongTrongLuongThanhLy = listSanPhamCamDoDb
                            .Where(x => x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.DA_THANH_LY
                            && x.HopDongCamDo.NgayThanhLy != null
                            && x.HopDongCamDo.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.CanNang);

                        int tongTienLaiThanhLy = listChiTietCamDoDb
                            .Where(x => x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.DA_THANH_LY
                            && x.HopDongCamDo.NgayThanhLy != null
                            && x.HopDongCamDo.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date
                            && x.NgayNopTienLai.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.SoTienDongLai);

                        int tongTienThanhLy = listHopDongCamDoDb
                            .Where(x => x.TrangThaiHopDong == HopDongCamDoConstant.DA_THANH_LY
                            && x.NgayThanhLy != null
                            && x.NgayThanhLy.Value.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.SoTienThanhLy);

                        double tongTrongLuongGiaHan = listChiTietCamDoDb
                            .Where(x => x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.CHUA_THANH_TOAN
                            && x.NgayNopTienLai.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.HopDongCamDo.SanPhamCamDoes.Where(o => o.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(y => y.CanNang));

                        int tongTienCamGiaHan = listChiTietCamDoDb
                            .Where(x => x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.CHUA_THANH_TOAN
                            && x.NgayNopTienLai.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.HopDongCamDo.SoTienCam);

                        int tongTienLaiGiaHan = listChiTietCamDoDb
                            .Where(x => x.HopDongCamDo.TrangThaiHopDong == HopDongCamDoConstant.CHUA_THANH_TOAN
                            && x.NgayNopTienLai.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.SoTienDongLai);

                        // Cập nhập dữ liệu bảng thống kê
                        bangThongKe.TongTienCamDo = tongTienCamDo;
                        bangThongKe.TongTienCamGiaHan = tongTienCamGiaHan;
                        bangThongKe.TongTienLaiGiaHan = tongTienLaiGiaHan;
                        bangThongKe.TongTienLaiThanhLy = tongTienLaiThanhLy;
                        bangThongKe.TongTienThanhLy = tongTienThanhLy;
                        bangThongKe.TongTienThanhToan = tongTienThanhToan;
                        bangThongKe.TongTienLaiThanhToan = tongTienLaiThanhToan;
                        bangThongKe.TongTrongLuongCamDo = tongTrongLuongCamDo;
                        bangThongKe.TongTrongLuongGiaHan = tongTrongLuongGiaHan;
                        bangThongKe.TongTrongLuongThanhLy = tongTrongLuongThanhLy;
                        bangThongKe.TongTrongLuongThanhToan = tongTrongLuongThanhToan;
                    }
                }

                _listOfBangThongKeCamDo = listOfBangThongKeCamDo;

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<BangThongKeCamDo> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var resultListOfBangThongKe = _listOfBangThongKeCamDo.Where(x => x.NgayThongKe.Date == ngayThongKe.Date).ToList();

                foreach (var item in resultListOfBangThongKe)
                {
                    item.KiHieuHamLuongVang = dbContext.KiHieuHamLuongVangs.Find(item.MaHamLuongVang);
                }

                return resultListOfBangThongKe;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public List<BangThongKeCamDo> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeCamDo> listBangThongKe = _listOfBangThongKeCamDo
                    .Where(x => tuNgay.Date <= x.NgayThongKe.Date && x.NgayThongKe.Date <= denNgay.Date)
                    .OrderBy(x => x.NgayThongKe)
                    .ToList();
                List<BangThongKeCamDo> listBangThongKeReturn = new List<BangThongKeCamDo>();
                List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
                {
                    BangThongKeCamDo bangThongKe = new BangThongKeCamDo
                    {
                        TongTienCamDo = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienCamDo),
                        TongTienCamGiaHan = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienCamGiaHan),
                        TongTienLaiGiaHan = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienLaiGiaHan),
                        TongTienLaiThanhLy = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienLaiThanhLy),
                        TongTienThanhLy = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienThanhLy),
                        TongTienThanhToan = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienThanhToan),
                        TongTienLaiThanhToan = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienLaiThanhToan),
                        TongTrongLuongCamDo = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTrongLuongCamDo),
                        TongTrongLuongGiaHan = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTrongLuongGiaHan),
                        TongTrongLuongThanhLy = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTrongLuongThanhLy),
                        TongTrongLuongThanhToan = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTrongLuongThanhToan),
                        KiHieuHamLuongVang = hamLuongVang
                    };

                    listBangThongKeReturn.Add(bangThongKe);
                }

                return listBangThongKeReturn;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
