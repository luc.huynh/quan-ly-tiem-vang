﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LichSuBanDAO
    {
        private static LichSuBanDAO instance;

        public static LichSuBanDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuBanDAO();
                return instance;
            }
        }

        public List<LichSuBan> GetList()
        {
            return new QuanLyTiemVangDbContext().LichSuBans.ToList();
        }

        public List<LichSuBan> GetListLichSuBan()
        {
            var dbContext = new QuanLyTiemVangDbContext();
            var listMaLichSuVangDoiVang = new List<String>();
            var listLichSuVangDoiVang = dbContext.LichSuDoiVangs.ToList();

            foreach(var lichSuVangDoiVang in listLichSuVangDoiVang)
            {
                listMaLichSuVangDoiVang.Add(lichSuVangDoiVang.MaLichSuBan);
            }

            return dbContext.LichSuBans.Where(x => !listMaLichSuVangDoiVang.Contains(x.MaLichSuBan)).ToList();
        }

        public LichSuBan GetLichSuBanById(string maLichSuBan)
        {
            return new QuanLyTiemVangDbContext().LichSuBans.Find(maLichSuBan);
        }

        public LichSuBan ThanhToanHoaDon(LichSuBan lichSuBan, List<SanPham> listSanPhamBanRa)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuBans.Add(lichSuBan);

                foreach (SanPham sanPham in listSanPhamBanRa)
                {
                    dbContext.SanPhams.Find(sanPham.MaSanPham).MaLichSuBan = entity.MaLichSuBan;
                }

                dbContext.SaveChanges();
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public bool XoaHoaDon(LichSuBan lichSuBan)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                foreach (SanPham sanPham in lichSuBan.SanPhams)
                {
                    dbContext.SanPhams.Find(sanPham.MaSanPham).MaLichSuBan = null;
                }

                dbContext.LichSuBans.Remove(dbContext.LichSuBans.Find(lichSuBan.MaLichSuBan));
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
