﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LichSuMuaDAO
    {
        private static LichSuMuaDAO instance;

        public static LichSuMuaDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuMuaDAO();
                return instance;
            }
        }

        public string Insert(LichSuMua lichSuMua)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuMuas.Add(lichSuMua);
                dbContext.SaveChanges();
                return entity.MaLichSuMua;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public List<LichSuMua> GetListLichSuMua()
        {
            var dbContext = new QuanLyTiemVangDbContext();
            var listMaLichSuVangDoiVang = new List<String>();
            var listLichSuVangDoiVang = dbContext.LichSuDoiVangs.ToList();

            foreach (var lichSuVangDoiVang in listLichSuVangDoiVang)
            {
                listMaLichSuVangDoiVang.Add(lichSuVangDoiVang.MaLichSuMua);
            }

            return dbContext.LichSuMuas.Where(x => !listMaLichSuVangDoiVang.Contains(x.MaLichSuMua)).ToList();
        }

        public LichSuMua GetLichSuMuaById(string maLichSuMua)
        {
            return new QuanLyTiemVangDbContext().LichSuMuas.Find(maLichSuMua);
        }

        public LichSuMua ThanhToanHoaDon(LichSuMua lichSuMua, List<SanPhamKhoCu> listSanPhamMuaVao)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuMuas.Add(lichSuMua);

                dbContext.SanPhamKhoCues.AddRange(listSanPhamMuaVao);
                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool XoaHoaDon(LichSuMua lichSuMua)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<SanPhamKhoCu> listSanPhamThuocLichSuMua = dbContext.SanPhamKhoCues.Where(x => x.MaLichSuMua.Equals(lichSuMua.MaLichSuMua)).ToList();

                foreach (SanPhamKhoCu sanPham in listSanPhamThuocLichSuMua)
                {
                    dbContext.SanPhamKhoCues.Remove(dbContext.SanPhamKhoCues.Find(sanPham.MaSanPhamKhoCu));
                }

                dbContext.LichSuMuas.Remove(dbContext.LichSuMuas.Find(lichSuMua.MaLichSuMua));
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
