﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ChiTietPhieuGiaCongDAO
    {
        #region instance
        private static ChiTietPhieuGiaCongDAO instance;

        public static ChiTietPhieuGiaCongDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new ChiTietPhieuGiaCongDAO();
                return instance;
            }
        }
        #endregion

        public List<ChiTietPhieuGiaCong> GetListByMaPhieuGiaCong(int maPhieuGiaCong)
        {
            return new QuanLyTiemVangDbContext().ChiTietPhieuGiaCongs.Where(x => x.MaPhieuGiaCong == maPhieuGiaCong).ToList();
        }

        public ChiTietPhieuGiaCong Insert(ChiTietPhieuGiaCong chiTietPhieuGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.ChiTietPhieuGiaCongs.Add(chiTietPhieuGiaCong);

                List<ChiTietPhieuGiaCong> listOfChiTietPhieuGiaCong = 
                    dbContext.ChiTietPhieuGiaCongs
                    .Where(x => x.MaPhieuGiaCong == chiTietPhieuGiaCong.MaPhieuGiaCong)
                    .ToList();

                listOfChiTietPhieuGiaCong.Add(entity);
                
                // Tính xem đã đủ khối lượng bàn giao của phiếu gia công này chưa
                PhieuGiaCong phieuGiaCong = dbContext.PhieuGiaCongs.Find(chiTietPhieuGiaCong.MaPhieuGiaCong);
                double tongTiLeHot = listOfChiTietPhieuGiaCong.Sum(x => x.TiLeHotHoanThanh + x.TiLeHotTruHao);
                double tongKhoiLuongTinh = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongTongHoanThanh + x.KhoiLuongTongTruHao);
                double tongKhoiLuongVang = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongVangHoanThanh + x.KhoiLuongVangTruHao);
                double tongKhoiLuongDa = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongDaHoanThanh + x.KhoiLuongDaTruHao);

                if (tongTiLeHot.CompareTo(phieuGiaCong.TiLeHotBanGiao) > 0 ||
                    tongKhoiLuongTinh.CompareTo(phieuGiaCong.KhoiLuongTongBanGiao) > 0 ||
                    tongKhoiLuongVang.CompareTo(phieuGiaCong.KhoiLuongVangBanGiao) > 0 ||
                    tongKhoiLuongDa.CompareTo(phieuGiaCong.KhoiLuongDaBanGiao) > 0)
                {
                    return null;
                }

                if (tongTiLeHot.CompareTo(phieuGiaCong.TiLeHotBanGiao) == 0 ||
                    tongKhoiLuongTinh.CompareTo(phieuGiaCong.KhoiLuongTongBanGiao) == 0 ||
                    tongKhoiLuongVang.CompareTo(phieuGiaCong.KhoiLuongVangBanGiao) == 0 ||
                    tongKhoiLuongDa.CompareTo(phieuGiaCong.KhoiLuongDaBanGiao) == 0)
                {
                    phieuGiaCong.NgayKetThuc = DateTime.Now;
                }

                phieuGiaCong.TiLeHotTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.TiLeHotTruHao);
                phieuGiaCong.KhoiLuongTongTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongTongTruHao);
                phieuGiaCong.KhoiLuongVangTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongVangTruHao);
                phieuGiaCong.KhoiLuongDaTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongDaTruHao);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public ChiTietPhieuGiaCong Update(ChiTietPhieuGiaCong chiTietPhieuGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.ChiTietPhieuGiaCongs.Find(chiTietPhieuGiaCong.MaChiTietPhieuGiaCong);

                dbContext.Entry(entity).CurrentValues.SetValues(chiTietPhieuGiaCong);

                List<ChiTietPhieuGiaCong> listOfChiTietPhieuGiaCong = 
                    dbContext.ChiTietPhieuGiaCongs
                    .Where(x => x.MaPhieuGiaCong == chiTietPhieuGiaCong.MaPhieuGiaCong)
                    .ToList();

                listOfChiTietPhieuGiaCong.Remove(entity);
                listOfChiTietPhieuGiaCong.Add(chiTietPhieuGiaCong);

                // Tính xem đã đủ khối lượng bàn giao của phiếu gia công này chưa
                PhieuGiaCong phieuGiaCong = dbContext.PhieuGiaCongs.Find(chiTietPhieuGiaCong.MaPhieuGiaCong);
                double tongTiLeHot = listOfChiTietPhieuGiaCong.Sum(x => x.TiLeHotHoanThanh + x.TiLeHotTruHao);
                double tongKhoiLuongTinh = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongTongHoanThanh + x.KhoiLuongTongTruHao);
                double tongKhoiLuongVang = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongVangHoanThanh + x.KhoiLuongVangTruHao);
                double tongKhoiLuongDa = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongDaHoanThanh + x.KhoiLuongDaTruHao);

                if (tongTiLeHot.CompareTo(phieuGiaCong.TiLeHotBanGiao) > 0 ||
                    tongKhoiLuongTinh.CompareTo(phieuGiaCong.KhoiLuongTongBanGiao) > 0 ||
                    tongKhoiLuongVang.CompareTo(phieuGiaCong.KhoiLuongVangBanGiao) > 0 ||
                    tongKhoiLuongDa.CompareTo(phieuGiaCong.KhoiLuongDaBanGiao) > 0)
                {
                    return null;
                }

                // Giả sử rằng khối lượng chưa đủ, set lại mặc định cho ngày kết thúc
                phieuGiaCong.NgayKetThuc = null;

                if (tongTiLeHot.CompareTo(phieuGiaCong.TiLeHotBanGiao) == 0 ||
                    tongKhoiLuongTinh.CompareTo(phieuGiaCong.KhoiLuongTongBanGiao) == 0 ||
                    tongKhoiLuongVang.CompareTo(phieuGiaCong.KhoiLuongVangBanGiao) == 0 ||
                    tongKhoiLuongDa.CompareTo(phieuGiaCong.KhoiLuongDaBanGiao) == 0)
                {
                    phieuGiaCong.NgayKetThuc = DateTime.Now;
                }

                phieuGiaCong.TiLeHotTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.TiLeHotTruHao);
                phieuGiaCong.KhoiLuongTongTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongTongTruHao);
                phieuGiaCong.KhoiLuongVangTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongVangTruHao);
                phieuGiaCong.KhoiLuongDaTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongDaTruHao);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public ChiTietPhieuGiaCong DeleteById(int maChiTietPhieuGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                ChiTietPhieuGiaCong entity = dbContext.ChiTietPhieuGiaCongs.Find(maChiTietPhieuGiaCong);

                dbContext.ChiTietPhieuGiaCongs.Remove(entity);

                List<ChiTietPhieuGiaCong> listOfChiTietPhieuGiaCong = 
                    dbContext.ChiTietPhieuGiaCongs
                    .Where(x => x.MaPhieuGiaCong == entity.MaPhieuGiaCong).ToList();

                // Tính xem đã đủ khối lượng bàn giao của phiếu gia công này chưa
                PhieuGiaCong phieuGiaCong = dbContext.PhieuGiaCongs.Find(entity.MaPhieuGiaCong);

                phieuGiaCong.TiLeHotTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.TiLeHotTruHao);
                phieuGiaCong.KhoiLuongTongTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongTongTruHao);
                phieuGiaCong.KhoiLuongVangTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongVangTruHao);
                phieuGiaCong.KhoiLuongDaTruHao = listOfChiTietPhieuGiaCong.Sum(x => x.KhoiLuongDaTruHao);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
