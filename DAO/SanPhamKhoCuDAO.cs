﻿using DTO.Models;
using DTO.UtilDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class SanPhamKhoCuDAO
    {
        private static SanPhamKhoCuDAO instance;

        public static SanPhamKhoCuDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SanPhamKhoCuDAO();
                }
                return instance;
            }
        }

        public SanPhamKhoCu Insert(SanPhamKhoCu sanPhamKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.SanPhamKhoCues.Add(sanPhamKhoCu);
                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public bool Insert(SanPhamKhoCu sanPhamKhoCu, int soLuong)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                PrepareUtil.SanPhamKhoCuPrepare(sanPhamKhoCu, true);

                for (int i = 0; i < soLuong; i++)
                {
                    dbContext.SanPhamKhoCues.Add(sanPhamKhoCu);
                    dbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public SanPhamKhoCu Update(SanPhamKhoCu sanPhamKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.SanPhamKhoCues.Find(sanPhamKhoCu.MaSanPhamKhoCu);

                PrepareUtil.SanPhamKhoCuPrepare(sanPhamKhoCu);

                dbContext.Entry(entity).CurrentValues.SetValues(sanPhamKhoCu);
                dbContext.SaveChanges();

                return sanPhamKhoCu;
            }
            catch
            {
                return null;
            }
        }

        public SanPhamKhoCu DelelteById(int maSanPhamKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.SanPhamKhoCues.Remove(dbContext.SanPhamKhoCues.Find(maSanPhamKhoCu));
                dbContext.SaveChanges();
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public List<SanPhamKhoCu> GetListAllSanPhamKhoCu()
        {
            return new QuanLyTiemVangDbContext().SanPhamKhoCues.ToList();
        }

        public List<SanPhamKhoCu> GetListSanPhamByLichSuMua(LichSuMua lichSuMua)
        {
            return new QuanLyTiemVangDbContext().SanPhamKhoCues.Where(x => x.MaLichSuMua == lichSuMua.MaLichSuMua).ToList();
        }

        public bool ThemDanhSachSanPhamMuaVaoKhoCu(List<SanPhamKhoCu> listSanPhamKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                
                foreach (SanPhamKhoCu sanPham in listSanPhamKhoCu)
                {
                    dbContext.SanPhamKhoCues.Add(sanPham);
                    dbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool XoaSanPhamTheoLichSuMua(List<SanPhamKhoCu> listSanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                foreach (SanPhamKhoCu sanPham in listSanPham)
                {
                    dbContext.SanPhamKhoCues.Remove(dbContext.SanPhamKhoCues.Find(sanPham.MaSanPhamKhoCu));
                }

                dbContext.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
