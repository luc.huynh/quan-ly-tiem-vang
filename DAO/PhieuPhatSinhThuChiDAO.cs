﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class PhieuPhatSinhThuChiDAO
    {
        private static PhieuPhatSinhThuChiDAO instance;

        public static PhieuPhatSinhThuChiDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PhieuPhatSinhThuChiDAO();
                }
                return instance;
            }
        }

        public PhieuPhatSinhThuChi Insert(PhieuPhatSinhThuChi phieuPhatSinhThuChi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.PhieuPhatSinhThuChis.Add(phieuPhatSinhThuChi);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public PhieuPhatSinhThuChi Update(PhieuPhatSinhThuChi phieuPhatSinhThuChi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.PhieuPhatSinhThuChis.Find(phieuPhatSinhThuChi.MaPhieuPhatSinhThuChi);

                dbContext.Entry(entity).CurrentValues.SetValues(phieuPhatSinhThuChi);
                dbContext.SaveChanges();

                return phieuPhatSinhThuChi;
            }
            catch
            {
                return null;
            }
        }

        public PhieuPhatSinhThuChi DeleteById(int maPhieuPhatSinhThuChi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.PhieuPhatSinhThuChis.Remove(dbContext.PhieuPhatSinhThuChis.Find(maPhieuPhatSinhThuChi));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public List<PhieuPhatSinhThuChi> GetList()
        {
            return new QuanLyTiemVangDbContext().PhieuPhatSinhThuChis.ToList();
        }

        public PhieuPhatSinhThuChi GetById(int id)
        {
            return new QuanLyTiemVangDbContext().PhieuPhatSinhThuChis.Find(id);
        }
    }
}
