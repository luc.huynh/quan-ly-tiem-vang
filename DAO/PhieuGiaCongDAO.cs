﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class PhieuGiaCongDAO
    {
        #region instance
        private static PhieuGiaCongDAO instance;

        public static PhieuGiaCongDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new PhieuGiaCongDAO();
                return instance;
            }
        }
        #endregion

        public List<PhieuGiaCong> GetList()
        {
            return new QuanLyTiemVangDbContext().PhieuGiaCongs.ToList();
        }

        public PhieuGiaCong Insert(PhieuGiaCong phieuGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.PhieuGiaCongs.Add(phieuGiaCong);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public PhieuGiaCong DeleteById(int maPhieuGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                PhieuGiaCong entity = dbContext.PhieuGiaCongs.Find(maPhieuGiaCong);

                dbContext.PhieuGiaCongs.Remove(entity);
                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
