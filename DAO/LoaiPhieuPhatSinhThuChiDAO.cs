﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LoaiPhieuPhatSinhThuChiDAO
    {
        private static LoaiPhieuPhatSinhThuChiDAO instance;

        public static LoaiPhieuPhatSinhThuChiDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LoaiPhieuPhatSinhThuChiDAO();
                }
                return instance;
            }
        }

        public LoaiPhieuPhatSinhThuChi Insert(LoaiPhieuPhatSinhThuChi loaiPhieuPhatSinhThuChi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LoaiPhieuPhatSinhThuChis.Add(loaiPhieuPhatSinhThuChi);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public LoaiPhieuPhatSinhThuChi Update(LoaiPhieuPhatSinhThuChi loaiPhieuPhatSinhThuChi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LoaiPhieuPhatSinhThuChis.Find(loaiPhieuPhatSinhThuChi.MaLoaiPhieuPhatSinhThuChi);

                dbContext.Entry(entity).CurrentValues.SetValues(loaiPhieuPhatSinhThuChi);
                dbContext.SaveChanges();

                return loaiPhieuPhatSinhThuChi;
            }
            catch
            {
                return null;
            }
        }

        public LoaiPhieuPhatSinhThuChi DeleteById(int maLoaiPhieuPhatSinhThuChi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LoaiPhieuPhatSinhThuChis.Remove(dbContext.LoaiPhieuPhatSinhThuChis.Find(maLoaiPhieuPhatSinhThuChi));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public List<LoaiPhieuPhatSinhThuChi> GetList()
        {
            return new QuanLyTiemVangDbContext().LoaiPhieuPhatSinhThuChis.ToList();
        }
    }
}
