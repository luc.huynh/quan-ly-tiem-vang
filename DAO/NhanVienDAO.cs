﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class NhanVienDAO
    {
        #region instance
        private static NhanVienDAO instance;

        public static NhanVienDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new NhanVienDAO();
                return instance;
            }
        }



        #endregion

        public DTO.Models.NhanVien Login(string user, string pass)
        {
            try
            {
                return new QuanLyTiemVangDbContext().NhanViens.FirstOrDefault(x => x.TaiKhoan == user && x.MatKhau == pass);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }

        public int CheckUser(string taiKhoan)
        {
            return new QuanLyTiemVangDbContext().NhanViens.Where(x => x.TaiKhoan == taiKhoan).Count();
        }

        public List<NhanVien> GetList()
        {
            return new QuanLyTiemVangDbContext().NhanViens.ToList();
        }

        public bool Insert(NhanVien nv)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.NhanViens.Add(nv);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }


        public bool Update(NhanVien nv)
        {
            try
            {

                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.NhanViens.Find(nv.MaNhanVien);
                dbContext.Entry(entity).CurrentValues.SetValues(nv);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool Delete(int maNhanVien)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.NhanViens.Remove(dbContext.NhanViens.Find(maNhanVien));
                dbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
