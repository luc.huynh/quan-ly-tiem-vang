﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class KhoiLuongGiaCongDAO
    {
        private static KhoiLuongGiaCongDAO instance;

        public static KhoiLuongGiaCongDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KhoiLuongGiaCongDAO();
                }
                return instance;
            }
        }

        public KhoiLuongGiaCong Insert(KhoiLuongGiaCong khoiLuongGiaCong)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhoiLuongGiaCongs.Add(khoiLuongGiaCong);
                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public KhoiLuongGiaCong Update(KhoiLuongGiaCong khoiLuongGiaCong)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhoiLuongGiaCongs.Find(khoiLuongGiaCong.MaKhoiLuongGiaCong);

                entity.KhoiLuongDaTieuHao = khoiLuongGiaCong.KhoiLuongDaTieuHao;
                entity.KhoiLuongTinhTieuHao = khoiLuongGiaCong.KhoiLuongTinhTieuHao;
                entity.KhoiLuongVangTieuHao = khoiLuongGiaCong.KhoiLuongVangTieuHao;
                entity.MaNhanVien = khoiLuongGiaCong.MaNhanVien;

                dbContext.SaveChanges();

                return khoiLuongGiaCong;
            }
            catch
            {
                return null;
            }
        }

        public KhoiLuongGiaCong DelelteById(int maKhoiLuongGiaCong)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhoiLuongGiaCongs.Remove(dbContext.KhoiLuongGiaCongs.Find(maKhoiLuongGiaCong));
                dbContext.SaveChanges();
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public List<KhoiLuongGiaCong> GetListAllKhoiLuongGiaCong()
        {
            return new QuanLyTiemVangDbContext().KhoiLuongGiaCongs.ToList();
        }
    }
}
