﻿using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangThongKeDoiNgoaiTeDAO
    {
        private static BangThongKeDoiNgoaiTeDAO instance;

        public static BangThongKeDoiNgoaiTeDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BangThongKeDoiNgoaiTeDAO();
                }
                return instance;
            }
        }

        private List<BangThongKeDoiNgoaiTe> _listOfBangThongKeDoiNgoaiTe = new List<BangThongKeDoiNgoaiTe>();

        public BangThongKeDoiNgoaiTe Insert(BangThongKeDoiNgoaiTe bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeDoiNgoaiTes.Add(bangThongKe);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeDoiNgoaiTe Update(BangThongKeDoiNgoaiTe bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeDoiNgoaiTes.Find(bangThongKe.MaBangThongKeDoiNgoaiTe);

                dbContext.Entry(entity).CurrentValues.SetValues(bangThongKe);
                dbContext.SaveChanges();

                return bangThongKe;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeDoiNgoaiTe DelelteById(int maBangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeDoiNgoaiTes.Remove(dbContext.BangThongKeDoiNgoaiTes.Find(maBangThongKe));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Tạo những bản thống kê đầu tiên theo từng loại tiền đã có, được bắt đầu từ ngày đầu kì đầu tiên
        /// </summary>
        /// <param name="ngayDauKi"></param>
        /// <returns></returns>
        public bool TaoDuLieuDauKiDauTien(List<BangThongKeDoiNgoaiTe> listOfBangThongKeDoiNgoaiTe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                //dbContext.BangThongKeDoiNgoaiTes.RemoveRange(dbContext.BangThongKeDoiNgoaiTes);

                if (dbContext.NgayDauKiThongKes.ToList().Count == 0)
                {
                    return false;
                }

                NgayDauKiThongKe ngayDauKiDb = dbContext.NgayDauKiThongKes.ToList().First();
                List<LoaiTien> listLoaiTienDb = dbContext.LoaiTiens.ToList();
                List<LichSuDoiNgoaiTe> listLichSuDoiNgoaiTeDb = dbContext.LichSuDoiNgoaiTes.ToList();
                //List<BangThongKeDoiNgoaiTe> listBangThongKe = new List<BangThongKeDoiNgoaiTe>();

                foreach (var loaiTien in listLoaiTienDb)
                {
                    BangThongKeDoiNgoaiTe bangThongKe = new BangThongKeDoiNgoaiTe
                    {
                        MaLoaiTien = loaiTien.MaLoaiTien,
                        TongTienNgoaiTeDoiRa = 0,
                        TongTienNgoaiTeThuVao = 0,
                        TongTienVietDoiRa = 0,
                        TongTienVietThuVao = 0,
                        NgayThongKe = ngayDauKiDb.NgayDauKi.Date
                    };

                    listOfBangThongKeDoiNgoaiTe.Add(bangThongKe);
                }

                //dbContext.BangThongKeDoiNgoaiTes.AddRange(listBangThongKe);
                //dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        /// <summary>
        /// Thêm những ngày thống kê bị thiếu theo từng hàm lượng vàng từ ngày thống kê đầu tiên cho đến today
        /// </summary>
        /// <param name="ngayThongKe"></param>
        /// <returns></returns>
        public bool TuDongCapNhapThongKeThieu(List<BangThongKeDoiNgoaiTe> listOfBangThongKeDoiNgoaiTe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                //List<BangThongKeDoiNgoaiTe> listBangThongKeDb = dbContext.BangThongKeDoiNgoaiTes.ToList();
                List<BangThongKeDoiNgoaiTe> listThongKeBiThieu = new List<BangThongKeDoiNgoaiTe>();
                List<LoaiTien> listLoaiTienDb = dbContext.LoaiTiens.ToList();

                // Kiểm tra nếu có loại tiền nào chưa từng có bảng thống kê kho cũ
                foreach (LoaiTien loaiTien in listLoaiTienDb)
                {
                    if (listOfBangThongKeDoiNgoaiTe.Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien).Count() == 0)
                    {
                        return false;
                    }
                }

                foreach (LoaiTien loaiTien in listLoaiTienDb)
                {
                    List<BangThongKeDoiNgoaiTe> listBangThongKeDoiNgoaiTe = listOfBangThongKeDoiNgoaiTe
                        .Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien)
                        .OrderBy(x => x.NgayThongKe).ToList();
                    DateTime firstDay = listBangThongKeDoiNgoaiTe.First().NgayThongKe;
                    int totalDay = (int)(DateTime.Today.Date - firstDay.Date).TotalDays;

                    for (int i = 0; i <= totalDay; i++)
                    {
                        if (listBangThongKeDoiNgoaiTe.Where(x => x.NgayThongKe.Date == firstDay.AddDays(i).Date).Count() == 0)
                        {
                            BangThongKeDoiNgoaiTe bangThongKeAdd = new BangThongKeDoiNgoaiTe
                            {
                                MaLoaiTien = loaiTien.MaLoaiTien,
                                TongTienNgoaiTeDoiRa = 0,
                                TongTienNgoaiTeThuVao = 0,
                                TongTienVietDoiRa = 0,
                                TongTienVietThuVao = 0,
                                NgayThongKe = firstDay.AddDays(i)
                            };

                            listThongKeBiThieu.Add(bangThongKeAdd);
                        }
                    }
                }

                listOfBangThongKeDoiNgoaiTe.AddRange(listThongKeBiThieu);
                //dbContext.BangThongKeDoiNgoaiTes.AddRange(listThongKeBiThieu);
                //dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TinhToanThongKe()
        {
            try
            {
                List<BangThongKeDoiNgoaiTe> listOfBangThongKeDoiNgoaiTe = new List<BangThongKeDoiNgoaiTe>();

                if (!TaoDuLieuDauKiDauTien(listOfBangThongKeDoiNgoaiTe) || !TuDongCapNhapThongKeThieu(listOfBangThongKeDoiNgoaiTe))
                {
                    return false;
                }

                var dbContext = new QuanLyTiemVangDbContext();

                DateTime ngayThongKeHomNay = DateTime.Today;
                List<LoaiTien> listLoaiTienDb = dbContext.LoaiTiens.ToList();
                //List<BangThongKeDoiNgoaiTe> listThongKeDoiNgoaiTeDb = dbContext.BangThongKeDoiNgoaiTes.ToList();
                List<LichSuDoiNgoaiTe> listLichSuDoiNgoaiTeDb = dbContext.LichSuDoiNgoaiTes.ToList();

                foreach (LoaiTien loaiTien in listLoaiTienDb)
                {
                    List<BangThongKeDoiNgoaiTe> listThongKeDoiNgoaiTe = listOfBangThongKeDoiNgoaiTe
                        .Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien)
                        .OrderBy(x => x.NgayThongKe).ToList();

                    foreach (BangThongKeDoiNgoaiTe bangThongKe in listThongKeDoiNgoaiTe)
                    {
                        int tongNgoaiTeThuVao = listLichSuDoiNgoaiTeDb
                            .Where(x => x.LichSuTyGiaTien.MaLoaiTien == loaiTien.MaLoaiTien
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.THU_MUA_NGOAI_TE)
                            .Sum(x => x.SoTienDoi);

                        int tongNgoaiTeDoiRa = listLichSuDoiNgoaiTeDb
                            .Where(x => x.LichSuTyGiaTien.MaLoaiTien == loaiTien.MaLoaiTien
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.BAN_NGOAI_TE)
                            .Sum(x => x.SoTienKhachNhan);

                        int tongTienVietDoiRa = listLichSuDoiNgoaiTeDb
                            .Where(x => x.LichSuTyGiaTien.MaLoaiTien == loaiTien.MaLoaiTien
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.THU_MUA_NGOAI_TE)
                            .Sum(x => x.SoTienKhachNhan);

                        int tongTienVietThuVao = listLichSuDoiNgoaiTeDb
                            .Where(x => x.LichSuTyGiaTien.MaLoaiTien == loaiTien.MaLoaiTien
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.DoiTheoChieuThuanHoacNguoc == QuyDoiNgoaiTeConstant.BAN_NGOAI_TE)
                            .Sum(x => x.SoTienDoi);

                        bangThongKe.TongTienNgoaiTeDoiRa = tongNgoaiTeDoiRa;
                        bangThongKe.TongTienNgoaiTeThuVao = tongNgoaiTeThuVao;
                        bangThongKe.TongTienVietDoiRa = tongTienVietDoiRa;
                        bangThongKe.TongTienVietThuVao = tongTienVietThuVao;

                        // Cập nhập lại mỗi bảng thống kê kho cũ
                        //var entity = dbContext.BangThongKeDoiNgoaiTes.Find(bangThongKe.MaBangThongKeDoiNgoaiTe);
                        //dbContext.Entry(entity).CurrentValues.SetValues(bangThongKe);
                    }
                }

                //dbContext.SaveChanges();
                _listOfBangThongKeDoiNgoaiTe = listOfBangThongKeDoiNgoaiTe;

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<BangThongKeDoiNgoaiTe> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var resultListOfBangThongKe = _listOfBangThongKeDoiNgoaiTe.Where(x => x.NgayThongKe.Date == ngayThongKe.Date).ToList();

                foreach (var item in resultListOfBangThongKe)
                {
                    item.LoaiTien = dbContext.LoaiTiens.Find(item.MaLoaiTien);
                }

                return resultListOfBangThongKe;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public List<BangThongKeDoiNgoaiTe> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeDoiNgoaiTe> listBangThongKe = _listOfBangThongKeDoiNgoaiTe
                    .Where(x => tuNgay.Date <= x.NgayThongKe.Date && x.NgayThongKe.Date <= denNgay.Date)
                    .OrderBy(x => x.NgayThongKe)
                    .ToList();
                List<BangThongKeDoiNgoaiTe> listBangThongKeReturn = new List<BangThongKeDoiNgoaiTe>();
                List<LoaiTien> listLoaiTienDb = dbContext.LoaiTiens.ToList();

                foreach (LoaiTien loaiTien in listLoaiTienDb)
                {
                    BangThongKeDoiNgoaiTe bangThongKe = new BangThongKeDoiNgoaiTe
                    {
                        LoaiTien = loaiTien,
                        MaLoaiTien = loaiTien.MaLoaiTien,
                        TongTienNgoaiTeDoiRa = listBangThongKe.Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien).Sum(x => x.TongTienNgoaiTeDoiRa),
                        TongTienNgoaiTeThuVao = listBangThongKe.Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien).Sum(x => x.TongTienNgoaiTeThuVao),
                        TongTienVietDoiRa = listBangThongKe.Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien).Sum(x => x.TongTienVietDoiRa),
                        TongTienVietThuVao = listBangThongKe.Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien).Sum(x => x.TongTienVietThuVao)
                    };

                    listBangThongKeReturn.Add(bangThongKe);
                }

                return listBangThongKeReturn;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
