﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.Models;
using DTO.TemporaryModel;

namespace DAO
{
    public class ChiTietCamDoDAO
    {
        private static ChiTietCamDoDAO instance;

        public static ChiTietCamDoDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new ChiTietCamDoDAO();
                return instance;
            }
        }

        public bool GiaHanHopDong(ChiTietCamDo chiTietCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.ChiTietCamDoes.Add(chiTietCamDo);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<ChiTietCamDoView> getListChiTietCamDo(string maHopDongCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                return (from nv in dbContext.NhanViens join cthd in dbContext.ChiTietCamDoes on nv.MaNhanVien equals cthd.MaNhanVien
                        where cthd.MaHopDongCamDo == maHopDongCamDo
                        select new ChiTietCamDoView()
                        {
                            MaChiTietCamDo = cthd.MaChiTietCamDo,
                            MaHopDongCamDo = cthd.MaHopDongCamDo,
                            NgayBatDauThang = cthd.NgayBatDauThang,
                            NgayNopTienLai = cthd.NgayNopTienLai,
                            SoTienDongLai = cthd.SoTienDongLai,
                            MaNhanVien = cthd.MaNhanVien,
                            TenNhanVien = nv.Ten
                        }).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
