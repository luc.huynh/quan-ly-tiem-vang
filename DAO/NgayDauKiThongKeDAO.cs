﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class NgayDauKiThongKeDAO
    {
        private static NgayDauKiThongKeDAO instance;

        public static NgayDauKiThongKeDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new NgayDauKiThongKeDAO();
                return instance;
            }
        }

        public NgayDauKiThongKe GetNgayDauKiLast()
        {
            return new QuanLyTiemVangDbContext().NgayDauKiThongKes.ToList().LastOrDefault();
        }

        public NgayDauKiThongKe Insert(NgayDauKiThongKe ngayDauKi)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                NgayDauKiThongKe ngayDauKiDb = dbContext.NgayDauKiThongKes.FirstOrDefault();

                if (ngayDauKiDb != null)
                {
                    ngayDauKiDb.NgayChinhSua = ngayDauKi.NgayChinhSua;
                    ngayDauKiDb.NgayDauKi = ngayDauKi.NgayDauKi;
                    ngayDauKiDb.SoTienDauKi = ngayDauKi.SoTienDauKi;

                    dbContext.SaveChanges();

                    return new QuanLyTiemVangDbContext().NgayDauKiThongKes.Find(ngayDauKiDb.MaNgayDauKi);
                }
                else
                {
                    var entity = dbContext.NgayDauKiThongKes.Add(ngayDauKi);

                    dbContext.SaveChanges();

                    return entity;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
