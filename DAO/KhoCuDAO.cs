﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class KhoCuDAO
    {
        private static KhoCuDAO instance;

        public static KhoCuDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KhoCuDAO();
                }
                return instance;
            }
        }

        public KhoCu Insert(KhoCu khoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhoCues.Add(khoCu);
                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public KhoCu Update(KhoCu khoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhoCues.Find(khoCu.MaKhoCu);
                dbContext.Entry(entity).CurrentValues.SetValues(khoCu);
                dbContext.SaveChanges();

                return khoCu;
            }
            catch
            {
                return null;
            }
        }

        public KhoCu DeleteById(int maKhoCu)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhoCues.Remove(dbContext.KhoCues.Find(maKhoCu));
                dbContext.SaveChanges();
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public KhoCu GetKhoCuByMaHamLuongVang(int maHamLuongVang)
        {
            try
            {
                return new QuanLyTiemVangDbContext().KhoCues.FirstOrDefault(x => x.MaHamLuongVang == maHamLuongVang);
            }
            catch
            {
                return null;
            }
        }

        public List<KhoCu> GetListAllKhoCu()
        {
            UpdateKhoiLuongAllKhoCu();
            return new QuanLyTiemVangDbContext().KhoCues.ToList();
        }

        #region Update khối lượng trong kho cũ
        public bool ThemKhoCu()
        {
            var dbContext = new QuanLyTiemVangDbContext();
            List<KhoCu> listKhoCu = dbContext.KhoCues.ToList();
            List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();
            List<KhoCu> listKhoCuInsert = new List<KhoCu>();

            // Thêm bản ghi kho cũ để lưu hàm lượng vàng nếu chưa có kho cho hàm lượng vàng đó
            foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
            {
                bool check = true;

                foreach (KhoCu khoCu in listKhoCu)
                {
                    if (khoCu.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                    {
                        check = false;
                        break;
                    }
                }

                if (check)
                {
                    KhoCu khoCuNew = new KhoCu
                    {
                        MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                        TongKhoiLuongDa = 0,
                        TongKhoiLuongTinh = 0,
                        TongKhoiLuongVang = 0
                    };

                    listKhoCuInsert.Add(khoCuNew);
                }
            }

            try
            {
                dbContext.KhoCues.AddRange(listKhoCuInsert);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool UpdateKhoiLuongAllKhoCu()
        {
            if (!ThemKhoCu())
            {
                return false;
            }

            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<KhoCu> listKhoCu = dbContext.KhoCues.ToList();
                List<KhoiLuongGiaCong> listKhoiLuongGiaCongDb = dbContext.KhoiLuongGiaCongs.ToList();
                List<SanPhamKhoCu> listSanPhamKhoCuDb = dbContext.SanPhamKhoCues.ToList();

                // Trừ tất cả khối lượng của đã đem đi gia công theo hàm lượng vàng ở kho cũ
                foreach (KhoCu khoCu in listKhoCu)
                {
                    khoCu.TongKhoiLuongTinh = listSanPhamKhoCuDb.Where(x => x.MaHamLuongVang == khoCu.MaHamLuongVang).Sum(x => x.KhoiLuongTinh)
                        - listKhoiLuongGiaCongDb.Where(x => x.KhoCu.MaHamLuongVang == khoCu.MaHamLuongVang).Sum(x => x.KhoiLuongTinhTieuHao);
                    khoCu.TongKhoiLuongDa = listSanPhamKhoCuDb.Where(x => x.MaHamLuongVang == khoCu.MaHamLuongVang).Sum(x => x.KhoiLuongDa)
                        - listKhoiLuongGiaCongDb.Where(x => x.KhoCu.MaHamLuongVang == khoCu.MaHamLuongVang).Sum(x => x.KhoiLuongDaTieuHao);
                    khoCu.TongKhoiLuongVang = listSanPhamKhoCuDb.Where(x => x.MaHamLuongVang == khoCu.MaHamLuongVang).Sum(x => x.KhoiLuongVang)
                        - listKhoiLuongGiaCongDb.Where(x => x.KhoCu.MaHamLuongVang == khoCu.MaHamLuongVang).Sum(x => x.KhoiLuongVangTieuHao);

                    var entity = dbContext.KhoCues.Find(khoCu.MaKhoCu);
                    dbContext.Entry(entity).CurrentValues.SetValues(khoCu);
                }

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        #endregion

    }
}
