﻿using DTO.Models;
using Share.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangThongKeTheoHamLuongVangDAO
    {
        private static BangThongKeTheoHamLuongVangDAO instance;

        public static BangThongKeTheoHamLuongVangDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BangThongKeTheoHamLuongVangDAO();
                }
                return instance;
            }
        }

        public List<BangThongKeTheoHamLuongVang> _listOfBangThongKeTheoHamLuongVang = new List<BangThongKeTheoHamLuongVang>();

        public BangThongKeTheoHamLuongVang Insert(BangThongKeTheoHamLuongVang bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeTheoHamLuongVangs.Add(bangThongKe);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeTheoHamLuongVang Update(BangThongKeTheoHamLuongVang bangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeTheoHamLuongVangs.Find(bangThongKe.MaBangThongKeTheoHamLuongVang);

                dbContext.Entry(entity).CurrentValues.SetValues(bangThongKe);
                dbContext.SaveChanges();

                return bangThongKe;
            }
            catch
            {
                return null;
            }
        }

        public BangThongKeTheoHamLuongVang DelelteById(int maBangThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.BangThongKeTheoHamLuongVangs.Remove(dbContext.BangThongKeTheoHamLuongVangs.Find(maBangThongKe));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public bool TaoDuLieuDauKiDauTien(List<BangThongKeTheoHamLuongVang> listOfBangThongKeTheoHamLuongVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                if (dbContext.NgayDauKiThongKes.ToList().Count == 0)
                {
                    return false;
                }

                NgayDauKiThongKe ngayDauKiDb = dbContext.NgayDauKiThongKes.ToList().First();
                List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPham> listSanPham = dbContext.SanPhams.ToList().Where(x => x.NgayNhap.Date < ngayDauKiDb.NgayDauKi.Date).ToList();

                foreach (var hamLuongVang in listHamLuongVang)
                {
                    double khoiLuongVangDaNhapTruocDauKi = listSanPham
                        .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                        && x.NgayNhap.Date < ngayDauKiDb.NgayDauKi.Date)
                        .Sum(x => x.KhoiLuongVang);
                    double khoiLuongVangDaBanTruocDauKi = listSanPham
                        .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                        && x.MaLichSuBan != null
                        && x.LichSuBan.NgayGiaoDich.Date < ngayDauKiDb.NgayDauKi.Date)
                        .Sum(x => x.KhoiLuongVang);

                    BangThongKeTheoHamLuongVang bangThongKe = new BangThongKeTheoHamLuongVang
                    {
                        MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                        KhoiLuongVangDauKi = Math.Round(khoiLuongVangDaNhapTruocDauKi - khoiLuongVangDaBanTruocDauKi, 1),
                        KhoiLuongVangNhapVao = 0,
                        KhoiLuongVangCuBanRa = 0,
                        KhoiLuongVangMoiBanRa = 0,
                        KhoiLuongVangMuaVao = 0,
                        KhoiLuongVangDoiNgangRa = 0,
                        KhoiLuongVangCuoiKi = Math.Round(khoiLuongVangDaNhapTruocDauKi - khoiLuongVangDaBanTruocDauKi, 1),
                        TongTienVangCuBanRa = 0,
                        TongTienVangMoiBanRa = 0,
                        TongTienVangBanRa = 0,
                        TongKhoiLuongVangBanRa = 0,
                        NgayThongKe = ngayDauKiDb.NgayDauKi.Date
                    };

                    listOfBangThongKeTheoHamLuongVang.Add(bangThongKe);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        /// <summary>
        /// Thêm những ngày thống kê bị thiếu theo từng hàm lượng vàng từ ngày thống kê đầu tiên cho đến today
        /// </summary>
        /// <param name="ngayThongKe"></param>
        /// <returns></returns>
        public bool TuDongCapNhapThongKeThieu(List<BangThongKeTheoHamLuongVang> listOfBangThongKeTheoHamLuongVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeTheoHamLuongVang> listThongKeBiThieu = new List<BangThongKeTheoHamLuongVang>();
                List<KiHieuHamLuongVang> listHamLuongVangDb = dbContext.KiHieuHamLuongVangs.ToList();

                // Kiểm tra nếu có hàm lượng vàng nào chưa từng có bảng thống kê kho cũ
                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    if (listOfBangThongKeTheoHamLuongVang.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Count() == 0)
                    {
                        return false;
                    }
                }

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVangDb)
                {
                    List<BangThongKeTheoHamLuongVang> listThongKeTheoHamLuongVang = listOfBangThongKeTheoHamLuongVang
                        .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .OrderBy(x => x.NgayThongKe.Date).ToList();
                    DateTime firstDay = listThongKeTheoHamLuongVang.First().NgayThongKe;
                    int totalDay = (int)(DateTime.Today.Date - firstDay.Date).TotalDays;

                    for (int i = 0; i <= totalDay; i++)
                    {
                        if (listThongKeTheoHamLuongVang.Where(x => x.NgayThongKe.Date == firstDay.AddDays(i).Date).Count() == 0)
                        {
                            BangThongKeTheoHamLuongVang bangThongKeAdd = new BangThongKeTheoHamLuongVang
                            {
                                MaHamLuongVang = hamLuongVang.MaHamLuongVang,
                                KhoiLuongVangDauKi = 0,
                                KhoiLuongVangNhapVao = 0,
                                KhoiLuongVangCuBanRa = 0,
                                KhoiLuongVangMoiBanRa = 0,
                                KhoiLuongVangDoiNgangRa = 0,
                                KhoiLuongVangMuaVao = 0,
                                KhoiLuongVangCuoiKi = 0,
                                TongTienVangMoiBanRa = 0,
                                TongTienVangCuBanRa = 0,
                                TongTienVangBanRa = 0,
                                TongKhoiLuongVangBanRa = 0,
                                NgayThongKe = firstDay.AddDays(i)
                            };

                            listThongKeBiThieu.Add(bangThongKeAdd);
                        }
                    }
                }

                listOfBangThongKeTheoHamLuongVang.AddRange(listThongKeBiThieu);

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool TinhToanThongKe()
        {
            try
            {
                List<BangThongKeTheoHamLuongVang> listOfBangThongKeTheoHamLuongVang = new List<BangThongKeTheoHamLuongVang>();

                if (!TaoDuLieuDauKiDauTien(listOfBangThongKeTheoHamLuongVang) || !TuDongCapNhapThongKeThieu(listOfBangThongKeTheoHamLuongVang))
                {
                    return false;
                }

                var dbContext = new QuanLyTiemVangDbContext();

                DateTime ngayThongKeHomNay = DateTime.Today;

                List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPham> listSanPhamDb = dbContext.SanPhams.ToList();
                List<LichSuDoiVang> listLichSuDoiVangDb = dbContext.LichSuDoiVangs.ToList();

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
                {
                    List<BangThongKeTheoHamLuongVang> listThongKeTheoHamLuongVang = listOfBangThongKeTheoHamLuongVang
                        .Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang)
                        .OrderBy(x => x.NgayThongKe).ToList();
                    double khoiLuongVangCuoiKiCache = 0;

                    foreach (BangThongKeTheoHamLuongVang bangThongKe in listThongKeTheoHamLuongVang)
                    {
                        double khoiLuongNhapVaoHienTai = listSanPhamDb
                            .Where(x => x.NgayNhap != null
                            && x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.NgayNhap.Date == bangThongKe.NgayThongKe.Date)
                            .Sum(x => x.KhoiLuongVang);

                        double klvMoiBanRaHienTai = listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == false
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count == 0)
                            .Sum(x => x.KhoiLuongVang)
                            +
                            listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == false
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count > 0
                            && ((x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() > 0
                                && !x.HamLuongVang.Equals(x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).FirstOrDefault().KiHieuHamLuongVang.HamLuongVang))
                                || (x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() == 0)))
                            .Sum(x => x.KhoiLuongVang);

                        double klvCuBanRaHienTai = listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == true
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count == 0)
                            .Sum(x => x.KhoiLuongVang)
                            +
                            listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == true
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count > 0
                            && ((x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() > 0
                                && !x.HamLuongVang.Equals(x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).FirstOrDefault().KiHieuHamLuongVang.HamLuongVang))
                                || (x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() == 0)))
                            .Sum(x => x.KhoiLuongVang);

                        int tongTienVangMoiBanRa = listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == false
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count == 0)
                            .Sum(x => x.GiaBan - x.TienCong)
                            +
                            listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == false
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count > 0
                            && ((x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() > 0
                                    && !x.HamLuongVang.Equals(x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).FirstOrDefault().KiHieuHamLuongVang.HamLuongVang))
                                    || (x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() == 0)))
                            .Sum(x => x.GiaBan - x.TienCong);

                        int tongTienVangCuBanRa = listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == true
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count == 0)
                            .Sum(x => x.GiaBan - x.TienCong)
                            +
                            listSanPhamDb
                            .Where(x => x.HamLuongVang.Equals(hamLuongVang.HamLuongVang)
                            && x.IsSanPhamChuyenKho == true
                            && x.MaLichSuBan != null
                            && x.LichSuBan.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date
                            && x.LichSuBan.LichSuDoiVangs.Count > 0
                            && ((x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() > 0
                                && !x.HamLuongVang.Equals(x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).FirstOrDefault().KiHieuHamLuongVang.HamLuongVang))
                                || (x.LichSuBan.LichSuDoiVangs.FirstOrDefault().LichSuMua.SanPhamKhoCues.Where(o => o.LoaiVangMuaVao.Equals(Constant.VANG_CU_DOI_NGANG)).Count() == 0)))
                            .Sum(x => x.GiaBan - x.TienCong);

                        List<LichSuDoiVang> listLichSuDoiVangTemp = listLichSuDoiVangDb
                            .Where(x => x.LichSuBan.SanPhams.Count > 0
                            && x.LichSuBan.SanPhams.FirstOrDefault().HamLuongVang == hamLuongVang.HamLuongVang
                            && x.NgayGiaoDich.Date == bangThongKe.NgayThongKe.Date)
                            .ToList();

                        double khoiLuongVangDoiNgangRa = listLichSuDoiVangTemp.Sum(x => Math.Min(x.KhoiLuongVangMoi, x.KhoiLuongVangCu));
                        double khoiLuongVangMoiDoiNgangBanRa = 0;
                        int tongTienVangMoiDoiNgangBanRa = 0;

                        if (listLichSuDoiVangTemp.Count > 0)
                        {
                            foreach (LichSuDoiVang lichSuDoiVang in listLichSuDoiVangTemp)
                            {
                                double KLVMoiTruKLVCu = lichSuDoiVang.KLVMoiTruKLVCu;

                                if (KLVMoiTruKLVCu > 0)
                                {
                                    khoiLuongVangMoiDoiNgangBanRa += Math.Abs(KLVMoiTruKLVCu);
                                    tongTienVangMoiDoiNgangBanRa += Math.Abs(lichSuDoiVang.ThanhTienVangMoi);
                                }
                            }
                        }

                        if (listThongKeTheoHamLuongVang.IndexOf(bangThongKe) > 0)
                        {
                            bangThongKe.KhoiLuongVangDauKi = Math.Round(khoiLuongVangCuoiKiCache, 1);
                        }

                        double khoiLuongDauKiHienTai = bangThongKe.KhoiLuongVangDauKi;
                        double khoiLuongVangMoiBanRa = klvMoiBanRaHienTai + khoiLuongVangMoiDoiNgangBanRa;
                        double tongKhoiLuongVangBanRa = khoiLuongVangMoiBanRa + klvCuBanRaHienTai;

                        khoiLuongVangCuoiKiCache = khoiLuongDauKiHienTai
                            - khoiLuongVangDoiNgangRa
                            - tongKhoiLuongVangBanRa
                            + khoiLuongNhapVaoHienTai;

                        bangThongKe.KhoiLuongVangNhapVao = Math.Round(khoiLuongNhapVaoHienTai, 1);
                        bangThongKe.KhoiLuongVangCuoiKi = Math.Round(khoiLuongVangCuoiKiCache, 1);
                        bangThongKe.KhoiLuongVangCuBanRa = Math.Round(klvCuBanRaHienTai, 1);
                        bangThongKe.KhoiLuongVangDoiNgangRa = Math.Round(khoiLuongVangDoiNgangRa, 1);
                        bangThongKe.KhoiLuongVangMoiBanRa = Math.Round(khoiLuongVangMoiBanRa, 1);
                        bangThongKe.TongTienVangMoiBanRa = tongTienVangMoiBanRa + tongTienVangMoiDoiNgangBanRa;
                        bangThongKe.TongTienVangCuBanRa = tongTienVangCuBanRa;
                        bangThongKe.TongTienVangBanRa = tongTienVangCuBanRa + tongTienVangMoiBanRa + tongTienVangMoiDoiNgangBanRa;
                        bangThongKe.TongKhoiLuongVangBanRa = Math.Round(tongKhoiLuongVangBanRa, 1);
                    }
                }

                _listOfBangThongKeTheoHamLuongVang = listOfBangThongKeTheoHamLuongVang;

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<BangThongKeTheoHamLuongVang> GetBangThongKeByDate(DateTime ngayThongKe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var resultListOfBangThongKe = _listOfBangThongKeTheoHamLuongVang.Where(x => x.NgayThongKe.Date == ngayThongKe.Date).ToList();

                foreach (var item in resultListOfBangThongKe)
                {
                    item.KiHieuHamLuongVang = dbContext.KiHieuHamLuongVangs.Find(item.MaHamLuongVang);
                }

                return resultListOfBangThongKe;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public List<BangThongKeTheoHamLuongVang> GetThongKeByTwoDifferenceDate(DateTime tuNgay, DateTime denNgay)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<BangThongKeTheoHamLuongVang> listBangThongKe = _listOfBangThongKeTheoHamLuongVang
                    .Where(x => tuNgay.Date <= x.NgayThongKe.Date && x.NgayThongKe.Date <= denNgay.Date)
                    .OrderBy(x => x.NgayThongKe)
                    .ToList();
                List<BangThongKeTheoHamLuongVang> listBangThongKeReturn = new List<BangThongKeTheoHamLuongVang>();
                List<KiHieuHamLuongVang> listHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();

                foreach (KiHieuHamLuongVang hamLuongVang in listHamLuongVang)
                {
                    BangThongKeTheoHamLuongVang bangThongKe = new BangThongKeTheoHamLuongVang
                    {
                        KhoiLuongVangCuBanRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangCuBanRa),
                        KhoiLuongVangCuoiKi = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Last().KhoiLuongVangCuoiKi,
                        KhoiLuongVangDauKi = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).First().KhoiLuongVangDauKi,
                        KhoiLuongVangDoiNgangRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangDoiNgangRa),
                        KhoiLuongVangMoiBanRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangMoiBanRa),
                        KhoiLuongVangMuaVao = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangMuaVao),
                        KhoiLuongVangNhapVao = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.KhoiLuongVangNhapVao),
                        TongKhoiLuongVangBanRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongKhoiLuongVangBanRa),
                        TongTienVangBanRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienVangBanRa),
                        TongTienVangCuBanRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienVangCuBanRa),
                        TongTienVangMoiBanRa = listBangThongKe.Where(x => x.MaHamLuongVang == hamLuongVang.MaHamLuongVang).Sum(x => x.TongTienVangMoiBanRa),
                        KiHieuHamLuongVang = hamLuongVang,
                        MaHamLuongVang = hamLuongVang.MaHamLuongVang
                    };

                    listBangThongKeReturn.Add(bangThongKe);
                }

                return listBangThongKeReturn;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
