﻿using DTO.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class KhachHangDAO
    {
        #region instance
        private static KhachHangDAO instance;

        public static KhachHangDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new KhachHangDAO();
                return instance;
            }
        }
        #endregion

        public List<KhachHang> GetList()
        {
            return new QuanLyTiemVangDbContext().KhachHangs.ToList();
        }

        public KhachHang Insert(KhachHang khachHang)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KhachHangs.Add(khachHang);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public KhachHang Update(KhachHang khachHang)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                KhachHang entity = dbContext.KhachHangs.Find(khachHang.MaKhachHang);

                dbContext.Entry(entity).CurrentValues.SetValues(khachHang);
                dbContext.SaveChanges();

                return khachHang;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public KhachHang DeleteById(int maKhachHang)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                KhachHang entity = dbContext.KhachHangs.Find(maKhachHang);

                dbContext.KhachHangs.Remove(entity);
                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
