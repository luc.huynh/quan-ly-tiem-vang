﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LichSuDoiVangDAO
    {

        #region instance
        private static LichSuDoiVangDAO instance;

        public static LichSuDoiVangDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuDoiVangDAO();
                return instance;
            }
        }
        #endregion

        public LichSuDoiVang Insert(LichSuDoiVang lichSuDoiVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuDoiVangs.Add(lichSuDoiVang);
                dbContext.SaveChanges();
                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public LichSuDoiVang GetLichSuDoiVangById(string maLichSuVangDoiVang)
        {
            return new QuanLyTiemVangDbContext().LichSuDoiVangs.Find(maLichSuVangDoiVang);
        }

        public List<LichSuDoiVang> GetListLichSuDoiVang()
        {
            return new QuanLyTiemVangDbContext().LichSuDoiVangs.ToList();
        }

        public LichSuDoiVang ThanhToanHoaDon(
            LichSuDoiVang lichSuDoiVang,
            LichSuBan lichSuBan,
            LichSuMua lichSuMua,
            List<SanPham> listSanPhamBanRa,
            List<SanPhamKhoCu> listSanPhamMuaVao)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var lichSuBanEntity = dbContext.LichSuBans.Add(lichSuBan);
                var lichSuMuaEntity = dbContext.LichSuMuas.Add(lichSuMua);
                var lichSuDoiVangEntity = dbContext.LichSuDoiVangs.Add(lichSuDoiVang);

                foreach (SanPham sanPham in listSanPhamBanRa)
                {
                    dbContext.SanPhams.Find(sanPham.MaSanPham).MaLichSuBan = lichSuBanEntity.MaLichSuBan;
                }

                dbContext.SanPhamKhoCues.AddRange(listSanPhamMuaVao);
                dbContext.SaveChanges();

                return lichSuDoiVangEntity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool XoaHoaDon(LichSuDoiVang lichSuDoiVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<SanPhamKhoCu> listSanPhamKhoCu = dbContext.SanPhamKhoCues.Where(x => x.MaLichSuMua.Equals(lichSuDoiVang.MaLichSuMua)).ToList();

                foreach (SanPhamKhoCu sanPham in listSanPhamKhoCu)
                {
                    dbContext.SanPhamKhoCues.Remove(dbContext.SanPhamKhoCues.Find(sanPham.MaSanPhamKhoCu));
                }

                foreach (SanPham sanPham in lichSuDoiVang.LichSuBan.SanPhams)
                {
                    dbContext.SanPhams.Find(sanPham.MaSanPham).MaLichSuBan = null;
                }

                dbContext.LichSuDoiVangs.Remove(dbContext.LichSuDoiVangs.Find(lichSuDoiVang.MaLichSuDoiVang));
                dbContext.LichSuBans.Remove(dbContext.LichSuBans.Find(lichSuDoiVang.MaLichSuBan));
                dbContext.LichSuMuas.Remove(dbContext.LichSuMuas.Find(lichSuDoiVang.MaLichSuMua));
                
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
