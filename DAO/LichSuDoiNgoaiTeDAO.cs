﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LichSuDoiNgoaiTeDAO
    {
        private static LichSuDoiNgoaiTeDAO instance;

        public static LichSuDoiNgoaiTeDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LichSuDoiNgoaiTeDAO();
                }
                return instance;
            }
        }

        public LichSuDoiNgoaiTe Insert(LichSuDoiNgoaiTe lichSuDoiNgoaiTe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuDoiNgoaiTes.Add(lichSuDoiNgoaiTe);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public LichSuDoiNgoaiTe Update(LichSuDoiNgoaiTe lichSuDoiNgoaiTe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuDoiNgoaiTes.Find(lichSuDoiNgoaiTe.MaLichSuDoiNgoaiTe);

                dbContext.Entry(entity).CurrentValues.SetValues(lichSuDoiNgoaiTe);
                dbContext.SaveChanges();

                return lichSuDoiNgoaiTe;
            }
            catch
            {
                return null;
            }
        }

        public LichSuDoiNgoaiTe DeleteById(int maLichSuDoiNgoaiTe)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LichSuDoiNgoaiTes.Remove(dbContext.LichSuDoiNgoaiTes.Find(maLichSuDoiNgoaiTe));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public List<LichSuDoiNgoaiTe> GetList()
        {
            return new QuanLyTiemVangDbContext().LichSuDoiNgoaiTes.ToList();
        }
    }
}
