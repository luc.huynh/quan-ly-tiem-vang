﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.TemporaryModel;
using Share.Constant;
using Share.Util;

namespace DAO
{
    public class HopDongCamDoDAO
    {
        private static HopDongCamDoDAO instance;

        public static HopDongCamDoDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new HopDongCamDoDAO();
                return instance;
            }
        }

        public HopDongCamDo Insert(HopDongCamDo hopDongCamDo, List<SanPhamCamDo> listSanPham)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.HopDongCamDoes.Add(hopDongCamDo);

                dbContext.SanPhamCamDoes.AddRange(listSanPham);
                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public bool Update(HopDongCamDo hopDongCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.HopDongCamDoes.Find(hopDongCamDo.MaHopDongCamDo);
                dbContext.Entry(entity).CurrentValues.SetValues(hopDongCamDo);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool Delete(string maHopDongCamDo)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                List<SanPhamCamDo> listSanPham = dbContext.SanPhamCamDoes.Where(x => x.MaHopDongCamDo == maHopDongCamDo).ToList();
                List<ChiTietCamDo> listChiTietCamDo = dbContext.ChiTietCamDoes.Where(x => x.MaHopDongCamDo == maHopDongCamDo).ToList();

                dbContext.ChiTietCamDoes.RemoveRange(listChiTietCamDo);
                dbContext.SanPhamCamDoes.RemoveRange(listSanPham);
                dbContext.HopDongCamDoes.Remove(dbContext.HopDongCamDoes.Find(maHopDongCamDo));

                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public int TinhSotienLai(string maHopDong, DateTime ngayDongLai)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                HopDongCamDo entity = dbContext.HopDongCamDoes.Find(maHopDong);
                int soNgay = (int)(ngayDongLai.Date - entity.NgayBatDau.Date).TotalDays;

                if (entity != null)
                {
                    int tienLai = TinhTienLai((double)entity.SoTienCam, (double)entity.LaiSuat, soNgay);

                    if (tienLai < 5000)
                    {
                        return 5000;
                    }

                    return TienUtil.LamTronTienViet(tienLai);
                }

                return -1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }
        }

        private int TinhTienLai(double soTienCam, double laiSuat, int soNgay)
        {
            if (soNgay == 0)
            {
                soNgay++;
            }

            return (int)Math.Round(soTienCam * laiSuat) * soNgay;
        }

        public List<HopDongCamDo> GetListDaThanhToan()
        {
            try
            {
                return new QuanLyTiemVangDbContext()
                    .HopDongCamDoes
                    .Where(x => !x.TrangThaiHopDong.Equals(HopDongCamDoConstant.CHUA_THANH_TOAN)).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<HopDongCamDo> GetListChuaThanhToan()
        {
            try
            {
                return new QuanLyTiemVangDbContext()
                    .HopDongCamDoes
                    .Where(x => x.TrangThaiHopDong.Equals(HopDongCamDoConstant.CHUA_THANH_TOAN)).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<HopDongCamDo> GetListThongBaoThanhLy()
        {
            try
            {
                return new QuanLyTiemVangDbContext().
                    HopDongCamDoes.
                    ToList().
                    Where(x => x.TrangThaiHopDong.Equals(HopDongCamDoConstant.CHUA_THANH_TOAN) && (DateTime.Today - x.NgayBatDau.Date).TotalDays >= 25).
                    ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool ThanhToan(string maHopDong)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                HopDongCamDo entity = dbContext.HopDongCamDoes.Where(x => x.MaHopDongCamDo == maHopDong).FirstOrDefault();

                entity.NgayThanhLy = DateTime.Today;
                entity.SoTienThanhLy = entity.SoTienCam;
                entity.TrangThaiHopDong = HopDongCamDoConstant.DA_THANH_TOAN;
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool ThanhLy(string maHopDong, int maNhanVien, int loaiVangMua, string maLichSuMua)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                HopDongCamDo hopDong = dbContext.HopDongCamDoes.Find(maHopDong);
                List<KiHieuHamLuongVang> listKiHieuHamLuongVang = dbContext.KiHieuHamLuongVangs.ToList();
                List<SanPhamKhoCu> listSanPhamMuaVao = new List<SanPhamKhoCu>();
                KhachHang khachVangLai = dbContext.KhachHangs.FirstOrDefault(x => x.MaKhachHang == 1);

                int soNgay = (int)(DateTime.Today.Date - hopDong.NgayBatDau.Date).TotalDays;

                int tienLai = (int)Math.Round((double)hopDong.SoTienCam * (double)hopDong.LaiSuat) * (soNgay + 1);

                if (tienLai < 5000)
                {
                    tienLai = 5000;
                }

                DateTime now = DateTime.Now;
                ChiTietCamDo chiTietCamDo = new ChiTietCamDo
                {
                    MaChiTietCamDo = now.Year.ToString() + now.Month.ToString() + now.Day.ToString() + now.Hour.ToString() + now.Minute.ToString() + now.Second.ToString(),
                    MaHopDongCamDo = maHopDong,
                    MaNhanVien = hopDong.MaNhanVien,
                    NgayBatDauThang = hopDong.NgayBatDau,
                    NgayNopTienLai = DateTime.Today.Date,
                    SoTienDongLai = tienLai,
                };

                dbContext.ChiTietCamDoes.Add(chiTietCamDo);
                
                int giaMuaVaoAvg = (hopDong.SoTienCam - tienLai) / hopDong.SanPhamCamDoes.Count;
                int giaDuRa = hopDong.SoTienCam - giaMuaVaoAvg;

                LichSuMua lichSuMua = new LichSuMua
                {
                    MaLichSuMua = maLichSuMua,
                    MaKhachHang = khachVangLai.MaKhachHang,
                    MaNhanVien = maNhanVien,
                    NgayGiaoDich = DateTime.Today,
                    TongTienVang = hopDong.SoTienCam - tienLai,
                    ThanhTienHoaDon = hopDong.SoTienCam - tienLai,
                    TienCongThem = 0
                };

                foreach (SanPhamCamDo sanPham in hopDong.SanPhamCamDoes)
                {
                    listSanPhamMuaVao.Add(new SanPhamKhoCu
                    {
                        DonGiaMuaVao = 0,
                        GiaMuaVao = hopDong.SanPhamCamDoes.ToList().IndexOf(sanPham) == (hopDong.SanPhamCamDoes.Count - 1) ? giaMuaVaoAvg + giaDuRa : giaMuaVaoAvg,
                        KhoiLuongDa = 0,
                        KhoiLuongTinh = sanPham.CanNang,
                        KhoiLuongVang = sanPham.CanNang,
                        MaHamLuongVang = sanPham.MaHamLuongVang,
                        MaLichSuMua = lichSuMua.MaLichSuMua,
                        LoaiVangMuaVao = loaiVangMua,
                        NgayNhap = DateTime.Today,
                        TenSanPham = sanPham.TenMonHang,
                        TiLeHot = 0,
                        MaNhanVien = maNhanVien
                    });
                }

                hopDong.NgayBatDau = DateTime.Today.AddDays(1);
                hopDong.NgayThanhLy = DateTime.Today;
                hopDong.TrangThaiHopDong = HopDongCamDoConstant.DA_THANH_LY;
                hopDong.SoTienThanhLy = hopDong.SoTienCam - tienLai;

                dbContext.LichSuMuas.Add(lichSuMua);
                dbContext.SanPhamKhoCues.AddRange(listSanPhamMuaVao);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public List<HopDongCamDo> GetAllHopDong()
        {
            try
            {
                return new QuanLyTiemVangDbContext().HopDongCamDoes.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public HopDongCamDo GetHopDongById(string maHopDong)
        {
            try
            {
                return new QuanLyTiemVangDbContext().HopDongCamDoes.Find(maHopDong);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public HopDongCamDo GiaHanHopDong(string maHopDong, DateTime ngayGiaHan, int soTienDongLai)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                HopDongCamDo hopDong = dbContext.HopDongCamDoes.Find(maHopDong);
                DateTime now = DateTime.Now;
                ChiTietCamDo chiTietCamDo = new ChiTietCamDo
                {
                    MaChiTietCamDo = Util.TaoMaTheoNamThangNgayGioPhutGiay(),
                    MaHopDongCamDo = maHopDong,
                    MaNhanVien = hopDong.MaNhanVien,
                    NgayBatDauThang = hopDong.NgayBatDau,
                    NgayNopTienLai = ngayGiaHan.Date,
                    SoTienDongLai = soTienDongLai,
                };

                hopDong.NgayBatDau = ngayGiaHan;
                dbContext.ChiTietCamDoes.Add(chiTietCamDo);
                dbContext.SaveChanges();

                return hopDong;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
