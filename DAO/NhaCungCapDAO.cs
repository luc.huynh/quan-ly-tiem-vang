﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class NhaCungCapDAO
    {
        #region instance

        private static NhaCungCapDAO instance;

        public static NhaCungCapDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new NhaCungCapDAO();
                return instance;
            }
        }
        #endregion

        public bool Insert(NhaCungCap nhaCungCap)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                dbContext.NhaCungCaps.Add(nhaCungCap);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<NhaCungCap> GetListAllNhaCungCap()
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                return dbContext.NhaCungCaps.ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }

        public bool Update(NhaCungCap nhaCungCap)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                NhaCungCap entity = dbContext.NhaCungCaps.Find(nhaCungCap.MaNhaCungCap);
                dbContext.Entry(entity).CurrentValues.SetValues(nhaCungCap);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool Delete(int maNhaCungCap)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                dbContext.NhaCungCaps.Remove(dbContext.NhaCungCaps.Find(maNhaCungCap));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool IsHaveNhaCungCap(string tenNhaCungCap)
        {
            if (new QuanLyTiemVangDbContext().NhaCungCaps.Where(x => x.TenNhaCungCap.Equals(tenNhaCungCap)).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public NhaCungCap GetById(int id)
        {
            return new QuanLyTiemVangDbContext().NhaCungCaps.Find(id);
        }

    }
}
