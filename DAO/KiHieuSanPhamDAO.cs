﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class KiHieuSanPhamDAO
    {
        private static KiHieuSanPhamDAO instance;

        public static KiHieuSanPhamDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new KiHieuSanPhamDAO();
                return instance;
            }
        }

        public List<KiHieuSanPham> GetListAllKiHieuSanPham()
        {
            return new QuanLyTiemVangDbContext().KiHieuSanPhams.ToList();
        }

        public KiHieuSanPham Insert(KiHieuSanPham kiHieuSanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KiHieuSanPhams.Add(kiHieuSanPham);

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public KiHieuSanPham Update(KiHieuSanPham kiHieuSanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KiHieuSanPhams.Find(kiHieuSanPham.MaKiHieuSanPham);

                dbContext.Entry(entity).CurrentValues.SetValues(kiHieuSanPham);
                dbContext.SaveChanges();

                return kiHieuSanPham;
            }
            catch
            {
                return null;
            }
        }

        public KiHieuSanPham DeleteById(int maKiHieuSanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.KiHieuSanPhams.Remove(dbContext.KiHieuSanPhams.Find(maKiHieuSanPham));

                dbContext.SaveChanges();

                return entity;
            }
            catch
            {
                return null;
            }
        }
    }
}
