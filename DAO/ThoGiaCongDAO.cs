﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ThoGiaCongDAO
    {
        private static ThoGiaCongDAO instance;

        public static ThoGiaCongDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new ThoGiaCongDAO();
                return instance;
            }
        }

        public List<ThoGiaCong> GetList()
        {
            return new QuanLyTiemVangDbContext().ThoGiaCongs.ToList();
        }

        public ThoGiaCong Insert(ThoGiaCong thoGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.ThoGiaCongs.Add(thoGiaCong);

                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public ThoGiaCong Update(ThoGiaCong thoGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                ThoGiaCong entity = dbContext.ThoGiaCongs.Find(thoGiaCong.MaThoGiaCong);

                dbContext.Entry(entity).CurrentValues.SetValues(thoGiaCong);
                dbContext.SaveChanges();

                return thoGiaCong;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public ThoGiaCong DeleteById(int maThoGiaCong)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                ThoGiaCong entity = dbContext.ThoGiaCongs.Find(maThoGiaCong);

                dbContext.ThoGiaCongs.Remove(entity);
                dbContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
