﻿using DTO.Models;
using DTO.TemporaryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LichSuGiaVangDAO
    {
        private static LichSuGiaVangDAO instance;

        public static LichSuGiaVangDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuGiaVangDAO();
                return instance;
            }
        }

        public bool NhapGiaVangDauNgay(List<LichSuGiaVang> listGiaVangHienTai, int maNhanVien, DateTime now)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();

                foreach (LichSuGiaVang giaVangHienTai in listGiaVangHienTai)
                {
                    //Lấy ra 1 record mới nhất của vàng theo mã
                    LichSuGiaVang entity = dbContext.LichSuGiaVangs.Where(x => x.MaHamLuongVang == giaVangHienTai.MaHamLuongVang).OrderByDescending(x => x.NgayBan).FirstOrDefault();

                    if (entity != null && entity.NgayBan.Date == now.Date)
                    {
                        entity.GiaVangBanRa = giaVangHienTai.GiaVangBanRa;
                        entity.GiaVangMuaVao = giaVangHienTai.GiaVangMuaVao;
                        entity.MaNhanVien = maNhanVien;
                        entity.NgayBan = now.Date;
                    }
                    else
                    {
                        LichSuGiaVang lichSuGiaVang = new LichSuGiaVang
                        {
                            GiaVangBanRa = giaVangHienTai.GiaVangBanRa,
                            GiaVangMuaVao = giaVangHienTai.GiaVangMuaVao,
                            MaNhanVien = maNhanVien,
                            NgayBan = now.Date,
                            MaHamLuongVang = giaVangHienTai.MaHamLuongVang
                        };

                        dbContext.LichSuGiaVangs.Add(lichSuGiaVang);
                    }

                    SanPhamDAO.Instance.NhapGiaVangDauNgay(giaVangHienTai.GiaVangBanRa, giaVangHienTai.KiHieuHamLuongVang.HamLuongVang);
                }

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<LichSuGiaVang> GetAllGiaVangHienTai()
        {
            List<LichSuGiaVang> list = new List<LichSuGiaVang>();
            List<KiHieuHamLuongVang> kiHieuHamLuongVangs = KiHieuHamLuongVangDAO.Instance.GetListAllHamLuongVang();
            List<LichSuGiaVang> listLichSuGiaVangDb = new QuanLyTiemVangDbContext().LichSuGiaVangs.ToList();

            foreach (KiHieuHamLuongVang kiHieuHamLuongVang in kiHieuHamLuongVangs)
            {
                LichSuGiaVang lichSuGiaVangMoiNhat = listLichSuGiaVangDb
                    .OrderBy(x => x.NgayBan)
                    .LastOrDefault(x => x.MaHamLuongVang == kiHieuHamLuongVang.MaHamLuongVang);

                if (lichSuGiaVangMoiNhat != null)
                {
                    list.Add(lichSuGiaVangMoiNhat);
                }
                else
                {
                    list.Add(new LichSuGiaVang
                    {
                        GiaVangBanRa = 0,
                        GiaVangMuaVao = 0,
                        NgayBan = DateTime.Today,
                        MaHamLuongVang = kiHieuHamLuongVang.MaHamLuongVang,
                        KiHieuHamLuongVang = kiHieuHamLuongVang
                    });
                }
            }

            return list;
        }
    }
}
