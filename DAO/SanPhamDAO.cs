﻿using DTO.Models;
using DTO.UtilDTO;
using Share.Constant;
using Share.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class SanPhamDAO
    {
        private static SanPhamDAO instance;

        public static SanPhamDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SanPhamDAO();
                }
                return instance;
            }
        }

        public SanPham GetSanPhamById(string maSanPham)
        {
            return new QuanLyTiemVangDbContext().SanPhams.Find(maSanPham);
        }

        public SanPham GetSanPhamByMaTem(string maTem)
        {
            return new QuanLyTiemVangDbContext().SanPhams.FirstOrDefault(x => x.MaTem == maTem);
        }

        public List<SanPham> GetListSanPhamByLichSuBan(LichSuBan lichSuBan)
        {
            return new QuanLyTiemVangDbContext().SanPhams.Where(x => x.MaLichSuBan.Equals(lichSuBan.MaLichSuBan)).ToList();
        }

        public List<SanPham> GetListSanPhamChuaBan()
        {
            return new QuanLyTiemVangDbContext().SanPhams.Where(x => x.MaLichSuBan == null).ToList();
        }

        public List<SanPham> GetList()
        {
            return new QuanLyTiemVangDbContext().SanPhams.ToList();
        }

        public bool NhapGiaVangDauNgay(int giaVang, string tenHamLuongVang)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var listSanPham = dbContext.SanPhams.Where(x => x.MaLichSuBan == null && x.HamLuongVang == tenHamLuongVang).ToList();

                foreach (SanPham sanPham in listSanPham)
                {
                    double? giaBanTemp = sanPham.KhoiLuongVang * (giaVang / 100.0) + sanPham.TienCong;
                    double giaBan = giaBanTemp.HasValue ? giaBanTemp.Value : 0;

                    sanPham.GiaBan = TienUtil.LamTronTienViet((int)Math.Round(giaBan));
                }

                dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool TinhGiaBanAllSanPham()
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                List<LichSuGiaVang> listGiaVangHienTai = LichSuGiaVangDAO.Instance.GetAllGiaVangHienTai();

                foreach (LichSuGiaVang giaVangHienTai in listGiaVangHienTai)
                {
                    //Lấy ra 1 record mới nhất của vàng theo mã
                    LichSuGiaVang entity = dbContext.LichSuGiaVangs.Where(x => x.MaHamLuongVang == giaVangHienTai.MaHamLuongVang).OrderByDescending(x => x.NgayBan).FirstOrDefault();

                    NhapGiaVangDauNgay((int)giaVangHienTai.GiaVangBanRa, giaVangHienTai.KiHieuHamLuongVang.HamLuongVang);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public List<SanPham> NhapSanPhamVaoKho(List<SanPham> listSanPham)
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                List<SanPham> listSanPhamDb = dbContext.SanPhams.ToList();

                foreach (SanPham sanPham in listSanPham)
                {
                    PrepareUtil.SanPhamPrepare(sanPham, true);

                    string maSanPham = sanPham.TenSanPham[0].ToString().ToUpper() + "0000";
                    SanPham sanPhamLastes = dbContext.SanPhams.ToList().Where(x => x.MaSanPham[0].Equals(sanPham.TenSanPham[0])).OrderByDescending(p => p.MaSanPham).FirstOrDefault();

                    if (sanPhamLastes != null)
                    {
                        maSanPham = sanPhamLastes.MaSanPham;
                    }

                    if (string.IsNullOrWhiteSpace(sanPham.MaSanPham))
                    {
                        maSanPham = XuLyMaSanPham(maSanPham);

                        while (listSanPhamDb.Where(x => x.MaTem.Equals(maSanPham)).Count() > 0
                            || listSanPham.Where(x => (x.MaTem != null && x.MaTem.Trim().Length > 0) && x.MaTem.Equals(maSanPham)).Count() > 0)
                        {
                            maSanPham = XuLyMaSanPham(maSanPham);
                        }

                        sanPham.MaSanPham = maSanPham;
                    }

                    if (string.IsNullOrWhiteSpace(sanPham.MaTem))
                    {
                        sanPham.MaTem = sanPham.MaSanPham;
                    }
                }

                List<SanPham> listInsert = dbContext.SanPhams.AddRange(listSanPham).ToList();
                dbContext.SaveChanges();

                return listInsert;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public SanPham Update(SanPham sanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.SanPhams.Find(sanPham.MaSanPham);

                PrepareUtil.SanPhamPrepare(sanPham);

                if (sanPham.MaTem == null)
                {
                    sanPham.MaTem = entity.MaTem;
                }

                dbContext.Entry(entity).CurrentValues.SetValues(sanPham);
                dbContext.SaveChanges();

                return sanPham;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public bool DeleteSanPham(SanPham sanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.SanPhams.Remove(dbContext.SanPhams.Find(sanPham.MaSanPham));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private string XuLyMaSanPham(string maSanPham)
        {
            char phanDau = maSanPham[0];
            int phanSau = int.Parse(maSanPham.Substring(1));
            string so0 = "";

            phanSau++;

            if (phanSau < 10) so0 += "000";
            else if (phanSau < 100) so0 += "00";
            else if (phanSau < 1000) so0 += "0";

            return (phanDau + so0.ToString() + phanSau.ToString()).ToUpper();
        }

        public bool XoaMaLichSuBan(List<SanPham> listSanPham)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                foreach (SanPham sanPham in listSanPham)
                {
                    dbContext.SanPhams.Find(sanPham.MaSanPham).MaLichSuBan = null;
                }

                dbContext.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool XoaListSanPhamXuatKho()
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();

                dbContext.SanPhams.RemoveRange(dbContext.SanPhams.Where(x => x.MaLichSuBan == null));
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }
    }
}
