﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LoaiTienDAO
    {
        private static LoaiTienDAO instance;

        public static LoaiTienDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new LoaiTienDAO();
                return instance;
            }
        }

        public List<LoaiTien> GetList()
        {
            return new QuanLyTiemVangDbContext().LoaiTiens.ToList();
        }

        public bool Insert(LoaiTien loaiTien)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.LoaiTiens.Add(loaiTien);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public List<LoaiTien> getListAllLoaiTien()
        {
            return new QuanLyTiemVangDbContext().LoaiTiens.ToList();
        }

        public bool Update(LoaiTien loaiTien)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                var entity = dbContext.LoaiTiens.Find(loaiTien.MaLoaiTien);
                dbContext.Entry(entity).CurrentValues.SetValues(loaiTien);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool Delete(int maLoaiTien)
        {
            try
            {
                var dbContext = new QuanLyTiemVangDbContext();
                dbContext.LoaiTiens.Remove(dbContext.LoaiTiens.Find(maLoaiTien));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
