﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.TemporaryModel;
using DTO.Models;

namespace DAO
{
    public class LichSuTyGiaTienDAO
    {
        private static LichSuTyGiaTienDAO instance;

        public static LichSuTyGiaTienDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new LichSuTyGiaTienDAO();
                return instance;
            }
        }

        public List<LichSuTyGiaTien> GetAllTyGiaTien()
        {
            if (!ThemLichSuTyGiaTienThieu())
            {
                return null;
            }

            QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
            List<LoaiTien> loaiTiens = dbContext.LoaiTiens.ToList();
            List<LichSuTyGiaTien> listLoaiTien = new List<LichSuTyGiaTien>();

            foreach (LoaiTien loaiTien in loaiTiens)
            {
                int maLoaiTien = loaiTien.MaLoaiTien;
                LichSuTyGiaTien lichSuGiaTienMoiNhat = dbContext.LichSuTyGiaTiens.ToList().LastOrDefault(x => x.MaLoaiTien == maLoaiTien);
                if (lichSuGiaTienMoiNhat == null)
                {
                    listLoaiTien.Add(new LichSuTyGiaTien
                    {

                    });
                }
                else
                {
                    listLoaiTien.Add(lichSuGiaTienMoiNhat);
                }
            }

            return listLoaiTien;
        }

        public bool ThemLichSuTyGiaTienThieu()
        {
            try
            {
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();
                List<LoaiTien> loaiTiens = dbContext.LoaiTiens.ToList();
                List<LichSuTyGiaTien> listLichSuTyGiaTienDb = dbContext.LichSuTyGiaTiens.ToList();
                List<LichSuTyGiaTien> listLichSuTyGiaTienAdd = new List<LichSuTyGiaTien>();

                foreach (LoaiTien loaiTien in loaiTiens)
                {
                    if (listLichSuTyGiaTienDb.Where(x => x.MaLoaiTien == loaiTien.MaLoaiTien).Count() == 0)
                    {
                        listLichSuTyGiaTienAdd.Add(new LichSuTyGiaTien
                        {
                            GiaBanRa = 0,
                            GiaMuaVao = 0,
                            MaLoaiTien = loaiTien.MaLoaiTien,
                            NgayNhap = DateTime.Today
                        });
                    }
                }

                dbContext.LichSuTyGiaTiens.AddRange(listLichSuTyGiaTienAdd);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public bool NhapTyGiaTien(List<LichSuTyGiaTien> listTyGiaTienHienTai, DateTime today)
        {
            try
            {
                DateTime now = DateTime.Now;
                QuanLyTiemVangDbContext dbContext = new QuanLyTiemVangDbContext();

                foreach (LichSuTyGiaTien tyGiaTienHienTai in listTyGiaTienHienTai)
                {
                    //Lấy ra 1 record mới nhất của tiền theo mã
                    LichSuTyGiaTien entity = dbContext.LichSuTyGiaTiens.Where(x => x.MaLoaiTien == tyGiaTienHienTai.MaLoaiTien).OrderByDescending(x => x.NgayNhap).FirstOrDefault();

                    if (entity != null)
                    {
                        if (entity.NgayNhap.Date == today.Date)
                        {
                            entity.GiaBanRa = tyGiaTienHienTai.GiaBanRa;
                            entity.GiaMuaVao = tyGiaTienHienTai.GiaMuaVao;
                        }
                        else
                        {
                            entity.GiaBanRa = tyGiaTienHienTai.GiaBanRa;
                            entity.GiaMuaVao = tyGiaTienHienTai.GiaMuaVao;
                            entity.NgayNhap = now;

                            dbContext.LichSuTyGiaTiens.Add(entity);
                        }
                    }
                    else
                    {
                        LichSuTyGiaTien lichSuTyGiaTien = new LichSuTyGiaTien
                        {
                            GiaBanRa = tyGiaTienHienTai.GiaBanRa,
                            GiaMuaVao = tyGiaTienHienTai.GiaMuaVao,
                            NgayNhap = now,
                            MaLoaiTien = tyGiaTienHienTai.MaLoaiTien
                        };

                        dbContext.LichSuTyGiaTiens.Add(lichSuTyGiaTien);
                    }
                }

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }
    }
}
